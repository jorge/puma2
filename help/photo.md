# Métriques pour les photos

## Compter le nombre d'éléments dans un répertoire 

```
ls | wc -l
```

## Taille moyenne d'un fichier dans un répertoire

```
ls -l | gawk '{sum += $5; n++;} END {print sum/n;}'
```

# Assez d'espace pour les photos ?

Pour l'instant la taille moyenne d'une photo (après redomension à max 400x400) est de 65k.

Il y a +/- 10G d'espace de libre sur le serveur.

D'où :

```
65k          <->   1       photo
1000k (1M)   <->   15.4    photo
1000M (1G)   <->   15400   photo
10 G         <->   154000  photo 
```
