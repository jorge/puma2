# Configurer apache2

## Création d'un nouveau site

- aller dans /etc/apache2/
- ajouter un nouveau fichier XXX.conf dans sitesavailable (et configurer le site)
- a2ensite XXX.conf (rendre le site active)
- service apache reload

/!\ le répertoire qui contient le site web doit appartenir à l'utilisateur `ẁww-data`
-> si le répertoire est XXX, il faut taper la commande : `sudo chown www-data:www-data -R XXX`
