# Installation d'un serveur

## Installer php sur serveur :

$ apt install php7.0
$ apt install php7.0-zip
$ apt install php7.0-xml
$ apt install php7.0-mysql

## Installer mysl sur serveur :

$  apt install mysql-server

## Installer apache2:

$ apt install apache2

## Installer composer selon https://getcomposer.org/download/

$ php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
$ php -r "if (hash_file('SHA384', 'composer-setup.php') === '669656bab3166a7aff8a7506b8cb2d1c292f042046c5a994c43155c0be6190fa0355160742ab2e1c88d40d5be660b410') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
$ php composer-setup.php
$ php -r "unlink('composer-setup.php');"

# Installer les dépendances
$ php composer.phar install

## Mettre à jour la configuration

$ vi app/config/config.yml
$ vi app/config/parameters.yml

## Configurer apache2

voir document apache2.txt
