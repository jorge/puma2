# Accès au fichier des logs

## En local

- regarder les fichiers qui se trouvent dans `var/logs/`

## Sur le serveur

- se connecter en ssh sur le serveur

- aller où est installé le programme (test : `/var/www/puma_test`; pour prod : `/var/www/puma_prod`)

- aller dans le répertoire des logs : `var/logs`

- explorer le fichier des logs (en prod : `prod.log`; sinon `dev-XXX.log`) à l'aide des commandes `tail`, `cat` et `grep` (voir plus bas pour plus de détails)

## Lire les logs à l'aide de `tail`, `cat` et `grep`

### `tail`

`tail` permet d'afficher la fin d'un ficher. Exemple :

``` 
tail prod.log
```

On peut utiliser le paramètre `-n XXX` pour choisir le nombre de lignes qui sera affiché. Exemple pour voir `200` dernières lignes :

```
tail -n 200 prod.log
```

### `grep`

`grep` permet de filtrer les données d'un pipe. Exemple pour affichier les lignes qui contiennent le mot `CRITICAL` :

```
... | grep CRITICAL 
```

Utilisé avec `tail`, on peut afficher toutes les lignes contenant `CRITICAL` des 200 dernières lignes de `prod.log` :

```
tail -n 200 prod.log | grep CRITICAL
```

### `cat`

`cat` permet de lire afficher et le le mettre dans un pipe. Il peut être utilisé avec `grep` pour afficher toutes les lignes de `prod.log` qui contiennent `CRITICAL`

```
cat prod.log | grep CRITICAL
``