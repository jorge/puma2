# Hébergement

## Serveur de test
test.puma.ovh

### Mettre à jour le serveur de test

```
$ # Se connecter sur le serveur :
$ ssh root@151.80.144.77 -i "C:/Users/Jorge.CSA/.ssh/id_rsa"
$ # Aller dans le repertoire ou se trouve le code :
$ cd /var/www/puma_test
$ # Lancer le script d'install :
$ bash update.sh
$ # On doit entre le mdp de puma serveur
```

Que fait le script `update.sh` ? Il contient un ensemble de commandes qui
sont lancées les unes après les autres. Pour les voir faire un `more update.sh`.

## Serveur master de prod
puma.ovh ( 151.80.144.77 )

### Mettre à jour le serveur de prod

Même procédure que pour le serveur de test mais à faire dans le
répertoire `/var/www/puma_prod`.

## Phpmysql
pda.puma.ovh ( 151.80.144.77:8080 )
