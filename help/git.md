# Aide à l'utilisation de GIT

## Bonne pratique

- Ne jamais travailler dans la branche 'prod'

## Modif et commit

### Ajout d'une modif (dans le commit en cours de création)

git add liens_vers_fichier_a_ajouter_dans_le_commit

### Creation d'un commit

git commit -m "message décrivant le commit"

## Branche

### Savoir dans quelle branche on se trouve :

$ git status

La première ligne indique sur quelle branche on se trouve. Par ex

`On branch master`

### Changer de branche (en local)

$ get checkout nom_branche

par exemple `checkout prod` ou `checkout master`

### Rapatrier tous les changements vers la branche courante

$ git merge nom_branch_qu_on_veut_rapatrier

## Mettre le code sur prod

$ git checkout prod      (commentaire : on va sur la branche prod)
$ git merge master       (commentaire : on prend tous les commits de master)
$ git push origin prod   (commenaitre : on envoie les fichiers sur prod)
$ git checkout master    (commentaire : on revient en master)
