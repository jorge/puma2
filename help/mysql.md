# Commandes de bases (sur le serveur)

## Accès à mysql
`mysql -u puma_03 -p -D puma_test` où

- `-u puma_03` indique que puma_03 est l'utilisateur
- `-p` indique qu'on veut encoder un mot de passe
- `-D puma_test` indique qu'on se connecte à la db puma_test

### Supprimer une base de données

`DROP DATABASE XXX` où `XXX` est le nom de la db.

### Créer une base de données

`CREATE DATABASE IF NOT EXISTS XXX DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;` où `XXX` est le nom de la db.

## Status mysql
se connecter sur le serveur
`service mysql status`

## Arrêter mysql
se connecter sur le serveur
`service mysql stop`

## Démarrer mysql
se connecter sur le serveur
`service mysql start`

## Mysqldump (sauvegarde de la db)

`mysqldump -u puma_03 -p puma_03 > /tmp/dump-db` où

- `-u puma_03` indique que puma_03 est l'utilisateur
- `-p` indique qu'on veut encoder un mot de passe
- `puma_03` (après le `-p`) indique qu'on travaille sur la db `puma_03`

## Charger le dump en db

`mysql -u puma_03 -p puma_03 < /tmp/dump-db`


## Modifier les utilisateurs

CREATE USER 'user'@'%' IDENTIFIED BY 'password';

TODO

# Services

## PhpmyAdmin

se trouve sur le serveur sur le port 8080
