# Trouver git-bash

Il se trouve dans `C:/puma/git/git-bash.exe` (faire un racourci sur le bureau)


# Vérification à faire

## Config git

```
$ git remote -v
```

ça doit afficher `origin	https://framagit.org/jorge/puma2.git (fetch)`

Pour réparer : `git remote set-url origin https://framagit.org/jorge/puma2.git`

## Fuseau horaire DB (mysql)

Vérifier pas de décallage horaire sur la page http://localhost:8000/synchro/dates-test/5

## Vérifier php ftp

Regarder dans `php.ini`

# Mettre à jour le code (et pas les librairies tierses utilisées) (maj-puma.bat)

```
$ cd C:/puma/puma3 # aller dans le répertoire ou est installé puma
$ git pull origin prod
$ php bin/console doctrine:migrations:migrate
```

# Mettre à jour les librairies tierses (comme maj-complete.bat)

```
$ cd C:/puma/puma3 # aller dans le répertoire ou est installé puma
$ git pull origin prod
$ composer install
$ php bin/console doctrine:migrations:migrate
```

# Tester l'install

Lancer puma et aller voir sur 127.0.0.1:8000

# Changer la db (comme update_db.bat)

Dézipper le dump de la db dans `C:/puma/puma3` ici on suppose quel le fichier sql est `C:/puma/puma3/2017-09-20__03h09m01s.sql`.

Attention ne pas utiliser git-bash mais utiliser l'invite de commande windows
(sinon le route des commandes n'est pas affiché).

```
mysql -h localhost -u root -p puma_03 -e "DROP DATABASE puma_03;"
mysql -h localhost -u root -p -e "CREATE DATABASE IF NOT EXISTS puma_03 DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;"
mysql -h localhost -u root -p -e "source C:/puma/puma3/2017-09-20__03h09m01s.sql" puma_03
```
