# SCSS

Le SCSS est utilisé car il est beaucoup plus puissant que le CSS :

- propose des variables
- permet d'imbriquer les règles

Actuellement le fichier scss se trouve dans `assets/css/app.scss` et il génère le
fichier `web/build/app.css`. Pour info, la configuration de ce comportement (webpack)
se trouve dans `ẁebpack.config.js`

# SCSS & Symfony

Pour utiliser le SCSS dans symfony nous avons besoin de `npm` (Node Package Manager).

Il nous permet d'installer `yarn` :

```
$ npm install
```

Une fois ça fait nous pouvons lancer la compilation scss -> css :

```
$ ./node_modules/.bin/encore dev --watch
```

Le paramètre `--watch` indique qu'en permanance il regarde le fichier `app.scss` et si ce
fichier a changé, il lance la compilation automatiquement.
