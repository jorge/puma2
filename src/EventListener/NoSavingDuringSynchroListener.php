<?php

declare(strict_types=1);

namespace App\EventListener;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\OnFlushEventArgs;

final class NoSavingDuringSynchroListener
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /*
     * Check that no synchronization is running. If it is the case, this function
     * Throw exception.
     */
    public function onFlush(OnFlushEventArgs $eventArgs)
    {
        // TODO: Does not exist in SynchroSessionRepository->findByClosed().
        $notClosedSessions = $this->em->getRepository(SynchroSession::class)->findByClosed(false);

        /*
        if (count($notClosedSessions) >= 1) {
            throw new \Exception("Une synchronisation est en cours. Il n'est pas possible d'enregistrer des données.", 1);
        }
         */
    }
}
