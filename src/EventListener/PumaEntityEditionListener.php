<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\PumaEntity;
use App\Entity\RecordType;
use DateTime;
use DateTimeZone;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\Security\Core\Security;

final class PumaEntityEditionListener
{
    private Security $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }

    /**
     * Event to register when the entity is linked to a record (fr: fiche (F0, F1, ...)) that
     * the record has been edited (who and when).
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof PumaEntity) {
            return;
        }

        $now = new DateTime();
        $now->setTimeZone(new DateTimeZone('UTC'));

        $recordType = $entity->getRecordType();

        if (null === $recordType) {
            return;
        }

        if (RecordType::isFRecord($recordType)) { // recordType is F0 ... F5
            $annee = $entity->getAnnee();
            $exploitation = $entity->getExploitation();

            $collecte = $exploitation->getCollecteFor($annee, RecordType::fRecordToCollectIdType($recordType));
            $collecte->setLastEditor($this->security->getUser());
            $collecte->setUDate($now);

            $entityManager = $args->getObjectManager();
            $entityManager->persist($collecte);
            $entityManager->flush();
        }
    }

    /**
     * Event to register when and who edited the object.
     */
    public function preUpdate(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();

        if (!$entity instanceof PumaEntity) {
            return;
        }

        $now = new DateTime();
        $now->setTimeZone(new DateTimeZone('UTC'));

        $entity->setUDate($now);
        $entity->setLastEditor($this->security->getUser());
    }
}
