<?php

declare(strict_types=1);

namespace App\EventListener;

use DateTime;
use DateTimeZone;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

use function chr;
use function function_exists;
use function is_array;
use function ord;

// TODO A METTRE AILLEURS
// see http://php.net/manual/fr/function.com-create-guid.php
function guidv4()
{
    if (function_exists('com_create_guid')) {
        return trim(com_create_guid(), '{}');
    }
    $data = openssl_random_pseudo_bytes(16);
    $data[6] = chr(ord($data[6]) & 0x0f | 0x40); // set version to 0100
    $data[8] = chr(ord($data[8]) & 0x3f | 0x80); // set bits 6-7 to 10

    return vsprintf('%s%s-%s-%s-%s-%s%s%s', mb_str_split(bin2hex($data), 4));
}

final class InitializeSynchroStatusKernelControllerListener
{
    private EntityManagerInterface $em;

    private $synchroRole;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $parameterBag)
    {
        $this->synchroRole = $parameterBag->get('synchro_role');
        $this->em = $em;
    }

    public function __invoke(ControllerEvent $event)
    {
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony2 but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        $controller = $controller[0];

        /*
        $synchroStatus = $this->em->getRepository(SynchroStatus::class)->findUnique();

        // ONLY FOR LOCAL
        if($this->synchroRole === SynchroCodeCommun::ROLE_LOCAL) {
            if(! $synchroStatus->isLocalInitialized()) {
                $synchroStatus->setLocalInitUdate($this->getDateNowUTC());
                $synchroStatus->setLocalInitialized(True);
                $synchroStatus->setLocalId(guidv4());
                $this->em->flush();
            }

            if($synchroStatus->getLocalId() === Null) {
                $synchroStatus->setLocalId(guidv4());
                $this->em->flush();
            }
        } else { // FOR MASTER CHECK THAT isLocalInitialized is not TRUE !
            if($synchroStatus->isLocalInitialized()) {
                throw new \Exception("La ligne de SynchroStatus du master ne peut être initialisée", 1);
            }
        }
         */
    }

    // TODO A METTRE AILLEURS
    private function getDateNowUTC()
    {
        $now = new DateTime('NOW');
        $now->setTimeZone(new DateTimeZone('UTC'));

        return $now;
    }
}
