<?php

declare(strict_types=1);

namespace App\EventListener;

use App\Entity\SynchroSession;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpKernel\Event\ControllerEvent;

use function count;

final class SetWarningNoSavingListener
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function __invoke(ControllerEvent $event)
    {
        $request = $event->getRequest();
        $session = $request->getSession();
        // TODO: Does not exist in SynchroSessionRepository->findByClosed().
        $notClosedSessions = $this->em->getRepository(SynchroSession::class)->findByClosed(false);

        if (count($notClosedSessions) >= 1) {
            $session->set('synchro_is_running', '1');
        } elseif ($session->has('synchro_is_running')) {
            $session->remove('synchro_is_running');
        }
    }
}
