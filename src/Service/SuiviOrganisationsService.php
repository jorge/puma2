<?php

declare(strict_types=1);

/*
 * Copyright (C) 2020, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Service;

use App\Entity\TblValeurChoixSimple;
use Symfony\Component\PropertyAccess\PropertyAccess;

class SuiviOrganisationsService
{
    public function getScore($suivi, $fieldId)
    {
        $propertyAccessor = PropertyAccess::createPropertyAccessor();
        $value = $propertyAccessor->getValue($suivi, $fieldId);

        if ('existenceStatus' === $fieldId || 'existenceROI' === $fieldId || 'affiliationFederation' === $fieldId) {
            return $this->score4bool($value);
        }

        if (
            'compteBancaire' === $fieldId || 'classementJustificatifs' === $fieldId || 'rapportAnnuelActivites' === $fieldId
            || 'rapportsFinanciereAnnuel' === $fieldId || 'planActionAnnuel' === $fieldId || 'planStrategiquePluriannuel' === $fieldId
        ) {
            return $this->score4bool($value, 2);
        }

        if (
            'rapportAnnuelActivites' === $fieldId || 'rapportSuiviPlanStrategique' === $fieldId
        ) {
            return $this->score4bool($value, 3);
        }

        if (
            'auditAnnuelDesComptes' === $fieldId
        ) {
            return $this->score4bool($value, 4);
        }

        if ('budget' === $fieldId) {
            return $this->scoreIfNotNull($value, 2);
        }

        if ('compteBancaire' === $fieldId || 'classementJustificatifs' === $fieldId) {
            $lc = $propertyAccessor->getValue($suivi, 'livreCaisse');
            $cr = $propertyAccessor->getValue($suivi, 'carnetRecus');
            $ls = $propertyAccessor->getValue($suivi, 'livretSocietaire'); // atention si on met aux 3 pas faire 3 fois l'addition

            if ($lc || $cr || $ls) {
                return 4; // todo autre solution ne le mettre qu'a lc et pas à cr ni ls
            }
        }

        if (
            'nbrReunionsAG' === $fieldId || 'nbrReunionsCE' === $fieldId || 'nbrReunionsCS' === $fieldId
            || 'alternanceCENbr' === $fieldId || 'nbrPVManquantsAG' === $fieldId || 'nbrPVManquantsCE' === $fieldId
            || 'majListeDeMembres' === $fieldId || 'comptabilite' === $fieldId || 'degrePriseEnChargeAG' === $fieldId
            || 'nbrContratsAvecActeursEco' === $fieldId || 'nbrCreditAvecIMFOuBanque' === $fieldId || 'signaturesPourSortieFonds' === $fieldId
        ) {
            return $this->score4ChoixSimple($value);
        }

        return null;
    }

    public function score4bool($value, $ret4True = 1, $ret4False = 0, $ret4Other = null)
    {
        if (true === $value) {
            return $ret4True;
        }

        if (false === $value) {
            return $ret4False;
        }

        return $ret4Other;
    }

    public function score4ChoixSimple($value, $ret4Null = null)
    {
        if ($value instanceof TblValeurChoixSimple) {
            $score = $value->getScore();

            if (null === $score) {
                return $ret4Null;
            }

            return $score;
        }

        return $ret4Null;
    }

    public function scoreIfNotNull($value, $ret4NotNull = 1, $ret4Null = 0)
    {
        if (null === $value) {
            return $ret4Null;
        }

        return $ret4NotNull;
    }
}

// niveauxReconnaissance
// manue
// rapportAnnuelActivites
// doubleSignatures
