<?php

declare(strict_types=1);

namespace App\Service\NumeroMembre;

interface GenerateurInterface
{
    /**
     * Generate the next available 'numero de membre'.
     */
    public function generateNextNumeroDeMembre(): int;

    /**
     * Give to  all the exploitations with a numero de membre null
     * a numero de membre.
     */
    public function generationNumeroMembre(): void;
}
