<?php

declare(strict_types=1);

namespace App\Service\NumeroMembre;

use App\Entity\Exploitations;
use App\Synchro\SynchroCodeCommun;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Service qui gère la génération des numéros de membres.
 */
final class Generateur implements GenerateurInterface
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, ParameterBagInterface $parameterBag)
    {
        $synchroRole = $parameterBag->get('synchro_role');

        if (SynchroCodeCommun::ROLE_MASTER !== $synchroRole) {
            throw new Exception('Ce service ne peut être utilisé que par master', 1);
        }

        $this->em = $em;
    }

    public function generateNextNumeroDeMembre(): int
    {
        $numeroDeMembreMax = $this->em->createQueryBuilder()
            ->select('MAX(e.numeroMembre)')
            ->from(Exploitations::class, 'e')
            ->getQuery()
            ->getSingleScalarResult();

        return $numeroDeMembreMax + 1;
    }

    public function generationNumeroMembre(): void
    {
        // TODO AVOIR UN LOCK ?
        // -> sauf si on est sur d'être avec un lock

        ini_set('max_execution_time', -1);
        ini_set('memory_limit', -1);

        $repository = $this->em->getRepository(Exploitations::class);
        $membresSansNumero = $repository->findBy(['numeroMembre' => null]);

        $nextNumeroDeMembre = $this->generateNextNumeroDeMembre();

        foreach ($membresSansNumero as $membre) {
            $membre->setNumeroMembre($nextNumeroDeMembre);
            ++$nextNumeroDeMembre;
        }

        $this->em->flush();
    }
}
