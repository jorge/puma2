<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\SynchroSession;
use App\Entity\SynchroStatus;
use Swift_Message;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class SendDebugInfoCommand extends Command
{
    protected static $defaultName = 'puma:send-debug-info';

    protected function configure()
    {
        $this->setDescription('Envoie un email à Jorge avec des infos de debug')
            ->setHelp('Envoie un email à Jorge avec des infos de debug');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $mailer = $this->getContainer()->get('swiftmailer.mailer');
        $em = $this->getContainer()->get('doctrine')->getManager();
        $twig = $this->getContainer()->get('templating');

        $lastCommitId = exec('git log --pretty=format:"%h %ad  %s" --date=short -n 1');
        $synchroStatus = $em->getRepository(SynchroStatus::class)->findUnique();
        $last10SyncSession = $em->getRepository(SynchroSession::class)->findBy([], ['dateDebutData' => 'DESC'], 10);

        $message = (new Swift_Message('Info mise à jour Puma'))
            ->setFrom(['csapumacontrol@gmail.com' => 'Puma Control'])
            ->setTo(['jorge.palma@csa-be.org', 'marc.ducobu@champs-libres.coop'])
            ->setBody(
                $twig->render(
                    'emails/info-debug.txt.twig',
                    [
                        'last_commit_id' => $lastCommitId,
                        'last_10_sync_session' => $last10SyncSession,
                        'synchro_status' => $synchroStatus,
                    ]
                ),
                'text/html'
            );

        $mailer->send($message);
    }
}
