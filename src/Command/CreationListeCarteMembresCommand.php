<?php

declare(strict_types=1);

namespace App\Command;

// use Symfony\Component\Console\Command\Command;
use App\Utils\ZipFile;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use function count;

class CreationListeCarteMembresCommand extends Command
{
    protected static $defaultName = 'puma:creation-liste-carte-membres';

    public function getAbsolutePathForImagesDir($strKey)
    {
        $pjtDir = $this->getContainer()->getParameter('kernel.project_dir');
        $CsvWebdir = $this->getContainer()->getParameter($strKey);

        return realpath($pjtDir . '/public' . $CsvWebdir);
    }

    protected function configure()
    {
        $this->setDescription('Crée fichiers excel et zip avec liste de membres avec les photos et QRcode respectives')
            ->setHelp('Creation listes pour carte de membres')
            ->addArgument('code', InputArgument::REQUIRED, 'Quelle coopérative vous voulez chercher?');
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine')->getManager();
        $code = $input->getArgument('code');
        $result = $this->_getListePourCarte($em, $code);
        $dql = $result['dql'];
        $stmt = $em->getConnection()
            ->prepare($dql);
        $stmt->execute();

        if (false !== $stmt) {
            $res = $stmt->fetchall();
            //        print(count($res));
            $cpt = count($res);
        //        $output->write($cpt);
        } else {
            //        print("aie aie aie");
            $cpt = 0;
            $output->write($cpt);
        }
        $zipFile = new ZipFile();
        $pstrNameZip = $code . '.zip';
        $qrCodeDir = $this->getAbsolutePathForImagesDir('exploitations_qrcode_webdir') . '/';
        $photoDir = $this->getAbsolutePathForImagesDir('personnes_photo_webdir') . '/';
        $csvCarteMembreWebdir = $this->getContainer()->getParameter('donnees_carte_membre_webdir');
        $filename = $code . '.csv';
        $path_and_file = 'web' . $csvCarteMembreWebdir . '/' . $filename;
        $output->write('zip: ' . $path_and_file);
        $output->write('dirCode: ' . $qrCodeDir);
        $output->write('dirPhoto:' . $photoDir);
        $list = [];
        $list[] = array_values($result['fields']);

        for ($i = 0; $i < $cpt; ++$i) {
            $photoName = $res[$i]['Photo'];
            $qrcodeName = $res[$i]['qrimage'];

            if ('' !== $photoName) {
                $msgPhoto = $zipFile->farr_addFileToArchive($photoDir, $photoName, $pstrNameZip);
            }

            if ('' !== $qrcodeName) {
                $msgQRcode = $zipFile->farr_addFileToArchive($qrCodeDir, $qrcodeName, $pstrNameZip);
            }
            $list[] = array_values($res[$i]);
        }

        // Output array into CSV file
        $fp = fopen($path_and_file, 'wb');
        header('Content-Type: text/csv');
        header('Content-Disposition: attachment; filename="' . $path_and_file . '"');

        foreach ($list as $ferow) {
            fputcsv($fp, $ferow, ';');
        }
        fclose($fp);
    }

    private function _getListePourCarte($em, $code)
    {
        $where = " AND tc.code ='" . $code . "'";

        $fields = ['NumeroMembre', 'Cni', 'Nom', 'Prenom', 'Cooperative', 'Groupement', 'Province', 'Commune', 'Colline', 'Photo', 'qrimage'];
        $query = $em->createQuery();
        $baseQuery = 'SELECT ';
        $baseQuery .= ' e.numero_membre as NumeroMembre, ';
        $baseQuery .= " (SELECT MAX(pch.cni) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Cni,";
        $baseQuery .= " (SELECT MAX(pch.nom) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Nom,";
        $baseQuery .= " (SELECT MAX(pch.prenom) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Prenom,";
        $baseQuery .= ' MAX(tc.denomination) as Cooperative,';
        $baseQuery .= ' MAX(t.denomination) as Groupement,';
        $baseQuery .= ' tprov.denomination as Province,';
        $baseQuery .= ' tcom.denomination as Commune,';
        $baseQuery .= ' tcol.denomination as Colline,';
        $baseQuery .= " (SELECT MAX(pch.photo) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Photo,";
        $baseQuery .= ' e.qrimage as qrimage';
        $baseQuery .= ' FROM exploitations e';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tcol ON e.localisation_admin_id =tcol.id ';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tcom ON tcol.parent_id =tcom.id ';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tprov ON tcom.parent_id =tprov.id ';
        $baseQuery .= ' INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id ';
        $baseQuery .= ' INNER JOIN tbl_organisations t ON t.id = p.organisation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations tc ON t.parent_id = tc.id';
        $baseQuery .= ' WHERE e.radie=0 ' . $where . ' GROUP BY NumeroMembre';
        $final = $baseQuery . ' ORDER BY e.numero_membre';
        // render the recordset in a new page? or into the same one?
        return [
            'fields' => $fields,
            'titre' => 'Resultat de la rêquete ',
            'dql' => $final,
        ];
    }
}
