<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblServicesAuxMembres;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblServicesAuxMembres controller.
 *
 * @Route("/{_locale}/references/tblservicesauxmembres")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblServicesAuxMembresController extends AbstractController
{
    /**
     * Deletes a TblServicesAuxMembres entity.
     *
     * @Route("/{id}", name="references_tblservicesauxmembres_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblServicesAuxMembres $tblServicesAuxMembre)
    {
        $form = $this->createDeleteForm($tblServicesAuxMembre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblServicesAuxMembre);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblservicesauxmembres_index');
    }

    /**
     * Displays a form to edit an existing TblServicesAuxMembres entity.
     *
     * @Route("/{id}/edit", name="references_tblservicesauxmembres_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblServicesAuxMembres $tblServicesAuxMembre)
    {
        $deleteForm = $this->createDeleteForm($tblServicesAuxMembre);
        $editForm = $this->createForm('App\Form\TblServicesAuxMembresType', $tblServicesAuxMembre);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblServicesAuxMembre);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblservicesauxmembres_index', ['id' => $tblServicesAuxMembre->getId()]);
        }

        return $this->render('tblservicesauxmembres/edit.html.twig', [
            'tblServicesAuxMembre' => $tblServicesAuxMembre,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblServicesAuxMembres entities.
     *
     * @Route("/", name="references_tblservicesauxmembres_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblServicesAuxMembres = $em->getRepository(TblServicesAuxMembres::class)->findAll();

        return $this->render('tblservicesauxmembres/index.html.twig', [
            'tblServicesAuxMembres' => $tblServicesAuxMembres,
        ]);
    }

    /**
     * Creates a new TblServicesAuxMembres entity.
     *
     * @Route("/new", name="references_tblservicesauxmembres_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblServicesAuxMembre = new TblServicesAuxMembres($user);
        $form = $this->createForm('App\Form\TblServicesAuxMembresType', $tblServicesAuxMembre);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblServicesAuxMembre);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblservicesauxmembres_index', ['id' => $tblServicesAuxMembre->getId()]);
        }

        return $this->render('tblservicesauxmembres/new.html.twig', [
            'tblServicesAuxMembre' => $tblServicesAuxMembre,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblServicesAuxMembres entity.
     *
     * @Route("/{id}", name="references_tblservicesauxmembres_show", methods={"GET"})
     */
    public function showAction(TblServicesAuxMembres $tblServicesAuxMembre)
    {
        $deleteForm = $this->createDeleteForm($tblServicesAuxMembre);

        return $this->render('tblservicesauxmembres/show.html.twig', [
            'tblServicesAuxMembre' => $tblServicesAuxMembre,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblServicesAuxMembres entity.
     *
     * @param TblServicesAuxMembres $tblServicesAuxMembre The TblServicesAuxMembres entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblServicesAuxMembres $tblServicesAuxMembre)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblservicesauxmembres_delete', ['id' => $tblServicesAuxMembre->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
