<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblTypesElevage;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblTypesElevage controller.
 *
 * @Route("/{_locale}/references/tbltypeselevage")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblTypesElevageController extends AbstractController
{
    /**
     * Deletes a TblTypesElevage entity.
     *
     * @Route("/{id}", name="references_tbltypeselevage_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblTypesElevage $tblTypesElevage)
    {
        $form = $this->createDeleteForm($tblTypesElevage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblTypesElevage);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbltypeselevage_index');
    }

    /**
     * Displays a form to edit an existing TblTypesElevage entity.
     *
     * @Route("/{id}/edit", name="references_tbltypeselevage_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblTypesElevage $tblTypesElevage)
    {
        $deleteForm = $this->createDeleteForm($tblTypesElevage);
        $editForm = $this->createForm('App\Form\TblTypesElevageType', $tblTypesElevage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypesElevage);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypeselevage_index', ['id' => $tblTypesElevage->getId()]);
        }

        return $this->render('tbltypeselevage/edit.html.twig', [
            'tblTypesElevage' => $tblTypesElevage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblTypesElevage entities.
     *
     * @Route("/", name="references_tbltypeselevage_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblTypesElevages = $em->getRepository(TblTypesElevage::class)->findAll();

        return $this->render('tbltypeselevage/index.html.twig', [
            'tblTypesElevages' => $tblTypesElevages,
        ]);
    }

    /**
     * Creates a new TblTypesElevage entity.
     *
     * @Route("/new", name="references_tbltypeselevage_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblTypesElevage = new TblTypesElevage($user);
        $form = $this->createForm('App\Form\TblTypesElevageType', $tblTypesElevage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypesElevage);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypeselevage_index', ['id' => $tblTypesElevage->getId()]);
        }

        return $this->render('tbltypeselevage/new.html.twig', [
            'tblTypesElevage' => $tblTypesElevage,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblTypesElevage entity.
     *
     * @Route("/{id}", name="references_tbltypeselevage_show", methods={"GET"})
     */
    public function showAction(TblTypesElevage $tblTypesElevage)
    {
        $deleteForm = $this->createDeleteForm($tblTypesElevage);

        return $this->render('tbltypeselevage/show.html.twig', [
            'tblTypesElevage' => $tblTypesElevage,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblTypesElevage entity.
     *
     * @param TblTypesElevage $tblTypesElevage The TblTypesElevage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblTypesElevage $tblTypesElevage)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbltypeselevage_delete', ['id' => $tblTypesElevage->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
