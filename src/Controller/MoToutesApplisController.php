<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\MoToutesApplis;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * MoToutesApplis controller.
 *
 * @Route("/{_locale}/references_motoutesapplis")
 */

use Symfony\Component\Routing\Annotation\Route;

final class MoToutesApplisController extends AbstractController
{
    /**
     * Deletes a MoToutesApplis entity.
     *
     * @Route("/{id}", name="references_motoutesapplis_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, MoToutesApplis $moToutesAppli)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];
        $parcelleHistorique_id = $moToutesAppli->getParcelleHistorique()->getId();
        $form = $this->createDeleteForm($moToutesAppli, [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $moToutesAppli->setRadie(TRUE);
            $em->persist($moToutesAppli);
            $em->flush();
        }

        return $this->redirectToRoute('references_parcellehistoriques_show', [
            'id' => $parcelleHistorique_id,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing MoToutesApplis entity.
     *
     * @Route("/{id}/edit", name="references_motoutesapplis_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, MoToutesApplis $moToutesAppli)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];
        $parcelleHistorique_id = $moToutesAppli->getParcelleHistorique()->getId();
        $deleteForm = $this->createDeleteForm($moToutesAppli);
        $editForm = $this->createForm('App\Form\MoToutesApplisType', $moToutesAppli);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($moToutesAppli);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $parcelleHistorique_id,
                //          'coop_id' => $coop_id,
                //          'group_id' => $group_id,
                'exploitation_id' => $exploitation_id,
                'annee' => $annee,
            ]);
        }

        return $this->render('motoutesapplis/edit.html.twig', [
            'moToutesAppli' => $moToutesAppli,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            //          'coop_id' => $coop_id,
            //          'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Lists all MoToutesApplis entities.
     *
     * @Route("/", name="references_motoutesapplis_index", methods={"GET"})
     */
    public function indexAction()
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $em = $this->getDoctrine()->getManager();

        $moToutesApplis = $em->getRepository(MoToutesApplis::class)->findAll();

        return $this->render('motoutesapplis/index.html.twig', [
            'moToutesApplis' => $moToutesApplis,
            //          'coop_id' => $coop_id,
            //          'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Creates a new MoToutesApplis entity.
     *
     * @Route("/new", name="references_motoutesapplis_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $parcelleHistorique_id = $request->query->get('parcelleHistorique_id');
        $traitement = $request->query->get('traitement');
        $user = $this->getUser();
        $moToutesAppli = new MoToutesApplis($user);
        $em = $this->getDoctrine()->getManager();
        $traitement_arr = $em->getRepository(TblTraitements::class)->findByTraitement($traitement);

        if (isset($traitement_arr[0])) {
            $traitement_obj = $traitement_arr[0];
        }

        if (isset($parcelleHistorique_id)) {
            $parcelleHistorique = $em->getRepository(ParcelleHistoriques::class)->find($parcelleHistorique_id);
            $moToutesAppli->setParcelleHistorique($parcelleHistorique);
            $moToutesAppli->setTraitement($traitement_obj);
        }

        $form = $this->createForm('App\Form\MoToutesApplisType', $moToutesAppli);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $moToutesAppli->setRadie(FALSE);
            $em->persist($moToutesAppli);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $parcelleHistorique_id,
                //          'coop_id' => $coop_id,
                //          'group_id' => $group_id,
                'exploitation_id' => $exploitation_id,
                'annee' => $annee,
            ]);
        }

        return $this->render('motoutesapplis/new.html.twig', [
            'moToutesAppli' => $moToutesAppli,
            'form' => $form->createView(),
            //          'coop_id' => $coop_id,
            //          'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Finds and displays a MoToutesApplis entity.
     *
     * @Route("/{id}", name="references_motoutesapplis_show", methods={"GET"})
     */
    public function showAction(MoToutesApplis $moToutesAppli)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $deleteForm = $this->createDeleteForm($moToutesAppli);

        return $this->render('motoutesapplis/show.html.twig', [
            'moToutesAppli' => $moToutesAppli,
            'delete_form' => $deleteForm->createView(),
            //          'coop_id' => $coop_id,
            //          'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Creates a form to delete a MoToutesApplis entity.
     *
     * @param MoToutesApplis $moToutesAppli The MoToutesApplis entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MoToutesApplis $moToutesAppli, $options = [])
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_motoutesapplis_delete', [
                'id' => $moToutesAppli->getId(),
                'coop_id' => $coop_id,
                'group_id' => $group_id,
                'exploitation_id' => $exploitation_id,
                'annee' => $annee,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
