<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblFumureOrg;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblFumureOrg controller.
 *
 * @Route("/{_locale}/references/tblfumureorg")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblFumureOrgController extends AbstractController
{
    /**
     * Deletes a TblFumureOrg entity.
     *
     * @Route("/{id}", name="references_tblfumureorg_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblFumureOrg $tblFumureOrg)
    {
        $form = $this->createDeleteForm($tblFumureOrg);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblFumureOrg);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblfumureorg_index');
    }

    /**
     * Displays a form to edit an existing TblFumureOrg entity.
     *
     * @Route("/{id}/edit", name="references_tblfumureorg_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblFumureOrg $tblFumureOrg)
    {
        $deleteForm = $this->createDeleteForm($tblFumureOrg);
        $editForm = $this->createForm('App\Form\TblFumureOrgType', $tblFumureOrg);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblFumureOrg);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblfumureorg_index', ['id' => $tblFumureOrg->getId()]);
        }

        return $this->render('tblfumureorg/edit.html.twig', [
            'tblFumureOrg' => $tblFumureOrg,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblFumureOrg entities.
     *
     * @Route("/", name="references_tblfumureorg_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblFumureOrgs = $em->getRepository(TblFumureOrg::class)->findAll();

        return $this->render('tblfumureorg/index.html.twig', [
            'tblFumureOrgs' => $tblFumureOrgs,
        ]);
    }

    /**
     * Creates a new TblFumureOrg entity.
     *
     * @Route("/new", name="references_tblfumureorg_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblFumureOrg = new TblFumureOrg($user);
        $form = $this->createForm('App\Form\TblFumureOrgType', $tblFumureOrg);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblFumureOrg);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblfumureorg_index', ['id' => $tblFumureOrg->getId()]);
        }

        return $this->render('tblfumureorg/new.html.twig', [
            'tblFumureOrg' => $tblFumureOrg,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblFumureOrg entity.
     *
     * @Route("/{id}", name="references_tblfumureorg_show", methods={"GET"})
     */
    public function showAction(TblFumureOrg $tblFumureOrg)
    {
        $deleteForm = $this->createDeleteForm($tblFumureOrg);

        return $this->render('tblfumureorg/show.html.twig', [
            'tblFumureOrg' => $tblFumureOrg,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblFumureOrg entity.
     *
     * @param TblFumureOrg $tblFumureOrg The TblFumureOrg entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblFumureOrg $tblFumureOrg)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblfumureorg_delete', ['id' => $tblFumureOrg->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
