<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\SecuriteAlimentaire;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * SecuriteAlimentaire controller.
 *
 * @Route("/{_locale}/references_securitealimentaire")
 */
final class SecuriteAlimentaireController extends AbstractController
{
    /**
     * Deletes a SecuriteAlimentaire entity.
     *
     * @Route("/{id}", name="references_securitealimentaire_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, SecuriteAlimentaire $securiteAlimentaire)
    {
        $annee = $securiteAlimentaire->getAnnee();
        $exploitation = $securiteAlimentaire->getExploitation();

        $form = $this->createDeleteForm($securiteAlimentaire, [
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $securiteAlimentaire->setRadie(true);
            $em->persist($securiteAlimentaire);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'annee' => $annee,
            'id' => $exploitation->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing SecuriteAlimentaire entity.
     *
     * @Route("/{id}/edit", name="references_securitealimentaire_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, SecuriteAlimentaire $securiteAlimentaire)
    {
        $annee = $securiteAlimentaire->getAnnee();
        $exploitation = $securiteAlimentaire->getExploitation();

        $deleteForm = $this->createDeleteForm($securiteAlimentaire);
        $editForm = $this->createForm('App\Form\SecuriteAlimentaireType', $securiteAlimentaire);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($securiteAlimentaire);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('securitealimentaire/edit.html.twig', [
            'securiteAlimentaire' => $securiteAlimentaire,
            'edit_form' => $editForm->createView(),
            'annee' => $annee,
            'exploitation' => $exploitation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new SecuriteAlimentaire entity.
     *
     * @Route("/new", name="references_securitealimentaire_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $exploitation_id = $request->query->get('exploitation_id');
        $annee = $request->query->get('annee');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $securiteAlimentaire = new SecuriteAlimentaire($user);

        if (isset($exploitation_id)) {
            $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
            $securiteAlimentaire->setExploitation($exploitation);
        }

        if (isset($annee)) {
            $securiteAlimentaire->setAnnee($annee);
        }
        $form = $this->createForm('App\Form\SecuriteAlimentaireType', $securiteAlimentaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $securiteAlimentaire->setRadie(false);
            $em->persist($securiteAlimentaire);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation_id,
                'exploitation' => $exploitation,
                'annee' => $annee, ]);
        }

        return $this->render('securitealimentaire/new.html.twig', [
            'securiteAlimentaire' => $securiteAlimentaire,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a SecuriteAlimentaire entity.
     *
     * @Route("/{id}", name="references_securitealimentaire_show", methods={"GET"})
     */
    public function showAction(SecuriteAlimentaire $securiteAlimentaire)
    {
        $annee = $securiteAlimentaire->getAnnee();
        $exploitation = $securiteAlimentaire->getExploitation();

        $deleteForm = $this->createDeleteForm($securiteAlimentaire);

        return $this->render('securitealimentaire/show.html.twig', [
            'securiteAlimentaire' => $securiteAlimentaire,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a SecuriteAlimentaire entity.
     *
     * @param SecuriteAlimentaire $securiteAlimentaire The SecuriteAlimentaire entity
     * @param mixed               $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SecuriteAlimentaire $securiteAlimentaire, $options = [])
    {
        $annee = $securiteAlimentaire->getAnnee();
        $exploitation = $securiteAlimentaire->getExploitation();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_securitealimentaire_delete', [
                'id' => $securiteAlimentaire->getId(),
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee, ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
