<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblEngrais;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblEngrais controller.
 *
 * @Route("/{_locale}/references/tblengrais")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblEngraisController extends AbstractController
{
    /**
     * Deletes a TblEngrais entity.
     *
     * @Route("/{id}", name="references_tblengrais_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblEngrais $tblEngrai)
    {
        $form = $this->createDeleteForm($tblEngrai);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblEngrai);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblengrais_index');
    }

    /**
     * Displays a form to edit an existing TblEngrais entity.
     *
     * @Route("/{id}/edit", name="references_tblengrais_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblEngrais $tblEngrai)
    {
        $deleteForm = $this->createDeleteForm($tblEngrai);
        $editForm = $this->createForm('App\Form\TblEngraisType', $tblEngrai);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblEngrai);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            $this->addFlash('success', 'Element modifié!');

            return $this->redirectToRoute('references_tblengrais_index');
        }

        return $this->render('tblengrais/edit.html.twig', [
            'tblEngrai' => $tblEngrai,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblEngrais entities.
     *
     * @Route("/", name="references_tblengrais_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblEngrais = $em->getRepository(TblEngrais::class)->findAll();

        return $this->render('tblengrais/index.html.twig', [
            'tblEngrais' => $tblEngrais,
        ]);
    }

    /**
     * Creates a new TblEngrais entity.
     *
     * @Route("/new", name="references_tblengrais_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblEngrai = new TblEngrais($user);
        $form = $this->createForm('App\Form\TblEngraisType', $tblEngrai);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblEngrai);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            $this->addFlash('success', 'Element ajouté!');

            return $this->redirectToRoute('references_tblengrais_index');
        }

        return $this->render('tblengrais/new.html.twig', [
            'tblEngrai' => $tblEngrai,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblEngrais entity.
     *
     * @Route("/{id}", name="references_tblengrais_show", methods={"GET"})
     */
    public function showAction(TblEngrais $tblEngrai)
    {
        $deleteForm = $this->createDeleteForm($tblEngrai);

        return $this->render('tblengrais/show.html.twig', [
            'tblEngrai' => $tblEngrai,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblEngrais entity.
     *
     * @param TblEngrais $tblEngrai The TblEngrais entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblEngrais $tblEngrai)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblengrais_delete', ['id' => $tblEngrai->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
