<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019-2020, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\SuiviOrganisations;
use App\Service\SuiviOrganisationsService;
use Doctrine\ORM\PersistentCollection;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\PropertyAccess\PropertyAccess;

/**
 * SuiviOrganisations controller.
 *
 * @Route("/{_locale}/references/suiviorganisations")
 */

use Symfony\Component\Routing\Annotation\Route;

use function count;

final class SuiviOrganisationsController extends AbstractController
{
    private SuiviOrganisationsService $serv;

    public function __construct(SuiviOrganisationsService $serv)
    {
        $this->serv = $serv;
    }

    /**
     * Deletes a SuiviOrganisations entity.
     *
     * @Route("/{id}", name="references_suiviorganisations_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, SuiviOrganisations $suiviOrganisation)
    {
        $org = $suiviOrganisation->getOrganisation();
        $group_id = $org->getId();
        $coop_id = $org->getParent();
        //    $exploitation_id = $_GET['exploitation_id'];
        $annee = $suiviOrganisation->getAnnee();

        $form = $this->createDeleteForm($suiviOrganisation, [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            //        'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $suiviOrganisation->setRadie(TRUE);
            $em->persist($suiviOrganisation);
            $em->flush();
            /* les tables liés */
            $this->radierBranchedeSuivis($suiviOrganisation->getId(), 1, $em);

            /*fin tables liés */
        }

        return $this->redirectToRoute('references_tblorganisations_show', [
            'coop_id' => $coop_id,
            'id' => $group_id,
            //    'suiviOrganisations' => $suiviOrganisation,
            //    'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing SuiviOrganisations entity.
     *
     * @Route("/{id}/edit", name="references_suiviorganisations_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, SuiviOrganisations $suiviOrganisation)
    {
        $em = $this->getDoctrine()->getManager();
        $org = $suiviOrganisation->getOrganisation();

        $deleteForm = $this->createDeleteForm($suiviOrganisation);

        if ($org->getLevel()->getLevel() === 1) {
            $editForm = $this->createForm('App\Form\SuiviOrganisationsType', $suiviOrganisation);
        } else {
            $editForm = $this->createForm('App\Form\SuiviOrganisationsGroupType', $suiviOrganisation);
        }
        $editForm->handleRequest($request);

        /* les tables liés */
        $mobilisations = $em->getRepository(MobilisationsCommunautaires::class)
            ->findBy(['suiviOrganisation' => $suiviOrganisation, 'radie' => 0]);
        /*fin tables liés */

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($suiviOrganisation);
            $em->flush();

            $em->flush();
            $this->addFlash(
                'success',
                'Le suivis a bien été mis à jour'
            );

            return $this->redirectToRoute('references_tblorganisations_show', ['id' => $suiviOrganisation->getOrganisation()->getId()]);
        }

        if ($org->getLevel()->getLevel() === 1) { // for coop
            return $this->render('suiviorganisations/edit.html.twig', [
                'suiviOrganisation' => $suiviOrganisation,
                'edit_form' => $editForm->createView(),
                'delete_form' => $deleteForm->createView(),
            ]);
        }   // for gpm TODO renommer edit_coop et edit_gpment

        return $this->render('suiviorganisations/editgroup.html.twig', [
            'suiviOrganisation' => $suiviOrganisation,
            'mobilisations' => $mobilisations,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new SuiviOrganisations entity.
     *
     * @Route("new", name="references_suiviorganisations_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $org_id = $request->query->get('id'); // todo passer ce param via url
        $em = $this->getDoctrine()->getManager();
        $org = $em->getRepository(TblOrganisations::class)->find($org_id);
        $user = $this->getUser();
        $suiviOrganisation = new SuiviOrganisations($user);
        $suiviOrganisation->setOrganisation($org);

        $form = $this->createForm('App\Form\SuiviOrganisationsBisType', $suiviOrganisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($suiviOrganisation);
            $em->flush();

            return $this->redirectToRoute('references_suiviorganisations_edit', ['id' => $suiviOrganisation->getId()]);
        }

        return $this->render('suiviorganisations/new.html.twig', [
            'suiviOrganisation' => $suiviOrganisation,
            'form' => $form->createView(),
        ]);
    }

    public function radierBranchedeSuivis($suiviId, $status, $em)
    {
        $mobilisations = $em->getRepository(MobilisationsCommunautaires::class)
            ->updateRadieStatus($suiviId, $status);
    }

    /**
     * TODO a terme : supprimer quand pour ok pour groupement
     * Finds and displays a SuiviOrganisations entity.
     *
     * @Route("/{id}", name="references_suiviorganisations_show", methods={"GET"})
     */
    public function showAction(SuiviOrganisations $suiviOrganisation)
    {
        $deleteForm = $this->createDeleteForm($suiviOrganisation);

        return $this->render(
            'suiviorganisations/show.html.twig',
            [
                'suiviOrganisation' => $suiviOrganisation,
                'delete_form' => $deleteForm->createView(),
            ]
        );
    }

    /**
     * Return the suivi as an excell.
     *
     * @Route("/{id}/export/single", name="references_suiviorganisations_export", methods={"GET", "POST"})
     */
    public function singleExport(SuiviOrganisations $suiviOrganisation)
    {
        $config = [
            ['Coopérative', 'organisation.denomination'],
            ['Coopérative id', 'organisation.id'],
            ['Année du suivi', 'annee'],
            ['Existence RIO', 'existenceROI', 1],
            ['Existence de status', 'existenceStatus', 1],
            ['Existence CE', 'existenceCE'],
            ['Existence CS', 'existenceCS'],
            ['niveauxReconnaissance', 'niveauxReconnaissance', 1],
            ['nbrReunionsAG', 'nbrReunionsAG', 2],
            ['nbrReunionsCE', 'nbrReunionsCE', 2],
            ['nbrReunionsCS', 'nbrReunionsCS', 2],
            ['alternanceCSNbr', 'alternanceCSNbr'],
            ['alternanceCENbr', 'alternanceCENbr', 2],
            ['planStrategiquePluriannuel', 'planStrategiquePluriannuel', 4],
            ['rapportSuiviPlanStrategique', 'rapportSuiviPlanStrategique', 4],
            ['evolutionNbrMembres', 'evolutionNbrMembres', 5],
            ['nombreTotalMembres', 'nombreTotalMembres', 5],
            ['classement', 'classement'],
            ['majListeDeMembres', 'majListeDeMembres', 2],
            ['nbrPVManquantsAG', 'nbrPVManquantsAG', 2],
            ['nbrPVManquantsCE', 'nbrPVManquantsCE', 2],
            ['nbrPVManquantsCS', 'nbrPVManquantsCS'],
            ['planActionAnnuel', 'planActionAnnuel', 4],
            ['rapportAnnuelActivites', 'rapportAnnuelActivites', 4],
            ['comptabilite', 'comptabilite', 3],
            ['budget', 'budget', 4],
            ['chiffreAffaireParMembre', 'chiffreAffaireParMembre'],
            ['chiffreDAffaires', 'chiffreDAffaires', 5],
            ['degrePriseEnChargeAG', 'degrePriseEnChargeAG', 6],
            ['degrePriseEnChargeCE', 'degrePriseEnChargeCE'],
            ['degrePriseEnChargeCS', 'degrePriseEnChargeCS'],
            ['compteBancaire', 'compteBancaire', 3],
            ['soldeAnneeAvant', 'soldeAnneeAvant'],
            ['signaturesPourSortieFonds', 'signaturesPourSortieFonds', 3],
            ['enregistrementCotisations', 'enregistrementCotisations'],
            ['livreCaisse', 'livreCaisse', 3],
            ['carnetRecus', 'carnetRecus', 3],
            ['livretSocietaire', 'livretSocietaire', 3],
            ['classementJustificatifs', 'classementJustificatifs', 3],
            ['institutionBancaire', 'institutionBancaire'],
            ['rapportsFinanciereAnnuel', 'rapportsFinanciereAnnuel', 3],
            ['auditAnnuelDesComptes', 'auditAnnuelDesComptes', 3],
            ['nombreMembresPayantsCotisation', 'nombreMembresPayantsCotisation', 5],
            ['ristourneParMembre', 'ristourneParMembre', 5],
            ['montantTotalCotisations', 'montantTotalCotisations', 6],
            ['montantServicesPayesParMembres', 'montantServicesPayesParMembres', 6],
            ['montantServicesPayesParTiers', 'montantServicesPayesParTiers', 6],
            ['servicesAuxMembres1', 'servicesAuxMembres1'],
            ['servicesAuxMembres2', 'servicesAuxMembres2'],
            ['servicesAuxMembres3', 'servicesAuxMembres3'],
            ['servicesPayesParMembres1', 'servicesPayesParMembres1'],
            ['servicesPayesParMembres2', 'servicesPayesParMembres2'],
            ['servicesPayesParMembres3', 'servicesPayesParMembres3'],
            ['servicesPayesParTiers1', 'servicesPayesParTiers1'],
            ['servicesPayesParTiers2', 'servicesPayesParTiers2'],
            ['servicesPayesParTiers3', 'servicesPayesParTiers3'],
            ['nbrMUSO', 'nbrMUSO'],
            ['montantEpargneMUSO', 'montantEpargneMUSO'],
            ['affiliationFederation', 'affiliationFederation', 7],
            ['nbrContratsAvecActeursEco', 'nbrContratsAvecActeursEco', 7],
            ['nbrCreditAuxMembresParIMFOuBanque', 'nbrCreditAuxMembresParIMFOuBanque'],
            ['membresContracteCredit', 'membresContracteCredit'],
            ['membresCreditMontant', 'membresCreditMontant'],
            ['institutionCreditMembres', 'institutionCreditMembres'],
            ['nbrCreditAvecIMFOuBanque', 'nbrCreditAvecIMFOuBanque', 7],
            ['creditPourOrganisationMontant', 'creditPourOrganisationMontant'],
            ['institutionFinancementCreditOrg', 'institutionFinancementCreditOrg'],
            ['infrastructures', 'infrastructures'],
            ['uniteDeTransformation', 'uniteDeTransformation'],
            ['materielRoulant', 'materielRoulant'],
            ['materielDeBureau', 'materielDeBureau'],
            ['equipementAgricole', 'equipementAgricole'],
            ['nbrPersonnelAdmin', 'nbrPersonnelAdmin'],
            ['nbrPersonnelTechnique', 'nbrPersonnelTechnique'],
            ['nbrPersonnelAppui', 'nbrPersonnelAppui'],
            ['besoinsFormation', 'besoinsFormation'],
            ['besoinsServicesAuxMembres', 'besoinsServicesAuxMembres'],
        ];

        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();

        $title = sprintf('Suivi %d $s', $suiviOrganisation->getAnnee(), $suiviOrganisation->getOrganisation()->getDenomination());
        $titlle = mb_substr($title, 0, 31); // max 31 chars for title
        $sheet->setTitle($title);
        $propertyAccessor = PropertyAccess::createPropertyAccessor();

        $spreadsheet->getActiveSheet()->setCellValue('A1', 'Champ');
        $spreadsheet->getActiveSheet()->setCellValue('B1', 'Valeur');
        $spreadsheet->getActiveSheet()->setCellValue('C1', 'Score');
        $spreadsheet->getActiveSheet()->setCellValue('D1', 'Degré formalisation');
        $spreadsheet->getActiveSheet()->setCellValue('E1', 'Qualité de la gouvernance');
        $spreadsheet->getActiveSheet()->setCellValue('F1', 'Gestion financière');
        $spreadsheet->getActiveSheet()->setCellValue('G1', 'Gestion opérationelle des activités');
        $spreadsheet->getActiveSheet()->setCellValue('H1', 'Portée des activités');
        $spreadsheet->getActiveSheet()->setCellValue('I1', 'Degré d\'autonomie');
        $spreadsheet->getActiveSheet()->setCellValue('J1', 'Relations externes');

        $lineNb = 2;

        foreach ($config as $value) {
            $dataName = $value[0];
            $dataValue = $propertyAccessor->getValue($suiviOrganisation, $value[1]);

            if ($dataValue instanceof PersistentCollection) {
                if (count($dataValue) === 0) {
                    $dataValue = '';
                } else {
                    $dataValueStr = '';

                    foreach ($dataValue as $elem) {
                        $dataValueStr = '' === $dataValueStr ? '' . ($elem) : $dataValueStr . ', ' . ($elem);
                    }
                    $dataValue = $dataValueStr;
                }
            } elseif (0 !== ('' === $dataValue | null === $dataValue)) {
                $dataValue = '-- No data --';
            }

            $score = $this->serv->getScore($suiviOrganisation, $value[1]);

            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(1, $lineNb, $dataName);
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(2, $lineNb, $dataValue);
            $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(3, $lineNb, $score);

            if (count($value) > 2 && null !== $value[2]) {
                $spreadsheet->getActiveSheet()->setCellValueByColumnAndRow(3 + $value[2], $lineNb, $score);
            }

            ++$lineNb;
        }

        $spreadsheet->getActiveSheet()->getColumnDimension('A')->setWidth(30);
        $spreadsheet->getActiveSheet()->getColumnDimension('B')->setWidth(20);
        $spreadsheet->getActiveSheet()->getColumnDimension('C')->setWidth(20);

        // Create your Office 2007 Excel (XLSX Format)
        $writer = new Xlsx($spreadsheet);

        // Create a Temporary file in the system
        $fileName = sprintf('export_coop_%s_%d.xlsx', $suiviOrganisation->getOrganisation()->getDenomination(), $suiviOrganisation->getAnnee());
        $temp_file = tempnam(sys_get_temp_dir(), $fileName);

        // Create the excel file in the tmp directory of the system
        $writer->save($temp_file);

        return $this->file($temp_file, $fileName, ResponseHeaderBag::DISPOSITION_INLINE);
    }

    /**
     * Creates a form to delete a SuiviOrganisations entity.
     *
     * @param SuiviOrganisations $suiviOrganisation The SuiviOrganisations entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SuiviOrganisations $suiviOrganisation, $options = [])
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'references_suiviorganisations_delete',
                    [
                        'id' => $suiviOrganisation->getId(),
                    ]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }
}
