<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\AnimauxLabour;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * AnimauxLabour controller.
 *
 * @Route("/{_locale}/references/animauxlabour")
 */

use Symfony\Component\Routing\Annotation\Route;

final class AnimauxLabourController extends AbstractController
{
    /**
     * Deletes a AnimauxLabour entity.
     *
     * @Route("/{id}", name="references_animauxlabour_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, AnimauxLabour $animauxLabour)
    {
        $form = $this->createDeleteForm($animauxLabour);
        $form->handleRequest($request);

        $troupeau = $animauxLabour->getAnimaux();

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $animauxLabour->setRadie(TRUE);
            $em->persist($animauxLabour);
            $em->flush();

            return $this->redirectToRoute('references_animaux_show', ['id' => $troupeau->getId()]);
        }
    }

    /**
     * Displays a form to edit an existing AnimauxLabour entity.
     *
     * @Route("/{id}/edit", name="references_animauxlabour_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, AnimauxLabour $animauxLabour)
    {
        $animaux = $animauxLabour->getAnimaux();
        $annee = $animauxLabour->getAnnee();
        $exploitation = $animaux->getExploitation();
        $exploitation_id = $animaux->getExploitation()->getId();
        $deleteForm = $this->createDeleteForm($animauxLabour);
        $editForm = $this->createForm('App\Form\AnimauxLabourType', $animauxLabour);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($animauxLabour);
            $em->flush();

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux->getId(),
                'annee' => $annee,
                'exploitation_id' => $exploitation_id,
                'exploitation' => $exploitation,
            ]);
        }

        return $this->render('animauxlabour/edit.html.twig', [
            'id' => $animaux->getId(),
            'animauxLabour' => $animauxLabour,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'annee' => $annee,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Creates a new AnimauxLabour entity.
     *
     * @Route("/new/{animaux}/", name="references_animauxlabour_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, \App\Entity\Animaux $animaux)
    {
        $animaux_id = $animaux->getId();
        //    $animaux_id = $request->query->get('animaux');
        $annee = $animaux->getAnnee();
        //    $annee = $request->query->get('annee');
        //$labourlabel = $request->query->get('labourlabel');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $animauxLabour = new AnimauxLabour($user);
        //    $animaux = $em->getRepository(Animaux::class)->find($animaux_id);
        $exploitation = $animaux->getExploitation();
        //    $labours = $em->getRepository(TblLabourAnimaux')->findOneBy(array('labour::class=>$labourlabel));
        $animauxLabour->setAnimaux($animaux);
        $animauxLabour->setAnnee($annee);
        //    $animauxLabour->setLabour($labours);
        $form = $this->createForm('App\Form\AnimauxLabourType', $animauxLabour);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            /*    $animauxLabour->setAnimaux($animaux);
                $animauxLabour->setAnnee($annee);
                $animauxLabour->setLabour($labours);
             */ $animauxLabour->setRadie(FALSE);
            $em->persist($animauxLabour);
            $em->flush();

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux_id,
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('animauxlabour/new.html.twig', [
            'animauxLabour' => $animauxLabour,
            'id' => $animaux->getId(),
            'annee' => $annee,
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a AnimauxLabour entity.
     *
     * @Route("/{id}", name="references_animauxlabour_show", methods={"GET"})
     */
    public function showAction(AnimauxLabour $animauxLabour)
    {
        $deleteForm = $this->createDeleteForm($animauxLabour);

        return $this->render('animauxlabour/show.html.twig', [
            'animauxLabour' => $animauxLabour,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a AnimauxLabour entity.
     *
     * @param AnimauxLabour $animauxLabour The AnimauxLabour entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AnimauxLabour $animauxLabour)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_animauxlabour_delete', ['id' => $animauxLabour->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
