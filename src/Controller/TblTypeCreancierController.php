<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblTypeCreancier;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblTypeCreancier controller.
 *
 * @Route("/{_locale}/references/tbltypecreancier")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblTypeCreancierController extends AbstractController
{
    /**
     * Deletes a TblTypeCreancier entity.
     *
     * @Route("/{id}", name="references_tbltypecreancier_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblTypeCreancier $tblTypeCreancier)
    {
        $form = $this->createDeleteForm($tblTypeCreancier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblTypeCreancier);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbltypecreancier_index');
    }

    /**
     * Displays a form to edit an existing TblTypeCreancier entity.
     *
     * @Route("/{id}/edit", name="references_tbltypecreancier_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblTypeCreancier $tblTypeCreancier)
    {
        $deleteForm = $this->createDeleteForm($tblTypeCreancier);
        $editForm = $this->createForm('App\Form\TblTypeCreancierType', $tblTypeCreancier);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeCreancier);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypecreancier_index', ['id' => $tblTypeCreancier->getId()]);
        }

        return $this->render('tbltypecreancier/edit.html.twig', [
            'tblTypeCreancier' => $tblTypeCreancier,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblTypeCreancier entities.
     *
     * @Route("/", name="references_tbltypecreancier_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblTypeCreanciers = $em->getRepository(TblTypeCreancier::class)->findAll();

        return $this->render('tbltypecreancier/index.html.twig', [
            'tblTypeCreanciers' => $tblTypeCreanciers,
        ]);
    }

    /**
     * Creates a new TblTypeCreancier entity.
     *
     * @Route("/new", name="references_tbltypecreancier_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblTypeCreancier = new TblTypeCreancier($user);
        $form = $this->createForm('App\Form\TblTypeCreancierType', $tblTypeCreancier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeCreancier);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypecreancier_index', ['id' => $tblTypeCreancier->getId()]);
        }

        return $this->render('tbltypecreancier/new.html.twig', [
            'tblTypeCreancier' => $tblTypeCreancier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblTypeCreancier entity.
     *
     * @Route("/{id}", name="references_tbltypecreancier_show", methods={"GET"})
     */
    public function showAction(TblTypeCreancier $tblTypeCreancier)
    {
        $deleteForm = $this->createDeleteForm($tblTypeCreancier);

        return $this->render('tbltypecreancier/show.html.twig', [
            'tblTypeCreancier' => $tblTypeCreancier,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblTypeCreancier entity.
     *
     * @param TblTypeCreancier $tblTypeCreancier The TblTypeCreancier entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblTypeCreancier $tblTypeCreancier)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbltypecreancier_delete', ['id' => $tblTypeCreancier->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
