<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblFilieres;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblFilieres controller.
 *
 * @Route("/{_locale}/references/tblfilieres")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblFilieresController extends AbstractController
{
    /**
     * Deletes a TblFilieres entity.
     *
     * @Route("/{id}", name="references_tblfilieres_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblFilieres $tblFiliere)
    {
        $form = $this->createDeleteForm($tblFiliere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblFiliere);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblfilieres_index');
    }

    /**
     * Displays a form to edit an existing TblFilieres entity.
     *
     * @Route("/{id}/edit", name="references_tblfilieres_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblFilieres $tblFiliere)
    {
        $deleteForm = $this->createDeleteForm($tblFiliere);
        $editForm = $this->createForm('App\Form\TblFilieresType', $tblFiliere);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblFiliere);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblfilieres_index', ['id' => $tblFiliere->getId()]);
        }

        return $this->render('tblfilieres/edit.html.twig', [
            'tblFiliere' => $tblFiliere,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblFilieres entities.
     *
     * @Route("/", name="references_tblfilieres_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblFilieres = $em->getRepository(TblFilieres::class)->findAll();

        return $this->render('tblfilieres/index.html.twig', [
            'tblFilieres' => $tblFilieres,
        ]);
    }

    /**
     * Creates a new TblFilieres entity.
     *
     * @Route("/new", name="references_tblfilieres_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblFiliere = new TblFilieres($user);
        $form = $this->createForm('App\Form\TblFilieresType', $tblFiliere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblFiliere);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblfilieres_index', ['id' => $tblFiliere->getId()]);
        }

        return $this->render('tblfilieres/new.html.twig', [
            'tblFiliere' => $tblFiliere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblFilieres entity.
     *
     * @Route("/{id}", name="references_tblfilieres_show", methods={"GET"})
     */
    public function showAction(TblFilieres $tblFiliere)
    {
        $deleteForm = $this->createDeleteForm($tblFiliere);

        return $this->render('tblfilieres/show.html.twig', [
            'tblFiliere' => $tblFiliere,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblFilieres entity.
     *
     * @param TblFilieres $tblFiliere The TblFilieres entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblFilieres $tblFiliere)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblfilieres_delete', ['id' => $tblFiliere->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
