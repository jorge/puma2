<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Utils\Path;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Process\Process;

set_time_limit(1200);

/**
 * Class UpdateController.
 *
 * @Route("/{_locale}")
 */
use Symfony\Component\Routing\Annotation\Route;

final class UpdateController extends AbstractController
{
    private $continue = true;

    private $params;

    public function confirm()
    {
        // TODO
    }

    /**
     * @Route("/db/reinit/", name="db_reinit")
     *
     * Re-init the database
     */
    public function databaseReInit()
    {
        $this->startPage();
        $this->title('Mise à jour de la base de donnée de PUMA. Ne pas quitter la page avant la fin de la mise à jour');

        echo '> Récupération de la db';

        $start = time();
        $downloadSrc = 'http://151.80.144.77:8787/last.sql.gz';
        $downloadSrcFile = fopen($downloadSrc, 'rb');
        $downloadDest = Path::getPath4TmpFile('last_puma_db.sql.gz');
        $downloadDestFile = fopen($downloadDest, 'w+b');

        $download = stream_copy_to_stream($downloadSrcFile, $downloadDestFile);

        fclose($downloadSrcFile);
        fclose($downloadDestFile);

        if ($download) {
            echo ' (download : ok)';
        } else {
            echo ' (download : nok)';
            $this->continue = false;
        }
        flush();

        if ($this->continue) {
            $unzipSrc = $downloadDest;
            $unzipSrcFile = gzopen($unzipSrc, 'rb');
            $unzipDest = Path::getPath4TmpFile('puma_db.sql');

            $unzipDestFile = fopen($unzipDest, 'wb');
            $unzip = stream_copy_to_stream($unzipSrcFile, $unzipDestFile);

            if ($unzip) {
                echo ' (unzip : ok)';
            } else {
                echo ' (unzip : nok)';
                $this->continue = false;
            }
            flush();
        }

        echo '<br>';

        gzclose($unzipSrcFile);
        fclose($unzipDestFile);

        $this->execSql('Suppression de l\'ancienne db', 'DROP DATABASE ' . $this->params['dbname'] . ';');

        $this->execSql(
            'Création de la nouvelle db',
            'CREATE DATABASE IF NOT EXISTS ' . $this->params['dbname'] . ' DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;',
            false
        );

        $this->execSql(
            'Ajout des données dans la db (cette étape peut prendre plusieurs minutes)',
            'source ' . $unzipDest . ';'
        );

        return $this->endPage();
    }

    /**
     * @Route("/app/update/", name="app_update")
     *
     * Update the code source of the application
     */
    public function updateApplication()
    {
        $this->startPage();
        $this->title("Mise à jour du code de l'application");

        $this->execCmd('git remote', 'git remote -v');
        $this->execCmd('git brahcn', 'git branch');

        $this->execCmdd('pwd');

        $this->execCmd('gitpull', 'git pull origin prod');
        $this->execCmd('db migrate', ['../app/console doctrine:migrations:migrate --no-interaction']);
        $this->execCmd('sendback info', ['../app/console doctrine:migrations:migrate puma:send-debug-info']);

        return $this->endPage();
    }

    public function updateComposerAndCode()
    {
        // TODO
    }

    private function endPage()
    {
        echo '</body>';
        echo '</html>';

        return new Response('');
    }

    /**
     * if $cmd is a string this is a command in the bach
     * if $cmd is an array it is a symfony command.
     *
     * @param mixed $name
     * @param mixed $cmd
     * @param mixed $oKMessage
     * @param mixed $failMessage
     */
    private function execCmd($name, $cmd, $oKMessage = 'terminé', $failMessage = 'erreur')
    {
        if ($this->continue) {
            echo '> ' . $name;

            $process = new Process($cmd);
            $process->setTimeout(600);
            $process->start();
            $process->wait();

            if ($process->isSuccessful()) {
                echo 'Ok';
                echo '<br>';
                echo $process->getOutput();
            // toto print(' : '.$okMessage.'<br>');
            } else {
                $this->continue = false;
                echo ' : ' . $failMessage . '<br>';
                echo '<br>';
                echo $process->getErrorOutput();
            }
        }
    }

    private function execSql($name, $sqlCmd, $dbSelection = true, $okMessage = 'terminé', $failMessage = 'erreur')
    {
        $cmd = 'mysql -h ' . $this->params['host'] . ' -u ' . $this->params['user'] . ' -p' . $this->params['password'] . ' -e "' . $sqlCmd . '"';
        $this->execCmd($name, $cmd, $okMessage, $failMessage);
    }

    private function init()
    {
        $em = $this->getDoctrine()->getManager();
        $conn = $em->getConnection();
        $this->params = $conn->getParams();
    }

    private function startPage()
    {
        $this->init();

        echo '<!DOCTYPE html>';
        echo '<html>';
        echo '<body style="background:black; color:white; font-size: 0.9em;">';
    }

    private function title($title)
    {
        echo '> ' . $title . '<br>';
        echo '> ----------------------------------------------------------------------------------------------<br>';
        echo '> <br>';
    }
}
