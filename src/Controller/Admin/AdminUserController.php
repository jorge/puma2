<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\Collecte;
use App\Entity\Saisies\ActivityUser;
use App\Entity\User;
use DateInterval;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

use function count;

final class AdminUserController extends AbstractController
{
    /**
     * @Route("/{_locale}/admin/user/{user}/activities", name="admin_user_activity")
     * @Route("/{_locale}/admin/users/activities", name="admin_users_activity")
     */
    public function activitiesForUserAction(Request $request, PaginatorInterface $paginator, ?User $user = null)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à ce page');

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $entityForm = new ActivityUser();

        if (null !== $user) {
            $entityForm->addUser($user);
        }

        $form = $this->createForm('App\Form\Saisies\ActivityUserType', $entityForm);
        $form->handleRequest($request);

        $qb = $qb
            ->select(['c'])
            ->from(Collecte::class, 'c');

        $params = [];

        if (count($entityForm->getUsers()) >= 1) {
            $qb = $qb->where('c.creator IN (:users) OR c.lastEditor IN (:users)');
            $params['users'] = $entityForm->getUsers();
        } else {
            $qb = $qb
                ->where('c.creator is not null OR c.lastEditor is not null');
        }

        if ($entityForm->getDateFrom() !== null) {
            $qb = $qb->andWhere('c.udate >= :from');
            $params['from'] = $entityForm->getDateFrom();
        }

        if ($entityForm->getDateTo() !== null) {
            $qb = $qb->andWhere('c.udate <= :to');
            $params['to'] = $entityForm->getDateTo();
            $params['to']->add(new DateInterval('P1D')); // hour=0 and minutes=0 -> until next day
        }

        $qb = $qb->orderBy('c.udate', 'DESC');

        foreach ($params as $key => $value) {
            $qb = $qb->setParameter($key, $value);
        }

        $q = $qb->getQuery();
        $result = $q->getResult();

        $paginated_res = $paginator->paginate(
            $result,
            $request->query->get('page', 1),
            10000
        );

        return $this->render('admin/users/activity.html.twig', [
            'collectes' => $paginated_res,
            'user' => $user,
            'form_date_range' => $form->createView(),
            'pagination' => $paginator,
        ]);
    }

    /**
     * changer le mot de passe d'un utilisateur existant.
     *
     * @Route("/{_locale}/admin/changepw/{id}", name="admin_changepw")
     */
    public function changepwAction(Request $request, User $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à ce page');

        $em = $this->getDoctrine()->getManager();

        $form = $this->createForm('App\Form\ChangePasswordFormType', $id);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $userManager = $this->container->get('fos_user.user_manager');
            // $userManager->updatePassword($id);

            $em->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/users/edit.html.twig', [
            'edit_form' => $form->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing Users.
     *
     * @Route("/{_locale}/admin/{id}/edit", name="admin_users_edit")
     */
    public function editAction(Request $request, User $id)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à cette page');

        $editForm = $this->createForm('App\Form\UserType', $id);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($id);
            $em->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/users/edit.html.twig', [
            'edit_form' => $editForm->createView(),
        ]);
    }

    /**
     * activer ou desactiver un utilisateur existante.
     *
     * @Route("/{_locale}/admin/enable/{id}/{enable}", name="admin_enable")
     *
     * @param mixed $enable
     */
    public function enableAction(Request $request, User $id, $enable = true)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à ce page');
        $em = $this->container->get('doctrine.orm.entity_manager');

        $userManager = $this->container->get('fos_user.user_manager');
        $user = $userManager->findUserBy(['id' => $id]);

        $user->setEnabled($enable);
        $em->persist($user);
        $em->flush();

        return $this->redirectToRoute('admin_users');
    }

    /**
     * Displays a form to edit an existing Users.
     *
     * @Route("/{_locale}/admin/new", name="admin_users_new")
     */
    public function newAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à cette page');

        $em = $this->getDoctrine()->getManager();

        $user = new \App\Entity\User();
        $form = $this->createForm('App\Form\RegistrationUserType', $user);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $user->setEnabled(true);
            $em->persist($user);
            $em->flush();

            return $this->redirectToRoute('admin_users');
        }

        return $this->render('admin/users/new.html.twig', [
            'user' => $user,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/admin/users", name="admin_users")
     */
    public function usersAction()
    {
        $entityManager = $this->getDoctrine()->getManager();
        $users = $entityManager->getRepository(User::class)->findAll();

        return $this->render('admin/users/index.html.twig', [
            'users' => $users,
        ]);
    }
}
