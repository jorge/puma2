<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

final class AdminBaseController extends AbstractController
{
    /**
     * @Route("/{_locale}/admin/", name="admin_base")
     */
    public function indexAction()
    {
        return $this->render('admin/base/index.html.twig');
    }

    /**
     * @Route("/{_locale}/admin/references", name="admin_references")
     */
    public function referenceAction()
    {
        return $this->render('admin/base/references.html.twig', [
            'listes' => [
                [
                    'titre' => 'Signaletique exploitation',
                    'classes' => [
                        ['titre' => 'Etat Civil', 'route' => 'references_tbletatcivil_index'],
                        ['titre' => 'Localisation -> tbl_decoupage_admin', 'route' => 'references_tbldecoupageadmin_choice_level'],
                        ['titre' => 'Appartenance -> Coopératives', 'route' => 'references_tblorganisations_index'],
                    ],
                ],
                [
                    'titre' => 'F0',
                    'classes' => [
                        ['titre' => 'Contraints', 'route' => 'references_tblcontraints_index'],
                        ['titre' => 'Co-exploitant details -> Domaines études superieures', 'route' => 'references_tbldomainesetudessuperieures_index'],
                        ['titre' => 'Co-exploitant details -> Professions', 'route' => 'references_tblprofessions_index'],
                        ['titre' => '0611, 0612 -> tbl_type_depense', 'route' => 'references_tbltypedepense_index'],
                        ['titre' => '0651, 0652 -> tbl_type_creancier', 'route' => 'references_tbltypecreancier_index'],
                        ['titre' => '0638 -> tbl_organisateurs', 'route' => 'references_tblorganisateurs_index'],
                        ['titre' => '0638 -> tbl_utilites', 'route' => 'references_tblutilites_index'],
                        ['titre' => '0638 -> tbl_themes_formations', 'route' => 'references_tblthemesformations_index'],
                        ['titre' => '090 -> tbl_contraints', 'route' => 'references_tblcontraints_index'],
                        ['titre' => '9639 -> tbl_services', 'route' => 'references_tblservices_index'],
                        ['titre' => '81 -> tbl_type_commercialisation', 'route' => 'references_tbltypecommercialisation_index'],
                        ['titre' => '81 -> tbl_produits', 'route' => 'references_tblproduits_index'],
                    ],
                ],
                [
                    'titre' => 'F1',
                    'classes' => [
                        ['titre' => 'Infrastructures', 'route' => 'references_tblinfrastructures_index'],
                        ['titre' => 'Moyens de transport', 'route' => 'references_tblmoyentransport_index'],
                        ['titre' => 'Outillages agricoles', 'route' => 'references_tblequipementsagricoles_index'],
                    ],
                ],
                [
                    'titre' => 'F2',
                    'classes' => [
                        ['titre' => 'Parcelle -> traitement antierosif', 'route' => 'references_tblantierosives_index'],
                        ['titre' => 'Parcelle -> exposition', 'route' => 'references_tblexposition_index'],
                        ['titre' => 'Parcelle -> mode culture', 'route' => 'references_tblmodeculture_index'],
                        ['titre' => 'Parcelle -> pente', 'route' => 'references_tblpente_index'],
                        ['titre' => 'Parcelle -> propriete', 'route' => 'references_tblpropriete_index'],
                        ['titre' => 'Parcelle -> situation', 'route' => 'references_tblsituation_index'],
                        ['titre' => 'Calendrier -> opérations culturales', 'route' => 'references_tbloperationculturaletype_index'],
                        ['titre' => 'Calendrier -> opérations culturales catégories', 'route' => 'tbloperationculturaletypecategory_index'],
                        ['titre' => 'Calendrier -> opérations culturales : intrants', 'route' => 'tbloperationculturaleintrant_index'],
                        ['titre' => 'Calendrier -> aléas', 'route' => 'references_tblaleas_index'],
                        ['titre' => 'Calendrier -> analyse de sol', 'route' => 'references_tblanalysesol_index'],
                        ['titre' => 'Produits', 'route' => 'references_tblproduits_index'],
                        ['titre' => 'Cultures', 'route' => 'references_tblcultures_index'],
                        ['titre' => 'tbl_unite_produits', 'route' => 'references_tbluniteproduits_index'],
                    ],
                ],
                [
                    'titre' => 'F3',
                    'classes' => [
                        ['titre' => 'Espece -> tbl_cultures', 'route' => 'references_tblcultures_index'],
                        ['titre' => 'Type elèvage -> tbl_types_elevage', 'route' => 'references_tbltypeselevage_index'],
                        ['titre' => 'Table Animaux_labours: labour->tbl_labour_animaux', 'route' => 'references_tbllabouranimaux_index'],
                        ['titre' => 'Alimentation du bétail', 'route' => 'references_tblalimentationbetail_index'],
                        ['titre' => 'Produits -> tbl_produits', 'route' => 'references_tblproduits_index'],
                        ['titre' => 'Table animaux_production: production->tbl_animaux_production', 'route' => 'references_tblanimauxProduction_index'],
                        ['titre' => 'Table animaux structure => tbl_animaux_mouvements', 'route' => 'references_tblanimauxmouvements_index'],
                    ],
                ],
                [
                    'titre' => 'F4',
                    'classes' => [
                        ['titre' => 'tbl_type_garantie', 'route' => 'references_tbltypegarantie_index'],
                        ['titre' => 'tbl_creditobjet', 'route' => 'references_creditobjet_index'],
                    ],
                ],
                [
                    'titre' => 'F5',
                    'classes' => [
                        ['titre' => 'Achats agricoles (catégories pour ExploitationCharges)', 'route' => 'references_tblachatagricoles_index'],
                    ],
                ],
                [
                    'titre' => 'F6',
                    'classes' => [
                        ['titre' => 'tbl_produit_phyto', 'route' => 'references_tblproduitphyto_index'],
                        ['titre' => 'Variété des semences', 'route' => 'references_tblvarietesemences_index'],
                        ['titre' => 'Alimentation du bétail', 'route' => 'references_tblalimentationbetail_index'],
                        ['titre' => 'Commandes semences -> tbl_semences', 'route' => 'references_tblsemences_index'],
                        ['titre' => 'Commandes semences -> tbl_unite_produits', 'route' => 'references_tbluniteproduits_index'],
                        ['titre' => 'Plants agroforestiers et fuitiers', 'route' => 'references_tblagrofofruitiers_index'],
                    ],
                ],
                [
                    'titre' => 'Suivi_organisations',
                    'classes' => [
                        ['titre' => 'Choix génériques', 'route' => 'references_tblvaleurchoixsimple_index'],
                        ['titre' => 'Institutions financieres', 'route' => 'references_tblinstitutionsfinancieres_index'],
                        ['titre' => 'Services aux membres', 'route' => 'references_tblservicesauxmembres_index'],
                        ['titre' => 'Themes de formation', 'route' => 'references_tblthemesformations_index'],
                        ['titre' => 'Equipements agricoles', 'route' => 'references_tblequipementsagricoles_index'],
                        ['titre' => 'Infrastructures', 'route' => 'references_tblinfrastructures_index'],
                        ['titre' => 'Materiel de bureau', 'route' => 'references_tblmaterielsbureaux_index'],
                        ['titre' => 'Moyens de transport', 'route' => 'references_tblmoyentransport_index'],
                        ['titre' => 'Moyens de transformations -> tbl_unites_transformations', 'route' => 'references_tblunitestransformations_index'],
                        ['titre' => 'Activités communautaires', 'route' => 'references_tblactivitescommunautaires_index'],
                        ['titre' => 'Niveau de reconnaissance', 'route' => 'references_tblniveauxreconnaissance_index'],
                        ['titre' => 'Prestations de services', 'route' => 'references_tblprestationsservices_index'],
                        ['titre' => 'Services payants aux membres', 'route' => 'references_tblservicespayants_index'],
                    ],
                ],
            ],
        ]);
    }
}
