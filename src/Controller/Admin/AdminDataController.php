<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Admin;

use App\Entity\Saisies\Organisations;
use App\Entity\TblOrganisations;
use App\Utils\ZipFile;
use chillerlan\QRCode\QRCode;
use chillerlan\QRCode\QROptions;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function count;

// ...

final class AdminDataController extends AbstractController
{
    public function _getMembresSansCni($org_id, $orgType)
    {
        if ('coop' === $orgType) {
            $where = " AND tc.id ='" . $org_id . "'";
            $subtitre = 'la coopérative';
        } else {
            $where = " AND t.id ='" . $org_id . "'";
            $subtitre = 'le groupement';
        }
        $fields = ['NumeroMembre', 'Cni', 'Nom', 'Prenom', 'Cooperative', 'Groupement', 'Photo', 'qrimage'];
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery();
        $baseQuery = 'SELECT ph.* FROM';
        $baseQuery .= ' (SELECT e.id, e.utilisateur,';
        $baseQuery .= ' e.numero_membre as NumeroMembre, e.qrimage as qrimage,';
        $baseQuery .= " (SELECT MAX(pch.prenom) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Prenom,";
        $baseQuery .= " (SELECT MAX(pch.nom) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Nom,";
        $baseQuery .= " (SELECT MAX(pch.photo) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Photo,";
        $baseQuery .= " (SELECT MAX(pch.cni) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Cni,";
        $baseQuery .= ' MAX(t.denomination) as Groupement, MAX(t.id) as group_id,';
        $baseQuery .= ' MAX(tc.denomination) as Cooperative, MAX(tc.id) as coop_id';
        $baseQuery .= ' FROM exploitations e';
        $baseQuery .= ' INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id ';
        $baseQuery .= ' INNER JOIN tbl_organisations t ON t.id = p.organisation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations tc ON t.parent_id = tc.id';
        $baseQuery .= ' WHERE e.radie=0 ' . $where . ' GROUP BY id';
        $final = $baseQuery . " ORDER BY e.numero_membre)ph WHERE ph.Cni='' OR ph.Cni IS NULL";
        // render the recordset in a new page? or into the same one?
        return [
            'fields' => $fields,
            'titre' => 'Resultat de la rêquete, dans ' . $subtitre,
            'dql' => $final,
        ];
    }

    public function _getMembresSansPhoto($org_id, $orgType)
    {
        if ('coop' === $orgType) {
            $where = " AND tc.id ='" . $org_id . "'";
            $subtitre = 'la coopérative';
        } else {
            $where = " AND t.id ='" . $org_id . "'";
            $subtitre = 'le groupement';
        }
        $fields = ['NumeroMembre', 'Cni', 'Nom', 'Prenom', 'Cooperative', 'Groupement', 'Photo', 'qrimage'];
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery();
        $baseQuery = 'SELECT ph.* FROM';
        $baseQuery .= ' (SELECT e.id,e.utilisateur,';
        $baseQuery .= ' e.numero_membre as NumeroMembre, e.qrimage as qrimage,';
        $baseQuery .= " (SELECT MAX(pch.prenom) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Prenom,";
        $baseQuery .= " (SELECT MAX(pch.nom) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Nom,";
        $baseQuery .= " (SELECT MAX(pch.photo) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Photo,";
        $baseQuery .= " (SELECT MAX(pch.cni) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Cni,";
        $baseQuery .= ' MAX(t.denomination) as Groupement, MAX(t.id) as group_id,';
        $baseQuery .= ' MAX(tc.denomination) as Cooperative, MAX(tc.id) as coop_id';
        $baseQuery .= ' FROM exploitations e';
        $baseQuery .= ' INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id ';
        $baseQuery .= ' INNER JOIN tbl_organisations t ON t.id = p.organisation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations tc ON t.parent_id = tc.id';
        $baseQuery .= ' WHERE e.radie=0 ' . $where . ' GROUP BY id';
        $final = $baseQuery . " ORDER BY e.numero_membre)ph WHERE ph.photo=''";
        // render the recordset in a new page? or into the same one?
        return [
            'fields' => $fields,
            'titre' => 'Resultat de la rêquete, dans ' . $subtitre,
            'dql' => $final,
        ];
    }

    public function _getRecordset($coop)
    {
        $baseQuery = ' SELECT e.id ';
        $baseQuery .= ' FROM exploitations e';
        $baseQuery .= ' INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations tg ON tg.id = p.organisation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations tc ON tg.parent_id = tc.id';
        $baseQuery .= ' WHERE e.radie=0 ';
        $baseQuery .= " AND tc.id='" . $coop->getId() . "'";
        $baseQuery .= " AND e.qrimage='';";

        return [
            'dql' => $baseQuery,
        ];
    }

    public function _getRecordsetDoublons()
    {
        $fields = ['utilisateur', 'cdate', 'udate', 'doublons', 'NumeroMembre', 'Cni', 'Prenom', 'Nom', 'cooperative', 'groupement', 'colline', 'commune', 'province', 'IDMM'];
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery();
        $baseQuery = ' SELECT count(*) as doublons, e.id, e.utilisateur, e.cdate, e.udate,';
        $baseQuery .= ' e.numero_membre as NumeroMembre,';
        $baseQuery .= ' (SELECT MAX(pch.prenom)';
        $baseQuery .= '         FROM personnes pch';
        $baseQuery .= "         WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Prenom,";
        $baseQuery .= ' (SELECT MAX(pch.nom)';
        $baseQuery .= '         FROM personnes pch';
        $baseQuery .= "         WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.nom is not null AND pch.rol_famille='Chef de famille' ) AS Nom,";
        $baseQuery .= ' (SELECT MAX(pch.cni)';
        $baseQuery .= '         FROM personnes pch';
        $baseQuery .= "         WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Cni,";
        $baseQuery .= ' tc.denomination as cooperative,';
        $baseQuery .= ' t.denomination as groupement,';
        $baseQuery .= ' tdcol.denomination as colline,';
        $baseQuery .= ' tdcom.denomination as commune,';
        $baseQuery .= ' tdprov.denomination as province,';
        $baseQuery .= ' GROUP_CONCAT(e.numero_membre) as IDMM,';
        $baseQuery .= ' t.id as group_id,';
        $baseQuery .= ' tdcol.id as coll_id';
        $baseQuery .= ' FROM exploitations e';
        $baseQuery .= ' INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations t ON t.id = p.organisation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations tc ON t.parent_id = tc.id';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tdcol ON tdcol.id = e.localisation_admin_id';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tdcom ON tdcol.parent_id = tdcom.id';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tdprov ON tdcom.parent_id = tdprov.id';
        $baseQuery .= ' WHERE e.radie=0';
        $baseQuery .= ' GROUP BY Prenom, Nom, group_id, coll_id';
        $baseQuery .= '    HAVING count(*)>1';
        $baseQuery .= '         ORDER BY nom;';  // render the recordset in a new page? or into the same one?

        return [
            'fields' => $fields,
            'titre' => 'Exploitations qui sont des doublons',
            'dql' => $baseQuery,
        ];
    }

    public function _getRecordsetListe(TblOrganisations $coop)
    {
        $coop_id = $coop->getId();
        $where = " AND tc.id ='" . $coop_id . "'";

        $fields = ['NumeroMembre', 'Cni', 'Nom', 'Prenom', 'Cooperative', 'Groupement', 'Province', 'Commune', 'Colline', 'Photo', 'qrimage'];
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery();
        $baseQuery = 'SELECT e.id,e.utilisateur,';
        $baseQuery .= ' e.numero_membre as NumeroMembre, e.qrimage as qrimage,';
        $baseQuery .= ' tprov.denomination as Province,';
        $baseQuery .= ' tcom.denomination as Commune,';
        $baseQuery .= ' tcol.denomination as Colline,';
        $baseQuery .= " (SELECT MAX(pch.prenom) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Prenom,";
        $baseQuery .= " (SELECT MAX(pch.nom) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Nom,";
        $baseQuery .= " (SELECT MAX(pch.photo) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Photo,";
        $baseQuery .= " (SELECT MAX(pch.cni) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Cni,";
        $baseQuery .= ' MAX(t.denomination) as Groupement, MAX(t.id) as group_id,';
        $baseQuery .= ' MAX(tc.denomination) as Cooperative, MAX(tc.id) as coop_id';
        $baseQuery .= ' FROM exploitations e';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tcol ON e.localisation_admin_id =tcol.id ';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tcom ON tcol.parent_id =tcom.id ';
        $baseQuery .= ' INNER JOIN tbl_decoupage_admin tprov ON tcom.parent_id =tprov.id ';
        $baseQuery .= ' INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id ';
        $baseQuery .= ' INNER JOIN tbl_organisations t ON t.id = p.organisation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations tc ON t.parent_id = tc.id';
        $baseQuery .= ' WHERE e.radie=0 ' . $where . ' GROUP BY id';
        $final = $baseQuery . ' ORDER BY e.numero_membre';
        // render the recordset in a new page? or into the same one?
        return [
            'fields' => $fields,
            'titre' => 'Resultat de la rêquete ',
            'dql' => $final,
        ];
    }

    public function _getRecordsetPlusunchef()
    {
        $fields = ['utilisateur', 'cdate', 'udate', 'NumeroMembre', 'double_chef', 'cooperative', 'groupement'];
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery();
        $baseQuery = ' SELECT p.*';
        $baseQuery .= ' FROM';
        $baseQuery .= '         (SELECT e.id,e.utilisateur, e.cdate, e.udate,';
        $baseQuery .= '         e.numero_membre as NumeroMembre,';
        $baseQuery .= '         (SELECT count(pch.id)';
        $baseQuery .= '                         FROM personnes pch';
        $baseQuery .= "                         WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.nom is not null AND pch.rol_famille='Chef de famille'";
        $baseQuery .= '                         GROUP BY pch.rol_famille';
        $baseQuery .= '                         HAVING count(pch.id)>1) AS double_chef,';
        $baseQuery .= '                 tc.denomination as cooperative,';
        $baseQuery .= '                 t.denomination as groupement';
        $baseQuery .= '         FROM exploitations e';
        $baseQuery .= '                 INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id';
        $baseQuery .= '                 INNER JOIN tbl_organisations t ON t.id = p.organisation_id';
        $baseQuery .= '                 INNER JOIN tbl_organisations tc ON t.parent_id = tc.id';
        $baseQuery .= '         WHERE e.radie=0';
        $baseQuery .= '         ORDER BY `double_chef`) p';
        $baseQuery .= '  WHERE p.double_chef>1;';

        return [
            'fields' => $fields,
            'titre' => "Exploitations qui ont plus d'un chef de famille defini",
            'dql' => $baseQuery,
        ];
    }

    public function _getRecordsetSanschef()
    {
        $fields = ['utilisateur', 'cdate', 'udate', 'NumeroMembre', 'Cni', 'Prenom', 'Nom', 'cooperative', 'groupement'];
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery();
        $baseQuery = 'SELECT p.*';
        $baseQuery .= ' FROM (SELECT e.id,e.utilisateur, e.cdate, e.udate,';
        $baseQuery .= ' e.numero_membre as NumeroMembre,';
        $baseQuery .= ' (SELECT MAX(pch.prenom)';
        $baseQuery .= '         FROM personnes pch';
        $baseQuery .= "         WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Prenom,";
        $baseQuery .= ' (SELECT MAX(pch.nom)';
        $baseQuery .= '         FROM personnes pch';
        $baseQuery .= "         WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.nom is not null AND pch.rol_famille='Chef de famille' ) AS Nom,";
        $baseQuery .= ' (SELECT MAX(pch.cni)';
        $baseQuery .= '         FROM personnes pch';
        $baseQuery .= "         WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS Cni,";
        $baseQuery .= ' tc.denomination as cooperative,';
        $baseQuery .= ' t.denomination as groupement';
        $baseQuery .= ' FROM exploitations e';
        $baseQuery .= ' INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations t ON t.id = p.organisation_id';
        $baseQuery .= ' INNER JOIN tbl_organisations tc ON t.parent_id = tc.id';
        $baseQuery .= ' WHERE e.radie=0';
        $baseQuery .= ' GROUP BY Prenom, Nom, groupement';
        $baseQuery .= ' ORDER BY nom) p';
        $baseQuery .= ' WHERE p.Nom is null';

        return [
            'fields' => $fields,
            'titre' => "Exploitations qui n'ont pas de chef de famille defini",
            'dql' => $baseQuery,
        ];
    }

    /**
     * @Route("/{_locale}/admin/controleDoublons/", name="admin_controleDoublons")
     */
    public function controleDoublonsAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à ce page');
        $result = $this->_getRecordsetDoublons();
        $dql = $result['dql'];
        $fields = $result['fields'];

        $stmt = $this->getDoctrine()->getManager()
            ->getConnection()
            ->prepare($dql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $em = $this->getDoctrine()->getManager();
        $fauxDoublons = $em->getRepository(FauxDoublons::class)->findAll();

        $filteredResult = [];
        $fauxDoublonsCpt = 0;

        foreach ($result as $row) {
            $exploitationIds = explode(',', $row['IDMM']);
            array_map('intval', $exploitationIds);
            sort($exploitationIds);
            $toKeep = true;

            foreach ($fauxDoublons as $fauxDoublon) {
                if ($fauxDoublon->getExploitationIds() === $exploitationIds) {
                    $toKeep = false;
                    ++$fauxDoublonsCpt;

                    break;
                }
            }

            // else (si pas un faux doublon)
            if ($toKeep) {
                $filteredResult[] = $row;
            }
        }

        $cpt = count($filteredResult);
        $titre = "Cas potentiels d'exploitations identifiées comme doublon";
        $message = sprintf(
            "Il y a %d cas potentiels d'exploitations identifiées comme doublon.
            De plus, il y a %d cas de faux doublons (non-affichés sur cette page)",
            $cpt,
            $fauxDoublonsCpt
        );

        return $this->render('saisies/rec_controle.html.twig', [
            'fields' => $fields,
            'titre' => $titre,
            'message' => $message,
            'records' => $filteredResult,
        ]);
    }

    /**
     * @Route("/{_locale}/admin/controlePlusunchef/", name="admin_controlePlusunchef")
     */
    public function controlePlusunchefAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à ce page');
        $result = $this->_getRecordsetPlusunchef();
        $dql = $result['dql'];
        $titre = $result['titre'];
        $fields = $result['fields'];
        $stmt = $this->getDoctrine()->getManager()
            ->getConnection()
            ->prepare($dql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $cpt = count($result);
        $titre = $titre . ' (' . $cpt . ' exploitations)';

        return $this->render('saisies/rec_controle.html.twig', [
            'fields' => $fields,
            'titre' => $titre,
            'records' => $result,
        ]);
    }

    /**
     * @Route("/{_locale}/admin/controleSanschef/", name="admin_controleSanschef")
     */
    public function controleSanschefAction()
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à ce page');
        $result = $this->_getRecordsetSanschef();
        $dql = $result['dql'];
        $titre = $result['titre'];
        $fields = $result['fields'];
        $stmt = $this->getDoctrine()->getManager()
            ->getConnection()
            ->prepare($dql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $cpt = count($result);
        $titre = $titre . ' (' . $cpt . ' exploitations)';

        return $this->render('saisies/rec_controle.html.twig', [
            'fields' => $fields,
            'titre' => $titre,
            'records' => $result,
        ]);
    }

    /**
     * @Route("/{_locale}/admin/CreateQR/", name="admin_CreateQR")
     *
     * @param mixed $exploitation_id
     */
    public function CreateQR($exploitation_id)
    {
        // $data = 'otpauth://totp/test?secret=B3JX4VCVJDVNXNZ5&issuer=chillerlan.net';
        // $exploitation_id = '0004160c-2699-11e7-98c8-b8ca3a965972';
        $data = 'https://puma.ovh/saisie/info_gen/' . $exploitation_id . '/';
        //quick and simple:
        //    return $this->render('saisies/CreateQR.html.twig', array(
        //        'imageQR' => (new QRCode)->render($data),
        //    ));
        $options = new QROptions([
            'eccLevel' => QRCode::ECC_L, //Error Correction Feature Level L
            'outputType' => QRCode::OUTPUT_IMAGE_JPG, //setando o output como PNG
            'imageBase64' => false, //evitando que seja gerado a imagem em base64
        ]);
        $nomfichier = $exploitation_id . '.jpg';
        $nometpathfichier = $this->getAbsolutePathForDir('exploitations_qrcode_webdir') . '/' . $nomfichier;
        file_put_contents($nometpathfichier, (new QRCode($options))->render($data)); //salvando a imagem como jpg

        return $nomfichier;
    }

    /**
     * @Route("/{_locale}/admin/displayFilesZipetCsv/", name="admin_displayFilesZipetCsv")
     */
    public function displayFilesZipetCsvAction()
    {
        /* listing des fichiers csv déjà fait */
        $repCsv = $this->getAbsolutePathForDir('donnees_carte_membre_webdir');
        $listCSV = $this->OuvrirRepertoire($repCsv, 'csv');
        /* listing des fichiers zip pour les photos déjà fait */
        $repPhoto = $this->getAbsolutePathForDir('personnes_photo_webdir');
        $listPhoto = $this->OuvrirRepertoire($repPhoto, 'zip');
        /* listing des fichiers zip pour le QRcode déjà fait */
        $repCode = $this->getAbsolutePathForDir('exploitations_qrcode_webdir');
        $listQRcode = $this->OuvrirRepertoire($repCode, 'zip');

        $arrTotal = [];
        $cptC = count($listCSV);

        for ($i = 0; $i < $cptC; ++$i) {
            $keyCoop = mb_substr($listCSV[$i], 0, 3);
            $arrTotal[$keyCoop] = ['csv' => $listCSV[$i]];
        }
        $cptP = count($listPhoto);

        for ($i = 0; $i < $cptP; ++$i) {
            $keyCoop = mb_substr($listPhoto[$i], 0, 3);
            $arrTotal[$keyCoop]['photo'] = $listPhoto[$i];
        }
        $cptQ = count($listQRcode);

        for ($i = 0; $i < $cptQ; ++$i) {
            $keyCoop = mb_substr($listQRcode[$i], 0, 3);
            $arrTotal[$keyCoop]['qrcode'] = $listQRcode[$i];
        }

        /* afficher les resultats */
        return $this->render('admin/base/list_dataCartes.html.twig', [
            'listTotal' => $arrTotal,
            'dataCsv' => $this->getParameter('donnees_carte_membre_webdir'),
            'photosZip' => $this->getParameter('personnes_photo_webdir'),
            'qrcodeZip' => $this->getParameter('exploitations_qrcode_webdir'),
        ]);
    }

    /**
     * @Route("/{_locale}/admin/data/carte/membre/{coop}", name="admin_upload_data_carte_membre")
     */
    public function exportListeCoopAction(Request $request, TblOrganisations $coop)
    {
        set_time_limit(0);

        // TODO FIXME check that coop et non groupement

        $zipFile = new ZipFile();
        $pstrNameZip = $coop->getCode() . '.zip';
        $qrCodeDir = $this->getAbsolutePathForDir('exploitations_qrcode_webdir') . '/';
        $photoDir = $this->getAbsolutePathForDir('personnes_photo_webdir') . '/';
        $result = $this->_getRecordsetListe($coop);
        $dql = $result['dql'];
        $stmt = $this->getDoctrine()->getManager()
            ->getConnection()
            ->prepare($dql);
        $stmt->execute();
        $result2 = $stmt->fetchAll();
        $cpt = count($result2);
        $titre = $cpt . ' exploitations';

        $qRCodeFileGenerated = false;
        $photoFileGenerated = false;

        for ($i = 0; $i < $cpt; ++$i) {
            $photoName = $result2[$i]['Photo'];
            $qrcodeName = $result2[$i]['qrimage'];

            if ('' !== $photoName) {
                $photoFileGenerated = $zipFile->farr_addFileToArchive($photoDir, $photoName, $pstrNameZip);
            }

            if ('' !== $qrcodeName) {
                $qRCodeFileGenerated = $zipFile->farr_addFileToArchive($qrCodeDir, $qrcodeName, $pstrNameZip);
            }
        }

        return $this->render('saisies/rec_controle.html.twig', [
            'fields' => $result['fields'],
            'titre' => $titre,
            'records' => $result2,
            'photosZip' => $this->getParameter('personnes_photo_webdir') . '/' . $pstrNameZip,
            'qrcodeZip' => $this->getParameter('exploitations_qrcode_webdir') . '/' . $pstrNameZip,
            'qRCodeFileGenerated' => $qRCodeFileGenerated,
            'photoFileGenerated' => $photoFileGenerated,
        ]);
    }

    public function getAbsolutePathForDir($webDir)
    {
        $pjtDir = $this->getParameter('kernel.project_dir');
        $mdir = $this->getParameter($webDir);

        return realpath($pjtDir . '/public' . $mdir);
    }

    /**
     * @Route("/{_locale}/admin/ListeSansCniOrg/", name="admin_ListeSansCniOrg")
     */
    public function ListeSansCniOrgAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $rechercheLocalisation = new Organisations();

        $form = $this->createForm(
            'App\Form\Saisies\GetOrganisationType',
            $rechercheLocalisation,
            ['em_manager' => $em,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $coop = $data->getCooperative();
            $groupe = $data->getGroupement();

            if (null === $groupe) {
                $orgType = 'coop';
                $org_id = $coop->getId();
            } else {
                $orgType = 'groupe';
                $org_id = $groupe->getId();
            }

            $result = $this->_getMembresSansCni($org_id, $orgType);
            $titre = $result['titre'];
            $dql = $result['dql'];
            $stmt = $this->getDoctrine()->getManager()
                ->getConnection()
                ->prepare($dql);
            $stmt->execute();
            $result2 = $stmt->fetchAll();
            $cpt = count($result2);
            $titre .= ' il y a ' . $cpt . ' chef de famille sans CNI';

            return $this->render('saisies/rec_controle.html.twig', [
                'fields' => $result['fields'],
                'titre' => $titre,
                'records' => $result2,
            ]);
        }

        return $this->render('saisies/getorganisation.html.twig', [
            'form' => $form->createView(),
            'item' => 'CNI',
        ]);
    }

    /**
     * @Route("/{_locale}/admin/ListeSansPhotoOrg/", name="admin_ListeSansPhotoOrg")
     */
    public function listeSansPhotoOrgAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $rechercheLocalisation = new Organisations();

        $form = $this->createForm(
            'App\Form\Saisies\GetOrganisationType',
            $rechercheLocalisation,
            ['em_manager' => $em,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $coop = $data->getCooperative();
            $groupe = $data->getGroupement();

            if (null === $groupe) { // il faut savoir qui sera le membre(répondant ou le chef de famille?)
                $orgType = 'coop';
                $org_id = $coop->getId();
            } else {
                $orgType = 'groupe';
                $org_id = $groupe->getId();
            }

            $result = $this->_getMembresSansPhoto($org_id, $orgType);
            $titre = $result['titre'];
            $dql = $result['dql'];
            $stmt = $this->getDoctrine()->getManager()
                ->getConnection()
                ->prepare($dql);
            $stmt->execute();
            $result2 = $stmt->fetchAll();
            $cpt = count($result2);
            $titre .= ' il y a ' . $cpt . ' chef de famille sans photo';

            return $this->render('saisies/rec_controle.html.twig', [
                'fields' => $result['fields'],
                'titre' => $titre,
                'records' => $result2,
            ]);
        }

        return $this->render('saisies/getorganisation.html.twig', [
            'form' => $form->createView(),
            'item' => 'PHOTO',
        ]);
    }

    /**
     * @Route("/{_locale}/admin/membership/card/for/{coop}", name="admin_membership_card_for_coop")
     */
    public function membershipCardByCoopAction(Request $request, TblOrganisations $coop)
    {
        // return new Response($coop->getDenomination());

        $columns = [
            ['name' => 'Dénomination', 'ref' => '__toString'],
            ['name' => 'Carte de membre', 'ref' => 'carteMembre', 'type' => 'bool'], ];
        $titre = 'Liste des cartes de membre pour ' . $coop;
        $rowDatas = $coop->getDescendantOrSelfExploitations();
        $entityManager = $this->getDoctrine()->getManager();

        $formBuilder = $this->createFormBuilder();

        foreach ($rowDatas as $data) {
            $formBuilder->add($data->getId(), CheckboxType::class, [
                'data' => $data->getCarteMembre(),
                'label' => 'Carte de membre',
                'required' => false,
            ]);

            $formBuilder->add('submit', SubmitType::class, ['label' => 'Enregistrer']);
        }

        $form = $formBuilder->getForm();
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            foreach ($rowDatas as $rowData) {
                if ($rowData->getCarteMembre() !== $formData[$rowData->getId()]) {
                    $rowData->setCarteMembre($formData[$rowData->getId()]);
                    $entityManager->persist($rowData);
                }
            }

            $entityManager->flush();
        }

        $data = ['entity' => $coop];

        return $this->render('_general/data_table.html.twig', [
            'columns' => $columns,
            'title' => $titre,
            'row_datas' => $rowDatas,
            'form' => $form->createView(),
        ]);
    }

    public function OuvrirRepertoire($dirname, $filtre)
    {
        $dir = opendir($dirname);
        $ret = [];

        while ($file = readdir($dir)) {
            if ('.' !== $file && '..' !== $file && !is_dir($dirname . $file) && mb_substr($file, -3) === $filtre) {
                $ret[] = $file;
            }
        }
        closedir($dir);

        return $ret;
    }

    /**
     * @Route("/{_locale}/admin/QRforCoop/{coop}", name="admin_QRforCoop")
     */
    public function QRforCoop(Request $request, TblOrganisations $coop)
    {
        set_time_limit(0);  // run foorreeveeerr
        $my_em_manager = $this->getDoctrine()->getManager();

        $result = $this->_getRecordset($coop);
        $dql = $result['dql'];
        $stmt = $this->getDoctrine()->getManager()
            ->getConnection()
            ->prepare($dql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $cpt = count($result);

        for ($i = 0; $i < $cpt; ++$i) {
            $nomfichier = $this->CreateQR($result[$i]['id']);
            $mquery = "UPDATE exploitations SET qrimage='" . $nomfichier . "' WHERE id='" . $result[$i]['id'] . "';";
            $st2 = $this->getDoctrine()->getManager()
                ->getConnection()
                ->prepare($mquery);
            $st2->execute();
        }
        $titre = 'titre' . ' (' . $cpt . ' exploitations)';

        // TODO AMELIORER LA REPOSNSE (retour vers base + message)
        return new Response(
            $cpt . ' QR codes créés'
        );
    }
}
