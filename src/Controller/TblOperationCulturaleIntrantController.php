<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblOperationCulturaleIntrant;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblOperationCulturaleIntrant controller.
 *
 * @Route("/{_locale}/tbloperationculturaleintrant")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblOperationCulturaleIntrantController extends AbstractController
{
    /**
     * Deletes a TblOperationCulturaleIntrant entity.
     *
     * @Route("/{id}", name="tbloperationculturaleintrant_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblOperationCulturaleIntrant $tblOperationCulturaleIntrant)
    {
        $form = $this->createDeleteForm($tblOperationCulturaleIntrant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblOperationCulturaleIntrant);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('tbloperationculturaleintrant_index');
    }

    /**
     * Displays a form to edit an existing TblOperationCulturaleIntrant entity.
     *
     * @Route("/{id}/edit", name="tbloperationculturaleintrant_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblOperationCulturaleIntrant $tblOperationCulturaleIntrant)
    {
        $deleteForm = $this->createDeleteForm($tblOperationCulturaleIntrant);
        $editForm = $this->createForm('App\Form\TblOperationCulturaleIntrantType', $tblOperationCulturaleIntrant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOperationCulturaleIntrant);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            $this->addFlash(
                'success',
                'L\\opération a bien été modifiée'
            );

            return $this->redirectToRoute('tbloperationculturaleintrant_index', ['id' => $tblOperationCulturaleIntrant->getId()]);
        }

        return $this->render('tbloperationculturaleintrant/edit.html.twig', [
            'tblOperationCulturaleIntrant' => $tblOperationCulturaleIntrant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblOperationCulturaleIntrant entities.
     *
     * @Route("/", name="tbloperationculturaleintrant_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblOperationCulturaleIntrants = $em->getRepository(TblOperationCulturaleIntrant::class)->findAll();

        return $this->render('tbloperationculturaleintrant/index.html.twig', [
            'tblOperationCulturaleIntrants' => $tblOperationCulturaleIntrants,
        ]);
    }

    /**
     * Creates a new TblOperationCulturaleIntrant entity.
     *
     * @Route("/new", name="tbloperationculturaleintrant_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblOperationCulturaleIntrant = new TblOperationCulturaleIntrant($user);
        $form = $this->createForm('App\Form\TblOperationCulturaleIntrantType', $tblOperationCulturaleIntrant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOperationCulturaleIntrant);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('tbloperationculturaleintrant_index');
        }

        return $this->render('tbloperationculturaleintrant/new.html.twig', [
            'tblOperationCulturaleIntrant' => $tblOperationCulturaleIntrant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblOperationCulturaleIntrant entity.
     *
     * @param TblOperationCulturaleIntrant $tblOperationCulturaleIntrant The TblOperationCulturaleIntrant entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblOperationCulturaleIntrant $tblOperationCulturaleIntrant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tbloperationculturaleintrant_delete', ['id' => $tblOperationCulturaleIntrant->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
