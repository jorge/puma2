<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\Habitat;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Habitat controller.
 *
 * @Route("/{_locale}/references/habitat")
 */
final class HabitatController extends AbstractController
{
    /**
     * Deletes a Habitat entity.
     *
     * @Route("/{id}", name="references_habitat_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Habitat $habitat)
    {
        $exploitation = $habitat->getExploitation();
        $annee = $habitat->getAnnee();

        $form = $this->createDeleteForm($habitat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $habitat->setRadie(true);
            $em->persist($habitat);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'id' => $exploitation->getId(),
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing Habitat entity.
     *
     * @Route("/{id}/edit", name="references_habitat_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Habitat $habitat)
    {
        $exploitation = $habitat->getExploitation();
        $annee = $habitat->getAnnee();
        $deleteForm = $this->createDeleteForm($habitat);
        $editForm = $this->createForm('App\Form\HabitatType', $habitat);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($habitat);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('habitat/edit.html.twig', [
            'exploitation' => $exploitation,
            'annee' => $annee,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new Habitat entity.
     *
     * @Route("/new", name="references_habitat_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $exploitation_id = $request->query->get('exploitation_id');
        $annee = $request->query->get('annee');
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $habitat = new Habitat($user);

        if (isset($exploitation_id)) {
            $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
            $habitat->setExploitation($exploitation);
        }

        if (isset($annee)) {
            $habitat->setAnnee($annee);
        }
        $form = $this->createForm('App\Form\HabitatType', $habitat);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $habitat->setRadie(false);
            $em->persist($habitat);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation_id,
                'annee' => $annee,
            ]);
        }

        return $this->render('habitat/new.html.twig', [
            'habitat' => $habitat,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Habitat entity.
     *
     * @Route("/{id}", name="references_habitat_show", methods={"GET"})
     */
    public function showAction(Habitat $habitat)
    {
        $exploitation = $habitat->getExploitation();
        $annee = $habitat->getAnnee();

        return $this->render('habitat/show.html.twig', [
            'habitat' => $habitat,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
    }

    /**
     * Creates a form to delete a Habitat entity.
     *
     * @param Habitat $habitat The Habitat entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Habitat $habitat)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_habitat_delete', [
                'id' => $habitat->getId(),
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
