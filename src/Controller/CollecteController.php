<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Collecte;
use App\Entity\Exercice;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Collecte controller.
 *
 * @Route("/{_locale}/references/collecte", name="collecte_")
 */

use Symfony\Component\Routing\Annotation\Route;

final class CollecteController extends AbstractController
{
    /**
     * Deletes a Collecte entity.
     *
     * @Route("/{id}", name="references_collecte_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Collecte $collecte)
    {
        $form = $this->createDeleteForm($collecte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $collecte->setRadie(TRUE);
            $em->persist($collecte);
            $em->flush();
        }

        return $this->redirectToRoute('references_collecte_index');
    }

    /**
     * Displays a form to edit an existing Collecte entity.
     *
     * @Route("/{id}/edit", name="references_collecte_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Collecte $collecte)
    {
        $deleteForm = $this->createDeleteForm($collecte);
        $editForm = $this->createForm('App\Form\CollecteType', $collecte);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($collecte);
            $em->flush();

            return $this->redirectToRoute('references_collecte_edit', ['id' => $collecte->getId()]);
        }

        return $this->render('collecte/edit.html.twig', [
            'collecte' => $collecte,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new Collecte entity.
     *
     * @Route("/{exercice}/new", name="new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, Exercice $exercice)
    {
        $user = $this->getUser();
        $collecte = new Collecte($user);
        $collecte->setExercice($exercice);
        $form = $this->createForm('App\Form\CollecteType', $collecte);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($collecte);
            $em->flush();

            return $this->redirectToRoute('saisie_info_gen', [
                'annee' => $exercice->getAnnee(),
                'exploitation' => $exercice->getExploitation()->getId(),
            ]);
        }

        return $this->render('collecte/new.html.twig', [
            'collecte' => $collecte,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Collecte entity.
     *
     * @Route("/{id}", name="references_collecte_show", methods={"GET"})
     */
    public function showAction(Collecte $collecte)
    {
        $deleteForm = $this->createDeleteForm($collecte);

        return $this->render('collecte/show.html.twig', [
            'collecte' => $collecte,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a Collecte entity.
     *
     * @param Collecte $collecte The Collecte entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Collecte $collecte)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_collecte_delete', ['id' => $collecte->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
