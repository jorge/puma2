<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblTypeCommercialisation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblTypeCommercialisation controller.
 *
 * @Route("/{_locale}/references/tbltypecommercialisation")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblTypeCommercialisationController extends AbstractController
{
    /**
     * Deletes a TblTypeCommercialisation entity.
     *
     * @Route("/{id}", name="references_tbltypecommercialisation_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblTypeCommercialisation $tblTypeCommercialisation)
    {
        $form = $this->createDeleteForm($tblTypeCommercialisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblTypeCommercialisation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbltypecommercialisation_index');
    }

    /**
     * Displays a form to edit an existing TblTypeCommercialisation entity.
     *
     * @Route("/{id}/edit", name="references_tbltypecommercialisation_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblTypeCommercialisation $tblTypeCommercialisation)
    {
        $deleteForm = $this->createDeleteForm($tblTypeCommercialisation);
        $editForm = $this->createForm('App\Form\TblTypeCommercialisationType', $tblTypeCommercialisation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeCommercialisation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypecommercialisation_index', ['id' => $tblTypeCommercialisation->getId()]);
        }

        return $this->render('tbltypecommercialisation/edit.html.twig', [
            'tblTypeCommercialisation' => $tblTypeCommercialisation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblTypeCommercialisation entities.
     *
     * @Route("/", name="references_tbltypecommercialisation_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblTypeCommercialisations = $em->getRepository(TblTypeCommercialisation::class)->findAll();

        return $this->render('tbltypecommercialisation/index.html.twig', [
            'tblTypeCommercialisations' => $tblTypeCommercialisations,
        ]);
    }

    /**
     * Creates a new TblTypeCommercialisation entity.
     *
     * @Route("/new", name="references_tbltypecommercialisation_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblTypeCommercialisation = new TblTypeCommercialisation($user);
        $form = $this->createForm('App\Form\TblTypeCommercialisationType', $tblTypeCommercialisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeCommercialisation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypecommercialisation_index', ['id' => $tblTypeCommercialisation->getId()]);
        }

        return $this->render('tbltypecommercialisation/new.html.twig', [
            'tblTypeCommercialisation' => $tblTypeCommercialisation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblTypeCommercialisation entity.
     *
     * @Route("/{id}", name="references_tbltypecommercialisation_show", methods={"GET"})
     */
    public function showAction(TblTypeCommercialisation $tblTypeCommercialisation)
    {
        $deleteForm = $this->createDeleteForm($tblTypeCommercialisation);

        return $this->render('tbltypecommercialisation/show.html.twig', [
            'tblTypeCommercialisation' => $tblTypeCommercialisation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblTypeCommercialisation entity.
     *
     * @param TblTypeCommercialisation $tblTypeCommercialisation The TblTypeCommercialisation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblTypeCommercialisation $tblTypeCommercialisation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbltypecommercialisation_delete', ['id' => $tblTypeCommercialisation->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
