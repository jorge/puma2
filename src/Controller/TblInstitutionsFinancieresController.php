<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblInstitutionsFinancieres;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblInstitutionsFinancieres controller.
 *
 * @Route("/{_locale}/references/tblinstitutionsfinancieres")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblInstitutionsFinancieresController extends AbstractController
{
    /**
     * Deletes a TblInstitutionsFinancieres entity.
     *
     * @Route("/{id}", name="references_tblinstitutionsfinancieres_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblInstitutionsFinancieres $tblInstitutionsFinanciere)
    {
        $form = $this->createDeleteForm($tblInstitutionsFinanciere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblInstitutionsFinanciere);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblinstitutionsfinancieres_index');
    }

    /**
     * Displays a form to edit an existing TblInstitutionsFinancieres entity.
     *
     * @Route("/{id}/edit", name="references_tblinstitutionsfinancieres_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblInstitutionsFinancieres $tblInstitutionsFinanciere)
    {
        $deleteForm = $this->createDeleteForm($tblInstitutionsFinanciere);
        $editForm = $this->createForm('App\Form\TblInstitutionsFinancieresType', $tblInstitutionsFinanciere);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblInstitutionsFinanciere);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblinstitutionsfinancieres_index', ['id' => $tblInstitutionsFinanciere->getId()]);
        }

        return $this->render('tblinstitutionsfinancieres/edit.html.twig', [
            'tblInstitutionsFinanciere' => $tblInstitutionsFinanciere,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblInstitutionsFinancieres entities.
     *
     * @Route("/", name="references_tblinstitutionsfinancieres_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblInstitutionsFinancieres = $em->getRepository(TblInstitutionsFinancieres::class)->findAll();

        return $this->render('tblinstitutionsfinancieres/index.html.twig', [
            'tblInstitutionsFinancieres' => $tblInstitutionsFinancieres,
        ]);
    }

    /**
     * Creates a new TblInstitutionsFinancieres entity.
     *
     * @Route("/new", name="references_tblinstitutionsfinancieres_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblInstitutionsFinanciere = new TblInstitutionsFinancieres($user);
        $form = $this->createForm('App\Form\TblInstitutionsFinancieresType', $tblInstitutionsFinanciere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblInstitutionsFinanciere);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblinstitutionsfinancieres_index', ['id' => $tblInstitutionsFinanciere->getId()]);
        }

        return $this->render('tblinstitutionsfinancieres/new.html.twig', [
            'tblInstitutionsFinanciere' => $tblInstitutionsFinanciere,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblInstitutionsFinancieres entity.
     *
     * @Route("/{id}", name="references_tblinstitutionsfinancieres_show", methods={"GET"})
     */
    public function showAction(TblInstitutionsFinancieres $tblInstitutionsFinanciere)
    {
        $deleteForm = $this->createDeleteForm($tblInstitutionsFinanciere);

        return $this->render('tblinstitutionsfinancieres/show.html.twig', [
            'tblInstitutionsFinanciere' => $tblInstitutionsFinanciere,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblInstitutionsFinancieres entity.
     *
     * @param TblInstitutionsFinancieres $tblInstitutionsFinanciere The TblInstitutionsFinancieres entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblInstitutionsFinancieres $tblInstitutionsFinanciere)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblinstitutionsfinancieres_delete', ['id' => $tblInstitutionsFinanciere->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
