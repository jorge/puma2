<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblDomainesEtudesSuperieures;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblDomainesEtudesSuperieures controller.
 *
 * @Route("/{_locale}/references/tbldomainesetudessuperieures")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblDomainesEtudesSuperieuresController extends AbstractController
{
    /**
     * Deletes a TblDomainesEtudesSuperieures entity.
     *
     * @Route("/{id}", name="references_tbldomainesetudessuperieures_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblDomainesEtudesSuperieures $tblDomainesEtudesSuperieure)
    {
        $form = $this->createDeleteForm($tblDomainesEtudesSuperieure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblDomainesEtudesSuperieure);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbldomainesetudessuperieures_index');
    }

    /**
     * Displays a form to edit an existing TblDomainesEtudesSuperieures entity.
     *
     * @Route("/{id}/edit", name="references_tbldomainesetudessuperieures_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblDomainesEtudesSuperieures $tblDomainesEtudesSuperieure)
    {
        $deleteForm = $this->createDeleteForm($tblDomainesEtudesSuperieure);
        $editForm = $this->createForm('App\Form\TblDomainesEtudesSuperieuresType', $tblDomainesEtudesSuperieure);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblDomainesEtudesSuperieure);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbldomainesetudessuperieures_index', ['id' => $tblDomainesEtudesSuperieure->getId()]);
        }

        return $this->render('tbldomainesetudessuperieures/edit.html.twig', [
            'tblDomainesEtudesSuperieure' => $tblDomainesEtudesSuperieure,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblDomainesEtudesSuperieures entities.
     *
     * @Route("/", name="references_tbldomainesetudessuperieures_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblDomainesEtudesSuperieures = $em->getRepository(TblDomainesEtudesSuperieures::class)->findAll();

        return $this->render('tbldomainesetudessuperieures/index.html.twig', [
            'tblDomainesEtudesSuperieures' => $tblDomainesEtudesSuperieures,
        ]);
    }

    /**
     * Creates a new TblDomainesEtudesSuperieures entity.
     *
     * @Route("/new", name="references_tbldomainesetudessuperieures_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblDomainesEtudesSuperieure = new TblDomainesEtudesSuperieures($user);
        $form = $this->createForm('App\Form\TblDomainesEtudesSuperieuresType', $tblDomainesEtudesSuperieure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblDomainesEtudesSuperieure);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbldomainesetudessuperieures_index', ['id' => $tblDomainesEtudesSuperieure->getId()]);
        }

        return $this->render('tbldomainesetudessuperieures/new.html.twig', [
            'tblDomainesEtudesSuperieure' => $tblDomainesEtudesSuperieure,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblDomainesEtudesSuperieures entity.
     *
     * @Route("/{id}", name="references_tbldomainesetudessuperieures_show", methods={"GET"})
     */
    public function showAction(TblDomainesEtudesSuperieures $tblDomainesEtudesSuperieure)
    {
        $deleteForm = $this->createDeleteForm($tblDomainesEtudesSuperieure);

        return $this->render('tbldomainesetudessuperieures/show.html.twig', [
            'tblDomainesEtudesSuperieure' => $tblDomainesEtudesSuperieure,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblDomainesEtudesSuperieures entity.
     *
     * @param TblDomainesEtudesSuperieures $tblDomainesEtudesSuperieure The TblDomainesEtudesSuperieures entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblDomainesEtudesSuperieures $tblDomainesEtudesSuperieure)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbldomainesetudessuperieures_delete', ['id' => $tblDomainesEtudesSuperieure->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
