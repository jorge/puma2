<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblSaisons;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * tblsaisons controller.
 *
 * @Route("/{_locale}/references/tblsaisons")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblSaisonsController extends AbstractController
{
    /**
     * Deletes a TblSaisons entity.
     *
     * @Route("/{id}", name="references_tblsaisons_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblSaisons $tblSaison)
    {
        $form = $this->createDeleteForm($tblSaison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblSaison);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblsaisons_index');
    }

    /**
     * Displays a form to edit an existing TblSaisons entity.
     *
     * @Route("/{id}/edit", name="references_tblsaisons_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblSaisons $tblSaison)
    {
        $deleteForm = $this->createDeleteForm($tblSaison);
        $editForm = $this->createForm('App\Form\TblSaisonsType', $tblSaison);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblSaison);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblsaisons_index', ['id' => $tblSaison->getId()]);
        }

        return $this->render('tblsaisons/edit.html.twig', [
            'tblSaison' => $tblSaison,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblSaisons entities.
     *
     * @Route("/", name="references_tblsaisons_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblSaisons = $em->getRepository(TblSaisons::class)->findAll();

        return $this->render('tblsaisons/index.html.twig', [
            'tblSaisons' => $tblSaisons,
        ]);
    }

    /**
     * Creates a new TblSaisons entity.
     *
     * @Route("/new", name="references_tblsaisons_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblSaison = new TblSaisons($user);
        $form = $this->createForm('App\Form\TblSaisonsType', $tblSaison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblSaison);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblsaisons_index', ['id' => $tblSaison->getId()]);
        }

        return $this->render('tblsaisons/new.html.twig', [
            'tblSaison' => $tblSaison,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblSaisons entity.
     *
     * @param TblSaisons $tblSaison The TblSaisons entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblSaisons $tblSaison)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblsaisons_delete', ['id' => $tblSaison->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
