<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\ConsultationF0;
use App\Entity\ConsultationF1;
use App\Entity\ConsultationSignaletique;
use App\Entity\Personnes;
use App\Entity\RapportsAllSignaletique;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Contracts\Translation\TranslatorInterface;

final class ConsultationController extends AbstractController
{
    /**
     * @Route("/{_locale}/consultation/allsignaletique", name="allsignaletique")
     */
    public function allsignaletiqueAction(Request $request, TranslatorInterface $translator)
    {
        $AllSignaletique = new RapportsAllSignaletique();

        $form = $this->createForm('App\Form\RapportsAllSignaletiqueType', $AllSignaletique, [
            //  'nb_annees_passees' => $this->container->getParameter('annee_passees')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->getAllSignaletique($form, $translator);

            return $this->render('rapports/rec_AllSignaletique.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
                'strSql' => $result['strSql'],
            ]);
        }

        return $this->render('rapports/allsignaletique.html.twig', [
            'AllSignaletique' => $AllSignaletique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/consultation", name="consultation")
     */
    public function consultationAction(Request $request, TranslatorInterface $translator)
    {
        $f0 = new ConsultationF0();

        $formf0 = $this->createForm('App\Form\ConsultationF0Type', $f0);
        /* prendre la requete pour f0 */
        $formf0->handleRequest($request);

        if ($formf0->isSubmitted() && $formf0->isValid()) {
            $result = $this->_getRecordsetF0($formf0, $translator);

            return $this->render('consultation/rec_f0.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
                'strSql' => $result['strSql'],
            ]);
        }

        return $this->render('consultation/consultation.html.twig', [
            'consultationf0' => $f0,
            'f0' => $formf0->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/consultation/f0", name="f0")
     */
    public function f0Action(Request $request, TranslatorInterface $translator)
    {
        $consultationf0 = new ConsultationF0();

        $form = $this->createForm('App\Form\ConsultationF0Type', $consultationf0);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->_getRecordsetF0($form, $translator);

            return $this->render('consultation/rec_f0.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
                'strSql' => $result['strSql'],
            ]);
        }

        return $this->render('consultation/f0.html.twig', [
            'consultationf0' => $consultationf0,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{_locale}/consultation/f1", name="f1")
     */
    public function f1Action(Request $request)
    {
        $themes = [
            'batiments' => 'Rapports des batîments des exploitations groupés par coopérative et/ou par groupement et/ou par année',
            'transport' => 'Rapports des moyens de transport/traction des exploitations groupés par coopérative et/ou par groupement et/ou par année',
            'outillages' => 'Rapports des outillages des exploitations groupés par coopérative et/ou par groupement et/ou par année',
            'parcelles' => 'Rapport des parcelles des exploitations groupés par coopérative et/ou par groupement et/ou par année',
            'troupeau' => 'Rapport des troupeaux des exploitations groupés par coopérative et/ou par groupement et/ou par année',
        ];
        $theme = $request->query->get('theme');
        $consultationf1 = new ConsultationF1();
        $consultationf1->setQueryTheme($theme);
        $form = $this->createForm('App\Form\ConsultationF1Type', $consultationf1);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->_getRecordsetF1($form);

            return $this->render('consultation/rec_f1.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
                'strSql' => $result['strSql'],
            ]);
        }
        $mTitre = $themes[$theme];

        return $this->render('consultation/f1.html.twig', [
            'consultationf1' => $consultationf1,
            'form' => $form->createView(),
            'mTitre' => $mTitre,
            'theme' => $theme,
        ]);
    }

    /**
     * @Route("/{_locale}/consultation/signaletique", name="signaletique")
     */
    public function signaletiqueAction(Request $request)
    {
        $theme = $request->query->get('theme');
        $consultation = new ConsultationSignaletique();
        $consultation->setQueryTheme($theme);
        $form = $this->createForm('App\Form\ConsultationSignaletiqueType', $consultation, []);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->_getRecordsetSignaletique($form);

            return $this->render('consultation/rec_signaletique.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
                'strSql' => $result['strSql'],
            ]);
        }

        return $this->render('consultation/signaletique.html.twig', [
            'consultation' => $consultation,
            'form' => $form->createView(),
        ]);
    }

    private function getAllSignaletique($form, TranslatorInterface $translator)
    {
        /* ok */
        /* construction de la requete  */
        $parametros = $_POST['rapports_all_signaletique'];
        $cooperative_id = $parametros['Denomination'];

        $strWhere = empty($cooperative_id) ? '' : " AND c.id='" . $cooperative_id . "'";

        $titre = 'Rapport sur la signaletique complet';

        $strSql = "SELECT c.denomination AS cooperative,
					g.denomination AS groupement,
					e.numero_membre,
					pers.nom,
					pers.prenom,
					e.annee_adhesion,
                                        e.membres_menage,
					e.personnes_charge,
					tec.etat_civil,
					e.nombre_fils,
					e.nombre_filles,
					e.fils_scolarises,
					e.filles_scolarisess AS filles_scolarisees,
					pers.rol_famille,
					pers.sexe,
					pers.date_naissance
			FROM tbl_organisations c
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
						INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						INNER JOIN exploitations e ON p.exploitation_id = e.id
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN personnes pers ON pers.exploitation_id = e.id
						INNER JOIN tbl_etat_civil tec ON pers.etat_civil_id = tec.id
			WHERE tol.level=1
			  AND pers.rol_famille='" . Personnes::role_chefDeFamille . "'
			  {$strWhere}
			ORDER BY cooperative, groupement, e.numero_membre";

        /* construction de champs à afficher */
        $fields['cooperative'] = $translator->trans('cooperative');
        $fields['groupement'] = $translator->trans('groupement');
        $fields['numero_membre'] = $translator->trans('numero_membre');
        $fields['nom'] = $translator->trans('nom');
        $fields['prenom'] = $translator->trans('prenom');
        $fields['annee_adhesion'] = $translator->trans('annee_adhesion');
        $fields['membres_menage'] = $translator->trans('membres_menage');
        $fields['personnes_charge'] = $translator->trans('personnes_charge');
        $fields['etat_civil'] = $translator->trans('etat_civil');
        $fields['rol_famille'] = $translator->trans('rol_famille');
        $fields['sexe'] = $translator->trans('sexe');
        $fields['date_naissance'] = $translator->trans('date_naissance');
        $fields['personnes_charge'] = $translator->trans('personnes_charge');
        $fields['nombre_fils'] = $translator->trans('nombre_fils');
        $fields['nombre_filles'] = $translator->trans('nombre_filles');
        $fields['fils_scolarises'] = $translator->trans('fils_scolarises');
        $fields['filles_scolarisees'] = $translator->trans('filles_scolarisees');

        /* execution requête et envoi de l'array  */
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
        $recordset = $stmt->fetchAll();

        return $result = [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $recordset,
        ];
    }

    private function _getRecordsetF0($form, TranslatorInterface $translator)
    {
        /* lire les parametres receptionnés ** */
        $denomination = $form->get('denomination')->getData();
        $groupegroup = $form->get('groupegroup')->getData();
        $queryTheme = $form->get('queryTheme')->getData();
        $exploitationGroupAnnee = $form->get('exploitationGroupAnnee')->getData();
        $exploitationListAnnee = $form->get('exploitationListAnnee')->getData();

        dump($exploitationListAnnee);

        $groupeCible = $form->get('groupeCible')->getData();
        $choixGroup = $form->get('choixGroup')->getData();
        $groupProf = $form->get('groupProf')->getData();
        $Cotisations = $form->get('Cotisations')->getData();
        $Commercialisation = $form->get('Commercialisation')->getData();
        $Formations_recus = $form->get('Formations_recus')->getData();
        $annee = $exploitationListAnnee;
        /* construction de la requête */
        $t_coop = '';
        $t_group = '';

        /* pour afficher les en-tête de colonne */
        $fields = [];
        $titre = 'Résultat de la requête';
        $select = 'SELECT coop.denomination as cooperative ';
        $from = 'FROM tbl_organisations coop
                    INNER JOIN tbl_organisations grpb ON coop.id = grpb.parent_id
                    INNER JOIN pivot_exploitation_organisations peo ON grpb.id = peo.organisation_id
                    INNER JOIN exploitations expl ON peo.exploitation_id = expl.id ';
        $from_p_pd = ' INNER JOIN personnes p ON expl.id=p.exploitation_id
                       INNER JOIN personne_details pd ON p.id=pd.personnes_id ';   // a changer de position "INNER JOIN personnes ... ... pd.personnes_id
        $where = ' WHERE ';
        $group = ' GROUP BY ';
        $order = ' ORDER BY ';

        if ($form->get('denomination')->getData()) {
            /* Une coopérative a été sélectionnée */
            $fields['cooperative'] = $translator->trans('cooperative');
            $where .= " coop.id = '" . $denomination->getId() . "'";
            //    $where .= " AND " . $where_gc; à mouvoir
            $group .= ' cooperative';

            if ('groupement' === $groupegroup) {
                $fields['groupement'] = $translator->trans('groupement');
                $select .= sprintf(', grpb.denomination as %s ', $fields['groupement']);
                $group .= ' , groupement';
            }   /* NB pas possible de ne pas grouper par groupement pour ce choix */
        } else {
            /* Toutes les coopératives */
            $fields['cooperative'] = $translator->trans('cooperative');

            if ('groupement' === $groupegroup) {
                $fields['groupement'] = $translator->trans('groupement');
                $select .= sprintf(', grpb.denomination as %s ', $fields['groupement']);
                $group .= ' cooperative, groupement';
            } else {
                $group .= ' cooperative';
            }

            if (mb_strpos($group, 'cooperative') === false) {
                $group .= ' cooperative ';
            }

            if (mb_strpos($order, 'cooperative') === false) {
                $order .= ' cooperative ';
            }
        }
        /* premier option de requête */
        switch ($queryTheme) {
            case 'etudes':
                $fields['annee'] = $translator->trans('annee');
                $select .= ' , pd.annee as annee ';

                if ($exploitationGroupAnnee) {
                    $group .= ' ,pd.annee';
                } elseif (mb_strlen($where) < 10) {
                    $where .= " pd.annee='" . $exploitationListAnnee . "'";
                } else {
                    $where .= " AND pd.annee='" . $exploitationListAnnee . "'";
                }
                $from .= $from_p_pd;

                if ('conjoints' === $groupeCible) {
                    $where_gc = " p.rol_famille='" . Personnes::role_conjoint . "'";
                } else {
                    $where_gc = " p.rol_famille='" . Personnes::role_chefDeFamille . "'";
                }

                if (mb_strlen($where) < 10) {
                    $where .= $where_gc;
                } else {
                    $where .= ' AND ' . $where_gc;
                }

                switch ($choixGroup) {
                    case 'kirundi':
                        $fields[$groupeCible] = $translator->trans($groupeCible);
                        $select .= ', Count(expl.id) AS ' . $groupeCible . ' ';
                        $titre = 'Rapport groupé';
                        $where .= ' AND pd.kirundi_lire=1 AND pd.kirundi_ecrire=1';
                        $titre .= ' des ' . $groupeCible . ' qui lisent et ecrivent le kirundi';

                        break;
                    case 'francais':
                        $fields['Conjoints'] = $translator->trans('Conjoints');
                        $select .= ',  Count(expl.id) AS Conjoints ';
                        $titre = 'Rapport group&eacute; ';
                        $where .= ' AND pd.francais_lire=1 AND pd.francais_ecrire=1';
                        $titre .= ' des conjoints qui lisent et écrivent le français';

                        break;
                    case 'primaire':
                        $fields['primaire'] = $translator->trans('primaire');
                        $select .= ',  count(pd.primaire) as primaire ';
                        $where .= ' AND pd.primaire = 1';

                        break;
                    case 'secondaire':
                        $fields['secondaire'] = $translator->trans('secondaire');
                        $select .= ',  count(pd.secondaire) as secondaire ';
                        $where .= ' AND pd.secondaire = 1';

                        break;
                    case 'superieur':
                        $fields['superieur'] = $translator->trans('superieur');
                        $select .= ',  count(pd.diplome) as superieur ';
                        $where .= ' AND pd.diplome = 6';

                        break;
                    case 'Sans_Diplome':
                        $fields['Sans_diplome'] = $translator->trans('Sans_Diplome');
                        $select .= ' , Count(expl.id) AS Sans_Diplome ';
                        $where .= ' AND pd.`diplome`=0 ';
                        $titre = "Rapport des conjoints groupés par type de diplome: 'Sans Diplôme' ";

                        break;
                    case 'A2':
                        $fields['A2'] = $translator->trans('A2');
                        $select .= ' , Count(expl.id) AS A2 ';
                        $where .= ' AND pd.`diplome`=1 ';
                        $titre = "Rapport des conjoints groupés par type de diplome: 'A2' ";

                        break;
                    case 'A3':
                        $fields['A3'] = $translator->trans('A3');
                        $select .= ' , Count(expl.id) AS A3 ';
                        $where .= ' AND pd.`diplome`=2 ';
                        $titre = "Rapport des conjoints groupés par type de diplome: 'A3' ";

                        break;
                    case 'D4':
                        $fields['D4'] = $translator->trans('D4');
                        $select .= ' , Count(expl.id) AS D4 ';
                        $where .= ' AND pd.`diplome`=3 ';
                        $titre = "Rapport des conjoints groupés par type de diplome: 'D4' ";

                        break;
                    case 'D6':
                        $fields['D6'] = $translator->trans('D6');
                        $select .= ' , Count(expl.id) AS D6 ';
                        $where .= ' AND pd.`diplome`=4';
                        $titre = "Rapport des conjoints groupés par type de diplome: 'D6' ";

                        break;
                    case 'D7':
                        $fields['D7'] = $translator->trans('D7');
                        $select .= ' , Count(expl.id) AS D7 ';
                        $where .= ' AND pd.`diplome`=5';
                        $titre = "Rapport des conjoints groupés par type de diplome: 'D7' ";

                        break;
                    case 'superieur':
                        $fields['Superieur'] = $translator->trans('Superieur');
                        $select .= ' , Count(expl.id) AS Superieur ';
                        $where .= ' AND pd.`diplome`=6';
                        $titre = "Rapport des conjoints groupés par type de diplome: 'Superieur' ";

                        break;
                    case 'domaine':
                        $fields['domaine'] = $translator->trans('domaine');
                        $fields['Nombre'] = $translator->trans('Nombre');
                        $select .= ', pd.domaine, Count(p.exploitation) AS Nombre ';
                        $where .= " AND pd.domaine<>''";
                        $group .= ', domaine ';
                        $titre = 'Rapport des conjoints groupés par domaine';

                        break;
                }

                break;
            case 'professions':
                $from .= $from_p_pd;

                if ($exploitationGroupAnnee) {
                    $group .= ' , pd.annee';
                } elseif (mb_strlen($where) < 10) {
                    $where .= " pd.annee = '" . $exploitationListAnnee . "'";
                } else {
                    $where .= " AND pd.annee = '" . $exploitationListAnnee . "'";
                }

                switch ($groupProf) {
                    case 'principale':
                            $fields['Profession'] = $translator->trans('Profession');
                            $fields['Membres'] = $translator->trans('Membres');
                            $from .= ' INNER JOIN tbl_professions tf ON tf.id=pd.profession_principale_id ';

                        if (mb_strlen($select) < 10) {
                            $select .= 'tf.profession AS Profession, Count( expl.id ) AS Membres ';
                        } else {
                            $select .= ', tf.profession AS Profession, Count( expl.id ) AS Membres ';
                        }
                            $titre = 'Les exploitants group&eacute;s par profession principale';

                        if (mb_strlen($group) < 10) {
                            $group .= ' profession ';
                        } else {
                            $group .= ', profession ';
                        }

                        break;
                    case 'secondaire':
                            $from .= ' INNER JOIN tbl_professions tf ON tf.id=pd.profession_secondaire_id ';
                            $fields['Profession'] = $translator->trans('Profession');
                            $fields['Membres'] = $translator->trans('Membres');

                        if (mb_strlen($select) < 10) {
                            $select .= ' tf.profession AS Profession, Count( expl.id ) AS Membres ';
                        } else {
                            $select .= ', tf.profession AS Profession, Count( expl.id) AS Membres ';
                        }
                            $titre = 'Les exploitants group&eacute;s par profession secondaire';

                        if (mb_strlen($group) < 10) {
                            $group .= ' pd.profession_secondaire_id';
                        } else {
                            $group .= ', pd.profession_secondaire_id';
                        }

                        break;
                }

                break;
            case 'cotisations':
                switch ($Cotisations) {
                    case 'regulierement':
                        $fields['EnOrdre'] = $translator->trans('EnOrdre');
                        $select .= ', Count(c.id) AS EnOrdre ';
                        $from .= ', cotisations c';
                        $where .= " AND peo.id = c.pivot_exploitation_organisation_id AND c.paye_regulierement=TRUE AND c.annee = '{$annee}'";
                        $titre = 'Rapport des exploitations avec ses cotisation en ordre';

                        break;
                    case 'total':
                        $fields['Total'] = $translator->trans('Total');
                        $select .= ', SUM(c.Paye_annee) AS Total ';
                        $from .= ', cotisations c';
                        $where .= " AND peo.id = c.pivot_exploitation_organisation_id AND c.Annee = '{$annee}'";
                        $titre = 'Rapport des exploitations avec le montant de ses cotisations pay&eacute;s';

                        break;
                }

                break;
            case 'Filieres':
                switch ($Filieres) {
                    case 'surface_total':
                            $fields['Filiere_principale'] = $translator->trans('Filiere_principale');
                            $fields['Surface'] = $translator->trans('Surface');
                            $select .= ', f.Filiere_principale, Sum(f.surface) AS Surface';
                            $from .= ', AppBundle:filieres f';
                            $where .= " AND e.ID_exploitation = f.ID_exploitation AND f.Annee = '{$annee}' AND f.Filiere_principale <> '0' ";
                            $group .= ', f.Filiere_principale';
                            $titre = ' Surface total group&eacute; par filiere';

                        break;
                    case 'Production_total':
                            $fields['Filiere_principale'] = $translator->trans('Filiere_principale');
                            $fields['Quantite'] = $translator->trans('Quantite');
                            $select .= ', f.Filiere_principale, Sum( f.Quantite_produit ) AS Quantite';
                            $from .= ', AppBundle:filieres f';
                            $where .= " AND e.ID_exploitation = f.ID_exploitation AND f.Annee = '{$annee}' AND f.Filiere_principale <> '0'";
                            $group .= ', f.Filiere_principale';
                            $titre = ' Production total group&eacute; par filiere';

                        break;
                    case 'surface_production':
                            $fields['Filiere_principale'] = $translator->trans('Filiere_principale');
                            $fields['Surface'] = $translator->trans('Surface');
                            $fields['Quantite'] = $translator->trans('Quantite');
                            $select .= ', f.Filiere_principale, Sum(f.surface) AS Surface, Sum( f.Quantite_produit ) AS Quantite';
                            $from .= ', AppBundle:filieres f';
                            $where .= " AND e.ID_exploitation = f.ID_exploitation AND f.Annee = '{$annee}' AND f.Filiere_principale <> '0'";
                            $group .= ', f.Filiere_principale';
                            $titre = ' Surface et production total group&eacute; par filiere';

                        break;
                    case 'rapport_especial':
                            $extra = 1;
                            $fields['Numero_membre'] = $translator->trans('Numero_membre');
                            $fields['Surface'] = $translator->trans('Surface');
                            $fields['Quantite'] = $translator->trans('Quantite');
                            $fields['Unite_calcul'] = $translator->trans('Unite_calcul');
                            $fields['Commercialise'] = $translator->trans('Commercialise');
                            $fields['Unite_commercialisation'] = $translator->trans('Unite_commercialisation');
                            $select .= ', e.numeroMembre, Sum(f.surface) AS Surface, Sum( f.Quantite_produit ) AS Quantite, f.Unite_calcul';
                            $select .= ', (fc.Quantite_commercialise) AS Commercialise, fc.Unite_commercialisation ';
                            $from .= ', AppBundle:filieres f, AppBundle:filiere_commercialisation fc';
                            $where .= " AND e.ID_exploitation = f.ID_exploitation AND f.ID_filiere=fc.ID_filiere AND f.Annee = '{$annee}'";
                            $where .= " AND f.Filiere_principale='Tomate' AND tc.Code_cooperative IN ('009','010','011','012')";
                            $group .= ', e.ID_exploitation';
                            $titre = " Surface et production total de tomates pour l'ann&eacute;e {$annee}";

                        break;
                }

                break;
            case 'commercialisation':
                switch ($Commercialisation) {
                    case 'warrante':
                        //        $fields['ProduitW'] = $translator->trans('ProduitW');
                            $fields['Membres'] = $translator->trans('Membres');
                            $select .= ', cp.type_commercialisation_id, Count( expl.id ) AS Membres ';
                            $from .= ', commercialisation_produits cp';
                            $where .= " AND expl.id = cp.exploitation_id AND cp.Annee = '{$annee}' AND cp.type_commercialisation_id = 3";
                            $group .= ', cp.type_commercialisation_id';

                        break;
                    case 'groupe':
                        //        $fields['ProduitG'] = $translator->trans('ProduitG');
                            $fields['Membres'] = $translator->trans('Membres');
                            $select .= ', cp.type_commercialisation_id, Count( expl.id ) AS Membres ';
                            $from .= ', commercialisation_produits cp';
                            $where .= " AND expl.id = cp.exploitation_id AND cp.Annee = '{$annee}' AND  cp.type_commercialisation_id =2 ";
                            $group .= ', cp.type_commercialisation_id';

                        break;
                    case 'transforme':
                     //       $fields['ProduitT'] = $translator->trans('ProduitT');
                            $fields['Membres'] = $translator->trans('Membres');
                            $select .= ', cp.type_commercialisation_id, Count( expl.id ) AS Membres ';
                            $from .= ', commercialisation_produits cp';
                            $where .= " AND expl.id = cp.exploitation_id AND cp.Annee = '{$annee}' AND  cp.type_commercialisation_id=1 ";
                            $group .= ', cp.type_commercialisation_id';

                        break;
                }

                break;
            case 'forms_recus':
                switch ($Formations_recus) {
                    case 'tous_operateurs':
                        //    $fields['organisateur'] = $translator->trans('organisateur');
                            $fields['Membres'] = $translator->trans('Membres');
                            $select .= ', Count( expl.id ) AS Membres ';
                            $from .= ', formations_recu fr ';  // INNER JOIN tbl_organisateurs tor ON fr.organisateur_id=tor.id ";

                        if (mb_strlen($where) < 10) {
                            $where .= " expl.id = fr.exploitation_id AND fr.Annee = '{$annee}' ";
                        } else {
                            $where .= " AND expl.id = fr.exploitation_id AND fr.Annee = '{$annee}' ";
                        }

                        break;
                    case 'capad':
                        //    $fields['formation'] = $translator->trans('formation');
                            $fields['Membres'] = $translator->trans('Membres');
                            $select .= ', Count( expl.id ) AS Membres ';
                            $from .= ', formations_recu fr, tbl_organisateurs tblo ';

                        if (mb_strlen($where) < 10) {
                            $where .= " expl.id = fr.exploitation_id and fr.organisateur_id = tblo.id AND fr.Annee = '{$annee}' AND  tblo.organisateur = 'CAPAD'";
                        } else {
                            $where .= " AND expl.id = fr.exploitation_id and fr.organisateur_id = tblo.id AND fr.Annee = '{$annee}' AND  tblo.organisateur = 'CAPAD'";
                        }
                            $group .= '';

                        break;
                    case 'theme':
                            $fields['theme_formation'] = $translator->trans('theme_formation');
                            $fields['Membres'] = $translator->trans('Membres');
                            $select .= ', ttf.theme_formation, Count( expl.id ) AS Membres ';
                            $from .= ', formations_recu fr, tbl_themes_formations ttf ';

                        if (mb_strlen($where) < 10) {
                            $where .= " expl.id = fr.exploitation_id AND fr.Annee = '{$annee}' AND  fr.theme_id = ttf.id ";
                        } else {
                            $where .= " AND expl.id = fr.exploitation_id AND fr.Annee = '{$annee}' AND  fr.theme_id = ttf.id ";
                        }
                            $group .= ', ttf.theme_formation ';

                        break;
                }

                break;
            case 'forms_besoins':
                $fields['theme_formation'] = $translator->trans('theme_formation');
                $fields['Membres'] = $translator->trans('Membres');
                $select .= ', ttf.theme_formation, Count( expl.id ) AS Membres ';
                $from .= ', formations_besoin fb, tbl_themes_formations ttf ';

                if (mb_strlen($where) < 10) {
                    $where .= " expl.id = fb.exploitation_id AND fb.Annee = '{$annee}' AND  fb.formation_id = ttf.id ";
                } else {
                    $where .= " AND expl.id = fb.exploitation_id AND fb.Annee = '{$annee}' AND  fb.formation_id = ttf.id ";
                }
                $group .= ', ttf.theme_formation ';
                $titre = 'Rapport des exploitations groupés par type de formation qui ont besoin';

                break;
            case 'contraintes':
                $fields['contraint'] = $translator->trans('contraint');
                $fields['Membres'] = $translator->trans('Membres');
                $select .= ', tcon.contraint, Count( expl.id ) AS Membres ';
                $from .= ', contraintes_developpement cd, tbl_contraints tcon ';

                if (mb_strlen($where) < 10) {
                    $where .= " expl.id = cd.exploitation_id AND cd.Annee = '{$annee}' AND  cd.contrainte_id = tcon.id ";
                } else {
                    $where .= " AND expl.id = cd.exploitation_id AND cd.Annee = '{$annee}' AND  cd.contrainte_id = tcon.id ";
                }
                $group .= ', tcon.contraint';
                $titre = 'Rapport des exploitations groupés par type de contrainte';

                break;
            case 'services':
                $fields['service'] = $translator->trans('service');
                $fields['Membres'] = $translator->trans('Membres');
                $select .= ', ts.service, Count( expl.id ) AS Membres ';
                $from .= ', services_besoin sb, tbl_services ts ';

                if (mb_strlen($where) < 10) {
                    $where .= " expl.id = sb.exploitation_id AND sb.annee = '{$annee}' AND  sb.service_id = ts.id";
                } else {
                    $where .= " AND expl.id = sb.exploitation_id AND sb.annee = '{$annee}' AND  sb.service_id = ts.id";
                }
                $group .= ', ts.service';
                $titre = 'Rapport des exploitations groupés par service qui ont besoin';

                break;
            case 'habitat':
                $fields['Mur_Pise'] = $translator->trans('Mur_Pise');
                $fields['Mur_Adobe'] = $translator->trans('Mur_Adobe');
                $fields['Mur_Brique'] = $translator->trans('Mur_Brique');
                $fields['Toit_Paille'] = $translator->trans('Toit_Paille');
                $fields['Toit_Tuile'] = $translator->trans('Toit_Tuile');
                $fields['Toit_Tole'] = $translator->trans('Toit_Tole');
                $select .= ",(SELECT Count(expl.id) FROM habitat f0 WHERE f0.`Mur`=1 AND expl.id=f0.exploitation_id AND f0.annee='2015' ) AS Mur_Pise ,
                          (SELECT Count(expl.id) FROM habitat f0 WHERE f0.`Mur`=2 AND expl.id=f0.exploitation_id AND f0.annee='2015' ) AS Mur_Adobe,
                          (SELECT Count(expl.id) FROM habitat f0 WHERE f0.`Mur`=3 AND expl.id=f0.exploitation_id AND f0.annee='2015' ) AS Mur_Brique,
                          (SELECT Count(expl.id) FROM habitat f0 WHERE f0.`Toiture`=1 AND expl.id=f0.exploitation_id AND f0.annee='2015' ) AS Toit_Paille,
                          (SELECT Count(expl.id) FROM habitat f0 WHERE f0.`Toiture`=2 AND expl.id=f0.exploitation_id AND f0.annee='2015' ) AS Toit_Tuile,
                          (SELECT Count(expl.id) FROM habitat f0 WHERE f0.`Toiture`=3 AND expl.id=f0.exploitation_id AND f0.annee='2015' ) AS Toit_Tole ";
                $from .= ', habitat f0';

                if (mb_strlen($where) < 10) {
                    $where .= " expl.id=f0.exploitation_id AND f0.annee='{$annee}'";
                } else {
                    $where .= " AND expl.id=f0.exploitation_id AND f0.annee='{$annee}'";
                }
                $order .= '';
                $titre = 'Rapport des exploitations groupés par type de mur et de toiture dans son habitat';

                break;
        }

        if (mb_strlen($order) < 12) {
            $order = '';
        }
        $strSql = $select . $from . $where . $group . $order;
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
        $recordset = $stmt->fetchAll();
        // render the recordset in a new page? or into the same one?
        return [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $recordset,
            'strSql' => $strSql,
        ];
    }

    private function _getRecordsetF1($form)
    {
        /* lire les parametres receptionnés ** */
        $denomination = $form->get('Denomination')->getData();

        if (null !== $denomination) {
            $coop_id = $denomination->getId();
        }
        $groupecoop = $form->get('groupecoop')->getData();
        $groupegroup = $form->get('groupegroup')->getData();
        $queryTheme = $form->get('queryTheme')->getData();
        $exploitationGroupAnnee = $form->get('exploitationGroupAnnee')->getData();
        $exploitationListAnnee = $form->get('exploitationListAnnee')->getData();
        $annee = $exploitationListAnnee;
        $terres = $form->get('terres')->getData();
        $animaux = $form->get('animaux')->getData();

        $fields = [];
        $select = 'SELECT coop.denomination as cooperative ';
        $from = 'FROM tbl_organisations coop
                    INNER JOIN tbl_organisations grpb ON coop.id = grpb.parent_id
                    INNER JOIN pivot_exploitation_organisations peo ON grpb.id = peo.organisation_id
                    INNER JOIN exploitations expl ON peo.exploitation_id = expl.id ';
        $where = ' WHERE expl.radie=0 ';
        $group = ' GROUP BY ';
        $order = ' ORDER BY ';

        if ($form->get('Denomination')->getData()) {
            /* Une coopérative a été sélectionnée */
            $fields['cooperative'] = $translator->trans('cooperative');
            $fields['groupement'] = $translator->trans('groupement');
            $select .= sprintf(', grpb.denomination as %s ', $fields['groupement']);
            $where .= " AND coop.id = '" . $coop_id . "'";
            $group .= ' groupement';
        } else {
            /* Toutes les coopératives */
            $fields['cooperative'] = $translator->trans('cooperative');
            $select = sprintf('SELECT coop.denomination as %s ', $fields['cooperative']);
            $group .= ' cooperative';
            // }
            if ($groupegroup) {
                $fields['groupement'] = $translator->trans('groupement');
                $select .= sprintf(', grpb.denomination as %s ', $fields['groupement']);

                if (mb_strlen($group) === 0) {
                    $group .= ' GROUP BY groupement';
                } else {
                    $group .= ' , groupement';
                }
            }
        }

        switch ($queryTheme) {
            case 'troupeau':
                switch ($animaux) {
                    case 'membres_elevage':
                        $fields['Espece'] = $translator->trans('Espece');
                        $fields['Membres'] = $translator->trans('Membres');
                        $from .= ' INNER JOIN animaux a ON a.exploitation_id = expl.id '
                            . ' INNER JOIN tbl_cultures tc ON a.espece_id = tc.id ';

                        if (false === $exploitationGroupAnnee) {
                            if (null !== $exploitationListAnnee) {
                                $annee = $exploitationListAnnee;
                                $where .= " AND a.annee = '{$annee}' ";
                            }
                        } else {
                            $fields['annee'] = $translator->trans('annee');
                            $group .= ' , annee ';
                            $select .= ', a.annee as annee ';
                        }
                        $select .= ', tc.culture as Espece, Count(expl.id) AS Membres ';
                        $where .= ' AND tc.elevage=1';
                        $group .= ', Espece';
                        $titre = 'Répartition des membres groupé par elevage';

                        break;
                    case 'membres_produit':
                        $fields['Produit_principal'] = $translator->trans('Produit_principal');
                        $fields['Membres'] = $translator->trans('Membres');
                        $select .= ', tap.production as Produit_principal, Count(expl.id) AS Membres ';
                        $from .= ' INNER JOIN animaux a ON a.exploitation_id = expl.id '
                            . ' INNER JOIN animaux_production ap ON ap.animaux_id = a.id '
                            . ' INNER JOIN tbl_animaux_production tap ON ap.production_id=tap.id ';

                        if (false === $exploitationGroupAnnee) {
                            if (null !== $exploitationListAnnee) {
                                $annee = $exploitationListAnnee;
                                $where .= " AND a.annee = '{$annee}' ";
                            }
                        } else {
                            $fields['annee'] = $translator->trans('annee');
                            $group .= ' , annee ';
                            $select .= ', a.annee as annee ';
                        }
                        $group .= ', Produit_principal';
                        $titre = 'Répartition des membres groupé par produit';

                        break;
                    case 'animaux_elevage':
                        $fields['Type_elevage'] = $translator->trans('Type_elevage');
                        $fields['troupeau'] = $translator->trans('troupeau');
                        $select .= ', tte.type_elevage as Type_elevage, Count(a.id) AS troupeau ';
                        $from .= ' INNER JOIN animaux a ON a.exploitation_id=expl.id '
                            . ' INNER JOIN tbl_types_elevage tte ON a.type_elevage_id=tte.id ';

                        if (false === $exploitationGroupAnnee) {
                            if (null !== $exploitationListAnnee) {
                                $annee = $exploitationListAnnee;
                                $where .= " AND a.annee = '{$annee}' ";
                            }
                        } else {
                            $fields['annee'] = $translator->trans('annee');
                            $group .= ' , annee ';
                            $select .= ', a.annee as annee ';
                        }
                        $group .= ', Type_elevage';
                        $titre = "Animaux groupé par type d'elevage";

                        break;
                }

                break;
            case 'transport':
                $fields['moyen_de_transport'] = $translator->trans('moyen_de_transport');
                $fields['Membres'] = $translator->trans('Membres');
                $select .= ', tmt.moyen_transport as moyen_de_transport, Count( expl.id) AS Membres ';
                $from .= ' INNER JOIN moyens_transport mt ON expl.id = mt.exploitation_id '
                    . 'INNER JOIN tbl_moyen_transport tmt ON mt.type_id = tmt.id ';

                if (false === $exploitationGroupAnnee) {
                    if (null !== $exploitationListAnnee) {
                        $annee = $exploitationListAnnee;
                        $where .= " AND mt.annee = '{$annee}' ";
                    }
                } else {
                    $group .= ' , mt.annee ';
                    $select .= ', mt.annee ';
                }
                $where .= ' AND expl.id = mt.exploitation_id ';
                $group .= ', moyen_de_transport';
                $titre = 'Rapports des moyens de transport des exploitations groupés par coopérative et/ou par groupement et/ou par année';

                break;
            case 'parcelles':
                $from .= ' INNER JOIN parcelles p ON expl.id=p.exploitation_id ';

                switch ($terres) {
                    case 'surface':
                        $fields['Total_surface'] = $translator->trans('Total_surface');
                        $fields['exploitants'] = $translator->trans('exploitants');
                        $fields['Moyenne'] = $translator->trans('Moyenne');
                        $select .= ', SUM(p.surface) AS Total_surface, Count(expl.id) AS exploitants,
                                     ROUND((SUM(p.surface)/Count(expl.id)),2) AS Moyenne ';
                        $titre = 'Rapport des exploitations avec la surface totale et moyenne';

                        break;
                    case 'amendement':
                        $from .= ' INNER JOIN parcelle_historiques ph ON p.id=ph.parcelle_id ';
                        $from .= ' INNER JOIN parcelle_operation_culturale poc ON ph.id=poc.parcelle_historique_id ';
                        $from .= ' INNER JOIN tbl_operation_culturale_type toct ON poc.type_id=toct.id ';

                        $select .= ' , toct.nom as type_amendement, count(poc.id) as nombre ';
                        $where .= ' AND toct.category_id=5 ';

                        if (false === $exploitationGroupAnnee) {
                            if (null !== $exploitationListAnnee) {
                                $annee = $exploitationListAnnee;
                                $where .= " AND ph.annee = '{$annee}' ";
                            }
                        } else {
                            $group .= ' , ph.annee ';
                            $select .= ', ph.annee ';
                            $fields['annee'] = $translator->trans('annee');
                        }

                        $group .= ' , type_amendement ';
                        $fields['type_amendement'] = $translator->trans('type_amendement');
                        $fields['nombre'] = $translator->trans('nombre');
                        $titre = 'Rapport des exploitations groupés par type de amendement';

                        break;
                    case 'surfaceExploitation':
                        $fields['Numero_membre'] = $translator->trans('Numero_membre');
                        $fields['Surface'] = $translator->trans('Surface');
                        $select .= ', expl.numero_membre as Numero_membre, SUM(p.surface) AS Surface ';
                        $where .= " AND p.annee = {$annee}";
                        $group .= ' , expl.id';
                        $titre = "Rapport des exploitations avec la surface pour l'année = {$annee}";

                        break;
                }

                break;
            case 'batiments':
                $fields['batiment'] = $translator->trans('batiment');
                $fields['membres'] = $translator->trans('membres');
                $from .= ' INNER JOIN batiments bat ON expl.id = bat.exploitation_id '
                    . 'INNER JOIN tbl_infrastructures tin ON bat.type_id = tin.id ';
                $select .= ', tin.infrastructure as batiment, count(expl.id) membres ';
                $group .= ', batiment';
                $titre = 'Rapports des batîments des exploitations groupés par coopérative et/ou par groupement et/ou par année';

                break;
            case 'outillages':
                $fields['outillage'] = $translator->trans('outillage');
                $fields['Membres'] = $translator->trans('Membres');
                $select .= ', touti.equipement_agricole as outillage, Count( expl.id) AS Membres ';
                $from .= ' INNER JOIN outillage outi ON expl.id = outi.exploitation_id '
                    . 'INNER JOIN tbl_equipements_agricoles touti ON outi.type_id = touti.id ';

                if (false === $exploitationGroupAnnee) {
                    if (null !== $exploitationListAnnee) {
                        $annee = $exploitationListAnnee;
                        $where .= " AND outi.annee = '{$annee}' ";
                    }
                } else {
                    $fields['annee'] = $translator->trans('annee');
                    $group .= ' , annee ';
                    $select .= ', outi.annee as annee ';
                }
                $where .= ' AND expl.id = outi.exploitation_id ';
                $group .= ', outillage';
                $titre = 'Rapports des outillages des exploitations groupés par coopérative et/ou par groupement et/ou par année';

                break;
        }

        if (mb_strlen($where) < 10) {
            $where = '';
        }
        $strSql = $select . $from . $where . $group;
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
        $recordset = $stmt->fetchAll();
        // render the recordset in a new page? or into the same one?
        return [
            'fields' => $fields,
            'recordset' => $recordset,
            'strSql' => $strSql,
            'titre' => $titre,
        ];
    }

    private function _getRecordsetSignaletique($form)
    {
        $denomination = $form->get('denomination')->getData();
        $groupecoop = $form->get('groupecoop')->getData();
        $groupegroup = $form->get('groupegroup')->getData();
        $queryTheme = $form->get('queryTheme')->getData();
        $exploitationGroupAnnee = $form->get('exploitationGroupAnnee')->getData();
        $exploitationListAnnee = $form->get('exploitationListAnnee')->getData();
        $exploitantTheme = $form->get('exploitantTheme')->getData();
        $exploitantAgeMin = $form->get('exploitantAgeMin')->getData();
        $exploitantAgeMax = $form->get('exploitantAgeMax')->getData();
        $exploitantEtatCivilGroup = $form->get('exploitantEtatCivilGroup')->getData();
        $exploitantEtatCivilList = $form->get('exploitantEtatCivilList')->getData();
        $exploitantGenreGroup = $form->get('exploitantGenreGroup')->getData();
        $exploitantGenreList = $form->get('exploitantGenreList')->getData();
        $chargesTheme = $form->get('chargesTheme')->getData();
        $chargesNombreGroup = $form->get('chargesNombreGroup')->getData();
        $chargesNombreMin = $form->get('chargesNombreMin')->getData();
        $chargesNombreMax = $form->get('chargesNombreMax')->getData();
        $chargesScolariseFils = $form->get('chargesScolariseFils')->getData();
        $chargesScolariseFilles = $form->get('chargesScolariseFilles')->getData();

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery();
        /* pour afficher les en-tête de colonne */
        $fields = [];
        $titre = 'Résultat de la requête';
        $from = ' FROM tbl_organisations coop';
        $from .= ' INNER JOIN tbl_organisations grp ON coop.id = grp.parent_id';
        $from .= ' INNER JOIN pivot_exploitation_organisations peo ON grp.id = peo.organisation_id ';
        $from .= ' INNER JOIN exploitations expl ON peo.exploitation_id = expl.id  ';
        $where = ' WHERE expl.radie=0 ';
        $group = '';
        $order = '';

        if ($form->get('denomination')->getData()) {
            /* Une coopérative a été sélectionnée */
            $select = 'SELECT coop.denomination as cooperative';
            $group = ' GROUP BY cooperative';
            $where .= ' AND coop.id = :id';

            if (!empty($groupegroup)) {
                $fields['groupement'] = $translator->trans('groupement');
                $select .= sprintf(' , grp.denomination as %s ', $fields['groupement']);
                $group .= ' , groupement';
            }

            $query->setParameter('id', $denomination->getId());

            if ($groupecoop) {
                $fields['cooperative'] = $translator->trans('cooperative');
                $pos = mb_strpos($group, 'cooperative');

                if (false === $pos) {
                    $group .= ' , cooperative';
                }
            }
            /* NB pas possible de ne pas grouper par groupement pour ce choix */
        } else {
            /* Toutes les coopératives */
            $fields['cooperative'] = $translator->trans('cooperative');
            $select = sprintf('SELECT coop.denomination as %s ', $fields['cooperative']);
            /* NB ceci doit toujours être activé dans ce cas de figure */
            $pos = mb_strpos($group, 'cooperative');

            if (false === $pos) {
                $group .= ' GROUP BY cooperative';
            }
            $order = ' ORDER BY cooperative ';
            // }
            if ($groupegroup) {
                $fields['groupement'] = $translator->trans('groupement');
                $select .= sprintf(', grp.denomination as %s ', $fields['groupement']);

                if (mb_strlen($group) === 0) {
                    $group .= ' GROUP BY groupement';
                } else {
                    $group .= ' , groupement';
                }
                $order .= ' , groupement ';
            }
        }

        switch ($queryTheme) {
            case 'exploitation':
                $fields['annee_adhesion'] = $translator->trans('annee_adhesion');
                $select .= sprintf(', expl.annee_adhesion as %s ', $fields['annee_adhesion']);
                $fields['membres'] = $translator->trans('membres');
                $select .= sprintf(', COUNT(expl.id) AS %s ', $fields['membres']);
                $group .= ', expl.annee_adhesion';

                if ($exploitationGroupAnnee) {
                    $where .= ' AND (expl.annee_adhesion < ' . date('Y');
                    $where .= ' AND expl.annee_adhesion > 2000)';
                    $titre = $translator->trans('Rapport par coopérative (' . $denomination . ') des exploitations groupés par année d\'adhesion (2000 à 2018)' . $groupegroup);
                } else {
                    $where .= ' AND expl.annee_adhesion= :exploitationListAnnee ';
                    $where .= " AND (expl.annee_adhesion <> '' ";
                    $where .= ' AND expl.annee_adhesion IS NOT NULL)';
                    $query->setParameter('exploitationListAnnee', $exploitationListAnnee);
                    $titre = sprintf('%s %d', $translator->trans('Rapport par coopérative(' . $denomination . ') des exploitations qui ont adheré l\'année '), $exploitationListAnnee);
                }

                break;
            case 'exploitant':
                switch ($exploitantTheme) {
                    case 'age':
                        $annCour = date('Y-m-d');
                        $fields['membres'] = $translator->trans('membres');
                        $select .= sprintf(', COUNT(expl.id) AS %s ', $fields['membres']);
                        $from .= ' INNER JOIN AppBundle:Personnes pers WITH expl.id = pers.exploitation';
                        $where .= " AND pers.rolFamille = '" . Personnes::role_chefDeFamille . "' ";
                        $where .= ' AND pers.dateNaissance IS NOT NULL ';
                        $where .= " AND pers.dateNaissance <> ''";

                        if (0 === $exploitantAgeMin && 0 < $exploitantAgeMax) {
                            /* mineur */
                            $annNais = strtotime($annCour . '- ' . $exploitantAgeMax . ' years');
                            $dateNaiss = date('Y-m-d', $annNais);
                            $where .= " AND pers.dateNaissance > '" . $dateNaiss . "'";
                            $titre = sprintf('%s %d %s', $translator->trans('Nombre d\'exploitants qui ont moins de'), $exploitantAgeMax, $translator->trans('ans'));
                        } elseif (0 < $exploitantAgeMin && 0 === $exploitantAgeMax) {
                            /* majeur */
                            $annNais = strtotime($annCour . '- ' . $exploitantAgeMin . ' years');
                            $dateNaiss = date('Y-m-d', $annNais);
                            $where .= " AND pers.dateNaissance < '" . $dateNaiss . "'";
                            $titre = sprintf('%s %d %s', $translator->trans('Nombre d\'exploitants qui ont plus de'), $exploitantAgeMin, $translator->trans('ans'));
                        } else {
                            /* entre */
                            if ($exploitantAgeMin < $exploitantAgeMax) {
                                $annMay = strtotime($annCour . '- ' . $exploitantAgeMin . ' years');
                                $annMay = date('Y-m-d', $annMay);
                                $annMin = strtotime($annCour . '- ' . $exploitantAgeMax . ' years');
                                $annMin = date('Y-m-d', $annMin);
                            } else {
                                $annMay = strtotime($annCour . '- ' . $exploitantAgeMax . ' years');
                                $annMay = date('Y-m-d', $annMay);
                                $annMin = strtotime($annCour . '- ' . $exploitantAgeMin . ' years');
                                $annMin = date('Y-m-d', $annMin);
                            }
                            $where .= " AND (pers.dateNaissance BETWEEN '" . $annMin . "' AND '" . $annMay . "') ";
                            $titre = sprintf('%s %d %s %d %s', $translator->trans('Nombre d\'exploitants qui sont entre les'), $exploitantAgeMin, $translator->trans('et'), $exploitantAgeMax, $translator->trans('ans'));
                        }

                        break;
                    case 'etat civil':
                        $fields['etat_civil'] = $translator->trans('etat_civil');
                        $select .= sprintf(', COALESCE(etciv.etatCivil, :undef) AS %s ', $fields['etat_civil']);
                        $fields['membres'] = $translator->trans('membres');
                        $select .= sprintf(', COUNT(expl.id) AS %s ', $fields['membres']);
                        $from .= ' LEFT OUTER JOIN AppBundle:personnes pers WITH expl.id = pers.exploitation';
                        $from .= ' LEFT OUTER JOIN AppBundle:TblEtatCivil etciv WITH pers.etatCivil = etciv.id';
                        $group .= ', etciv.etatCivil';
                        $query->setParameter('undef', $translator->trans('indéfini'));

                        if ($exploitantEtatCivilGroup) {
                            $titre = $translator->trans('Nombre d\'exploitants groupé par état civil par coopérative');
                        } else {
                            $where .= ' AND etciv.id = :etatcivilid';
                            $query->setParameter('etatcivilid', $exploitantEtatCivilList->getId());
                            $titre = sprintf('%s %s %s', $translator->trans('Nombre d\'exploitants qui sont'), $exploitantEtatCivilList->getEtatCivil(), $translator->trans('(par coopérative)'));
                        }

                        break;
                    case 'genre':
                        $fields['genre'] = $translator->trans('genre');
                        $select .= sprintf(', pers.sexe AS %s ', $fields['genre']);
                        $fields['membres'] = $translator->trans('membres');
                        $select .= sprintf(', COUNT(expl.id) AS %s ', $fields['membres']);
                        $from .= ' INNER JOIN AppBundle:Personnes pers WITH expl.id = pers.exploitation';
                        $group .= ', pers.sexe';

                        if ($exploitantGenreGroup) {
                            $titre = $translator->trans('Exploitations groupées par genre');
                        } else {
                            $where .= sprintf(" AND pers.sexe like '%s'", mb_substr($exploitantGenreList, 0, 1) . '%');
                            $titre = sprintf('%s %s', $translator->trans('Producteurs du genre'), $exploitantGenreList);
                        }

                        break;
                }

                break;
            case 'charges':
                switch ($chargesTheme) {
                    case 'nombre':
                        if ($chargesNombreGroup) {
                            $fields['membres_menage'] = $translator->trans('membres_menage');
                            $select .= sprintf(', expl.membres_menage AS %s ', $fields['membres_menage']);
                            $fields['membres'] = $translator->trans('membres');
                            $select .= sprintf(', COUNT(expl.id) AS %s ', $fields['membres']);
                            $group .= ', expl.membres_menage';
                            $titre = $translator->trans('Exploitations groupées par quantité de personnes dans le menage');
                        } else {
                            $fields['membres'] = $translator->trans('membres');
                            $select .= sprintf(', COUNT(expl.id) AS %s ', $fields['membres']);

                            if (0 < $chargesNombreMin && 0 === $chargesNombreMax) {
                                $where .= ' AND expl.membres_menage > ' . $chargesNombreMin;
                                $titre = sprintf('%s %d %s', $translator->trans('Exploitations avec plus de'), $chargesNombreMin, $translator->trans('personnes à charge'));
                            } elseif (0 === $chargesNombreMin && 0 < $chargesNombreMax) {
                                $where .= ' AND expl.membres_menage < :nbpersacharge';
                                $query->setParameter('nbpersacharge', $chargesNombreMax);
                                $titre = sprintf('%s %d %s', $translator->trans('Exploitations avec moins de'), $chargesNombreMax, $translator->trans('personnes à charge'));
                            } elseif (0 < $chargesNombreMin && 0 < $chargesNombreMax) {
                                /* entre */
                                if ($chargesNombreMin < $chargesNombreMax) {
                                    $nbMax = $chargesNombreMax;
                                    $nbMin = $chargesNombreMin;
                                } else {
                                    $nbMax = $chargesNombreMin;
                                    $nbMin = $chargesNombreMax;
                                }
                                $where .= ' AND (expl.totalMenage BETWEEN :nbMin AND :nbMax)';
                                $query->setParameter('nbMax', $nbMax);
                                $query->setParameter('nbMin', $nbMin);
                                $titre = sprintf('%s %d %s %d %s', $translator->trans('Exploitations ayant entre'), $chargesNombreMin, $translator->trans('et'), $chargesNombreMax, $translator->trans('personnes à charge'));
                            }
                        }

                        break;
                    case 'scolarise':
                        $titre = '';

                        if ($chargesScolariseFils) {
                            $fields['fils'] = $translator->trans('fils');
                            $select .= sprintf(', SUM(expl.nombreFils) AS %s ', $fields['fils']);
                            $fields['fils_scolarises'] = $translator->trans('fils_scolarises');
                            $select .= sprintf(', SUM(expl.filsScolarises) AS %s ', $fields['fils_scolarises']);
                            $titre .= $translator->trans('Total de fils, et pourcentage de fils scolarisés');
                        }

                        if ($chargesScolariseFilles) {
                            $fields['filles'] = $translator->trans('filles');
                            $select .= sprintf(', SUM(expl.nombreFilles) AS %s ', $fields['filles']);
                            $fields['filles_scolarises'] = $translator->trans('filles_scolarises');
                            $select .= sprintf(', SUM(expl.fillesScolarisess) AS %s ', $fields['filles_scolarises']);

                            if (mb_strlen($titre) > 0) {
                                $titre .= ' | ';
                            }
                            $titre .= $translator->trans('Total de filles, et pourcentage de filles scolarisées');
                        }

                        break;
                }

                break;
        }

        $strSql = $select . $from . $where . $group . $order;
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
        $recordset = $stmt->fetchAll();

        return $result = [
            'fields' => $fields,
            'titre' => $titre,
            'strSql' => $strSql,
            'recordset' => $recordset,
        ];

        return $result;
    }
}
