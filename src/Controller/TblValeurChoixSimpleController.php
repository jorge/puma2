<?php

declare(strict_types=1);

/*
 * Copyright (C) 2020, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblValeurChoixSimple;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 *  controller.
 *
 * @Route("/{_locale}/references/valeur/choix")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblValeurChoixSimpleController extends AbstractController
{
    /**
     * Deletes a TblValeurChoixSimple entity.
     *
     * @Route("/{id}", name="references_tblvaleurchoixsimple_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblValeurChoixSimple $TblValeurChoixSimple)
    {
        $form = $this->createDeleteForm($TblValeurChoixSimple);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($TblValeurChoixSimple);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblvaleurchoixsimple_index');
    }

    /**
     * Displays a form to edit an existing TblValeurChoixSimple entity.
     *
     * @Route("/{id}/edit", name="references_tblvaleurchoixsimple_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblValeurChoixSimple $TblValeurChoixSimple)
    {
        $deleteForm = $this->createDeleteForm($TblValeurChoixSimple);
        $editForm = $this->createForm('App\Form\TblValeurChoixSimpleType', $TblValeurChoixSimple);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($TblValeurChoixSimple);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblvaleurchoixsimple_index', ['id' => $TblValeurChoixSimple->getId()]);
        }

        return $this->render('tblvaleurchoixsimple/edit.html.twig', [
            'TblValeurChoixSimple' => $TblValeurChoixSimple,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblValeurChoixSimple entities.
     *
     * @Route("/", name="references_tblvaleurchoixsimple_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $TblValeurChoixSimples = $em->getRepository(TblValeurChoixSimple::class)->findAll();

        return $this->render('tblvaleurchoixsimple/index.html.twig', [
            'entities' => $TblValeurChoixSimples,
        ]);
    }

    /**
     * Creates a new TblValeurChoixSimple entity.
     *
     * @Route("/new", name="references_tblvaleurchoixsimple_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $TblValeurChoixSimple = new TblValeurChoixSimple($user);
        $form = $this->createForm('App\Form\TblValeurChoixSimpleType', $TblValeurChoixSimple);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($TblValeurChoixSimple);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblvaleurchoixsimple_index', ['id' => $TblValeurChoixSimple->getId()]);
        }

        return $this->render('tblvaleurchoixsimple/new.html.twig', [
            'TblValeurChoixSimple' => $TblValeurChoixSimple,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblValeurChoixSimple entity.
     *
     * @param TblValeurChoixSimple $TblValeurChoixSimple The TblValeurChoixSimple entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblValeurChoixSimple $TblValeurChoixSimple)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblvaleurchoixsimple_delete', ['id' => $TblValeurChoixSimple->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
