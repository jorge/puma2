<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\SemenceDemandee;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * SemenceDemandee controller.
 *
 * @Route("/{_locale}/references/semencedemandee")
 */

use Symfony\Component\Routing\Annotation\Route;

final class SemenceDemandeeController extends AbstractController
{
    /**
     * Deletes a SemenceDemandee entity.
     *
     * @Route("/{id}", name="references_semencedemandee_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, SemenceDemandee $semenceDemandee)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $form = $this->createDeleteForm($semenceDemandee, [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $semenceDemandee->setRadie(TRUE);
            $em->persist($semenceDemandee);
            $em->flush();
        }

        return $this->redirectToRoute('references_semencedemandee_index', [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing SemenceDemandee entity.
     *
     * @Route("/{id}/edit", name="references_semencedemandee_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, SemenceDemandee $semenceDemandee)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $deleteForm = $this->createDeleteForm($semenceDemandee);
        $editForm = $this->createForm('App\Form\SemenceDemandeeType', $semenceDemandee);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($semenceDemandee);
            $em->flush();

            return $this->redirectToRoute('references_semencedemandee_edit', ['id' => $semenceDemandee->getId()]);
        }

        return $this->render('semencedemandee/edit.html.twig', [
            'semenceDemandee' => $semenceDemandee,
            'edit_form' => $editForm->createView(),
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all SemenceDemandee entities.
     *
     * @Route("/", name="references_semencedemandee_index", methods={"GET"})
     */
    public function indexAction()
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $em = $this->getDoctrine()->getManager();

        $semenceDemandees = $em->getRepository(SemenceDemandee::class)->findAll();

        return $this->render('semencedemandee/index.html.twig', [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'semenceDemandees' => $semenceDemandees,
        ]);
    }

    /**
     * Creates a new SemenceDemandee entity.
     *
     * @Route("/new", name="references_semencedemandee_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $user = $this->getUser();
        $semenceDemandee = new SemenceDemandee($user);
        $form = $this->createForm('App\Form\SemenceDemandeeType', $semenceDemandee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($semenceDemandee);
            $em->flush();

            return $this->redirectToRoute('references_semencedemandee_show', ['id' => $semenceDemandee->getId()]);
        }

        return $this->render('semencedemandee/new.html.twig', [
            'semenceDemandee' => $semenceDemandee,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a SemenceDemandee entity.
     *
     * @Route("/{id}", name="references_semencedemandee_show", methods={"GET"})
     */
    public function showAction(SemenceDemandee $semenceDemandee)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $deleteForm = $this->createDeleteForm($semenceDemandee);

        return $this->render('semencedemandee/show.html.twig', [
            'semenceDemandee' => $semenceDemandee,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a SemenceDemandee entity.
     *
     * @param SemenceDemandee $semenceDemandee The SemenceDemandee entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SemenceDemandee $semenceDemandee, $options = [])
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'references_semencedemandee_delete',
                ['id' => $semenceDemandee->getId(),
                    'coop_id' => $coop_id,
                    'group_id' => $group_id,
                    'exploitation_id' => $exploitation_id,
                    'annee' => $annee,
                ]
            ))
            ->setMethod('DELETE')
            ->getForm();
    }
}
