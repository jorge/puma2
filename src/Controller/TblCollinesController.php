<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblCollines;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblCollines controller.
 *
 * @Route("/{_locale}/references/tblcollines")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblCollinesController extends AbstractController
{
    /**
     * Deletes a TblCollines entity.
     *
     * @Route("/{id}", name="references_tblcollines_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblCollines $tblColline)
    {
        $form = $this->createDeleteForm($tblColline);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblColline);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblcollines_index');
    }

    /**
     * Displays a form to edit an existing TblCollines entity.
     *
     * @Route("/{id}/edit", name="references_tblcollines_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblCollines $tblColline)
    {
        $deleteForm = $this->createDeleteForm($tblColline);
        $editForm = $this->createForm('App\Form\TblCollinesType', $tblColline);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblColline);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcollines_index', ['id' => $tblColline->getId()]);
        }

        return $this->render('tblcollines/edit.html.twig', [
            'tblColline' => $tblColline,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblCollines entities.
     *
     * @Route("/", name="references_tblcollines_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblCollines = $em->getRepository(TblCollines::class)->findAll();

        return $this->render('tblcollines/index.html.twig', [
            'tblCollines' => $tblCollines,
        ]);
    }

    /**
     * Creates a new TblCollines entity.
     *
     * @Route("/new", name="references_tblcollines_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblColline = new TblCollines($user);
        $form = $this->createForm('App\Form\TblCollinesType', $tblColline);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblColline);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcollines_index', ['id' => $tblColline->getId()]);
        }

        return $this->render('tblcollines/new.html.twig', [
            'tblColline' => $tblColline,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblCollines entity.
     *
     * @Route("/{id}", name="references_tblcollines_show", methods={"GET"})
     */
    public function showAction(TblCollines $tblColline)
    {
        $deleteForm = $this->createDeleteForm($tblColline);

        return $this->render('tblcollines/show.html.twig', [
            'tblColline' => $tblColline,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblCollines entity.
     *
     * @param TblCollines $tblColline The TblCollines entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblCollines $tblColline)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblcollines_delete', ['id' => $tblColline->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
