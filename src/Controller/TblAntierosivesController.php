<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblAntierosives;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblAntierosives controller.
 *
 * @Route("/{_locale}/references/tblantierosives")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblAntierosivesController extends AbstractController
{
    /**
     * Deletes a TblAntierosives entity.
     *
     * @Route("/{id}", name="references_tblantierosives_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblAntierosives $tblAntierosive)
    {
        $form = $this->createDeleteForm($tblAntierosive);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblAntierosive);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblantierosives_index');
    }

    /**
     * Displays a form to edit an existing TblAntierosives entity.
     *
     * @Route("/{id}/edit", name="references_tblantierosives_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblAntierosives $tblAntierosive)
    {
        $deleteForm = $this->createDeleteForm($tblAntierosive);
        $editForm = $this->createForm('App\Form\TblAntierosivesType', $tblAntierosive);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAntierosive);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblantierosives_index', ['id' => $tblAntierosive->getId()]);
        }

        return $this->render('tblantierosives/edit.html.twig', [
            'tblAntierosive' => $tblAntierosive,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblAntierosives entities.
     *
     * @Route("/", name="references_tblantierosives_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblAntierosives = $em->getRepository(TblAntierosives::class)->findAll();

        return $this->render('tblantierosives/index.html.twig', [
            'tblAntierosives' => $tblAntierosives,
        ]);
    }

    /**
     * Creates a new TblAntierosives entity.
     *
     * @Route("/new", name="references_tblantierosives_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblAntierosive = new TblAntierosives($user);
        $form = $this->createForm('App\Form\TblAntierosivesType', $tblAntierosive);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAntierosive);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblantierosives_index', ['id' => $tblAntierosive->getId()]);
        }

        return $this->render('tblantierosives/new.html.twig', [
            'tblAntierosive' => $tblAntierosive,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblAntierosives entity.
     *
     * @Route("/{id}", name="references_tblantierosives_show", methods={"GET"})
     */
    public function showAction(TblAntierosives $tblAntierosive)
    {
        $deleteForm = $this->createDeleteForm($tblAntierosive);

        return $this->render('tblantierosives/show.html.twig', [
            'tblAntierosive' => $tblAntierosive,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblAntierosives entity.
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblAntierosives $tblAntierosive)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblantierosives_delete', ['id' => $tblAntierosive->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
