<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\Parcelles;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Parcelles controller.
 *
 * @Route("/{_locale}/references/parcelles")
 */

use Symfony\Component\Routing\Annotation\Route;

final class ParcellesController extends AbstractController
{
    /**
     * Deletes a Parcelles entity.
     *
     * @Route("/delete/{parcelle}/{annee}", name="references_parcelles_delete", methods={"DELETE"})
     *
     * @param mixed $annee
     */
    public function deleteAction(Request $request, Parcelles $parcelle, $annee)
    {
        $exploitation = $parcelle->getExploitation();

        $form = $this->createDeleteForm($parcelle, $annee);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $parcelle->setRadie(TRUE);
            $em->persist($parcelle);
            $em->flush();
            $this->radierBrancheParcelle($parcelle->getId(), 1, $em);
        }

        return $this->redirectToRoute('references_parcelles_index', [
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation->getId(),
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing Parcelles entity.
     *
     * @Route("/edit/{parcelle}/{annee}", name="references_parcelles_edit", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function editAction(Request $request, Parcelles $parcelle, $annee)
    {
        $redirect = $request->get('redirect');

        $deleteForm = $this->createDeleteForm($parcelle, $annee);

        $options = [];

        if ($redirect) {
            $options['action'] = '?redirect=' . $redirect;
        }

        $editForm = $this->createForm('App\Form\ParcellesType', $parcelle, $options);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parcelle);
            $em->flush();

            if ('f1' === $redirect) {
                return $this->redirectToRoute('saisie_f1_membre', [
                    'id' => $parcelle->getExploitation()->getId(),
                    'annee' => $annee,
                ]);
            }

            return $this->redirectToRoute('references_parcelles_index', [
                'exploitation_id' => $parcelle->getExploitation()->getId(),
                'exploitation' => $parcelle->getExploitation()->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('parcelles/edit.html.twig', [
            'parcelle' => $parcelle,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'exploitation' => $parcelle->getExploitation(),
            'annee' => $annee,
        ]);
    }

    /**
     * Lists all Parcelles entities.
     *
     * @Route("/{exploitation}/{annee}", name="references_parcelles_index", methods={"GET"})
     *
     * Attention pas d'annne pour les parcelles
     *
     * @param mixed $annee
     */
    public function indexAction(Request $request, Exploitations $exploitation, int $annee)
    {
        $em = $this->getDoctrine()->getManager();
        $parcelles = $exploitation->getParcellesFor($annee);

        return $this->render('parcelles/index.html.twig', [
            'parcelles' => $parcelles,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
    }

    /**
     * Creates a new Parcelles entity.
     *
     * @Route("/{exploitation}/{annee}/new", name="references_parcelles_new", methods={"GET", "POST"})
     * @Route("/{exploitation}/{annee}/new/{redirect}", name="references_parcelles_new_redirect", methods={"GET", "POST"})
     *
     * @param mixed|null $redirect
     */
    public function newAction(Request $request, Exploitations $exploitation, int $annee, $redirect = null, bool $planification = false)
    {
        $redirect = $request->get('redirect');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $parcelle = new Parcelles($user);
        $parcelle->setExploitation($exploitation);
        $parcelle->setAnnee($annee);
        $parcelle->setPlanification($planification);
        $options = [];

        $form = $this->createForm('App\Form\ParcellesType', $parcelle, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $parcelle->setRadie(FALSE);
            $em->persist($parcelle);
            $em->flush();

            if ('f1' === $redirect) {
                return $this->redirectToRoute('saisie_f1_membre', [
                    'id' => $exploitation->getId(),
                    'annee' => $annee,
                ]);
            }

            if ($planification) { // then return vers planSaison (f6). Then redirection = ref vers plan saison (F6)
                return $this->redirectToRoute('references_exploitationplansaison_parcelles4plan', [
                    'id' => $redirect,
                ]);
            }

            return $this->redirectToRoute('references_parcelles_index', [
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('parcelles/new.html.twig', [
            'parcelle' => $parcelle,
            'exploitation' => $exploitation,
            'form' => $form->createView(),
            'annee' => $annee,
        ]);
    }

    public function radierBrancheParcelle($parcelle_id, $status, $em)
    {
        $select = "SELECT ph.id FROM parcelle_historiques ph WHERE ph.parcelle_id='" . $parcelle_id . "'";
        /* historique_cultures */
        $strSql = 'UPDATE historique_cultures SET radie=' . $status . '
					  WHERE parcelle_historique_id in (' . $select . ');';
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* parcelle_historiques */
        $strSql = 'UPDATE parcelle_historiques SET radie=' . $status . "
                  WHERE parcelle_id = '" . $parcelle_id . "';";
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* parcelle_amendements */
//          $strSql = "UPDATE parcelle_amendements SET radie=" . $status . "
//                    WHERE parcelle_historique_id in (" .$select . ");";
//          $em = $this->getDoctrine()->getManager();
//          $stmt = $em->getConnection()->prepare($strSql);
//          $stmt->execute();

            /* parcelle_fumure_min */
//          $strSql = "UPDATE parcelle_fumure_min SET radie=" . $status . "
//                    WHERE parcelle_historique_id in (" .$select . ");";
//          $em = $this->getDoctrine()->getManager();
//          $stmt = $em->getConnection()->prepare($strSql);
//          $stmt->execute();

            /* parcelle_fumure_org */
//          $strSql = "UPDATE parcelle_fumure_org SET radie=" . $status . "
//                    WHERE parcelle_historique_id in (" .$select . ");";
//          $em = $this->getDoctrine()->getManager();
//          $stmt = $em->getConnection()->prepare($strSql);
//          $stmt->execute();

            /* parcelle_herbicide */
//          $strSql = "UPDATE parcelle_herbicide SET radie=" . $status . "
//                    WHERE parcelle_historique_id in (" .$select . ");";
//          $em = $this->getDoctrine()->getManager();
//          $stmt = $em->getConnection()->prepare($strSql);
//          $stmt->execute();

            /* parcelle_phytosanitaire */
//          $strSql = "UPDATE parcelle_phytosanitaire SET radie=" . $status . "
//                    WHERE parcelle_historique_id in (" .$select . ");";
//          $em = $this->getDoctrine()->getManager();
//          $stmt = $em->getConnection()->prepare($strSql);
//          $stmt->execute();

            /* parcelle_recoltes */
//          $strSql = "UPDATE parcelle_recoltes SET radie=" . $status . "
//                    WHERE parcelle_historique_id in (" .$select . ");";
//          $em = $this->getDoctrine()->getManager();
//          $stmt = $em->getConnection()->prepare($strSql);
//          $stmt->execute();

            /* parcelle_semences */
//          $strSql = "UPDATE parcelle_semences SET radie=" . $status . "
//                    WHERE parcelle_historique_id in (" .$select . ");";
//          $em = $this->getDoctrine()->getManager();
//          $stmt = $em->getConnection()->prepare($strSql);
//          $stmt->execute();

            /* parcelle_techniques */
//          $strSql = "UPDATE parcelle_techniques SET radie=" . $status . "
//                    WHERE parcelle_historique_id in (" .$select . ");";
//          $em = $this->getDoctrine()->getManager();
//          $stmt = $em->getConnection()->prepare($strSql);
//          $stmt->execute();
    }

    /**
     * Finds and displays a Parcelles entity.
     *
     * @Route("/show/{parcelle}/{annee}", name="references_parcelles_show", methods={"GET"})
     * @Route("/show/{parcelle}/{annee}/hide_history/{hideHistory}", name="references_parcelles_show_without_history", methods={"GET"})
     *
     * @var int at 1 if you do not want to display the history
     *
     * @param mixed $annee
     */
    public function showAction(Request $request, Parcelles $parcelle, $annee, ?int $hideHistory = null)
    {
        $exploitation = $parcelle->getExploitation();
        $deleteForm = $this->createDeleteForm($parcelle, $annee);

        $em = $this->getDoctrine()->getManager();
        $parcelleHistoriques = $em->getRepository(ParcelleHistoriques::class)
            ->findBy(['parcelle' => $parcelle, 'radie' => 0, 'annee' => $annee]);

        return $this->render('parcelles/show.html.twig', [
            'parcelle' => $parcelle,
            'parcelleHistoriques' => $parcelleHistoriques,
            'delete_form' => $deleteForm->createView(),
            'annee' => $annee,
            'exploitation' => $exploitation,
            'hide_history' => $hideHistory,
        ]);
    }

    /**
     * Creates a form to delete a Parcelles entity.
     *
     * @param Parcelles $parcelle
     *          The Parcelles entity
     * @param mixed $annee
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Parcelles $parcelle, $annee)
    {
        return $this->createFormBuilder()->setAction($this->generateUrl(
            'references_parcelles_delete',
            [
                'parcelle' => $parcelle->getId(),
                'annee' => $annee,
            ]
        ))->setMethod('DELETE')->getForm();
    }
}
