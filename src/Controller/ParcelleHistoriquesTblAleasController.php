<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\ParcelleHistoriques;
use App\Entity\Exploitations;
use App\Entity\PivotExploitationOrganisations;
use App\Form\ParcelleHistoriquesTblAleas;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * ParcelleHistoriquesTblAleas controller.
 *
 * @Route("/{_locale}/references_parcellehistoriquestblaleas")
 */

use Symfony\Component\Routing\Annotation\Route;

final class ParcelleHistoriquesTblAleasController extends AbstractController
{
    /**
     * Deletes a ParcelleHistoriquesTblAleas entity.
     *
     * @Route("/{id}", name="references_parcellehistoriquestblaleas_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, \App\Entity\ParcelleHistoriquesTblAleas $parcelleHistoriqueTblAleas)
    {
        $em = $this->getDoctrine()->getManager();

        $parcelle = $parcelleHistoriqueTblAleas->getParcelleHistorique()->getParcelle();
        $exploitation = $parcelle->getExploitation();
        $annee = $parcelleHistoriqueTblAleas->getParcelleHistorique()->getAnnee();
        $form = $this->createDeleteForm($parcelleHistoriqueTblAleas);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($parcelleHistoriqueTblAleas);
            $em->flush();
        }

        return $this->redirectToRoute('references_parcellehistoriques_show', [
            'annee' => $annee,
            'id' => $parcelleHistoriqueTblAleas->getParcelleHistorique()->getId(),
            'parcelleHistorique' => $parcelleHistoriqueTblAleas->getParcelleHistorique(),
        ]);
    }

    /**
     * Displays a form to edit an existing ParcelleHistoriquesTblAleas entity.
     *
     * @Route("/{id}/edit", name="references_parcellehistoriquestblaleas_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, \App\Entity\ParcelleHistoriquesTblAleas $parcelleHistoriqueTblAleas)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $parcelleHistoriqueTblAleas->getParcelleHistorique()->getParcelle()->getExploitation();
        $annee = $parcelleHistoriqueTblAleas->getParcelleHistorique()->getAnnee();
        $deleteForm = $this->createDeleteForm($parcelleHistoriqueTblAleas);
        $editForm = $this->createForm('App\Form\ParcelleHistoriquesTblAleasType', $parcelleHistoriqueTblAleas);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($parcelleHistoriqueTblAleas);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $parcelleHistoriqueTblAleas->getParcelleHistorique()->getId(),
            ]);
        }

        return $this->render('parcellehistoriquestblaleas/edit.html.twig', [
            'parcelleHistoriqueTblAleas' => $parcelleHistoriqueTblAleas,
            'edit_form' => $editForm->createView(),
            'exploitation' => $exploitation,
            'annee' => $annee,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new ParcelleHistoriquesTblAleas entity.
     *
     * @Route("/new/{historique}", name="references_parcellehistoriquestblaleas_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, ParcelleHistoriques $historique)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $historique->getParcelle()->getExploitation();
        $annee = $historique->getAnnee();
        $user = $this->getUser();
        $parcelleHistoriqueTblAleas = new \App\Entity\ParcelleHistoriquesTblAleas($user);
        $parcelleHistoriqueTblAleas->setParcelleHistorique($historique);

        $form = $this->createForm('App\Form\ParcelleHistoriquesTblAleasType', $parcelleHistoriqueTblAleas);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($parcelleHistoriqueTblAleas);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $historique->getId(),
            ]);
        }

        return $this->render('parcellehistoriquestblaleas/new.html.twig', [
            'parcelleHistoriqueTblAleas' => $parcelleHistoriqueTblAleas,
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a ParcelleHistoriquesTblAleas entity.
     *
     * @param ParcelleHistoriquesTblAleas $parcelleHistoriqueTblAleas The ParcelleHistoriquesTblAleas entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(\App\Entity\ParcelleHistoriquesTblAleas $parcelleHistoriqueTblAleas, $options = [])
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation_id = $parcelleHistoriqueTblAleas->getParcelleHistorique()->getParcelle()->getExploitation()->getId();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_parcellehistoriquestblaleas_delete', [
                'id' => $parcelleHistoriqueTblAleas->getId(),
                'exploitation' => $exploitation,
                'coop_id' => $coop->getId(),
                'group_id' => $groupe->getId(),
                'exploitation_id' => $exploitation_id,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
