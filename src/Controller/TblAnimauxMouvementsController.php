<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblAnimauxMouvements;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblAnimauxMouvements controller.
 *
 * @Route("/{_locale}/references/TblAnimauxMouvements")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblAnimauxMouvementsController extends AbstractController
{
    /**
     * Deletes a tblAnimauxMouvements entity.
     *
     * @Route("/{id}", name="references_tblanimauxmouvements_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblAnimauxMouvements $tblAnimauxMouvements)
    {
        $form = $this->createDeleteForm($tblAnimauxMouvements);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblAnimauxMouvements);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblanimauxmouvements_index');
    }

    /**
     * Displays a form to edit an existing TblAnimauxMouvements entity.
     *
     * @Route("/{id}/edit", name="references_tblanimauxmouvements_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblAnimauxMouvements $tblAnimauxMouvements)
    {
        $deleteForm = $this->createDeleteForm($tblAnimauxMouvements);
        $editForm = $this->createForm('App\Form\TblAnimauxMouvementsType', $tblAnimauxMouvements);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAnimauxMouvements);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblanimauxmouvements_index', ['id' => $tblAnimauxMouvements->getId()]);
        }

        return $this->render('tblanimauxmouvements/edit.html.twig', [
            'tblAnimauxMouvements' => $tblAnimauxMouvements,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all tblAnimauxMouvements entities.
     *
     * @Route("/", name="references_tblanimauxmouvements_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblAnimauxMouvements = $em->getRepository(TblAnimauxMouvements::class)->findAll();

        return $this->render('tblanimauxmouvements/index.html.twig', [
            'tblAnimauxMouvements' => $tblAnimauxMouvements,
        ]);
    }

    /**
     * Creates a new TblAnimauxMouvements entity.
     *
     * @Route("/new", name="references_tblanimauxmouvements_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblAnimauxMouvements = new TblAnimauxMouvements($user);
        $form = $this->createForm('App\Form\TblAnimauxMouvementsType', $tblAnimauxMouvements);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAnimauxMouvements);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblanimauxmouvements_index', ['id' => $tblAnimauxMouvements->getId()]);
        }

        return $this->render('tblanimauxmouvements/new.html.twig', [
            'tblAnimauxMouvements' => $tblAnimauxMouvements,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblAnimauxMouvements entity.
     *
     * @Route("/{id}", name="references_tblanimauxmouvements_show", methods={"GET"})
     */
    public function showAction(TblAnimauxMouvements $tblAnimauxMouvements)
    {
        $deleteForm = $this->createDeleteForm($tblAnimauxMouvements);

        return $this->render('tblanimauxmouvements/show.html.twig', [
            'tblAnimauxMouvements' => $tblAnimauxMouvements,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblAnimauxMouvements entity.
     *
     * @param TblAnimauxMouvements $tblAnimauxMouvements The TblAnimauxMouvements entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblAnimauxMouvements $tblAnimauxMouvements)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblanimauxmouvements_delete', ['id' => $tblAnimauxMouvements->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
