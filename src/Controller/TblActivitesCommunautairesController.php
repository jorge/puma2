<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblActivitesCommunautaires;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblActivitesCommunautaires controller.
 *
 * @Route("/{_locale}/references/tblactivitescommunautaires")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblActivitesCommunautairesController extends AbstractController
{
    /**
     * Deletes a TblActivitesCommunautaires entity.
     *
     * @Route("/{id}", name="references_tblactivitescommunautaires_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblActivitesCommunautaires $tblActivitesCommunautaire)
    {
        $form = $this->createDeleteForm($tblActivitesCommunautaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblActivitesCommunautaire);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblactivitescommunautaires_index');
    }

    /**
     * Displays a form to edit an existing TblActivitesCommunautaires entity.
     *
     * @Route("/{id}/edit", name="references_tblactivitescommunautaires_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblActivitesCommunautaires $tblActivitesCommunautaire)
    {
        $deleteForm = $this->createDeleteForm($tblActivitesCommunautaire);
        $editForm = $this->createForm('App\Form\TblActivitesCommunautairesType', $tblActivitesCommunautaire);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblActivitesCommunautaire);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblactivitescommunautaires_index', ['id' => $tblActivitesCommunautaire->getId()]);
        }

        return $this->render('tblactivitescommunautaires/edit.html.twig', [
            'tblActivitesCommunautaire' => $tblActivitesCommunautaire,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblActivitesCommunautaires entities.
     *
     * @Route("/", name="references_tblactivitescommunautaires_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblActivitesCommunautaires = $em->getRepository(TblActivitesCommunautaires::class)->findAll();

        return $this->render('tblactivitescommunautaires/index.html.twig', [
            'tblActivitesCommunautaires' => $tblActivitesCommunautaires,
        ]);
    }

    /**
     * Creates a new TblActivitesCommunautaires entity.
     *
     * @Route("/new", name="references_tblactivitescommunautaires_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblActivitesCommunautaire = new TblActivitesCommunautaires($user);
        $form = $this->createForm('App\Form\TblActivitesCommunautairesType', $tblActivitesCommunautaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblActivitesCommunautaire);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblactivitescommunautaires_index', ['id' => $tblActivitesCommunautaire->getId()]);
        }

        return $this->render('tblactivitescommunautaires/new.html.twig', [
            'tblActivitesCommunautaire' => $tblActivitesCommunautaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblActivitesCommunautaires entity.
     *
     * @Route("/{id}", name="references_tblactivitescommunautaires_show", methods={"GET"})
     */
    public function showAction(TblActivitesCommunautaires $tblActivitesCommunautaire)
    {
        $deleteForm = $this->createDeleteForm($tblActivitesCommunautaire);

        return $this->render('tblactivitescommunautaires/show.html.twig', [
            'tblActivitesCommunautaire' => $tblActivitesCommunautaire,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblActivitesCommunautaires entity.
     *
     * @param TblActivitesCommunautaires $tblActivitesCommunautaire The TblActivitesCommunautaires entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblActivitesCommunautaires $tblActivitesCommunautaire)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblactivitescommunautaires_delete', ['id' => $tblActivitesCommunautaire->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
