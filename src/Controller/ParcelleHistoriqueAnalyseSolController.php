<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\ParcelleHistoriqueAnalyseSol;
use App\Entity\ParcelleHistoriques;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * ParcelleHistoriqueAnalyseSol controller.
 *
 * @Route("/{_locale}/references/parcellehistoriqueanalysesol")
 */

use Symfony\Component\Routing\Annotation\Route;

final class ParcelleHistoriqueAnalyseSolController extends AbstractController
{
    /**
     * Deletes a ParcelleHistoriqueAnalyseSol entity.
     *
     * @Route("/{id}", name="references_parcellehistoriqueanalysesol_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, ParcelleHistoriqueAnalyseSol $parcelleHistoriqueAnalyseSol)
    {
        $parcelle = $parcelleHistoriqueAnalyseSol->getParcelleHistorique()->getParcelle();
        $exploitation = $parcelle->getExploitation();
        $annee = $parcelleHistoriqueAnalyseSol->getParcelleHistorique()->getAnnee();
        $form = $this->createDeleteForm($parcelleHistoriqueAnalyseSol);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($parcelleHistoriqueAnalyseSol);
            $em->flush();
        }

        return $this->redirectToRoute('references_parcellehistoriques_show', [
            'id' => $parcelleHistoriqueAnalyseSol->getParcelleHistorique()->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing ParcelleHistoriqueAnalyseSol entity.
     *
     * @Route("/{id}/edit", name="references_parcellehistoriqueanalysesol_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, ParcelleHistoriqueAnalyseSol $parcelleHistoriqueAnalyseSol)
    {
        $exploitation = $parcelleHistoriqueAnalyseSol->getParcelleHistorique()->getParcelle()->getExploitation();
        $annee = $parcelleHistoriqueAnalyseSol->getParcelleHistorique()->getAnnee();
        $deleteForm = $this->createDeleteForm($parcelleHistoriqueAnalyseSol);
        $editForm = $this->createForm('App\Form\ParcelleHistoriqueAnalyseSolType', $parcelleHistoriqueAnalyseSol);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parcelleHistoriqueAnalyseSol);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $parcelleHistoriqueAnalyseSol->getParcelleHistorique()->getId(), ]);
        }

        return $this->render('parcellehistoriqueanalysesol/edit.html.twig', [
            'parcelleHistoriqueAnalyseSol' => $parcelleHistoriqueAnalyseSol,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new ParcelleHistoriqueAnalyseSol entity.
     *
     * @Route("/new/{historique}", name="references_parcellehistoriqueanalysesol_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, ParcelleHistoriques $historique)
    {
        $exploitation = $historique->getParcelle()->getExploitation();
        $annee = $historique->getAnnee();
        $user = $this->getUser();
        $parcelleHistoriqueAnalyseSol = new ParcelleHistoriqueAnalyseSol($user);
        $parcelleHistoriqueAnalyseSol->setParcelleHistorique($historique);
        $form = $this->createForm('App\Form\ParcelleHistoriqueAnalyseSolType', $parcelleHistoriqueAnalyseSol);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parcelleHistoriqueAnalyseSol);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $historique->getId(), ]);
        }

        return $this->render('parcellehistoriqueanalysesol/new.html.twig', [
            'parcelleHistoriqueAnalyseSol' => $parcelleHistoriqueAnalyseSol,
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a ParcelleHistoriqueAnalyseSol entity.
     *
     * @Route("/{id}", name="references_parcellehistoriqueanalysesol_show", methods={"GET"})
     */
    public function showAction(ParcelleHistoriqueAnalyseSol $parcelleHistoriqueAnalyseSol)
    {
        $deleteForm = $this->createDeleteForm($parcelleHistoriqueAnalyseSol);

        return $this->render('parcellehistoriqueanalysesol/show.html.twig', [
            'parcelleHistoriqueAnalyseSol' => $parcelleHistoriqueAnalyseSol,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a ParcelleHistoriqueAnalyseSol entity.
     *
     * @param ParcelleHistoriqueAnalyseSol $parcelleHistoriqueAnalyseSol The ParcelleHistoriqueAnalyseSol entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ParcelleHistoriqueAnalyseSol $parcelleHistoriqueAnalyseSol)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_parcellehistoriqueanalysesol_delete', ['id' => $parcelleHistoriqueAnalyseSol->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
