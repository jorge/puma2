<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblOperationCulturaleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblOperationCulturaleType controller.
 *
 * @Route("/{_locale}/references/tbloperationculturaletype")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblOperationCulturaleTypeController extends AbstractController
{
    /**
     * Deletes a TblOperationCulturaleType entity.
     *
     * @Route("/{id}", name="references_tbloperationculturaletype_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblOperationCulturaleType $tblOperationCulturaleType)
    {
        $form = $this->createDeleteForm($tblOperationCulturaleType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblOperationCulturaleType);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbloperationculturaletype_index');
    }

    /**
     * Displays a form to edit an existing TblOperationCulturaleType entity.
     *
     * @Route("/{id}/edit", name="references_tbloperationculturaletype_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblOperationCulturaleType $tblOperationCulturaleType)
    {
        $deleteForm = $this->createDeleteForm($tblOperationCulturaleType);
        $editForm = $this->createForm('App\Form\TblOperationCulturaleTypeType', $tblOperationCulturaleType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOperationCulturaleType);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbloperationculturaletype_index', ['id' => $tblOperationCulturaleType->getId()]);
        }

        return $this->render('tbloperationculturaletype/edit.html.twig', [
            'tblOperationCulturaleType' => $tblOperationCulturaleType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblOperationCulturaleType entities.
     *
     * @Route("/", name="references_tbloperationculturaletype_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblOperationCulturaleTypes = $em->getRepository(TblOperationCulturaleType::class)->findAll();

        return $this->render('tbloperationculturaletype/list.html.twig', [
            'tblOperationCulturaleTypes' => $tblOperationCulturaleTypes,
        ]);
    }

    /**
     * Creates a new TblOperationCulturaleType entity.
     *
     * @Route("/new", name="references_tbloperationculturaletype_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblOperationCulturaleType = new TblOperationCulturaleType($user);
        $form = $this->createForm('App\Form\TblOperationCulturaleTypeType', $tblOperationCulturaleType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOperationCulturaleType);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbloperationculturaletype_index', ['id' => $tblOperationCulturaleType->getId()]);
        }

        return $this->render('tbloperationculturaletype/new.html.twig', [
            'tblOperationCulturaleType' => $tblOperationCulturaleType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblOperationCulturaleType entity.
     *
     * @param TblOperationCulturaleType $tblOperationCulturaleType The TblOperationCulturaleType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblOperationCulturaleType $tblOperationCulturaleType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbloperationculturaletype_delete', ['id' => $tblOperationCulturaleType->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
