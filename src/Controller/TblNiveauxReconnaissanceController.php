<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblNiveauxReconnaissance;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblNiveauxReconnaissance controller.
 *
 * @Route("/{_locale}/references/tblniveauxreconnaissance")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblNiveauxReconnaissanceController extends AbstractController
{
    /**
     * Deletes a TblNiveauxReconnaissance entity.
     *
     * @Route("/{id}", name="references_tblniveauxreconnaissance_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblNiveauxReconnaissance $tblNiveauxReconnaissance)
    {
        $form = $this->createDeleteForm($tblNiveauxReconnaissance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblNiveauxReconnaissance);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblniveauxreconnaissance_index');
    }

    /**
     * Displays a form to edit an existing TblNiveauxReconnaissance entity.
     *
     * @Route("/{id}/edit", name="references_tblniveauxreconnaissance_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblNiveauxReconnaissance $tblNiveauxReconnaissance)
    {
        $deleteForm = $this->createDeleteForm($tblNiveauxReconnaissance);
        $editForm = $this->createForm('App\Form\TblNiveauxReconnaissanceType', $tblNiveauxReconnaissance);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblNiveauxReconnaissance);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblniveauxreconnaissance_index', ['id' => $tblNiveauxReconnaissance->getId()]);
        }

        return $this->render('tblniveauxreconnaissance/edit.html.twig', [
            'tblNiveauxReconnaissance' => $tblNiveauxReconnaissance,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblNiveauxReconnaissance entities.
     *
     * @Route("/", name="references_tblniveauxreconnaissance_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblNiveauxReconnaissances = $em->getRepository(TblNiveauxReconnaissance::class)->findAll();

        return $this->render('tblniveauxreconnaissance/index.html.twig', [
            'tblNiveauxReconnaissances' => $tblNiveauxReconnaissances,
        ]);
    }

    /**
     * Creates a new TblNiveauxReconnaissance entity.
     *
     * @Route("/new", name="references_tblniveauxreconnaissance_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblNiveauxReconnaissance = new TblNiveauxReconnaissance($user);
        $form = $this->createForm('App\Form\TblNiveauxReconnaissanceType', $tblNiveauxReconnaissance);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblNiveauxReconnaissance);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblniveauxreconnaissance_index', ['id' => $tblNiveauxReconnaissance->getId()]);
        }

        return $this->render('tblniveauxreconnaissance/new.html.twig', [
            'tblNiveauxReconnaissance' => $tblNiveauxReconnaissance,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblNiveauxReconnaissance entity.
     *
     * @Route("/{id}", name="references_tblniveauxreconnaissance_show", methods={"GET"})
     */
    public function showAction(TblNiveauxReconnaissance $tblNiveauxReconnaissance)
    {
        $deleteForm = $this->createDeleteForm($tblNiveauxReconnaissance);

        return $this->render('tblniveauxreconnaissance/show.html.twig', [
            'tblNiveauxReconnaissance' => $tblNiveauxReconnaissance,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblNiveauxReconnaissance entity.
     *
     * @param TblNiveauxReconnaissance $tblNiveauxReconnaissance The TblNiveauxReconnaissance entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblNiveauxReconnaissance $tblNiveauxReconnaissance)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblniveauxreconnaissance_delete', ['id' => $tblNiveauxReconnaissance->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
