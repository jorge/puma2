<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Animaux;
use App\Entity\AnimauxLabour;
use App\Entity\AnimauxProduction;
use App\Entity\AnimauxStructure;
use App\Entity\Exploitations;
use App\Entity\TblAnimauxProduction;
use App\Repository\PivotExploitationOrganisationsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Animaux controller.
 *
 * @Route("/{_locale}/references_animaux")
 */

use function count;

final class AnimauxController extends AbstractController
{
    /**
     * Deletes a Animaux entity.
     *
     * @Route("/{id}", name="references_animaux_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Animaux $animaux, EntityManagerInterface $em)
    {
        $exploitation = $animaux->getExploitation();
        $annee = $animaux->getAnnee();

        $form = $this->createDeleteForm($animaux, [
            'annee' => $annee,
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $animaux->setRadie(true);
            $em->persist($animaux);
            $em->flush();
            $this->radierBrancheAnimaux($animaux->getId(), 1, $em);
        }

        return $this->redirectToRoute('references_animaux_index', [
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing Animaux entity.
     *
     * @Route("/edit/{id}", name="references_animaux_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Animaux $animaux)
    {
        $redirect = $request->get('redirect');
        $em = $this->getDoctrine()->getManager();

        $annee = $animaux->getAnnee();
        $exploitation = $animaux->getExploitation();

        $deleteForm = $this->createDeleteForm($animaux);

        $options = [];

        if ($redirect) {
            $options['action'] = '?redirect=' . $redirect;
        }

        $editForm = $this->createForm('App\Form\AnimauxType', $animaux, $options);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($animaux);
            $em->flush();

            if ('f1' === $redirect) {
                return $this->redirectToRoute('saisie_f1_membre', [
                    'id' => $exploitation->getId(),
                    'annee' => $annee,
                ]);
            }

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('animaux/edit.html.twig', [
            'animaux' => $animaux,
            'edit_form' => $editForm->createView(),
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'annee' => $annee,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all Animaux entities.
     *
     * @Route("/list/{exploitation_id}/{annee}", name="references_animaux_index", methods={"GET"})
     */
    public function indexAction(string $exploitation_id, int $annee)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $troupeaux = $em->getRepository(Animaux::class)->findBy(['exploitation' => $exploitation, 'radie' => 0, 'annee' => $annee]);
        $annee_precedent = $this->posibleTroupeauListe($exploitation, $annee);

        return $this->render('animaux/index.html.twig', [
            'troupeaux' => $troupeaux,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'annee_precedent' => $annee_precedent,
        ]);
    }

    /**
     * Creates a new Animaux entity.
     *
     * @Route("/new/{exploitation}/{annee}", name="references_animaux_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, Exploitations $exploitation, int $annee)
    {
        $redirect = $request->get('redirect');
        $em = $this->getDoctrine()->getManager();

        $user = $this->getUser();
        $animaux = new Animaux($user);
        $animaux->setExploitation($exploitation);
        $animaux->setAnnee($annee);

        $options = [];

        if ($redirect) {
            $options['action'] = '?redirect=' . $redirect;
        }

        $form = $this->createForm('App\Form\AnimauxType', $animaux, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $animaux->setRadie(false);
            $em->persist($animaux);
            $em->flush();

            if ('f1' === $redirect) {
                return $this->redirectToRoute('saisie_f1_membre', [
                    'id' => $exploitation->getId(),
                    'annee' => $annee,
                ]);
            }

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee, ]);
        }

        return $this->render('animaux/new.html.twig', [
            'animaux' => $animaux,
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'annee' => $annee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Liste des troupeaux des enquêtes précédentes, pour être recopier
     * dans une nouvelle enquête.
     *
     * @param mixed $annee
     */
    public function posibleTroupeauListe(Exploitations $exploitation, int $annee)
    {
        $exercice = $exploitation->getExerciceFor($annee);
        $collectes = $exercice->getCollectes();

        $arrAnnees = [];
        $keyMax = count($arrAnnees) > 0 ? array_search(max($arrAnnees), $arrAnnees, true) : null;

        return null === $keyMax ? null : $arrAnnees[$keyMax];
    }

    /* maj radie pour les tables dependent de animaux */
    public function radierBrancheAnimaux($animaux_id, $status, $em)
    {
        $structure = $em
            ->getRepository(AnimauxStructure::class)
            ->updateRadieStatus($animaux_id, $status);
        $production = $em
            ->getRepository(AnimauxProduction::class)
            ->updateRadieStatus($animaux_id, $status);
        $labour = $em
            ->getRepository(AnimauxLabour::class)
            ->updateRadieStatus($animaux_id, $status);
    }

    /**
     * @Route("/recuperer/troupeau/{id}/{annee}", name="recopier_troupeau_nouvelle_enquete")
     *
     * Recopie les données d'un troupeau d'une enquête pour une nouvelle
     * enquête
     * $id -> id du troupeau à recopier
     * $annee -> année de la nouvelle enquête
     *
     * @param mixed $id
     * @param mixed $annee
     */
    public function recupererTroupeau(Request $request, $id, $annee)
    {
        $redirect = $request->get('redirect');

        if ($redirect) {
            $options['action'] = '?redirect=' . $redirect;
        }

        $em = $this->getDoctrine()->getManager();
        $old_troupeau = $em->getRepository(Animaux::class)->find($id);
        $user = $this->getUser();
        $animaux = new Animaux($user);
        $animaux->setExploitation($old_troupeau->getExploitation());
        $animaux->setAnnee($annee);
        $animaux->setEspece($old_troupeau->getEspece());
        $animaux->setFemellesReproductrices($old_troupeau->getFemellesReproductrices());
        $animaux->setJeunesFemelles($old_troupeau->getJeunesFemelles());
        $animaux->setJeunesImmatures($old_troupeau->getJeunesImmatures());
        $animaux->setJeunesMales($old_troupeau->getJeunesMales());
        $animaux->setMalesReproducteurs($old_troupeau->getMalesReproducteurs());
        $animaux->setNbrTotalTetes($old_troupeau->getNbrTotalTetes());
        $animaux->setAgeReformeFemelles($old_troupeau->getAgeReformeFemelles());
        $produits = $old_troupeau->getProduits();
        $produitsCount = count($produits);

        for ($i = 0; $produitsCount > $i; ++$i) {
            $animaux->addProduit($produits[$i]);
        }
        $alimentations = $old_troupeau->getAlimentations();
        $alimentationsCount = count($alimentations);

        for ($k = 0; $alimentationsCount > $k; ++$k) {
            $animaux->addAlimentation($alimentations[$k]);
        }
        $animaux->setTypeElevage($old_troupeau->getTypeElevage());
        $options = [];
        $form = $this->createForm('App\Form\AnimauxType', $animaux, $options);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $animaux->setRadie(0);
            $em->persist($animaux);
            $em->flush();

            if ('f1' === $redirect) {
                return $this->redirectToRoute('references_animaux_index', [
                    'exploitation_id' => $old_troupeau->getExploitation()->getId(),
                    'annee' => $annee,
                ]);
            }

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux->getId(),
                'exploitation' => $old_troupeau->getExploitation(),
                'annee' => $annee, ]);
        }

        return $this->render('animaux/new.html.twig', [
            'animaux' => $animaux,
            'exploitation' => $old_troupeau->getExploitation(),
            'exploitation_id' => $old_troupeau->getExploitation()->getId(),
            'annee' => $annee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/recuperer/troupeau/{exploitation_id}/{annee}/{anneePrecedent}/liste", name="recuperer_troupeau_enquete_precedente_liste")
     *
     * @param mixed $exploitation_id
     * @param mixed $annee
     * @param mixed $anneePrecedent
     */
    public function recupererTroupeauListe($exploitation_id, $annee, $anneePrecedent)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $troupeaux = $em->getRepository(Animaux::class)->findBy(['exploitation' => $exploitation, 'radie' => 0, 'annee' => $anneePrecedent]);

        return $this->render('saisies/recupererListeTroupeau.html.twig', [
            'troupeaux' => $troupeaux,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Finds and displays a Animaux entity.
     *
     * @Route("/show/{id}", name="references_animaux_show", methods={"GET"})
     * @Route("/show/{id}/hide_history/{hideHistory}", name="references_animaux_show_without_history", methods={"GET"})
     *
     * @var int at 1 if you do not want to display the history
     */
    public function showAction(Animaux $animaux, ?int $hideHistory = null, PivotExploitationOrganisationsRepository $pivotExploitationOrganisationsRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $animaux->getAnnee();
        $exploitation = $animaux->getExploitation();
        $pivot = $pivotExploitationOrganisationsRepository->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $deleteForm = $this->createDeleteForm($animaux);
        /* structure -> objet technique */
        $mouvements = $em->getRepository(AnimauxStructure::class)->getMouvements($animaux);
        /* production */
        $l = $em->getRepository(TblAnimauxProduction::class)->findByProduction('lait');
        $o = $em->getRepository(TblAnimauxProduction::class)->findByProduction('oeufs');
        $f = $em->getRepository(TblAnimauxProduction::class)->findByProduction('fumier');

        /* production */
        $laits = $em->getRepository(AnimauxProduction::class)->findBy(['animaux' => $animaux, 'production' => $l, 'annee' => $annee, 'radie' => 0]);
        $oeufs = $em->getRepository(AnimauxProduction::class)->findBy(['animaux' => $animaux, 'production' => $o, 'annee' => $annee, 'radie' => 0]);
        $fumiers = $em->getRepository(AnimauxProduction::class)->findBy(['animaux' => $animaux, 'production' => $f, 'annee' => $annee, 'radie' => 0]);

        return $this->render('animaux/show.html.twig', [
            'id' => $animaux->getId(),
            'animaux' => $animaux,
            'exploitation' => $exploitation,
            'mouvements' => $mouvements,
            'laits' => $laits,
            'oeufs' => $oeufs,
            'fumiers' => $fumiers,
            'delete_form' => $deleteForm->createView(),
            'annee' => $annee,
            'hide_history' => $hideHistory,
        ]);
    }

    /**
     * Creates a form to delete a Animaux entity.
     *
     * @param Animaux $animaux The Animaux entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Animaux $animaux, $options = [])
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $animaux->getAnnee();
        $exploitation = $animaux->getExploitation();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'references_animaux_delete',
                ['id' => $animaux->getId(),
                    'exploitation_id' => $exploitation->getId(),
                    'exploitation' => $exploitation,
                    'annee' => $annee,
                ]
            ))
            ->setMethod('DELETE')
            ->getForm();
    }
}
