<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Batiments;
use App\Entity\Exploitations;
use App\Entity\PivotExploitationOrganisations;
use DateTime;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;


/**
 * Batiments controller.
 *
 * @Route("/{_locale}/references/batiments")
 */

use Symfony\Component\Routing\Annotation\Route;

final class BatimentsController extends AbstractController
{
    /**
     * Deletes a Batiments entity.
     *
     * @Route("/{id}", name="references_batiments_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Batiments $batiment)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $batiment->getAnnee();
        $exploitation_id = $batiment->getExploitation()->getId();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );
        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $form = $this->createDeleteForm($batiment, ['coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $batiment->setRadie(TRUE);
            $em->persist($batiment);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f1_membre', [
            'id' => $exploitation_id,
            'annee' => $annee,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing Batiments entity.
     *
     * @Route("/{id}/edit", name="references_batiments_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Batiments $batiment)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $batiment->getAnnee();
        $exploitation_id = $batiment->getExploitation()->getId();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );
        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();
        $deleteForm = $this->createDeleteForm($batiment);
        $editForm = $this->createForm('App\Form\BatimentsType', $batiment);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($batiment);
            $em->flush();

            return $this->redirectToRoute('saisie_f1_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
                'exploitation_id' => $exploitation_id,
                'exploitation' => $exploitation,
                'coop_id' => $coop->getId(),
                'group_id' => $groupe->getId(),
            ]);
        }

        return $this->render('batiments/edit.html.twig', [
            'batiment' => $batiment,
            'edit_form' => $editForm->createView(),
            'annee' => $annee,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new Batiments entity.
     *
     * @Route("/{exploitation}/new", name="references_batiments_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, Exploitations $exploitation)
    {
        $this->denyAccessUnlessGranted('ROLE_ENCODER');

        $em = $this->getDoctrine()->getManager();
        $annee = $request->query->get('annee', (new DateTime('now'))->format('Y'));
        $annee = intval($annee);

        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );
        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $user = $this->getUser();
        $batiment = new Batiments($user);

        $batiment->setExploitation($exploitation);
        $batiment->setAnnee($annee);

        $form = $this->createForm('App\Form\BatimentsType', $batiment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $batiment->setRadie(FALSE);
            $em->persist($batiment);
            $em->flush();

            return $this->redirectToRoute('saisie_f1_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('batiments/new.html.twig', [
            'batiment' => $batiment,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a Batiments entity.
     *
     * @param Batiments $batiment The Batiments entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Batiments $batiment, $options = [])
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation_id = $batiment->getExploitation()->getId();
        $annee = '2017'; // $_GET['annee'];
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );
        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_batiments_delete', [
                'id' => $batiment->getId(),
                'coop_id' => $coop->getId(),
                'group_id' => $groupe->getId(),
                'exploitation_id' => $exploitation_id,
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
