<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblModeCulture;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblModeCulture controller.
 *
 * @Route("/{_locale}/references/tblmodeculture")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblModeCultureController extends AbstractController
{
    /**
     * Deletes a TblModeCulture entity.
     *
     * @Route("/{id}", name="references_tblmodeculture_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblModeCulture $tblModeCulture)
    {
        $form = $this->createDeleteForm($tblModeCulture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblModeCulture);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblmodeculture_index');
    }

    /**
     * Displays a form to edit an existing TblModeCulture entity.
     *
     * @Route("/{id}/edit", name="references_tblmodeculture_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblModeCulture $tblModeCulture)
    {
        $deleteForm = $this->createDeleteForm($tblModeCulture);
        $editForm = $this->createForm('App\Form\TblModeCultureType', $tblModeCulture);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblModeCulture);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblmodeculture_index', ['id' => $tblModeCulture->getId()]);
        }

        return $this->render('tblmodeculture/edit.html.twig', [
            'tblModeCulture' => $tblModeCulture,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblModeCulture entities.
     *
     * @Route("/", name="references_tblmodeculture_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblModeCultures = $em->getRepository(TblModeCulture::class)->findAll();

        return $this->render('tblmodeculture/index.html.twig', [
            'tblModeCultures' => $tblModeCultures,
        ]);
    }

    /**
     * Creates a new TblModeCulture entity.
     *
     * @Route("/new", name="references_tblmodeculture_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblModeCulture = new TblModeCulture($user);
        $form = $this->createForm('App\Form\TblModeCultureType', $tblModeCulture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblModeCulture);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblmodeculture_index', ['id' => $tblModeCulture->getId()]);
        }

        return $this->render('tblmodeculture/new.html.twig', [
            'tblModeCulture' => $tblModeCulture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblModeCulture entity.
     *
     * @Route("/{id}", name="references_tblmodeculture_show", methods={"GET"})
     */
    public function showAction(TblModeCulture $tblModeCulture)
    {
        $deleteForm = $this->createDeleteForm($tblModeCulture);

        return $this->render('tblmodeculture/show.html.twig', [
            'tblModeCulture' => $tblModeCulture,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblModeCulture entity.
     *
     * @param TblModeCulture $tblModeCulture The TblModeCulture entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblModeCulture $tblModeCulture)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblmodeculture_delete', ['id' => $tblModeCulture->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
