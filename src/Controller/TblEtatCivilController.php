<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblEtatCivil;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblEtatCivil controller.
 *
 * @Route("/{_locale}/references/tbletatcivil")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblEtatCivilController extends AbstractController
{
    /**
     * Deletes a TblEtatCivil entity.
     *
     * @Route("/{id}", name="references_tbletatcivil_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblEtatCivil $tblEtatCivil)
    {
        $form = $this->createDeleteForm($tblEtatCivil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblEtatCivil);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbletatcivil_index');
    }

    /**
     * Displays a form to edit an existing TblEtatCivil entity.
     *
     * @Route("/{id}/edit", name="references_tbletatcivil_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblEtatCivil $tblEtatCivil)
    {
        $deleteForm = $this->createDeleteForm($tblEtatCivil);
        $editForm = $this->createForm('App\Form\TblEtatCivilType', $tblEtatCivil);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblEtatCivil);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbletatcivil_index', ['id' => $tblEtatCivil->getId()]);
        }

        return $this->render('tbletatcivil/edit.html.twig', [
            'tblEtatCivil' => $tblEtatCivil,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblEtatCivil entities.
     *
     * @Route("/", name="references_tbletatcivil_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblEtatCivils = $em->getRepository(TblEtatCivil::class)->findAll();

        return $this->render('tbletatcivil/index.html.twig', [
            'tblEtatCivils' => $tblEtatCivils,
        ]);
    }

    /**
     * Creates a new TblEtatCivil entity.
     *
     * @Route("/new", name="references_tbletatcivil_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblEtatCivil = new TblEtatCivil($user);
        $form = $this->createForm('App\Form\TblEtatCivilType', $tblEtatCivil);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblEtatCivil);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbletatcivil_index', ['id' => $tblEtatCivil->getId()]);
        }

        return $this->render('tbletatcivil/new.html.twig', [
            'tblEtatCivil' => $tblEtatCivil,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblEtatCivil entity.
     *
     * @Route("/{id}", name="references_tbletatcivil_show", methods={"GET"})
     */
    public function showAction(TblEtatCivil $tblEtatCivil)
    {
        $deleteForm = $this->createDeleteForm($tblEtatCivil);

        return $this->render('tbletatcivil/show.html.twig', [
            'tblEtatCivil' => $tblEtatCivil,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblEtatCivil entity.
     *
     * @param TblEtatCivil $tblEtatCivil The TblEtatCivil entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblEtatCivil $tblEtatCivil)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbletatcivil_delete', ['id' => $tblEtatCivil->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
