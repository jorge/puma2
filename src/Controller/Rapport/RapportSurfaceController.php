<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Rapport;

use App\Entity\RapportsSurface;
use App\Entity\TblOrganisations;
use App\Form\RapportsSurfaceType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;
use Symfony\Component\Form\FormInterface;


/**
 * Class RapportSurfaceController.
 *
 * @Route("/{_locale}")
 */


final class RapportSurfaceController extends AbstractController
{
    //RapportsEnDeveloppementContoller
    public static $strSql_surfaceexploAction = "SELECT c.denomination as cooperative,
                    g.denomination AS groupement,
                    te.numero_membre,
                    te.surface AS total,
                    te.nombre_parcelles,
                    te.personnes_charge,
                    te.nombre_filles,
                    te.nombre_fils
                FROM tbl_organisations c
                    LEFT JOIN tbl_organisations g ON c.id = g.parent_id
                    LEFT JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
                    LEFT JOIN tbl_organisation_level tol ON c.level_id=tol.id
                    LEFT JOIN (SELECT SUM(par.surface) as surface, COUNT(par.id) as nombre_parcelles,
                               e.numero_membre numero_membre, e.id,
                               e.personnes_charge, e.nombre_filles, e.nombre_fils
                              FROM exploitations e, parcelles par WHERE par.exploitation_id=e.id GROUP BY e.id)te ON te.id=p.exploitation_id
                WHERE tol.level=1 AND te.id is not null AND c.id='%s'
                ORDER BY cooperative ASC, groupement ASC";

    /**
     * @Route("/rapports/presurface", name="rapports_presurface")
     */
    public function presurfaceAction(Request $request)
    {
        return $this->render('rapports/surface.html.twig');
    }

        /**
     * @Route("/rapports/surface", name="surface")
     */
    public function surfaceAction(TranslatorInterface $translator, EntityManagerInterface $em)
    {
        $result = $this->_getRecordsetSurface($translator, $em);

        return $this->render('rapports/rec_surface.html.twig', [
            'recordset' => $result['recordset'],
            'cooperative' => 'Toutes les coopératives',
            'fields' => $result['fields'],
            'titre' => $result['titre'],
            'champs' => 5,
            'strSql' => $result['strSql'],
        ]);
    }

    /**
     * @Route("/rapports/surfaceexplo", name="surfaceexplo")
     * "Liste de surfaces et nombre de parcelles des exploitations(Unité: Ares)"
     */
    public function surfaceexploAction(Request $request, TranslatorInterface $translator, EntityManagerInterface $em)
    {
        $RapportsSurface = new RapportsSurface();
        $form = $this->createForm(
            'App\Form\RapportsSurfaceType',
            $RapportsSurface
        );
        $form->handleRequest($request);
        $titre = 'Liste de surfaces et nombre de parcelles des exploitations(Unité: Ares)';

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->_getRecordsetSurfaceexplo($translator, $em);

            return $this->render('rapports/rec_surface.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'cooperative' => $result['cooperative'],
                'titre' => $result['titre'],
                'strSql' => $result['strSql'],
                'champs' => 9,
            ]);
        }

        return $this->render('rapports/surfacebis.html.twig', [
            'RapportsSurface' => $RapportsSurface,
            'form' => $form->createView(),
            'titre' => $titre,
        ]);
    }

    /**
     * @Route("/rapports/surfacegroup", name="surfacegroup")
     */
    public function surfacegroupAction(Request $request, TranslatorInterface $translator, EntityManagerInterface $em)
    {
        /*  ok  */
        $RapportsSurface = new RapportsSurface();
        $form = $this->createForm(RapportsSurfaceType::class, $RapportsSurface);
        $form->handleRequest($request);
        $titre = 'Rapports de surfaces moyennes et totales par exploitation, groupé par groupement';

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->_getRecordsetSurfacegroup($translator, $em, $form);

            return $this->render('rapports/rec_surfacegroup.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'cooperative' => $result['cooperative'],
                'titre' => $result['titre'],
                'requete' => $result['requete'],
            ]);
        }

        return $this->render('rapports/surfacebis.html.twig', [
            'RapportsSurface' => $RapportsSurface,
            'form' => $form->createView(),
            'titre' => $titre,
        ]);
    }


    private function _getRecordsetSurface(TranslatorInterface $translator, EntityManagerInterface $em): array
    {
        set_time_limit(0);

        $strSql = 'SELECT c.denomination AS cooperative,
            AVG(te.surface) AS moyenne,
            AVG(te.nombre_parcelles) AS moy_nombre_parcelles,
            SUM(te.surface) AS total,
            COUNT(te.id) AS nombre_exploitations
        FROM tbl_organisations c
	LEFT JOIN tbl_organisations g ON c.id = g.parent_id
	LEFT JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
        LEFT JOIN tbl_organisation_level tol ON c.level_id=tol.id
        LEFT JOIN (
            SELECT SUM(par.surface) as surface, COUNT(par.id) AS nombre_parcelles, e.id
            FROM exploitations e, parcelles par
            WHERE par.exploitation_id=e.id
            GROUP BY e.id)
            te ON te.id=p.exploitation_id
        WHERE tol.level=1
        GROUP BY c.id';
        $titre = 'Liste de surfaces moyennes des exploitations et totales par coopérative';
        $fields['cooperative'] = [
            'nom' => $translator->trans('Coopérative'), ];
        $fields['moyenne'] = [
            'nom' => $translator->trans('Moyenne'),
            'unite' => 'Ares', ]; // TODO rassembler
        $fields['moy_nombre_parcelles'] = [
            'nom' => $translator->trans('Moyenne du nombre de parcelles'),
            'decimal' => true, ];
        $fields['total'] = [
            'nom' => $translator->trans('Superficie totale'),
            'unite' => 'Ares', ]; // TODO rassembler
        $fields['nombre_exploitations'] = [
            'nom' => $translator->trans('Nombre d\'exploitations'), ];
        /* **** fin de construction requete ***** */

        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        return [
            'fields' => $fields,
            'titre' => $titre,
            'strSql' => $strSql,
            'recordset' => $stmt->fetchAll(),
        ];
    }

    private function _getRecordsetSurfaceexplo(TranslatorInterface $translator, EntityManagerInterface $em): array
    {
        // set_time_limit(10);
        $parametros = $_GET['rapports_surface'];
        $cooperative_id = $parametros['Denomination'];
        $cooperative = $em->getRepository(TblOrganisations::class)->find($cooperative_id);

        $titre = 'Liste de surfaces et nombre de parcelles des exploitations';

        $fields['cooperative'] = [
            'nom' => $translator->trans('Coopérative'), ];
        $fields['groupement'] = [
            'nom' => $translator->trans('Groupement'), ];
        $fields['numero_membre'] = [
            'nom' => $translator->trans('Numéro de membre'), ];
        $fields['total'] = [
            'nom' => $translator->trans('Total'),
            'unite' => 'Ares', ]; // TODO rassembler
        $fields['nombre_parcelles'] = [
            'nom' => $translator->trans('Nombre de parcelles'), ];
        $fields['personnes_charge'] = [
            'nom' => $translator->trans('Personnes à charges'), ];
        $fields['nombre_filles'] = [
            'nom' => $translator->trans('Nombre de filles'), ];
        $fields['nombre_fils'] = [
            'nom' => $translator->trans('Nombre de fils'), ];

        $strSql = sprintf(self::$strSql_surfaceexploAction, $cooperative_id);

        /* **** fin de construction requete ***** */
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        return [
            'cooperative' => $cooperative,
            'fields' => $fields,
            'titre' => $titre,
            'strSql' => $strSql,
            'recordset' => $stmt->fetchAll(),
        ];
    }

    private function _getRecordsetSurfacegroup(TranslatorInterface $translator, EntityManagerInterface $em, FormInterface $form): array
    {
        set_time_limit(0);

        $cooperative_id = $form['Denomination']->getData()->getId();
        $cooperative = $em->getRepository(TblOrganisations::class)->find($cooperative_id);
        $cooperative_nom = $cooperative->getDenomination();

        $strSql = 'SELECT g.denomination AS groupement,
            AVG(te.surface) AS moyenne_surface,
            AVG(te.nombre_parcelles) AS moy_nombre_parcelles,
            SUM(te.surface) AS total,
            AVG(te.personnes_charge) AS moy_pers_charge,
            AVG(te.nombre_filles) AS moy_nombre_filles,
            AVG(te.nombre_fils) AS moy_nombre_fils
        FROM tbl_organisations c
	       LEFT JOIN tbl_organisations g ON c.id = g.parent_id
	       LEFT JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
           LEFT JOIN tbl_organisation_level tol ON c.level_id=tol.id
           LEFT JOIN (SELECT SUM(par.surface) as surface, Count(par.id) as nombre_parcelles, e.id,
                               e.personnes_charge, e.nombre_filles, e.nombre_fils FROM exploitations e, parcelles par WHERE par.exploitation_id=e.id GROUP BY e.id)te ON te.id=p.exploitation_id
           WHERE tol.level=1 AND c.id=:cooperative_id AND te.surface > 0
           GROUP BY g.id';

        $titre = 'Liste de surfaces moyennes des exploitations et totales par groupement';

        $fields['groupement'] = $translator->trans('groupement');
        $fields['moyenne_surface'] = $translator->trans('moyenne_surface');
        $fields['moy_nombre_parcelles'] = $translator->trans('moy_nombre_parcelles');
        $fields['total'] = $translator->trans('total');
        $fields['moy_pers_charge'] = $translator->trans('moy_pers_charge');
        $fields['moy_nombre_filles'] = $translator->trans('moy_nombre_filles');
        $fields['moy_nombre_fils'] = $translator->trans('moy_nombre_fils');
        /* **** fin de construction requete ***** */

        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute([
            'cooperative_id' => $cooperative_id,
        ]);

        return [
            'fields' => $fields,
            'cooperative' => $cooperative_nom,
            'titre' => $titre,
            'requete' => $strSql,
            'recordset' => $stmt->fetchAll(),
        ];
    }



}