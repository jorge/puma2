<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\CommercialisationProduits;
use App\Entity\Exploitations;
use App\Entity\TblTypeCommercialisation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * CommercialisationProduits controller.
 *
 * @Route("/{_locale}/references/commercialisationproduits")
 */

use Symfony\Component\Routing\Annotation\Route;

final class CommercialisationProduitsController extends AbstractController
{
    /**
     * Deletes a CommercialisationProduits entity.
     *
     * @Route("/{id}", name="references_commercialisationproduits_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, CommercialisationProduits $commercialisationProduit)
    {
        $exploitation_id = $commercialisationProduit->getExploitation()->getId();
        $annee = $commercialisationProduit->getAnnee();
        $exploitation = $commercialisationProduit->getExploitation();
        $form = $this->createDeleteForm($commercialisationProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $commercialisationProduit->setRadie(TRUE);
            $em->persist($commercialisationProduit);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f5_membre', [
            'id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing CommercialisationProduits entity.
     *
     * @Route("/{id}/edit", name="references_commercialisationproduits_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, CommercialisationProduits $commercialisationProduit)
    {
        $exploitation_id = $commercialisationProduit->getExploitation()->getId();
        $annee = $commercialisationProduit->getAnnee();
        $exploitation = $commercialisationProduit->getExploitation();
        $deleteForm = $this->createDeleteForm($commercialisationProduit);
        $typeCommercialisation = $commercialisationProduit->getTypeCommercialisation();

        if (isset($commercialisationProduit)) {
            $editForm = $this->createForm('App\Form\CommercialisationProduitsF0Type', $commercialisationProduit);
        } else {
            $editForm = $this->createForm('App\Form\CommercialisationProduitsType', $commercialisationProduit);
        }
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($commercialisationProduit);
            $em->flush();

            $this->addFlash(
                'success',
                'Mise à jour réussie'
            );

            if (isset($commercialisationProduit)) { // f0 ?
                return $this->redirectToRoute('saisie_f0_membre', [
                    'id' => $exploitation->getId(),
                    'annee' => $annee,
                ]);
            } // sinon F5 ?

            return $this->redirectToRoute('references_commercialisationproduits_edit', [
                'id' => $commercialisationProduit->getId(),
            ]);
        }

        return $this->render('commercialisationproduits/edit.html.twig', [
            'commercialisationProduit' => $commercialisationProduit,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Displays a form to edit an existing CommercialisationProduits entity.
     *
     * @Route("/{id}/editvente", name="references_commercialisationproduits_editvente", methods={"GET", "POST"})
     */
    public function editventeAction(Request $request, CommercialisationProduits $commercialisationProduit)
    {
        $exploitation_id = $commercialisationProduit->getExploitation()->getId();
        $annee = $commercialisationProduit->getAnnee();
        $exploitation = $commercialisationProduit->getExploitation();
        $elevage = $_GET['elevage'];
        /* creation options unites */
        $em = $this->getDoctrine()->getManager();
        $unites = ['kg' => 'kg', 'litres' => 'litres', 'Fbu' => 'Fbu', 'pourcentage' => 'pourcentage', 'nombre' => 'nombre'];
        /* fin */
        $deleteForm = $this->createDeleteForm($commercialisationProduit);
        $editForm = $this->createForm('App\Form\CommercialisationProduitsVenteType', $commercialisationProduit, ['unites' => $unites, 'elevage' => $elevage]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($commercialisationProduit);
            $em->flush();

            return $this->redirectToRoute('saisie_f5_membre', [
                'id' => $exploitation_id,
                'unites' => $unites,
                'elevage' => $elevage,
                'exploitation_id' => $exploitation_id,
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('commercialisationproduits/edit.html.twig', [
            'commercialisationProduit' => $commercialisationProduit,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'unites' => $unites,
            'elevage' => $elevage,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new CommercialisationProduits entity.
     *
     * @Route("/{exploitation}/{annee}/new/{type}/", name="references_commercialisationproduits_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Exploitations $exploitation, int $annee, TblTypeCommercialisation $type)
    {
        $user = $this->getUser();
        $commercialisationProduit = new CommercialisationProduits($user);
        $commercialisationProduit->setExploitation($exploitation);
        $commercialisationProduit->setAnnee($annee);
        $commercialisationProduit->setTypeCommercialisation($type);

        $form = $this->createForm('App\Form\CommercialisationProduitsF0Type', $commercialisationProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $commercialisationProduit->setRadie(FALSE);
            $em->persist($commercialisationProduit);
            $em->flush();

            $this->addFlash(
                'success',
                'Ajout réussi'
            );

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('commercialisationproduits/new.html.twig', [
            'commercialisationProduit' => $commercialisationProduit,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a new CommercialisationProduits entity. (F5).
     *
     * @Route("/newvente", name="references_commercialisationproduits_newvente", methods={"GET", "POST"})
     */
    public function newventeAction(Request $request)
    {
        $exploitation_id = $request->query->get('exploitation_id');
        $annee = $request->query->get('annee');
        $elevage = $request->query->get('elevage');

        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $commercialisationProduit = new CommercialisationProduits($user);
        $typecommercialisation = new TblTypeCommercialisation($user);

        if (isset($exploitation_id)) {
            $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
            $commercialisationProduit->setExploitation($exploitation);
        }

        if (isset($annee)) {
            $commercialisationProduit->setAnnee($annee);
        }

        $unites = ['kg' => 'kg', 'litres' => 'litres', 'Fbu' => 'Fbu', 'pourcentage' => 'pourcentage', 'nombre' => 'nombre'];
        $commercialisationProduit->setTypeCommercialisation(null);

        $form = $this->createForm('App\Form\CommercialisationProduitsVenteType', $commercialisationProduit, ['elevage' => $elevage, 'unites' => $unites]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $commercialisationProduit->setRadie(FALSE);
            $em->persist($commercialisationProduit);

            $em->flush();

            return $this->redirectToRoute('saisie_f5_membre', [
                'id' => $exploitation_id,
                'exploitation' => $exploitation,
                'exploitation_id' => $exploitation_id,
                'annee' => $annee,
            ]);
        }

        return $this->render('commercialisationproduits/new.html.twig', [
            'commercialisationProduit' => $commercialisationProduit,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'form' => $form->createView(),
            'elevage' => $elevage,
            'unites' => $unites,
        ]);
    }

    /**
     * Creates a form to delete a CommercialisationProduits entity.
     *
     * @param CommercialisationProduits $commercialisationProduit The CommercialisationProduits entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(CommercialisationProduits $commercialisationProduit, $options = [])
    {
        $exploitation = $commercialisationProduit->getExploitation();
        $annee = $commercialisationProduit->getAnnee();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_commercialisationproduits_delete', [
                'id' => $commercialisationProduit->getId(),
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
