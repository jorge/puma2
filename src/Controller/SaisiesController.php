<?php

declare(strict_types=1);

/*
 * This file is part of Puma2.
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puma2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License
 * along with Puma2.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\PivotExploitationOrganisations;
use App\Entity\Saisies\GetLocalisation;
use App\Entity\Saisies\GroupeAndLocalisation;
use App\Entity\Saisies\Localisation;
use App\Entity\Saisies\LocAndAdhesion;
use App\Entity\Saisies\Recherche;
use App\Entity\Saisies\Socies;
use App\Entity\TblOrganisations;
use App\Form\Saisies\LocalisationType;
use App\Form\Saisies\LocAndAdhesionType;
use App\Repository\AnimauxRepository;
use App\Repository\BatimentsRepository;
use App\Repository\CommercialisationProduitsRepository;
use App\Repository\ContraintesDeveloppementRepository;
use App\Repository\CotisationsRepository;
use App\Repository\DepensesMenageRepository;
use App\Repository\DettesRepository;
use App\Repository\ExploitationsRepository;
use App\Repository\FormationsBesoinRepository;
use App\Repository\FormationsRecuRepository;
use App\Repository\HabitatRepository;
use App\Repository\MoFamilialeExterneRepository;
use App\Repository\MoFamilialeInterneRepository;
use App\Repository\MoyensTransportRepository;
use App\Repository\OutillageRepository;
use App\Repository\PivotExploitationOrganisationsRepository;
use App\Repository\SecuriteAlimentaireRepository;
use App\Repository\ServicesBesoinRepository;
use App\Repository\TblCreditObjetRepository;
use App\Repository\TblDecoupageAdminRepository;
use App\Repository\TblOrganisationsRepository;
use App\Repository\TblTypeCommercialisationRepository;
use App\Repository\TblAchatAgricolesRepository;
use App\Repository\ExploitationChargesRepository;
use App\Service\NumeroMembre\GenerateurInterface;
use App\Synchro\SynchroCodeCommun;
use Doctrine\ORM\EntityManagerInterface;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Response;
use Throwable;

/**
 * @Route("/{_locale}/saisie", name="saisie_")
 */

use function count;

final class SaisiesController extends AbstractController
{
    private EntityManagerInterface $em;

    private GenerateurInterface $generateur;

    public function __construct(EntityManagerInterface $em, GenerateurInterface $generateur)
    {
        $this->em = $em;
        $this->generateur = $generateur;
    }

    public function _getInfoBD()
    {
        // TODO: Use QueryBuilder.
        $baseQuery = 'SELECT tc.denomination, COUNT(e.id) AS nombre ';
        $baseQuery .= 'FROM exploitations e ';
        $baseQuery .= 'INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id ';
        $baseQuery .= 'INNER JOIN tbl_organisations t ON t.id = p.organisation_id ';
        $baseQuery .= 'INNER JOIN tbl_organisations tc ON t.parent_id = tc.id ';
        $baseQuery .= 'WHERE e.radie=0 GROUP BY tc.denomination HAVING COUNT(e.id)>0';
        $stmt = $this->em
            ->getConnection()
            ->prepare($baseQuery);
        $stmt->execute();
        $results = $stmt->fetchAll();
        $msgInfoDB = 'Vous avez les cooperatives suivantes, avec le total des exploitations: ';

        foreach ($results as $row) {
            $msgInfoDB .= $row['denomination'] . ': ' . $row['nombre'] . ', ';
        }

        return $msgInfoDB;
    }

    public function _getRecordsetColline($colline)
    {
        $where = '';
        $colline_id = $colline->getId();

        if (null !== $colline_id) {
            $where = " AND e.localisationAdmin='" . $colline_id . "'";
        }

        $baseQuery = 'SELECT e.id,';
        $baseQuery .= ' e.numeroMembre,';
        $baseQuery .= "  e.anneeAdhesion, CONCAT(pch.nom,' ',pch.prenom) AS nom, ";
        $baseQuery .= ' pch.rolFamille, pch.cni, ';
        $baseQuery .= "(CASE WHEN (pch.radie=0) THEN 'Valide' ELSE 'Effacé' END) AS status,";
        $baseQuery .= ' (pch.dateNaissance) as annee_naiss, ';
        $baseQuery .= ' t.denomination as groupement,t.id as group_id,';
        $baseQuery .= ' tc.denomination as cooperative,tc.id as coop_id,';
        $baseQuery .= ' tda.denomination as colline';
        $baseQuery .= ' FROM App:Exploitations e';
        $baseQuery .= ' INNER JOIN App:PivotExploitationOrganisations p WITH e.id =p.exploitation ';
        $baseQuery .= ' INNER JOIN App:TblOrganisations t WITH t.id = p.organisation';
        $baseQuery .= ' INNER JOIN App:TblOrganisations tc WITH t.parent = tc.id';
        $baseQuery .= ' LEFT JOIN App:Personnes pch WITH e.id = pch.exploitation';
        $baseQuery .= ' LEFT JOIN App:TblDecoupageAdmin tda WITH e.localisationAdmin = tda.id';
        $baseQuery .= " WHERE e.radie=0 AND pch.nom<>''";
        $final = $baseQuery . $where . ' ORDER BY nom';

        $query = $this->em->createQuery();
        $query->setDql($final);

        return [
            'fields' => [
                'nom',
                'annee_naiss',
                'cni',
                'numeroMembre',
                'anneeAdhesion',
                'groupement',
                'cooperative',
            ],
            'titre' => 'lISTE DES EXPLOITATIONS LOCALISE DANS LA COLLINE ',
            'query' => $query,
        ];
    }

    public function _getRecordsetLocalisation($localisation)
    {
        $where = '';
        $colline = $localisation->getColline();
        $commune = $localisation->getCommune();
        $province = $localisation->getProvince();

        // TODO: Use QueryBuilder.
        if (null !== $colline) {
            $where = " AND e.localisationAdmin='" . $colline->getId() . "'";
        } elseif (null !== $commune) {
            $where = " AND (e.localisationAdmin = '" . $commune->getId() . "' OR tda.parent = '" . $commune->getId() . "')";
        } elseif (null !== $province) {
            $where = " AND (e.localisationAdmin = '" . $province->getId() . "' OR tda.parent = '" . $province->getId() . "' OR tda2.parent = '" . $province->getId() . "')";
        }

        $baseQuery = 'SELECT e.id,';
        $baseQuery .= ' e.numeroMembre AS NumeroMembre,';
        $baseQuery .= "  e.anneeAdhesion AS AnneeAdhesion, CONCAT(pch.prenom,' ',pch.nom) AS Nom, ";
        $baseQuery .= ' pch.rolFamille, pch.cni AS CNI, ';
        $baseQuery .= ' (pch.dateNaissance) AS AnneeNaissance, ';
        $baseQuery .= ' t.denomination AS Groupement,t.id AS group_id,';
        $baseQuery .= ' tc.denomination AS Cooperative,tc.id AS coop_id,';
        $baseQuery .= ' tda.denomination AS colline,tda2.denomination AS commune, tda3.denomination AS province';
        $baseQuery .= ' FROM App:Exploitations e';
        $baseQuery .= ' INNER JOIN App:PivotExploitationOrganisations p WITH e.id =p.exploitation ';
        $baseQuery .= ' INNER JOIN App:TblOrganisations t WITH t.id = p.organisation';
        $baseQuery .= ' INNER JOIN App:TblOrganisations tc WITH t.parent = tc.id';
        $baseQuery .= ' LEFT JOIN App:Personnes pch WITH e.id = pch.exploitation';
        $baseQuery .= ' LEFT JOIN App:TblDecoupageAdmin tda WITH e.localisationAdmin = tda.id';
        $baseQuery .= ' LEFT JOIN App:TblDecoupageAdmin tda2 WITH tda.parent = tda2.id';
        $baseQuery .= ' LEFT JOIN App:TblDecoupageAdmin tda3 WITH tda2.parent = tda3.id';
        $baseQuery .= ' WHERE e.radie=0';
        $final = $baseQuery . $where . ' ORDER BY e.numeroMembre';

        return [
            'fields' => [
                'Nom',
                'AnneeNaissance',
                'CNI',
                'NumeroMembre',
                'AnneeAdhesion',
                'id',
                'Cooperative',
                'Groupement',
            ],
            'titre' => 'Resultat de la rêquete ',
            'dql' => $final,
        ];
    }

    /**
     * Choose a colline for adding a new exploitation.
     *
     * @Route("/new/exploitation/getcolline/", name="new_exp_getcolline")
     */
    public function getCollineAction(Request $request, PaginatorInterface $paginator)
    {
        /** Création du formulaire  */
        $localisation = new Localisation();
        $form = $this->createForm(LocalisationType::class, $localisation);
        $form->handleRequest($request);

        $groupement = $request->query->get('groupement');

        $cooperative = $request->query->get('cooperative');

        if ($form->isSubmitted() && $form->isValid()) {
            $colline = $localisation->getColline();
            $result = $this->_getRecordsetColline($colline);

            $exploitations = $paginator->paginate(
                $result['query'],
                $request->query->get('page', 1),
                30
            );

            if (null !== $groupement) {
                $locAndAdhesion = new LocAndAdhesion();
                $form = $this->createForm(LocAndAdhesionType::class, $locAndAdhesion);

                return $this->render('saisies/LocAndAdhesion.html.twig', [
                    'form' => $form->createView(),
                    'groupement' => $groupement,
                    'cooperative' => $cooperative,
                    'pagination' => $exploitations,
                ]);
            }

            return $this->render('saisies/intermediaireNewExploi.html.twig', [
                'fields' => $result['fields'],
                'titre' => $result['titre'],
                'pagination' => $exploitations,
                'colline' => $colline,
            ]);
        }

        return $this->render(
            'saisies/getcolline.html.twig',
            ['form' => $form->createView()]
        );
    }

    // TODO fusionner avec TblOrganisations

    /**
     * Choose a cooperative  for adding a new groupement.
     *
     * @Route("/getcooperative", name="getcooperative")
     */
    public function getcooperativeAction(Request $request)
    {
        // Création du formulaire.
        $form = $this->createForm(
            'App\Form\Saisies\GetCooperativeType',
            null,
            [
                'em_manager' => $this->em, // Use Dependency Injection in GetCooperativeType::class
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute('references_tblorganisations_new', ['id' => $data['cooperative']->getId(), 'objet' => 'groupement']);
        }

        return $this->render('saisies/getcooperative.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Choose a coop + a groument for adding a new exploitation.
     *
     * @Route("/new/exploitation/getgroupeandloc", name="new_exp_getgroupeandlocalisation")
     */
    public function getgroupeandlocalisationAction(Request $request, TblDecoupageAdminRepository $tblDecoupageAdminRepository)
    {
        $localisation_id = $request->query->get('localisation');
        $localisation = $tblDecoupageAdminRepository->find($localisation_id);
        $groupandlocalisation = new GroupeAndLocalisation();
        $groupandlocalisation->setColline($localisation);
        $form = $this->createForm(
            'App\Form\Saisies\GetGroupeAndLocalisationType',
            $groupandlocalisation,
            ['em_manager' => $this->em, 'localisation' => $localisation]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $telephone = $data->getTelephone();
            $anneeAdhesion = $data->getAnneeAdhesion();
            $group_id = $data->getGroupement()->getID();
            /* création de l'exploitation */
            return $this->redirectToRoute(
                'saisie_newgetpersonne',
                [
                    'telephone' => $telephone,
                    'anneeAdhesion' => $anneeAdhesion,
                    'annee' => (int) 'Y',
                    'group_id' => $group_id,
                    'localisation_id' => $localisation_id,
                    'localisation' => $localisation,
                ]
            );
        }

        return $this->render('saisies/getgroupeandlocalisation.html.twig', [
            'form' => $form->createView(),
            'localisation' => $localisation,
        ]);
    }

    /**
     * Choose a cooperative and groupement  for adding a new exploitation.
     *
     * @Route("/getgroupement", name="getgroupement")
     */
    public function getgroupementAction(
        Request $request,
        ExploitationsRepository $exploitationsRepository,
        PivotExploitationOrganisationsRepository $pivotExploitationOrganisationsRepository
    ) {
        $user = $this->getUser();

        $exploitation_id = $request->query->get('exploitation_id');
        $annee = $request->query->get('annee');
        $form = $this->createForm(
            'App\Form\Saisies\GetGroupeType',
            null,
            [
                'em_manager' => $this->em,
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            /* sauver le pivot */
            $pivot = new \App\Entity\PivotExploitationOrganisations($user);
            $exploitation = $exploitationsRepository->find($exploitation_id);
            $pivot->setExploitation($exploitation);
            $pivot->setOrganisation($data->getGroupement());
            /** control si le pivot existe déjà */
            $controlPivot = $pivotExploitationOrganisationsRepository->findBy(['organisation' => $data->getGroupement(), 'exploitation' => $exploitation]);

            if (empty($controlPivot)) {
                $this->em->persist($pivot);
                $this->em->flush();
            }

            return $this->redirectToRoute('references_exploitations_edit', [
                'groupement' => $data->getGroupement(),
                'id' => $exploitation_id,
                'annee' => $annee,
            ]);
        }

        return $this->render('saisies/getcooperative.html.twig', [
            'form' => $form->createView(),
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Choose a coop + a groument for adding a new exploitation.
     *
     * @Route("/new/exploitation/getgroupewithloc", name="new_exp_getgroupewithlocalisation")
     *
     * TODO DELETE : n'est plus utilisée
     *
     * @param mixed $tblDecoupageAdminRepository
     */
    public function getgroupewithloc(Request $request, TblOrganisationsRepository $tblOrganisationsRepository, $tblDecoupageAdminRepository)
    {
        /**
         * Création du formulaire.
         */
        $localisation_id = $request->query->get('localisation');
        $groupe_id = $request->query->get('groupement');
        $groupement = $tblOrganisationsRepository->find($groupe_id);
        $cooperative = $groupement->getParent();
        $localisation = $tblDecoupageAdminRepository->find($localisation_id);
        $groupandlocalisation = new \App\Entity\Saisies\GroupeAndLocalisation();
        $groupandlocalisation->setColline($localisation);
        $groupandlocalisation->setCooperative($cooperative);
        $groupandlocalisation->setGroupement($groupement);

        $form = $this->createForm(
            'App\Form\Saisies\GetGroupeWithLocType',
            $groupandlocalisation,
            ['em_manager' => $this->em, 'localisation' => $localisation]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $telephone = $data->getTelephone();
            $anneeAdhesion = $data->getAnneeAdhesion();
            $group_id = $data->getGroupement()->getID();
            /* création de l'exploitation */
            return $this->redirectToRoute(
                'saisie_newgetpersonne',
                [
                    'telephone' => $telephone,
                    'anneeAdhesion' => $anneeAdhesion,
                    'annee' => (int) 'Y',
                    'group_id' => $group_id,
                    'localisation_id' => $localisation_id,
                    'localisation' => $localisation,
                ]
            );
        }

        return $this->render('saisies/getgroupeandlocalisation.html.twig', [
            'form' => $form->createView(),
            'localisation' => $localisation,
        ]);
    }

    /**
     * Choose a province + a commune for adding a new exploitation.
     *
     * @Route("/GetLocalisation", name="GetLocalisation")
     */
    public function GetLocalisationAction(
        Request $request,
        ExploitationsRepository $exploitationsRepository,
        TblDecoupageAdminRepository $tblDecoupageAdminRepository
    ) {
        /**
         * Création du formulaire.
         */
        $exploitation_id = $request->query->get('exploitation_id');
        $coop_id = $request->query->get('coop_id');
        $group_id = $request->query->get('group_id');
        $annee = $request->query->get('annee');
        $exploitation = $exploitationsRepository->find($exploitation_id);
        $localisation = new GetLocalisation($exploitation);

        $form = $this->createForm(
            'App\Form\Saisies\GetLocalisationType',
            $localisation,
            ['em_manager' => $this->em],
            [
                'action' => $this->generateUrl(
                    'references_exploitations_edit',
                    [
                        'id' => $exploitation->getId(),
                        'annee' => $annee,
                        'returnPath' => $request->query->get('returnPath', null),
                    ]
                ),
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $colline = $data->getColline();
            $decoupage = $tblDecoupageAdminRepository->find($localisation->getColline());
            $exploitation->setLocalisationAdmin($decoupage);
            $this->em->persist($exploitation);
            $this->em->flush();

            if ($request->query->has('returnPath')) {
                return $this->redirect($request->query->get('returnPath'));
            }

            return $this->redirectToRoute('references_exploitations_edit', [
                'id' => $exploitation_id,
                'exploitation_id' => $exploitation_id,
                'coop_id' => $coop_id,
                'annee' => $annee,
                'group_id' => $group_id,
                'colline_id' => $data->getColline()->getId(), ]);
        }

        return $this->render('saisies/GetLocalisation.html.twig', [
            'form' => $form->createView(),
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Choose a coop + a groument for adding a new exploitation.
     *
     * @Route("/exploitation/edit/GroupAdhesionTel/{pivot}/{exploitation}/", name="exp_GroupAdhesionTel")
     */
    public function GroupAdhesionTelAction(
        Request $request,
        PivotExploitationOrganisations $pivot
    ) {
        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();
        $exploitation = $pivot->getExploitation();
        $telephone = $exploitation->getTelephone();
        $anneeAdhesion = $exploitation->getAnneeAdhesion();
        $GroupAdhesionTel = new \App\Entity\Saisies\GroupAdhesionTel();
        $GroupAdhesionTel->setCooperative($coop);
        $GroupAdhesionTel->setGroupement($groupe);
        $GroupAdhesionTel->setAnneeAdhesion($anneeAdhesion);
        $GroupAdhesionTel->setTelephone($telephone);
        $GroupAdhesionTel->setCarteMembre($exploitation->getCarteMembre());
        $form = $this->createForm(
            'App\Form\Saisies\GroupAdhesionTelType',
            $GroupAdhesionTel,
            ['em_manager' => $this->em]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();
            $telephone = $data->getTelephone();
            $anneeAdhesion = $data->getAnneeAdhesion();
            $groupement = $data->getGroupement();
            $pivot->setOrganisation($groupement);
            $exploitation->setAnneeAdhesion($anneeAdhesion);
            $exploitation->setTelephone($telephone);
            $exploitation->setCarteMembre($data->getCarteMembre());
            $this->em->persist($pivot);
            $this->em->flush();
            $this->em->persist($exploitation);
            $this->em->flush();

            return $this->redirectToRoute(
                'references_exploitations_show_sans_annee',
                [
                    'id' => $exploitation->getId(),
                ]
            );
        }

        return $this->render('saisies/GroupAdhesionTel.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/info_gen/{exploitation}/", name="info_gen_sans_annee")
     * @Route("/info_gen/{exploitation}/{annee}", name="info_gen")
     *
     * @param mixed|null $annee
     */
    public function infoGenAction(Request $request, ?Exploitations $exploitation = null, ?int $annee = null)
    {
        if (null === $exploitation) {
            $msg = "Cette exploitation n'existe pas";
        } elseif ($exploitation->getRadie() === 1) {
            $msg = 'Cette exploitation a été effacé';
        } else {
            return $this->render('saisies/info_gen.html.twig', [
                'exploitation' => $exploitation,
                'annee' => $annee,
                'exploitation_id' => $exploitation->getId(),
                'annee_precedent' => null,
            ]);
        }

        return $this->render('saisies/info_gen_erreur.html.twig', [
            'message' => $msg,
        ]);
    }

    /**
     * Choose a colline for adding a new exploitation.
     *
     * @Route("/new/LocAndAdhesion/", name="new_LocAndAdhesion")
     */
    public function LocAndAdhesionAction(Request $request, PaginatorInterface $paginator)
    {
        /** Création du formulaire  */
        $LocAndAdhesion = new \App\Entity\Saisies\LocAndAdhesion();
        $form = $this->createForm('App\Form\Saisies\LocAndAdhesionType', $LocAndAdhesion);
        $form->handleRequest($request);
        $groupement = $request->query->get('groupement');
        $cooperative = $request->query->get('cooperative');

        if ($form->isSubmitted() && $form->isValid()) {
            $colline = $LocAndAdhesion->getColline();
            $result = $this->_getRecordsetColline($colline);

            $exploitations = $paginator->paginate(
                $result['query'],
                $request->query->get('page', 1),
                30
            );
            $anneeAdhesion = $LocAndAdhesion->getAnneeAdhesion();
            $telephone = $LocAndAdhesion->getTelephone();

            return $this->render('saisies/intermediaireNewExploi.html.twig', [
                'fields' => $result['fields'],
                'titre' => $result['titre'],
                'pagination' => $exploitations,
                'groupement' => $groupement,
                'cooperative' => $cooperative,
                'colline' => $colline,
                'anneeAdhesion' => $anneeAdhesion,
                'telephone' => $telephone,
            ]);
        }

        return $this->render(
            'saisies/LocAndAdhesion.html.twig',
            ['form' => $form->createView(),
                'groupement' => $groupement,
                'cooperative' => $cooperative,
            ]
        );
    }

    /**
     * @Route("/new/exploitation/newgetpersonne/", name="newgetpersonne")
     */
    public function newgetpersonneAction(
        Request $request,
        TblDecoupageAdminRepository $tblDecoupageAdminRepository,
        TblOrganisationsRepository $tblOrganisationsRepository
    ) {
        $user = $this->getUser();
        $telephone = $request->query->get('telephone');
        $anneeAdhesion = $request->query->get('anneeAdhesion');
        $group_id = $request->query->get('groupement');

        if (!$group_id) {
            $group_id = $request->query->get('group_id');
        }

        $localisation_id = $request->query->get('localisation');

        if (!$localisation_id) {
            $localisation_id = $request->query->get('localisation_id');
        }

        $socie = new Socies();
        $socie->setRepondant(true);
        $form = $this->createForm(
            'App\Form\Saisies\SociesType',
            $socie
        );
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exploitation = new \App\Entity\Exploitations($user);
            $pivot = new \App\Entity\PivotExploitationOrganisations($user);
            $personne = new \App\Entity\Personnes($user);
            $exploitation->setTelephone($telephone);
            $exploitation->setAnneeAdhesion($anneeAdhesion);
            $localisation = $tblDecoupageAdminRepository->find($localisation_id);
            $exploitation->setLocalisationAdmin($localisation);
            $exploitation->setAnneeAdhesion($anneeAdhesion);
            $exploitation->setTelephone($telephone);

            $isMaster = $this->getParameter('synchro_role') === SynchroCodeCommun::ROLE_MASTER;

            if ($isMaster) {
                $numeroMembre = $this->generateur->generateNextNumeroDeMembre();
                $exploitation->setNumeroMembre($numeroMembre);
            }

            // this code is in a db-transaction because
            // an exploitation whitout an associated personne is useless
            // (empty exploitations -> no research, ...)
            try {
                $this->em->getConnection()->beginTransaction(); // start transaction
                $this->em->persist($exploitation);
                $this->em->flush();

                $pivot->setExploitation($exploitation);
                $organisation = $tblOrganisationsRepository->Find($group_id);
                $pivot->setOrganisation($organisation);
                $this->em->persist($pivot);
                $this->em->flush();

                $socie = $form->getData();
                $personne->setNom(mb_strtoupper($socie->getNom()));
                $personne->setPrenom(mb_strtoupper($socie->getPrenom()));
                $personne->setEtatCivil($socie->getEtatCivil());
                $personne->setRolFamille($socie->getRolFamille());
                $personne->setSexe($socie->getSexe());
                $personne->setDateNaissance($socie->getDateNaissance());
                $personne->setCni($socie->getCni());
                $personne->setRepondant($socie->getRepondant());
                $personne->setMembreMenage($socie->getMembreMenage());
                $personne->setExploitation($exploitation);
                $this->em->persist($personne);
                $this->em->flush();
                $this->em->getConnection()->commit();
            } catch (Throwable $e) {
                $this->em->getConnection()->rollBack();

                throw $e;
            }

            return $this->redirectToRoute(
                'references_exploitations_show',
                [
                    'id' => $exploitation->getId(),
                    'annee' => (int) (date('Y')),
                ]
            );
        }

        return $this->render('saisies/socies.html.twig', [
            'form' => $form->createView(),
            'telephone' => $request->query->get('telephone'),
            'anneeAdhesion' => $request->query->get('anneeAdhesion'),
            'groupement' => $request->query->get('group_id'),
            'localisation' => $request->query->get('localisation'),
        ]);
    }

    /**
     * @Route("/recherche", name="recherche")
     */
    public function rechercheAction(Request $request)
    {
        $result = null; // resultat de la recherche si non null à afficher via recherche_exploitation_resultat
        // verification formualire recherche
        $saisieRecherche = new Recherche();
        $formRecherche = $this->createForm(
            'App\Form\Saisies\RechercheType',
            $saisieRecherche
        );

        $formRecherche->handleRequest($request);

        if ($formRecherche->isSubmitted() && $formRecherche->isValid()) {
            $result = $this->getRecordsetRecherche($saisieRecherche);
        }

        if (null !== $result) {
            $session = $request->getSession();
            $session->set('titre', $result['titre']);
            $session->set('dql', $result['dql']);
            $session->set('fields', $result['fields']);

            return $this->redirectToRoute('saisie_recherche_exploitation_resultat');
        }

        return $this->render('saisies/recherche.html.twig', [
            'form_recherche' => $formRecherche->createView(),
        ]);
    }

    /* recherche des exploitations en doublons */

    /**
     * @Route("/rechercheEnDouble", name="rechercheEnDouble")
     */
    public function rechercheEnDoubleAction(Request $request, TblOrganisationsRepository $tblOrganisationsRepository)
    {
        $result = null; // resultat de la recherche si non null à afficher via recherche_exploitation_resultat
        $saisieRecherche = new Recherche();
        $saisieRecherche->setNom($request->query->get('nom'));
        $saisieRecherche->setPrenom($request->query->get('prenom'));
        $groupe_id = $request->query->get('groupement');
        $groupement = $tblOrganisationsRepository->find($groupe_id);
        $saisieRecherche->setGroupement($groupement);
        $result = $this->getRecordsetRecherche($saisieRecherche);

        if (null !== $result) {
            $session = $request->getSession();
            $session->set('titre', $result['titre']);
            $session->set('dql', $result['dql']);
            $session->set('fields', $result['fields']);

            return $this->redirectToRoute('saisie_recherche_exploitation_resultat');
        }

        return $this->render('saisies/recherche.html.twig', [
            'form_recherche' => $formRecherche->createView(),
        ]);
    }

    /**
     * @Route("/recherche/resultats/", name="recherche_exploitation_resultat")
     *
     * Display the result of a search action (for exploitations). The result
     * is stored in a session
     */
    public function resultsAction(Request $request, PaginatorInterface $paginator)
    {
        $session = $request->getSession();
        /* affichage des infos sur coops avec des exploitations */
        if ($session->has('dql')) {
            $dql = $session->get('dql');
            $titre = $session->get('titre');
            $fields = $session->get('fields');
        } else {
            $this->addFlash(
                'error',
                'Pas de données à afficher'
            );

            return $this->redirectToRoute('homepage');
        }
        $stmt = $this->em
            ->getConnection()
            ->prepare($dql);
        $stmt->execute();
        $result = $stmt->fetchAll();
        $cpt = count($result);
        $titre .= "(Nombre d'exploitations: {$cpt})";
        $exploitations = $paginator->paginate(
            $result,
            intval($request->query->get('page', 1)),
            100
        );

        return $this->render('saisies/rec_recherche.html.twig', [
            'fields' => $fields,
            'titre' => $titre,
            'pagination' => $exploitations,
            'records' => $result,
        ]);
    }


    /**
     * @Route("/recherche/resultats/csv", name="recherche_exploitation_resultat_csv")
     *
     * Display the result of a search action (for exploitations). The result
     * is stored in a session
     */
    public function resultsCSVAction(Request $request)
    {
        $session = $request->getSession();
        /* affichage des infos sur coops avec des exploitations */
        if ($session->has('dql')) {
            $dql = $session->get('dql');
            $titre = $session->get('titre');
            $fields = $session->get('fields');
        }

        $stmt = $this->em
            ->getConnection()
            ->prepare($dql);
        $stmt->execute();
        $result = $stmt->fetchAll();

        $fp = fopen('php://temp', 'w');
        $has_header = FALSE;

        foreach ($result as $row) {
            if(! $has_header) {
                fputcsv($fp, array_keys($row));
                $has_header = TRUE;
            }

            fputcsv($fp, array_values($row));
        }


        rewind($fp);
        $response = new Response(stream_get_contents($fp));
        fclose($fp);

        $response->headers->set('Content-Type', 'text/csv');
        $response->headers->set('Content-Disposition', 'attachment; filename="export.csv"');

        return $response;
    }




    /**
     * @Route("/f0_membre/{id}/{annee}", name="f0_membre")
     */
    public function saisieF0MembreAction(
        int $annee,
        Exploitations $exploitation,
        DettesRepository $dettesRepository,
        FormationsRecuRepository $formationsRecuRepository,
        MoFamilialeExterneRepository $moFamilialeExterneRepository,
        MoFamilialeInterneRepository $moFamilialeInterneRepository,
        DepensesMenageRepository $depensesMenageRepository,
        SecuriteAlimentaireRepository $securiteAlimentaireRepository,
        FormationsBesoinRepository $formationsBesoinRepository,
        ContraintesDeveloppementRepository $contraintesDeveloppementRepository,
        ServicesBesoinRepository $servicesBesoinRepository,
        TblTypeCommercialisationRepository $tblTypeCommercialisationRepository,
        CommercialisationProduitsRepository $commercialisationProduitsRepository,
        HabitatRepository $habitatRepository,
        CotisationsRepository $cotisationsRepository
    ) {
        $pivots = $exploitation->getPivots();
        $groupement = $pivots->first()->getOrganisation();
        $cooperative = $groupement->getParent();
        $pivot = $pivots->first();

        $user = $this->getUser();

        /* fin de la signalétique */
        $formationsRecus = $formationsRecuRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $moFamilialeExternes = $moFamilialeExterneRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $moFamilialeInternes = $moFamilialeInterneRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);

        $depenses = $depensesMenageRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);

        $dettes_intrants = $dettesRepository->getIntrants($exploitation->getId(), $annee);
        $dettes_consommations = $dettesRepository->getConsommations($exploitation->getId(), $annee);
        $securiteAlimentaires = $securiteAlimentaireRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $formationsBesoins = $formationsBesoinRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $contraintesDeveloppements = $contraintesDeveloppementRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $servicesBesoins = $servicesBesoinRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);

        $typeCommercialisation = [];

        foreach (['warrante', 'vente groupe', 'transforme'] as $name) {
            $type = $tblTypeCommercialisationRepository->findByTypeCommercialisation($name);
            $typeCommercialisation[$name] = $type;
        }

        $warrantes = $commercialisationProduitsRepository->findBy(['exploitation' => $exploitation, 'typeCommercialisation' => $typeCommercialisation['warrante'], 'annee' => $annee, 'radie' => 0]);
        $groupes = $commercialisationProduitsRepository->findBy(['exploitation' => $exploitation, 'typeCommercialisation' => $typeCommercialisation['vente groupe'], 'annee' => $annee, 'radie' => 0]);
        $transformes = $commercialisationProduitsRepository->findBy(['exploitation' => $exploitation, 'typeCommercialisation' => $typeCommercialisation['transforme'], 'annee' => $annee, 'radie' => 0]);
        $habitats = $habitatRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $cotisations = $cotisationsRepository->findBy(['pivotExploitationOrganisation' => $pivot, 'annee' => $annee, 'radie' => 0]);

        // TODO: $chefdetails is never used.
        if (null !== $exploitation->getChefDeFamille()) {
            $chefdetails = $exploitation->getChefDeFamille()->getPersonneDetails()->first();
        } else {
            $chefdetails = null;
        }

        // TODO: $exercice is never used.
        $exercice = $exploitation->getExerciceFor($annee);

        return $this->render('saisies/membre_f0.html.twig', [
            'exploitation' => $exploitation,
            'formationsRecus' => $formationsRecus,
            'moFamilialeExternes' => $moFamilialeExternes,
            'moFamilialeInternes' => $moFamilialeInternes,
            'dettes_intrants' => $dettes_intrants,
            'dettes_consommations' => $dettes_consommations,
            'securiteAlimentaires' => $securiteAlimentaires,
            'formationsBesoins' => $formationsBesoins,
            'contraintesDeveloppements' => $contraintesDeveloppements,
            'servicesBesoins' => $servicesBesoins,
            'habitats' => $habitats,
            'cotisations' => $cotisations,
            'annee' => $annee,
            'exploitation_id' => $exploitation->getId(),
            'pivot' => $pivots->first(), // TODO ATTENTOION PAS AGE DE RETRAITE
            'depenses' => $depenses,
            'type_commercialisation' => $typeCommercialisation,
            'warrantes' => $warrantes,
            'groupes' => $groupes,
            'transformes' => $transformes,
        ]);
    }

    /**
     * @Route("/f1_membre/{id}/{annee}", name="f1_membre")
     *
     * @param mixed $annee
     */
    public function saisieF1MembreAction(
        Request $request,
        Exploitations $exploitation,
        BatimentsRepository $batimentsRepository,
        MoyensTransportRepository $moyensTransportRepository,
        OutillageRepository $outillageRepository,
        AnimauxRepository $animauxRepository,
        int $annee
    ) {
        $batiments = $batimentsRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $moyenstransport = $moyensTransportRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $outillage = $outillageRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);
        $parcelles = $exploitation->getParcellesFor($annee);
        $troupeaux = $animauxRepository->findBy(['exploitation' => $exploitation, 'annee' => $annee, 'radie' => 0]);

        return $this->render('saisies/membre_f1.html.twig', [
            'batiments' => $batiments,
            'moyenstransport' => $moyenstransport,
            'outillage' => $outillage,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'parcelles' => $parcelles,
            'troupeaux' => $troupeaux,
        ]);
    }

    /**
     * @Route("/f4_membre/{id}/{annee}", name="f4_membre")
     */
    public function saisieF4MembreAction(
        Exploitations $exploitation,
        TblCreditObjetRepository $tblCreditObjetRepository,
        PivotExploitationOrganisationsRepository $pivotExploitationOrganisationsRepository,
        DettesRepository $dettesRepository,
        int $annee
    ) {
        $pivot = $pivotExploitationOrganisationsRepository->findOneBy(
            ['exploitation' => $exploitation, 'radie' => 0]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $cons = $tblCreditObjetRepository->findByCreditobjet('consommation');
        $camp = $tblCreditObjetRepository->findByCreditobjet('campagne');
        $inve = $tblCreditObjetRepository->findByCreditobjet('investissement');
        $consommations = $dettesRepository->findBy(['exploitation' => $exploitation, 'creditobjet' => $cons, 'radie' => 0]);
        $campagnes = $dettesRepository->findBy(['exploitation' => $exploitation, 'creditobjet' => $camp, 'radie' => 0]);
        $investissements = $dettesRepository->findBy(['exploitation' => $exploitation, 'creditobjet' => $inve, 'radie' => 0]);

        return $this->render('saisies/membre_f4.html.twig', [
            'exploitation' => $exploitation,
            'consommations' => $consommations,
            'campagnes' => $campagnes,
            'investissements' => $investissements,
            'annee' => $annee,
            'exploitation_id' => $exploitation->getId(),
            'creditobjet_id' => '',
            'group_id' => $groupe->getId(),
            'coop_id' => $coop->getId(),
        ]);
    }

    /**
     * @Route("/f5_membre/{id}/{annee}", name="f5_membre")
     */
    public function saisieF5MembreAction(
        Request $request,
        Exploitations $exploitation,
        int $annee,
        PivotExploitationOrganisationsRepository $pivotExploitationOrganisationsRepository,
        CommercialisationProduitsRepository $commercialisationProduitsRepository,
        TblAchatAgricolesRepository $tblAchatAgricolesRepository,
        ExploitationChargesRepository $exploitationChargesRepository) : Response
    {
        $groupe = $exploitation->getGroupement();
        $coop = $groupe->getParent();

        /* prendres les entrées */
        $commercialisationVegetales = $commercialisationProduitsRepository->prendreVegetales($exploitation->getId(), $annee, 0);
        $commercialisationAnimales = $commercialisationProduitsRepository->prendreAnimales($exploitation->getId(), $annee, 0);
        /* prendre tous les objets des charges */
        $urs = $tblAchatAgricolesRepository->findBy(['achat' => 'Urée', 'typeAchat' => 'agricole']);
        $vets = $tblAchatAgricolesRepository->findBy(['achat' => 'Produits vétérinaire', 'typeAchat' => 'elevage']);
        $ds = $tblAchatAgricolesRepository->findBy(['achat' => 'DAP', 'typeAchat' => 'agricole']);
        $ks = $tblAchatAgricolesRepository->findBy(['achat' => 'KCI', 'typeAchat' => 'agricole']);
        $svs = $tblAchatAgricolesRepository->findBy(['achat' => 'Service vétérinaire', 'typeAchat' => 'elevage']);
        $ns = $tblAchatAgricolesRepository->findBy(['achat' => 'NPK', 'typeAchat' => 'agricole']);
        $t1s = $tblAchatAgricolesRepository->findBy(['achat' => 'Transport', 'typeAchat' => 'elevage']);
        $hs = $tblAchatAgricolesRepository->findBy(['achat' => 'Herbicides', 'typeAchat' => 'agricole']);
        $ps = $tblAchatAgricolesRepository->findBy(['achat' => 'Phytos', 'typeAchat' => 'agricole']);
        $as = $tblAchatAgricolesRepository->findBy(['achat' => 'Animaux', 'typeAchat' => 'elevage']);
        $t2s = $tblAchatAgricolesRepository->findBy(['achat' => 'Transport', 'typeAchat' => 'agricole']);
        $ls = $tblAchatAgricolesRepository->findBy(['achat' => 'Location de la terre', 'typeAchat' => 'agricole']);
        $es = $tblAchatAgricolesRepository->findBy(['achat' => 'Emballage', 'typeAchat' => 'agricole']);
        $m1s = $tblAchatAgricolesRepository->findBy(['achat' => 'Main d oeuvre externe', 'typeAchat' => 'agricole']);
        $ss = $tblAchatAgricolesRepository->findBy(['achat' => 'Semences', 'typeAchat' => 'agricole']);
        $ous = $tblAchatAgricolesRepository->findBy(['achat' => 'Achat/réparation matériel et outils', 'typeAchat' => 'materiel']);
        $m2s = $tblAchatAgricolesRepository->findBy(['achat' => 'Main d oeuvre externe', 'typeAchat' => 'elevage']);
        $cs = $tblAchatAgricolesRepository->findBy(['achat' => 'Compost-Fumier-Fiente', 'typeAchat' => 'agricole']);
        $is = $tblAchatAgricolesRepository->findBy(['achat' => 'intérêts', 'typeAchat' => 'interets']);
        $xs = $tblAchatAgricolesRepository->findBy(['achat' => 'Chaux', 'typeAchat' => 'agricole']);
        $aas = $tblAchatAgricolesRepository->findBy(['achat' => 'Achat aliments animaux', 'typeAchat' => 'elevage']);

        $locations = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $ls, 'radie' => 0]);
        $mains = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $m1s, 'radie' => 0]);
        $semences = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $ss, 'radie' => 0]);
        $compostes = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $cs, 'radie' => 0]);
        $chaus = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $xs, 'radie' => 0]);
        $urees = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $urs, 'radie' => 0]);
        $daps = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $ds, 'radie' => 0]);
        $kcis = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $ks, 'radie' => 0]);
        $npks = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $ns, 'radie' => 0]);
        $herbicides = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $hs, 'radie' => 0]);
        $phytos = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $ps, 'radie' => 0]);
        $transportintrants = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $t2s, 'radie' => 0]);
        $emballages = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $es, 'radie' => 0]);
        $mainintrants = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $m2s, 'radie' => 0]);
        $animales = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $as, 'radie' => 0]);
        $aliments = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $aas, 'radie' => 0]);
        $produitvetes = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $vets, 'radie' => 0]);
        $servicevetes = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $svs, 'radie' => 0]);
        $transportanimales = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $t1s, 'radie' => 0]);
        $materiels = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $ous, 'radie' => 0]);
        $interets = $exploitationChargesRepository->findBy(['exploitation' => $exploitation->getId(), 'annee' => $annee, 'achat' => $is, 'radie' => 0]);

        return $this->render('saisies/membre_f5.html.twig', [
            'exploitation' => $exploitation,
            'annee' => $annee,
            'group_id' => $groupe->getId(),
            'coop_id' => $coop->getId(),
            'exploitation_id' => $exploitation->getId(),
            'commercialisationVegetales' => $commercialisationVegetales,
            'commercialisationAnimales' => $commercialisationAnimales,
            'locations' => $locations,
            'mains' => $mains,
            'semences' => $semences,
            'compostes' => $compostes,
            'chaus' => $chaus,
            'urees' => $urees,
            'daps' => $daps,
            'kcis' => $kcis,
            'npks' => $npks,
            'herbicides' => $herbicides,
            'phytos' => $phytos,
            'transportintrants' => $transportintrants,
            'emballages' => $emballages,
            'mainintrants' => $mainintrants,
            'animales' => $animales,
            'aliments' => $aliments,
            'produitvetes' => $produitvetes,
            'servicevetes' => $servicevetes,
            'transportanimales' => $transportanimales,
            'materiels' => $materiels,
            'interets' => $interets,
        ]);
    }

    private function getRecordsetRecherche(Recherche $recherche): array
    {
        $where = [['e.radie=0']];

        if (null !== $anneeAdhesion = $recherche->getAnneeAdhesion()) {
            $where[] = ["AND e.annee_adhesion='%s'", $anneeAdhesion];
        }

        $numeroMembre = $recherche->getNumeroMembre();
        $numeroMembre = Exploitations::getNumeroMembreFromHumanNotation($numeroMembre);

        if (null !== $numeroMembre && 0 !== $numeroMembre) {
            $where[] = ["AND e.numero_membre='%s'", $numeroMembre];
        }

        if (null !== $nom = $recherche->getNom()) {
            $where[] = ["AND pe.nom LIKE '%s%'", $nom];
        }

        if (null !== $prenom = $recherche->getPrenom()) {
            $where[] = ["AND pe.prenom LIKE '%s%'", $prenom];
        }

        if (null !== $anneeNaissance = $recherche->getAnneeNaissance()) {
            $where[] = ["AND pe.date_naissance='%s'", $anneeNaissance];
        }

        if (null !== $cni = $recherche->getCni()) {
            $where[] = ["AND pe.cni='%s'", $cni];
        }

        if (null !== $cooperative = $recherche->getCooperative()) {
            $where[] = ["AND tc.id='%s'", $cooperative->getId()];
        }

        if (null !== $groupement = $recherche->getGroupement()) {
            $where[] = ["AND t.id='%s'", $groupement->getId()];
        }

        if (null !== $province = $recherche->getProvince()) {
            $where[] = ["AND tprov.id='%s'", $province->getId()];
        }

        if (null !== $commune = $recherche->getCommune()) {
            $where[] = ["AND tcom.id='%s'", $commune->getId()];
        }

        if (null !== $colline = $recherche->getColline()) {
            $where[] = ["AND tcol.id='%s'", $colline->getId()];
        }

        $baseQuery[] = 'SELECT e.id,';
        $baseQuery[] = 'e.utilisateur AS encodeur, e.numero_membre AS NumeroMembre,';
        $baseQuery[] = 'tprov.denomination AS Province,';
        $baseQuery[] = 'tcom.denomination AS Commune,';
        $baseQuery[] = 'tcol.denomination AS Colline,';
        $baseQuery[] = "(SELECT MAX(pch.date_naissance) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS date_naissance, ";
        $baseQuery[] = "(SELECT MAX(pch.cni) FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS cni, ";
        $baseQuery[] = "(SELECT GROUP_CONCAT(CONCAT(pch.prenom,' ',pch.nom) SEPARATOR '; ') FROM personnes pch WHERE pch.exploitation_id=e.id AND pch.radie=0 AND pch.rol_famille='Chef de famille' ) AS ChefMenage, ";
        $baseQuery[] = "(SELECT GROUP_CONCAT(CONCAT(pco.prenom,' ',pco.nom) SEPARATOR '; ') FROM personnes pco WHERE pco.exploitation_id=e.id AND pco.radie=0 AND pco.rol_famille='Conjoint' ) AS Conjoint, ";
        $baseQuery[] = 'MAX(t.denomination) AS Groupement,';
        $baseQuery[] = 'MAX(tc.denomination) AS Cooperative ';
        $baseQuery[] = 'FROM exploitations e';
        $baseQuery[] = 'INNER JOIN tbl_decoupage_admin tcol ON e.localisation_admin_id =tcol.id ';
        $baseQuery[] = 'INNER JOIN tbl_decoupage_admin tcom ON tcol.parent_id =tcom.id ';
        $baseQuery[] = 'INNER JOIN tbl_decoupage_admin tprov ON tcom.parent_id =tprov.id ';
        $baseQuery[] = 'INNER JOIN pivot_exploitation_organisations p ON e.id =p.exploitation_id ';
        $baseQuery[] = 'INNER JOIN tbl_organisations t ON t.id = p.organisation_id';
        $baseQuery[] = 'INNER JOIN tbl_organisations tc ON t.parent_id = tc.id';
        $baseQuery[] = 'LEFT JOIN personnes pe ON pe.exploitation_id = e.id';
        $baseQuery[] = 'WHERE';
        $baseQuery[] = implode(' ', array_map(static fn (array $data): string => sprintf(...$data), $where));
        $baseQuery[] = 'GROUP BY id ORDER BY e.numero_membre';

        // render the recordset in a new page? or into the same one?
        return [
            'fields' => [
                'NumeroMembre',
                'cni',
                'date_naissance',
                'ChefMenage',
                'Conjoint',
                'Cooperative',
                'Groupement',
                'Province',
                'Commune',
                'Colline',
                'encodeur',
            ],
            'titre' => 'Resultat de la rêquete ',
            'dql' => implode(' ', $baseQuery),
        ];
    }
}
