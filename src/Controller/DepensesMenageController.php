<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\DepensesMenage;
use App\Entity\Exploitations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * DepensesMenage controller.
 *
 * @Route("/{_locale}/references_depensesmenage")
 */

use Symfony\Component\Routing\Annotation\Route;

final class DepensesMenageController extends AbstractController
{
    /**
     * Deletes a DepensesMenage entity.
     *
     * @Route("/{id}", name="references_depensesmenage_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, DepensesMenage $depensesMenage)
    {
        $exploitation = $depensesMenage->getExploitation();
        $annee = $depensesMenage->getAnnee();

        $form = $this->createDeleteForm($depensesMenage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $depensesMenage->setRadie(TRUE);
            $em->persist($depensesMenage);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'annee' => $annee,
            'id' => $exploitation->getId(),
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Displays a form to edit an existing DepensesMenage entity.
     *
     * @Route("/{id}/edit", name="references_depensesmenage_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, DepensesMenage $depensesMenage)
    {
        $exploitation = $depensesMenage->getExploitation();
        $annee = $depensesMenage->getAnnee();
        $typeDep = $depensesMenage->getTypeDepense();
        $deleteForm = $this->createDeleteForm($depensesMenage);
        $editForm = $this->createForm('App\Form\DepensesMenageType', $depensesMenage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($depensesMenage);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('depensesmenage/edit.html.twig', [
            'depensesMenage' => $depensesMenage,
            'annee' => $annee,
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new DepensesMenage entity.
     *
     * @Route("/{exploitation}/{annee}/new/", name="references_depensesmenage_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Exploitations $exploitation, $annee)
    {
        $user = $this->getUser();
        $depensesMenage = new DepensesMenage($user);
        $depensesMenage->setExploitation($exploitation);
        $depensesMenage->setAnnee($annee);
        //    $depensesMenage->setTypeDepense($type);

        $form = $this->createForm('App\Form\DepensesMenageType', $depensesMenage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $depensesMenage->setRadie(FALSE);
            $em->persist($depensesMenage);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('depensesmenage/new.html.twig', [
            'depensesMenage' => $depensesMenage,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a DepensesMenage entity.
     *
     * @Route("/{id}", name="references_depensesmenage_show", methods={"GET"})
     */
    public function showAction(DepensesMenage $depensesMenage)
    {
        $annee = $depensesMenage->getAnnee();
        $exploitation = $depensesMenage->getExploitation();

        return $this->render('depensesmenage/show.html.twig', [
            'depensesMenage' => $depensesMenage,
            'annee' => $annee,
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Creates a form to delete a DepensesMenage entity.
     *
     * @param DepensesMenage $depensesMenage The DepensesMenage entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DepensesMenage $depensesMenage)
    {
        $exploitation = $depensesMenage->getExploitation();
        $annee = $depensesMenage->getAnnee();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_depensesmenage_delete', [
                'id' => $depensesMenage->getId(),
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
