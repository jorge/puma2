<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\FormationsRecu;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * FormationsRecu controller.
 *
 * @Route("/{_locale}/references/formationsrecu")
 */

use Symfony\Component\Routing\Annotation\Route;

final class FormationsRecuController extends AbstractController
{
    /**
     * Deletes a FormationsRecu entity.
     *
     * @Route("/{id}", name="references_formationsrecu_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, FormationsRecu $formationsRecu)
    {
        $exploitation = $formationsRecu->getExploitation();
        $annee = $formationsRecu->getAnnee();

        $form = $this->createDeleteForm($formationsRecu, [
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $formationsRecu->setRadie(TRUE);
            $em->persist($formationsRecu);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'annee' => $annee,
            'id' => $exploitation->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing FormationsRecu entity.
     *
     * @Route("/{id}/edit", name="references_formationsrecu_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, FormationsRecu $formationsRecu)
    {
        $exploitation = $formationsRecu->getExploitation();
        $annee = $formationsRecu->getAnnee();
        $em = $this->getDoctrine()->getManager();

        $deleteForm = $this->createDeleteForm($formationsRecu);
        $editForm = $this->createForm('App\Form\FormationsRecuType', $formationsRecu);

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formationsRecu);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('formationsrecu/edit.html.twig', [
            'formationsRecu' => $formationsRecu,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all FormationsRecu entities par exploitation.
     *
     * @Route("/exploitation/{id}", name="references_formationsrecu_exploitation_list", methods={"GET"})
     * TODO supprimer ?
     */
    public function exploitationListAction(Request $request, Exploitations $exploitation)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $em = $this->getDoctrine()->getManager();
        $formationsRecus = $em->getRepository(FormationsRecu::class)->findBy([
            'exploitation' => $exploitation,
            'radie' => 0, ]);

        return $this->render('formationsrecu/exploitationList.html.twig', [
            'formationsRecus' => $formationsRecus,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
            //          'group_id' => $group_id,
            //          'coop_id'=>$coop_id
        ]);
    }

    /**
     * Creates a new FormationsRecu entity.
     *
     * @Route("/{exploitation}/{annee}/new", name="references_formationsrecu_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Exploitations $exploitation, $annee)
    {
        $user = $this->getUser();
        $formationsRecu = new FormationsRecu($user);
        $formationsRecu->setExploitation($exploitation);
        $formationsRecu->setAnnee($annee);

        $form = $this->createForm('App\Form\FormationsRecuType', $formationsRecu);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $formationsRecu->setRadie(FALSE);
            $em->persist($formationsRecu);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('formationsrecu/new.html.twig', [
            'annee' => $annee,
            'exploitation' => $exploitation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a FormationsRecu entity.
     *
     * @Route("/{id}", name="references_formationsrecu_show", methods={"GET"})
     */
    public function showAction(FormationsRecu $formationsRecu)
    {
        $exploitation = $formationsRecu->getExploitation();
        $annee = $formationsRecu->getAnnee();
        $deleteForm = $this->createDeleteForm($formationsRecu);

        return $this->render('formationsrecu/show.html.twig', [
            'formationsRecu' => $formationsRecu,
            'annee' => $annee,
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Creates a form to delete a FormationsRecu entity.
     *
     * @param FormationsRecu $formationsRecu The FormationsRecu entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FormationsRecu $formationsRecu, $options = [])
    {
        $exploitation = $formationsRecu->getExploitation();
        $annee = $formationsRecu->getAnnee();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_formationsrecu_delete', [
                'id' => $formationsRecu->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
