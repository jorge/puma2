<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblEquipementsAgricoles;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblEquipementsAgricoles controller.
 *
 * @Route("/{_locale}/references/tblequipementsagricoles")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblEquipementsAgricolesController extends AbstractController
{
    /**
     * Deletes a TblEquipementsAgricoles entity.
     *
     * @Route("/{id}", name="references_tblequipementsagricoles_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblEquipementsAgricoles $tblEquipementsAgricole)
    {
        $form = $this->createDeleteForm($tblEquipementsAgricole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblEquipementsAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblequipementsagricoles_index');
    }

    /**
     * Displays a form to edit an existing TblEquipementsAgricoles entity.
     *
     * @Route("/{id}/edit", name="references_tblequipementsagricoles_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblEquipementsAgricoles $tblEquipementsAgricole)
    {
        $deleteForm = $this->createDeleteForm($tblEquipementsAgricole);
        $editForm = $this->createForm('App\Form\TblEquipementsAgricolesType', $tblEquipementsAgricole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblEquipementsAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblequipementsagricoles_index', ['id' => $tblEquipementsAgricole->getId()]);
        }

        return $this->render('tblequipementsagricoles/edit.html.twig', [
            'tblEquipementsAgricole' => $tblEquipementsAgricole,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblEquipementsAgricoles entities.
     *
     * @Route("/", name="references_tblequipementsagricoles_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblEquipementsAgricoles = $em->getRepository(TblEquipementsAgricoles::class)->findAll();

        return $this->render('tblequipementsagricoles/index.html.twig', [
            'tblEquipementsAgricoles' => $tblEquipementsAgricoles,
        ]);
    }

    /**
     * Creates a new TblEquipementsAgricoles entity.
     *
     * @Route("/new", name="references_tblequipementsagricoles_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblEquipementsAgricole = new TblEquipementsAgricoles($user);
        $form = $this->createForm('App\Form\TblEquipementsAgricolesType', $tblEquipementsAgricole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblEquipementsAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblequipementsagricoles_index', ['id' => $tblEquipementsAgricole->getId()]);
        }

        return $this->render('tblequipementsagricoles/new.html.twig', [
            'tblEquipementsAgricole' => $tblEquipementsAgricole,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblEquipementsAgricoles entity.
     *
     * @Route("/{id}", name="references_tblequipementsagricoles_show", methods={"GET"})
     */
    public function showAction(TblEquipementsAgricoles $tblEquipementsAgricole)
    {
        $deleteForm = $this->createDeleteForm($tblEquipementsAgricole);

        return $this->render('tblequipementsagricoles/show.html.twig', [
            'tblEquipementsAgricole' => $tblEquipementsAgricole,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblEquipementsAgricoles entity.
     *
     * @param TblEquipementsAgricoles $tblEquipementsAgricole The TblEquipementsAgricoles entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblEquipementsAgricoles $tblEquipementsAgricole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblequipementsagricoles_delete', ['id' => $tblEquipementsAgricole->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
