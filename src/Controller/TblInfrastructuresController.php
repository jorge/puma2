<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblInfrastructures;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblInfrastructures controller.
 *
 * @Route("/{_locale}/references/tblinfrastructures")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblInfrastructuresController extends AbstractController
{
    /**
     * Deletes a TblInfrastructures entity.
     *
     * @Route("/{id}", name="references_tblinfrastructures_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblInfrastructures $tblInfrastructure)
    {
        $form = $this->createDeleteForm($tblInfrastructure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblInfrastructure);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblinfrastructures_index');
    }

    /**
     * Displays a form to edit an existing TblInfrastructures entity.
     *
     * @Route("/{id}/edit", name="references_tblinfrastructures_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblInfrastructures $tblInfrastructure)
    {
        $deleteForm = $this->createDeleteForm($tblInfrastructure);
        $editForm = $this->createForm('App\Form\TblInfrastructuresType', $tblInfrastructure);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblInfrastructure);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblinfrastructures_index', ['id' => $tblInfrastructure->getId()]);
        }

        return $this->render('tblinfrastructures/edit.html.twig', [
            'tblInfrastructure' => $tblInfrastructure,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblInfrastructures entities.
     *
     * @Route("/", name="references_tblinfrastructures_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblInfrastructures = $em->getRepository(TblInfrastructures::class)->findAll();

        return $this->render('tblinfrastructures/index.html.twig', [
            'tblInfrastructures' => $tblInfrastructures,
        ]);
    }

    /**
     * Creates a new TblInfrastructures entity.
     *
     * @Route("/new", name="references_tblinfrastructures_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblInfrastructure = new TblInfrastructures($user);
        $form = $this->createForm('App\Form\TblInfrastructuresType', $tblInfrastructure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblInfrastructure);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblinfrastructures_index', ['id' => $tblInfrastructure->getId()]);
        }

        return $this->render('tblinfrastructures/new.html.twig', [
            'tblInfrastructure' => $tblInfrastructure,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblInfrastructures entity.
     *
     * @Route("/{id}", name="references_tblinfrastructures_show", methods={"GET"})
     */
    public function showAction(TblInfrastructures $tblInfrastructure)
    {
        $deleteForm = $this->createDeleteForm($tblInfrastructure);

        return $this->render('tblinfrastructures/show.html.twig', [
            'tblInfrastructure' => $tblInfrastructure,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblInfrastructures entity.
     *
     * @param TblInfrastructures $tblInfrastructure The TblInfrastructures entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblInfrastructures $tblInfrastructure)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblinfrastructures_delete', ['id' => $tblInfrastructure->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
