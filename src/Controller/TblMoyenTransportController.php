<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblMoyenTransport;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblMoyenTransport controller.
 *
 * @Route("/{_locale}/references/tblmoyentransport")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblMoyenTransportController extends AbstractController
{
    /**
     * Deletes a TblMoyenTransport entity.
     *
     * @Route("/{id}", name="references_tblmoyentransport_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblMoyenTransport $tblMoyenTransport)
    {
        $form = $this->createDeleteForm($tblMoyenTransport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblMoyenTransport);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblmoyentransport_index');
    }

    /**
     * Displays a form to edit an existing TblMoyenTransport entity.
     *
     * @Route("/{id}/edit", name="references_tblmoyentransport_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblMoyenTransport $tblMoyenTransport)
    {
        $deleteForm = $this->createDeleteForm($tblMoyenTransport);
        $editForm = $this->createForm('App\Form\TblMoyenTransportType', $tblMoyenTransport);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblMoyenTransport);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblmoyentransport_index', ['id' => $tblMoyenTransport->getId()]);
        }

        return $this->render('tblmoyentransport/edit.html.twig', [
            'tblMoyenTransport' => $tblMoyenTransport,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblMoyenTransport entities.
     *
     * @Route("/", name="references_tblmoyentransport_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblMoyenTransports = $em->getRepository(TblMoyenTransport::class)->findAll();

        return $this->render('tblmoyentransport/index.html.twig', [
            'tblMoyenTransports' => $tblMoyenTransports,
        ]);
    }

    /**
     * Creates a new TblMoyenTransport entity.
     *
     * @Route("/new", name="references_tblmoyentransport_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblMoyenTransport = new TblMoyenTransport($user);
        $form = $this->createForm('App\Form\TblMoyenTransportType', $tblMoyenTransport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblMoyenTransport);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblmoyentransport_index', ['id' => $tblMoyenTransport->getId()]);
        }

        return $this->render('tblmoyentransport/new.html.twig', [
            'tblMoyenTransport' => $tblMoyenTransport,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblMoyenTransport entity.
     *
     * @Route("/{id}", name="references_tblmoyentransport_show", methods={"GET"})
     */
    public function showAction(TblMoyenTransport $tblMoyenTransport)
    {
        $deleteForm = $this->createDeleteForm($tblMoyenTransport);

        return $this->render('tblmoyentransport/show.html.twig', [
            'tblMoyenTransport' => $tblMoyenTransport,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblMoyenTransport entity.
     *
     * @param TblMoyenTransport $tblMoyenTransport The TblMoyenTransport entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblMoyenTransport $tblMoyenTransport)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblmoyentransport_delete', ['id' => $tblMoyenTransport->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
