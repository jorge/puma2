<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblRapports;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblRapports controller.
 *
 * @Route("/{_locale}/references/tblrapports")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblRapportsController extends AbstractController
{
    /**
     * Deletes a TblRapports entity.
     *
     * @Route("/{id}", name="references_tblrapports_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblRapports $tblRapport)
    {
        $form = $this->createDeleteForm($tblRapport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblRapport);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblrapports_index');
    }

    /**
     * Displays a form to edit an existing TblRapports entity.
     *
     * @Route("/{id}/edit", name="references_tblrapports_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblRapports $tblRapport)
    {
        $deleteForm = $this->createDeleteForm($tblRapport);
        $editForm = $this->createForm('App\Form\TblRapportsType', $tblRapport);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblRapport);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblrapports_index', ['id' => $tblRapport->getId()]);
        }

        return $this->render('tblrapports/edit.html.twig', [
            'tblRapport' => $tblRapport,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblRapports entities.
     *
     * @Route("/", name="references_tblrapports_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblRapports = $em->getRepository(TblRapports::class)->findAll();

        return $this->render('tblrapports/index.html.twig', [
            'tblRapports' => $tblRapports,
        ]);
    }

    /**
     * Creates a new TblRapports entity.
     *
     * @Route("/new", name="references_tblrapports_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblRapport = new TblRapports($user);
        $form = $this->createForm('App\Form\TblRapportsType', $tblRapport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblRapport);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblrapports_index', ['id' => $tblRapport->getId()]);
        }

        return $this->render('tblrapports/new.html.twig', [
            'tblRapport' => $tblRapport,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblRapports entity.
     *
     * @Route("/{id}", name="references_tblrapports_show", methods={"GET"})
     */
    public function showAction(TblRapports $tblRapport)
    {
        $deleteForm = $this->createDeleteForm($tblRapport);

        return $this->render('tblrapports/show.html.twig', [
            'tblRapport' => $tblRapport,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblRapports entity.
     *
     * @param TblRapports $tblRapport The TblRapports entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblRapports $tblRapport)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblrapports_delete', ['id' => $tblRapport->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
