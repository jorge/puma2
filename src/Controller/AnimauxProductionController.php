<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\AnimauxProduction;
use App\Entity\Exploitations;
use App\Entity\Animaux;
use App\Entity\TblAnimauxProduction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * AnimauxProduction controller.
 *
 * @Route("/{_locale}/references/animauxproduction")
 */

use Symfony\Component\Routing\Annotation\Route;

final class AnimauxProductionController extends AbstractController
{
    /**
     * Deletes a AnimauxProduction entity.
     *
     * @Route("/{id}", name="references_animauxproduction_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, AnimauxProduction $animauxProduction)
    {
        $form = $this->createDeleteForm($animauxProduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $animauxProduction->setRadie(TRUE);
            $em->persist($animauxProduction);
            $em->flush();
        }

        return $this->redirectToRoute('references_animaux_show', [
            'id' => $animauxProduction->getAnimaux()->getId(),
            'exploitation' => $animauxProduction->getAnimaux()->getExploitation(),
        ]);
    }

    /**
     * Displays a form to edit an existing AnimauxProduction entity.
     *
     * @Route("/{id}/edit", name="references_animauxproduction_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, AnimauxProduction $animauxProduction)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $animauxProduction->getAnnee();
        $animaux = $animauxProduction->getAnimaux();
        $exploitation_id = $animaux->getExploitation();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        //    $animaux = $em->getRepository(Animaux::class)->find($animaux_id);

        $deleteForm = $this->createDeleteForm($animauxProduction);
        $editForm = $this->createForm('App\Form\AnimauxProductionType', $animauxProduction);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($animauxProduction);
            $em->flush();

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux->getId(),
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('animauxproduction/edit.html.twig', [
            'animauxProduction' => $animauxProduction,
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
            'id' => $animaux->getId(),
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all AnimauxProduction entities.
     *
     * @Route("/", name="references_animauxproduction_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $animauxProductions = $em->getRepository(AnimauxProduction::class)->findAll();

        return $this->render('animauxproduction/index.html.twig', [
            'animauxProductions' => $animauxProductions,
        ]);
    }

    /**
     * Creates a new AnimauxProduction entity.
     *
     * @Route("/new", name="references_animauxproduction_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $animaux_id = $request->query->get('animaux');
        $listeunite = $request->query->get('listeunite');
        $annee = $request->query->get('annee');
        $exploitation_id = $request->query->get('exploitation');

        $em = $this->getDoctrine()->getManager();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $animaux = $em->getRepository(Animaux::class)->find($animaux_id);
        $prod = $em->getRepository(TblAnimauxProduction::class)->findOneBy(['production' => $listeunite]);

        if ('lait' === $listeunite) {
            $liste = ['litre' => 'litre', 'Fbu' => 'Fbu'];
        } elseif ('oeufs' === $listeunite) {
            $liste = ['piece' => 'piece', 'Fbu' => 'Fbu'];
        } elseif ('fumier' === $listeunite) {
            $liste = ['Fbu' => 'Fbu'];
        }
        $user = $this->getUser();
        $animauxProduction = new AnimauxProduction($user);
        $animauxProduction->setAnimaux($animaux);
        $animauxProduction->setAnnee($annee);
        $animauxProduction->setProduction($prod);
        $form = $this->createForm('App\Form\AnimauxProductionType', $animauxProduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $animauxProduction->setRadie(FALSE);
            $em->persist($animauxProduction);
            $em->flush();

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux_id,
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('animauxproduction/new.html.twig', [
            'animauxProduction' => $animauxProduction,
            'id' => $animaux_id,
            'exploitation_id' => $exploitation->getId(),
            'annee' => $annee,
            'exploitation' => $exploitation,
            'liste' => $liste,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a AnimauxProduction entity.
     *
     * @Route("/{id}", name="references_animauxproduction_show", methods={"GET"})
     */
    public function showAction(AnimauxProduction $animauxProduction)
    {
        $deleteForm = $this->createDeleteForm($animauxProduction);

        return $this->render('animauxproduction/show.html.twig', [
            'animauxProduction' => $animauxProduction,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a AnimauxProduction entity.
     *
     * @param AnimauxProduction $animauxProduction The AnimauxProduction entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AnimauxProduction $animauxProduction)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_animauxproduction_delete', ['id' => $animauxProduction->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
