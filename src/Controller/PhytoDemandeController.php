<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\PhytoDemande;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * PhytoDemande controller.
 *
 * @Route("/{_locale}/references/phytodemande")
 */

use Symfony\Component\Routing\Annotation\Route;

final class PhytoDemandeController extends AbstractController
{
    /**
     * Deletes a PhytoDemande entity.
     *
     * @Route("/{id}", name="references_phytodemande_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, PhytoDemande $phytoDemande)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $form = $this->createDeleteForm($phytoDemande, [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $phytoDemande->setRadie(TRUE);
            $em->persist($phytoDemande);
            $em->flush();
        }

        return $this->redirectToRoute('references_phytodemande_index', [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing PhytoDemande entity.
     *
     * @Route("/{id}/edit", name="references_phytodemande_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, PhytoDemande $phytoDemande)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $deleteForm = $this->createDeleteForm($phytoDemande);
        $editForm = $this->createForm('App\Form\PhytoDemandeType', $phytoDemande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($phytoDemande);
            $em->flush();

            return $this->redirectToRoute('references_phytodemande_edit', ['id' => $phytoDemande->getId()]);
        }

        return $this->render('phytodemande/edit.html.twig', [
            'phytoDemande' => $phytoDemande,
            'edit_form' => $editForm->createView(),
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all PhytoDemande entities.
     *
     * @Route("/", name="references_phytodemande_index", methods={"GET"})
     */
    public function indexAction()
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $em = $this->getDoctrine()->getManager();

        $phytoDemandes = $em->getRepository(PhytoDemande::class)->findAll();

        return $this->render('phytodemande/index.html.twig', [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'phytoDemandes' => $phytoDemandes,
        ]);
    }

    /**
     * Creates a new PhytoDemande entity.
     *
     * @Route("/new", name="references_phytodemande_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $user = $this->getUser();
        $phytoDemande = new PhytoDemande($user);
        $form = $this->createForm('App\Form\PhytoDemandeType', $phytoDemande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($phytoDemande);
            $em->flush();

            return $this->redirectToRoute('references_phytodemande_show', ['id' => $phytoDemande->getId()]);
        }

        return $this->render('phytodemande/new.html.twig', [
            'phytoDemande' => $phytoDemande,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a PhytoDemande entity.
     *
     * @Route("/{id}", name="references_phytodemande_show", methods={"GET"})
     */
    public function showAction(PhytoDemande $phytoDemande)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $deleteForm = $this->createDeleteForm($phytoDemande);

        return $this->render('phytodemande/show.html.twig', [
            'phytoDemande' => $phytoDemande,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a PhytoDemande entity.
     *
     * @param PhytoDemande $phytoDemande The PhytoDemande entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PhytoDemande $phytoDemande, $options = [])
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'references_phytodemande_delete',
                ['id' => $phytoDemande->getId(),
                    'coop_id' => $coop_id,
                    'group_id' => $group_id,
                    'exploitation_id' => $exploitation_id,
                    'annee' => $annee,
                ]
            ))
            ->setMethod('DELETE')
            ->getForm();
    }
}
