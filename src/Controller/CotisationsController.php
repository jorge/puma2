<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Cotisations;
use App\Entity\PivotExploitationOrganisations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Cotisations controller.
 *
 * @Route("/{_locale}/references/cotisations")
 */

use Symfony\Component\Routing\Annotation\Route;

final class CotisationsController extends AbstractController
{
    /**
     * Deletes a Cotisations entity.
     *
     * @Route("/{id}", name="references_cotisations_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Cotisations $cotisation)
    {
        $annee = $cotisation->getAnnee();
        $exploitation = $cotisation->getPivotExploitationOrganisation()->getExploitation();
        $form = $this->createDeleteForm($cotisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $cotisation->setRadie(TRUE);
            $em->persist($cotisation);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'annee' => $annee,
            'id' => $exploitation->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing Cotisations entity.
     *
     * @Route("/{id}/edit", name="references_cotisations_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Cotisations $cotisation)
    {
        $exploitation = $cotisation->getPivotExploitationOrganisation()->getExploitation();
        $annee = $cotisation->getAnnee();

        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($cotisation);
        $editForm = $this->createForm('App\Form\CotisationsType', $cotisation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cotisation);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('cotisations/edit.html.twig', [
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'annee' => $annee,
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Creates a new Cotisations entity.
     *
     * @Route("/new/{pivot}/{annee}", name="references_cotisations_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, PivotExploitationOrganisations $pivot, $annee)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $cotisation = new Cotisations($user);

        $cotisation->setPivotExploitationOrganisation($pivot);
        $cotisation->setAnnee($annee);

        $form = $this->createForm('App\Form\CotisationsType', $cotisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($cotisation);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $pivot->getExploitation()->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('cotisations/new.html.twig', [
            'cotisation' => $cotisation,
            'form' => $form->createView(),
            'annee' => $annee,
            'exploitation' => $pivot->getExploitation(),
        ]);
    }

    /**
     * Creates a form to delete a Cotisations entity.
     *
     * @param Cotisations $cotisation The Cotisations entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Cotisations $cotisation, $options = [])
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_cotisations_delete', [
                'id' => $cotisation->getId(), ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
