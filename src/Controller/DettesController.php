<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Dettes;
use App\Entity\Exploitations;
use App\Entity\TblCreditObjet;
use function defined;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Dettes controller.
 *
 * @Route("/{_locale}/references/dettes")
 */
final class DettesController extends AbstractController
{
    /**
     * Deletes a Dettes entity.
     *
     * @Route("/{id}", name="references_dettes_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Dettes $dette)
    {
        $exploitation = $dette->getExploitation();
        $annee = $dette->getAnnee();
        $form = $this->createDeleteForm($dette, [
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $dette->setRadie(true);
            $em->persist($dette);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f4_membre', [
            'id' => $exploitation->getId(),
            'annee' => $annee,
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing Dettes entity.
     *
     * @Route("/{id}/edit", name="references_dettes_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Dettes $dette)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $dette->getExploitation();
        $annee = $dette->getAnnee();
        $deleteForm = $this->createDeleteForm($dette);

        $autre = $request->query->get('autre');

        if (isset($autre)) {
            if ('intrants' === $autre) {
                $editForm = $this->createForm('App\Form\DettesIntransType', $dette);
            } elseif ('consommation' === $autre) {
                /* à reviser */
                $creobj = $em->getRepository(TblCreditObjet::class)->findOneByCreditobjet($autre);
                $dette->setCreditobjet($creobj);
                /* fin */
                $editForm = $this->createForm('App\Form\DettesConsommationsType', $dette);
            }
        } else {
            $editForm = $this->createForm('App\Form\DettesType', $dette);
        }

        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($dette);
            $em->flush();

            if (isset($autre)) {
                return $this->redirectToRoute('saisie_f0_membre', [
                    'id' => $exploitation->getId(),
                    'exploitation' => $exploitation,
                    'annee' => $annee,
                ]);
            }

            return $this->redirectToRoute('saisie_f4_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
                'exploitation' => $exploitation,
            ]);
        }

        return $this->render('dettes/edit.html.twig', [
            'dette' => $dette,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new Dettes entity.
     *
     * @Route("/new", name="references_dettes_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $exploitation_id = $request->query->get('exploitation_id');
        $annee = $request->query->get('annee');
        $autre = $request->query->get('autre');

        $creditobjet = $request->query->get('creditobjet');
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $dette = new Dettes($user);

        if (isset($exploitation_id)) {
            $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
            $dette->setExploitation($exploitation);
        }

        if (isset($annee)) {
            $dette->setAnnee($annee);
        }

        if (isset($creditobjet)) {
            $creobj = $em->getRepository(TblCreditObjet::class)->findOneByCreditobjet($creditobjet);
            $dette->setCreditobjet($creobj);
        } else {
            $dette->setCreditobjet(null);
        }

        if (isset($autre)) {
            if ('intrants' === $autre) {
                $form = $this->createForm('App\Form\DettesIntransType', $dette);
            } elseif ('consommation' === $autre) {
                /* à reviser */
                $creobj = $em->getRepository(TblCreditObjet::class)->findOneByCreditobjet($autre);
                $dette->setCreditobjet($creobj);
                /* fin */
                $form = $this->createForm('App\Form\DettesConsommationsType', $dette);
            }
        } else {
            $form = $this->createForm('App\Form\DettesType', $dette);
        }
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $dette->setRadie(false);
            $em->persist($dette);
            $em->flush();

            if (isset($autre)) {
                return $this->redirectToRoute('saisie_f0_membre', [
                    'id' => $exploitation_id,
                    'exploitation_id' => $exploitation_id,
                    'exploitation' => $exploitation,
                    'annee' => $annee, ]);
            }

            return $this->redirectToRoute('saisie_f4_membre', [
                'id' => $exploitation_id,
                'exploitation_id' => $exploitation_id,
                'exploitation' => $exploitation,
                'annee' => $annee, ]);
        }

        return $this->render('dettes/new.html.twig', [
            'dette' => $dette,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation_id,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Dettes entity.
     *
     * @Route("/{id}", name="references_dettes_show", methods={"GET"})
     */
    public function showAction(Dettes $dette)
    {
        $exploitation = $dette->getExploitation();
        $annee = $dette->getAnnee();
        $deleteForm = $this->createDeleteForm($dette);

        return $this->render('dettes/show.html.twig', [
            'dette' => $dette,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a Dettes entity.
     *
     * @param Dettes $dette   The Dettes entity
     * @param mixed  $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Dettes $dette, $options = [])
    {
        $exploitation = $dette->getExploitation();
        $annee = $dette->getId();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_dettes_delete', [
                'id' => $dette->getId(),
                'exploitation' => $exploitation,
                'exploitation_id' => $exploitation->getId(),
                'annee' => $annee,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
