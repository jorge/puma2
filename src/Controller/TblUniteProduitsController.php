<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblUniteProduits;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblUniteProduits controller.
 *
 * @Route("/{_locale}/references/tbluniteproduits")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblUniteProduitsController extends AbstractController
{
    /**
     * Deletes a TblUniteProduits entity.
     *
     * @Route("/{id}", name="references_tbluniteproduits_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblUniteProduits $tblUniteProduit)
    {
        $form = $this->createDeleteForm($tblUniteProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblUniteProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbluniteproduits_index');
    }

    /**
     * Displays a form to edit an existing TblUniteProduits entity.
     *
     * @Route("/{id}/edit", name="references_tbluniteproduits_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblUniteProduits $tblUniteProduit)
    {
        $deleteForm = $this->createDeleteForm($tblUniteProduit);
        $editForm = $this->createForm('App\Form\TblUniteProduitsType', $tblUniteProduit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblUniteProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbluniteproduits_index', ['id' => $tblUniteProduit->getId()]);
        }

        return $this->render('tbluniteproduits/edit.html.twig', [
            'tblUniteProduit' => $tblUniteProduit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblUniteProduits entities.
     *
     * @Route("/", name="references_tbluniteproduits_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblUniteProduits = $em->getRepository(TblUniteProduits::class)->findAll();

        return $this->render('tbluniteproduits/index.html.twig', [
            'tblUniteProduits' => $tblUniteProduits,
        ]);
    }

    /**
     * Creates a new TblUniteProduits entity.
     *
     * @Route("/new", name="references_tbluniteproduits_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblUniteProduit = new TblUniteProduits($user);
        $form = $this->createForm('App\Form\TblUniteProduitsType', $tblUniteProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblUniteProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbluniteproduits_index', ['id' => $tblUniteProduit->getId()]);
        }

        return $this->render('tbluniteproduits/new.html.twig', [
            'tblUniteProduit' => $tblUniteProduit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblUniteProduits entity.
     *
     * @Route("/{id}", name="references_tbluniteproduits_show", methods={"GET"})
     */
    public function showAction(TblUniteProduits $tblUniteProduit)
    {
        $deleteForm = $this->createDeleteForm($tblUniteProduit);

        return $this->render('tbluniteproduits/show.html.twig', [
            'tblUniteProduit' => $tblUniteProduit,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblUniteProduits entity.
     *
     * @param TblUniteProduits $tblUniteProduit The TblUniteProduits entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblUniteProduits $tblUniteProduit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbluniteproduits_delete', ['id' => $tblUniteProduit->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
