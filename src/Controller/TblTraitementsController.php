<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblTraitements;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblTraitements controller.
 *
 * @Route("/{_locale}/references/tbltraitements")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblTraitementsController extends AbstractController
{
    /**
     * Deletes a TblTraitements entity.
     *
     * @Route("/{id}", name="references_tbltraitements_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblTraitements $tblTraitement)
    {
        $form = $this->createDeleteForm($tblTraitement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblTraitement);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbltraitements_index');
    }

    /**
     * Displays a form to edit an existing TblTraitements entity.
     *
     * @Route("/{id}/edit", name="references_tbltraitements_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblTraitements $tblTraitement)
    {
        $deleteForm = $this->createDeleteForm($tblTraitement);
        $editForm = $this->createForm('App\Form\TblTraitementsType', $tblTraitement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTraitement);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltraitements_index', ['id' => $tblTraitement->getId()]);
        }

        return $this->render('tbltraitements/edit.html.twig', [
            'tblTraitement' => $tblTraitement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblTraitements entities.
     *
     * @Route("/", name="references_tbltraitements_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblTraitements = $em->getRepository(TblTraitements::class)->findAll();

        return $this->render('tbltraitements/index.html.twig', [
            'tblTraitements' => $tblTraitements,
        ]);
    }

    /**
     * Creates a new TblTraitements entity.
     *
     * @Route("/new", name="references_tbltraitements_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblTraitement = new TblTraitements($user);
        $form = $this->createForm('App\Form\TblTraitementsType', $tblTraitement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTraitement);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltraitements_index', ['id' => $tblTraitement->getId()]);
        }

        return $this->render('tbltraitements/new.html.twig', [
            'tblTraitement' => $tblTraitement,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblTraitements entity.
     *
     * @Route("/{id}", name="references_tbltraitements_show", methods={"GET"})
     */
    public function showAction(TblTraitements $tblTraitement)
    {
        $deleteForm = $this->createDeleteForm($tblTraitement);

        return $this->render('tbltraitements/show.html.twig', [
            'tblTraitement' => $tblTraitement,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblTraitements entity.
     *
     * @param TblTraitements $tblTraitement The TblTraitements entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblTraitements $tblTraitement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbltraitements_delete', ['id' => $tblTraitement->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
