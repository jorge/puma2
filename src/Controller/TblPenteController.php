<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblPente;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblPente controller.
 *
 * @Route("/{_locale}/references/tblpente")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblPenteController extends AbstractController
{
    /**
     * Deletes a TblPente entity.
     *
     * @Route("/{id}", name="references_tblpente_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblPente $tblPente)
    {
        $form = $this->createDeleteForm($tblPente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblPente);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblpente_index');
    }

    /**
     * Displays a form to edit an existing TblPente entity.
     *
     * @Route("/{id}/edit", name="references_tblpente_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblPente $tblPente)
    {
        $deleteForm = $this->createDeleteForm($tblPente);
        $editForm = $this->createForm('App\Form\TblPenteType', $tblPente);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblPente);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblpente_index', ['id' => $tblPente->getId()]);
        }

        return $this->render('tblpente/edit.html.twig', [
            'tblPente' => $tblPente,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblPente entities.
     *
     * @Route("/", name="references_tblpente_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblPentes = $em->getRepository(TblPente::class)->findAll();

        return $this->render('tblpente/index.html.twig', [
            'tblPentes' => $tblPentes,
        ]);
    }

    /**
     * Creates a new TblPente entity.
     *
     * @Route("/new", name="references_tblpente_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblPente = new TblPente($user);
        $form = $this->createForm('App\Form\TblPenteType', $tblPente);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblPente);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblpente_index', ['id' => $tblPente->getId()]);
        }

        return $this->render('tblpente/new.html.twig', [
            'tblPente' => $tblPente,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblPente entity.
     *
     * @Route("/{id}", name="references_tblpente_show", methods={"GET"})
     */
    public function showAction(TblPente $tblPente)
    {
        $deleteForm = $this->createDeleteForm($tblPente);

        return $this->render('tblpente/show.html.twig', [
            'tblPente' => $tblPente,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblPente entity.
     *
     * @param TblPente $tblPente The TblPente entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblPente $tblPente)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblpente_delete', ['id' => $tblPente->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
