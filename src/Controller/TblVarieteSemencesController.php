<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblVarieteSemences;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblVarieteSemences controller.
 *
 * @Route("/{_locale}/references/tblvarietesemences")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblVarieteSemencesController extends AbstractController
{
    /**
     * Deletes a TblVarieteSemences entity.
     *
     * @Route("/{id}", name="references_tblvarietesemences_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblVarieteSemences $tblVarieteSemence)
    {
        $form = $this->createDeleteForm($tblVarieteSemence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblVarieteSemence);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblvarietesemences_index');
    }

    /**
     * Displays a form to edit an existing TblVarieteSemences entity.
     *
     * @Route("/{id}/edit", name="references_tblvarietesemences_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblVarieteSemences $tblVarieteSemence)
    {
        $deleteForm = $this->createDeleteForm($tblVarieteSemence);
        $editForm = $this->createForm('App\Form\TblVarieteSemencesType', $tblVarieteSemence);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblVarieteSemence);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            $this->addFlash(
                'success',
                'Elément mis à jour'
            );

            return $this->redirectToRoute('references_tblvarietesemences_index');
        }

        return $this->render('tblvarietesemences/edit.html.twig', [
            'tblVarieteSemence' => $tblVarieteSemence,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblVarieteSemences entities.
     *
     * @Route("/", name="references_tblvarietesemences_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblVarieteSemences = $em->getRepository(TblVarieteSemences::class)->findAll();

        return $this->render('tblvarietesemences/index.html.twig', [
            'tblVarieteSemences' => $tblVarieteSemences,
        ]);
    }

    /**
     * Creates a new TblVarieteSemences entity.
     *
     * @Route("/new", name="references_tblvarietesemences_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblVarieteSemence = new TblVarieteSemences($user);
        $form = $this->createForm('App\Form\TblVarieteSemencesType', $tblVarieteSemence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblVarieteSemence);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblvarietesemences_index', ['id' => $tblVarieteSemence->getId()]);
        }

        return $this->render('tblvarietesemences/new.html.twig', [
            'tblVarieteSemence' => $tblVarieteSemence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblVarieteSemences entity.
     *
     * @param TblVarieteSemences $tblVarieteSemence The TblVarieteSemences entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblVarieteSemences $tblVarieteSemence)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblvarietesemences_delete', ['id' => $tblVarieteSemence->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
