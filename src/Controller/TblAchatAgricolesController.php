<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblAchatAgricoles;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblAchatAgricoles controller.
 *
 * @Route("/{_locale}/references/tblachatagricoles")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblAchatAgricolesController extends AbstractController
{
    /**
     * Deletes a TblAchatAgricoles entity.
     *
     * @Route("/{id}", name="references_tblachatagricoles_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblAchatAgricoles $tblAchatAgricole)
    {
        $form = $this->createDeleteForm($tblAchatAgricole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblAchatAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblachatagricoles_index');
    }

    /**
     * Displays a form to edit an existing TblAchatAgricoles entity.
     *
     * @Route("/{id}/edit", name="references_tblachatagricoles_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblAchatAgricoles $tblAchatAgricole)
    {
        $deleteForm = $this->createDeleteForm($tblAchatAgricole);
        $editForm = $this->createForm('App\Form\TblAchatAgricolesType', $tblAchatAgricole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAchatAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblachatagricoles_index', ['id' => $tblAchatAgricole->getId()]);
        }

        return $this->render('tblachatagricoles/edit.html.twig', [
            'tblAchatAgricole' => $tblAchatAgricole,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblAchatAgricoles entities.
     *
     * @Route("/", name="references_tblachatagricoles_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblAchatAgricoles = $em->getRepository(TblAchatAgricoles::class)->findAll();

        return $this->render('tblachatagricoles/index.html.twig', [
            'tblAchatAgricoles' => $tblAchatAgricoles,
        ]);
    }

    /**
     * Creates a new TblAchatAgricoles entity.
     *
     * @Route("/new", name="references_tblachatagricoles_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblAchatAgricole = new TblAchatAgricoles($user);
        $form = $this->createForm('App\Form\TblAchatAgricolesType', $tblAchatAgricole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAchatAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblachatagricoles_index', ['id' => $tblAchatAgricole->getId()]);
        }

        return $this->render('tblachatagricoles/new.html.twig', [
            'tblAchatAgricole' => $tblAchatAgricole,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblAchatAgricoles entity.
     *
     * @Route("/{id}", name="references_tblachatagricoles_show", methods={"GET"})
     */
    public function showAction(TblAchatAgricoles $tblAchatAgricole)
    {
        $deleteForm = $this->createDeleteForm($tblAchatAgricole);

        return $this->render('tblachatagricoles/show.html.twig', [
            'tblAchatAgricole' => $tblAchatAgricole,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblAchatAgricoles entity.
     *
     * @param TblAchatAgricoles $tblAchatAgricole The TblAchatAgricoles entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblAchatAgricoles $tblAchatAgricole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblachatagricoles_delete', ['id' => $tblAchatAgricole->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
