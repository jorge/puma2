<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblUtilites;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblUtilites controller.
 *
 * @Route("/{_locale}/references/tblutilites")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblUtilitesController extends AbstractController
{
    /**
     * Deletes a TblUtilites entity.
     *
     * @Route("/{id}", name="references_tblutilites_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblUtilites $tblUtilite)
    {
        $form = $this->createDeleteForm($tblUtilite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblUtilite);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblutilites_index');
    }

    /**
     * Displays a form to edit an existing TblUtilites entity.
     *
     * @Route("/{id}/edit", name="references_tblutilites_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblUtilites $tblUtilite)
    {
        $deleteForm = $this->createDeleteForm($tblUtilite);
        $editForm = $this->createForm('App\Form\TblUtilitesType', $tblUtilite);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblUtilite);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblutilites_index', ['id' => $tblUtilite->getId()]);
        }

        return $this->render('tblutilites/edit.html.twig', [
            'tblUtilite' => $tblUtilite,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblUtilites entities.
     *
     * @Route("/", name="references_tblutilites_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblUtilites = $em->getRepository(TblUtilites::class)->findAll();

        return $this->render('tblutilites/index.html.twig', [
            'tblUtilites' => $tblUtilites,
        ]);
    }

    /**
     * Creates a new TblUtilites entity.
     *
     * @Route("/new", name="references_tblutilites_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblUtilite = new TblUtilites($user);
        $form = $this->createForm('App\Form\TblUtilitesType', $tblUtilite);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblUtilite);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblutilites_index', ['id' => $tblUtilite->getId()]);
        }

        return $this->render('tblutilites/new.html.twig', [
            'tblUtilite' => $tblUtilite,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblUtilites entity.
     *
     * @Route("/{id}", name="references_tblutilites_show", methods={"GET"})
     */
    public function showAction(TblUtilites $tblUtilite)
    {
        $deleteForm = $this->createDeleteForm($tblUtilite);

        return $this->render('tblutilites/show.html.twig', [
            'tblUtilite' => $tblUtilite,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblUtilites entity.
     *
     * @param TblUtilites $tblUtilite The TblUtilites entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblUtilites $tblUtilite)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblutilites_delete', ['id' => $tblUtilite->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
