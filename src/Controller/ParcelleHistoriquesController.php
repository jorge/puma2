<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\ParcelleHistoriques;
use App\Entity\Parcelles;
use App\Entity\Exploitations;
use App\Entity\PivotExploitationOrganisations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * ParcelleHistoriques controller.
 *
 * @Route("/{_locale}/references_parcellehistoriques")
 */

use Symfony\Component\Routing\Annotation\Route;

final class ParcelleHistoriquesController extends AbstractController
{
    /**
     * Deletes a ParcelleHistoriques entity.
     *
     * @Route("/{id}", name="references_parcellehistoriques_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, ParcelleHistoriques $parcelleHistorique)
    {
        $em = $this->getDoctrine()->getManager();

        $parcelle = $parcelleHistorique->getParcelle();
        $annee = $parcelleHistorique->getAnnee();
        $exploitation = $parcelle->getExploitation();
        $form = $this->createDeleteForm($parcelleHistorique, [
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $parcelleHistorique->setRadie(TRUE);
            $em->persist($parcelleHistorique);
            $em->flush();
            $this->radierBrancheHistoriques($parcelleHistorique->getId(), 1, $em);
        }

        return $this->redirectToRoute('references_parcelles_index', [
            'exploitation' => $parcelle->getExploitation()->getId(),
            'annee' => $annee,
            'parcelle' => $parcelle->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing ParcelleHistoriques entity.
     *
     * @Route("/{id}/edit", name="references_parcellehistoriques_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, ParcelleHistoriques $parcelleHistorique)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $parcelleHistorique->getParcelle()->getExploitation();
        $theme = $request->query->get('theme');

        $deleteForm = $this->createDeleteForm($parcelleHistorique);
        $editForm = $this->createForm('App\Form\ParcelleHistoriquesType', $parcelleHistorique);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($parcelleHistorique);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $parcelleHistorique->getId(),
            ]);
        }

        return $this->render('parcellehistoriques/edit.html.twig', [
            'parcelleHistorique' => $parcelleHistorique,
            'edit_form' => $editForm->createView(),
            'exploitation' => $exploitation,
            'annee' => $parcelleHistorique->getAnnee(),
            'delete_form' => $deleteForm->createView(),
            'theme' => $theme,
        ]);
    }

    /**
     * Displays a form to edit an existing ParcelleHistoriques entity.
     *
     * @Route("/{id}/editproblemes", name="references_parcellehistoriques_editproblemes", methods={"GET", "POST"})
     */
    public function editproblemesAction(Request $request, ParcelleHistoriques $parcelleHistorique)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $editForm = $this->createForm('App\Form\ParcelleHistoriquesProblemesType', $parcelleHistorique);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($parcelleHistorique);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $parcelleHistorique->getId(),
            ]);
        }

        return $this->render('parcellehistoriques/editproblemes.html.twig', [
            'parcelleHistorique' => $parcelleHistorique,
            'edit_form' => $editForm->createView(),
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
        ]);
    }

    /**
     * Creates a new ParcelleHistoriques entity.
     *
     * @Route("/new/{annee}/{parcelle}", name="references_parcellehistoriques_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, $annee, Parcelles $parcelle)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $parcelle->getExploitation();

        $user = $this->getUser();
        $parcelleHistorique = new ParcelleHistoriques($user);
        $parcelleHistorique->setParcelle($parcelle);
        $parcelleHistorique->setAnnee($annee);

        $form = $this->createForm('App\Form\ParcelleHistoriquesType', $parcelleHistorique);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $parcelleHistorique->setRadie(FALSE);
            $em->persist($parcelleHistorique);
            $em->flush();

            return $this->redirectToRoute('references_parcellehistoriques_show', [
                'id' => $parcelleHistorique->getId(),
            ]);
        }

        return $this->render('parcellehistoriques/new.html.twig', [
            'parcelleHistorique' => $parcelleHistorique,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'form' => $form->createView(),
        ]);
    }

    /* maj radie pour les tables dependent de parcelle_historiques */
    public function radierBrancheHistoriques($parcel_historique_id, $status, $em)
    {
        //          $fumuremin = $em->getRepository(ParcelleFumureMin::class)
//              ->updateRadieStatus($parcel_historique_id,$status);
//          $fumureorg = $em->getRepository(ParcelleFumureOrg::class)
//              ->updateRadieStatus($parcel_historique_id,$status);
//          $herbicide = $em->getRepository(ParcelleHerbicide::class)
//              ->updateRadieStatus($parcel_historique_id,$status);
//          $phytosanitaire = $em->getRepository(ParcellePhytosanitaire::class)
//              ->updateRadieStatus($parcel_historique_id,$status);
//          $recoltes = $em->getRepository(ParcelleRecoltes::class)
//              ->updateRadieStatus($parcel_historique_id,$status);
//          $semences = $em->getRepository(ParcelleSemences::class)
//              ->updateRadieStatus($parcel_historique_id,$status);
//          $techniques = $em->getRepository(ParcelleTechniques::class)
//              ->updateRadieStatus($parcel_historique_id,$status);
    }

    /**
     * Finds and displays a ParcelleHistoriques entity.
     *
     * @Route("/{id}", name="references_parcellehistoriques_show", methods={"GET"})
     */
    public function showAction(ParcelleHistoriques $parcelleHistorique)
    {
        $em = $this->getDoctrine()->getManager();
        $parcelle = $parcelleHistorique->getParcelle();
        $exploitation = $parcelleHistorique->getParcelle()->getExploitation();
        $annee = $parcelleHistorique->getAnnee();

        $deleteForm = $this->createDeleteForm($parcelleHistorique);

        return $this->render('parcellehistoriques/show.html.twig', [
            'parcelleHistorique' => $parcelleHistorique,
            'parcelle' => $parcelle,
            'numeroParcelle' => $parcelle->getNumeroParcelle(),
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a ParcelleHistoriques entity.
     *
     * @param ParcelleHistoriques $parcelleHistorique The ParcelleHistoriques entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ParcelleHistoriques $parcelleHistorique, $options = [])
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation_id = $parcelleHistorique->getParcelle()->getExploitation()->getId();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_parcellehistoriques_delete', [
                'id' => $parcelleHistorique->getId(),
                'exploitation' => $exploitation,
                'coop_id' => $coop->getId(),
                'group_id' => $groupe->getId(),
                'exploitation_id' => $exploitation_id,
                'annee' => $parcelleHistorique->getAnnee(),
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
