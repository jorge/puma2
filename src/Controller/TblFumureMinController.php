<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblFumureMin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblFumureMin controller.
 *
 * @Route("/{_locale}/references/tblfumuremin")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblFumureMinController extends AbstractController
{
    /**
     * Deletes a TblFumureMin entity.
     *
     * @Route("/{id}", name="references_tblfumuremin_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblFumureMin $tblFumureMin)
    {
        $form = $this->createDeleteForm($tblFumureMin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblFumureMin);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblfumuremin_index');
    }

    /**
     * Displays a form to edit an existing TblFumureMin entity.
     *
     * @Route("/{id}/edit", name="references_tblfumuremin_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblFumureMin $tblFumureMin)
    {
        $deleteForm = $this->createDeleteForm($tblFumureMin);
        $editForm = $this->createForm('App\Form\TblFumureMinType', $tblFumureMin);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblFumureMin);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblfumuremin_index', ['id' => $tblFumureMin->getId()]);
        }

        return $this->render('tblfumuremin/edit.html.twig', [
            'tblFumureMin' => $tblFumureMin,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblFumureMin entities.
     *
     * @Route("/", name="references_tblfumuremin_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblFumureMins = $em->getRepository(TblFumureMin::class)->findAll();

        return $this->render('tblfumuremin/index.html.twig', [
            'tblFumureMins' => $tblFumureMins,
        ]);
    }

    /**
     * Creates a new TblFumureMin entity.
     *
     * @Route("/new", name="references_tblfumuremin_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblFumureMin = new TblFumureMin($user);
        $form = $this->createForm('App\Form\TblFumureMinType', $tblFumureMin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblFumureMin);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblfumuremin_index', ['id' => $tblFumureMin->getId()]);
        }

        return $this->render('tblfumuremin/new.html.twig', [
            'tblFumureMin' => $tblFumureMin,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblFumureMin entity.
     *
     * @Route("/{id}", name="references_tblfumuremin_show", methods={"GET"})
     */
    public function showAction(TblFumureMin $tblFumureMin)
    {
        $deleteForm = $this->createDeleteForm($tblFumureMin);

        return $this->render('tblfumuremin/show.html.twig', [
            'tblFumureMin' => $tblFumureMin,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblFumureMin entity.
     *
     * @param TblFumureMin $tblFumureMin The TblFumureMin entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblFumureMin $tblFumureMin)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblfumuremin_delete', ['id' => $tblFumureMin->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
