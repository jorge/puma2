<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Filieres;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Filieres controller.
 *
 * @Route("/{_locale}/references/filieres")
 */

use Symfony\Component\Routing\Annotation\Route;

final class FilieresController extends AbstractController
{
    /**
     * Deletes a Filieres entity.
     *
     * @Route("/{id}", name="references_filieres_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Filieres $filiere)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $form = $this->createDeleteForm($filiere, [
            //              'coop_id' => $coop_id,
            //              'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $filiere->setRadie(TRUE);
            $em->persist($filiere);
            $em->flush();
        }

        return $this->redirectToRoute('references_filieres_index', [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing Filieres entity.
     *
     * @Route("/{id}/edit", name="references_filieres_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Filieres $filiere)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $deleteForm = $this->createDeleteForm($filiere);
        $editForm = $this->createForm('App\Form\FilieresType', $filiere);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($filiere);
            $em->flush();

            return $this->redirectToRoute('references_filieres_edit', [
                'id' => $filiere->getId(),
            ]);
        }

        return $this->render('filieres/edit.html.twig', [
            'filiere' => $filiere,
            //                          'coop_id' => $coop_id,
            //              'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all Filieres entities.
     *
     * @Route("/", name="references_filieres_index", methods={"GET"})
     */
    public function indexAction()
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $em = $this->getDoctrine()->getManager();

        // $filieres = $em->getRepository(Filieres::class)->findAll();
        $filieres = $em->getRepository(Filieres::class)->findBy([], null, 1000, 0);

        return $this->render('filieres/index.html.twig', [
            //                          'coop_id' => $coop_id,
            //              'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'filieres' => $filieres,
        ]);
    }

    /**
     * Creates a new Filieres entity.
     *
     * @Route("/new", name="references_filieres_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $user = $this->getUser();
        $filiere = new Filieres($user);
        $form = $this->createForm('App\Form\FilieresType', $filiere);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $filiere->setRadie(FALSE);
            $em->persist($filiere);
            $em->flush();

            return $this->redirectToRoute('references_filieres_show', [
                'id' => $filiere->getId(),
            ]);
        }

        return $this->render('filieres/new.html.twig', [
            'filiere' => $filiere,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Filieres entity.
     *
     * @Route("/{id}", name="references_filieres_show", methods={"GET"})
     */
    public function showAction(Filieres $filiere)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $deleteForm = $this->createDeleteForm($filiere);

        return $this->render('filieres/show.html.twig', [
            'filiere' => $filiere,
            //                          'coop_id' => $coop_id,
            //              'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a Filieres entity.
     *
     * @param Filieres $filiere
     *          The Filieres entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Filieres $filiere, $options = [])
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        return $this->createFormBuilder()->setAction($this->generateUrl(
            'references_filieres_delete',
            [
                'id' => $filiere->getId(),
                //                  'coop_id' => $coop_id,
                //                  'group_id' => $group_id,
                'exploitation_id' => $exploitation_id,
                'annee' => $annee,
            ]
        ))->setMethod('DELETE')->getForm();
    }
}
