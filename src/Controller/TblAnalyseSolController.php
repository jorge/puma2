<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblAnalyseSol;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblAnalyseSol controller.
 *
 * @Route("/{_locale}/references/tblanalysesol")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblAnalyseSolController extends AbstractController
{
    /**
     * Deletes a TblAnalyseSol entity.
     *
     * @Route("/{id}", name="references_tblanalysesol_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblAnalyseSol $tblAnalyseSol)
    {
        $form = $this->createDeleteForm($tblAnalyseSol);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblAnalyseSol);
            $em->flush();
        }

        return $this->redirectToRoute('references_tblanalysesol_index');
    }

    /**
     * Displays a form to edit an existing TblAnalyseSol entity.
     *
     * @Route("/{id}/edit", name="references_tblanalysesol_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblAnalyseSol $tblAnalyseSol)
    {
        $deleteForm = $this->createDeleteForm($tblAnalyseSol);
        $editForm = $this->createForm('App\Form\TblAnalyseSolType', $tblAnalyseSol);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAnalyseSol);
            $em->flush();

            return $this->redirectToRoute('references_tblanalysesol_edit', ['id' => $tblAnalyseSol->getId()]);
        }

        return $this->render('tblanalysesol/edit.html.twig', [
            'tblAnalyseSol' => $tblAnalyseSol,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblAnalyseSol entities.
     *
     * @Route("/", name="references_tblanalysesol_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblAnalyseSols = $em->getRepository(TblAnalyseSol::class)->findAll();

        return $this->render('tblanalysesol/index.html.twig', [
            'tblAnalyseSols' => $tblAnalyseSols,
        ]);
    }

    /**
     * Creates a new TblAnalyseSol entity.
     *
     * @Route("/new", name="references_tblanalysesol_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblAnalyseSol = new TblAnalyseSol($user);
        $form = $this->createForm('App\Form\TblAnalyseSolType', $tblAnalyseSol);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAnalyseSol);
            $em->flush();

            return $this->redirectToRoute('references_tblanalysesol_show', ['id' => $tblAnalyseSol->getId()]);
        }

        return $this->render('tblanalysesol/new.html.twig', [
            'tblAnalyseSol' => $tblAnalyseSol,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblAnalyseSol entity.
     *
     * @Route("/{id}", name="references_tblanalysesol_show", methods={"GET"})
     */
    public function showAction(TblAnalyseSol $tblAnalyseSol)
    {
        $deleteForm = $this->createDeleteForm($tblAnalyseSol);

        return $this->render('tblanalysesol/show.html.twig', [
            'tblAnalyseSol' => $tblAnalyseSol,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblAnalyseSol entity.
     *
     * @param TblAnalyseSol $tblAnalyseSol The TblAnalyseSol entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblAnalyseSol $tblAnalyseSol)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblanalysesol_delete', ['id' => $tblAnalyseSol->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
