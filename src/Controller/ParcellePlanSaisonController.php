<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\ParcellePlanSaison;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * ParcellePlanSaison controller.
 *
 * @Route("/{_locale}/references/parcelleplansaison")
 */

use function count;

final class ParcellePlanSaisonController extends AbstractController
{
    /**
     * Deletes a ParcellePlanSaison entity.
     *
     * @Route("/{id}", name="references_parcelleplansaison_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, ParcellePlanSaison $parcellePlanSaison)
    {
        $exploitationPlanSaison = $parcellePlanSaison->getExploitationPlanSaison();
        $form = $this->createDeleteForm($parcellePlanSaison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($parcellePlanSaison);
            $em->flush();
        }

        return $this->redirectToRoute('references_exploitationplansaison_show', [
            'id' => $exploitationPlanSaison . getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing ParcellePlanSaison entity.
     *
     * @Route("/{id}/edit", name="references_parcelleplansaison_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, ParcellePlanSaison $parcellePlanSaison)
    {
        $deleteForm = $this->createDeleteForm($parcellePlanSaison);
        $editForm = $this->createForm('App\Form\ParcellePlanSaisonType', $parcellePlanSaison);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parcellePlanSaison);

            if (count($parcellePlanSaison->getCultures()) === 0) {
                $editForm->get('cultures')->addError(new FormError('Avant de sauver, vous devez choisir, au moins, une culture'));
            } elseif ($parcellePlanSaison->getModeCulture() === 'pure' && count($parcellePlanSaison->getCultures()) !== 1) {
                $editForm->get('cultures')->addError(new FormError('pour le modeCulture pure, on doit choisir une seule culture'));
            } elseif ($parcellePlanSaison->getModeCulture() === 'associée' && count($parcellePlanSaison->getCultures()) < 2) {
                $editForm->get('cultures')->addError(new FormError('pour le modeCulture associée, on doit choisir plus d\'une culture'));
            } else {
                $em->flush();

                return $this->redirectToRoute('references_exploitationplansaison_show', [
                    'id' => $parcellePlanSaison->getExploitationPlanSaison()->getId(),
                ]);
            }
        }

        return $this->render('parcelleplansaison/edit.html.twig', [
            'parcellePlanSaison' => $parcellePlanSaison,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new ParcellePlanSaison entity.
     *
     * @Route("/new/{exploitationPlanSaison}", name="references_parcelleplansaison_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, \App\Entity\ExploitationPlanSaison $exploitationPlanSaison)
    {
        $user = $this->getUser();
        $parcellePlanSaison = new ParcellePlanSaison($user);
        $parcellePlanSaison->setExploitationPlanSaison($exploitationPlanSaison);
        $form = $this->createForm('App\Form\ParcellePlanSaisonType', $parcellePlanSaison);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parcellePlanSaison);

            if (count($parcellePlanSaison->getCultures()) === 0) {
                $form->get('cultures')->addError(new FormError('Avant de sauver, vous devez choisir, au moins, une culture'));
            } elseif ($parcellePlanSaison->getModeCulture() === 'pure' && count($parcellePlanSaison->getCultures()) !== 1) {
                $form->get('cultures')->addError(new FormError('pour le modeCulture pure, on doit choisir une seule culture'));
            } elseif ($parcellePlanSaison->getModeCulture() === 'associée' && count($parcellePlanSaison->getCultures()) < 2) {
                $form->get('cultures')->addError(new FormError('pour le modeCulture associée, on doit choisir plus d\'une culture'));
            } else {
                $em->flush();

                return $this->redirectToRoute('references_exploitationplansaison_show', ['id' => $exploitationPlanSaison->getId()]);
            }
        }

        return $this->render('parcelleplansaison/new.html.twig', [
            'parcellePlanSaison' => $parcellePlanSaison,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a ParcellePlanSaison entity.
     *
     * @param ParcellePlanSaison $parcellePlanSaison The ParcellePlanSaison entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ParcellePlanSaison $parcellePlanSaison)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_parcelleplansaison_delete', ['id' => $parcellePlanSaison->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
