<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblOrganisationLevel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblOrganisationLevel controller.
 *
 * @Route("/{_locale}/references/tblorganisationlevel")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblOrganisationLevelController extends AbstractController
{
    /**
     * Deletes a TblOrganisationLevel entity.
     *
     * @Route("/{id}", name="references_tblorganisationlevel_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblOrganisationLevel $tblOrganisationLevel)
    {
        $form = $this->createDeleteForm($tblOrganisationLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblOrganisationLevel);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblorganisationlevel_index');
    }

    /**
     * Displays a form to edit an existing TblOrganisationLevel entity.
     *
     * @Route("/{id}/edit", name="references_tblorganisationlevel_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblOrganisationLevel $tblOrganisationLevel)
    {
        $deleteForm = $this->createDeleteForm($tblOrganisationLevel);
        $editForm = $this->createForm('App\Form\TblOrganisationLevelType', $tblOrganisationLevel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOrganisationLevel);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblorganisationlevel_index', ['id' => $tblOrganisationLevel->getId()]);
        }

        return $this->render('tblorganisationlevel/edit.html.twig', [
            'tblOrganisationLevel' => $tblOrganisationLevel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblOrganisationLevel entities.
     *
     * @Route("/", name="references_tblorganisationlevel_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblOrganisationLevels = $em->getRepository(TblOrganisationLevel::class)->findAll();

        return $this->render('tblorganisationlevel/index.html.twig', [
            'tblOrganisationLevels' => $tblOrganisationLevels,
        ]);
    }

    /**
     * Creates a new TblOrganisationLevel entity.
     *
     * @Route("/new", name="references_tblorganisationlevel_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblOrganisationLevel = new TblOrganisationLevel($user);
        $form = $this->createForm('App\Form\TblOrganisationLevelType', $tblOrganisationLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOrganisationLevel);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblorganisationlevel_index', ['id' => $tblOrganisationLevel->getId()]);
        }

        return $this->render('tblorganisationlevel/new.html.twig', [
            'tblOrganisationLevel' => $tblOrganisationLevel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblOrganisationLevel entity.
     *
     * @Route("/{id}", name="references_tblorganisationlevel_show", methods={"GET"})
     */
    public function showAction(TblOrganisationLevel $tblOrganisationLevel)
    {
        $deleteForm = $this->createDeleteForm($tblOrganisationLevel);

        return $this->render('tblorganisationlevel/show.html.twig', [
            'tblOrganisationLevel' => $tblOrganisationLevel,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblOrganisationLevel entity.
     *
     * @param TblOrganisationLevel $tblOrganisationLevel The TblOrganisationLevel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblOrganisationLevel $tblOrganisationLevel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblorganisationlevel_delete', ['id' => $tblOrganisationLevel->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
