<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\DemandeCredit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * DemandeCredit controller.
 *
 * @Route("/{_locale}/references/demandecredit")
 */

use Symfony\Component\Routing\Annotation\Route;

final class DemandeCreditController extends AbstractController
{
    /**
     * Deletes a DemandeCredit entity.
     *
     * @Route("/demandecreditdel/{id}", name="references_demandecredit_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, DemandeCredit $demandeCredit)
    {
        $exploitationPlanSaison = $demandeCredit->getExploitationPlanSaison();
        $form = $this->createDeleteForm($demandeCredit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($demandeCredit);
            $em->flush();
        }

        return $this->redirectToRoute('references_exploitationplansaison_show', [
            'id' => $exploitationPlanSaison->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing DemandeCredit entity.
     *
     * @Route("/demandecredit/{id}/edit", name="references_demandecredit_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, DemandeCredit $demandeCredit)
    {
        $deleteForm = $this->createDeleteForm($demandeCredit);
        $editForm = $this->createForm('App\Form\DemandeCreditType', $demandeCredit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($demandeCredit);
            $em->flush();

            return $this->redirectToRoute('references_exploitationplansaison_show', [
                'id' => $demandeCredit->getExploitationPlanSaison()->getId(), ]);
        }

        return $this->render('demandecredit/edit.html.twig', [
            'demandeCredit' => $demandeCredit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new DemandeCredit entity.
     *
     * @Route("/demandecredit_new/{exploitationPlanSaison}", name="references_demandecredit_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, \App\Entity\ExploitationPlanSaison $exploitationPlanSaison)
    {
        $user = $this->getUser();
        $demandeCredit = new DemandeCredit($user);
        $demandeCredit->setExploitationPlanSaison($exploitationPlanSaison);
        $form = $this->createForm('App\Form\DemandeCreditType', $demandeCredit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($demandeCredit);
            $em->flush();

            return $this->redirectToRoute('references_exploitationplansaison_show', ['id' => $exploitationPlanSaison->getId()]);
        }

        return $this->render('demandecredit/new.html.twig', [
            'demandeCredit' => $demandeCredit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a DemandeCredit entity.
     *
     * @param DemandeCredit $demandeCredit The DemandeCredit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(DemandeCredit $demandeCredit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_demandecredit_delete', ['id' => $demandeCredit->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
