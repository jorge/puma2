<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblCreditObjet;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblCreditObjet controller.
 *
 * @Route("/{_locale}/references/creditobjet")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblCreditObjetController extends AbstractController
{
    /**
     * Deletes a TblCreditObjet entity.
     *
     * @Route("/{id}", name="references_creditobjet_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblCreditObjet $tblCreditObjet)
    {
        $form = $this->createDeleteForm($tblCreditObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblCreditObjet);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_creditobjet_index');
    }

    /**
     * Displays a form to edit an existing TblCreditObjet entity.
     *
     * @Route("/{id}/edit", name="references_creditobjet_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblCreditObjet $tblCreditObjet)
    {
        $deleteForm = $this->createDeleteForm($tblCreditObjet);
        $editForm = $this->createForm('App\Form\TblCreditObjetType', $tblCreditObjet);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblCreditObjet);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_creditobjet_index', ['id' => $tblCreditObjet->getId()]);
        }

        return $this->render('tblcreditobjet/edit.html.twig', [
            'tblCreditObjet' => $tblCreditObjet,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblCreditObjet entities.
     *
     * @Route("/", name="references_creditobjet_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblCreditObjets = $em->getRepository(TblCreditObjet::class)->findAll();

        return $this->render('tblcreditobjet/index.html.twig', [
            'tblCreditObjets' => $tblCreditObjets,
        ]);
    }

    /**
     * Creates a new TblCreditObjet entity.
     *
     * @Route("/new", name="references_creditobjet_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblCreditObjet = new TblCreditObjet($user);
        $form = $this->createForm('App\Form\TblCreditObjetType', $tblCreditObjet);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblCreditObjet);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_creditobjet_index', ['id' => $tblCreditObjet->getId()]);
        }

        return $this->render('tblcreditobjet/new.html.twig', [
            'tblCreditObjet' => $tblCreditObjet,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblCreditObjet entity.
     *
     * @Route("/{id}", name="references_creditobjet_show", methods={"GET"})
     */
    public function showAction(TblCreditObjet $tblCreditObjet)
    {
        $deleteForm = $this->createDeleteForm($tblCreditObjet);

        return $this->render('tblcreditobjet/show.html.twig', [
            'tblCreditObjet' => $tblCreditObjet,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblCreditObjet entity.
     *
     * @param TblCreditObjet $tblCreditObjet The TblCreditObjet entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblCreditObjet $tblCreditObjet)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_creditobjet_delete', ['id' => $tblCreditObjet->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
