<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblContraints;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblContraints controller.
 *
 * @Route("/{_locale}/references/tblcontraints")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblContraintsController extends AbstractController
{
    /**
     * Deletes a TblContraints entity.
     *
     * @Route("/{id}", name="references_tblcontraints_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblContraints $tblContraint)
    {
        $form = $this->createDeleteForm($tblContraint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblContraint);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblcontraints_index');
    }

    /**
     * Displays a form to edit an existing TblContraints entity.
     *
     * @Route("/{id}/edit", name="references_tblcontraints_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblContraints $tblContraint)
    {
        $deleteForm = $this->createDeleteForm($tblContraint);
        $editForm = $this->createForm('App\Form\TblContraintsType', $tblContraint);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblContraint);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcontraints_index', ['id' => $tblContraint->getId()]);
        }

        return $this->render('tblcontraints/edit.html.twig', [
            'tblContraint' => $tblContraint,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblContraints entities.
     *
     * @Route("/", name="references_tblcontraints_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblContraints = $em->getRepository(TblContraints::class)->findAll();

        return $this->render('tblcontraints/index.html.twig', [
            'tblContraints' => $tblContraints,
        ]);
    }

    /**
     * Creates a new TblContraints entity.
     *
     * @Route("/new", name="references_tblcontraints_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblContraint = new TblContraints($user);
        $form = $this->createForm('App\Form\TblContraintsType', $tblContraint);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblContraint);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcontraints_index', ['id' => $tblContraint->getId()]);
        }

        return $this->render('tblcontraints/new.html.twig', [
            'tblContraint' => $tblContraint,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblContraints entity.
     *
     * @Route("/{id}", name="references_tblcontraints_show", methods={"GET"})
     */
    public function showAction(TblContraints $tblContraint)
    {
        $deleteForm = $this->createDeleteForm($tblContraint);

        return $this->render('tblcontraints/show.html.twig', [
            'tblContraint' => $tblContraint,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblContraints entity.
     *
     * @param TblContraints $tblContraint The TblContraints entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblContraints $tblContraint)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblcontraints_delete', ['id' => $tblContraint->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
