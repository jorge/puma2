<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblUnitesTransformations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblUnitesTransformations controller.
 *
 * @Route("/{_locale}/references/tblunitestransformations")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblUnitesTransformationsController extends AbstractController
{
    /**
     * Deletes a TblUnitesTransformations entity.
     *
     * @Route("/{id}", name="references_tblunitestransformations_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblUnitesTransformations $tblUnitesTransformation)
    {
        $form = $this->createDeleteForm($tblUnitesTransformation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblUnitesTransformation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblunitestransformations_index');
    }

    /**
     * Displays a form to edit an existing TblUnitesTransformations entity.
     *
     * @Route("/{id}/edit", name="references_tblunitestransformations_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblUnitesTransformations $tblUnitesTransformation)
    {
        $deleteForm = $this->createDeleteForm($tblUnitesTransformation);
        $editForm = $this->createForm('App\Form\TblUnitesTransformationsType', $tblUnitesTransformation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblUnitesTransformation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblunitestransformations_index', ['id' => $tblUnitesTransformation->getId()]);
        }

        return $this->render('tblunitestransformations/edit.html.twig', [
            'tblUnitesTransformation' => $tblUnitesTransformation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblUnitesTransformations entities.
     *
     * @Route("/", name="references_tblunitestransformations_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblUnitesTransformations = $em->getRepository(TblUnitesTransformations::class)->findAll();

        return $this->render('tblunitestransformations/index.html.twig', [
            'tblUnitesTransformations' => $tblUnitesTransformations,
        ]);
    }

    /**
     * Creates a new TblUnitesTransformations entity.
     *
     * @Route("/new", name="references_tblunitestransformations_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblUnitesTransformation = new TblUnitesTransformations($user);
        $form = $this->createForm('App\Form\TblUnitesTransformationsType', $tblUnitesTransformation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblUnitesTransformation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblunitestransformations_index', ['id' => $tblUnitesTransformation->getId()]);
        }

        return $this->render('tblunitestransformations/new.html.twig', [
            'tblUnitesTransformation' => $tblUnitesTransformation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblUnitesTransformations entity.
     *
     * @Route("/{id}", name="references_tblunitestransformations_show", methods={"GET"})
     */
    public function showAction(TblUnitesTransformations $tblUnitesTransformation)
    {
        $deleteForm = $this->createDeleteForm($tblUnitesTransformation);

        return $this->render('tblunitestransformations/show.html.twig', [
            'tblUnitesTransformation' => $tblUnitesTransformation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblUnitesTransformations entity.
     *
     * @param TblUnitesTransformations $tblUnitesTransformation The TblUnitesTransformations entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblUnitesTransformations $tblUnitesTransformation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblunitestransformations_delete', ['id' => $tblUnitesTransformation->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
