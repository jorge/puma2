<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\PersonneDetails;
use App\Entity\Personnes;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * PersonneDetails controller.
 *
 * @Route("/{_locale}/references/personnedetails")
 */

use Symfony\Component\Routing\Annotation\Route;

final class PersonneDetailsController extends AbstractController
{
    /**
     * Deletes a PersonneDetails entity.
     *
     * @Route("/{id}/delete", name="references_personnedetails_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, PersonneDetails $personneDetail)
    {
        $form = $this->createDeleteForm($personneDetail);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $personneDetail->setRadie(TRUE);
            $em->persist($personneDetail);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'id' => $personneDetail->getPersonnes()->getExploitation()->getId(),
            'exploitation' => $personneDetail->getPersonnes()->getExploitation(),
            'annee' => $personneDetail->getAnnee(),
        ]);
    }

    /**
     * Displays a form to edit an existing PersonneDetails entity.
     *
     * @Route("/{id}/edit", name="references_personnedetails_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, PersonneDetails $personneDetail)
    {
        $annee = intval($request->query->get('annee'));
        $personneDetail->setAnnee($annee);
        $deleteForm = $this->createDeleteForm($personneDetail);
        $editForm = $this->createForm('App\Form\PersonneDetailsType', $personneDetail);
        $editForm->handleRequest($request);
        $exploitation = $personneDetail->getPersonnes()->getExploitation();
        $roleDansFamille = $personneDetail->getPersonnes()->getRolFamille();

        if (Personnes::role_chefDeFamille === $roleDansFamille) {
            $titre = "051 Etudes du {$roleDansFamille}";
        } else {
            $titre = "052 Etudes du {$roleDansFamille}";
        }

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($personneDetail);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $personneDetail->getPersonnes()->getExploitation()->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('personnedetails/edit.html.twig', [
            'personneDetail' => $personneDetail,
            'edit_form' => $editForm->createView(),
            'exploitation_id' => $personneDetail->getPersonnes()->getExploitation()->getId(),
            'exploitation' => $exploitation,
            'annee' => $personneDetail->getAnnee(),
            'delete_form' => $deleteForm->createView(),
            'titre' => $titre,
        ]);
    }

    /**
     * Creates a new PersonneDetails entity.
     *
     * @Route("/{personne}/{annee}/new", name="references_personnedetails_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Personnes $personne, $annee)
    {
        $user = $this->getUser();
        $personneDetail = new PersonneDetails($user);
        $personneDetail->setRadie(false);
        $personneDetail->setPersonnes($personne);
        $personneDetail->setAnnee($annee);
        $exploitation = $personne->getExploitation();
        $form = $this->createForm('App\Form\PersonneDetailsType', $personneDetail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $personne->addPersonneDetails($personneDetail);
            $em->persist($personneDetail);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $personneDetail->getPersonnes()->getExploitation()->getId(),
                'annee' => $annee,
                'exploitation_id' => $personneDetail->getPersonnes()->getExploitation()->getId(),
                'exploitation' => $exploitation,
            ]);
        }

        return $this->render('personnedetails/new.html.twig', [
            'personneDetail' => $personneDetail,
            'personne' => $personne,
            'annee' => $annee,
            'form' => $form->createView(),
            'exploitation_id' => $personneDetail->getPersonnes()->getExploitation()->getId(),
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Creates a form to delete a PersonneDetails entity.
     *
     * @param PersonneDetails $personneDetail The PersonneDetails entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(PersonneDetails $personneDetail)
    {
        return $this->createFormBuilder()
            ->setAction(
                $this->generateUrl(
                    'references_personnedetails_delete',
                    [
                        'id' => $personneDetail->getId(),
                        'exploitation' => $personneDetail->getPersonnes()->getExploitation(),
                    ]
                )
            )
            ->setMethod('DELETE')
            ->getForm();
    }
}
