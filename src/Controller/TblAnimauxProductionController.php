<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblAnimauxProduction;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblAnimauxProduction controller.
 *
 * @Route("/{_locale}/references/tblanimauxProduction")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblAnimauxProductionController extends AbstractController
{
    /**
     * Deletes a TblAnimauxProduction entity.
     *
     * @Route("/{id}", name="references_tblanimauxProduction_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblAnimauxProduction $tblAnimauxProduction)
    {
        $form = $this->createDeleteForm($tblAnimauxProduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblAnimauxProduction);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblanimauxProduction_index');
    }

    /**
     * Displays a form to edit an existing TblAnimauxProduction entity.
     *
     * @Route("/{id}/edit", name="references_tblanimauxProduction_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblAnimauxProduction $tblAnimauxProduction)
    {
        $deleteForm = $this->createDeleteForm($tblAnimauxProduction);
        $editForm = $this->createForm('App\Form\TblAnimauxProductionType', $tblAnimauxProduction);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAnimauxProduction);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblanimauxProduction_index', ['id' => $tblAnimauxProduction->getId()]);
        }

        return $this->render('tblanimauxproduction/edit.html.twig', [
            'tblAnimauxProduction' => $tblAnimauxProduction,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblAnimauxProduction entities.
     *
     * @Route("/", name="references_tblanimauxProduction_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblAnimauxProductions = $em->getRepository(TblAnimauxProduction::class)->findAll();

        return $this->render('tblanimauxproduction/index.html.twig', [
            'tblAnimauxProductions' => $tblAnimauxProductions,
        ]);
    }

    /**
     * Creates a new TblAnimauxProduction entity.
     *
     * @Route("/new", name="references_tblanimauxProduction_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblAnimauxProduction = new TblAnimauxProduction($user);
        $form = $this->createForm('App\Form\TblAnimauxProductionType', $tblAnimauxProduction);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAnimauxProduction);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblanimauxProduction_index', ['id' => $tblAnimauxProduction->getId()]);
        }

        return $this->render('tblanimauxproduction/new.html.twig', [
            'tblAnimauxProduction' => $tblAnimauxProduction,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblAnimauxProduction entity.
     *
     * @Route("/{id}", name="references_tblanimauxProduction_show", methods={"GET"})
     */
    public function showAction(TblAnimauxProduction $tblAnimauxProduction)
    {
        $deleteForm = $this->createDeleteForm($tblAnimauxProduction);

        return $this->render('tblanimauxproduction/show.html.twig', [
            'tblAnimauxProduction' => $tblAnimauxProduction,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblAnimauxProduction entity.
     *
     * @param TblAnimauxProduction $tblAnimauxProduction The TblAnimauxProduction entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblAnimauxProduction $tblAnimauxProduction)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('C:_puma_git_references_tblanimauxProduction_delete', ['id' => $tblAnimauxProduction->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
