<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblVarieteProduit;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblVarieteProduit controller.
 *
 * @Route("/{_locale}/references/tblvarieteproduit")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblVarieteProduitController extends AbstractController
{
    /**
     * Deletes a TblVarieteProduit entity.
     *
     * @Route("/{id}", name="references_tblvarieteproduit_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblVarieteProduit $tblVarieteProduit)
    {
        $form = $this->createDeleteForm($tblVarieteProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblVarieteProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_index');
    }

    /**
     * Displays a form to edit an existing TblVarieteProduit entity.
     *
     * @Route("/{id}/edit", name="references_tblvarieteproduit_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblVarieteProduit $tblVarieteProduit)
    {
        $deleteForm = $this->createDeleteForm($tblVarieteProduit);
        $editForm = $this->createForm('App\Form\TblVarieteProduitType', $tblVarieteProduit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblVarieteProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblvarieteproduit_index', ['id' => $tblVarieteProduit->getId()]);
        }

        return $this->render('tblvarieteproduit/edit.html.twig', [
            'tblVarieteProduit' => $tblVarieteProduit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblVarieteProduit entities.
     *
     * @Route("/", name="references_tblvarieteproduit_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblVarieteProduits = $em->getRepository(TblVarieteProduit::class)->findAll();

        return $this->render('tblvarieteproduit/index.html.twig', [
            'tblVarieteProduits' => $tblVarieteProduits,
        ]);
    }

    /**
     * Creates a new TblVarieteProduit entity.
     *
     * @Route("/new", name="references_tblvarieteproduit_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblVarieteProduit = new TblVarieteProduit($user);
        $form = $this->createForm('App\Form\TblVarieteProduitType', $tblVarieteProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblVarieteProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblvarieteproduit_index', ['id' => $tblVarieteProduit->getId()]);
        }

        return $this->render('tblvarieteproduit/new.html.twig', [
            'tblVarieteProduit' => $tblVarieteProduit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblVarieteProduit entity.
     *
     * @Route("/{id}", name="references_tblvarieteproduit_show", methods={"GET"})
     */
    public function showAction(TblVarieteProduit $tblVarieteProduit)
    {
        $deleteForm = $this->createDeleteForm($tblVarieteProduit);

        return $this->render('tblvarieteproduit/show.html.twig', [
            'tblVarieteProduit' => $tblVarieteProduit,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblVarieteProduit entity.
     *
     * @param TblVarieteProduit $tblVarieteProduit The TblVarieteProduit entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblVarieteProduit $tblVarieteProduit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblvarieteproduit_delete', ['id' => $tblVarieteProduit->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
