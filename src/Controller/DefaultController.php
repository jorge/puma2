<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Repository\SynchroStatusRepository;
use App\Service\ResponderInterface;
use App\Utils\Path;
use Doctrine\ORM\EntityManagerInterface;
use Michelf\Markdown;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Routing\Annotation\Route;

final class DefaultController
{
    /**
     * @Route("/{_locale}/help/", name="help_index")
     * @Route("/{_locale}/help/{page}/", name="help")
     *
     * @param mixed|null $page
     */
    public function helpAction(?string $page, ResponderInterface $responder, ParameterBagInterface $parameterBagInterface)
    {
        $mdFile = Path::from($parameterBagInterface->get('wiki_path'), $page . '.md');

        if (file_exists($mdFile)) {
            $markdownContent = file_get_contents($mdFile);
            $data = Markdown::defaultTransform($markdownContent);

            return $responder->render(
                'default/page_html.html.twig',
                [
                    'data' => $data,
                ]
            );
        }

        //todo faire regex pour remplacer [XXX]({}) par [XXX](/help/{})
        // + target new

        // todo : à traiter :
        // return $this->render('default/signaletique.html.twig');

        // sinon par défaut :
        return $responder->render('default/help.html.twig');
    }

    /**
     * @Route("/{_locale}/", name="homepage")
     */
    public function indexAction(SynchroStatusRepository $repository, ResponderInterface $responder, EntityManagerInterface $em)
    {
        return $responder
            ->render(
                'default/index.html.twig',
                [
                    'last_commit_id' => exec('git log --format="%H" -n 1'),
                    'local_computer_id' => $repository->findUnique()->getLocalId(),
                    'infoDB' => $this->_getInfoDB($em),
                ]
            );
    }

    /**
     * @Route("/", name="local_redirect")
     */
    public function localRedirectAction(ResponderInterface $responder)
    {
        return $responder->redirectToRoute('homepage');
    }

    private function _getInfoDB(EntityManagerInterface $em): array
    {
        // Todo: Use QueryBuilder.
        $baseQuery = [
            'SELECT coop.denomination, COUNT(e.id) AS nombre',
            'FROM exploitations e',
            'INNER JOIN pivot_exploitation_organisations piv ON e.id =piv.exploitation_id',
            'INNER JOIN tbl_organisations grp ON grp.id = piv.organisation_id',
            'INNER JOIN tbl_organisations coop ON grp.parent_id = coop.id',
            'INNER JOIN personnes p ON p.exploitation_id = e.id',
            'WHERE e.radie=0',
            'AND p.radie=0',
            'AND p.repondant=1',
            'GROUP BY coop.denomination',
        ];

        $stmt = $em->getConnection()->prepare(implode(' ', $baseQuery));
        $stmt->execute();

        return array_map(
            static fn (array $row): string => sprintf('%s: %s', $row['denomination'], $row['nombre']),
            $stmt->fetchAll()
        );
    }
}
