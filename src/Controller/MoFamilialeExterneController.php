<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\MoFamilialeExterne;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

/**
 * MoFamilialeExterne controller.
 *
 * @Route("/{_locale}/references_mofamilialeexterne")
 */
final class MoFamilialeExterneController extends AbstractController
{
    /**
     * Deletes a MoFamilialeExterne entity.
     *
     * @Route("/{id}", name="references_mofamilialeexterne_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, MoFamilialeExterne $moFamilialeExterne)
    {
        $exploitation = $moFamilialeExterne->getExploitation();
        $form = $this->createDeleteForm($moFamilialeExterne, [
            'exploitation_id' => $exploitation->getId(),
            'annee' => $moFamilialeExterne->getAnnee(),
        ]);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $moFamilialeExterne->setRadie(true);
            $em->persist($moFamilialeExterne);
            $em->flush();
        }

        return $this->redirectToRoute(
            'saisie_f0_membre',
            [
                'id' => $exploitation->getId(),
                'annee' => $moFamilialeExterne->getAnnee(),
                'exploitation_id' => $exploitation->getId(), ]
        );
    }

    /**
     * Displays a form to edit an existing MoFamilialeExterne entity.
     *
     * @Route("/{id}/edit", name="references_mofamilialeexterne_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, MoFamilialeExterne $moFamilialeExterne)
    {
        $deleteForm = $this->createDeleteForm($moFamilialeExterne);
        $editForm = $this->createForm('App\Form\MoFamilialeExterneType', $moFamilialeExterne);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($moFamilialeExterne);
            $em->flush();

            return $this->redirectToRoute(
                'saisie_f0_membre',
                [
                    'id' => $moFamilialeExterne->getExploitation()->getId(),
                    'exploitation_id' => $moFamilialeExterne->getExploitation()->getId(),
                    'annee' => $moFamilialeExterne->getAnnee(),
                ]
            );
        }

        return $this->render('mofamilialeexterne/edit.html.twig', [
            'moFamilialeExterne' => $moFamilialeExterne,
            'edit_form' => $editForm->createView(),
            'exploitation' => $moFamilialeExterne->getExploitation(),
            'annee' => $moFamilialeExterne->getAnnee(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all MoFamilialeExterne entities.
     *
     * @Route("/", name="references_mofamilialeexterne_index", methods={"GET"})
     */
    public function indexAction(Request $request)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $request->query->get('annee');

        $em = $this->getDoctrine()->getManager();

        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        // $moFamilialeExternes=$exploitation->getMo_familiale_externes();
        $moFamilialeExternes = $em->getRepository(MoFamilialeExterne::class)->findBy([
            'exploitation' => $exploitation,
            'annee' => $annee,
            'radie' => 0,
        ]);

        return $this->render('mofamilialeexterne/index.html.twig', [
            'moFamilialeExternes' => $moFamilialeExternes,
            'annee' => $annee,
            //          'coop_id' => $coop_id,
            //          'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
        ]);
    }

    /**
     * Creates a new MoFamilialeExterne entity.
     *
     * @Route("/new/{exploitation}/{annee}", name="references_mofamilialeexterne_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Exploitations $exploitation, int $annee)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $moFamilialeExterne = new MoFamilialeExterne($user);

        $moFamilialeExterne->setExploitation($exploitation);
        $moFamilialeExterne->setAnnee($annee);

        $form = $this->createForm('App\Form\MoFamilialeExterneType', $moFamilialeExterne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $moFamilialeExterne->setRadie(false);
            $em->persist($moFamilialeExterne);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', ['id' => $exploitation->getId(), 'annee' => $annee, 'exploitation_id' => $exploitation->getId()]);
        }

        return $this->render('mofamilialeexterne/new.html.twig', [
            'exploitation_id' => $exploitation->getId(),
            'annee' => $annee,
            'exploitation' => $exploitation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a MoFamilialeExterne entity.
     *
     * @Route("/{id}", name="references_mofamilialeexterne_show", methods={"GET"})
     */
    public function showAction(MoFamilialeExterne $moFamilialeExterne)
    {
        $deleteForm = $this->createDeleteForm($moFamilialeExterne);

        return $this->render('mofamilialeexterne/show.html.twig', [
            'moFamilialeExterne' => $moFamilialeExterne,
            'exploitation' => $moFamilialeExterne->getExploitation(),
            'annee' => $moFamilialeExterne->getAnnee('annee'),
        ]);
    }

    /**
     * Creates a form to delete a MoFamilialeExterne entity.
     *
     * @param MoFamilialeExterne $moFamilialeExterne The MoFamilialeExterne entity
     * @param mixed              $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MoFamilialeExterne $moFamilialeExterne, $options = [])
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_mofamilialeexterne_delete', [
                'id' => $moFamilialeExterne->getId(),
                'exploitation_id' => $moFamilialeExterne->getExploitation()->getId(),
                'annee' => $moFamilialeExterne->getAnnee(), ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
