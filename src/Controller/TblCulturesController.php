<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblCultures;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblCultures controller.
 *
 * @Route("/{_locale}/references/tblcultures")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblCulturesController extends AbstractController
{
    /**
     * Deletes a TblCultures entity.
     *
     * @Route("/{id}", name="references_tblcultures_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblCultures $tblCulture)
    {
        $form = $this->createDeleteForm($tblCulture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblCulture);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblcultures_index');
    }

    /**
     * Displays a form to edit an existing TblCultures entity.
     *
     * @Route("/{id}/edit", name="references_tblcultures_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblCultures $tblCulture)
    {
        $deleteForm = $this->createDeleteForm($tblCulture);
        $editForm = $this->createForm('App\Form\TblCulturesType', $tblCulture);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblCulture);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcultures_index', ['id' => $tblCulture->getId()]);
        }

        return $this->render('tblcultures/edit.html.twig', [
            'tblCulture' => $tblCulture,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblCultures entities.
     *
     * @Route("/", name="references_tblcultures_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblCultures = $em->getRepository(TblCultures::class)->findAll();

        return $this->render('tblcultures/index.html.twig', [
            'tblCultures' => $tblCultures,
        ]);
    }

    /**
     * Creates a new TblCultures entity.
     *
     * @Route("/new", name="references_tblcultures_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblCulture = new TblCultures($user);
        $form = $this->createForm('App\Form\TblCulturesType', $tblCulture);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblCulture);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcultures_index', ['id' => $tblCulture->getId()]);
        }

        return $this->render('tblcultures/new.html.twig', [
            'tblCulture' => $tblCulture,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblCultures entity.
     *
     * @Route("/{id}", name="references_tblcultures_show", methods={"GET"})
     */
    public function showAction(TblCultures $tblCulture)
    {
        $deleteForm = $this->createDeleteForm($tblCulture);

        return $this->render('tblcultures/show.html.twig', [
            'tblCulture' => $tblCulture,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblCultures entity.
     *
     * @param TblCultures $tblCulture The TblCultures entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblCultures $tblCulture)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblcultures_delete', ['id' => $tblCulture->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
