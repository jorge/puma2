<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\FauxDoublons;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * FauxDoublons controller.
 *
 * @Route("/{_locale}/fauxdoublons")
 */

use Symfony\Component\Routing\Annotation\Route;

final class FauxDoublonsController extends AbstractController
{
    /**
     * Deletes a FauxDoublons entity.
     *
     * @Route("/delete/{id}", name="fauxdoublons_delete")
     */
    public function deleteAction(Request $request, FauxDoublons $fauxDoublon)
    {
        $em = $this->getDoctrine()->getManager();
        $em->remove($fauxDoublon);
        $em->flush();

        return $this->redirectToRoute('fauxdoublons_index');
    }

    /**
     * Lists all FauxDoublons entities.
     *
     * @Route("/", name="fauxdoublons_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $fauxDoublons = $em->getRepository(FauxDoublons::class)->findAll();

        return $this->render('fauxdoublons/index.html.twig', [
            'fauxDoublons' => $fauxDoublons,
        ]);
    }

    /**
     * Creates a new FauxDoublons entity.
     *
     * @Route("/new/{exploitationIds}/", name="fauxdoublons_new", methods={"GET", "POST"})
     *
     * @param mixed $exploitationIds
     */
    public function newAction(Request $request, $exploitationIds)
    {
        $this->_createFauxDoublon($exploitationIds);

        return $this->redirectToRoute('fauxdoublons_index');
    }

    /**
     * Indiquer un faux doublon (faux positif) pour une recherche.
     *
     * @Route("/register/pas/doublon/{exploitationIds}", name="faux_doublon_pour_recherche", methods={"GET", "POST"})
     *
     * @param mixed $exploitationIds
     */
    public function registerFauxDoublonPourRechercheAction($exploitationIds)
    {
        $this->_createFauxDoublon($exploitationIds);

        return $this->redirectToRoute('admin_controleDoublons');
    }

    private function _createFauxDoublon($exploitationIds)
    {
        $user = $this->getUser();
        $fauxDoublon = new FauxDoublons($user);

        $exploitationIdsArr = explode(',', $exploitationIds);
        array_map('intval', $exploitationIdsArr);
        sort($exploitationIdsArr);
        $fauxDoublon->setExploitationIds($exploitationIdsArr);

        $em = $this->getDoctrine()->getManager();
        $em->persist($fauxDoublon);
        $em->flush();

        $this->addFlash('success', 'Faux doublon ajouté');
    }
}
