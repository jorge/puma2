<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\EngraisDemande;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * EngraisDemande controller.
 *
 * @Route("/{_locale}/references/engraisdemande")
 */

use Symfony\Component\Routing\Annotation\Route;

final class EngraisDemandeController extends AbstractController
{
    /**
     * Deletes a EngraisDemande entity.
     *
     * @Route("/{id}", name="references_engraisdemande_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, EngraisDemande $engraisDemande)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $form = $this->createDeleteForm($engraisDemande, [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $engraisDemande->setRadie(TRUE);
            $em->persist($engraisDemande);
            $em->flush();
        }

        return $this->redirectToRoute('references_engraisdemande_index', [
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing EngraisDemande entity.
     *
     * @Route("/{id}/edit", name="references_engraisdemande_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, EngraisDemande $engraisDemande)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $deleteForm = $this->createDeleteForm($engraisDemande);
        $editForm = $this->createForm('App\Form\EngraisDemandeType', $engraisDemande);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($engraisDemande);
            $em->flush();

            return $this->redirectToRoute('references_engraisdemande_edit', ['id' => $engraisDemande->getId()]);
        }

        return $this->render('engraisdemande/edit.html.twig', [
            'engraisDemande' => $engraisDemande,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all EngraisDemande entities.
     *
     * @Route("/", name="references_engraisdemande_index", methods={"GET"})
     */
    public function indexAction()
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $em = $this->getDoctrine()->getManager();

        $engraisDemandes = $em->getRepository(EngraisDemande::class)->findAll();

        return $this->render('engraisdemande/index.html.twig', [
            //                          'coop_id' => $coop_id,
            //              'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'engraisDemandes' => $engraisDemandes,
        ]);
    }

    /**
     * Creates a new EngraisDemande entity.
     *
     * @Route("/new", name="references_engraisdemande_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $user = $this->getUser();
        $engraisDemande = new EngraisDemande($user);
        $form = $this->createForm('App\Form\EngraisDemandeType', $engraisDemande);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $engraisDemande->setRadie(FALSE);
            $em->persist($engraisDemande);
            $em->flush();

            return $this->redirectToRoute('references_engraisdemande_show', ['id' => $engraisDemande->getId()]);
        }

        return $this->render('engraisdemande/new.html.twig', [
            'engraisDemande' => $engraisDemande,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a EngraisDemande entity.
     *
     * @Route("/{id}", name="references_engraisdemande_show", methods={"GET"})
     */
    public function showAction(EngraisDemande $engraisDemande)
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET('annee');

        $deleteForm = $this->createDeleteForm($engraisDemande);

        return $this->render('engraisdemande/show.html.twig', [
            'engraisDemande' => $engraisDemande,
            'coop_id' => $coop_id,
            'group_id' => $group_id,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,

            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a EngraisDemande entity.
     *
     * @param EngraisDemande $engraisDemande The EngraisDemande entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(EngraisDemande $engraisDemande, $options = [])
    {
        $coop_id = $_GET['coop_id'];
        $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'references_engraisdemande_delete',
                ['id' => $engraisDemande->getId(),
                    'coop_id' => $coop_id,
                    'group_id' => $group_id,
                    'exploitation_id' => $exploitation_id,
                    'annee' => $annee,
                ]
            ))
            ->setMethod('DELETE')
            ->getForm();
    }
}
