<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblSituation;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblSituation controller.
 *
 * @Route("/{_locale}/references/tblsituation")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblSituationController extends AbstractController
{
    /**
     * Deletes a TblSituation entity.
     *
     * @Route("/{id}", name="references_tblsituation_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblSituation $tblSituation)
    {
        $form = $this->createDeleteForm($tblSituation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblSituation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblsituation_index');
    }

    /**
     * Displays a form to edit an existing TblSituation entity.
     *
     * @Route("/{id}/edit", name="references_tblsituation_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblSituation $tblSituation)
    {
        $deleteForm = $this->createDeleteForm($tblSituation);
        $editForm = $this->createForm('App\Form\TblSituationType', $tblSituation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblSituation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblsituation_index', ['id' => $tblSituation->getId()]);
        }

        return $this->render('tblsituation/edit.html.twig', [
            'tblSituation' => $tblSituation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblSituation entities.
     *
     * @Route("/", name="references_tblsituation_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblSituations = $em->getRepository(TblSituation::class)->findAll();

        return $this->render('tblsituation/index.html.twig', [
            'tblSituations' => $tblSituations,
        ]);
    }

    /**
     * Creates a new TblSituation entity.
     *
     * @Route("/new", name="references_tblsituation_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblSituation = new TblSituation($user);
        $form = $this->createForm('App\Form\TblSituationType', $tblSituation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblSituation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblsituation_index', ['id' => $tblSituation->getId()]);
        }

        return $this->render('tblsituation/new.html.twig', [
            'tblSituation' => $tblSituation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblSituation entity.
     *
     * @Route("/{id}", name="references_tblsituation_show", methods={"GET"})
     */
    public function showAction(TblSituation $tblSituation)
    {
        $deleteForm = $this->createDeleteForm($tblSituation);

        return $this->render('tblsituation/show.html.twig', [
            'tblSituation' => $tblSituation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblSituation entity.
     *
     * @param TblSituation $tblSituation The TblSituation entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblSituation $tblSituation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblsituation_delete', ['id' => $tblSituation->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
