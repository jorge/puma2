<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblDecoupageAdmin;
use App\Entity\TblDecoupageAdminLevel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblDecoupageAdmin controller.
 *
 * @Route("/{_locale}/references/tbldecoupageadmin")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblDecoupageAdminController extends AbstractController
{
    /**
     * Choice of a TblDecoupageAdminLevel for displaying
     * the list of all the for the TblDecoupageAdmin entities.
     *
     * @Route("/", name="references_tbldecoupageadmin_choice_level", methods={"GET"})
     */
    public function choiceLevelAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblDecoupageAdminLevels = $em->getRepository(TblDecoupageAdminLevel::class)->findAll();

        return $this->render('tbldecoupageadmin/choice_level.html.twig', [
            'tblDecoupageAdminLevels' => $tblDecoupageAdminLevels,
        ]);
    }

    /**
     * Deletes a TblDecoupageAdmin entity.
     *
     * @Route("/{id}", name="references_tbldecoupageadmin_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblDecoupageAdmin $tblDecoupageAdmin)
    {
        $form = $this->createDeleteForm($tblDecoupageAdmin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblDecoupageAdmin);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbldecoupageadmin_choice_level');
    }

    /**
     * Displays a form to edit an existing TblDecoupageAdmin entity.
     *
     * @Route("/{id}/edit", name="references_tbldecoupageadmin_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblDecoupageAdmin $tblDecoupageAdmin)
    {
        $deleteForm = $this->createDeleteForm($tblDecoupageAdmin);
        $editForm = $this->createForm('App\Form\TblDecoupageAdminType', $tblDecoupageAdmin);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblDecoupageAdmin);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute(
                'references_tbldecoupageadmin_list',
                ['level' => $tblDecoupageAdmin->getLevel()->getId()]
            );
        }

        return $this->render('tbldecoupageadmin/edit.html.twig', [
            'tblDecoupageAdmin' => $tblDecoupageAdmin,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblDecoupageAdmin entities for a given level (country, province, ...).
     *
     * @Route("/list/level/{level}/", name="references_tbldecoupageadmin_list", methods={"GET"})
     */
    public function listAction(TblDecoupageAdminLevel $level)
    {
        $em = $this->getDoctrine()->getManager();

        $tblDecoupageAdmins = $em->getRepository(TblDecoupageAdmin::class)->findBy(['level' => $level]);

        return $this->render('tbldecoupageadmin/list_by_level.html.twig', [
            'tblDecoupageAdmins' => $tblDecoupageAdmins,
            'level' => $level,
        ]);
    }

    /**
     * Creates a new TblDecoupageAdmin entity.
     *
     * @Route("/new", name="references_tbldecoupageadmin_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblDecoupageAdmin = new TblDecoupageAdmin($user);
        $form = $this->createForm('App\Form\TblDecoupageAdminType', $tblDecoupageAdmin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblDecoupageAdmin);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute(
                'references_tbldecoupageadmin_list',
                ['level' => $tblDecoupageAdmin->getLevel()->getId()]
            );
        }

        return $this->render('tbldecoupageadmin/new.html.twig', [
            'tblDecoupageAdmin' => $tblDecoupageAdmin,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblDecoupageAdmin entity.
     *
     * @Route("/{id}", name="references_tbldecoupageadmin_show", methods={"GET"})
     */
    public function showAction(TblDecoupageAdmin $tblDecoupageAdmin)
    {
        $deleteForm = $this->createDeleteForm($tblDecoupageAdmin);

        return $this->render('tbldecoupageadmin/show.html.twig', [
            'tblDecoupageAdmin' => $tblDecoupageAdmin,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblDecoupageAdmin entity.
     *
     * @param TblDecoupageAdmin $tblDecoupageAdmin The TblDecoupageAdmin entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblDecoupageAdmin $tblDecoupageAdmin)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbldecoupageadmin_delete', ['id' => $tblDecoupageAdmin->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
