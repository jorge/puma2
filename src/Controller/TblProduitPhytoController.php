<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblProduitPhyto;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblProduitPhyto controller.
 *
 * @Route("/{_locale}/references/tblproduitphyto")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblProduitPhytoController extends AbstractController
{
    /**
     * Deletes a TblProduitPhyto entity.
     *
     * @Route("/{id}", name="references_tblproduitphyto_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblProduitPhyto $tblProduitPhyto)
    {
        $form = $this->createDeleteForm($tblProduitPhyto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblProduitPhyto);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblproduitphyto_index');
    }

    /**
     * Displays a form to edit an existing TblProduitPhyto entity.
     *
     * @Route("/{id}/edit", name="references_tblproduitphyto_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblProduitPhyto $tblProduitPhyto)
    {
        $deleteForm = $this->createDeleteForm($tblProduitPhyto);
        $editForm = $this->createForm('App\Form\TblProduitPhytoType', $tblProduitPhyto);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblProduitPhyto);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblproduitphyto_index', ['id' => $tblProduitPhyto->getId()]);
        }

        return $this->render('tblproduitphyto/edit.html.twig', [
            'tblProduitPhyto' => $tblProduitPhyto,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblProduitPhyto entities.
     *
     * @Route("/", name="references_tblproduitphyto_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblProduitPhytos = $em->getRepository(TblProduitPhyto::class)->findAll();

        return $this->render('tblproduitphyto/index.html.twig', [
            'tblProduitPhytos' => $tblProduitPhytos,
        ]);
    }

    /**
     * Creates a new TblProduitPhyto entity.
     *
     * @Route("/new", name="references_tblproduitphyto_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblProduitPhyto = new TblProduitPhyto($user);
        $form = $this->createForm('App\Form\TblProduitPhytoType', $tblProduitPhyto);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblProduitPhyto);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblproduitphyto_index', ['id' => $tblProduitPhyto->getId()]);
        }

        return $this->render('tblproduitphyto/new.html.twig', [
            'tblProduitPhyto' => $tblProduitPhyto,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblProduitPhyto entity.
     *
     * @Route("/{id}", name="references_tblproduitphyto_show", methods={"GET"})
     */
    public function showAction(TblProduitPhyto $tblProduitPhyto)
    {
        $deleteForm = $this->createDeleteForm($tblProduitPhyto);

        return $this->render('tblproduitphyto/show.html.twig', [
            'tblProduitPhyto' => $tblProduitPhyto,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblProduitPhyto entity.
     *
     * @param TblProduitPhyto $tblProduitPhyto The TblProduitPhyto entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblProduitPhyto $tblProduitPhyto)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblproduitphyto_delete', ['id' => $tblProduitPhyto->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
