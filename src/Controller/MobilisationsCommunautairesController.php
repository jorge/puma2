<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\MobilisationsCommunautaires;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * MobilisationsCommunautaires controller.
 *
 * @Route("/{_locale}/references/mobilisationscommunautaires")
 */

use Symfony\Component\Routing\Annotation\Route;

final class MobilisationsCommunautairesController extends AbstractController
{
    /**
     * Deletes a MobilisationsCommunautaires entity.
     *
     * @Route("/{id}", name="references_mobilisationscommunautaires_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, MobilisationsCommunautaires $mobilisationsCommunautaire)
    {
        $form = $this->createDeleteForm($mobilisationsCommunautaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $mobilisationsCommunautaire->setRadie(TRUE);
            $em->persist($mobilisationsCommunautaire);
            $em->flush();
        }

        return $this->redirectToRoute('references_suiviorganisations_edit', [
            'id' => $mobilisationsCommunautaire->getSuiviOrganisation(), ]);
    }

    /**
     * Displays a form to edit an existing MobilisationsCommunautaires entity.
     *
     * @Route("/{id}/edit", name="references_mobilisationscommunautaires_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, MobilisationsCommunautaires $mobilisationsCommunautaire)
    {
        $deleteForm = $this->createDeleteForm($mobilisationsCommunautaire);
        $editForm = $this->createForm('App\Form\MobilisationsCommunautairesType', $mobilisationsCommunautaire);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($mobilisationsCommunautaire);
            $em->flush();

            return $this->redirectToRoute('references_suiviorganisations_edit', [
                'id' => $mobilisationsCommunautaire->getSuiviOrganisation(), ]);
        }

        return $this->render('mobilisationscommunautaires/edit.html.twig', [
            'mobilisationsCommunautaire' => $mobilisationsCommunautaire,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all MobilisationsCommunautaires entities.
     *
     * @Route("/", name="references_mobilisationscommunautaires_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $mobilisationsCommunautaires = $em->getRepository(MobilisationsCommunautaires::class)->findAll();

        return $this->render('mobilisationscommunautaires/index.html.twig', [
            'mobilisationsCommunautaires' => $mobilisationsCommunautaires,
        ]);
    }

    /**
     * Creates a new MobilisationsCommunautaires entity.
     *
     * @Route("/new", name="references_mobilisationscommunautaires_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $suivi_id = $request->query->get('suivi_id');
        $em = $this->getDoctrine()->getManager();
        $suivi = $em->getRepository(SuiviOrganisations::class)->find($suivi_id);
        $user = $this->getUser();
        $mobilisationsCommunautaire = new MobilisationsCommunautaires($user);
        $mobilisationsCommunautaire->setSuiviOrganisation($suivi);
        $form = $this->createForm('App\Form\MobilisationsCommunautairesType', $mobilisationsCommunautaire);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $mobilisationsCommunautaire->setRadie(FALSE);
            $em->persist($mobilisationsCommunautaire);
            $em->flush();

            return $this->redirectToRoute('references_suiviorganisations_edit', [
                'id' => $mobilisationsCommunautaire->getSuiviOrganisation(), ]);
        }

        return $this->render('mobilisationscommunautaires/new.html.twig', [
            'mobilisationsCommunautaire' => $mobilisationsCommunautaire,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a MobilisationsCommunautaires entity.
     *
     * @Route("/{id}", name="references_mobilisationscommunautaires_show", methods={"GET"})
     */
    public function showAction(MobilisationsCommunautaires $mobilisationsCommunautaire)
    {
        $deleteForm = $this->createDeleteForm($mobilisationsCommunautaire);

        return $this->render('mobilisationscommunautaires/show.html.twig', [
            'mobilisationsCommunautaire' => $mobilisationsCommunautaire,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a MobilisationsCommunautaires entity.
     *
     * @param MobilisationsCommunautaires $mobilisationsCommunautaire The MobilisationsCommunautaires entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MobilisationsCommunautaires $mobilisationsCommunautaire, $options = [])
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'references_mobilisationscommunautaires_delete',
                ['id' => $mobilisationsCommunautaire->getId(),
                ]
            ))
            ->setMethod('DELETE')
            ->getForm();
    }
}
