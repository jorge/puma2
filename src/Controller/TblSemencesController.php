<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblSemences;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblSemences controller.
 *
 * @Route("/{_locale}/references/tblsemences")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblSemencesController extends AbstractController
{
    /**
     * Deletes a TblSemences entity.
     *
     * @Route("/{id}", name="references_tblsemences_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblSemences $tblSemence)
    {
        $form = $this->createDeleteForm($tblSemence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblSemence);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblsemences_index');
    }

    /**
     * Displays a form to edit an existing TblSemences entity.
     *
     * @Route("/{id}/edit", name="references_tblsemences_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblSemences $tblSemence)
    {
        $deleteForm = $this->createDeleteForm($tblSemence);
        $editForm = $this->createForm('App\Form\TblSemencesType', $tblSemence);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblSemence);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblsemences_index', ['id' => $tblSemence->getId()]);
        }

        return $this->render('tblsemences/edit.html.twig', [
            'tblSemence' => $tblSemence,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblSemences entities.
     *
     * @Route("/", name="references_tblsemences_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblSemences = $em->getRepository(TblSemences::class)->findAll();

        return $this->render('tblsemences/index.html.twig', [
            'tblSemences' => $tblSemences,
        ]);
    }

    /**
     * Creates a new TblSemences entity.
     *
     * @Route("/new", name="references_tblsemences_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblSemence = new TblSemences($user);
        $form = $this->createForm('App\Form\TblSemencesType', $tblSemence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblSemence);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblsemences_index', ['id' => $tblSemence->getId()]);
        }

        return $this->render('tblsemences/new.html.twig', [
            'tblSemence' => $tblSemence,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblSemences entity.
     *
     * @Route("/{id}", name="references_tblsemences_show", methods={"GET"})
     */
    public function showAction(TblSemences $tblSemence)
    {
        $deleteForm = $this->createDeleteForm($tblSemence);

        return $this->render('tblsemences/show.html.twig', [
            'tblSemence' => $tblSemence,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblSemences entity.
     *
     * @param TblSemences $tblSemence The TblSemences entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblSemences $tblSemence)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblsemences_delete', ['id' => $tblSemence->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
