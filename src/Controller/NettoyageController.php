<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Listecooperatives;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class NettoyageController.
 *
 * @Route("/{_locale}")
 */

use Symfony\Component\Routing\Annotation\Route;

final class NettoyageController extends AbstractController
{
    public function _effacecoop($effacer)
    {
        $em = $this->getDoctrine()->getManager();
        // pour les données des suivis des groupements de la cooperative
        $em->getConnection()->executeUpdate("DELETE FROM services_payants_aux_membres
                WHERE suivi_organisation_id IN
                    (SELECT id FROM suivi_organisations s
                     WHERE s.organisation_id IN
                        (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM services_aux_membres WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM prestation_de_services WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM unite_de_transformation WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM besoin_service_aux_membres WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM besoins_formations WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM besoins_institutionel WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM equipement_agricole WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM infrastructures WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM materiel_de_bureau WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM materiel_roulant WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM mobilisations_communautaires WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM niveau_de_reconnaissance WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM suivi_organisations
    WHERE organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "');");
        //  Pour les contacts des groupements des coopératives
        $em->getConnection()->executeUpdate("DELETE FROM personne_details WHERE personnes_id  IN
    (SELECT personne_id FROM pivot_personnes_organisations p WHERE p.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        //  on peux pas effacer les personnes liées a la organisation
        $em->getConnection()->executeUpdate("UPDATE personnes SET radie=1 WHERE id  IN
    (SELECT personne_id FROM pivot_personnes_organisations p
    WHERE p.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM pivot_personnes_organisations WHERE organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "');");
        //  apres avoir effacé le pivot, on peux effacer les personnes
        $em->getConnection()->executeUpdate('DELETE FROM personnes WHERE radie=1;');
        //  pour la coopérative
        $em->getConnection()->executeUpdate("DELETE FROM services_payants_aux_membres WHERE suivi_organisation_id IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM services_aux_membres WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM prestation_de_services WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM unite_de_transformation WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM besoin_service_aux_membres WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM besoins_formations WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM besoins_institutionel WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM equipement_agricole WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM infrastructures WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM materiel_de_bureau WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM materiel_roulant WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM mobilisations_communautaires WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM niveau_de_reconnaissance WHERE suivi_organisation_id  IN
    (SELECT id FROM suivi_organisations s
    WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM suivi_organisations
    WHERE organisation_id='" . $effacer . "';");
        //  les contacts de la coopérative
        $em->getConnection()->executeUpdate("DELETE FROM personne_details WHERE personnes_id  IN
    (SELECT personne_id FROM pivot_personnes_organisations p WHERE p.organisation_id='" . $effacer . "');");
        //  on peux pas effacer les personnes liées a la organisation
        $em->getConnection()->executeUpdate("UPDATE personnes SET radie=1 WHERE id  IN
    (SELECT personne_id FROM pivot_personnes_organisations p
    WHERE p.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM pivot_personnes_organisations WHERE organisation_id='" . $effacer . "';");
        // apres avoir effacé le pivot, on peux effacer les personnes
        $em->getConnection()->executeUpdate('DELETE FROM personnes WHERE radie=1;');

        // pour les exploitations
        $em->getConnection()->executeUpdate("DELETE FROM mo_toutes_applis WHERE mo_toutes_applis.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_amendements WHERE parcelle_amendements.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_fumure_min WHERE parcelle_fumure_min.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_fumure_org WHERE parcelle_fumure_org.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_herbicide WHERE parcelle_herbicide.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_phytosanitaire WHERE parcelle_phytosanitaire.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_recoltes WHERE parcelle_recoltes.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_semences WHERE parcelle_semences.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_techniques WHERE parcelle_techniques.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM historique_cultures WHERE parcelle_historique_id IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND ph.parcelle_id=p.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_historiques WHERE parcelle_historiques.parcelle_id IN
    (SELECT p.id
     FROM parcelles p, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND p.exploitation_id=e.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_alimentation WHERE animaux_alimentation.animaux_id IN
    (SELECT a.id
     FROM animaux a, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND a.exploitation_id=e.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_produits WHERE animaux_produits.animaux_id IN
    (SELECT a.id
     FROM animaux a, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND a.exploitation_id=e.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_structure WHERE animaux_structure.animaux_id IN
    (SELECT a.id
     FROM animaux a, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND a.exploitation_id=e.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_labour WHERE animaux_labour.animaux_id IN
    (SELECT a.id
     FROM animaux a, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND a.exploitation_id=e.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_production WHERE animaux_production.animaux_id IN
    (SELECT a.id
     FROM animaux a, exploitations e, pivot_exploitation_organisations pi
     WHERE pi.exploitation_id=e.id
       AND a.exploitation_id=e.id
       AND pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM animaux WHERE animaux.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM batiments WHERE batiments.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM collecte WHERE collecte.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM commercialisation_produits WHERE commercialisation_produits.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM contraintes_developpement WHERE contraintes_developpement .exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM depenses_menage WHERE depenses_menage.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM dettes WHERE dettes.exploitation_id  IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM engrais_demande WHERE engrais_demande.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM exploitation_charges WHERE exploitation_charges.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM filieres WHERE filieres.exploitation_id  IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM formations_besoin WHERE formations_besoin.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM formations_recu WHERE formations_recu.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM habitat WHERE habitat.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM mo_familiale_externe WHERE mo_familiale_externe.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM mo_familiale_interne WHERE mo_familiale_interne.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM moyens_transport WHERE moyens_transport.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM outillage WHERE outillage.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM parcelles WHERE parcelles.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM personne_details WHERE personnes_id IN
    (SELECT personnes.id
                     FROM personnes, exploitations, pivot_exploitation_organisations
                     WHERE pivot_exploitation_organisations.exploitation_id=exploitations.id
                          AND personnes.exploitation_id=exploitations.id
                          AND pivot_exploitation_organisations.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM personnes WHERE personnes.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM phyto_demande WHERE phyto_demande.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM securite_alimentaire WHERE securite_alimentaire.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM semence_demandee WHERE semence_demandee.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM services_besoin WHERE services_besoin.exploitation_id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        //  on peux pas effacer les exploitations avant d'effacer les pivots
        $em->getConnection()->executeUpdate("UPDATE exploitations set radie=1 WHERE id IN
    (SELECT pi.exploitation_id
     FROM pivot_exploitation_organisations pi
     WHERE pi.organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "'));");
        $em->getConnection()->executeUpdate("DELETE FROM pivot_exploitation_organisations WHERE organisation_id IN
    (SELECT id FROM tbl_organisations WHERE parent_id='" . $effacer . "');");
        //  maintenant, on afface les exploitations
        $em->getConnection()->executeUpdate('DELETE FROM exploitations WHERE radie=1;');

        $em->getConnection()->executeUpdate("DELETE FROM tbl_organisations WHERE parent_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM tbl_organisations WHERE id='" . $effacer . "';");

        return true;
    }

    public function _effacegroup($effacer)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->executeUpdate("DELETE FROM services_payants_aux_membres WHERE suivi_organisation_id IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM services_aux_membres WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM prestation_de_services WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM unite_de_transformation WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM besoin_service_aux_membres WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM besoins_formations WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM besoins_institutionel WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM equipement_agricole WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM infrastructures WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM materiel_de_bureau WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM materiel_roulant WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM mobilisations_communautaires WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM niveau_de_reconnaissance WHERE suivi_organisation_id  IN
            (SELECT id FROM suivi_organisations s
                WHERE s.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM suivi_organisations
            WHERE organisation_id='" . $effacer . "';");

        $em->getConnection()->executeUpdate("DELETE FROM personne_details WHERE personnes_id  IN
            (SELECT personne_id FROM pivot_personnes_organisations p WHERE p.organisation_id='" . $effacer . "');");
        //on peux pas effacer les personnes liées a la organisation
        $em->getConnection()->executeUpdate("UPDATE personnes SET radie=1 WHERE id  IN
            (SELECT personne_id FROM pivot_personnes_organisations p
                WHERE p.organisation_id='" . $effacer . "');");

        $em->getConnection()->executeUpdate("DELETE FROM pivot_personnes_organisations WHERE organisation_id='" . $effacer . "';");
        //apres avoir effacé le pivot, on peux effacer les personnes
        $em->getConnection()->executeUpdate('DELETE FROM personnes WHERE radie=1;');

        //--------------------------------------//--
        //cas effacer un groupement
        //les exploitations
        //---------------------------------------- --
        $em->getConnection()->executeUpdate("DELETE FROM mo_toutes_applis WHERE mo_toutes_applis.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_amendements WHERE parcelle_amendements.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_fumure_min WHERE parcelle_fumure_min.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_fumure_org WHERE parcelle_fumure_org.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_herbicide WHERE parcelle_herbicide.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_phytosanitaire WHERE parcelle_phytosanitaire.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_recoltes WHERE parcelle_recoltes.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_semences WHERE parcelle_semences.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_techniques WHERE parcelle_techniques.parcelle_historique_id  IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");

        $em->getConnection()->executeUpdate("DELETE FROM historique_cultures WHERE parcelle_historique_id IN
                            (SELECT ph.id
                             FROM parcelle_historiques ph, parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND ph.parcelle_id=p.id
                               AND pi.organisation_id='" . $effacer . "');");

        $em->getConnection()->executeUpdate("DELETE FROM parcelle_historiques WHERE parcelle_historiques.parcelle_id IN
                            (SELECT p.id
                             FROM parcelles p, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND p.exploitation_id=e.id
                               AND pi.organisation_id='" . $effacer . "');");

        $em->getConnection()->executeUpdate("DELETE FROM animaux_alimentation WHERE animaux_alimentation.animaux_id IN
                            (SELECT a.id
                             FROM animaux a, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND a.exploitation_id=e.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_produits WHERE animaux_produits.animaux_id IN
                            (SELECT a.id
                             FROM animaux a, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND a.exploitation_id=e.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_structure WHERE animaux_structure.animaux_id IN
                            (SELECT a.id
                             FROM animaux a, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND a.exploitation_id=e.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_labour WHERE animaux_labour.animaux_id IN
                            (SELECT a.id
                             FROM animaux a, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND a.exploitation_id=e.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_production WHERE animaux_production.animaux_id IN
                            (SELECT a.id
                             FROM animaux a, exploitations e, pivot_exploitation_organisations pi
                             WHERE pi.exploitation_id=e.id
                               AND a.exploitation_id=e.id
                               AND pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux WHERE animaux.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM batiments WHERE batiments.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM collecte WHERE collecte.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM commercialisation_produits WHERE commercialisation_produits.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM contraintes_developpement WHERE contraintes_developpement .exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM depenses_menage WHERE depenses_menage.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM dettes WHERE dettes.exploitation_id  IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM engrais_demande WHERE engrais_demande.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM exploitation_charges WHERE exploitation_charges.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM filieres WHERE filieres.exploitation_id  IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM formations_besoin WHERE formations_besoin.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM formations_recu WHERE formations_recu.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM habitat WHERE habitat.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM mo_familiale_externe WHERE mo_familiale_externe.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM mo_familiale_interne WHERE mo_familiale_interne.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM moyens_transport WHERE moyens_transport.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM outillage WHERE outillage.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelles WHERE parcelles.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM personne_details WHERE personnes_id IN
                            (SELECT personnes.id
                             FROM personnes, exploitations, pivot_exploitation_organisations
                             WHERE pivot_exploitation_organisations.exploitation_id=exploitations.id
                                  AND personnes.exploitation_id=exploitations.id
                                  AND pivot_exploitation_organisations.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM personnes WHERE personnes.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM phyto_demande WHERE phyto_demande.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM securite_alimentaire WHERE securite_alimentaire.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM semence_demandee WHERE semence_demandee.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM services_besoin WHERE services_besoin.exploitation_id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");
        //on peux pas effacer les exploitations avant d'effacer les pivots
        $em->getConnection()->executeUpdate("UPDATE exploitations set radie=1 WHERE id IN
                            (SELECT pi.exploitation_id
                             FROM pivot_exploitation_organisations pi
                             WHERE pi.organisation_id='" . $effacer . "');");

        $em->getConnection()->executeUpdate("DELETE FROM pivot_exploitation_organisations WHERE organisation_id='" . $effacer . "';");
        //maintenant, on afface les exploitations
        $em->getConnection()->executeUpdate('DELETE FROM exploitations WHERE radie=1;');

        //---------------------------------------- --
        //fin groupement
        //----------------------------------------- --
        $em->getConnection()->executeUpdate("DELETE FROM tbl_organisations WHERE id='" . $effacer . "';");

        return true;
    }

    public function _effacexploitation($effacer)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->executeUpdate("DELETE FROM mo_toutes_applis WHERE mo_toutes_applis.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_amendements WHERE parcelle_amendements.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_fumure_min WHERE parcelle_fumure_min.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_fumure_org WHERE parcelle_fumure_org.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_herbicide WHERE parcelle_herbicide.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_phytosanitaire WHERE parcelle_phytosanitaire.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_recoltes WHERE parcelle_recoltes.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_semences WHERE parcelle_semences.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_techniques WHERE parcelle_techniques.parcelle_historique_id  IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM historique_cultures WHERE parcelle_historique_id IN
    (SELECT ph.id
     FROM parcelle_historiques ph, parcelles p
     WHERE ph.parcelle_id=p.id
       AND p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM parcelle_historiques WHERE parcelle_historiques.parcelle_id IN
    (SELECT p.id
     FROM parcelles p
     WHERE p.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_alimentation WHERE animaux_alimentation.animaux_id IN
    (SELECT a.id
     FROM animaux a
     WHERE a.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_produits WHERE animaux_produits.animaux_id IN
    (SELECT a.id
     FROM animaux a
     WHERE a.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_structure WHERE animaux_structure.animaux_id IN
    (SELECT a.id
     FROM animaux a
     WHERE a.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_labour WHERE animaux_labour.animaux_id IN
    (SELECT a.id
     FROM animaux a
     WHERE a.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux_production WHERE animaux_production.animaux_id IN
    (SELECT a.id
     FROM animaux a
     WHERE a.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM animaux WHERE animaux.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM batiments WHERE batiments.exploitation_id ='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM collecte WHERE collecte.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM commercialisation_produits WHERE commercialisation_produits.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM contraintes_developpement WHERE contraintes_developpement .exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM depenses_menage WHERE depenses_menage.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM dettes WHERE dettes.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM engrais_demande WHERE engrais_demande.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM exploitation_charges WHERE exploitation_charges.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM filieres WHERE filieres.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM formations_besoin WHERE formations_besoin.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM formations_recu WHERE formations_recu.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM habitat WHERE habitat.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM mo_familiale_externe WHERE mo_familiale_externe.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM mo_familiale_interne WHERE mo_familiale_interne.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM moyens_transport WHERE moyens_transport.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM outillage WHERE outillage.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM parcelles WHERE parcelles.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM personne_details WHERE personnes_id IN
    (SELECT personnes.id
                     FROM personnes
                     WHERE personnes.exploitation_id='" . $effacer . "');");
        $em->getConnection()->executeUpdate("DELETE FROM personnes WHERE personnes.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM phyto_demande WHERE phyto_demande.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM securite_alimentaire WHERE securite_alimentaire.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM semence_demandee WHERE semence_demandee.exploitation_id='" . $effacer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM services_besoin WHERE services_besoin.exploitation_id='" . $effacer . "';");
        // on peux pas effacer les exploitations avant d'effacer les pivots
        $em->getConnection()->executeUpdate("UPDATE exploitations set radie=1 WHERE id='" . $effacer . "';");

        $em->getConnection()->executeUpdate("DELETE FROM pivot_exploitation_organisations WHERE exploitation_id='" . $effacer . "';");
        // maintenant, on afface les exploitations
        $em->getConnection()->executeUpdate('DELETE FROM exploitations WHERE radie=1;');

        return true;
    }

    /**
     * @Route("/nettoyage/coopeffacer", name="nettoyage_coopeffacer")
     */
    public function coopeffacerAction()
    {
        $effacer = $_POST['effacer'];
        $ret = $this->_effacecoop($effacer);

        return $this->redirectToRoute('nettoyage_effacercoop');
    }

    /**
     * @Route("/nettoyage/effacegroup", name="nettoyage_effacegroup")
     */
    public function effacegroupAction()
    {
        $effacer = $_POST['effacer'];
        $ret = $this->_effacegroup($effacer);

        return $this->redirectToRoute('nettoyage_effacercoop');
    }

    /**
     * @Route("/nettoyage/effacercoop", name="nettoyage_effacercoop")
     */
    public function effacercoopAction()
    {
        $result = $this->_getCooperatives();

        return $this->render('nettoyage/efface_de_liste.html.twig', [
            'recordset' => $result['recordset'],
            'fields' => $result['fields'],
            'titre' => $result['titre'],
        ]);
    }

    /**
     * @Route("/nettoyage/effacerexploitation", name="nettoyage_effacerexploitation")
     */
    public function effacerexploitationAction(Request $request)
    {
        /* TODO */
        $Listecooperatives = new Listecooperatives();
        $my_em_manager = $this->getDoctrine()->getManager();
        $form = $this->createForm('App\Form\ListeCooperativesType', $Listecooperatives, [
            'my_em_manager' => $my_em_manager,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->_exploitationsparCooperative($form);

            return $this->render('nettoyage/effacerexploitations.html.twig', [
                'titre' => $result['titre'],
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
            ]);
        }

        return $this->render('nettoyage/exploitationscooperatives.html.twig', [
            'Listecooperatives' => $Listecooperatives,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/nettoyage/effacergroup", name="nettoyage_effacergroup")
     */
    public function effacergroupAction()
    {
        $result = $this->_getListeGroupements();

        return $this->render('nettoyage/effacegroups.html.twig', [
            'recordset' => $result['recordset'],
            'fields' => $result['fields'],
            'titre' => $result['titre'],
        ]);
    }

    /**
     * @Route("/nettoyage/effacexploitation", name="nettoyage_effacexploitation")
     */
    public function effacexploitationAction()
    {
        $effacer = $_POST['effacer'];
        $ret = $this->_effacexploitation($effacer);

        return $this->redirectToRoute('nettoyage_effacerexploitation');
    }

    /**
     * @Route("/nettoyage/exploitations", name="nettoyage_exploitations")
     */
    public function exploitationsAction(Request $request)
    {
        $Listecooperatives = new Listecooperatives();
        $my_em_manager = $this->getDoctrine()->getManager();
        $form = $this->createForm('App\Form\ListeCooperativesType', $Listecooperatives, [
            'my_em_manager' => $my_em_manager,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->_exploitationsparCooperative($form);

            return $this->render('nettoyage/listeexploitations.html.twig', [
                'titre' => $result['titre'],
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
            ]);
        }

        return $this->render('nettoyage/exploitationscooperatives.html.twig', [
            'Listecooperatives' => $Listecooperatives,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/nettoyage/group_exec", name="nettoyage_group_exec")
     */
    public function group_execAction()
    {
        $recevoir = $_POST['recevoir'];
        $envoyer = $_POST['envoyer'];
        $ret = $this->movegroup($recevoir, $envoyer);

        return $this->redirectToRoute('nettoyage_groupements', ['toutes' => 0]);
    }

    /**
     * @Route("/nettoyage/groupements/{toutes}", name="nettoyage_groupements")
     *
     * @param mixed $toutes
     */
    public function groupementsAction(Request $request, $toutes)
    {
        if (1 === $toutes) {
            $result = $this->_getListeGroupements();

            return $this->render('nettoyage/enregistrements.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
            ]);
        }

        if (0 === $toutes) {
            $Listecooperatives = new Listecooperatives();
            $my_em_manager = $this->getDoctrine()->getManager();
            $form = $this->createForm('App\Form\ListeCooperativesType', $Listecooperatives, [
                'my_em_manager' => $my_em_manager,
            ]);
            $form->handleRequest($request);

            if ($form->isSubmitted() && $form->isValid()) {
                $groupements = $this->_groupesparCooperative($form);

                return $this->render('nettoyage/groupements.html.twig', [
                    'groupements' => $groupements,
                ]);
            }

            return $this->render('nettoyage/groupescooperatives.html.twig', [
                'Listecooperatives' => $Listecooperatives,
                'form' => $form->createView(),
            ]);
        }
    }

    public function GroupsDeCooperative(Request $request)
    {
        $Listecooperatives = new Listecooperatives();
        $my_em_manager = $this->getDoctrine()->getManager();
        $form = $this->createForm('App\Form\ListeCooperativesType', $Listecooperatives, [
            'my_em_manager' => $my_em_manager,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $groupements = $this->_groupesparCooperative($form);

            return $this->render('consultation/groupements.html.twig', [
                'groupements' => $groupements,
            ]);
        }

        return $this->render('consultation/groupescooperatives.html.twig', [
            '$Listecooperatives' => $Listecooperatives,
            'form' => $form->createView(),
        ]);
    }

    public function movegroup($recevoir, $envoyer)
    {
        $em = $this->getDoctrine()->getManager();
        $em->getConnection()->executeUpdate("UPDATE pivot_exploitation_organisations SET organisation_id='" . $recevoir . "' WHERE organisation_id='" . $envoyer . "';");
        $em->getConnection()->executeUpdate("UPDATE pivot_personnes_organisations SET organisation_id='" . $recevoir . "' WHERE organisation_id='" . $envoyer . "';");
        $em->getConnection()->executeUpdate("UPDATE suivi_organisations SET organisation_id='" . $recevoir . "' WHERE organisation_id='" . $envoyer . "';");
        $em->getConnection()->executeUpdate("DELETE FROM tbl_organisations WHERE id='" . $envoyer . "';");

        return true;
    }

    /**
     * @Route("/nettoyage", name="nettoyage")
     */
    public function nettoyageAction()
    {
        return $this->render('nettoyage/nettoyage.html.twig');
    }

    public function TousLesGroupements()
    {
        $result = $this->_getListeGroupements();

        return $this->render('nettoyage/enregistrements.html.twig', [
            'recordset' => $result['recordset'],
            'fields' => $result['fields'],
            'titre' => $result['titre'],
        ]);
    }

    private function _exploitationsparCooperative($form)
    {
        $coop_id = $form->get('coop')->getData()->getId();
        $coop = $form->get('coop')->getData();

        $em = $this->getDoctrine()->getManager();

        $sql = "SELECT coop.denomination as cooperative,
                    grp.denomination as groupement,
                    e.id as id,
                    e.numeroMembre as NumeroMembre,
                    e.anneeAdhesion as AnneeAdhesion,
                    tda.denomination as localisation,
                    CONCAT(pChef.nom,' ',pChef.prenom) AS chef,
                    CONCAT(pConj.nom,' ',pConj.prenom) AS conjoint,
                    pChef.dateNaissance as nee,
                    pChef.cni as cni
                FROM AppBundle:TblOrganisations grp
                 LEFT JOIN AppBundle:TblOrganisations coop WITH grp.parent=coop.id
                 LEFT JOIN AppBundle:PivotExploitationOrganisations pi WITH grp.id=pi.organisation
                 LEFT JOIN AppBundle:Exploitations e WITH pi.exploitation=e.id
                 LEFT JOIN AppBundle:TblDecoupageAdmin tda WITH e.localisationAdmin=tda.id
                 LEFT JOIN AppBundle:Personnes pChef WITH e.id=pChef.exploitation
                 LEFT JOIN AppBundle:personnes pConj WITH e.id=pConj.exploitation
                WHERE coop.id='" . $coop_id . "'
                    AND pChef.rolFamille='Chef de famille'
                    AND pConj.rolFamille='Conjoint'
                ORDER BY groupement ASC";
        $fields = [];
        $titre = 'Résultat de la liste de exploitations';
        $fields['cooperative'] = $this->get('translator')->trans('cooperative');
        $fields['groupement'] = $this->get('translator')->trans('groupement');
        $fields['id'] = $this->get('translator')->trans('id');
        $fields['NumeroMembre'] = $this->get('translator')->trans('NumeroMembre');
        $fields['AnneeAdhesion'] = $this->get('translator')->trans('AnneeAdhesion');
        $fields['localisation'] = $this->get('translator')->trans('localisation');
        $fields['chef'] = $this->get('translator')->trans('chef');
        $fields['conjoint'] = $this->get('translator')->trans('conjoint');
        $fields['nee'] = $this->get('translator')->trans('nee');
        $fields['cni'] = $this->get('translator')->trans('cni');
        $query = $em->createQuery();
        $query->setDql($sql);
        $recordset = $query->getResult();

        return [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $recordset,
        ];
    }

    private function _getListeGroupements()
    {
        $sql = 'SELECT coop.denomination as cooperative,
            grp.id as id,
            grp.denomination as groupement,
            tda.denomination as localisation,
            tdal.denomination as typeLocalisation,
            tdap.denomination as commune,
            grp.contact1Telephone as telephone,
            tda2.denomination as coop_localisation,
            tdal2.denomination as typeLocalisationCoop
            FROM AppBundle:TblOrganisations grp
                 INNER JOIN AppBundle:TblOrganisationLevel tol WITH grp.level=tol.id
                 INNER JOIN AppBundle:TblOrganisations coop WITH grp.parent=coop.id
                 INNER JOIN AppBundle:TblDecoupageAdmin tda WITH grp.decoupageAdmin=tda.id
                 INNER JOIN AppBundle:TblDecoupageAdmin tda2 WITH coop.decoupageAdmin=tda2.id
                 INNER JOIN AppBundle:TblDecoupageAdminLevel tdal WITH tda.level=tdal.id
                 INNER JOIN AppBundle:TblDecoupageAdminLevel tdal2 WITH tda2.level=tdal2.id
                 INNER JOIN AppBundle:TblDecoupageAdmin tdap WITH tda.parent= tdap.id
                 INNER JOIN AppBundle:TblDecoupageAdmin tdap2 WITH tda2.parent= tdap2.id
             WHERE tol.level=2
             ORDER BY cooperative ASC';
        $fields = [];
        $titre = 'Résultat de la liste de groupements';
        $fields['cooperative'] = $this->get('translator')->trans('cooperative');
        $fields['id'] = $this->get('translator')->trans('id');
        $fields['groupement'] = $this->get('translator')->trans('groupement');
        $fields['localisation'] = $this->get('translator')->trans('localisation');
        $fields['typeLocalisation'] = $this->get('translator')->trans('typeLocalisation');
        $fields['commune'] = $this->get('translator')->trans('commune');
        $fields['telephone'] = $this->get('translator')->trans('telephone');
        $fields['coop_localisation'] = $this->get('translator')->trans('coop_localisation');
        $fields['typeLocalisationCoop'] = $this->get('translator')->trans('typeLocalisationCoop');
        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery();
        $query->setDql($sql);
        $recordset = $query->getResult();

        return [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $recordset,
        ];
    }

    private function _groupesparCooperative($form)
    {
        $coop_id = $form->get('coop')->getData()->getId();

        $em = $this->getDoctrine()->getManager();
        $coop = $form->get('coop')->getData();

        return $em->getRepository(TblOrganisations::class)->getAllGroupements($coop->getId());
    }
}
