<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Animaux;
use App\Entity\AnimauxStructure;
use App\Entity\Exploitations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * AnimauxStructure controller.
 *
 * @Route("/{_locale}/references_animauxstructure")
 */

use Symfony\Component\Routing\Annotation\Route;

final class AnimauxStructureController extends AbstractController
{
    /**
     * Deletes a AnimauxStructure entity.
     *
     * @Route("/{id}", name="references_animauxstructure_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, AnimauxStructure $animauxStructure)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];
        $animaux_id = $animauxStructure->getAnimaux()->getId();
        $exploitation = $animauxStructure->getAnimaux()->getExploitation();

        $form = $this->createDeleteForm($animauxStructure, [
            //  'coop_id'=>$coop_id,
            //                                  'group_id'=>$group_id,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $animauxStructure->setRadie(TRUE);
            $em->persist($animauxStructure);
            $em->flush();
        }

        return $this->redirectToRoute('references_animaux_show', [
            'id' => $animaux_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing AnimauxStructure entity.
     *
     * @Route("/{id}/edit", name="references_animauxstructure_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, AnimauxStructure $animauxStructure)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];
        $animaux_id = $animauxStructure->getAnimaux()->getId();
        $exploitation = $animauxStructure->getAnimaux()->getExploitation();
        $deleteForm = $this->createDeleteForm($animauxStructure);
        $editForm = $this->createForm('App\Form\AnimauxStructureType', $animauxStructure);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($animauxStructure);
            $em->flush();

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux_id,
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('animauxstructure/edit.html.twig', [
            'id' => $animaux_id,
            'animauxStructure' => $animauxStructure,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
            //          'group_id' => $group_id,
            //          'coop_id' => $coop_id,
        ]);
    }

    /**
     * Lists all AnimauxStructure entities.
     *
     * @Route("/", name="references_animauxstructure_index", methods={"GET"})
     */
    public function indexAction()
    {
        // TODO : pas utilité -> à supprimer + template
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['id'];
        $annee = $_GET['annee'];

        $em = $this->getDoctrine()->getManager();

        $animauxStructures = $em->getRepository(AnimauxStructure::class)->findAll();

        return $this->render('animauxstructure/index.html.twig', [
            'animauxStructures' => $animauxStructures,
            'exploitation_id' => $exploitation_id,
            'annee' => $annee,
            //          'group_id' => $group_id,
            //          'coop_id' => $coop_id,
        ]);
    }

    /**
     * Creates a new AnimauxStructure entity.
     *
     * @Route("/new/{exploitation}/{annee}/{animaux}", name="references_animauxstructure_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(
        Request $request,
        Exploitations $exploitation,
        $annee,
        Animaux $animaux
    ) {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();

        $animauxStructure = new AnimauxStructure($user);
        $animauxStructure->setAnimaux($animaux);
        $animauxStructure->setAnnee($annee);
        $form = $this->createForm('App\Form\AnimauxStructureType', $animauxStructure);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($animauxStructure);
            $animauxStructure->setRadie(FALSE);
            $em->flush();

            return $this->redirectToRoute('references_animaux_show', [
                'id' => $animaux->getId(),
                'annee' => $annee,
                'exploitation' => $exploitation,
            ]);
        }

        return $this->render('animauxstructure/new.html.twig', [
            'id' => $animaux->getId(),
            'animauxStructure' => $animauxStructure,
            'form' => $form->createView(),
            'annee' => $annee,
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'animaux_id' => $animaux->getId(),
            'id' => $animaux->getId(),
        ]);
    }

    /**
     * Finds and displays a AnimauxStructure entity.
     *
     * @Route("/{id}", name="references_animauxstructure_show", methods={"GET"})
     */
    public function showAction(AnimauxStructure $animauxStructure)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['id'];
        $annee = $_GET['annee'];
        $exploitation = $animauxStructure->getAnimaux()->getExploitation();
        $deleteForm = $this->createDeleteForm($animauxStructure);

        return $this->render('animauxstructure/show.html.twig', [
            'id' => $animauxStructure->getAnimaux()->getId(),
            'animauxStructure' => $animauxStructure,
            'delete_form' => $deleteForm->createView(),
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
            //          'group_id' => $group_id,
            //          'coop_id' => $coop_id,
        ]);
    }

    /**
     * Creates a form to delete a AnimauxStructure entity.
     *
     * @param AnimauxStructure $animauxStructure The AnimauxStructure entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(AnimauxStructure $animauxStructure, $options = [])
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];
        $exploitation = $animauxStructure->getAnimaux()->getExploitation();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_animauxstructure_delete', [
                'id' => $animauxStructure->getId(),
                //                  'coop_id' => $coop_id,
                //                  'group_id' => $group_id,
                'exploitation_id' => $exploitation_id,
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
