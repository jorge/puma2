<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblFournisseurs;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblFournisseurs controller.
 *
 * @Route("/{_locale}/references/tblfournisseurs")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblFournisseursController extends AbstractController
{
    /**
     * Deletes a TblFournisseurs entity.
     *
     * @Route("/{id}", name="references_tblfournisseurs_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblFournisseurs $tblFournisseur)
    {
        $form = $this->createDeleteForm($tblFournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblFournisseur);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblfournisseurs_index');
    }

    /**
     * Displays a form to edit an existing TblFournisseurs entity.
     *
     * @Route("/{id}/edit", name="references_tblfournisseurs_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblFournisseurs $tblFournisseur)
    {
        $deleteForm = $this->createDeleteForm($tblFournisseur);
        $editForm = $this->createForm('App\Form\TblFournisseursType', $tblFournisseur);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblFournisseur);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblfournisseurs_index', ['id' => $tblFournisseur->getId()]);
        }

        return $this->render('tblfournisseurs/edit.html.twig', [
            'tblFournisseur' => $tblFournisseur,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblFournisseurs entities.
     *
     * @Route("/", name="references_tblfournisseurs_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblFournisseurs = $em->getRepository(TblFournisseurs::class)->findAll();

        return $this->render('tblfournisseurs/index.html.twig', [
            'tblFournisseurs' => $tblFournisseurs,
        ]);
    }

    /**
     * Creates a new TblFournisseurs entity.
     *
     * @Route("/new", name="references_tblfournisseurs_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblFournisseur = new TblFournisseurs($user);
        $form = $this->createForm('App\Form\TblFournisseursType', $tblFournisseur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblFournisseur);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblfournisseurs_index', ['id' => $tblFournisseur->getId()]);
        }

        return $this->render('tblfournisseurs/new.html.twig', [
            'tblFournisseur' => $tblFournisseur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblFournisseurs entity.
     *
     * @Route("/{id}", name="references_tblfournisseurs_show", methods={"GET"})
     */
    public function showAction(TblFournisseurs $tblFournisseur)
    {
        $deleteForm = $this->createDeleteForm($tblFournisseur);

        return $this->render('tblfournisseurs/show.html.twig', [
            'tblFournisseur' => $tblFournisseur,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblFournisseurs entity.
     *
     * @param TblFournisseurs $tblFournisseur The TblFournisseurs entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblFournisseurs $tblFournisseur)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblfournisseurs_delete', ['id' => $tblFournisseur->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
