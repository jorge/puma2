<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\ServicesBesoin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * ServicesBesoin controller.
 *
 * @Route("/{_locale}/references/servicesbesoin")
 */

use Symfony\Component\Routing\Annotation\Route;

final class ServicesBesoinController extends AbstractController
{
    /**
     * Deletes a ServicesBesoin entity.
     *
     * @Route("/{id}", name="references_servicesbesoin_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, ServicesBesoin $servicesBesoin)
    {
        $exploitation = $servicesBesoin->getExploitation();
        $annee = $servicesBesoin->getAnnee();
        $form = $this->createDeleteForm($servicesBesoin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $servicesBesoin->setRadie(TRUE);
            $em->persist($servicesBesoin);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'id' => $exploitation->getId(),
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing ServicesBesoin entity.
     *
     * @Route("/{id}/edit", name="references_servicesbesoin_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, ServicesBesoin $servicesBesoin)
    {
        $exploitation = $servicesBesoin->getExploitation();
        $annee = $servicesBesoin->getAnnee();
        $deleteForm = $this->createDeleteForm($servicesBesoin);
        $editForm = $this->createForm('App\Form\ServicesBesoinType', $servicesBesoin);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($servicesBesoin);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('servicesbesoin/edit.html.twig', [
            'edit_form' => $editForm->createView(),
            'annee' => $annee,
            'exploitation' => $exploitation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new ServicesBesoin entity.
     *
     * @Route("/{exploitation}/{annee}/new", name="references_servicesbesoin_new")
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Exploitations $exploitation, $annee)
    {
        $user = $this->getUser();
        $servicesBesoin = new ServicesBesoin($user);
        $servicesBesoin->setExploitation($exploitation);
        $servicesBesoin->setAnnee($annee);

        $form = $this->createForm('App\Form\ServicesBesoinType', $servicesBesoin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $servicesBesoin->setRadie(FALSE);
            $em->persist($servicesBesoin);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('servicesbesoin/new.html.twig', [
            'annee' => $annee,
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a ServicesBesoin entity.
     *
     * @Route("/{id}", name="references_servicesbesoin_show", methods={"GET"})
     */
    public function showAction(ServicesBesoin $servicesBesoin)
    {
        $exploitation = $servicesBesoin->getExploitation();
        $annee = $servicesBesoin->getAnnee();
        $deleteForm = $this->createDeleteForm($servicesBesoin);

        return $this->render('servicesbesoin/show.html.twig', [
            'servicesBesoin' => $servicesBesoin,
            'annee' => $annee,
            'exploitation' => $exploitation,
        ]);
    }

    /**
     * Creates a form to delete a ServicesBesoin entity.
     *
     * @param ServicesBesoin $servicesBesoin The ServicesBesoin entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ServicesBesoin $servicesBesoin, $options = [])
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_servicesbesoin_delete', [
                'id' => $servicesBesoin->getId(),
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
