<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\FormationsBesoin;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * FormationsBesoin controller.
 *
 * @Route("/{_locale}/references/formationsbesoin")
 */

use Symfony\Component\Routing\Annotation\Route;

final class FormationsBesoinController extends AbstractController
{
    /**
     * Deletes a FormationsBesoin entity.
     *
     * @Route("/{id}", name="references_formationsbesoin_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, FormationsBesoin $formationsBesoin)
    {
        $exploitation = $formationsBesoin->getExploitation();
        $annee = $formationsBesoin->getAnnee();
        $form = $this->createDeleteForm($formationsBesoin, [
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $formationsBesoin->setRadie(TRUE);
            $em->persist($formationsBesoin);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'id' => $exploitation->getId(),
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing FormationsBesoin entity.
     *
     * @Route("/{id}/edit", name="references_formationsbesoin_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, FormationsBesoin $formationsBesoin)
    {
        $exploitation = $formationsBesoin->getExploitation();
        $annee = $formationsBesoin->getAnnee();

        $deleteForm = $this->createDeleteForm($formationsBesoin);
        $editForm = $this->createForm('App\Form\FormationsBesoinType', $formationsBesoin);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($formationsBesoin);
            $em->flush();

            return $this->redirectToRoute('references_formationsbesoin_edit', [
                'id' => $formationsBesoin,
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('formationsbesoin/edit.html.twig', [
            'formationsBesoin' => $formationsBesoin,
            'exploitation' => $exploitation,
            'exploitation_id' => $exploitation->getId(),
            'annee' => $annee,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all FormationsBesoin entities par exploitation.
     *
     * @Route("/exploitation/{id}", name="references_formationsbesoin_exploitation_list", methods={"GET"})
     * TODO a supprimer ?
     */
    public function exploitationListAction(Request $request, Exploitations $exploitation)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $em = $this->getDoctrine()->getManager();
        $formationsBesoin = $em->getRepository(FormationsBesoin::class)->findBy([
            'exploitation' => $exploitation,
            'radie' => 0,
        ]);

        return $this->render('formationsbesoin/exploitationList.html.twig', [
            'formationsBesoin' => $formationsBesoin,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
            //          'group_id' => $group_id,
            //          'coop_id'=>$coop_id
        ]);
    }

    /**
     * Creates a new FormationsBesoin entity.
     *
     * @Route("/new/{exploitation}/{annee}/", name="references_formationsbesoin_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Exploitations $exploitation, $annee)
    {
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $formationsBesoin = new FormationsBesoin($user);
        $formationsBesoin->setExploitation($exploitation);
        $formationsBesoin->setAnnee($annee);

        $form = $this->createForm('App\Form\FormationsBesoinType', $formationsBesoin);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $formationsBesoin->setRadie(FALSE);
            $em->persist($formationsBesoin);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);
        }

        return $this->render('formationsbesoin/new.html.twig', [
            'formationsBesoin' => $formationsBesoin,
            'exploitation' => $exploitation,
            'annee' => $annee,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a FormationsBesoin entity.
     *
     * @Route("/{id}", name="references_formationsbesoin_show", methods={"GET"})
     */
    public function showAction(FormationsBesoin $formationsBesoin)
    {
        $exploitation = $formationsBesoin->getExploitation();
        $annee = $formationsBesoin->getAnnee();

        $deleteForm = $this->createDeleteForm($formationsBesoin);

        return $this->render('formationsbesoin/show.html.twig', [
            'formationsBesoin' => $formationsBesoin,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
    }

    /**
     * Creates a form to delete a FormationsBesoin entity.
     *
     * @param FormationsBesoin $formationsBesoin The FormationsBesoin entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(FormationsBesoin $formationsBesoin, $options = [])
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_formationsbesoin_delete', [
                'id' => $formationsBesoin->getId(),
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
