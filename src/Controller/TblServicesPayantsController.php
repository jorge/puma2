<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblServicesPayants;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblServicesPayants controller.
 *
 * @Route("/{_locale}/references/tblservicespayants")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblServicesPayantsController extends AbstractController
{
    /**
     * Deletes a TblServicesPayants entity.
     *
     * @Route("/{id}", name="references_tblservicespayants_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblServicesPayants $tblServicesPayant)
    {
        $form = $this->createDeleteForm($tblServicesPayant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblServicesPayant);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblservicespayants_index');
    }

    /**
     * Displays a form to edit an existing TblServicesPayants entity.
     *
     * @Route("/{id}/edit", name="references_tblservicespayants_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblServicesPayants $tblServicesPayant)
    {
        $deleteForm = $this->createDeleteForm($tblServicesPayant);
        $editForm = $this->createForm('App\Form\TblServicesPayantsType', $tblServicesPayant);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblServicesPayant);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblservicespayants_index', ['id' => $tblServicesPayant->getId()]);
        }

        return $this->render('tblservicespayants/edit.html.twig', [
            'tblServicesPayant' => $tblServicesPayant,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblServicesPayants entities.
     *
     * @Route("/", name="references_tblservicespayants_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblServicesPayants = $em->getRepository(TblServicesPayants::class)->findAll();

        return $this->render('tblservicespayants/index.html.twig', [
            'tblServicesPayants' => $tblServicesPayants,
        ]);
    }

    /**
     * Creates a new TblServicesPayants entity.
     *
     * @Route("/new", name="references_tblservicespayants_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblServicesPayant = new TblServicesPayants($user);
        $form = $this->createForm('App\Form\TblServicesPayantsType', $tblServicesPayant);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblServicesPayant);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblservicespayants_index', ['id' => $tblServicesPayant->getId()]);
        }

        return $this->render('tblservicespayants/new.html.twig', [
            'tblServicesPayant' => $tblServicesPayant,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblServicesPayants entity.
     *
     * @Route("/{id}", name="references_tblservicespayants_show", methods={"GET"})
     */
    public function showAction(TblServicesPayants $tblServicesPayant)
    {
        $deleteForm = $this->createDeleteForm($tblServicesPayant);

        return $this->render('tblservicespayants/show.html.twig', [
            'tblServicesPayant' => $tblServicesPayant,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblServicesPayants entity.
     *
     * @param TblServicesPayants $tblServicesPayant The TblServicesPayants entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblServicesPayants $tblServicesPayant)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblservicespayants_delete', ['id' => $tblServicesPayant->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
