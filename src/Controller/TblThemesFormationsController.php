<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblThemesFormations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblThemesFormations controller.
 *
 * @Route("/{_locale}/references/tblthemesformations")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblThemesFormationsController extends AbstractController
{
    /**
     * Deletes a TblThemesFormations entity.
     *
     * @Route("/{id}", name="references_tblthemesformations_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblThemesFormations $tblThemesFormation)
    {
        $form = $this->createDeleteForm($tblThemesFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblThemesFormation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblthemesformations_index');
    }

    /**
     * Displays a form to edit an existing TblThemesFormations entity.
     *
     * @Route("/{id}/edit", name="references_tblthemesformations_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblThemesFormations $tblThemesFormation)
    {
        $deleteForm = $this->createDeleteForm($tblThemesFormation);
        $editForm = $this->createForm('App\Form\TblThemesFormationsType', $tblThemesFormation);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblThemesFormation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblthemesformations_index', ['id' => $tblThemesFormation->getId()]);
        }

        return $this->render('tblthemesformations/edit.html.twig', [
            'tblThemesFormation' => $tblThemesFormation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblThemesFormations entities.
     *
     * @Route("/", name="references_tblthemesformations_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblThemesFormations = $em->getRepository(TblThemesFormations::class)->findAll();

        return $this->render('tblthemesformations/index.html.twig', [
            'tblThemesFormations' => $tblThemesFormations,
        ]);
    }

    /**
     * Creates a new TblThemesFormations entity.
     *
     * @Route("/new", name="references_tblthemesformations_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblThemesFormation = new TblThemesFormations($user);
        $form = $this->createForm('App\Form\TblThemesFormationsType', $tblThemesFormation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblThemesFormation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblthemesformations_index', ['id' => $tblThemesFormation->getId()]);
        }

        return $this->render('tblthemesformations/new.html.twig', [
            'tblThemesFormation' => $tblThemesFormation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblThemesFormations entity.
     *
     * @param TblThemesFormations $tblThemesFormation The TblThemesFormations entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblThemesFormations $tblThemesFormation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblthemesformations_delete', ['id' => $tblThemesFormation->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
