<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\PersonneDetails;
use App\Entity\Personnes;
use App\Entity\PivotPersonnesOrganisations;
use App\Entity\TblOrganisations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 * Personnes controller.
 *
 * @Route("/{_locale}/references/personnes")
 */

use Symfony\Component\Routing\Annotation\Route;

final class PersonnesController extends AbstractController
{
    /**
     * Displays a form to edit an existing Personnes entity.
     *
     * @Route("/{id}/contactedit", name="references_personnes_contactedit", methods={"GET", "POST"})
     */
    public function contacteditAction(Request $request, Personnes $personne)
    {
        $contact = $request->query->get('contact');
        $deleteForm = $this->createDeleteContactForm($personne);
        $editForm = $this->createForm('App\Form\PersonnesOrgType', $personne);
        $editForm->handleRequest($request);
        $personneDetails = null;

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $personne->setNom(mb_strtoupper($personne->getNom()));
            $personne->setPrenom(mb_strtoupper($personne->getPrenom()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($personne);
            $em->flush();
            $pivot_array = $em->getRepository(PivotPersonnesOrganisations::class)->findBy(['personne' => $personne]);
            $pivot = $pivot_array[0];
            $organ = $pivot->getOrganisation();

            return $this->redirectToRoute('references_tblorganisations_show', ['id' => $organ->getId(), 'contact' => $contact]);
        }

        return $this->render('personnes/editorg.html.twig', [
            'personne' => $personne,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Deletes a Personnes entity.
     *
     * @Route("/{id}", name="references_personnes_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Personnes $personne)
    {
        $form = $this->createDeleteForm($personne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $exploitation = $personne->getExploitation();

            $em = $this->getDoctrine()->getManager();
            $personne->setRadie(TRUE);
            $em->persist($personne);
            $em->flush();

            return $this->redirectToRoute(
                'references_exploitations_show_sans_annee',
                [
                    'id' => $exploitation->getId(),
                ]
            );
        }

        return $this->redirectToRoute(
            'references_personnes_edit',
            [
                'id' => $personne->getId(),
            ]
        );
    }

    /**
     * Deletes a Personnes entity.
     *
     * @Route("/{id}", name="references_personnes_deletecontact", methods={"DELETE"})
     */
    public function deletecontactAction(Request $request, Personnes $personne)
    {
        $exploitation = $personne->getExploitation();

        $form = $this->createDeleteContactForm($personne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $personne->setRadie(TRUE);
            $em->persist($personne);
            $em->flush();
        }

        return $this->redirectToRoute(
            'references_exploitations_show_sans_annee',
            [
                'id' => $exploitation->getId(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing Personnes entity.
     *
     * @Route("/{id}/edit/", name="references_personnes_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Personnes $personne)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $personne->getExploitation();
        $msg = '';
        $personneDetails = $em->getRepository(PersonneDetails::class)->findBy(['personnes' => $personne]);
        $deleteForm = $this->createDeleteForm($personne);

        $editForm = $this->createForm('App\Form\PersonnesType', $personne);
        $editForm->handleRequest($request);
        $contact = null;

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $personne->storeUploadedPhoto($this->getAbsolutePathForPhotoDir());
            $personne->setNom(mb_strtoupper($personne->getNom()));
            $personne->setPrenom(mb_strtoupper($personne->getPrenom()));

            $em = $this->getDoctrine()->getManager();
            $em->persist($personne);
            $em->flush();

            // $this->addFlash('success', 'personne est bien enregistré');

            if ($request->query->has('returnPath')) {
                return $this->redirect($request->query->get('returnPath'));
            }

            return $this->redirectToRoute('references_personnes_edit', [
                'id' => $personne->getId(),
                'msg' => $msg,
            ]);
        }

        return $this->render('personnes/edit.html.twig', [
            'personne' => $personne,
            'personneDetails' => $personneDetails,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Page to edit the picture/photo of an existing Personnes.
     *
     * @Route("/{id}/edit/photo/", name="references_personnes_edit_photo", methods={"GET", "POST"})
     */
    public function editPhotoAction(Request $request, Personnes $personne)
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $personne->getExploitation();

        return $this->render('personnes/edit_photo.html.twig', [
            'personne' => $personne,
            'personnes_photo_webdir' => ($this->getParameter('personnes_photo_webdir')),
        ]);
    }

    public function getAbsolutePathForPhotoDir()
    {
        // passer $this
        $pjtDir = $this->getParameter('kernel.project_dir');
        $personnePhotoWebdir = $this->getParameter('personnes_photo_webdir');

        return realpath($pjtDir . '/public' . $personnePhotoWebdir);
    }

    /**
     * Creates a new Personnes entity.
     *
     * @Route("{exploitation}/new/", name="references_personnes_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, Exploitations $exploitation)
    {
        $user = $this->getUser();
        $personne = new Personnes($user);
        $personne->setExploitation($exploitation);

        $form = $this->createForm('App\Form\PersonnesType', $personne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $personne->setNom(mb_strtoupper($personne->getNom()));
            $personne->setPrenom(mb_strtoupper($personne->getPrenom()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($personne);
            $em->flush();

            return $this->redirectToRoute(
                'references_exploitations_show_sans_annee',
                [
                    'id' => $exploitation->getId(),
                ]
            );
        }

        return $this->render('personnes/new.html.twig', [
            'personne' => $personne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a new Personnes entity.
     *
     * @Route("/contact/{organisation}/new/", name="references_contact_new", methods={"GET", "POST"})
     */
    public function newContactAction(Request $request, TblOrganisations $organisation)
    {
        $user = $this->getUser();
        $personne = new Personnes($user);
        $contact = $request->query->get('contact');
        $personne->setContact($contact);
        //$personne->setExploitation($exploitation);
        $pivotpersonnesorganisation = new PivotPersonnesOrganisations($user);
        //$pivotpersonnesorganisation->setPersonne($personne);
        $form = $this->createForm('App\Form\PersonnesOrgType', $personne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $personne->setNom(mb_strtoupper($personne->getNom()));
            $personne->setPrenom(mb_strtoupper($personne->getPrenom()));
            $personne->setMembreMenage(0);
            $em = $this->getDoctrine()->getManager();
            $em->persist($personne);
            $em->flush();
            $pivotpersonnesorganisation->setRadie(FALSE);
            $pivotpersonnesorganisation->setOrganisation($organisation);
            $pivotpersonnesorganisation->setPersonne($personne);
            $em->persist($pivotpersonnesorganisation);
            $em->flush();

            return $this->redirectToRoute(
                'references_tblorganisations_show',
                [
                    'id' => $organisation->getId(),
                ]
            );
        }

        return $this->render('personnes/newcontact.html.twig', [
            'personne' => $personne,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a Personnes entity.
     *
     * @Route("/contact/{id}", name="references_personnes_showcontact", methods={"GET"})
     */
    public function showcontactAction(Personnes $personne)
    {
        $deleteForm = $this->createDeleteContactForm($personne);

        return $this->render('personnes/showcontact.html.twig', [
            'personne' => $personne,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Page to edit the picture/photo of an existing Personnes.
     *
     * @Route("/{id}/edit/photo/upload", name="references_personnes_edit_photo_upload")
     * @Method({"POST"})
     */
    public function uploadPhotoAction(Request $request, Personnes $personne)
    {
        if ($request->getMethod() === 'POST') {
            $newPhoto = $request->files->get('photo');
            $personne->setPhotoUploadedFile($newPhoto);

            if ($personne->storeUploadedPhoto($this->getAbsolutePathForPhotoDir()) === false) {
                return new JsonResponse(['error' => true, 'error_message' => 'Fichier Non Valide']);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($personne);
            $em->flush();

            $rep = new JsonResponse(['error' => false]);
        } else {
            $rep = new JsonResponse(['error' => true, 'error_message' => 'NotSubmitted or NotValid']);
            $rep->setStatusCode(JsonResponse::HTTP_INTERNAL_SERVER_ERROR);
        }

        return $rep;
    }

    /**
     * Creates a form to delete a Personnes entity.
     *
     * @param Personnes $personne The Personnes entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteContactForm(Personnes $personne, $options = [])
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'references_personnes_deletecontact',
                ['id' => $personne->getId()]
            ))
            ->setMethod('DELETE')
            ->getForm();
    }

    /**
     * Creates a form to delete a Personnes entity.
     *
     * @param Personnes $personne The Personnes entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Personnes $personne, $options = [])
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl(
                'references_personnes_delete',
                ['id' => $personne->getId(),
                ]
            ))
            ->setMethod('DELETE')
            ->getForm();
    }
}
