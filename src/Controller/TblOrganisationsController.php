<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblOrganisations;
use App\Entity\TblOrganisationLevel;
use App\Entity\SuiviOrganisations;
use App\Repository\TblOrganisationLevelRepository;
use App\Repository\TblOrganisationsRepository;
use App\Repository\PersonnesRepository;
use App\Repository\ExploitationsRepository;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblOrganisations controller.
 *
 * @Route("/{_locale}/references/tblorganisations")
 */

use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Security;
use Symfony\Contracts\Translation\TranslatorInterface;

final class TblOrganisationsController extends AbstractController
{
    public const LVL_COOP = 1;

    public const LVL_GPM = 2;

    /**
     * Creates a new TblOrganisations entity.
     *
     * @Route("/coopnew", name="references_tblorganisations_coopnew", methods={"GET", "POST"})
     */
    public function coopnewAction(Request $request)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à ce page');
        $user = $this->getUser();
        $tblOrganisation = new TblOrganisations($user);
        $form = $this->createForm('App\Form\TblOrganisationsType', $tblOrganisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tblOrganisation->setDenomination(mb_strtoupper($tblOrganisation->getDenomination()));
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOrganisation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblorganisations_show', ['id' => $tblOrganisation->getId()]);
        }

        return $this->render('tblorganisations/new.html.twig', [
            'tblOrganisation' => $tblOrganisation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Deletes a TblOrganisations entity.
     *
     * @Route("/{id}", name="references_tblorganisations_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblOrganisations $tblOrganisation, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, 'Vous n\'avez pas le droit d\'accès à ce page');
        $form = $this->createDeleteForm($tblOrganisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tblOrganisation->setRadie(TRUE);
            $em->persist($tblOrganisation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
            $level = $em->getRepository(TblOrganisationLevel::class)->find($tblOrganisation->getLevel()->getId());
            /* radier branche organisation */
            $this->RadierBrancheOrganisation($tblOrganisation->getId(), 1, $em, $level->getDenomination());
        }

        return $this->redirectToRoute('references_tblorganisations_index');
    }

    /**
     * Displays a form to edit an existing TblOrganisations entity.
     *
     * @Route("/{id}/edit", name="references_tblorganisations_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblOrganisations $tblOrganisation, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, "Vous n'avez pas le droit d'acceder à cette page");

        $deleteForm = $this->createDeleteForm($tblOrganisation);
        $editForm = $this->createForm('App\Form\TblOrganisationsType', $tblOrganisation);
        $editForm->handleRequest($request);

        $org_id = $tblOrganisation->getId();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $tblOrganisation->setDenomination(mb_strtoupper($tblOrganisation->getDenomination()));
            $em->persist($tblOrganisation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblorganisations_show', ['id' => $tblOrganisation->getId()]);
        }

        return $this->render('tblorganisations/edit.html.twig', [
            'tblOrganisation' => $tblOrganisation,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'org_id' => $org_id,
        ]);
    }

    /**
     * Choose a coop then execute an action.
     *
     * Next action must have a parameter {parent} for giving back the id
     * of the parent selected.
     *
     * @param $next the action where to redirect after the selection. Must
     * have a parameter {parent} for giving back the id of the parent selected
     *
     * @Route("/select/coop/then/{next}/", name="choose_a_coop_then")
     */
    public function genericSelectCoopThen(Request $request, string $next, TranslatorInterface $translator, EntityManagerInterface $em)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, "Vous n'avez pas le droit d'acceder à cette page");

        $form = $this->createForm(
            'App\Form\Saisies\GetCooperativeType',
            null,
            [
                'em_manager' => $em, // Use Dependency Injection in GetCooperativeType::class
            ]
        );

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $data = $form->getData();

            return $this->redirectToRoute(
                $next,
                [
                    'parent' => $data['cooperative']->getId(), // todo fixme uniformier
                    'coop' => $data['cooperative']->getId(),
                ]
            );
        }

        return $this->render('saisies/getcooperative.html.twig', [
            'form' => $form->createView(),
            'title' => $translator->trans("Choix d'une coopérative"),
        ]);
    }



    /**
     * Lists all TblOrganisations entities.
     *
     * @Route("/", name="references_tblorganisations_index", methods={"GET"})
     * @Route("/level/{level}/", name="references_tblorganisations_level", methods={"GET"})
     */
    public function index(?int $level, TblOrganisationLevelRepository $tblOrganisationLevelRepository, TblOrganisationsRepository $tblOrganisationsRepository)
    {
        $level = $tblOrganisationLevelRepository
            ->findOneByLevel(null === $level ? self::LVL_COOP : $level);

        $organisations = $tblOrganisationsRepository
            ->findBy(
                [
                    'level' => $level,
                    'radie' => 0,
                ]
            );

        return $this->render(
            'tblorganisations/index.html.twig',
            [
                'organisations' => $organisations,
                'level' => $level,
            ]
        );
    }

    /**
     * Creates a new TblOrganisations entity (cooperative (orga of level 1)  or
     * groupement (orga of level2).
     *
     * For a groupement the parent is not null
     *
     * @Route("/new", name="references_tblorganisations_new")
     * @Route("/{parent}/suborga/new", name="references_tblorganisations_new_with_parent", methods={"GET", "POST"})
     */
    public function newAction(Request $request, Security $security, TblOrganisationsRepository $tblOrganisationsRepository, TblOrganisationLevelRepository $tblOrganisationLevelRepository, EntityManagerInterface $em, ?TblOrganisations $parent = null)
    {
        $this->denyAccessUnlessGranted('ROLE_ADMIN', null, "Vous n'avez pas le droit d'acceder à cette page");

        $user = $security->getUser();

        $maxCode = $em->createQueryBuilder()
            ->select('MAX(o.code)')
            ->from(TblOrganisations::class, 'o')
            ->getQuery()
            ->getSingleScalarResult();

        $maxCode = (int) $maxCode + 1;

        if (null === $parent) { // we will create a groupementlvl2)
            $intLevel = self::LVL_COOP;
            $parent = $tblOrganisationsRepository->findByLevel(1)[0];
        } else {
            $intLevel = self::LVL_GPM;
        }

        $level = $tblOrganisationLevelRepository->findOneByLevel($intLevel);
        $tblOrganisation = new TblOrganisations($user);
        $tblOrganisation->setParent($parent);
        $tblOrganisation->setLevel($level);
        $tblOrganisation->setCode($maxCode);
        $form = $this->createForm('App\Form\TblOrganisationsType', $tblOrganisation);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $tblOrganisation->setDenomination(mb_strtoupper($tblOrganisation->getDenomination()));
            $tblOrganisation->setParent($parent);
            $tblOrganisation->setLevel($level);
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOrganisation);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblorganisations_show', ['id' => $tblOrganisation->getId()]);
        }

        return $this->render('tblorganisations/new.html.twig', [
            'tblOrganisation' => $tblOrganisation,
            'form' => $form->createView(),
        ]);
    }

    public function radierBrancheAnimaux($organisation_id, $status, EntityManagerInterface $em)
    {
        $select = "SELECT a.id FROM pivot_exploitation_organisations p
                   INNER JOIN exploitations e ON p.exploitation_id = e.id INNER JOIN animaux a ON a.exploitation_id = e.id
                   WHERE p.organisation_id ='" . $organisation_id . "'";
        //    $strSql = "UPDATE animaux_alimentation SET radie=" . $status . "
        //  WHERE animaux_id in (" .$select . ");";
        //    $stmt = $em->getConnection()->prepare($strSql);
        //    $stmt->execute();

        $strSql = 'UPDATE animaux_produits SET radie=' . $status . '
      WHERE animaux_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE animaux_structure SET radie=' . $status . '
      WHERE animaux_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE animaux_production SET radie=' . $status . '
      WHERE animaux_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE animaux_labour SET radie=' . $status . '
      WHERE animaux_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
    }

    public function radierBranchedeSuivis($organisation_id, $status, EntityManagerInterface $em)
    {
        $select = "SELECT s.id FROM tbl_organisations o INNER JOIN suivi_organisations s ON s.organisation_id = o.id
                   WHERE o.id = '" . $organisation_id . "'";

        $strSql = 'UPDATE services_aux_membres SET radie=' . $status . '
                  WHERE suivi_organisation_id in (' . $select . ');';

        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE services_payants_aux_membres SET radie=' . $status . '
                   WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE prestation_de_services SET radie=' . $status . '
                   WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE niveau_de_reconnaissance SET radie=' . $status . '
                   WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE infrastructures SET radie=' . $status . '
                   WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE materiel_roulant SET radie=' . $status . '
      WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE unite_de_transformation SET radie=' . $status . '
      WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE materiel_de_bureau SET radie=' . $status . '
      WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE equipement_agricole SET radie=' . $status . '
      WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE besoins_formations SET radie=' . $status . '
      WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE besoins_institutionel SET radie=' . $status . '
      WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE besoin_service_aux_membres SET radie=' . $status . '
      WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE mobilisations_communautaires SET radie=' . $status . '
      WHERE suivi_organisation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
    }

    public function RadierBrancheOrganisation($organisation_id, $status, EntityManagerInterface $em, $org_type)
    {
        if ('groupement' !== $org_type) {
            return;
        }

        /* radier les pivot_personnes_organsations */
        $pivot = $em
            ->getRepository(PivotPersonnesOrganisations::class)
            ->updateRadieStatus($organisation_id, $status);
        /* radier les contacts */
        $select = "SELECT personne_id FROM pivot_personnes_organisations WHERE organisation_id='" . $organisation_id . "'";
        $strSql = 'UPDATE personnes SET radie=' . $status . ' WHERE id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* radier les suivis */
        $pivot = $em->getRepository(SuiviOrganisations::class)
            ->updateRadieStatus($organisation_id, $status);

        /* ***** effacer la branche suivi ***** */
        $this->radierBranchedeSuivis($organisation_id, $status, $em);

        /* ***** radier les pivot_exploitation_organisations ***** */
        $Animal = $em->getRepository(PivotExploitationOrganisations::class)
            ->updateRadieStatus($organisation_id, $status);

        /* radier les exploitations */
        $select = "SELECT p.exploitation_id FROM pivot_exploitation_organisations p WHERE p.organisation_id ='" . $organisation_id . "'";
        $strSql = 'UPDATE exploitations SET radie=' . $status . '
                    WHERE id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* radier la branche exploitations */
        $strSql = 'UPDATE animaux SET radie=' . $status . ' WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* ***** voir branche animaux ***** */
        $this->radierBrancheAnimaux($organisation_id, $status, $em);

        $strSql = 'UPDATE batiments SET radie=' . $status . ' WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE commercialisation_produits SET radie=' . $status . ' WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE contraintes_developpement SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE depenses_menage SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE dettes SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE engrais_demande SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE exploitation_charges SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE filieres SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE formations_besoin SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE formations_recu SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE habitat SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE mo_familiale_externe SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE mo_familiale_interne SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE moyens_transport SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE outillage SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE parcelles SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* voir branche parcelles */
        $this->radierBrancheParcelle($organisation_id, $status, $em);

        $strSql = 'UPDATE personnes SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* voir branche personnes */
        $this->radierBranchePersonnes($organisation_id, $status, $em);

        $strSql = 'UPDATE phyto_demande SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE securite_alimentaire SET radie=' . $status . '
                    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE semence_demandee SET radie=' . $status . '
                    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        $strSql = 'UPDATE services_besoin SET radie=' . $status . '
    WHERE exploitation_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
    }

    /* maj radie pour les tables dependent de parcelles */
    public function radierBrancheParcelle($organisation_id, $status, EntityManagerInterface $em)
    {
        $select = "SELECT piv.exploitation_id
                    FROM pivot_exploitation_organisations piv
                    WHERE piv.organisation_id='" . $organisation_id . "'";

        /* parcelle_historiques */
        $strSql = 'UPDATE parcelle_historiques SET radie=' . $status . '
      WHERE parcelle_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* ******* sub-branche parcelle_historiques ******** */
        /* historique_cultures */
        $select = "SELECT ph.id FROM pivot_exploitation_organisations p
                      INNER JOIN exploitations e ON p.exploitation_id = e.id
                      INNER JOIN parcelles par ON par.exploitation_id = e.id
                      INNER JOIN parcelle_historiques ph ON par.id=ph.parcelle_id
                      WHERE p.organisation_id ='" . $organisation_id . "'";

        $strSql = 'UPDATE historique_cultures SET radie=' . $status . '
      WHERE parcelle_historique_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        /* parcelle_amendements */
    //    $strSql = "UPDATE parcelle_amendements SET radie=" . $status . "
    //      WHERE parcelle_historique_id in (" .$select . ");";
    //    $stmt = $em->getConnection()->prepare($strSql);
    //    $stmt->execute();

        /* parcelle_fumure_min */
    /*    $strSql = "UPDATE parcelle_fumure_min SET radie=" . $status . "
          WHERE parcelle_historique_id in (" .$select . ");";
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
     */
        /* parcelle_fumure_org */
    /*    $strSql = "UPDATE parcelle_fumure_org SET radie=" . $status . "
          WHERE parcelle_historique_id in (" .$select . ");";
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
     */
        /* parcelle_herbicide */
     /*   $strSql = "UPDATE parcelle_herbicide SET radie=" . $status . "
          WHERE parcelle_historique_id in (" .$select . ");";
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
      */
        /* parcelle_phytosanitaire */
    /*    $strSql = "UPDATE parcelle_phytosanitaire SET radie=" . $status . "
          WHERE parcelle_historique_id in (" .$select . ");";
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
     */
        /* parcelle_recoltes */
    /*    $strSql = "UPDATE parcelle_recoltes SET radie=" . $status . "
          WHERE parcelle_historique_id in (" .$select . ");";
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
     */
        /* parcelle_semences */
    /*    $strSql = "UPDATE parcelle_semences SET radie=" . $status . "
          WHERE parcelle_historique_id in (" .$select . ");";
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
     */
        /* parcelle_techniques */
     /*   $strSql = "UPDATE parcelle_techniques SET radie=" . $status . "
          WHERE parcelle_historique_id in (" .$select . ");";
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
      */
    }

    /* maj radie pour les tables dependent de personnes */
    public function radierBranchePersonnes($organisation_id, $status, EntityManagerInterface $em)
    {
        $select = "SELECT pers.id FROM pivot_exploitation_organisations piv
                      INNER JOIN exploitations e ON piv.exploitation_id = e.id
                      INNER JOIN personnes pers ON pers.exploitation_id = e.id
                      WHERE piv.organisation_id ='" . $organisation_id . "'";
        /* personne_details */
        $strSql = 'UPDATE personne_details SET radie=' . $status . '
             WHERE personnes_id in (' . $select . ');';
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
    }

    /**
     * @Route("/set/lonlat/{id}/", name="references_tblorganisations_set_lonlat")
     */
    public function setLonLatAction(Request $request, TblOrganisations $orga)
    {
        $form = $this->createForm('App\Form\TblOrganisationsLonLatType', $orga);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($orga);
            $em->flush();

            return $this->redirectToRoute('references_tblorganisations_show', [
                'id' => $orga->getId(),
            ]);
        }

        return $this->render('tblorganisations/set_lonlat.html.twig', [
            'orga' => $orga,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblOrganisations entity.
     *
     * @Route("/{id}", name="references_tblorganisations_show", methods={"GET"})
     */
    public function showAction(TblOrganisations $tblOrganisation, PersonnesRepository $personnesRepository, ExploitationsRepository $exploitationsRepository)
    {
        $em = $this->getDoctrine()->getManager();
        $deleteForm = $this->createDeleteForm($tblOrganisation);

        $objet_id = $tblOrganisation->getLevel();
        $obj = $em->getRepository(TblOrganisationLevel::class)->find($objet_id);
        $objet = $obj->getDenomination(); // coop or groupement

        $suivis = $em->getRepository(SuiviOrganisations::class)->findBy([
            'organisation' => $tblOrganisation,
            'radie' => 0,
        ]);

        $org_id = $tblOrganisation->getId();
        $cont_id1 = $personnesRepository->getPersonContact(1, $org_id);
        $cont_id2 = $personnesRepository->getPersonContact(2, $org_id);

        if (empty($cont_id1)) {
            $contact1 = null;
        } else {
            $id1 = $cont_id1[0]['id'];
            $contact1 = $personnesRepository->find($id1);
        }

        if (empty($cont_id2)) {
            $contact2 = null;
        } else {
            $id2 = $cont_id2[0]['id'];
            $contact2 = $personnesRepository->find($id2);
        }

        $groupements = $em->getRepository(TblOrganisations::class)->findByParent($tblOrganisation->getId());

        if ('groupement' === $objet) {
            $exploitations = $exploitationsRepository->getAllExploitationsByOrgBis($org_id);

            return $this->render('tblorganisations/show_groupement.html.twig', [
                'delete_form' => $deleteForm->createView(),
                'objet' => $objet,
                'suivis' => $suivis,
                'contact1' => $contact1,
                'contact2' => $contact2,
                'exploitations' => $exploitations,
                'coop' => $tblOrganisation->getParent(),
                'groupement' => $tblOrganisation,
            ]);
        }

        return $this->render('tblorganisations/show_coop.html.twig', [
            'coop' => $tblOrganisation,
            'delete_form' => $deleteForm->createView(),
            'objet' => $objet,
            'suivis' => $suivis,
            'contact1' => $contact1,
            'contact2' => $contact2,
            'groupements' => $groupements,
        ]);
    }

    /**
     * Creates a form to delete a TblOrganisations entity.
     *
     * @param TblOrganisations $tblOrganisation The TblOrganisations entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblOrganisations $tblOrganisation)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblorganisations_delete', ['id' => $tblOrganisation->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
