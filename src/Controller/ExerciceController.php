<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exercice;
use App\Entity\Exploitations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Exercice controller.
 *
 * @Route("/{_locale}/exercice", name="exercice_")
 */

use Symfony\Component\Routing\Annotation\Route;

final class ExerciceController extends AbstractController
{
    /**
     * Creates a new Exercice entity.
     *
     * @Route("/{exploitation}/new", name="new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, Exploitations $exploitation)
    {
        $user = $this->getUser();
        $exercice = new Exercice($user);
        $exercice->setExploitation($exploitation);
        $form = $this->createForm('App\Form\ExerciceType', $exercice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($exercice);
            $em->flush();

            return $this->redirectToRoute('saisie_info_gen', [
                'annee' => $exercice->getAnnee(),
                'exploitation' => $exploitation->getId(),
            ]);
        }

        return $this->render('exercice/new.html.twig', [
            'exercice' => $exercice,
            'form' => $form->createView(),
        ]);
    }
}
