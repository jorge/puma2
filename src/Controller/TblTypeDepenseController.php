<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblTypeDepense;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblTypeDepense controller.
 *
 * @Route("/{_locale}/references/tbltypedepense")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblTypeDepenseController extends AbstractController
{
    /**
     * Deletes a TblTypeDepense entity.
     *
     * @Route("/{id}", name="references_tbltypedepense_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblTypeDepense $tblTypeDepense)
    {
        $form = $this->createDeleteForm($tblTypeDepense);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblTypeDepense);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbltypedepense_index');
    }

    /**
     * Displays a form to edit an existing TblTypeDepense entity.
     *
     * @Route("/{id}/edit", name="references_tbltypedepense_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblTypeDepense $tblTypeDepense)
    {
        $deleteForm = $this->createDeleteForm($tblTypeDepense);
        $editForm = $this->createForm('App\Form\TblTypeDepenseType', $tblTypeDepense);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeDepense);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypedepense_index', ['id' => $tblTypeDepense->getId()]);
        }

        return $this->render('tbltypedepense/edit.html.twig', [
            'tblTypeDepense' => $tblTypeDepense,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblTypeDepense entities.
     *
     * @Route("/", name="references_tbltypedepense_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblTypeDepenses = $em->getRepository(TblTypeDepense::class)->findAll();

        return $this->render('tbltypedepense/index.html.twig', [
            'tblTypeDepenses' => $tblTypeDepenses,
        ]);
    }

    /**
     * Creates a new TblTypeDepense entity.
     *
     * @Route("/new", name="references_tbltypedepense_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblTypeDepense = new TblTypeDepense($user);
        $form = $this->createForm('App\Form\TblTypeDepenseType', $tblTypeDepense);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeDepense);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypedepense_index');
        }

        return $this->render('tbltypedepense/new.html.twig', [
            'tblTypeDepense' => $tblTypeDepense,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblTypeDepense entity.
     *
     * @Route("/{id}", name="references_tbltypedepense_show", methods={"GET"})
     */
    public function showAction(TblTypeDepense $tblTypeDepense)
    {
        $deleteForm = $this->createDeleteForm($tblTypeDepense);

        return $this->render('tbltypedepense/show.html.twig', [
            'tblTypeDepense' => $tblTypeDepense,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblTypeDepense entity.
     *
     * @param TblTypeDepense $tblTypeDepense The TblTypeDepense entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblTypeDepense $tblTypeDepense)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbltypedepense_delete', ['id' => $tblTypeDepense->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
