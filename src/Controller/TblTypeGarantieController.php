<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblTypeGarantie;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblTypeGarantie controller.
 *
 * @Route("/{_locale}/references/tbltypegarantie")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblTypeGarantieController extends AbstractController
{
    /**
     * Deletes a TblTypeGarantie entity.
     *
     * @Route("/{id}", name="references_tbltypegarantie_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblTypeGarantie $tblTypeGarantie)
    {
        $form = $this->createDeleteForm($tblTypeGarantie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblTypeGarantie);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbltypegarantie_index');
    }

    /**
     * Displays a form to edit an existing TblTypeGarantie entity.
     *
     * @Route("/{id}/edit", name="references_tbltypegarantie_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblTypeGarantie $tblTypeGarantie)
    {
        $deleteForm = $this->createDeleteForm($tblTypeGarantie);
        $editForm = $this->createForm('App\Form\TblTypeGarantieType', $tblTypeGarantie);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeGarantie);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypegarantie_index', ['id' => $tblTypeGarantie->getId()]);
        }

        return $this->render('tbltypegarantie/edit.html.twig', [
            'tblTypeGarantie' => $tblTypeGarantie,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblTypeGarantie entities.
     *
     * @Route("/", name="references_tbltypegarantie_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblTypeGaranties = $em->getRepository(TblTypeGarantie::class)->findAll();

        return $this->render('tbltypegarantie/index.html.twig', [
            'tblTypeGaranties' => $tblTypeGaranties,
        ]);
    }

    /**
     * Creates a new TblTypeGarantie entity.
     *
     * @Route("/new", name="references_tbltypegarantie_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblTypeGarantie = new TblTypeGarantie($user);
        $form = $this->createForm('App\Form\TblTypeGarantieType', $tblTypeGarantie);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeGarantie);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypegarantie_index', ['id' => $tblTypeGarantie->getId()]);
        }

        return $this->render('tbltypegarantie/new.html.twig', [
            'tblTypeGarantie' => $tblTypeGarantie,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblTypeGarantie entity.
     *
     * @Route("/{id}", name="references_tbltypegarantie_show", methods={"GET"})
     */
    public function showAction(TblTypeGarantie $tblTypeGarantie)
    {
        $deleteForm = $this->createDeleteForm($tblTypeGarantie);

        return $this->render('tbltypegarantie/show.html.twig', [
            'tblTypeGarantie' => $tblTypeGarantie,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblTypeGarantie entity.
     *
     * @param TblTypeGarantie $tblTypeGarantie The TblTypeGarantie entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblTypeGarantie $tblTypeGarantie)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbltypegarantie_delete', ['id' => $tblTypeGarantie->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
