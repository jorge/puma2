<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblDecoupageAdminLevel;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblDecoupageAdminLevel controller.
 *
 * @Route("/{_locale}/references/tbldecoupageadminlevel")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblDecoupageAdminLevelController extends AbstractController
{
    /**
     * Deletes a TblDecoupageAdminLevel entity.
     *
     * @Route("/{id}", name="references_tbldecoupageadminlevel_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblDecoupageAdminLevel $tblDecoupageAdminLevel)
    {
        $form = $this->createDeleteForm($tblDecoupageAdminLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblDecoupageAdminLevel);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbldecoupageadminlevel_index');
    }

    /**
     * Displays a form to edit an existing TblDecoupageAdminLevel entity.
     *
     * @Route("/{id}/edit", name="references_tbldecoupageadminlevel_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblDecoupageAdminLevel $tblDecoupageAdminLevel)
    {
        $deleteForm = $this->createDeleteForm($tblDecoupageAdminLevel);
        $editForm = $this->createForm('App\Form\TblDecoupageAdminLevelType', $tblDecoupageAdminLevel);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblDecoupageAdminLevel);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbldecoupageadminlevel_index', ['id' => $tblDecoupageAdminLevel->getId()]);
        }

        return $this->render('tbldecoupageadminlevel/edit.html.twig', [
            'tblDecoupageAdminLevel' => $tblDecoupageAdminLevel,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblDecoupageAdminLevel entities.
     *
     * @Route("/", name="references_tbldecoupageadminlevel_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblDecoupageAdminLevels = $em->getRepository(TblDecoupageAdminLevel::class)->findAll();

        return $this->render('tbldecoupageadminlevel/index.html.twig', [
            'tblDecoupageAdminLevels' => $tblDecoupageAdminLevels,
        ]);
    }

    /**
     * Creates a new TblDecoupageAdminLevel entity.
     *
     * @Route("/new", name="references_tbldecoupageadminlevel_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblDecoupageAdminLevel = new TblDecoupageAdminLevel($user);
        $form = $this->createForm('App\Form\TblDecoupageAdminLevelType', $tblDecoupageAdminLevel);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblDecoupageAdminLevel);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbldecoupageadminlevel_index', ['id' => $tblDecoupageAdminLevel->getId()]);
        }

        return $this->render('tbldecoupageadminlevel/new.html.twig', [
            'tblDecoupageAdminLevel' => $tblDecoupageAdminLevel,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblDecoupageAdminLevel entity.
     *
     * @Route("/{id}", name="references_tbldecoupageadminlevel_show", methods={"GET"})
     */
    public function showAction(TblDecoupageAdminLevel $tblDecoupageAdminLevel)
    {
        $deleteForm = $this->createDeleteForm($tblDecoupageAdminLevel);

        return $this->render('tbldecoupageadminlevel/show.html.twig', [
            'tblDecoupageAdminLevel' => $tblDecoupageAdminLevel,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblDecoupageAdminLevel entity.
     *
     * @param TblDecoupageAdminLevel $tblDecoupageAdminLevel The TblDecoupageAdminLevel entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblDecoupageAdminLevel $tblDecoupageAdminLevel)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbldecoupageadminlevel_delete', ['id' => $tblDecoupageAdminLevel->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
