<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblEntretiens;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblEntretiens controller.
 *
 * @Route("/{_locale}/references/tblentretiens")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblEntretiensController extends AbstractController
{
    /**
     * Deletes a TblEntretiens entity.
     *
     * @Route("/{id}", name="references_tblentretiens_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblEntretiens $tblEntretien)
    {
        $form = $this->createDeleteForm($tblEntretien);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblEntretien);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblentretiens_index');
    }

    /**
     * Displays a form to edit an existing TblEntretiens entity.
     *
     * @Route("/{id}/edit", name="references_tblentretiens_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblEntretiens $tblEntretien)
    {
        $deleteForm = $this->createDeleteForm($tblEntretien);
        $editForm = $this->createForm('App\Form\TblEntretiensType', $tblEntretien);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblEntretien);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblentretiens_index', ['id' => $tblEntretien->getId()]);
        }

        return $this->render('tblentretiens/edit.html.twig', [
            'tblEntretien' => $tblEntretien,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblEntretiens entities.
     *
     * @Route("/", name="references_tblentretiens_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblEntretiens = $em->getRepository(TblEntretiens::class)->findAll();

        return $this->render('tblentretiens/index.html.twig', [
            'tblEntretiens' => $tblEntretiens,
        ]);
    }

    /**
     * Creates a new TblEntretiens entity.
     *
     * @Route("/new", name="references_tblentretiens_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblEntretien = new TblEntretiens($user);
        $form = $this->createForm('App\Form\TblEntretiensType', $tblEntretien);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblEntretien);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblentretiens_index', ['id' => $tblEntretien->getId()]);
        }

        return $this->render('tblentretiens/new.html.twig', [
            'tblEntretien' => $tblEntretien,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblEntretiens entity.
     *
     * @Route("/{id}", name="references_tblentretiens_show", methods={"GET"})
     */
    public function showAction(TblEntretiens $tblEntretien)
    {
        $deleteForm = $this->createDeleteForm($tblEntretien);

        return $this->render('tblentretiens/show.html.twig', [
            'tblEntretien' => $tblEntretien,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblEntretiens entity.
     *
     * @param TblEntretiens $tblEntretien The TblEntretiens entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblEntretiens $tblEntretien)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblentretiens_delete', ['id' => $tblEntretien->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
