<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblAlimentationBetail;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblAlimentationBetail controller.
 *
 * @Route("/{_locale}/references/tblalimentationbetail")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblAlimentationBetailController extends AbstractController
{
    /**
     * Deletes a TblAlimentationBetail entity.
     *
     * @Route("/{id}", name="references_tblalimentationbetail_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblAlimentationBetail $tblAlimentationBetail)
    {
        $form = $this->createDeleteForm($tblAlimentationBetail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblAlimentationBetail);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblalimentationbetail_index');
    }

    /**
     * Displays a form to edit an existing TblAlimentationBetail entity.
     *
     * @Route("/{id}/edit", name="references_tblalimentationbetail_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblAlimentationBetail $tblAlimentationBetail)
    {
        $deleteForm = $this->createDeleteForm($tblAlimentationBetail);
        $editForm = $this->createForm('App\Form\TblAlimentationBetailType', $tblAlimentationBetail);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAlimentationBetail);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblalimentationbetail_index');
            $this->addFlash('success', 'Elément modifié!');
        }

        return $this->render('tblalimentationbetail/edit.html.twig', [
            'tblAlimentationBetail' => $tblAlimentationBetail,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblAlimentationBetail entities.
     *
     * @Route("/", name="references_tblalimentationbetail_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblAlimentationBetails = $em->getRepository(TblAlimentationBetail::class)->findAll();

        return $this->render('tblalimentationbetail/index.html.twig', [
            'tblAlimentationBetails' => $tblAlimentationBetails,
        ]);
    }

    /**
     * Creates a new TblAlimentationBetail entity.
     *
     * @Route("/new", name="references_tblalimentationbetail_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblAlimentationBetail = new TblAlimentationBetail($user);
        $form = $this->createForm('App\Form\TblAlimentationBetailType', $tblAlimentationBetail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAlimentationBetail);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblalimentationbetail_index');
            $this->addFlash('success', 'Elément ajouté!');
        }

        return $this->render('tblalimentationbetail/new.html.twig', [
            'tblAlimentationBetail' => $tblAlimentationBetail,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblAlimentationBetail entity.
     *
     * @Route("/{id}", name="references_tblalimentationbetail_show", methods={"GET"})
     */
    public function showAction(TblAlimentationBetail $tblAlimentationBetail)
    {
        $deleteForm = $this->createDeleteForm($tblAlimentationBetail);

        return $this->render('tblalimentationbetail/show.html.twig', [
            'tblAlimentationBetail' => $tblAlimentationBetail,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblAlimentationBetail entity.
     *
     * @param TblAlimentationBetail $tblAlimentationBetail The TblAlimentationBetail entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblAlimentationBetail $tblAlimentationBetail)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblalimentationbetail_delete', ['id' => $tblAlimentationBetail->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
