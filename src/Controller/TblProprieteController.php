<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblPropriete;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblPropriete controller.
 *
 * @Route("/{_locale}/references/tblpropriete")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblProprieteController extends AbstractController
{
    /**
     * Deletes a TblPropriete entity.
     *
     * @Route("/{id}", name="references_tblpropriete_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblPropriete $tblPropriete)
    {
        $form = $this->createDeleteForm($tblPropriete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblPropriete);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblpropriete_index');
    }

    /**
     * Displays a form to edit an existing TblPropriete entity.
     *
     * @Route("/{id}/edit", name="references_tblpropriete_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblPropriete $tblPropriete)
    {
        $deleteForm = $this->createDeleteForm($tblPropriete);
        $editForm = $this->createForm('App\Form\TblProprieteType', $tblPropriete);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblPropriete);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblpropriete_index', ['id' => $tblPropriete->getId()]);
        }

        return $this->render('tblpropriete/edit.html.twig', [
            'tblPropriete' => $tblPropriete,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblPropriete entities.
     *
     * @Route("/", name="references_tblpropriete_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblProprietes = $em->getRepository(TblPropriete::class)->findAll();

        return $this->render('tblpropriete/index.html.twig', [
            'tblProprietes' => $tblProprietes,
        ]);
    }

    /**
     * Creates a new TblPropriete entity.
     *
     * @Route("/new", name="references_tblpropriete_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblPropriete = new TblPropriete($user);
        $form = $this->createForm('App\Form\TblProprieteType', $tblPropriete);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblPropriete);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblpropriete_index', ['id' => $tblPropriete->getId()]);
        }

        return $this->render('tblpropriete/new.html.twig', [
            'tblPropriete' => $tblPropriete,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblPropriete entity.
     *
     * @Route("/{id}", name="references_tblpropriete_show", methods={"GET"})
     */
    public function showAction(TblPropriete $tblPropriete)
    {
        $deleteForm = $this->createDeleteForm($tblPropriete);

        return $this->render('tblpropriete/show.html.twig', [
            'tblPropriete' => $tblPropriete,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblPropriete entity.
     *
     * @param TblPropriete $tblPropriete The TblPropriete entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblPropriete $tblPropriete)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblpropriete_delete', ['id' => $tblPropriete->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
