<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblOperationCulturaleTypeCategory;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblOperationCulturaleTypeCategory controller.
 *
 * @Route("/{_locale}/tbloperationculturaletypecategory")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblOperationCulturaleTypeCategoryController extends AbstractController
{
    /**
     * Deletes a TblOperationCulturaleTypeCategory entity.
     *
     * @Route("/{id}", name="tbloperationculturaletypecategory_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblOperationCulturaleTypeCategory $tblOperationCulturaleTypeCategory)
    {
        $form = $this->createDeleteForm($tblOperationCulturaleTypeCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblOperationCulturaleTypeCategory);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('tbloperationculturaletypecategory_index');
    }

    /**
     * Displays a form to edit an existing TblOperationCulturaleTypeCategory entity.
     *
     * @Route("/{id}/edit", name="tbloperationculturaletypecategory_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblOperationCulturaleTypeCategory $tblOperationCulturaleTypeCategory)
    {
        $deleteForm = $this->createDeleteForm($tblOperationCulturaleTypeCategory);
        $editForm = $this->createForm('App\Form\TblOperationCulturaleTypeCategoryType', $tblOperationCulturaleTypeCategory);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOperationCulturaleTypeCategory);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('tbloperationculturaletypecategory_index', ['id' => $tblOperationCulturaleTypeCategory->getId()]);
        }

        return $this->render('tbloperationculturaletypecategory/edit.html.twig', [
            'tblOperationCulturaleTypeCategory' => $tblOperationCulturaleTypeCategory,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblOperationCulturaleTypeCategory entities.
     *
     * @Route("/", name="tbloperationculturaletypecategory_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblOperationCulturaleTypeCategories = $em->getRepository(TblOperationCulturaleTypeCategory::class)->findAll();

        return $this->render('tbloperationculturaletypecategory/index.html.twig', [
            'tblOperationCulturaleTypeCategories' => $tblOperationCulturaleTypeCategories,
        ]);
    }

    /**
     * Creates a new TblOperationCulturaleTypeCategory entity.
     *
     * @Route("/new", name="tbloperationculturaletypecategory_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblOperationCulturaleTypeCategory = new TblOperationCulturaleTypeCategory($user);
        $form = $this->createForm('App\Form\TblOperationCulturaleTypeCategoryType', $tblOperationCulturaleTypeCategory);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblOperationCulturaleTypeCategory);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('tbloperationculturaletypecategory_index', ['id' => $tblOperationCulturaleTypeCategory->getId()]);
        }

        return $this->render('tbloperationculturaletypecategory/new.html.twig', [
            'tblOperationCulturaleTypeCategory' => $tblOperationCulturaleTypeCategory,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblOperationCulturaleTypeCategory entity.
     *
     * @Route("/{id}", name="tbloperationculturaletypecategory_show", methods={"GET"})
     */
    public function showAction(TblOperationCulturaleTypeCategory $tblOperationCulturaleTypeCategory)
    {
        $deleteForm = $this->createDeleteForm($tblOperationCulturaleTypeCategory);

        return $this->render('tbloperationculturaletypecategory/show.html.twig', [
            'tblOperationCulturaleTypeCategory' => $tblOperationCulturaleTypeCategory,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblOperationCulturaleTypeCategory entity.
     *
     * @param TblOperationCulturaleTypeCategory $tblOperationCulturaleTypeCategory The TblOperationCulturaleTypeCategory entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblOperationCulturaleTypeCategory $tblOperationCulturaleTypeCategory)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('tbloperationculturaletypecategory_delete', ['id' => $tblOperationCulturaleTypeCategory->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
