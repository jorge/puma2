<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Outillage;
use App\Entity\Exploitations;
use APp\Entity\PivotExploitationOrganisations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Outillage controller.
 *
 * @Route("/{_locale}/references_outillage")
 */

use Symfony\Component\Routing\Annotation\Route;

final class OutillageController extends AbstractController
{
    /**
     * Deletes a Outillage entity.
     *
     * @Route("/{id}", name="references_outillage_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, Outillage $outillage)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $outillage->getAnnee();
        $exploitation_id = $outillage->getExploitation()->getId();
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );
        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $form = $this->createDeleteForm($outillage, [
            'coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $outillage->setRadie(TRUE);
            $em->persist($outillage);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f1_membre', [
            'id' => $exploitation_id,
            'annee' => $annee,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing Outillage entity.
     *
     * @Route("/{id}/edit", name="references_outillage_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, Outillage $outillage)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $outillage->getAnnee();
        $exploitation = $outillage->getExploitation();
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $deleteForm = $this->createDeleteForm($outillage);
        $editForm = $this->createForm('App\Form\OutillageType', $outillage);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($outillage);
            $em->flush();

            return $this->redirectToRoute('saisie_f1_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'coop_id' => $coop->getId(),
                'group_id' => $groupe->getId(),
            ]);
        }

        return $this->render('outillage/edit.html.twig', [
            'outillage' => $outillage,
            'edit_form' => $editForm->createView(),
            'annee' => $annee,
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new Outillage entity.
     *
     * @Route("/new", name="references_outillage_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $_GET['annee'];
        $exploitation_id = $_GET['exploitation_id'];
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $user = $this->getUser();
        $outillage = new Outillage($user);

        $outillage->setExploitation($exploitation);

        $outillage->setAnnee($annee);

        $form = $this->createForm('App\Form\OutillageType', $outillage);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $outillage->setRadie(FALSE);
            $em->persist($outillage);
            $em->flush();

            return $this->redirectToRoute('saisie_f1_membre', [
                'id' => $exploitation_id,
                'annee' => $annee,
                'group_id' => $groupe->getId(),
                'exploitation_id' => $exploitation_id,
                'exploitation' => $exploitation,
                'coop_id' => $coop->getId(), ]);
        }

        return $this->render('outillage/new.html.twig', [
            'outillage' => $outillage,
            'annee' => $annee,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
            'form' => $form->createView(),
        ]);
    }

    /**
     * Creates a form to delete a Outillage entity.
     *
     * @param Outillage $outillage The Outillage entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Outillage $outillage, $options = [])
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_outillage_delete', [
                'id' => $outillage->getId(),
                'coop_id' => $coop->getId(),
                'group_id' => $groupe->getId(),
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
