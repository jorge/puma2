<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblMaterielsBureaux;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblMaterielsBureaux controller.
 *
 * @Route("/{_locale}/references/tblmaterielsbureaux")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblMaterielsBureauxController extends AbstractController
{
    /**
     * Deletes a TblMaterielsBureaux entity.
     *
     * @Route("/{id}", name="references_tblmaterielsbureaux_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblMaterielsBureaux $tblMaterielsBureaux)
    {
        $form = $this->createDeleteForm($tblMaterielsBureaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblMaterielsBureaux);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblmaterielsbureaux_index');
    }

    /**
     * Displays a form to edit an existing TblMaterielsBureaux entity.
     *
     * @Route("/{id}/edit", name="references_tblmaterielsbureaux_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblMaterielsBureaux $tblMaterielsBureaux)
    {
        $deleteForm = $this->createDeleteForm($tblMaterielsBureaux);
        $editForm = $this->createForm('App\Form\TblMaterielsBureauxType', $tblMaterielsBureaux);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblMaterielsBureaux);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblmaterielsbureaux_index', ['id' => $tblMaterielsBureaux->getId()]);
        }

        return $this->render('tblmaterielsbureaux/edit.html.twig', [
            'tblMaterielsBureaux' => $tblMaterielsBureaux,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblMaterielsBureaux entities.
     *
     * @Route("/", name="references_tblmaterielsbureaux_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblMaterielsBureauxes = $em->getRepository(TblMaterielsBureaux::class)->findAll();

        return $this->render('tblmaterielsbureaux/index.html.twig', [
            'tblMaterielsBureauxes' => $tblMaterielsBureauxes,
        ]);
    }

    /**
     * Creates a new TblMaterielsBureaux entity.
     *
     * @Route("/new", name="references_tblmaterielsbureaux_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblMaterielsBureaux = new TblMaterielsBureaux($user);
        $form = $this->createForm('App\Form\TblMaterielsBureauxType', $tblMaterielsBureaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblMaterielsBureaux);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblmaterielsbureaux_index', ['id' => $tblMaterielsBureaux->getId()]);
        }

        return $this->render('tblmaterielsbureaux/new.html.twig', [
            'tblMaterielsBureaux' => $tblMaterielsBureaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblMaterielsBureaux entity.
     *
     * @Route("/{id}", name="references_tblmaterielsbureaux_show", methods={"GET"})
     */
    public function showAction(TblMaterielsBureaux $tblMaterielsBureaux)
    {
        $deleteForm = $this->createDeleteForm($tblMaterielsBureaux);

        return $this->render('tblmaterielsbureaux/show.html.twig', [
            'tblMaterielsBureaux' => $tblMaterielsBureaux,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblMaterielsBureaux entity.
     *
     * @param TblMaterielsBureaux $tblMaterielsBureaux The TblMaterielsBureaux entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblMaterielsBureaux $tblMaterielsBureaux)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblmaterielsbureaux_delete', ['id' => $tblMaterielsBureaux->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
