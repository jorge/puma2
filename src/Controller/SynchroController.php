<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/*
TODO ;
- 1) faire le code JS
- 2) verifier les dates (utiliser)
- 3) mettre les lockers


- se débarasser de $this->SynchroSession

- mettre le locker sur la DB (et le retirer)
- verifier que tout se passe bien pour les donnes (on change bien de status)


- a la fin retirer le lock
- avoir un status pour repartir au debut ?


- mettre creationEnvoiFichierLocal en premier (et faire attention à ne pas recréer si relance la procédure)
    - ne pas entrer dans la procédure si on a reçu des infos de master
    -> le mettre en temps qu'acion (qui sera lancee par le script)


- puis appeler master Traitement Action ()


- a la fin traitementFichierMaster (lance aussi par un script)


 */

namespace App\Controller;

use App\Entity\SynchroSession;
use App\Synchro\SynchroCodeCommun;
use DateTime;
use DateTimeZone;
use Exception;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Filesystem\LockHandler;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

use function count;

/**
 * Class SynchroController.
 *
 * @Route("/{_locale}")
 */

use function in_array;

final class SynchroController extends AbstractController
{
    public const PHASE_1 = 'creationEnvoiFichierLocal'; //on doit faire exécuter creationEnvoiFichierLocalAction par local

    public const PHASE_2 = 'synchronizeMaster'; //on doit faire exécuter synchronizeMasterAction par master

    public const PHASE_3 = 'traitementDonneesMaster'; //on doit faire exécuter traitementDonneesFromMasterAction par local

    public const PHASE_4 = 'finish'; // Il faut fermer la session

    private $dBArchiver; // POUR CREER L'ARCHIVE DU FICHIER (et la charger en db)

    private $fileSender; // Pour envoyer les fichiers

    private $synchroLogger; // The logger linked to the $session_id

    private $synchroSession;

    /**
     * @Route("/synchro/creation-envoi-fichier-local", name="creation_envoi_fichier_local")
     *
     *  Creation et envoi par local du fichier local
     *
     * SI TOUT SE PASSE BIEN LA SESSION DOIT ETRE AVEC COMME STATUS : STATUS_ARCHIVE_ENVOYEE
     */
    public function creationEnvoiFichierLocalAction(Request $request)
    {
        if (!$this->isMaster()) {
            $logger = $this->get('logger');
            $logger->info('creationEnvoiFichierLocalAction');

            $this->synchroSession = $this->getOrCreateSynchroSession($request);
            $this->initSyncServices($this->synchroSession);
            $this->synchroLogger->write('');
            $this->synchroLogger->write(
                'creationEnvoiFichierLocal (STATUS : ' . $this->synchroSession->getStatus() . ')'
            );

            $result = $this->synchroSession->getId();

            if ($this->synchroSession->getMasterStatus() === SynchroSession::STATUS_TO_INIT) { //master n'a encore rien recu
                try {
                    /**
                     * reparamétrage temporel de php.
                     */
                    set_time_limit(0);
                    $mymem = ini_get('memory_limit');
                    ini_set('memory_limit', '700M');
                    //ini_set('display_errors', 'off');
                    $mydtz = ini_get('date.timezone');
                    ini_set('date.timezone', 'UTC');

                    if ($this->synchroSession->getStatus() === SynchroSession::STATUS_ARCHIVE_ENVOYEE) {
                        return new JsonResponse($this->synchroSession->toArray());
                    }

                    if (
                        SynchroSession::NBTRY_LIMIT < 0
                            || $this->synchroSession->getNbtry() < SynchroSession::NBTRY_LIMIT
                    ) {
                        $this->synchroLogger->write('Ok for computation');
                        $this->synchroSession->incrementNbtry();

                        if ($this->synchroSession->getStatus() === SynchroSession::STATUS_TO_INIT) {
                            $this->synchroLogger->write('if STATUS_TO_INIT');
                            $previousClosedSession = $this->getPreviousClosedSession($request);

                            if ($previousClosedSession) { // Pas la première session de synchro
                                $this->synchroLogger->write('Fin a previous version');
                                $this->synchroSession->setDateDebutData(
                                    $previousClosedSession->getDateFinMajSql()
                                ); // donnes à prendre doivent être > dateDebut pour eviter d'envoyer des données qui viennent d'être reçues.
                                $this->synchroSession->setMasterDateDebutData(
                                    $previousClosedSession->getMasterDateFinMajSql()
                                );
                            } else {
                                $this->synchroLogger->write('No previous version founded');
                                $this->synchroLogger->write('Calcul a partir de la db');

                                $em = $this->getDoctrine()->getManager();

                                $synchroStatus = $em->getRepository(SynchroStatus::class)->findUnique();

                                $this->synchroSession->setDateDebutData(
                                    $synchroStatus->getLocalInitUdate()
                                );

                                $this->synchroSession->setMasterDateDebutData(
                                    $synchroStatus->getMasterLastUdate()
                                );
                            }
                            $this->synchroSession->setDateFinData($this->getDateNowUTC());

                            $this->synchroSession->setStatus(SynchroSession::STATUS_INIT_OK);
                        }

                        if (
                            $this->synchroSession->getStatus() === SynchroSession::STATUS_INIT_OK
                            || $this->synchroSession->getStatus() === SynchroSession::STATUS_ARCHIVE_KO
                        ) {
                            $this->synchroLogger->write('Archive computation local');

                            /* Créer l'archive zip de local et le envoyer au maitre */
                            $this->synchroLogger->write('case local_status: ' . $this->synchroSession->getStatus());
                            $this->synchroLogger->write('Début création Archive avec tous les fichiers sql.');
                            $archive_result = $this->dBArchiver->_creation_archive($this->synchroSession);

                            if (!$archive_result['succes']) {
                                $this->synchroLogger->write('PBM');
                                $this->synchroLogger->write($archive_result['msg']);
                                $this->synchroSession->setStatus(SynchroSession::STATUS_ARCHIVE_KO);
                            } else {
                                $this->synchroLogger->write('Le fichier archive a bien ete cree');
                                $this->synchroSession->setStatus(SynchroSession::STATUS_ARCHIVE_CREE);
                            }
                        } else {
                            $this->synchroLogger->write('Archive computation (no) mais PK ?');
                        }

                        if ($this->synchroSession->getStatus() === SynchroSession::STATUS_ARCHIVE_CREE) {
                            $this->synchroLogger->write('Archive creation');

                            /* Chargement de local data vers master */
                            $this->synchroLogger->write('Envois vers le site ftp');
                            $my_zip = $this->synchroSession->getZipname();
                            $this->synchroSession->saveIntoAFile(
                                $this->container->getParameter('synchro_path') . $this->synchroSession->getSessionFileName()
                            ); // on est en local et on traite le fichier local
                            $result = $this->fileSender->_upload_archive($this->synchroSession);

                            if ($result['succes']) {
                                unlink($this->container->getParameter('synchro_path') . $my_zip);
                                $this->synchroSession->setStatus(SynchroSession::STATUS_ARCHIVE_ENVOYEE);
                                $this->synchroLogger->write('Fichier des modifs envoyé sur le serveur FTP.');
                                $this->synchroLogger->write($result['msg']);
                                $this->synchroLogger->write(sprintf('Pour completer la synchronisation veuilliez relancer le processus dans %s heures', $this->container->getParameter('synchro_wait_hours')));
                            } else {
                                $this->synchroLogger->write("L\\'envois vers le serveur FTP a échoué.");
                                $this->synchroLogger->write($result['msg']);
                            }
                        } else {
                            $this->synchroLogger->write('Archive creation (no)');
                        }

                        if ($this->synchroSession->getStatus() === SynchroSession::STATUS_ARCHIVE_ENVOYEE) {
                        }

                        // TODO GERER CES STATUS
                        if (
                            $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_MD5_KO
                            || $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_ZIP_KO
                            || $this->synchroSession->getStatus() === SynchroSession::STATUS_LOAD_SQL_KO
                        ) {
                            // TODO METTRE CES STATUS setStatus XXX Dans ce fichier
                            // PBM
                            // cas à gérer ici ?

                            $this->synchroLogger->write('Le fichier reçu par le serveur a &eacute;t&eacute; corrompu, veuillez relancer la synchronisation.');
                            $this->synchroLogger->write('PROBLEME: ' . $this->synchroSession->getStatus() . ', NBTRY: ' . $this->synchroSession->getNbtry());
                            /**
                             * Effacer les fichiers master pour pouvoir les retélécharger.
                             */
                            unlink($this->container->getParameter('synchro_path') . $master_archive_filename); // on est en local TODO FAUT IL SUPPIER ?
                            unlink($this->container->getParameter('synchro_path') . $local_master_status_filename); // on est en local
                        }

                        // CAS PAR DEFAULT (si rentré dans rien )
                        /*
                        if {
                            $this->synchroLogger->write("PROBLEME: $local_status, NBTRY: $nbtry");
                            throw new \Exception($local_status);
                            break;
                        }
                         */

                        return new JsonResponse($this->synchroSession->toArray());
                    }

                    return new JsonResponse([
                        'error' => true,
                        'msg' => 'NBTRY > ' . SynchroSession::NBTRY_LIMIT, ]);
                } catch (Exception $e) {
                    $msg = sprintf('Dump local interrompu pour exception: %s', $e->getMessage());
                    $this->synchroLogger->write($msg);
                    $result = $msg;

                    return new JsonResponse($result);
                } finally {
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    $this->synchroLogger->write('Création du fichier local_status depuis ce qui a été sauvé dans la DB');
                    ini_set('memory_limit', $mymem);
                    ini_set('date.timezone', $mydtz);
                }
            } else {
                $arrayRep = $this->synchroSession->toArray();
                $arrayRep['msg'] = 'Deja fait';

                return new JsonResponse($arrayRep);
            }
        } else {
            return new JsonResponse(['error' => true, 'msg' => 'La méthode ne peut être exécutée qu\'avec un ordinateur ayant le role LOCAL.']);
        }
    }

    public function getCurrentSession()
    {
        $em = $this->getDoctrine()->getManager();
        $notClosedSessions = $em->getRepository(SynchroSession::class)->findByClosed(false);

        $currentSession = null;

        if (count($notClosedSessions) > 1) {
            throw new Exception("Plus d'une session de synchro existante", 1);
        }

        if (count($notClosedSessions) === 1) {
            $currentSession = $notClosedSessions[0];
        }

        return $currentSession;
    }

    /**
     * Retourne la phase de la sychro qui doit être jouée en fonction de la session.
     */
    public function getPhaseFromSession()
    {
        $currentSession = $this->getCurrentSession();

        if (
            !$currentSession || in_array($currentSession->getStatus(), [
                SynchroSession::STATUS_TO_INIT,
                SynchroSession::STATUS_INIT_OK,
                SynchroSession::STATUS_ARCHIVE_KO,
                SynchroSession::STATUS_ARCHIVE_CREE, ], true)
        ) {
            return self::PHASE_1;
        }

        if (
            $currentSession->getStatus() === SynchroSession::STATUS_ARCHIVE_ENVOYEE
            && in_array($currentSession->getMasterStatus(), [
                SynchroSession::STATUS_TO_INIT,
                SynchroSession::STATUS_FICHIERS_NON_RECUS,
                SynchroSession::STATUS_FICHIERS_RECUS,
                SynchroSession::STATUS_REMOTE_MD5_KO,
                SynchroSession::STATUS_REMOTE_MD5_OK,
                SynchroSession::STATUS_REMOTE_ZIP_KO,
                SynchroSession::STATUS_REMOTE_ZIP_OK,
                SynchroSession::STATUS_ARCHIVE_KO,
                SynchroSession::STATUS_ARCHIVE_CREE,
                SynchroSession::STATUS_LOAD_SQL_KO,
            ], true)
        ) {
            return self::PHASE_2;
        }

        if (
            in_array($currentSession->getMasterStatus(), [
                SynchroSession::STATUS_LOAD_SQL_OK,
                SynchroSession::STATUS_TO_INIT, // si fichier masterSESSION PAS ENCORE RECU
            ], true)
            && in_array($currentSession->getStatus(), [
                SynchroSession::STATUS_LOAD_SQL_OK,
                SynchroSession::STATUS_ARCHIVE_ENVOYEE,
                SynchroSession::STATUS_REMOTE_INTROUVABLES,
                SynchroSession::STATUS_REMOTE_TELECHARGES,
                SynchroSession::STATUS_REMOTE_SESSION_FILE_CORROMPUS,
                SynchroSession::STATUS_REMOTE_SESSION_FILE_CORRECT,
                SynchroSession::STATUS_REMOTE_SYNC_NOT_FINISH,
                SynchroSession::STATUS_REMOTE_SESSION_FILE_CORRECT,
                SynchroSession::STATUS_REMOTE_MD5_KO,
                SynchroSession::STATUS_REMOTE_MD5_OK,
                SynchroSession::STATUS_REMOTE_ZIP_KO,
                SynchroSession::STATUS_REMOTE_ZIP_OK,
                SynchroSession::STATUS_LOAD_SQL_KO,
            ], true)
        ) {
            return self::PHASE_3;
        }

        if (
            $currentSession->getMasterStatus() === SynchroSession::STATUS_LOAD_SQL_OK
            && $currentSession->getStatus() === SynchroSession::STATUS_LOAD_SQL_OK
        ) {
            return self::PHASE_4;
        }

        throw new Exception(
            'STATUS SESSION NON GERER local:' . $currentSession->getStatus() .
            ' master:' . $currentSession->getMasterStatus(),
            1
        );
    }

    /**
     * @Route("/synchro", name="synchro")
     *
     * Ecran d'acceuil pour la syncho (voir l'état de la dernière et instructions
     * pour faire la suivante)
     *
     * Lance la synchro via JS (voir web/pumastatic/js/synchro.js)
     */
    public function indexAction(Request $request)
    {
        $currentSession = $this->getCurrentSession();
        $phase = $this->getPhaseFromSession();

        // TODO voir ce qui a entre les deux
        $arg = null;
        $cmd = 'creationEnvoiFichierLocal';

        if ($currentSession) {
            if (self::PHASE_3 === $phase) {
                $cmd = 'traitementDonneesMaster';
            } elseif (self::PHASE_2 === $phase) {
                $cmd = 'masterSynchronize';
                $arg = $currentSession->getId();
            }

            $synchroLogger = $this->container->get('synchro_logger');
            $synchroLogger->setSession($currentSession);
            $lastLogsLines = $synchroLogger->getLastXLogLines();
            $phase = $this->getPhaseFromSession();
        } else {
            $lastLogsLines = null;
            $phase = null;
        }

        $em = $this->getDoctrine()->getManager();
        $synchroStatus = $em->getRepository(SynchroStatus::class)->findUnique();

        // Get $synchroMasterUrl
        $synchroMasterUrl = null;

        if (!$this->isMaster()) {
            // PAS pour master
            if (!$this->container->hasParameter('synchro_master_url')) {
                $synchroMasterUrl = 'http://151.80.144.77';
            /*
            return $this->render(
                'erreur.html.twig',
                array(
                    'titre' => "Le paramètre synchro_master_url n'existe pas !",
                    'message' => "Le paramètre synchro_master_url indique l'url du 'master' pour la syncho. Ce paramètre n'existe pas, veuillez contacter la personne qui gère les installations afin de régler le problème. Merci",
                )
            );
             */
            } else {
                $synchroMasterUrl = $this->container->getParameter('synchro_master_url');
            }
        }

        $reponse = $this->render(
            'synchro/index.html.twig',
            [
                'synchro_master_url' => $synchroMasterUrl,
                'current_session' => $currentSession,
                'cmd' => $cmd,
                'arg' => $arg,
                'ordi_id' => $synchroStatus->getLocalId(),
                'last_log_lines' => $lastLogsLines,
                'phase' => $phase,
            ]
        );
        $reponse->headers->set('Access-Control-Allow-Origin', '151:80:144:70');

        return $reponse;
    }

    /**
     * @Route("/synchro/dates-test/{limit}", name="synchro-dates-test", requirements={"limit": "\d+"})
     * Tests pour vérifier que les dates mysql et php sont bien synchro.
     * Attention pour avoir cette synchro, il faut utiliser des dates en UTC
     *
     * @param mixed $limit
     */
    public function mysqlPhpDateComparisonAction($limit = 50)
    {
        $outHtml = '<html>';

        for ($i = 0; $i < $limit; ++$i) {
            $outHtml .= ('---' . $i . '---<br>');

            $conn = $this->getDoctrine()->getManager()->getConnection();
            $time = $conn->query('SELECT NOW() as now;')->fetch();

            $outHtml .= ('mysql: ' . $time['now'] . '<br>');

            $now = new DateTime();
            $now->setTimeZone(new DateTimeZone('UTC'));
            $outHtml .= ('php  : ' . $now->format('Y-m-d H:i:s') . '<br>');
        }

        for ($i = 0; $i < $limit; ++$i) {
            $outHtml .= ('---' . $i . '---<br>');

            $now = new DateTime();
            $now->setTimeZone(new DateTimeZone('UTC'));
            $outHtml .= ('php  : ' . $now->format('Y-m-d H:i:s') . '<br>');

            $conn = $this->getDoctrine()->getManager()->getConnection();
            $time = $conn->query('SELECT NOW() as now;')->fetch();

            $outHtml .= ('mysql: ' . $time['now'] . '<br>');
        }

        $outHtml .= ('-----<br>');
        $outHtml .= ($conn->getDatabasePlatform()->getCurrentTimeSql()) . '<br>';
        $outHtml .= ('-----<br>');

        $outHtml .= '</html>';

        return new Response($outHtml);
    }

    /**
     * @Route("/synchro/master/synchronize/{sessionId}", name="exe_synchro_master")
     *
     * Lancement de la synchronisation du master
     *
     * @param mixed $sessionId
     */
    public function synchronizeMasterAction(Request $request, $sessionId)
    {
        if ($this->isMaster()) {
            // TRAVAILER AVEC les masterStatus

            $logger = $this->get('logger');
            $logger->info('Démarrage de la mise à jour master pour session' . $sessionId);

            $this->fileSender = $this->container->get('synchro_file_sender');
            $this->fileSender->setLogger($logger);

            $localSessionFileName = SynchroSession::sessionFileNameFor($sessionId);
            $absoluteLocalSessionFileName = $this->fileSender->absoluteSynchroPath($localSessionFileName);

            $localZipName = SynchroSession::zipNameFor($sessionId);
            $absoluteLocalZipName = $this->fileSender->absoluteSynchroPath($localZipName);

            // SI FICHIER MASTER EXISTE DEJA CHARGER CELUI LA !!!!
            // TODO SI N'EXISTE PAS ?
            $session = SynchroSession::loadFromFile($absoluteLocalSessionFileName);
            $this->initSyncServices($session);

            if (
                SynchroSession::NBTRY_LIMIT >= 0
                    && $session->getMasterNbtry() >= SynchroSession::NBTRY_LIMIT
            ) {
                $this->synchroLogger->write('-- -- NBTRY DEPASSE -- --'); // A FAIRE DU COTE JS ?
                $retArray = $session->toArray();
                $retArray['error'] = true;
                $retArray['msg'] = 'Limite nbtry depassée :' . SynchroSession::NBTRY_LIMIT;

                return new JsonResponse($retArray);
            }

            if (
                $session->getMasterStatus() === SynchroSession::STATUS_LOAD_SQL_OK
                && $session->getMasterDateFinMajSql()
            ) { // A DEJA ETE FAIT
                $this->synchroLogger->write('-- -- DEJA ETE FAIT -- --');
                $retArray = $session->toArray();
                $retArray['msg'] = 'MAJ master deja faite';

                return new JsonResponse($retArray);
            }
            $this->synchroLogger->write('-- -- Nouvel essai - MASTER -- -- (status' . $session->getMasterStatus() . ') ');
            $session->incrementMasterNbtry();

            if (
                $session->getMasterStatus() === SynchroSession::STATUS_TO_INIT
                    || $session->getMasterStatus() === SynchroSession::STATUS_FICHIERS_NON_RECUS
            ) {
                if (!(file_exists($absoluteLocalSessionFileName) && file_exists($absoluteLocalZipName))) {
                    $session->setMasterStatus(SynchroSession::STATUS_FICHIERS_NON_RECUS);
                } else {
                    $session->setMasterStatus(SynchroSession::STATUS_FICHIERS_RECUS);
                }
            }

            $this->synchroLogger->write('Vérifier le md5 du fichier envoyé par local');
            $this->synchroLogger->write($session->getMasterStatus());

            if (
                $session->getMasterStatus() === SynchroSession::STATUS_FICHIERS_RECUS
                    || $session->getMasterStatus() === SynchroSession::STATUS_REMOTE_MD5_KO
            ) {
                $zipMd5 = $this->dBArchiver->_get_md5_file($absoluteLocalZipName);

                $this->synchroLogger->write($zipMd5 . ' ?= ' . $session->getZipmd5());

                if ($session->getZipmd5() === $zipMd5) {
                    $session->setMasterStatus(SynchroSession::STATUS_REMOTE_MD5_OK);
                } else {
                    $session->setMasterStatus(SynchroSession::STATUS_REMOTE_MD5_KO);
                    // TODO
                        // REMOVE ARCHIVE
                        // REDEMANDER SYNCHOOR
                }
            }

            $extract_folder = $this->container->getParameter('synchro_path') . $session->getId() . '_extract/';

            if (
                $session->getMasterStatus() === SynchroSession::STATUS_REMOTE_MD5_OK
                    || $session->getMasterStatus() === SynchroSession::STATUS_REMOTE_ZIP_KO
            ) {
                /**
                 * Dézipper l'archive et charger les data.
                 */
                $this->synchroLogger->write('Dézipper l\'archive et charger les data');

                // OLD : $blnExt = $this->_extract_archive($this->synchro_path . $master_archive_filename, $this->synchro_path); #TODO METTRE AILLEURS
                $blnExt = $this->dBArchiver->_extract_archive($absoluteLocalZipName, $extract_folder);

                if ($blnExt['succes']) {
                    $session->setMasterStatus(SynchroSession::STATUS_REMOTE_ZIP_OK);
                } else {
                    $session->setMasterStatus(SynchroSession::STATUS_REMOTE_ZIP_KO);
                }
            }

            // ON BLOQUE LA DB POUR AVOIR 1 SEUL ACCESS A LA DB
            if ((new LockHandler('App:synchronizeMasterAction:acces-db'))->lock()) {
                $this->synchroLogger->write('Acces DB OK');

                if (
                    $session->getMasterStatus() === SynchroSession::STATUS_REMOTE_ZIP_OK
                        || $session->getMasterStatus() === SynchroSession::STATUS_ARCHIVE_KO
                ) {
                    $session->setMasterDateFinData($this->getDateNowUTC());
                    $archive_result = $this->dBArchiver->_creation_archive($session);

                    if (!$archive_result['succes']) {
                        $session->setMasterStatus(SynchroSession::STATUS_ARCHIVE_KO);
                    } else {
                        $this->synchroLogger->write('Le fichier archive a bien ete cree');
                        $session->setMasterStatus(SynchroSession::STATUS_ARCHIVE_CREE);
                    }
                }

                if (
                    $session->getMasterStatus() === SynchroSession::STATUS_ARCHIVE_CREE
                        || $session->getMasterStatus() === SynchroSession::STATUS_LOAD_SQL_KO
                ) {
                    $loadSql = $this->dBArchiver->loadSqlFct($extract_folder, 'synchro.sql');

                    if ($loadSql['succes']) {
                        $session->setMasterStatus(SynchroSession::STATUS_LOAD_SQL_OK);
                        $session->setMasterDateFinMajSql($this->getDateNowUTC());

                        // /!\ la setMasterDateFinMajSql est à eclure des prochaines données à passer (pour éviter de renvoyer des données reçues)
                        // par contre le nouveau numéro de membre ne doit pas être exlus -> on attend 1 seconde pour être sûr que
                        // masterDateFinMajSql < au  udate des entites avec nouveau numéro de membres

                        sleep(1);

                        $nMGenerateur = $this->container->get('generateur_numero_membre');
                        $nMGenerateur->generationNumeroMembre();

                        $em = $this->getDoctrine()->getManager();
                        $synchroStatus = $em->getRepository(SynchroStatus::class)->findUnique();
                        $synchroStatus->setMasterLastUdate($session->getMasterDateFinMajSql());
                        $em->flush();
                    } else {
                        $this->synchroLogger->write($loadSql['msg']);
                        $session->setMasterStatus(SynchroSession::STATUS_LOAD_SQL_KO);
                    }
                }
            } else {
                $this->synchroLogger->write('Acces DB KO');
            }

            $masterSessionFileName = SynchroSession::masterSessionFileNameFor($sessionId);

            $absoluteMasterSessionFileName = $this->fileSender->absoluteSynchroPath($masterSessionFileName);
            $session->saveIntoAFile($absoluteMasterSessionFileName);

            // SAUVEGARDER LE FICHIER ? (sinon tout recommence)

            $this->synchroLogger->write('- -- - - -FIN --- - - - ');
            $this->synchroLogger->write($session->getMasterStatus());
            $this->synchroLogger->write(json_encode($session->toArray()));

            //return new JsonResponse(array('12' => 12));
            $response = new Response(json_encode($session->toArray()));
            $response->headers->set('Content-Type', 'application/json');
            $response->headers->set('Access-Control-Allow-Origin', '*');

            return $response;
        }

        $this->synchroLogger->write('- -- - - -AIE --- - - - ');

        return new JsonResponse(['error' => true, 'msg' => 'La méthode ne peut être exécutée qu\'avec un ordinateur ayant le role MASTER.']);
    }

    /**
     * @Route("/synchro/traitement-donnees-master", name="traitement_donnees_master")
     *
     * Traitement par local du fichier master
     * TODO Trouver un autre nom qui rock
     *
     * Pour commencer la session doit être en STATUS_ARCHIVE_ENVOYEE
     */
    public function traitementDonneesFromMasterAction(Request $request)
    {
        if (!$this->isMaster()) {
            $logger = $this->get('logger');
            $logger->info('traitementDonneesFromMasterAction');

            $this->synchroSession = $this->getOrCreateSynchroSession($request); // pas get or create
            $this->initSyncServices($this->synchroSession);

            $this->synchroLogger->write('');
            $this->synchroLogger->write(
                'traitementDonneesFromMasterAction nbry:' . $this->synchroSession->getNbtry() . ' status ' . $this->synchroSession->getStatus()
            );

            $result = $this->synchroSession->getId();

            $this->synchroLogger->write('Récuperer le fichier maitre_synchro');
            $file_master_telecharge = true;

            $masterArchiveFilename = $this->synchroSession->getMasterZipname();
            $masterArchiveFullFilename = $this->fileSender->absoluteSynchroPath($masterArchiveFilename);

            $this->synchroLogger->write($masterArchiveFullFilename);

            $masterSessionFilename = $this->synchroSession->getMasterSessionFileName();
            $masterSessionFullFilename = $this->fileSender->absoluteSynchroPath($masterSessionFilename);

            $this->synchroLogger->write($masterSessionFullFilename);

            if ($this->synchroSession->getStatus() === SynchroSession::STATUS_LOAD_SQL_OK) {
                $retArray = $this->synchroSession->toArray();
                $retArray['Deja fait'] = true;

                return new JsonResponse($retArray);
            }

            if (SynchroSession::NBTRY_LIMIT < 0 || $this->synchroSession->getNbtry() < SynchroSession::NBTRY_LIMIT) {
                $this->synchroSession->incrementNbtry();

                try {
                    /**
                     * reparamétrage temporel de php.
                     */
                    set_time_limit(0);
                    $mymem = ini_get('memory_limit');
                    ini_set('memory_limit', '700M');
                    //ini_set('display_errors', 'off');
                    $mydtz = ini_get('date.timezone');
                    ini_set('date.timezone', 'UTC');

                    //TODO ICI DETECTER SI ETAPE DEJA FAITE

                    if (
                        $this->synchroSession->getStatus() === SynchroSession::STATUS_ARCHIVE_ENVOYEE
                        || $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_INTROUVABLES
                        || $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_SESSION_FILE_CORROMPUS
                    ) {
                        $result = $this->fileSender->_download_archive_and_statusfile($this->synchroSession);
                        $this->synchroLogger->write($result['msg']);
                        $this->synchroLogger->write('----');
                        $this->synchroSession->setStatus($masterSessionFullFilename); // TODO PBM ICI

                        if ($result['succes'] && file_exists($masterSessionFullFilename)) {
                            // succes renvoyer par _download_archive_and_statusfile est a vrai si on a telecharger le fichier
                            // + verifier via file_exists TODO à faire ?
                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_TELECHARGES);
                        } else {
                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_INTROUVABLES);
                        }
                    }

                    if ($this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_TELECHARGES) {
                        /**
                         * Fichiers récupérés ou présents: lire le status pour décider
                         * l'action à  entreprendre.
                         */
                        $this->synchroLogger->write('TRAITEMENT FICHIER MASTER MASTER');
                        $master_session = SynchroSession::loadFromFile($masterSessionFullFilename);

                        if ($this->synchroSession->getId() !== $master_session->getId()) {
                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_SESSION_FILE_CORROMPUS);
                        } else {
                            //$this->synchroSession->setMasterDateDebutData($master_session->getMasterDateDebutData());
                            $this->synchroSession->setMasterDateFinData($master_session->getMasterDateFinData());
                            $this->synchroSession->setMasterDateFinMajSql($master_session->getMasterDateFinMajSql());
                            $this->synchroSession->setMasterNbtry($master_session->getMasterNbtry());
                            $this->synchroSession->setMasterZipmd5($master_session->getMasterZipmd5());
                            $this->synchroSession->setMasterStatus($master_session->getMasterStatus());

                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_SESSION_FILE_CORRECT);

                            $master_status = $this->synchroSession->getMasterStatus();
                            $this->synchroLogger->write('MASTER STATUS' . $master_status);
                        }

                        if (SynchroSession::STATUS_LOAD_SQL_OK !== $master_status) {
                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_SYNC_NOT_FINISH);
                        }

                        if ('A JOUR' === $master_status) { // TODO SUPPRIMER OU GERER
                            throw new Exception('PAS GERER', 1);
                            /* Le serveur est à jour rien à importer */
                            $this->synchroLogger->write('Pas de nouvelles donn&eacute;es pour nous sur le serveur. A JOUR.');
                        }
                    }

                    // VERIFIER LE MD5
                    if (
                        $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_SESSION_FILE_CORRECT
                        || $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_MD5_KO
                    ) {
                        // TODO SI LE ZIP PAS LE MEME IL FAUT RETELECHARGER

                        $master_archive_md5 = $this->synchroSession->getMasterZipmd5();

                        $this->synchroLogger->write('Vérifier le md5 de maitre_archive');
                        $expected_md5 = $this->dBArchiver->_get_md5_file($masterArchiveFullFilename); //TODO METTRE AILLEURS
                        $this->synchroLogger->write('expected: ' . $expected_md5 . ' - read: ' . $master_archive_md5 . '');

                        if ($expected_md5 === $master_archive_md5) {
                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_MD5_OK);
                        } else {
                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_MD5_KO);
                        }
                    }

                    // CHECK LE ZIP
                    if (
                        $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_MD5_OK
                        || $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_ZIP_KO
                    ) {
                        $extractFolder = $this->dBArchiver->getMasterSqlRepositoryPath($this->synchroSession); // LA OU SONT EXTRAIT LES FICHIERS SQL
                        $blnExt = $this->dBArchiver->_extract_archive($masterArchiveFullFilename, $extractFolder);

                        if (!$blnExt['succes']) {
                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_ZIP_KO);
                        } else {
                            // TODO FINI ICI
                            $this->synchroSession->setStatus(SynchroSession::STATUS_REMOTE_ZIP_OK);
                        }
                    }

                    // CHARGER LES FICHIERS SQL
                    if (
                        $this->synchroSession->getStatus() === SynchroSession::STATUS_REMOTE_ZIP_OK
                        || $this->synchroSession->getStatus() === SynchroSession::STATUS_LOAD_SQL_KO
                    ) {
                        $extractFolder = $this->dBArchiver->getMasterSqlRepositoryPath($this->synchroSession);
                        $loadSql = $this->dBArchiver->loadSqlFct($extractFolder, 'synchro.sql');
                        // $this->_delete_sql_files(); TODO
                        if ($loadSql['succes']) {
                            $this->synchroSession->setStatus(SynchroSession::STATUS_LOAD_SQL_OK);
                            $this->synchroSession->setDateFinMajSql($this->getDateNowUTC());
                            $this->synchroSession->setClosed(true);

                            $masterZipFileName = $this->fileSender->absoluteSynchroPath($this->synchroSession->getMasterZipname());

                            if (file_exists($masterZipFileName)) {
                                unlink($masterZipFileName);
                            }

                            $masterSessionFileName = $this->fileSender->absoluteSynchroPath($this->synchroSession->getMasterSessionFileName());

                            if (file_exists($masterSessionFileName)) {
                                unlink($masterSessionFileName);
                            }
                        } else {
                            $this->synchroSession->setStatus(SynchroSession::STATUS_LOAD_SQL_KO);
                        }
                    }
                } catch (Exception $e) {
                    $msg = sprintf('Dump local interrompu pour exception: %s', $e->getMessage());
                    $this->synchroLogger->write($msg);
                    $result = $msg;

                    return new JsonResponse(json_encode($result)); // TODO PBM retour dans catch avant finallu
                } finally {
                    $em = $this->getDoctrine()->getManager();
                    $em->flush();
                    $this->synchroLogger->write('Création du fichier local_status depuis ce qui a été sauvé dans la DB');
                    ini_set('memory_limit', $mymem);
                    ini_set('date.timezone', $mydtz);
                }

                return new JsonResponse($this->synchroSession->toArray());
            }

            return new JsonResponse([
                'error' => true,
                'msg' => 'NBTRY > ' . SynchroSession::NBTRY_LIMIT, ]);
        }

        return new JsonResponse(['error' => true, 'msg' => 'La méthode ne peut être exécutée qu\'avec un ordinateur ayant le role LOCAL.']);
    }

    /*
     * Return the date 'NOW' in UTC
     */
    private function getDateNowUTC()
    {
        $now = new DateTime('NOW');
        $now->setTimeZone(new DateTimeZone('UTC'));

        return $now;
    }

    /**
     * Cherche la dernière synchroSession non fermée. Si ça n'existe pass
     * crée une nouvelle synchro session.
     */
    private function getOrCreateSynchroSession(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        // TODO: Does not exist in SynchroSessionRepository->findByClosed().
        $notClosedSessions = $em->getRepository(SynchroSession::class)->findByClosed(false);

        if (count($notClosedSessions) > 1) {
            throw new Exception("Plus d'une session de synchro existante", 1);
        }

        if (count($notClosedSessions) === 1) {
            return $notClosedSessions[0];
        }
        $synchroSession = new SynchroSession();
        $em = $this->getDoctrine()->getManager();
        $em->persist($synchroSession);
        $em->flush();

        return $synchroSession;
    }

    private function getPreviousClosedSession(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $closedSessionOrderByFinDate = $em
            ->getRepository(SynchroSession::class)
            ->findBy(['closed' => true], ['dateFinMajSql' => 'DESC']);

        if ([] !== $closedSessionOrderByFinDate) {
            return $closedSessionOrderByFinDate[0];
        }

        return null;
        // SINON PRENDRE 1 SESSION QUI A POUR POUR MASTER
            // CHOSES DEJA DEDANS :
            // - date master : jusque quand la db a fait tourner le script de synchro (data lors du dump)
            // - data local : avoir le plus vieux updata (date de l'install)
    }

    /**
     * Initialise et paramétrise les services :
     * - synchroLogger (pour ecrire des logs)
     * - dBArchiver (pour création et chargement des archives)
     * - fileSender (pour envoi des archives via ftp).
     *
     * @param mixed $synchroSession
     */
    private function initSyncServices($synchroSession)
    {
        $this->synchroLogger = $this->container->get('synchro_logger');
        $this->synchroLogger->setSession($synchroSession);

        $this->dBArchiver = $this->container->get('synchro_db_archiver');
        $this->dBArchiver->setLogger($this->synchroLogger);

        $this->fileSender = $this->container->get('synchro_file_sender');
        $this->fileSender->setLogger($this->synchroLogger);
    }

    /**
     * Retourne true si l'ordi sur lequel on travaille a le role master
     * (pour la synchro).
     */
    private function isMaster()
    {
        return $this->container->getParameter('synchro_role') === SynchroCodeCommun::ROLE_MASTER;
    }
}
