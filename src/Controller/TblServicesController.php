<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblServices;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblServices controller.
 *
 * @Route("/{_locale}/references/tblservices")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblServicesController extends AbstractController
{
    /**
     * Deletes a TblServices entity.
     *
     * @Route("/{id}", name="references_tblservices_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblServices $tblService)
    {
        $form = $this->createDeleteForm($tblService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblService);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblservices_index');
    }

    /**
     * Displays a form to edit an existing TblServices entity.
     *
     * @Route("/{id}/edit", name="references_tblservices_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblServices $tblService)
    {
        $deleteForm = $this->createDeleteForm($tblService);
        $editForm = $this->createForm('App\Form\TblServicesType', $tblService);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblService);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblservices_index', ['id' => $tblService->getId()]);
        }

        return $this->render('tblservices/edit.html.twig', [
            'tblService' => $tblService,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblServices entities.
     *
     * @Route("/", name="references_tblservices_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblServices = $em->getRepository(TblServices::class)->findAll();

        return $this->render('tblservices/index.html.twig', [
            'tblServices' => $tblServices,
        ]);
    }

    /**
     * Creates a new TblServices entity.
     *
     * @Route("/new", name="references_tblservices_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblService = new TblServices($user);
        $form = $this->createForm('App\Form\TblServicesType', $tblService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblService);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblservices_index', ['id' => $tblService->getId()]);
        }

        return $this->render('tblservices/new.html.twig', [
            'tblService' => $tblService,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblServices entity.
     *
     * @Route("/{id}", name="references_tblservices_show", methods={"GET"})
     */
    public function showAction(TblServices $tblService)
    {
        $deleteForm = $this->createDeleteForm($tblService);

        return $this->render('tblservices/show.html.twig', [
            'tblService' => $tblService,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblServices entity.
     *
     * @param TblServices $tblService The TblServices entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblServices $tblService)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblservices_delete', ['id' => $tblService->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
