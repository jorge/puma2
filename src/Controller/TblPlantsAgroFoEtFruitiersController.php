<?php

declare(strict_types=1);

namespace App\Controller;

use App\Entity\TblPlantsAgroFoEtFruitiers;
use App\Form\TblPlantsAgroFoEtFruitiersType;
use App\Repository\TblPlantsAgroFoEtFruitiersRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * @Route("/{_locale}/references/plants/agrofo/fuitiers")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblPlantsAgroFoEtFruitiersController extends AbstractController
{
    /**
     * @Route("/{id}", name="references_tblagrofofruitiers_delete", methods={"DELETE"})
     */
    public function delete(Request $request, TblPlantsAgroFoEtFruitiers $tblPlantsAgroFoEtFruitier): Response
    {
        if ($this->isCsrfTokenValid('delete' . $tblPlantsAgroFoEtFruitier->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($tblPlantsAgroFoEtFruitier);
            $entityManager->flush();
        }

        return $this->redirectToRoute('references_tblagrofofruitiers_index');
    }

    /**
     * @Route("/{id}/edit", name="references_tblagrofofruitiers_edit", methods={"GET", "POST"})
     */
    public function edit(Request $request, TblPlantsAgroFoEtFruitiers $tblPlantsAgroFoEtFruitier): Response
    {
        $form = $this->createForm(TblPlantsAgroFoEtFruitiersType::class, $tblPlantsAgroFoEtFruitier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('references_tblagrofofruitiers_index');
        }

        return $this->render('tbl_plants_agro_fo_et_fruitiers/edit.html.twig', [
            'tbl_plants_agro_fo_et_fruitier' => $tblPlantsAgroFoEtFruitier,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/", name="references_tblagrofofruitiers_index", methods={"GET"})
     */
    public function index(TblPlantsAgroFoEtFruitiersRepository $tblPlantsAgroFoEtFruitiersRepository): Response
    {
        return $this->render('tbl_plants_agro_fo_et_fruitiers/index.html.twig', [
            'tbl_plants_agro_fo_et_fruitiers' => $tblPlantsAgroFoEtFruitiersRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="references_tblagrofofruitiers_new", methods={"GET", "POST"})
     */
    public function new(Request $request): Response
    {
        $user = $this->getUser();
        $tblPlantsAgroFoEtFruitier = new TblPlantsAgroFoEtFruitiers($user);
        $form = $this->createForm(TblPlantsAgroFoEtFruitiersType::class, $tblPlantsAgroFoEtFruitier);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($tblPlantsAgroFoEtFruitier);
            $entityManager->flush();

            return $this->redirectToRoute('references_tblagrofofruitiers_index');
        }

        return $this->render('tbl_plants_agro_fo_et_fruitiers/new.html.twig', [
            'tbl_plants_agro_fo_et_fruitier' => $tblPlantsAgroFoEtFruitier,
            'form' => $form->createView(),
        ]);
    }
}
