<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller\Saisies;

use App\Entity\Collecte;
use App\Entity\Exercice;
use App\Entity\Exploitations;
use App\Entity\ParcelleHistoriques;
use App\Entity\Parcelles;
use App\Entity\Saisies\CollecteExercice;
use App\Form\Saisies\CollecteExerciceAnneeType;
use App\Form\Saisies\CollecteExerciceType;
use App\Repository\ParcelleHistoriquesRepository;
use App\Repository\ParcellesRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Validator\Validator\ValidatorInterface;

/**
 * @Route("/{_locale}/saisie_collecte_exercice", name="saisie_collect_exercice__")
 */

use function count;

final class CollecteExerciceController extends AbstractController
{
    /**
     * Display a form to change Form Exercice year.
     *
     * @Route("/{exploitation}/{annee}/F{type}/edit/", name="edit", methods={"GET", "POST"})
     */
    public function editAnneeAction(
        Request $request,
        Exploitations $exploitation,
        ParcellesRepository $parcellesRepository,
        ParcelleHistoriquesRepository $parcelleHistoriquesRepository,
        int $annee,
        int $type
    ) {
        // Il existe bien une collecte/exercice pour cette exploitation
        if ($exploitation->hasCollecteFor($annee, $type)) {
            /** @var Collecte $existingCollecte */
            $existingCollecte = $exploitation->getCollecteFor($annee, $type);
        } else {
            throw new \Exception("Erreur ! Cette exploitation n'a pas de fiche F" . $type . " pour l'exercice " . $annee . '.');
        }

        // Crée un object collecteExercice temporaire
        $collecteExercice = $this->createTempCollecteExercice($existingCollecte);

        $form = $this->createForm(CollecteExerciceAnneeType::class, $collecteExercice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $user = $this->getUser();

            // L'année modifiée lorsque le form est soumis
            $anneeModifiee = $collecteExercice->getExercice();

            /**
             * L'exercice source, et la(les) collecte(s) associée(s).
             *
             * @var Exercice $fromExercice
             */
            $fromExercice = $exploitation->getExerciceFor($annee);

            $collectes = $fromExercice->getCollectes();

            if (count($collectes) > 1) {
                // il y a plusieurs collectes liées à l'exercice source
                foreach ($collectes as $collecte) {
                    // dont celle concernée
                    /** @var Collecte $collecte */
                    if ($collecte->getType() === $type) {
                        /**
                         * L'exercice de destination.
                         *
                         * @var Exercice $toExercice
                         */
                        $toExercice = $exploitation->getExerciceFor($anneeModifiee);

                        // l'exercice de destination existe déjà
                        if ($toExercice) {
                            // et il a déjà la collecte concernée
                            if ($exploitation->hasCollecteFor($anneeModifiee, $type)) {
                                throw $this->notMergedException($anneeModifiee, $type);
                            }
                            // ou alors il n'a pas encore cette collecte

                            // attache la collecte à l'exercice de destination
                            $collecte->setExercice($toExercice);
                        }
                        // l'exercice de destination n'existe pas encore
                        else {
                            // crée l'exercice
                            $toNewExercice = new Exercice($user);
                            $toNewExercice->setExploitation($exploitation);
                            $toNewExercice->setAnnee($anneeModifiee); // <===
                            $em->persist($toNewExercice);

                            // attache la collecte
                            $collecte->setExercice($toNewExercice);
                        }
                    }
                }
            } elseif (count($collectes) === 1) {
                // une seule collecte liée à l'exercice source
                /** @var Collecte $collecte */
                $collecte = $collectes[0];

                /**
                 * L'exercice de destination.
                 *
                 * @var Exercice $toExercice
                 */
                $toExercice = $exploitation->getExerciceFor($anneeModifiee);

                // l'exercice de destination existe déjà
                if ($toExercice) {
                    // et il a déjà la collecte concernée
                    if ($exploitation->hasCollecteFor($anneeModifiee, $type)) {
                        throw $this->notMergedException($anneeModifiee, $type);
                    }
                    // ou alors il n'a pas encore cette collecte

                    // attache la collecte à l'exercice de destination
                    $collecte->setExercice($toExercice);

                    // et supprime l'exercice source
                    $em->remove($fromExercice);
                }
                // l'exercice de destination n'existe pas encore
                else {
                    // l'exercice source devient l'exercice de destination
                    // on change juste l'année de l'exercice source

                    /** @var Exercice $fromExercice */
                    $fromExercice->setAnnee($anneeModifiee); // <===
                }
            } else {
                // sans doute pas nécessaire
                throw new \Exception("Erreur d'inconsistance! aucune collecte n'est liée à l'exercice " . $fromExercice . '.');
            }

            //////////

            /**
             * COLLECTE F2: les parcelles.
             */
            $parcelles = $parcellesRepository->findBy([
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]);

            foreach ($parcelles as $parcelle) {
                /** @var Parcelles $parcelle */
                $parcelle->setAnnee($anneeModifiee);

                $parcelle_historiques = $parcelleHistoriquesRepository->findBy([
                    'parcelle' => $parcelle,
                    'annee' => $annee,
                ]);

                foreach ($parcelle_historiques as $historique) {
                    /** @var ParcelleHistoriques $historique */
                    $historique->setAnnee($anneeModifiee);
                }
            }

            $em->flush();

            return $this->redirectToRoute('saisie_info_gen_sans_annee', [
                'exploitation' => $exploitation->getId(),
            ]);
        }

        return $this->render('saisies/collecte_exercice_edit.html.twig', [
            'form' => $form->createView(),
            'exploitation' => $exploitation,
            'collecteExercice' => $collecteExercice,
            'annee' => $annee,
            'type' => $type,
        ]);
    }

    /**
     * @Route("/{exploitation}/new/", name="new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, Exploitations $exploitation, ValidatorInterface $validator)
    {
        $em = $this->getDoctrine()->getManager();

        $collecteExercice = new CollecteExercice();

        $user = $this->getUser();

        $form = $this->createForm(CollecteExerciceType::class, $collecteExercice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // checker si exo existe
            // puis créer collecte (sauf si existe déjà)
            // retour en info_gen

            $anneeExercice = $collecteExercice->getExercice();
            $existingExercice = $exploitation->getExerciceFor($anneeExercice);

            if (null === $existingExercice) {
                $exercice = new Exercice($user);
                $exercice->setExploitation($exploitation);
                $exercice->setAnnee($anneeExercice);

                $em->persist($exercice);
            } else {
                $exercice = $existingExercice;
            }

            $collecte = new Collecte($user);
            $collecte->setExercice($exercice);
            $collecte->setEnqueteur($collecteExercice->getEnqueteur());
            $collecte->setDate($collecteExercice->getDate());
            $collecte->setType($collecteExercice->getType());
            $collecte->setEnqueteur($this->getUser()); // TODO REMOVE utilisateur car se trouve dans creator & lastEditor

            $errors = $validator->validate($collecte);

            if (count($errors) > 0) {
                foreach ($errors as $e) {
                    $this->addFlash(
                        'error',
                        $e->getMessage()
                    );
                }
            } else {
                $em->persist($collecte);
                $em->flush();

                return $this->redirectToRoute('saisie_info_gen', [
                    'exploitation' => $exploitation->getId(),
                ]);
            }
        }

        return $this->render('saisies/collecte_exercice_new.html.twig', [
            'form' => $form->createView(),
            'exploitation' => $exploitation,
            'collecteExercice' => $collecteExercice,
        ]);
    }

    /**
     * Crée un objet temporaire pour gérer les 2 tables.
     *
     * @var CollecteExercice
     *
     * @return CollecteExercice
     */
    private function createTempCollecteExercice(Collecte $existingCollecte)
    {
        $collecteExercice = new CollecteExercice();

        $collecteExercice
            ->setExercice($existingCollecte->getExercice()->getAnnee())
            ->setType($existingCollecte->getType())
            ->setEnqueteur($existingCollecte->getEnqueteur())
            ->setDate($existingCollecte->getDate());

        return $collecteExercice;
    }

    /**
     * @param $anneeModifiee
     * @param $type
     *
     * @return Exception
     */
    private function notMergedException($anneeModifiee, $type)
    {
        return new \Exception('Il y a déjà une fiche F' . $type . " pour l'exercice " . $anneeModifiee
            . ". La fusion des deux fiches n'est pas prise en charge !");
    }
}
