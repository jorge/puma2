<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblProvinces;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblProvinces controller.
 *
 * @Route("/{_locale}/references/tblprovinces")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblProvincesController extends AbstractController
{
    /**
     * Deletes a TblProvinces entity.
     *
     * @Route("/{id}", name="references_tblprovinces_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblProvinces $tblProvince)
    {
        $form = $this->createDeleteForm($tblProvince);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblProvince);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblprovinces_index');
    }

    /**
     * Displays a form to edit an existing TblProvinces entity.
     *
     * @Route("/{id}/edit", name="references_tblprovinces_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblProvinces $tblProvince)
    {
        $deleteForm = $this->createDeleteForm($tblProvince);
        $editForm = $this->createForm('App\Form\TblProvincesType', $tblProvince);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblProvince);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblprovinces_edit', ['id' => $tblProvince->getId()]);
        }

        return $this->render('tblprovinces/edit.html.twig', [
            'tblProvince' => $tblProvince,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblProvinces entities.
     *
     * @Route("/", name="references_tblprovinces_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblProvinces = $em->getRepository(TblProvinces::class)->findAll();

        return $this->render('tblprovinces/index.html.twig', [
            'tblProvinces' => $tblProvinces,
        ]);
    }

    /**
     * Creates a new TblProvinces entity.
     *
     * @Route("/new", name="references_tblprovinces_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblProvince = new TblProvinces($user);
        $form = $this->createForm('App\Form\TblProvincesType', $tblProvince);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblProvince);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblprovinces_show', ['id' => $tblProvince->getId()]);
        }

        return $this->render('tblprovinces/new.html.twig', [
            'tblProvince' => $tblProvince,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblProvinces entity.
     *
     * @Route("/{id}", name="references_tblprovinces_show", methods={"GET"})
     */
    public function showAction(TblProvinces $tblProvince)
    {
        $deleteForm = $this->createDeleteForm($tblProvince);

        return $this->render('tblprovinces/show.html.twig', [
            'tblProvince' => $tblProvince,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblProvinces entity.
     *
     * @param TblProvinces $tblProvince The TblProvinces entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblProvinces $tblProvince)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblprovinces_delete', ['id' => $tblProvince->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
