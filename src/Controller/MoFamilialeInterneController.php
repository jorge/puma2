<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\MoFamilialeInterne;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * MoFamilialeInterne controller.
 *
 * @Route("/{_locale}/references_mofamilialeinterne")
 */

use Symfony\Component\Routing\Annotation\Route;

final class MoFamilialeInterneController extends AbstractController
{
    /**
     * Deletes a MoFamilialeInterne entity.
     *
     * @Route("/{id}", name="references_mofamilialeinterne_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, MoFamilialeInterne $moFamilialeInterne)
    {
        $exploitation = $moFamilialeInterne->getExploitation();
        $anne = $moFamilialeInterne->getAnnee();

        $form = $this->createDeleteForm($moFamilialeInterne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $moFamilialeInterne->setRadie(TRUE);
            $em->persist($moFamilialeInterne);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'annee' => $moFamilialeInterne->getAnnee(),
            'id' => $exploitation->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing MoFamilialeInterne entity.
     *
     * @Route("/{id}/edit", name="references_mofamilialeinterne_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, MoFamilialeInterne $moFamilialeInterne)
    {
        $exploitation = $moFamilialeInterne->getExploitation();
        $annee = $moFamilialeInterne->getAnnee();

        $deleteForm = $this->createDeleteForm($moFamilialeInterne);
        $editForm = $this->createForm('App\Form\MoFamilialeInterneType', $moFamilialeInterne);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($moFamilialeInterne);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('mofamilialeinterne/edit.html.twig', [
            'moFamilialeInterne' => $moFamilialeInterne,
            'edit_form' => $editForm->createView(),
            'annee' => $annee,
            'exploitation' => $exploitation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new MoFamilialeInterne entity.
     *
     * @Route("/{exploitation}/{annee}/new", name="references_mofamilialeinterne_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Exploitations $exploitation, $annee)
    {
        $user = $this->getUser();
        $moFamilialeInterne = new MoFamilialeInterne($user);
        $moFamilialeInterne->setExploitation($exploitation);
        $moFamilialeInterne->setAnnee($annee);

        $form = $this->createForm('App\Form\MoFamilialeInterneType', $moFamilialeInterne);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $moFamilialeInterne->setRadie(FALSE);
            $em->persist($moFamilialeInterne);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('mofamilialeinterne/new.html.twig', [
            'moFamilialeInterne' => $moFamilialeInterne,
            'annee' => $annee,
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a MoFamilialeInterne entity.
     *
     * @Route("/{id}", name="references_mofamilialeinterne_show", methods={"GET"})
     */
    public function showAction(MoFamilialeInterne $moFamilialeInterne)
    {
        $exploitation = $moFamilialeInterne->getExploitation();
        $annee = $moFamilialeInterne->getAnnee();

        $deleteForm = $this->createDeleteForm($moFamilialeInterne);

        return $this->render('mofamilialeinterne/show.html.twig', [
            'moFamilialeInterne' => $moFamilialeInterne,
            'annee' => $annee,
            'exploitation' => $exploitation,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a MoFamilialeInterne entity.
     *
     * @param MoFamilialeInterne $moFamilialeInterne The MoFamilialeInterne entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MoFamilialeInterne $moFamilialeInterne, $options = [])
    {
        $exploitation = $moFamilialeInterne->getExploitation();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_mofamilialeinterne_delete', [
                'id' => $moFamilialeInterne->getId(),
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
