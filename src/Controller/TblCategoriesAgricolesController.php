<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblCategoriesAgricoles;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblCategoriesAgricoles controller.
 *
 * @Route("/{_locale}/references/tblcategoriesagricoles")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblCategoriesAgricolesController extends AbstractController
{
    /**
     * Deletes a TblCategoriesAgricoles entity.
     *
     * @Route("/{id}", name="references_tblcategoriesagricoles_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblCategoriesAgricoles $tblCategoriesAgricole)
    {
        $form = $this->createDeleteForm($tblCategoriesAgricole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblCategoriesAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblcategoriesagricoles_index');
    }

    /**
     * Displays a form to edit an existing TblCategoriesAgricoles entity.
     *
     * @Route("/{id}/edit", name="references_tblcategoriesagricoles_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblCategoriesAgricoles $tblCategoriesAgricole)
    {
        $deleteForm = $this->createDeleteForm($tblCategoriesAgricole);
        $editForm = $this->createForm('App\Form\TblCategoriesAgricolesType', $tblCategoriesAgricole);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblCategoriesAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcategoriesagricoles_index', ['id' => $tblCategoriesAgricole->getId()]);
        }

        return $this->render('tblcategoriesagricoles/edit.html.twig', [
            'tblCategoriesAgricole' => $tblCategoriesAgricole,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblCategoriesAgricoles entities.
     *
     * @Route("/", name="references_tblcategoriesagricoles_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblCategoriesAgricoles = $em->getRepository(TblCategoriesAgricoles::class)->findAll();

        return $this->render('tblcategoriesagricoles/index.html.twig', [
            'tblCategoriesAgricoles' => $tblCategoriesAgricoles,
        ]);
    }

    /**
     * Creates a new TblCategoriesAgricoles entity.
     *
     * @Route("/new", name="references_tblcategoriesagricoles_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblCategoriesAgricole = new TblCategoriesAgricoles($user);
        $form = $this->createForm('App\Form\TblCategoriesAgricolesType', $tblCategoriesAgricole);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblCategoriesAgricole);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcategoriesagricoles_index', ['id' => $tblCategoriesAgricole->getId()]);
        }

        return $this->render('tblcategoriesagricoles/new.html.twig', [
            'tblCategoriesAgricole' => $tblCategoriesAgricole,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblCategoriesAgricoles entity.
     *
     * @Route("/{id}", name="references_tblcategoriesagricoles_show", methods={"GET"})
     */
    public function showAction(TblCategoriesAgricoles $tblCategoriesAgricole)
    {
        $deleteForm = $this->createDeleteForm($tblCategoriesAgricole);

        return $this->render('tblcategoriesagricoles/show.html.twig', [
            'tblCategoriesAgricole' => $tblCategoriesAgricole,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblCategoriesAgricoles entity.
     *
     * @param TblCategoriesAgricoles $tblCategoriesAgricole The TblCategoriesAgricoles entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblCategoriesAgricoles $tblCategoriesAgricole)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblcategoriesagricoles_delete', ['id' => $tblCategoriesAgricole->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
