<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblProfessions;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblProfessions controller.
 *
 * @Route("/{_locale}/references/tblprofessions")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblProfessionsController extends AbstractController
{
    /**
     * Deletes a TblProfessions entity.
     *
     * @Route("/{id}", name="references_tblprofessions_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblProfessions $tblProfession)
    {
        $form = $this->createDeleteForm($tblProfession);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblProfession);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblprofessions_index');
    }

    /**
     * Displays a form to edit an existing TblProfessions entity.
     *
     * @Route("/{id}/edit", name="references_tblprofessions_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblProfessions $tblProfession)
    {
        $deleteForm = $this->createDeleteForm($tblProfession);
        $editForm = $this->createForm('App\Form\TblProfessionsType', $tblProfession);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblProfession);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblprofessions_index', ['id' => $tblProfession->getId()]);
        }

        return $this->render('tblprofessions/edit.html.twig', [
            'tblProfession' => $tblProfession,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblProfessions entities.
     *
     * @Route("/", name="references_tblprofessions_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblProfessions = $em->getRepository(TblProfessions::class)->findAll();

        return $this->render('tblprofessions/index.html.twig', [
            'tblProfessions' => $tblProfessions,
        ]);
    }

    /**
     * Creates a new TblProfessions entity.
     *
     * @Route("/new", name="references_tblprofessions_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblProfession = new TblProfessions($user);
        $form = $this->createForm('App\Form\TblProfessionsType', $tblProfession);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblProfession);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblprofessions_index', ['id' => $tblProfession->getId()]);
        }

        return $this->render('tblprofessions/new.html.twig', [
            'tblProfession' => $tblProfession,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblProfessions entity.
     *
     * @Route("/{id}", name="references_tblprofessions_show", methods={"GET"})
     */
    public function showAction(TblProfessions $tblProfession)
    {
        $deleteForm = $this->createDeleteForm($tblProfession);

        return $this->render('tblprofessions/show.html.twig', [
            'tblProfession' => $tblProfession,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblProfessions entity.
     *
     * @param TblProfessions $tblProfession The TblProfessions entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblProfessions $tblProfession)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblprofessions_delete', ['id' => $tblProfession->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
