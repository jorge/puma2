<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\ContraintesDeveloppement;
use App\Entity\Exploitations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * ContraintesDeveloppement controller.
 *
 * @Route("/{_locale}/references/contraintesdeveloppement")
 */

use Symfony\Component\Routing\Annotation\Route;

final class ContraintesDeveloppementController extends AbstractController
{
    /**
     * Deletes a ContraintesDeveloppement entity.
     *
     * @Route("/{id}", name="references_contraintesdeveloppement_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, ContraintesDeveloppement $contraintesDeveloppement)
    {
        $exploitation = $contraintesDeveloppement->getExploitation();
        $annee = $contraintesDeveloppement->getAnnee();
        $form = $this->createDeleteForm($contraintesDeveloppement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $contraintesDeveloppement->setRadie(TRUE);
            $em->persist($contraintesDeveloppement);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f0_membre', [
            'id' => $exploitation->getId(),
            'annee' => $annee,
        ]);
    }

    /**
     * Displays a form to edit an existing ContraintesDeveloppement entity.
     *
     * @Route("/{id}/edit", name="references_contraintesdeveloppement_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, ContraintesDeveloppement $contraintesDeveloppement)
    {
        $exploitation = $contraintesDeveloppement->getExploitation();
        $annee = $contraintesDeveloppement->getAnnee();

        $deleteForm = $this->createDeleteForm($contraintesDeveloppement);
        $editForm = $this->createForm('App\Form\ContraintesDeveloppementType', $contraintesDeveloppement);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contraintesDeveloppement);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('contraintesdeveloppement/edit.html.twig', [
            'contraintesDeveloppement' => $contraintesDeveloppement,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
    }

    /**
     * Lists all ContraintesDevaloppement entities par exploitation.
     *
     * @Route("/exploitation/{id}", name="references_contraintesdeveloppement_exploitation_list", methods={"GET"})
     *
     * TODO a supprimer ?
     */
    public function exploitationListAction(Request $request, Exploitations $exploitation)
    {
        //      $coop_id = $_GET['coop_id'];
        //      $group_id = $_GET['group_id'];
        $exploitation_id = $_GET['exploitation_id'];
        $annee = $_GET['annee'];

        $em = $this->getDoctrine()->getManager();
        $contraintesdeveloppement = $em->getRepository(ContraintesDeveloppement::class)->findBy([
            'exploitation' => $exploitation, 'radie' => 0, ]);

        return $this->render('contraintesdeveloppement/exploitationList.html.twig', [
            'contraintesDeveloppement' => $contraintesdeveloppement,
            'exploitation_id' => $exploitation_id,
            'exploitation' => $exploitation,
            'annee' => $annee,
            //          'group_id' => $group_id,
            //          'coop_id'=>$coop_id
        ]);
    }

    /**
     * Creates a new ContraintesDeveloppement entity.
     *
     * @Route("/{exploitation}/{annee}/new", name="references_contraintesdeveloppement_new", methods={"GET", "POST"})
     *
     * @param mixed $annee
     */
    public function newAction(Request $request, Exploitations $exploitation, $annee)
    {
        $user = $this->getUser();
        $contraintesDeveloppement = new ContraintesDeveloppement($user);
        $contraintesDeveloppement->setExploitation($exploitation);
        $contraintesDeveloppement->setAnnee($annee);

        $form = $this->createForm('App\Form\ContraintesDeveloppementType', $contraintesDeveloppement);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($contraintesDeveloppement);
            $em->flush();

            return $this->redirectToRoute('saisie_f0_membre', [
                'id' => $exploitation->getId(),
                'annee' => $annee,
            ]);
        }

        return $this->render('contraintesdeveloppement/new.html.twig', [
            'form' => $form->createView(),
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
    }

    /**
     * Finds and displays a ContraintesDeveloppement entity.
     *
     * @Route("/{id}", name="references_contraintesdeveloppement_show", methods={"GET"})
     */
    public function showAction(ContraintesDeveloppement $contraintesDeveloppement)
    {
        $exploitation = $contraintesDeveloppement->getExploitation();
        $annee = $contraintesDeveloppement->getAnnee();

        $deleteForm = $this->createDeleteForm($contraintesDeveloppement);

        return $this->render('contraintesdeveloppement/show.html.twig', [
            'contraintesDeveloppement' => $contraintesDeveloppement,
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
    }

    /**
     * Creates a form to delete a ContraintesDeveloppement entity.
     *
     * @param ContraintesDeveloppement $contraintesDeveloppement The ContraintesDeveloppement entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ContraintesDeveloppement $contraintesDeveloppement)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_contraintesdeveloppement_delete', [
                'id' => $contraintesDeveloppement->getId(),
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
