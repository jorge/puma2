<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblPrestationsServices;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblPrestationsServices controller.
 *
 * @Route("/{_locale}/references/tblprestationsservices")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblPrestationsServicesController extends AbstractController
{
    /**
     * Deletes a TblPrestationsServices entity.
     *
     * @Route("/{id}", name="references_tblprestationsservices_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblPrestationsServices $tblPrestationsService)
    {
        $form = $this->createDeleteForm($tblPrestationsService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblPrestationsService);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblprestationsservices_index');
    }

    /**
     * Displays a form to edit an existing TblPrestationsServices entity.
     *
     * @Route("/{id}/edit", name="references_tblprestationsservices_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblPrestationsServices $tblPrestationsService)
    {
        $deleteForm = $this->createDeleteForm($tblPrestationsService);
        $editForm = $this->createForm('App\Form\TblPrestationsServicesType', $tblPrestationsService);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblPrestationsService);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblprestationsservices_index', ['id' => $tblPrestationsService->getId()]);
        }

        return $this->render('tblprestationsservices/edit.html.twig', [
            'tblPrestationsService' => $tblPrestationsService,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblPrestationsServices entities.
     *
     * @Route("/", name="references_tblprestationsservices_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblPrestationsServices = $em->getRepository(TblPrestationsServices::class)->findAll();

        return $this->render('tblprestationsservices/index.html.twig', [
            'tblPrestationsServices' => $tblPrestationsServices,
        ]);
    }

    /**
     * Creates a new TblPrestationsServices entity.
     *
     * @Route("/new", name="references_tblprestationsservices_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblPrestationsService = new TblPrestationsServices($user);
        $form = $this->createForm('App\Form\TblPrestationsServicesType', $tblPrestationsService);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblPrestationsService);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblprestationsservices_index', ['id' => $tblPrestationsService->getId()]);
        }

        return $this->render('tblprestationsservices/new.html.twig', [
            'tblPrestationsService' => $tblPrestationsService,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblPrestationsServices entity.
     *
     * @Route("/{id}", name="references_tblprestationsservices_show", methods={"GET"})
     */
    public function showAction(TblPrestationsServices $tblPrestationsService)
    {
        $deleteForm = $this->createDeleteForm($tblPrestationsService);

        return $this->render('tblprestationsservices/show.html.twig', [
            'tblPrestationsService' => $tblPrestationsService,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblPrestationsServices entity.
     *
     * @param TblPrestationsServices $tblPrestationsService The TblPrestationsServices entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblPrestationsServices $tblPrestationsService)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblprestationsservices_delete', ['id' => $tblPrestationsService->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
