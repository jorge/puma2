<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblCommunes;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblCommunes controller.
 *
 * @Route("/{_locale}/references/tblcommunes")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblCommunesController extends AbstractController
{
    /**
     * Deletes a TblCommunes entity.
     *
     * @Route("/{id}", name="references_tblcommunes_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblCommunes $tblCommune)
    {
        $form = $this->createDeleteForm($tblCommune);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblCommune);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblcommunes_index');
    }

    /**
     * Displays a form to edit an existing TblCommunes entity.
     *
     * @Route("/{id}/edit", name="references_tblcommunes_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblCommunes $tblCommune)
    {
        $deleteForm = $this->createDeleteForm($tblCommune);
        $editForm = $this->createForm('App\Form\TblCommunesType', $tblCommune);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblCommune);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcommunes_edit', ['id' => $tblCommune->getId()]);
        }

        return $this->render('tblcommunes/edit.html.twig', [
            'tblCommune' => $tblCommune,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblCommunes entities.
     *
     * @Route("/", name="references_tblcommunes_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblCommunes = $em->getRepository(TblCommunes::class)->findAll();

        return $this->render('tblcommunes/index.html.twig', [
            'tblCommunes' => $tblCommunes,
        ]);
    }

    /**
     * Creates a new TblCommunes entity.
     *
     * @Route("/new", name="references_tblcommunes_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblCommune = new TblCommunes($user);
        $form = $this->createForm('App\Form\TblCommunesType', $tblCommune);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblCommune);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblcommunes_show', ['id' => $tblCommune->getId()]);
        }

        return $this->render('tblcommunes/new.html.twig', [
            'tblCommune' => $tblCommune,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblCommunes entity.
     *
     * @Route("/{id}", name="references_tblcommunes_show", methods={"GET"})
     */
    public function showAction(TblCommunes $tblCommune)
    {
        $deleteForm = $this->createDeleteForm($tblCommune);

        return $this->render('tblcommunes/show.html.twig', [
            'tblCommune' => $tblCommune,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblCommunes entity.
     *
     * @param TblCommunes $tblCommune The TblCommunes entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblCommunes $tblCommune)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblcommunes_delete', ['id' => $tblCommune->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
