<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\Exploitations;
use App\Entity\PersonneDetails;
use App\Entity\Personnes;
use App\Entity\RapportsAllSignaletique;
use App\Entity\RapportsFilieres;
use App\Entity\RapportsSpecialF0;
use App\Entity\TblSaisons;
use App\Form\RapportsFilieresType;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\StreamedResponse;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Contracts\Translation\TranslatorInterface;

/**
 * Class RapportsController.
 *
 * @Route("/{_locale}")
 */

use function count;

final class RapportsController extends AbstractController
{
    /**
     * @Route("/rapports/allsignaletique", name="rapports_allsignaletique")
     */
    public function allsignaletiqueAction(
        Request $request,
        TranslatorInterface $translator,
        EntityManagerInterface $em
    ) {
        $AllSignaletique = new RapportsAllSignaletique();

        $form = $this->createForm('App\Form\RapportsAllSignaletiqueType', $AllSignaletique, [
            //  'nb_annees_passees' => $this->container->getParameter('annee_passees')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->getAllSignaletique($translator, $em, $form);

            return $this->render('rapports/rec_AllSignaletique.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
            ]);
        }

        return $this->render('rapports/allsignaletique.html.twig', [
            'AllSignaletique' => $AllSignaletique,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Ask Req with year.
     *
     * @Route("/report/year/season/choice/then/{reportName}", name="rapport_ask_year_saison_then_report")
     */
    public function askYearAndSeasonReport(Request $request, string $reportName)
    {
        $form = $this->createFormBuilder()
            ->add('year', IntegerType::class, ['label' => 'Annéee'])
            ->add('saison', EntityType::class, [
                'label' => 'Saison',
                'class' => TblSaisons::class, ])
            ->add('save', SubmitType::class, ['label' => 'Générer l\'export des données'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            return $this->redirectToRoute(
                $reportName,
                ['year' => $formData['year'], 'saison' => $formData['saison']->getId()]
            );
        }

        return $this->render('rapports/option_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /**
     * Ask Req with year.
     *
     * @Route("/rapports/year/choice/then/{reportName}", name="rapport_ask_year_then_report")
     */
    public function askYearlyReport(Request $request, string $reportName)
    {
        $form = $this->createFormBuilder()
            ->add('year', IntegerType::class, ['label' => 'Année'])
            ->add('save', SubmitType::class, ['label' => 'Générer l\'export des données'])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $formData = $form->getData();

            return $this->redirectToRoute($reportName, ['year' => $formData['year']]);
        }

        return $this->render('rapports/option_form.html.twig', [
            'form' => $form->createView(),
        ]);
    }

    /* --- cooperatives ----*/

    /**
     * @Route("/rapports/cooperatives", name="rapports_cooperatives")
     */
    public function cooperativesAction(Request $request)
    {
        return $this->render('rapports/cooperatives.html.twig', [
            'params' => 'mes paramètres pour les rapports des coopératives',
        ]);
    }

    /**
     * @Route("/rapports/coopnum", name="coopnum")
     */
    public function coopnumAction(Request $request, TranslatorInterface $translator)
    {
        $numero = (int) ($request->get('num'));
        $annee = (int) $request->get('annee');

        if (10 !== $numero) {
            $result = $this->getcooperatives($numero, $annee, $translator);

            return $this->render('rapports/rec_cooperatives.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
            ]);
        }
        $result = $this->getcooperatives10($numero, $annee, $translator);

        return $this->render('rapports/rec_cooperatives10.html.twig', [
            'recordset1' => $result['recordset1'],
            'recordset2' => $result['recordset2'],
            'recordset3' => $result['recordset3'],
            'fields1' => $result['fields1'],
            'fields2' => $result['fields2'],
            'fields3' => $result['fields3'],
            'titre1' => $result['titre1'],
            'titre2' => $result['titre2'],
            'titre3' => $result['titre3'],
        ]);
    }

    /**
     * Evaluation / Number.
     *
     * @Route("/rapports/evalnum/{requestId}", name="rapports_evalnum")
     *
     * @param mixed $requestId
     */
    public function evalnumAction(
        TranslatorInterface $translator,
        EntityManagerInterface $em,
        Request $request,
        $requestId
    ) {
        $annee = $request->get('annee'); // todo fixme : à fair passer dans url
        $mtwig = $request->get('redirect'); // todo fixme : à faire passer aussi dans url, ne voit pas à quoi ça sert..

        if (!is_numeric($requestId)) {
            switch ($requestId) {
                case 'agroeco':
                    $result = $this->evalNbExpAgroEco($annee);

                    break;
                case 'coesexage':
                    $result = $this->evalListExpSexeAge();

                    break;
                case 'repsexage':
                    $result = $this->evalListRepondantSexeAge();

                    break;
                case 'norep':
                    $result = $this->evalQualityNoRepondant();

                    break;
            }
        } else {
            $result = $this->getEvaluation($translator, $em, $requestId, $annee);
        }

        if (null !== $mtwig) {  // TODO / FixMe : savoir à quoi sert mtwig ?
            return $this->render('consultation/rec_f1.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
                'strSql' => $result['strSql'],
            ]);
        }

        return $this->render('rapports/evalnum.html.twig', [
            'recordset' => $result['recordset'],
            'fields' => $result['fields'],
            'titre' => $result['titre'],
        ]);
    }

    /**
     * Evaluation.
     *
     * @Route("/rapports/evaluation", name="rapports_evaluation")
     */
    public function evaluationAction(Request $request)
    {
        return $this->render('rapports/evaluation.html.twig');
    }

    /* -------------- filieres -----------------------*/

    /**
     * @Route("/rapports/filieres", name="rapports_filieres") }}")
     * (à garder ?)
     */
    public function filieresAction(Request $request, TranslatorInterface $translator, EntityManagerInterface $em)
    {
        $RapportsFilieres = new RapportsFilieres();

        $form = $this->createForm(RapportsFilieresType::class, $RapportsFilieres, [
            //      'nb_annees_passees' => $this->container->getParameter('annee_passees')
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->getfiliere($translator, $em);

            return $this->render('rapports/rec_filiere.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
            ]);
        }

        return $this->render('rapports/filieres.html.twig', [
            'RapportsFilieres' => $RapportsFilieres,
            'form' => $form->createView(),
        ]);
    }

    /*
     * Get the first rows for structured CAPAD requests.
     * All these resquests returns csv data having always the first rows.
     * These rows are :
     * - Province
     * - Commune
     * - Colline
     * - Coopérative
     * - Groupement
     * - Nom
     * - Prénom
     * - Numéro de membre
     *
     * Takes 2 arguments : $exploitation, $person
     * - if both are null : the return is the name of the columns
     * - if $exploitation is null, the exploitation will be $person->getExploitation()
     * - if $person is null, the person will be $exploitation->getGerant()
     *
     */
    public function get_first_colums_for_reqX(?Exploitations $exploitation = null, ?Personnes $person = null)
    {
        $returnHeader = null === $exploitation && null === $person; // si la personne veut l'entête

        if ($returnHeader) {
            return [
                'Province', 'Commune', 'Colline',
                utf8_decode('Coopérative'), 'Groupement',
                'Nom', utf8_decode('Prénom'),
                utf8_decode('Numéro de membre'), ];
        }

        if (null === $exploitation) {
            $exploitation = $person->getExploitation();
        }

        if (null === $person) {
            $person = $exploitation->getRepondant();
        }

        $localisation = $exploitation->getLocalisationAdmin();

        [$groupementObj, $cooperativeObj] = $exploitation->getGroupementAndCooperative();

        $collineObj = $localisation->getByLevel('colline');
        $communeObj = null;
        $provinceObj = null;

        if (null !== $collineObj) {
            $communeObj = $collineObj->getParent();

            if (null !== $communeObj) {
                $provinceObj = $communeObj->getParent();
            }
        }

        return [
            $provinceObj ? utf8_decode($provinceObj->getDenomination()) : null,
            $communeObj ? utf8_decode($communeObj->getDenomination()) : null,
            $collineObj ? utf8_decode($collineObj->getDenomination()) : null,
            $cooperativeObj ? utf8_decode($cooperativeObj->getDenomination()) : null,
            $groupementObj ? utf8_decode($groupementObj->getDenomination()) : null,
            utf8_decode((string) ($person->getNom())),
            utf8_decode((string) ($person->getPrenom())),
            utf8_decode((string) ($exploitation->getNumeroMembreForHuman())), ];
    }

    public function getcooperatives(int $numero, int $annee, TranslatorInterface $translator)
    {
        switch ($numero) {
            case 1:
                $titre = 'Répartition spatiale, nombre par commune et provinces';
                $fields['province'] = $translator->trans('province');
                $fields['commune'] = $translator->trans('commune');
                $fields['ocurrences'] = $translator->trans('ocurrences');
                $strSQL = 'SELECT t.province AS province,
						tda.denomination AS commune,
						Count(c.id) AS ocurrences
					  FROM tbl_organisations c
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						LEFT JOIN tbl_decoupage_admin tda ON c.decoupage_admin_id=tda.id
						LEFT JOIN (SELECT tda2.id AS id, tda2.denomination AS province
									FROM tbl_decoupage_admin tda2)t	ON tda.parent_id=t.id
					WHERE tol.level=1 AND t.province IS NOT NULL
					GROUP BY province, commune;';

                break;
            case 2: /* rien a voir */
                $titre = 'Montant total des cotisations , par coopérative [639]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields[''] = $translator->trans('');
                $strSQL = "SELECT DISTINCT (c.denomination) AS Cooperative,
						(SELECT COUNT(s.id)
						FROM suivi_organisations s
						WHERE c.id=s.organisation_id
							AND s.annee={$annee}
							AND s.no_reunions_ag=True) AS AG,
						(SELECT COUNT(s1.id)
						FROM suivi_organisations s1
						WHERE c.id=s1.organisation_id
							AND s1.annee={$annee}
							AND s1.no_reunions_ce=True) AS CE,
						(SELECT COUNT(s2.id)
							FROM suivi_organisations s2
							WHERE c.id=s2.organisation_id
								AND s2.annee={$annee}
								AND s2.no_reunions_cs=True) AS CS
					FROM tbl_organisations c
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
					WHERE tol.level=1";

                break;
            case 3:
                $titre = 'Existence de statuts [I1], Niveau de reconnaissance [I2] ; existence de ROI [I3]';
                $fields['Cooperative'] = $translator->trans('Cooperative');
                $fields['existence_status'] = $translator->trans('existence_status');
                $fields['niveau_reconnaissance'] = $translator->trans('niveau_reconnaissance');
                $fields['existence_roi'] = $translator->trans('existence_roi');
                $strSQL = "SELECT c.denomination AS Cooperative,
						if(s.existence_status=1,'oui', 'non') AS existence_status,
						tnr.niveau_reconnaissance,
						if(s.existence_roi=1, 'oui','non') AS existence_roi
					FROM tbl_organisations c
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN suivi_organisations s ON c.id=s.organisation_id
                        INNER JOIN niveau_de_reconnaissance nr ON nr.suivi_organisation_id=s.id
                        INNER JOIN tbl_niveaux_reconnaissance tnr ON nr.reconnaissance_id=tnr.id
					WHERE tol.level=1
						AND s.annee={$annee};";

                break;
            case 4:
                $titre = 'Existence de PV de réunion [II3], Rapport annuel d\'activités [II5]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['pv_reunions_ag'] = $translator->trans('pv_reunions_ag');
                $fields['pv_reunions_ce'] = $translator->trans('pv_reunions_ce');
                $fields['pv_reunions_cs'] = $translator->trans('pv_reunions_cs');
                $fields['rapport_annuel_activites'] = $translator->trans('rapport_annuel_activites');
                $strSQL = "SELECT DISTINCT (c.denomination) AS cooperative,
						if(s.pv_reunions_ag=1,'Oui', 'Non') AS pv_reunions_ag,
						if(s.pv_reunions_ce=1,'Oui', 'Non') AS pv_reunions_ce,
						if(s.pv_reunions_cs=1,'Oui', 'Non') AS pv_reunions_cs,
						if(s.rapport_annuel_activites=1,'Oui', 'Non') AS rapport_annuel_activites
					FROM tbl_organisations c
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN suivi_organisations s ON c.id=s.organisation_id
					WHERE tol.level=1
						AND s.annee={$annee};";

                break;
            case 5:
                $titre = 'L\'enregistrement des cotisations [III4], le classement des justificatifs [III8],
                    l\'existence de rapport financier annuel [III9]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['enregistrement_cotisations'] = $translator->trans('enregistrement_cotisations');
                $fields['classement_justificatifs'] = $translator->trans('classement_justificatifs');
                $fields['rapports_financiere_annuel'] = $translator->trans('rapports_financiere_annuel');
                $strSQL = "SELECT DISTINCT (c.denomination) AS cooperative,
						if(s.enregistrement_cotisations=1,'Oui', 'Non') AS enregistrement_cotisations,
						if(s.classement_justificatifs=1,'Oui', 'Non') AS classement_justificatifs,
						if(s.rapports_financiere_annuel=1,'Oui', 'Non') AS rapports_financiere_annuel
					FROM tbl_organisations c
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN suivi_organisations s ON c.id=s.organisation_id
					WHERE tol.level=1
						AND s.annee={$annee}
					ORDER BY Cooperative;";

                break;
            case 6:
                $titre = 'Nombre de MUSO/coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['muso'] = $translator->trans('muso');
                $strSQL = "SELECT DISTINCT (c.denomination) AS cooperative,
						  s.muso
					 FROM tbl_organisations c
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN suivi_organisations s ON c.id=s.organisation_id
					WHERE tol.level=1
						AND s.annee={$annee}
					ORDER BY Cooperative;";

                break;
            case 7:
                $titre = 'Pourcentage des membres ayant contracté un crédit [V1]/membre total membres ;
 montant total des crédits contractés [V1]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['total_membres'] = $translator->trans('total_membres');
                $fields['membres_contracte_credit'] = $translator->trans('membres_contracte_credit');
                $fields['membres_credit_montant'] = $translator->trans('membres_credit_montant');
                $Rapport_membres = 'SELECT tbl_cooperatives.Code_cooperative,
                                            Count(exploitations.ID_exploitation) AS membres
					  FROM tbl_organisations c
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
                                                , tbl_groupement, exploitations
					  WHERE tbl_cooperatives.Code_cooperative = tbl_groupement.Code_cooperative
						  AND tbl_groupement.Code_groupement = exploitations.Code_groupement
					  GROUP BY tbl_cooperatives.Code_cooperative;';

                $strSQL = "SELECT DISTINCT (tc.denomination) AS cooperative,
						t.total_membres,
						s.membres_contracte_credit,
						s.membres_credit_montant
					FROM tbl_organisations tc
						INNER JOIN tbl_organisation_level tol ON tc.level_id=tol.id
						INNER JOIN suivi_organisations s ON tc.id=s.organisation_id
						LEFT JOIN (SELECT c.id AS ident,
						 			Count(e.id) AS total_membres
					  			 FROM tbl_organisations c
									INNER JOIN tbl_organisations g ON c.id = g.parent_id
									INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									INNER JOIN exploitations e ON p.exploitation_id = e.id
									INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						  	     WHERE tol.level=1
					  			 GROUP BY ident)t ON tc.id=t.ident
					WHERE tol.level=1
						AND s.annee={$annee};";

                break;
            case 8:
                $titre = 'Part des prêts octroyés par la coopérative [V2] sur le montant total [V1]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['membres_credit_montant'] = $translator->trans('membres_credit_montant');
                $fields['montant_financement'] = $translator->trans('montant_financement');
                $fields['Pourcentage'] = $translator->trans('Pourcentage');
                $strSQL = "SELECT DISTINCT (c.denomination) AS cooperative,
                                    s.membres_credit_montant,
                                    s.montant_financement,
                                    Round((s.montant_financement/s.membres_credit_montant)*100,2) AS Pourcentage
                                FROM tbl_organisations c
                                     INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
                                     INNER JOIN suivi_organisations s ON c.id=s.organisation_id
                                WHERE tol.level=1
                                    AND s.annee={$annee};";

                break;
            case 9:
                $titre = 'Fréquence des institutions ayant octroyé; le crédit, pour l\'ensemble des coopératives';
                $fields['institution'] = $translator->trans('institution');
                $fields['frequence'] = $translator->trans('frequence');
                $strSQL = "SELECT tif.institution,
                                    Count(s.id) AS frequence
				FROM tbl_institutions_financieres tif, suivi_organisations s
				WHERE tif.id = s.institution_financement_credit_org_id
					AND s.annee={$annee}
					GROUP BY tif.institution;";

                break;
        }
        /* execution requête et envoi de l'array  */
        $em = $this->getDoctrine()->getManager();
        $stmt = $em->getConnection()->prepare($strSQL);
        $stmt->execute();
        $recordset = $stmt->fetchAll();

        return $result = [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $recordset,
        ];
    }

    public function getcooperatives10(int $numero, int $annee, TranslatorInterface $translator)
    {
        $titre1 = 'Fréquence des besoins en formation [VII1]';
        $titre2 = 'Frequence de demandes en appui institutionnel [VII2]';
        $titre3 = 'Frequence de demandes en service [VII3]. ';
        $fields1['Theme_formation'] = $translator->trans('Theme_formation');
        $fields1['ocurrences'] = $translator->trans('ocurrences');
        $strSQL1 = "SELECT ttf.Theme_formation, COUNT( bf.id) AS ocurrences
			  			FROM suivi_organisations s
			  				INNER JOIN besoins_formations bf ON s.id=bf.suivi_organisation_id
							LEFT JOIN tbl_themes_formations ttf ON bf.formation_id=ttf.id
			  			WHERE s.annee ={$annee}
			  			GROUP BY Theme_formation;";

        $fields2['theme_institutionel'] = $translator->trans('theme_institutionel');
        $fields2['FrequenceInstitutionel'] = $translator->trans('FrequenceInstitutionel');
        $strSQL2 = "SELECT ttf.theme_formation AS theme_institutionel, COUNT(bi.id) AS FrequenceInstitutionel
			  			FROM suivi_organisations s
			  				INNER JOIN besoins_institutionel bi ON s.id=bi.suivi_organisation_id
			  				LEFT JOIN tbl_themes_formations ttf ON bi.institutionel_id=ttf.id
			  			WHERE s.annee={$annee}
			  			GROUP BY theme_formation;";
        $fields3['service'] = $translator->trans('service');
        $fields3['ocurrences'] = $translator->trans('ocurrences');
        $strSQL3 = "SELECT ts.service AS service, COUNT( bsm.id ) AS ocurrences
					  FROM suivi_organisations s
					  	INNER JOIN `besoin_service_aux_membres` bsm ON s.id = bsm.suivi_organisation_id
					  	LEFT JOIN tbl_services_aux_membres ts ON bsm.service_id = ts.id
					  WHERE s.annee={$annee}
					  GROUP BY service";
        /*------------------------------------*/
        $em = $this->getDoctrine()->getManager();
        $stmt1 = $em->getConnection()->prepare($strSQL1);
        $stmt1->execute();
        $recordset1 = $stmt1->fetchAll();

        $em = $this->getDoctrine()->getManager();
        $stmt2 = $em->getConnection()->prepare($strSQL2);
        $stmt2->execute();
        $recordset2 = $stmt2->fetchAll();

        $em = $this->getDoctrine()->getManager();
        $stmt3 = $em->getConnection()->prepare($strSQL3);
        $stmt3->execute();
        $recordset3 = $stmt3->fetchAll();

        return $result = [
            'fields1' => $fields1,
            'titre1' => $titre1,
            'recordset1' => $recordset1,
            'fields2' => $fields2,
            'titre2' => $titre2,
            'recordset2' => $recordset2,
            'fields3' => $fields3,
            'titre3' => $titre3,
            'recordset3' => $recordset3,
        ];
    }

    /* ------------------ groupements ---------------------------*/

    /**
     * @Route("/rapports/groupements", name="rapports_groupements")
     */
    public function groupementsAction(Request $request)
    {
        return $this->render('rapports/groupements.html.twig', [
            'params' => 'mes paramètres pour les rapports des coopératives',
        ]);
    }

    /**
     * @Route("/rapports/groupnum", name="groupnum")
     */
    public function groupnumAction(Request $request, TranslatorInterface $translator, EntityManagerInterface $em)
    {
        $numero = (int) ($request->get('num'));
        $annee = (int) ($request->get('annee'));
        $result = $this->getgroupements($translator, $em, $numero, $annee);

        return $this->render('rapports/rec_groupements.html.twig', [
            'recordset' => $result['recordset'],
            'fields' => $result['fields'],
            'titre' => $result['titre'],
        ]);
    }

    /* ---------------------- planification ------------------*/

    /**
     * @Route("/rapports/planification", name="rapports_planification")
     */
    public function planificationAction(Request $request)
    {
        return $this->render('rapports/planification.html.twig');
    }

    /* ---------------- planification/number ---------------------*/

    /**
     * @Route("/rapports/plannum")
     */
    public function plannumAction(Request $request, TranslatorInterface $translator, EntityManagerInterface $em)
    {
        $numero = $request->get('num');
        $annee = $request->get('annee');
        //echo "avant: $annee";
        $result = $this->getPlanification($translator, $em, $numero, $annee);

        return $this->render('rapports/plannum.html.twig', [
            'recordset' => $result['recordset'],
            'fields' => $result['fields'],
            'titre' => $result['titre'],
        ]);
    }

    /**
     * Req 1.
     *
     * @Route("/rapports/req1", name="rapports_req1")
     */
    public function rapportReq1(EntityManagerInterface $em)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '150');

        $query = $em->createQuery(
            'SELECT p
            FROM App:Personnes p
            INNER JOIN p.exploitation e
            WHERE p.repondant = TRUE
            AND e.radie = False
            AND p.radie = False'
        );

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/csv');
        $filename = 'export_req1_' . date('Y-m-d_h-i-s') . '.csv';
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

        $that = $this; // todo fixme pas beau
        $response->setCallback(static function () use ($query, $em, $that) {
            $personnes = $query->getResult();

            $outputBuffer = fopen('php://output', 'wb');
            $csv_delimiter = ';';
            fputcsv(
                $outputBuffer,
                array_merge(
                    $that->get_first_colums_for_reqX(),
                    [
                        'Sexe', utf8_decode('Année naissance'),
                        utf8_decode('État civil'),
                        utf8_decode('Nombre personne ménage'),
                        utf8_decode('Niveau d\'études secondaire'),
                    ]
                ),
                $csv_delimiter
            );

            foreach ($personnes as $person) {
                $exploitation = $person->getExploitation();
                $persDet = $em->getRepository(PersonneDetails::class)
                    ->findOneBy(
                        ['personnes' => $person->id]
                    );

                fputcsv(
                    $outputBuffer,
                    array_merge(
                        $that->get_first_colums_for_reqX(null, $person),
                        [
                            utf8_decode((string) ($person->getSexe())),
                            $person->getDateNaissance() ? $person->getDateNaissance()->format('Y') : null,
                            utf8_decode((string) ($person->getEtatCivil())),
                            $exploitation->getTotalMenage(),
                            null !== $persDet ? $persDet->getSecondaire() : null,
                        ]
                    ),
                    $csv_delimiter
                );
            }
            fclose($outputBuffer);
        });

        return $response;
    }

    /**
     * Req 2.
     *
     * @Route("/rapports/req2/{year}", name="rapports_req2")
     */
    public function rapportReq2(Request $request, int $year)
    {
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '150');

        $em = $this->getDoctrine()->getManager();
        $query = $em->createQuery(
            'SELECT p
            FROM App:Personnes p
            INNER JOIN p.exploitation e
            WHERE p.repondant = TRUE
            AND e.radie = False
            AND p.radie = False'
        );

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/csv');
        $filename = 'export_req2_' . $year . '_' . date('Y-m-d_h-i-s') . '.csv';
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

        $that = $this;
        $response->setCallback(static function () use ($query, $year, $that) {
            $personnes = $query->getResult();

            $outputBuffer = fopen('php://output', 'wb');
            $csv_delimiter = ';';
            fputcsv(
                $outputBuffer,
                [
                    'Province', 'Commune', 'Colline',
                    utf8_decode('Coopérative'), 'Groupement',
                    'Nom', utf8_decode('Prénom'),
                    utf8_decode('Numéro de membre'),
                    'Membre de la famille',
                    'Nature du travail',
                    'Lieu',
                    'Revenu annuel (FBu)', ],
                $csv_delimiter
            );

            foreach ($personnes as $personne) {
                $exploitation = $personne->getExploitation();

                $mo_familiale_externes = $exploitation->getMo_familiale_externes();
                $mo_familiale_externes_annee = [];

                foreach ($mo_familiale_externes as $mo) {
                    if ($mo->getAnnee() === $year) {
                        $mo_familiale_externes_annee[] = $mo;
                    }
                }

                if (count($mo_familiale_externes_annee) > 0) {
                    foreach ($mo_familiale_externes_annee as $mo) {
                        fputcsv(
                            $outputBuffer,
                            array_merge(
                                $that->get_first_colums_for_reqX(null, $personne),
                                [
                                    utf8_decode($mo->getMembres()),
                                    $mo->getTypeTravail() ? utf8_decode($mo->getTypeTravail()->getProfession()) : null,
                                    utf8_decode($mo->getLieu()),
                                    $mo->getRevenu(),
                                ]
                            ),
                            $csv_delimiter
                        );
                    }
                } else {
                    fputcsv(
                        $outputBuffer,
                        $that->get_first_colums_for_reqX(null, $personne),
                        $csv_delimiter
                    );
                }
            }
            fclose($outputBuffer);
        });

        return $response;
    }

    /**
     * Req 3.
     *
     * @Route("/rapports/req3/{year}", name="rapports_req3")
     */
    public function rapportReq3(Request $request, int $year)
    {
        return $this->genericRapportReq3Req4($request, $year, false); // agricole
    }

    /**
     * Req 4.
     *
     * @Route("/rapports/req4/{year}", name="rapports_req4")
     */
    public function rapportReq4(Request $request, int $year)
    {
        return $this->genericRapportReq3Req4($request, $year, true); // elevage
    }

    /**
     * Req 5. generic.
     *
     * @Route("/rapports/req5/{type}/{year}/{saison}", name="rapports_req5")
     */
    public function rapportReq5(Request $request, string $type, int $year, TblSaisons $saison)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        if ('engrais' === $type) {
            $entityClass = 'App:ExploitationCommandeEngrais';
        } elseif ('semences' === $type) {
            $entityClass = 'App:ExploitationCommandeSemences';
        } else {
            $entityClass = 'App:ExploitationCommandePhytosanitaires';
        }

        $qb
            ->select('cmd')
            ->from($entityClass, 'cmd')
            ->innerJoin('cmd.exploitationPlanSaison', 'eps')
            ->innerJoin('eps.collecte', 'col')
            ->innerJoin('col.exercice', 'exer') // annee
            ->innerJoin('eps.saisonPlan', 'sais') // saison
            ->innerJoin('eps.exploitation', 'exp')
            ->innerJoin('exp.personnes', 'pers')
            ->where('pers.repondant = True')
            ->andWhere('exer.annee = :year')
            ->andWhere('sais = :saison')
            ->andWhere('exp.radie = False')
            ->andWhere('pers.radie = False')
            ->orderBy('exp.numeroMembre')
            ->setParameter('year', $year)
            ->setParameter('saison', $saison);

        $query = $qb->getQuery();
        $reqName = 'req5';

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/csv');
        $filename = sprintf(
            'export_%s_%s_%s_s%s_%s.csv',
            $reqName,
            $type,
            $year,
            $saison->getId(),
            date('Y-m-d_h-i-s')
        );
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

        $that = $this; // fixme later todo / pas beau
        $response->setCallback(static function () use ($query, $that, $entityClass) {
            $commandeEngrais = $query->getResult();
            $outputBuffer = fopen('php://output', 'wb');
            $csv_delimiter = ';';

            if ('App:ExploitationCommandeSemences' === $entityClass) {
                $customTitles = ['Variétés', 'Qualité', 'Quantité'];
            } else {
                $customTitles = ['Produit', 'Quantité'];
            }

            fputcsv(
                $outputBuffer,
                array_merge(
                    $that->get_first_colums_for_reqX(),
                    $customTitles
                ),
                $csv_delimiter
            );

            foreach ($commandeEngrais as $cmd) {
                $exploitation = $cmd->getExploitationPlanSaison()->getExploitation();

                if ('App:ExploitationCommandeEngrais' === $entityClass) {
                    $customDatas = [
                        utf8_decode((string) ($cmd->getEngrais())),
                        utf8_decode((string) ($cmd->getQuantite())),
                    ];
                } elseif ('App:ExploitationCommandeSemences' === $entityClass) {
                    $customDatas = [
                        utf8_decode((string) ($cmd->getVariete())),
                        utf8_decode((string) ($cmd->getQualiteSemence())),
                        utf8_decode((string) ($cmd->getQuantite())),
                    ];
                } else {  // $entityClass == "App:ExploitationCommandePhytosanitaires"
                    $customDatas = [
                        utf8_decode((string) ($cmd->getPhytosanitaire())),
                        utf8_decode((string) ($cmd->getQuantite())),
                    ];
                }

                fputcsv(
                    $outputBuffer,
                    array_merge(
                        $that->get_first_colums_for_reqX($exploitation),
                        $customDatas
                    ),
                    $csv_delimiter
                );
            }
            fclose($outputBuffer);
        });

        return $response;
    }

    /**
     * Req 5. engrais.
     *
     * @Route("/rapports/req5_engrais/{year}/{saison}", name="rapports_req5_engrais")
     */
    public function rapportReq5Engrais(Request $request, int $year, TblSaisons $saison)
    {
        return $this->redirectToRoute(
            'rapports_req5',
            [
                'type' => 'engrais',
                'year' => $year,
                'saison' => $saison->getId(), ]
        );
    }

    /**
     * Req 5. phyto.
     *
     * @Route("/rapports/req5_phyto/{year}/{saison}", name="rapports_req5_phyto")
     */
    public function rapportReq5Phyto(Request $request, int $year, TblSaisons $saison)
    {
        return $this->redirectToRoute(
            'rapports_req5',
            [
                'type' => 'phyto',
                'year' => $year,
                'saison' => $saison->getId(), ]
        );
    }

    /**
     * Req 5. semences.
     *
     * @Route("/rapports/req5_semences/{year}/{saison}", name="rapports_req5_semences")
     */
    public function rapportReq5Semences(Request $request, int $year, TblSaisons $saison)
    {
        return $this->redirectToRoute(
            'rapports_req5',
            [
                'type' => 'semences',
                'year' => $year,
                'saison' => $saison->getId(), ]
        );
    }

    /**
     * Req 6. Répartition des dépenses en intrants d’élevage.
     *
     * @Route("/rapports/req6_intrants_elevage/{year}/", name="rapports_req6_intrants_elevage")
     */
    public function rapportReq6IntrantsElevage(Request $request, int $year)
    {
        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $qb
            ->select('SUM(charge.valeur) AS sum_valeur, SUM(charge.quantite) AS sum_quantite, cat.achat AS produit, exp.id AS exp_id, exp.numeroMembre AS exp_nbr')
            ->from('App:ExploitationCharges', 'charge')
            ->innerJoin('charge.achat', 'cat')
            ->innerJoin('charge.exploitation', 'exp')
            ->innerJoin('exp.personnes', 'pers')

            ->where('charge.annee = :year')
            ->andWhere('charge.radie = False')
            ->andWhere('cat.typeAchat = :elevage')
            ->andwhere('pers.repondant = True')
            ->andWhere('exp.radie = False')
            ->andWhere('pers.radie = False')

            ->groupBy('cat.achat, exp.id, exp.numeroMembre')

            ->orderBy('exp.numeroMembre')
            ->setParameter('year', $year)
            ->setParameter('elevage', 'elevage');

        $query = $qb->getQuery();

        $response = new StreamedResponse();

        $response->headers->set('Content-Type', 'text/csv');
        $filename = 'export_req6_' . $year . '_' . date('Y-m-d_h-i-s') . '.csv';
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

        $that = $this;

        $response->setCallback(static function () use ($query, $that, $em) {
            $rows = $query->getResult();

            $outputBuffer = fopen('php://output', 'wb');
            $csv_delimiter = ';';

            fputcsv(
                $outputBuffer,
                array_merge(
                    $that->get_first_colums_for_reqX(),
                    ['Produit', 'Quantité (somme)', 'Valeur (somme)',
                    ]
                ),
                $csv_delimiter
            );

            foreach ($rows as $row) {
                $exp = $em->getRepository(Exploitations::class)
                    ->findOneBy(['id' => $row['exp_id']]);

                fputcsv(
                    $outputBuffer,
                    array_merge(
                        $that->get_first_colums_for_reqX($exp),
                        [
                            $row['produit'],
                            $row['sum_quantite'],
                            $row['sum_valeur'], ]
                    ),
                    $csv_delimiter
                );
            }
            fclose($outputBuffer);
        });

        return $response;
    }

    /**
     * @Route("/rapports", name="rapports")
     */
    public function rapportsAction()
    {
        return $this->render('rapports/rapports.html.twig');
    }

    /* ---------------------- export des donnes --------------*/

    /**
     * @Route("/rapports/export/data", name="rapports_export_data")
     */
    public function rexportExportDataAction(Request $request)
    {
        return $this->render('rapports/submenu_export.html.twig');
    }

    /* -------------------------- specialF0 --------------------*/

    /**
     * @Route("/rapports/specialF0", name="rapports_specialF0")
     */
    public function specialF0Action(
        TranslatorInterface $translator,
        EntityManagerInterface $em,
        Request $request
    ) {
        $SpecialF0 = new RapportsSpecialF0();

        $form = $this->createForm('App\Form\RapportsSpecialF0Type', $SpecialF0, [
            //      'nb_annees_passees' => $this->container->getParameter('annee_passees')
            'nb_annees_passees' => 10,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $result = $this->getspecialF0($translator, $em);

            return $this->render('rapports/rec_specialF0.html.twig', [
                'recordset' => $result['recordset'],
                'fields' => $result['fields'],
                'titre' => $result['titre'],
            ]);
        }

        return $this->render('rapports/specialF0.html.twig', [
            'specialF0' => $SpecialF0,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Liste des répondants et co-exploitants avec sexe et age et groupement.
     */
    private function evalListExpSexeAge()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $query = $entityManager->createQuery(
            'SELECT grp.denomination as groupement,
                    e.numeroMembre,
                    p.nom,
                    p.prenom,
                    p.sexe,
                    (DATE_DIFF(CURRENT_DATE(), p.dateNaissance) / 365) AS age
            FROM App\Entity\TblOrganisations grp
            INNER JOIN grp.level lvl
            INNER JOIN grp.pivots pivot
            INNER JOIN pivot.exploitation e
            INNER JOIN e.personnes p
            WHERE lvl.level = 2
            AND e.radie = False
            AND p.radie = False'
        );

        $result = $query->getResult();
        $resultSize = count($result);

        for ($i = 0; $i < $resultSize; ++$i) {
            $result[$i]['age'] = (int) ($result[$i]['age']);
        }

        return [
            'fields' => ['groupement', 'numeroMembre', 'nom', 'prenom', 'sexe', 'age'],
            'titre' => 'Liste des co-exploitants (avec sexe et age et groupemennt)',
            'recordset' => $result,
        ];
    }

    /**
     * Liste des répondants avec sexe, age, groupement et coop.
     */
    private function evalListRepondantSexeAge()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $query = $entityManager->createQuery(
            'SELECT coop.denomination as cooperative, gpment.denomination as groupement, e.numeroMembre, p.nom, p.prenom, p.sexe, (DATE_DIFF(CURRENT_DATE(), p.dateNaissance) / 365) AS age
            FROM App\Entity\TblOrganisations coop
            INNER JOIN coop.level lvl
            INNER JOIN coop.children gpment
            INNER JOIN gpment.pivots pivot
            INNER JOIN pivot.exploitation e
            INNER JOIN e.personnes p
            WHERE lvl.level = 1
            AND p.repondant = TRUE
            AND e.radie = False
            AND p.radie = False'
        );

        $result = $query->getResult();
        $resultSize = count($result);

        for ($i = 0; $i < $resultSize; ++$i) {
            $result[$i]['age'] = (int) ($result[$i]['age']);
        }

        return [
            'fields' => ['cooperative', 'groupement', 'numeroMembre', 'nom', 'prenom', 'sexe', 'age'],
            'titre' => 'Liste des répondants (avec sexe et age)',
            'recordset' => $result,
        ];
    }

    /**
     * Calcul du nombre d'exploitations appliquant des techiques argo-écologiques
     * pour l'année $annee.
     */
    private function evalNbExpAgroEco(int $annee)
    {
        // TODO FIXME regrouper par coop ?
        $entityManager = $this->getDoctrine()->getManager();

        $query = $entityManager->createQuery(
            'SELECT count(e) as count
            FROM App\Entity\Exploitations e
            INNER JOIN e.parcelles p
            INNER JOIN p.parcelleHistoriques h
            INNER JOIN h.operations o
            INNER JOIN o.type t
            WHERE e.radie = False
            AND p.radie = False
            AND h.radie = False
            AND h.annee = :annee
            AND t.agroEco = True
            GROUP BY e.numeroMembre'
        );

        $query->setParameter('annee', $annee);
        $result = $query->getResult();

        return [
            'fields' => ['count'],
            'titre' => "Nombre d'exploitations appliquant des techiques argo-écologiques",
            'recordset' => $result,
        ];
    }

    /**
     * Exploitations sans répondant.
     *
     * TODO METTRE AILLEURS = ici quality
     */
    private function evalQualityNoRepondant()
    {
        $entityManager = $this->getDoctrine()->getManager();

        $query = $entityManager->createQuery(
            'SELECT
                coop.denomination as nomCooperative,
                grp.denomination as nomGroupement,
                exp.id as idExploitation,
                COUNT(p) as exp_rep
            FROM App\\Entity\\Exploitations exp
            JOIN exp.pivots pivot
            JOIN pivot.organisation grp
            JOIN grp.level grp_lvl
            JOIN grp.parent coop
            LEFT JOIN exp.personnes p WITH p.repondant=TRUE
            WHERE grp_lvl.level = 2
            AND grp.radie = FALSE
            AND exp.radie = FALSE
            GROUP BY exp.id
            HAVING COUNT(p) = 0'
        );

        $result = $query->getResult();
        $resultSize = count($result);

        for ($i = 0; $i < $resultSize; ++$i) {
            $url = $this->generateUrl('saisie_info_gen_sans_annee', [
                'exploitation' => $result[$i]['idExploitation'],
            ]);
            $result[$i]['link_raw'] = '<a href="' . $url . '">' . $result[$i]['idExploitation'] . '</a>';
        }

        return [
            'fields' => ['nomCooperative', 'nomGroupement', 'idExploitation', 'link_raw'],
            'titre' => 'Exploitations sans répondant',
            'recordset' => $result,
        ];
    }

    private function genericRapportReq3Req4(Request $request, int $year, bool $elevage)
    {
        $reqName = $elevage ? 'req4' : 'req3';

        $em = $this->getDoctrine()->getManager();
        $qb = $em->createQueryBuilder();

        $qb
            ->select('cp')
            ->from('App:CommercialisationProduits', 'cp')
            ->innerJoin('cp.exploitation', 'e')
            ->innerJoin('e.personnes', 'pers')
            ->innerJoin('cp.produit', 'prod')
            ->innerJoin('prod.culture', 'cult')
            ->where('pers.repondant = True')
            ->andWhere('cp.annee = :annee')
            ->andWhere('e.radie = False')
            ->andWhere('pers.radie = False')
            ->andWhere('cult.elevage = :elevage')
            ->orderBy('e.numeroMembre')
            ->setParameter('annee', $year)
            ->setParameter('elevage', $elevage);

        $query = $qb->getQuery();

        $response = new StreamedResponse();
        $response->headers->set('Content-Type', 'text/csv');
        $filename = 'export_' . $reqName . '_' . $year . '_' . date('Y-m-d_h-i-s') . '.csv';
        $response->headers->set('Content-Disposition', 'attachment; filename="' . $filename . '"');

        $that = $this; // fixme later todo / pas beau
        $response->setCallback(static function () use ($query, $that) {
            $commercialisationProduits = $query->getResult();
            $outputBuffer = fopen('php://output', 'wb');
            $csv_delimiter = ';';
            fputcsv(
                $outputBuffer,
                array_merge(
                    $that->get_first_colums_for_reqX(),
                    ['Produit', 'Prix de vente',
                    ]
                ),
                $csv_delimiter
            );

            foreach ($commercialisationProduits as $cp) {
                $exploitation = $cp->getExploitation();

                fputcsv(
                    $outputBuffer,
                    array_merge(
                        $that->get_first_colums_for_reqX($exploitation),
                        [
                            utf8_decode((string) ($cp->getProduit())),
                            utf8_decode((string) ($cp->getPrixVente())),
                        ]
                    ),
                    $csv_delimiter
                );
            }
            fclose($outputBuffer);
        });

        return $response;
    }

    private function getAllSignaletique(TranslatorInterface $translator, EntityManagerInterface $em, $form): array
    {
        /* ok */
        /* construction de la requete  */
        $parametros = $_POST['rapports_all_signaletique'];
        $cooperative_id = $parametros['Denomination'];

        $strWhere = empty($cooperative_id) ? '' : " AND c.id='" . $cooperative_id . "'";

        $titre = 'Rapport sur la signaletique complet';

        $strSql = "SELECT c.denomination AS cooperative,
					g.denomination AS groupement,
					e.numero_membre,
					pers.nom,
					pers.prenom,
					e.annee_adhesion,
					e.membres_menage,
					e.personnes_charge,
					tec.etat_civil,
					e.nombre_fils,
					e.nombre_filles,
					e.fils_scolarises,
					e.filles_scolarisess AS filles_scolarisees,
					pers.rol_famille,
					pers.sexe,
					pers.date_naissance
			FROM tbl_organisations c
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
						INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						INNER JOIN exploitations e ON p.exploitation_id = e.id
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN personnes pers ON pers.exploitation_id = e.id
						INNER JOIN tbl_etat_civil tec ON pers.etat_civil_id = tec.id
			WHERE tol.level=1
			  AND pers.rol_famille='" . Personnes::role_chefDeFamille . "'
			  {$strWhere}
			ORDER BY cooperative, groupement, e.numero_membre";

        /* construction de champs à afficher */
        $fields['cooperative'] = $translator->trans('cooperative');
        $fields['groupement'] = $translator->trans('groupement');
        $fields['numero_membre'] = $translator->trans('numero_membre');
        $fields['nom'] = $translator->trans('nom');
        $fields['prenom'] = $translator->trans('prenom');
        $fields['annee_adhesion'] = $translator->trans('annee_adhesion');
        $fields['membres_menage'] = $translator->trans('membres_menage');
        $fields['personnes_charge'] = $translator->trans('personnes_charge');
        $fields['etat_civil'] = $translator->trans('etat_civil');
        $fields['rol_famille'] = $translator->trans('rol_famille');
        $fields['sexe'] = $translator->trans('sexe');
        $fields['date_naissance'] = $translator->trans('date_naissance');
        $fields['personnes_charge'] = $translator->trans('personnes_charge');
        $fields['nombre_fils'] = $translator->trans('nombre_fils');
        $fields['nombre_filles'] = $translator->trans('nombre_filles');
        $fields['fils_scolarises'] = $translator->trans('fils_scolarises');
        $fields['filles_scolarisees'] = $translator->trans('filles_scolarisees');

        /* execution requête et envoi de l'array  */
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
        $recordset = $stmt->fetchAll();

        return $result = [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $recordset,
        ];
    }

    private function getEvaluation(TranslatorInterface $translator, EntityManagerInterface $em, string $numero, $annee = null)
    {
        $titre = 'Sans titre';
        $fields = [];
        /* from à partir de les coopératives jusqu'àux exploitations */

        $from_exploitations = 'FROM tbl_organisations c
			INNER JOIN tbl_organisations g ON c.id = g.parent_id
			INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
			INNER JOIN exploitations e ON p.exploitation_id = e.id
			INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id';

        switch ($numero) {
            case 1: /*  ok  */
                $titre = 'Nombre de membres et de groupement par coopérative (021, 022)';
                $fields['denomination'] = $translator->trans('denomination');
                $fields['membres'] = $translator->trans('membres');
                $fields['groupements'] = $translator->trans('groupements');
                $sql = 'SELECT DISTINCT(pto.denomination), pf.membres, ph.groupements
					  FROM tbl_organisations pto
						  INNER JOIN
								 (SELECT c.denomination, count( e.id ) AS membres
								  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  WHERE g.parent_id = c.id
								  GROUP BY c.denomination)pf ON pto.denomination=pf.denomination
						  INNER JOIN (
							  SELECT c.denomination, count( g.id ) AS groupements
							  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
							  WHERE g.parent_id = c.id GROUP BY c.denomination)ph ON pto.denomination=ph.denomination
							  INNER JOIN tbl_organisation_level tol ON pto.level_id=tol.id WHERE tol.level=1';

                break;
            case 2: /* ok mais il manque l'annee des donnees */
                $titre = 'Résultat:Nombre et pourcentage par coopérative d’hommes et de femmes et de jeunes (<35 ans) (031)';
                $fields['denomination'] = $translator->trans('denomination');
                $fields['hommes'] = $translator->trans('hommes');
                $fields['femmes'] = $translator->trans('femmes');
                $fields['jeunes'] = $translator->trans('jeunes');
        //      $fields['total'] = $translator->trans('total');
        //      $fields['pas_defini'] = $translator->trans('pas_defini');
                $sql = "SELECT * FROM tbl_organisations JOIN ( SELECT coop_id,
                	count(pers_id) AS hommes
                	FROM base_bis
                    WHERE sexe='Masculin' GROUP BY coop_id)AS hommes ON tbl_organisations.id=hommes.coop_id
                    JOIN ( SELECT coop_id,
                	count(pers_id) AS femmes
                	FROM base_bis
                    WHERE sexe='Féminin' GROUP BY coop_id)AS femmes ON tbl_organisations.id=femmes.coop_id
                    JOIN ( SELECT coop_id,
                    	count(pers_id) AS jeunes
                	FROM base_bis
                    WHERE date_naissance>YEAR(DATE_SUB(Now(),INTERVAL 35 YEAR)) GROUP BY coop_id)AS jeunes ON tbl_organisations.id=jeunes.coop_id;";

                break;
            case 3: /*   ok   */
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['Moyenne_fils'] = $translator->trans('Moyenne_fils');
                $fields['Moyenne_filles'] = $translator->trans('Moyenne_filles');
                $fields['Moyenne_fils_scolarises'] = $translator->trans('Moyenne_fils_scolarises');
                $fields['Moyenne_filles_scolarisees'] = $translator->trans('Moyenne_filles_scolarisees');

                $sql = 'SELECT c.denomination as cooperative,
					  	Round(Avg(e.Nombre_fils),2) AS Moyenne_fils,
						Round(Avg(e.Nombre_filles),2) AS Moyenne_filles,
						Round(Avg(e.Fils_scolarises),2) AS Moyenne_fils_scolarises,
						Round(Avg(e.Filles_scolarisess),2) AS Moyenne_filles_scolarisees
					  ' . $from_exploitations . '
					  WHERE tol.level=1 AND e.radie=0
					  GROUP BY cooperative';

                break;
            case 4:
                $reqAnnee = isset($annee) ? "AND pd.annee='" . $annee . "'" : '';
                $titre = 'Taux moyen d’alphabétisation par sexe pour les coopératives (051, 052)';
                $fields['denomination'] = $translator->trans('denomination');
                $fields['total'] = $translator->trans('total');
                $fields['kirundi_lire_masc'] = $translator->trans('kirundi_lire_masc');
                $fields['kirundi_lire_fem'] = $translator->trans('kirundi_lire_fem');
                $fields['kirundi_ecrire_masc'] = $translator->trans('kirundi_ecrire_masc');
                $fields['kirundi_ecrire_fem'] = $translator->trans('kirundi_ecrire_fem');
                $fields['francais_lire_masc'] = $translator->trans('francais_lire_masc');
                $fields['francais_lire_fem'] = $translator->trans('francais_lire_fem');
                $fields['francais_ecrire_masc'] = $translator->trans('francais_ecrire_masc');
                $fields['francais_ecrire_fem'] = $translator->trans('francais_ecrire_fem');
                $sql = "SELECT DISTINCT(pto.denomination),
							pf.total,
							ph.kirundi_lire_masc,
							sf.kirundi_lire_fem,
							sh.kirundi_ecrire_masc,
							supf.kirundi_ecrire_fem,
							suph.francais_lire_masc,
							flf.francais_lire_fem,
							fem.francais_ecrire_masc,
							fef.francais_ecrire_fem
					  FROM tbl_organisations pto
						  INNER JOIN
								 (SELECT c.denomination, count( pd.id ) AS total
								  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  WHERE g.parent_id = c.id  {$reqAnnee}
								  GROUP BY c.denomination)pf ON pto.denomination=pf.denomination

						  INNER JOIN (
							  SELECT c.denomination, count( pd.id ) AS kirundi_lire_masc
							  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
							  WHERE g.parent_id = c.id AND pers.sexe='Masculin'
							  	AND pd.kirundi_lire=1 {$reqAnnee}
								GROUP BY c.denomination)ph ON pto.denomination=ph.denomination

							  INNER JOIN (
								  SELECT c.denomination, count( pd.id ) AS kirundi_lire_fem
								  FROM tbl_organisations c
									  INNER JOIN tbl_organisations g ON c.id = g.parent_id
									  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									  INNER JOIN exploitations e ON p.exploitation_id = e.id
									  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									  INNER JOIN personnes pers ON pers.exploitation_id = e.id
									  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  WHERE g.parent_id = c.id AND pers.nom <> '' AND pers.sexe='Féminin'
								  AND pd.kirundi_lire=1 {$reqAnnee}
								  GROUP BY c.denomination)sf ON pto.denomination=sf.denomination

							  INNER JOIN (
								  SELECT c.denomination, count( pd.id ) AS kirundi_ecrire_masc
								  FROM tbl_organisations c
									  INNER JOIN tbl_organisations g ON c.id = g.parent_id
									  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									  INNER JOIN exploitations e ON p.exploitation_id = e.id
									  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									  INNER JOIN personnes pers ON pers.exploitation_id = e.id
									  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  WHERE g.parent_id = c.id AND pers.nom <> '' AND pers.sexe='Masculin'
								  AND pd.kirundi_ecrire=1 {$reqAnnee}
								  GROUP BY c.denomination)sh ON pto.denomination=sh.denomination

							  INNER JOIN (
								  SELECT c.denomination, count( pd.id ) AS kirundi_ecrire_fem
								  FROM tbl_organisations c
									  INNER JOIN tbl_organisations g ON c.id = g.parent_id
									  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									  INNER JOIN exploitations e ON p.exploitation_id = e.id
									  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									  INNER JOIN personnes pers ON pers.exploitation_id = e.id
									  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  WHERE g.parent_id = c.id AND pers.nom <> '' AND pers.sexe='Féminin'
								  AND pd.kirundi_ecrire=1 {$reqAnnee}
								  GROUP BY c.denomination)supf ON pto.denomination=supf.denomination

							  INNER JOIN (
								  SELECT c.denomination, count( pd.id ) AS francais_lire_masc
								  FROM tbl_organisations c
									  INNER JOIN tbl_organisations g ON c.id = g.parent_id
									  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									  INNER JOIN exploitations e ON p.exploitation_id = e.id
									  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									  INNER JOIN personnes pers ON pers.exploitation_id = e.id
									  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  WHERE g.parent_id = c.id AND pers.nom <> ''
								  AND pers.sexe='Masculin' AND pd.francais_lire=1 {$reqAnnee}
								  GROUP BY c.denomination)suph ON pto.denomination=suph.denomination
							  INNER JOIN (
								  SELECT c.denomination, count( pd.id ) AS francais_lire_fem
								  FROM tbl_organisations c
									  INNER JOIN tbl_organisations g ON c.id = g.parent_id
									  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									  INNER JOIN exploitations e ON p.exploitation_id = e.id
									  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									  INNER JOIN personnes pers ON pers.exploitation_id = e.id
									  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  WHERE g.parent_id = c.id AND pers.nom <> '' AND pers.sexe='Féminin'
								  AND pd.francais_lire=1 {$reqAnnee}
								  GROUP BY c.denomination)flf ON pto.denomination=flf.denomination
							  INNER JOIN (
								  SELECT c.denomination, count( pd.id ) AS francais_ecrire_masc
								  FROM tbl_organisations c
									  INNER JOIN tbl_organisations g ON c.id = g.parent_id
									  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									  INNER JOIN exploitations e ON p.exploitation_id = e.id
									  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									  INNER JOIN personnes pers ON pers.exploitation_id = e.id
									  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  WHERE g.parent_id = c.id AND pers.nom <> '' AND pers.sexe='Masculin'
								  AND pd.francais_ecrire=1 {$reqAnnee}
								  GROUP BY c.denomination)fem ON pto.denomination=fem.denomination
							  INNER JOIN (
								  SELECT c.denomination, count( pd.id ) AS francais_ecrire_fem
								  FROM tbl_organisations c
									  INNER JOIN tbl_organisations g ON c.id = g.parent_id
									  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									  INNER JOIN exploitations e ON p.exploitation_id = e.id
									  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									  INNER JOIN personnes pers ON pers.exploitation_id = e.id
									  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  WHERE g.parent_id = c.id AND pers.nom <> '' AND  pers.sexe='Féminin'
								  AND pd.francais_ecrire=1 {$reqAnnee}
								  GROUP BY c.denomination)fef ON pto.denomination=fef.denomination
							  INNER JOIN tbl_organisation_level tol ON pto.level_id=tol.id WHERE tol.level=1";

                break;
            case 5: //il n'y a pas des données valides
                $titre = 'Classement et occurrence des cultures et élevages par coopérative pour les lignes 1, 2, 3 (351)';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['filiere_principale'] = $translator->trans('filiere_principale');
                $fields['ocurrences'] = $translator->trans('ocurrences');

                $reqAnnee = isset($annee) ? "AND f.annee='" . $annee . "'" : '';
                $sql = "SELECT c.denomination as cooperative,
						  tcult.culture AS filiere_principale,
						  COUNT(f.id) As ocurrences
					  FROM tbl_organisations c
						  INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						  INNER JOIN exploitations e ON p.exploitation_id = e.id
						  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						  INNER JOIN filieres f ON e.id=f.exploitation_id
						  INNER JOIN tbl_cultures tcult ON f.filiere_principale_id=tcult.id
					  WHERE tol.level=1 {$reqAnnee}
					  GROUP BY cooperative, filiere_principale
					  ORDER BY cooperative ASC,ocurrences DESC";

                break;
            case 10: /*  ok  */
                $titre = ' Nombre et pourcentage de membres ayant reçu une, deux formations tous opérateurs confondu, et via la CAPAD (638)';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['ocurrences_tous'] = $translator->trans('ocurrences_tous');
                $fields['ocurrence_capad'] = $translator->trans('ocurrence_capad');
                $fields['pourcentage_capad'] = $translator->trans('pourcentage_capad');
                $sql = "SELECT DISTINCT(pto.denomination) AS cooperative,
						pf.ocurrences_tous,
						ph.ocurrence_capad,
						ROUND((ph.ocurrence_capad/pf.ocurrences_tous)*100,2) AS pourcentage_capad
					  FROM tbl_organisations pto
						  INNER JOIN (SELECT c.denomination, count( p.id ) AS ocurrences_tous
								  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN formations_recu fr ON e.id = fr.exploitation_id
								  INNER JOIN tbl_organisateurs torg ON fr.organisateur_id=torg.id
								  WHERE g.parent_id = c.id
								  GROUP BY c.denomination)pf ON pto.denomination=pf.denomination
						   INNER JOIN (SELECT c.denomination, count( p.id ) AS ocurrence_capad
							  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN formations_recu fr ON e.id = fr.exploitation_id
								  INNER JOIN tbl_organisateurs torg ON fr.organisateur_id=torg.id
							  WHERE g.parent_id = c.id  AND torg.organisateur='CAPAD'
							  GROUP BY c.denomination)ph ON pto.denomination=ph.denomination
					       INNER JOIN tbl_organisation_level tol ON pto.level_id=tol.id
							  WHERE tol.level=1
							  ORDER BY pto.denomination";

                break;
            case 12: /* en fin une ok */
                $titre = 'Nombre et pourcentage, par coopérative, des occurrences par thème de formation (638)';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['theme'] = $translator->trans('theme');
                $fields['ocurrences'] = $translator->trans('ocurrences');

                $reqAnnee = isset($annee) ? "AND fr.annee='" . $annee . "'" : '';

                $sql = "SELECT c.denomination AS cooperative,
						ttf.theme_formation AS theme,
						COUNT(fr.id) AS ocurrences
					FROM tbl_organisations c
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
						INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						INNER JOIN exploitations e ON p.exploitation_id = e.id
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN formations_recu fr ON e.id = fr.exploitation_id
						INNER JOIN tbl_themes_formations ttf ON fr.theme_id=ttf.id
					WHERE fr.theme_id<>'' {$reqAnnee}
					GROUP BY cooperative, fr.theme_id
					ORDER BY cooperative, fr.theme_id";

                break;
            case 14: /*  ok */
                $titre = 'Nombre et pourcentage des exploitants exerçant l’agriculture à titre principal (061)';
                $fields['denomination'] = $translator->trans('denomination');
                $fields['agriculteurs'] = $translator->trans('agriculteurs');
                $fields['pourcentage'] = $translator->trans('pourcentage');

                if (isset($annee)) {
                    $reqAnnee = "AND pd.annee='" . $annee . "'";
                    $reqAnnee2 = "AND e.annee_data='" . $annee . "'";
                } else {
                    $reqAnnee = '';
                    $reqAnnee2 = '';
                }

                $sql = "SELECT DISTINCT(pto.denomination), pf.agriculteurs, Round((pf.agriculteurs/ph.total)*100,2) AS pourcentage
					  FROM tbl_organisations pto
						  INNER JOIN
								 (SELECT c.denomination, count( p.id ) AS agriculteurs
								  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
								  INNER JOIN tbl_professions tpp ON pd.profession_principale_id=tpp.id
								  WHERE g.parent_id = c.id {$reqAnnee} AND tpp.profession='Agriculteur'
								  GROUP BY c.denomination)pf ON pto.denomination=pf.denomination
						  INNER JOIN (
							  SELECT c.denomination, count( p.id ) AS total
							  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
							  WHERE g.parent_id = c.id
									  {$reqAnnee2} GROUP BY c.denomination)ph ON pto.denomination=ph.denomination
						  INNER JOIN tbl_organisation_level tol ON pto.level_id=tol.id WHERE tol.level=1 ";

                break;
            case 17:  // ok
                $titre = 'Répartition des membres producteurs par situation matrimoniale et par Coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['Celibataire'] = $translator->trans('Celibataire');
                $fields['Marie'] = $translator->trans('Marie');
                $fields['Veuf_Veuve'] = $translator->trans('Veuf_Veuve');
                $fields['Divorce'] = $translator->trans('Divorce');
                $fields['Polygame'] = $translator->trans('Polygame');

                $sql = "SELECT DISTINCT(pto.denomination) AS cooperative, pf.Celibataire, ph.Marie, sf.Veuf_Veuve, sh.Divorce,supf.Polygame
					  FROM tbl_organisations pto
						  INNER JOIN
								 (SELECT c.denomination, count( p.id ) AS Celibataire
								  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN tbl_etat_civil tec ON pers.etat_civil_id=tec.id
								  WHERE g.parent_id = c.id AND tec.etat_civil='Célibataire'
								  AND pers.rol_famille='" . Personnes::role_chefDeFamille . "'
								  GROUP BY c.denomination)pf ON pto.denomination=pf.denomination
						  INNER JOIN (
							  SELECT c.denomination, count( p.id ) AS Marie
							  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN tbl_etat_civil tec ON pers.etat_civil_id=tec.id
							  WHERE g.parent_id = c.id AND tec.etat_civil='Marié(e)'
							  AND pers.rol_famille='" . Personnes::role_chefDeFamille . "' GROUP BY c.denomination)ph ON pto.denomination=ph.denomination
						  INNER JOIN (
							  SELECT c.denomination, count( p.id ) AS Veuf_Veuve
							  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN tbl_etat_civil tec ON pers.etat_civil_id=tec.id
							  WHERE g.parent_id = c.id AND pers.nom <> '' AND tec.etat_civil='Veuf/Veuve'
							  AND pers.rol_famille='" . Personnes::role_chefDeFamille . "' GROUP BY c.denomination)sf ON pto.denomination=sf.denomination
						  INNER JOIN (
							  SELECT c.denomination, count( p.id ) AS Divorce
							  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN tbl_etat_civil tec ON pers.etat_civil_id=tec.id
							  WHERE g.parent_id = c.id AND pers.nom <> '' AND tec.etat_civil='Divorcé'
							  AND pers.rol_famille='" . Personnes::role_chefDeFamille . "' GROUP BY c.denomination)sh ON pto.denomination=sh.denomination
						  INNER JOIN (
							  SELECT c.denomination, count( p.id ) AS Polygame
							  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN tbl_etat_civil tec ON pers.etat_civil_id=tec.id
							  WHERE g.parent_id = c.id AND pers.nom <> '' AND tec.etat_civil='Polygame'
							  AND pers.rol_famille='" . Personnes::role_chefDeFamille . "' GROUP BY c.denomination)supf ON pto.denomination=supf.denomination
						  INNER JOIN tbl_organisation_level tol ON pto.level_id=tol.id WHERE tol.level=1 ";

                break;
            case 18:/* ok */
                $titre = 'Répartition des membres producteurs par niveau d’études et par Coopérative';
                $fields['denomination'] = $translator->trans('denomination');
                $fields['primaire'] = $translator->trans('primaire_femme');
                $fields['primaire_homme'] = $translator->trans('primaire_homme');
                $fields['secondaire_femme'] = $translator->trans('secondaire_femme');
                $fields['secondaire_hommes'] = $translator->trans('secondaire_hommes');
                $fields['superieur_femmes'] = $translator->trans('superieur_femmes');
                $fields['superieur_hommes'] = $translator->trans('superieur_hommes');

                $sql = "SELECT DISTINCT(pto.denomination),
							pf.primaire_femme,
							ph.primaire_homme,
							sf.secondaire_femme,
							sh.secondaire_hommes,
							supf.superieur_femmes,
							suph.superieur_hommes
					FROM tbl_organisations pto
						INNER JOIN (SELECT c.denomination, count( p.id ) AS primaire_femme
									FROM tbl_organisations c
									  INNER JOIN tbl_organisations g ON c.id = g.parent_id
									  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
									  INNER JOIN exploitations e ON p.exploitation_id = e.id
									  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									  INNER JOIN personnes pers ON pers.exploitation_id = e.id
									  INNER JOIN personne_details pd ON pers.id=pd.personnes_id
									WHERE g.parent_id = c.id AND pd.primaire=1 AND pers.sexe='Féminin'
									GROUP BY c.denomination)pf ON pto.denomination=pf.denomination
						INNER JOIN (SELECT c.denomination, count( p.id ) AS primaire_homme
									FROM tbl_organisations c
										INNER JOIN tbl_organisations g ON c.id = g.parent_id
										INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
										INNER JOIN exploitations e ON p.exploitation_id = e.id
										INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
										INNER JOIN personnes pers ON pers.exploitation_id = e.id
										INNER JOIN personne_details pd ON pers.id=pd.personnes_id
									WHERE g.parent_id = c.id AND pd.primaire=1 AND pers.sexe='Masculin'
									GROUP BY c.denomination)ph ON pto.denomination=ph.denomination
						INNER JOIN (SELECT c.denomination, count( p.id ) AS secondaire_femme
									FROM tbl_organisations c
										INNER JOIN tbl_organisations g ON c.id = g.parent_id
										INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
										INNER JOIN exploitations e ON p.exploitation_id = e.id
										INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
										INNER JOIN personnes pers ON pers.exploitation_id = e.id
										INNER JOIN personne_details pd ON pers.id=pd.personnes_id
									WHERE g.parent_id = c.id AND pers.nom <> '' AND pd.secondaire=1 AND pers.sexe='Féminin'
									GROUP BY c.denomination)sf ON pto.denomination=sf.denomination
						INNER JOIN (SELECT c.denomination, count( p.id ) AS secondaire_hommes
									FROM tbl_organisations c
										INNER JOIN tbl_organisations g ON c.id = g.parent_id
										INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
										INNER JOIN exploitations e ON p.exploitation_id = e.id
										INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
										INNER JOIN personnes pers ON pers.exploitation_id = e.id
										INNER JOIN personne_details pd ON pers.id=pd.personnes_id
									WHERE g.parent_id = c.id AND pers.nom <> '' AND pd.secondaire=1 AND pers.sexe='Masculin'
									GROUP BY c.denomination)sh ON pto.denomination=sh.denomination
						INNER JOIN (SELECT c.denomination, count( p.id ) AS superieur_femmes
									FROM tbl_organisations c
										INNER JOIN tbl_organisations g ON c.id = g.parent_id
										INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
										INNER JOIN exploitations e ON p.exploitation_id = e.id
										INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
										INNER JOIN personnes pers ON pers.exploitation_id = e.id
										INNER JOIN personne_details pd ON pers.id=pd.personnes_id
									WHERE g.parent_id = c.id AND pers.nom <> '' AND pd.superieur=1 AND pers.sexe='Féminin'
									GROUP BY c.denomination)supf ON pto.denomination=supf.denomination
						INNER JOIN (SELECT c.denomination, count( p.id ) AS superieur_hommes
									FROM tbl_organisations c
										INNER JOIN tbl_organisations g ON c.id = g.parent_id
										INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
										INNER JOIN exploitations e ON p.exploitation_id = e.id
										INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
										INNER JOIN personnes pers ON pers.exploitation_id = e.id
										INNER JOIN personne_details pd ON pers.id=pd.personnes_id
									WHERE g.parent_id = c.id AND pers.nom <> '' AND pd.superieur=1 AND pers.sexe='Masculin'
									GROUP BY c.denomination)suph ON pto.denomination=suph.denomination
						INNER JOIN tbl_organisation_level tol ON pto.level_id=tol.id WHERE tol.level=1 ";

                break;
            case 21: /* ok */
                $titre = 'Répartition des membres producteurs par nombre moyen de filles et par Coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['nombre_filles'] = $translator->trans('nombre_filles');
                $fields['membres'] = $translator->trans('membres');

                $sql = 'SELECT c.denomination AS cooperative,
						e.Nombre_filles AS nombre_filles,
						COUNT( e.id ) AS membres
					  FROM tbl_organisations c
					  	INNER JOIN tbl_organisations g ON c.id = g.parent_id
					  	INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
					  	INNER JOIN exploitations e ON p.exploitation_id = e.id
					  	INNER JOIN tbl_organisation_level tol ON c.level_id = tol.id
					  WHERE tol.level =1
					  GROUP BY cooperative, nombre_filles';

                break;
            case 22: /* ok  */
                $titre = 'Répartition des membres producteurs par nombre moyen de fils scolarisés et par Coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['fils_scolarises'] = $translator->trans('fils_scolarises');
                $fields['membres'] = $translator->trans('membres');

                $sql = 'SELECT c.denomination AS cooperative,
						e.Fils_scolarises AS fils_scolarises,
						COUNT(e.id) AS membres
					FROM tbl_organisations c
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
						INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						INNER JOIN exploitations e ON p.exploitation_id = e.id
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
					WHERE tol.level=1
					GROUP BY cooperative, fils_scolarises;';

                break;
            case 23: /* ok  */
                $titre = 'Répartition des membres producteurs par nombre moyen de filles scolarisées et par Coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['filles_scolarisees'] = $translator->trans('filles_scolarisees');
                $fields['membres'] = $translator->trans('membres');

                $sql = 'SELECT c.denomination AS cooperative, e.Filles_scolarisess AS filles_scolarisees, COUNT( e.id ) AS membres
					  FROM tbl_organisations c
					  INNER JOIN tbl_organisations g ON c.id = g.parent_id
					  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
					  INNER JOIN exploitations e ON p.exploitation_id = e.id
					  INNER JOIN tbl_organisation_level tol ON c.level_id = tol.id
					  WHERE tol.level =1
					  GROUP BY cooperative, filles_scolarisees';

                break;
            case 24:/* le champ filiere_principale_id is null!!!   */
                $titre = 'Production par filière et par Coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['filiere_principale'] = $translator->trans('filiere_principale');
                $fields['quantite'] = $translator->trans('quantite');

                $reqAnnee = isset($annee) ? "AND f.annee='" . $annee . "'" : '';

                $sql = 'SELECT c.denomination AS cooperative,
							tcult.culture AS filiere_principale,
							Sum(f.Quantite_produit) AS quantite
						' . $from_exploitations . "
							LEFT JOIN filieres f ON e.id = f.exploitation_id
							INNER JOIN tbl_cultures tcult ON f.filiere_principale_id=tcult.id
						WHERE f.Filiere_principale_id<>''
							OR f.Filiere_principale_id<>0
							{$reqAnnee}
						GROUP BY cooperative, filiere_principale";

                break;
            case 25: /* ok   */
                $titre = 'Production par filière et par Coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['filiere'] = $translator->trans('filiere');
                $fields['quantite'] = $translator->trans('quantite');

                $reqAnnee = isset($annee) ? "AND f.annee='" . $annee . "'" : '';

                $sql = "SELECT c.denomination AS cooperative, tc.culture AS filiere, Sum( f.quantite_produit ) AS quantite
					  FROM tbl_organisations c
					  INNER JOIN tbl_organisations g ON c.id = g.parent_id
					  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
					  INNER JOIN exploitations e ON p.exploitation_id = e.id
					  INNER JOIN tbl_organisation_level tol ON c.level_id = tol.id
					  LEFT JOIN filieres f ON e.id = f.exploitation_id
					  INNER JOIN tbl_cultures tc ON f.filiere_principale_id=tc.id
					  WHERE tol.level=1 {$reqAnnee}
					  GROUP BY cooperative, filiere";

                break;
            case 26: /*   ok   */
                $titre = 'Quantité par produit commercialisé et par coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['produit'] = $translator->trans('produit');
                $fields['quantite'] = $translator->trans('quantite');

                $reqAnnee = isset($annee) ? "AND f.annee='" . $annee . "'" : '';

                $sql = "SELECT c.denomination as cooperative,
						  tp.produit,
						  Sum(fc.quantite_commercialise) AS quantite
					  FROM tbl_organisations c
						  INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						  INNER JOIN exploitations e ON p.exploitation_id = e.id
						  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						  INNER JOIN filieres f ON f.exploitation_id=e.id
						  INNER JOIN filiere_commercialisation fc ON fc.filiere_id=f.id
						  INNER JOIN tbl_produits tp ON tp.id=fc.produit_commercialise_id
					  WHERE tol.level=1 {$reqAnnee}
					  GROUP BY cooperative, tp.produit
					  HAVING (((tp.produit) IS NOT NULL AND (tp.produit<>'')))";

                break;
            case 27: /*   ok  */
                $titre = 'Montant total des cotisations , par coopérative [639]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['paye_annee'] = $translator->trans('paye_annee');

                if (isset($annee)) {
                    $reqAnnee = '';
                }
                $sql = "SELECT c.denomination as cooperative,
						  SUM(p.paye_annee) AS paye_annee
					  FROM tbl_organisations c
						  INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						  INNER JOIN exploitations e ON p.exploitation_id = e.id
						  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
					  WHERE tol.level=1 {$reqAnnee}
					  GROUP BY cooperative";

                break;
            case 28:/*   mauvais  */
                $titre = 'Montant total des cotisations , par coopérative [639]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['produit'] = $translator->trans('produit');
                $fields['quantite'] = $translator->trans('quantite');
                $sql = 'mauvais';

                break;
            case 35: /*   ok   */
                $titre = "Répartition de membres producteurs selon le type d'élevage pratiqué par coopérative";
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['type_elevage'] = $translator->trans('type_elevage');
                $fields['nombre_exploitations'] = $translator->trans('nombre_exploitations');

                $reqAnnee = isset($annee) ? "AND a.annee='" . $annee . "'" : '';
                $sql = "SELECT c.denomination as cooperative, tte.type_elevage as type_elevage, Count(e.id) AS nombre_exploitations
					  FROM tbl_organisations c
							INNER JOIN tbl_organisations g ON c.id = g.parent_id
							INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
							INNER JOIN exploitations e ON p.exploitation_id = e.id
							INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
							INNER JOIN animaux a ON a.exploitation_id=e.id
							INNER JOIN tbl_types_elevage tte ON a.type_elevage_id=tte.id
					  WHERE tol.level=1 {$reqAnnee}
					  GROUP BY cooperative, type_elevage;";

                break;
            case 46: /*  ok  */
                $titre = "Répartition des membres producteurs selon les thèmes de formation qu'eux et leurs conjoints ont bénéficié";
                $fields['theme_formation'] = $translator->trans('theme_formation');
                $fields['Ocurrences'] = $translator->trans('Ocurrences');

                $reqAnnee = isset($annee) ? "AND fr.annee='" . $annee . "'" : '';
                $sql = "SELECT ttf.theme_formation AS theme_formation, Count(fr.id) AS Ocurrences
					  FROM formations_recu fr
						  INNER JOIN tbl_themes_formations ttf ON fr.theme_id=ttf.id
					  WHERE 1=1 {$reqAnnee}
					  GROUP BY theme_formation
					  ORDER BY theme_formation;";

                break;
            case 47: /*  ok  */
                $titre = "Répartition de membres producteurs selon le bénéficiaire de la formation qu'eux et leurs conjoints ont bénéficié";
                $fields['theme_formation'] = $translator->trans('theme_formation');
                $fields['Ocurrences_Chef_de_famille'] = $translator->trans('Ocurrences_Chef_de_famille');
                $fields['Ocurrences_Conjoint'] = $translator->trans('Ocurrences_Conjoint');

                $reqAnnee = isset($annee) ? "AND fr.annee='" . $annee . "'" : '';
                $sql = "SELECT ttf.theme_formation AS theme_formation,
						  (SELECT count(fr.id)
						  FROM formations_recu fr
						  WHERE fr.theme_id=ttf.id
							  AND fr.beneficiaire='" . Personnes::role_chefDeFamille . "'
							  AND {$reqAnnee}) AS Ocurrences_Chef_de_famille,
						  (SELECT count(fr.id)
						  FROM formations_recu fr
						  WHERE fr.theme_id=ttf.id
							  AND fr.beneficiaire='Conjoint'
							  {$reqAnnee}) AS Ocurrences_Conjoint
					  FROM tbl_themes_formations ttf
					  GROUP BY ttf.theme_formation";

                break;
            case 48:  /*  ok  */
                $titre = "Répartition des membres producteurs selon l'utilisation faite de la formation qu'eux et leurs conjoints ont bénéficié";
                $fields['theme_formation'] = $translator->trans('theme_formation');
                $fields['utilite'] = $translator->trans('utilite');
                $fields['ocurrences'] = $translator->trans('ocurrences');

                $reqAnnee = isset($annee) ? "AND fr.annee='" . $annee . "'" : '';
                $sql = "SELECT ttf.theme_formation AS theme_formation, tu.utilite AS utilite, Count(fr.id) AS ocurrences
					  FROM formations_recu fr
						  INNER JOIN tbl_themes_formations ttf ON fr.theme_id=ttf.id
						  INNER JOIN tbl_utilites tu ON fr.niveau_utilite_id=tu.id
					  WHERE 1=1 {$reqAnnee}
					  GROUP BY theme_formation,tu.utilite;";

                break;
            case 49:  /*  ok  */
                $titre = "Répartition des membres producteurs selon l'organisateur de la formation qu'eux et leurs conjoints ont bénéficié";
                $fields['organisateur'] = $translator->trans('organisateur');
                $fields['ocurrences'] = $translator->trans('ocurrences');

                $reqAnnee = isset($annee) ? "AND fr.annee='" . $annee . "'" : '';
                $sql = "SELECT tor.organisateur AS organisateur, Count(fr.id) AS ocurrences
					  FROM formations_recu fr
						INNER JOIN tbl_organisateurs tor ON fr.organisateur_id=tor.id
					  WHERE 1=1 {$reqAnnee}
					  GROUP BY organisateur;";

                break;
            case 50: /*  ok  */
                $titre = "Répartition des membres producteurs selon les thèmes de formation qu'eux et leurs conjoints ont besoin";
                $fields['theme_formation'] = $translator->trans('theme_formation');
                $fields['Ocurrences'] = $translator->trans('Ocurrences');

                $reqAnnee = isset($annee) ? "AND fb.annee='" . $annee . "'" : '';
                $sql = "SELECT ttf.theme_formation AS theme_formation, Count(fb.id) AS Ocurrences
						FROM formations_besoin fb
							INNER JOIN tbl_themes_formations ttf ON fb.formation_id=ttf.id
						WHERE 1=1 {$reqAnnee}
						GROUP BY theme_formation
						ORDER BY theme_formation;";

                break;
            case 51:  /* ok  */
                $titre = 'Répartition des membres producteurs selon les contraintes majeures au développement des activités agricoles citées';
                $fields['contraint'] = $translator->trans('contraint');
                $fields['ocurrences'] = $translator->trans('ocurrences');

                $reqAnnee = isset($annee) ? "AND cd.annee='" . $annee . "'" : '';
                $sql = "SELECT tc.contraint AS contraint, Count(cd.id) AS ocurrences
					  FROM contraintes_developpement cd
						  INNER JOIN tbl_contraints tc ON  cd.contrainte_id=tc.id
					  WHERE 1=1 {$reqAnnee}
					  GROUP BY contraint";

                break;
            case 52:  /*  ok  */
                $titre = 'Répartition des membres producteurs selon les services dont ils ont besoin par coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['service'] = $translator->trans('service');
                $fields['ocurrences'] = $translator->trans('ocurrences');

                $reqAnnee = isset($annee) ? "AND sb.annee='" . $annee . "'" : '';
                $sql = "SELECT c.denomination AS cooperative, ts.service AS service, Count(e.id) AS ocurrences
					  FROM tbl_organisations c
					  INNER JOIN tbl_organisations g ON c.id = g.parent_id
					  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
					  INNER JOIN exploitations e ON p.exploitation_id = e.id
					  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
					  INNER JOIN services_besoin sb ON sb.exploitation_id=e.id
					  INNER JOIN tbl_services ts ON sb.service_id=ts.id
					  WHERE tol.level=1 {$reqAnnee}
					  GROUP BY cooperative, service";

                break;
            case 53:  /*  ok  */
                $titre = 'F0 - les superficies et la production de mais cultivé par coopérative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['surface'] = $translator->trans('surface');
                $fields['quantite'] = $translator->trans('quantite');

                $reqAnnee = isset($annee) ? "AND f.annee='" . $annee . "'" : '';
                $sql = "SELECT c.denomination AS cooperative,
							SUM(f.surface) AS surface,
							SUM(f.quantite_produit) AS quantite
				  	  FROM tbl_organisations c
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
						INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						INNER JOIN exploitations e ON p.exploitation_id = e.id
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN filieres f ON f.exploitation_id=e.id
						INNER JOIN tbl_cultures tcult ON f.filiere_principale_id=tcult.id
						WHERE tol.level=1
							AND tcult.culture='Maïs' {$reqAnnee}
						GROUP BY cooperative";

                break;
            case 60:  /* ok  */
                $titre = 'Suivi coopératives - les coopératives qui répondent OUI aux indicateurs institutionnels';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['existence_status'] = $translator->trans('existence_status');
                $fields['existence_roi'] = $translator->trans('existence_roi');
                $fields['existence_ce'] = $translator->trans('existence_ce');
                $fields['existence_cs'] = $translator->trans('existence_cs');

                $reqAnnee = isset($annee) ? "AND so.annee='" . $annee . "'" : '';
                $sql = "SELECT c.denomination AS cooperative,
							IF(so.existence_status=1,'yes','non')AS existence_status,
							IF(so.existence_roi=1,'yes','non')AS existence_roi,
							IF(so.existence_ce=1,'yes','non')AS existence_ce,
							IF(so.existence_cs=1,'yes','non')AS existence_cs
					  FROM tbl_organisations c
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN suivi_organisations so ON c.id=so.organisation_id
					  WHERE tol.level=1
					  	{$reqAnnee}
					  ORDER BY cooperative";

                break;
            case 62:  /*  ok  */
                $titre = 'F1 - Liste des exploitations avec F1, par cooperative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['numero_membre'] = $translator->trans('numero_membre');
                $fields['nom'] = $translator->trans('nom');
                $fields['prenom'] = $translator->trans('prenom');

                $sql = "SELECT c.denomination AS cooperative,
							e.numero_membre,
							pers.nom,
							pers.prenom
					  FROM tbl_organisations c
                          INNER JOIN tbl_organisations g ON c.id = g.parent_id
                          INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
                          INNER JOIN exploitations e ON p.exploitation_id = e.id
                          INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
                          INNER JOIN personnes pers ON pers.exploitation_id = e.id
					  WHERE pers.rol_famille='" . Personnes::role_chefDeFamille . "'
						AND e.Numero_membre<>''
					 ORDER BY cooperative, e.numero_membre";

                break;
            case 63:  /*  ok  */
                $titre = 'F0 - Liste des exploitations avec F0, par cooperative';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['numero_membre'] = $translator->trans('numero_membre');
                $fields['nom'] = $translator->trans('nom');
                $fields['prenom'] = $translator->trans('prenom');
                $fields['annee'] = $translator->trans('annee');

                $reqAnnee = isset($annee) ? "AND sb.annee='" . $annee . "'" : '';
                $sql = "SELECT c.denomination AS cooperative,
							e.numero_membre,
							pers.nom,
							pers.prenom,
							pd.annee
					  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
								  INNER JOIN exploitations e ON p.exploitation_id = e.id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  INNER JOIN personnes pers ON pers.exploitation_id = e.id
								  INNER JOIN personne_details pd ON pd.personnes_id = pers.id
					  WHERE pers.rol_famille='" . Personnes::role_chefDeFamille . "'
						AND e.Numero_membre<>''
						AND (pd.annee IS NOT NULL) AND (pd.annee<>'')
					  ORDER BY cooperative, e.numero_membre, pd.annee";

                break;
            case 64:  /*  ok  */
                $titre = '6330 F0 - Formations complémentaires dont un co-exploitant a bénéficié (groupé par commune), année: ' . $annee . ': Planification Agricole.';
                $fields['Province'] = $translator->trans('Province');
                $fields['Commune'] = $translator->trans('Commune');
                $fields['Quantite_personnes'] = $translator->trans('Quantite_personnes');

                $reqAnnee = isset($annee) && '' !== $annee ? "AND forma.anneee_formation='" . $annee . "'" : '';
                $sql = 'SELECT prov.denomination AS Province, com.denomination AS Commune, COUNT(forma_pers.personnes_id) AS Quantite_personnes '
                    . 'FROM exploitations e INNER JOIN tbl_decoupage_admin col ON e.localisation_admin_id=col.id '
                    . 'INNER JOIN tbl_decoupage_admin com ON com.id = col.parent_id '
                    . 'INNER JOIN tbl_decoupage_admin prov ON prov.id = com.parent_id '
                    . 'INNER JOIN formations_recu forma ON forma.exploitation_id = e.id '
                    . 'INNER JOIN formations_recu_personnes forma_pers ON forma.id=forma_pers.formations_recu_id '
                    . 'WHERE e.radie=0 AND forma.theme_id=12 ' . $reqAnnee
                    . ' GROUP BY Commune '
                    . ' ORDER BY Province, Commune;';

                break;
            case 65:  /*  ok  */
                $titre = '6330 Formations complémentaires dont un co-exploitant a bénéficié-  (groupé par commune), année: ' . $annee . ': pratiques de fertilisation durable';
                $fields['Province'] = $translator->trans('Province');
                $fields['Commune'] = $translator->trans('Commune');
                $fields['Quantite_personnes'] = $translator->trans('Quantite_personnes');

                $reqAnnee = isset($annee) && '' !== $annee ? "AND forma.anneee_formation='" . $annee . "'" : '';
                $sql = 'SELECT prov.denomination AS Province, com.denomination AS Commune, COUNT(forma_pers.personnes_id) AS Quantite_personnes '
                    . 'FROM exploitations e INNER JOIN tbl_decoupage_admin col ON e.localisation_admin_id=col.id '
                    . 'INNER JOIN tbl_decoupage_admin com ON com.id = col.parent_id '
                    . 'INNER JOIN tbl_decoupage_admin prov ON prov.id = com.parent_id '
                    . 'INNER JOIN formations_recu forma ON forma.exploitation_id = e.id '
                    . 'INNER JOIN formations_recu_personnes forma_pers ON forma.id=forma_pers.formations_recu_id '
                    . 'WHERE e.radie=0 AND forma.theme_id=18 ' . $reqAnnee
                    . ' GROUP BY Commune '
                    . ' ORDER BY Province, Commune;';

                break;
            case 69:  /*  ok  */
                $titre = 'Nombre de repas par exploitant en période de culture. Année: ' . $annee;
                $fields['Province'] = $translator->trans('Province');
                $fields['Commune'] = $translator->trans('Commune');
                $fields['NombreRepas'] = $translator->trans('NombreRepas');
                $fields['NombreExploitations'] = $translator->trans('NombreExploitations');

                $reqAnnee = isset($annee) && '' !== $annee ? " AND sa.annee='" . $annee . "'" : '';
                $sql = 'SELECT prov.denomination AS Province, com.denomination AS Commune, sa.repas_culture AS NombreRepas, COUNT(e.id) AS NombreExploitations'
                    . ' FROM exploitations e '
                    . 'INNER JOIN tbl_decoupage_admin col ON e.localisation_admin_id=col.id '
                    . 'INNER JOIN tbl_decoupage_admin com ON com.id = col.parent_id '
                    . 'INNER JOIN tbl_decoupage_admin prov ON prov.id = com.parent_id '
                    . 'INNER JOIN securite_alimentaire sa ON sa.exploitation_id=e.id '
                    . 'WHERE e.radie=0 ' . $reqAnnee
                    . ' GROUP BY Commune, sa.repas_culture'
                    . ' ORDER BY Province, Commune;';

                break;
            case 70:  /*  ok  */
                $titre = 'Nombre de repas en période post récolte. Année: ' . $annee;
                $fields['Province'] = $translator->trans('Province');
                $fields['Commune'] = $translator->trans('Commune');
                $fields['NombreRepas'] = $translator->trans('NombreRepas');
                $fields['NombreExploitations'] = $translator->trans('NombreExploitations');

                $reqAnnee = isset($annee) && '' !== $annee ? " AND sa.annee='" . $annee . "'" : '';
                $sql = 'SELECT prov.denomination AS Province, com.denomination AS Commune, sa.repas_post_recolte AS NombreRepas, COUNT(e.id) AS NombreExploitations'
                    . ' FROM exploitations e '
                    . 'INNER JOIN tbl_decoupage_admin col ON e.localisation_admin_id=col.id '
                    . 'INNER JOIN tbl_decoupage_admin com ON com.id = col.parent_id '
                    . 'INNER JOIN tbl_decoupage_admin prov ON prov.id = com.parent_id '
                    . 'INNER JOIN securite_alimentaire sa ON sa.exploitation_id=e.id '
                    . 'WHERE e.radie=0 ' . $reqAnnee
                    . ' GROUP BY Commune, sa.repas_post_recolte '
                    . ' ORDER BY Province, Commune;';

                break;

            default:
                break;
        }

        /* creationrequête et envoi de l'array  */
        $stmt = $em->getConnection()->prepare($sql);
        $stmt->execute();

        return [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $stmt->fetchAll(),
            'strSql' => $sql,
        ];
    }

    private function getfiliere(TranslatorInterface $translator, EntityManagerInterface $em): array
    {
        /* à garder ? */
        /*------------------------------------------*/
        $parametros = $_POST['rapports_filieres'];
        $cooperative_id = $parametros['Denomination'];
        $grouper = $parametros['groupementType'];
        $annee = $parametros['exploitationListAnnee'];

        if (isset($parametros['groupecoop'])) {
            $GroupAnnee = ' , annee';
            $whereAnnee = '';
            $selectAnnee = ',f.annee';
        } else {
            $GroupAnnee = '';
            $whereAnnee = " AND f.annee={$annee} ";
            $selectAnnee = '';
        }

        $where = empty($cooperative_id) ? '' : " AND c.id='" . $cooperative_id . "' ";
        $titre = 'Rapport sur les filieres';

        switch ($grouper) {
            case 'province':
                $fields['province'] = $translator->trans('province');

                // TODO: What ???
                if (2 < mb_strlen($GroupAnnee)) {
                    $fields['annee'] = $translator->trans('anneee');
                }
                $fields['filiere_principale'] = $translator->trans('filiere_principale');
                $fields['production_filiere'] = $translator->trans('production_filiere');
                $strSql = "SELECT prov.denomination AS province {$selectAnnee} ,
							tc.culture AS filiere_principale,
							SUM(f.surface) as surface,
							SUM(f.quantite_produit) AS production_filiere
					 FROM tbl_decoupage_admin prov
					 	  INNER JOIN tbl_decoupage_admin_level tdal ON prov.level_id=tdal.id
					 	  INNER JOIN tbl_decoupage_admin comm ON comm.parent_id=prov.id
						  INNER JOIN tbl_decoupage_admin coll ON coll.parent_id=comm.id
						  INNER JOIN exploitations e ON e.localisation_admin_id=coll.id
						  INNER JOIN filieres f ON f.exploitation_id=e.id
						  INNER JOIN tbl_cultures tc ON f.filiere_principale_id=tc.id
					  WHERE tdal.level=1 {$whereAnnee}
					  GROUP BY province {$GroupAnnee}, filiere_principale";

                break;
            case 'cooperative':
                $fields['cooperative'] = $translator->trans('cooperative');

                if (2 < mb_strlen($GroupAnnee)) {
                    $fields['annee'] = $translator->trans('anneee');
                }
                $fields['filiere_principale'] = $translator->trans('filiere_principale');
                $fields['production_filiere'] = $translator->trans('production_filiere');
                $strSql = "SELECT c.denomination AS cooperative {$selectAnnee},
							tc.culture AS filiere_principale,
							SUM(f.surface) as surface,
							SUM(f.quantite_produit) AS production_filiere
					 FROM tbl_organisations c
					 	  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						  INNER JOIN tbl_organisations g ON g.parent_id=c.id
						  INNER JOIN pivot_exploitation_organisations p ON p.organisation_id=g.id
						  INNER JOIN exploitations e ON p.exploitation_id=e.id
						  INNER JOIN filieres f ON f.exploitation_id=e.id
						  INNER JOIN tbl_cultures tc ON f.filiere_principale_id=tc.id
				     WHERE tol.level=1 {$where} {$whereAnnee}
					 GROUP BY cooperative {$GroupAnnee},filiere_principale";

                break;
            case 'groupement':
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['groupement'] = $translator->trans('groupement');

                if (2 < mb_strlen($GroupAnnee)) {
                    $fields['annee'] = $translator->trans('anneee');
                }
                $fields['filiere_principale'] = $translator->trans('filiere_principale');
                $fields['production_filiere'] = $translator->trans('production_filiere');
                $strSql = "SELECT c.denomination AS cooperative,
							g.denomination AS groupement {$selectAnnee},
							tc.culture AS filiere_principale,
							SUM(f.surface) as surface,
							SUM(f.quantite_produit) AS production_filiere
					 FROM tbl_organisations c
					 	  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						  INNER JOIN tbl_organisations g ON g.parent_id=c.id
						  INNER JOIN pivot_exploitation_organisations p ON p.organisation_id=g.id
						  INNER JOIN exploitations e ON p.exploitation_id=e.id
						  INNER JOIN filieres f ON f.exploitation_id=e.id
						  INNER JOIN tbl_cultures tc ON f.filiere_principale_id=tc.id
				     WHERE tol.level=1 {$where} {$whereAnnee}
					 GROUP BY cooperative, groupement {$GroupAnnee} , filiere_principale";

                break;
        }

        /* **** fin de construction requete ***** */

        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();
        $recordset = $stmt->fetchAll();

        return [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $recordset,
        ];
    }

    private function getgroupements(TranslatorInterface $translator, EntityManagerInterface $em, int $numero, int $annee): array
    {
        $strSQL = '';

        switch ($numero) {
            case 1:
                $titre = 'Répartition spatiale, nombre de groupements par commune et coopérative [02, 03]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['communes'] = $translator->trans('communes');
                $fields['Ocurrences'] = $translator->trans('Ocurrences');
                $strSQL = 'SELECT c.denomination AS cooperative, t.commune AS communes, Count(g.id) AS Ocurrences
						  FROM tbl_organisations c
								  INNER JOIN tbl_organisations g ON c.id = g.parent_id
								  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								  LEFT JOIN tbl_decoupage_admin tda ON g.	decoupage_admin_id=tda.id
								  LEFT JOIN (SELECT tda2.id AS id, tda2.denomination AS commune
											  FROM tbl_decoupage_admin tda2)t	ON tda.parent_id=t.id
						  WHERE tol.level=1
						  GROUP BY cooperative, communes;';

                break;
            case 2:
                $titre = 'Existence du ROI [I1] par Année de renouvellement [I4]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['annee_renouv_comite'] = $translator->trans('annee_renouv_comite');
                $fields['ocurrences'] = $translator->trans('ocurrences');
                $strSQL = 'SELECT c.denomination AS cooperative,
							  s.`annee_renouvellement_ce` AS annee_renouv_comite,
							  COUNT(g.id) AS ocurrences
						  FROM tbl_organisations c
								INNER JOIN tbl_organisations g ON c.id = g.parent_id
								INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
								LEFT JOIN suivi_organisations s ON g.id=s.organisation_id
						  WHERE tol.level=1 AND s.existence_roi=1
						  GROUP BY cooperative, annee_renouv_comite;';

                break;
            case 3:
                $titre = sprintf("Existence de PV de réunion [II3], Rapport annuel d'activités [II5]. (Année: %s)", $annee);
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['Total'] = $translator->trans('Total');
                $fields['AG'] = $translator->trans('AG');
                $fields['Comite'] = $translator->trans('Comite');
                $fields['Rapport'] = $translator->trans('Rapport');
                $strSQL = 'SELECT (c.denomination) AS cooperative,
						(SELECT Count(g.id)
							FROM suivi_organisations s
							WHERE g.id= s.organisation_id
								AND s.annee=2012
								GROUP BY c.denomination)  AS Total,
						(SELECT Count(g.id)
						FROM suivi_organisations s1
						WHERE g.id = s1.organisation_id
							AND s1.annee=2012
							AND s1.pv_reunions_ag=True
							GROUP BY c.denomination)  AS AG,
						(SELECT Count(g.id)
						FROM suivi_organisations s2
						WHERE g.id = s2.organisation_id
							AND s2.annee=2012
							AND s2.pv_reunions_ce=True
							GROUP BY c.denomination)  AS Comite,
						(SELECT Count(g.id)
								FROM suivi_organisations s3
								WHERE g.id= s3.organisation_id
									AND s3.annee=2012
							AND s3.rapport_annuel_activites=True
							GROUP BY c.denomination)  AS Rapport
					FROM tbl_organisations c
						  INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
					WHERE tol.level=1
					GROUP BY cooperative;';

                break;
            case 4:
                $titre = sprintf("L'enregistrement des cotisations [III4], le classement des justificatifs [III8], l'existence de rapport financier annuel [III9]. (Année: %s)", $annee);
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['cotisations'] = $translator->trans('cotisations');
                $fields['classement'] = $translator->trans('classement');
                $fields['rapports_financieres'] = $translator->trans('rapports_financieres');
                $strSQL = "SELECT DISTINCT(c.denomination) AS cooperative,
						(SELECT Count(g.id)
							FROM suivi_organisations s
							WHERE g.id= s.organisation_id
								AND s.annee={$annee}
								AND s.enregistrement_cotisations=True
								GROUP BY c.denomination)  AS cotisations,
						(SELECT Count(g.id)
						FROM suivi_organisations s1
						WHERE g.id = s1.organisation_id
							AND s1.annee={$annee}
							AND s1.classement_justificatifs=True
							GROUP BY c.denomination)  AS classement,
						(SELECT Count(g.id)
						FROM suivi_organisations s2
						WHERE g.id = s2.organisation_id
							AND s2.annee={$annee}
							AND s2.rapports_financiere_annuel=True
							GROUP BY c.denomination)  AS rapports_financieres
					FROM tbl_organisations c
						  INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
					WHERE tol.level=1
					GROUP BY cooperative";

                break;
            case 5:
                $titre = 'Revenu total: [IV6]+[IV8]+[IV9]; pourcentages de [IV6]/ total, [IV8]/total, [IV9]/total';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['cotisations'] = $translator->trans('cotisations');
                $fields['agr'] = $translator->trans('agr');
                $strSQL = "SELECT DISTINCT(c.denomination) AS cooperative,
						(SELECT SUM(s.cotisations_annee_avant)
						FROM suivi_organisations s
						WHERE s.organisation_id=g.id
							AND s.annee={$annee}
						GROUP BY c.denomination) AS cotisations,
						(SELECT SUM(s1.agr_montant)
							FROM suivi_organisations s1
							WHERE s1.organisation_id=g.id
								AND s1.annee={$annee}
						GROUP BY c.denomination) AS agr
						FROM tbl_organisations c
						  INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
					WHERE tol.level=1
					GROUP BY cooperative";

                break;
            case 6:
                $titre = 'Fréquence de [IV1], [IV2], [IV3], [IV4], [IV7]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['Intrans'] = $translator->trans('Intrans');
                $fields['Groupage_produit'] = $translator->trans('Groupage_produit');
                $fields['Chaine_solidarite'] = $translator->trans('Chaine_solidarite');
                $fields['Credits'] = $translator->trans('Credits');
                $fields['collecte_cotisations'] = $translator->trans('collecte_cotisations');
                $strSQL = "SELECT DISTINCT (c.denomination) AS cooperative,
							(SELECT Count(s.id)
							FROM suivi_organisations s
							WHERE g.id = s.organisation_id
								AND s.annee={$annee}
								AND s.besoins_intrans=True
							GROUP BY c.denomination) AS Intrans,
							(SELECT Count(s1.id)
							FROM suivi_organisations s1
							WHERE g.id = s1.organisation_id
								AND s1.annee={$annee}
								AND s1.groupage_produits=True
							GROUP BY c.denomination) AS Groupage_produit,
							(SELECT Count(s2.id)
							FROM suivi_organisations s2
							WHERE g.id = s2.organisation_id
								AND s2.annee={$annee}
								AND s2.chaine_solidarite_elevage=True
							GROUP BY c.denomination) AS Chaine_solidarite,
							(SELECT Count(s3.id)
							FROM suivi_organisations s3
							WHERE g.id = s3.organisation_id
								AND s3.annee={$annee}
								AND s3.recouvrement_credits=True
							GROUP BY c.denomination) AS Credits,
							(SELECT Count(s4.id)
							FROM suivi_organisations s4
							WHERE g.id = s4.organisation_id
								AND s4.annee={$annee}
								AND s4.collecte_cotisations=True
							GROUP BY c.denomination) AS collecte_cotisation
						FROM tbl_organisations c
						  INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						WHERE tol.level=1
						GROUP BY cooperative";

                break;
            case 7:
                $titre = 'Pourcentage des membres ayant contracté un crédit [V1] ; montant total des crédits contractés [V1]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['Membres_credit'] = $translator->trans('Membres_credit');
                $fields['Montant'] = $translator->trans('Montant');
                $fields['Total_membres'] = $translator->trans('Total_membres');
                $strSQL = "SELECT DISTINCT (c.denomination) AS cooperative,
							(SELECT Sum(s.membres_contracte_credit)
							FROM suivi_organisations s
							WHERE s.organisation_id=g.id
								AND s.annee={$annee}
							GROUP BY c.denomination)AS Membres_credit,
							(SELECT Sum(s1.membres_credit_montant)
							FROM suivi_organisations s1
							WHERE s1.organisation_id=g.id
								AND s1.annee={$annee}
							GROUP BY c.denomination)AS Montant,
							(SELECT Count(e.id)
							FROM exploitations e
							WHERE e.id=p.exploitation_id
							AND e.annee_data={$annee}
							GROUP BY c.denomination)AS Total_membres
						FROM tbl_organisations c
						  INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
							INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						WHERE tol.level=1
						GROUP BY cooperative";

                break;
            case 8:
                $titre = 'Part des prêts octroyés par la coopérative [V2] sur le montant total [V1]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['financement'] = $translator->trans('financement');
                $fields['membres_credit'] = $translator->trans('membres_credit');
                $fields['montant'] = $translator->trans('montant');
                $strSQL = "SELECT c.denomination AS cooperative,
									Sum( s.montant_financement ) AS financement,
									Sum( s.membres_contracte_credit ) AS membres_credit,
									Sum( s.membres_credit_montant ) AS montant
								FROM tbl_organisations c
						  			INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  			INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									LEFT JOIN suivi_organisations s ON g.id=s.organisation_id
								WHERE tol.level=1
									AND s.annee ={$annee}
								GROUP BY cooperative";

                break;
            case 9:
                $titre = 'Fréquence des institutions ayant octroyé le crédit, pour l\'ensemble des groupements d\'une coopérative [V2]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['quantite'] = $translator->trans('quantite');
                $fields['institution'] = $translator->trans('institution');
                $strSQL = "SELECT c.denomination AS cooperative,
								COUNT( s.institution_financement_credit_org_id ) AS quantite,
								(SELECT DISTINCT (institution)
				 				FROM tbl_institutions_financieres tif
								WHERE tif.id = s.institution_financement_credit_org_id) AS institution
								FROM tbl_organisations c
						  			INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  			INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									LEFT JOIN suivi_organisations s ON g.id=s.organisation_id
								WHERE tol.level=1
									AND s.annee={$annee}
								GROUP BY cooperative, institution";

                break;
            case 10:
                $titre = 'Montant total des crédits contractés (octroyés) par la coopérative à ses groupements [V2]';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['credit'] = $translator->trans('credit');
                $strSQL = "SELECT c.denomination As cooperative,
									Sum( s.credit_pour_organisation_montant ) AS credit
								FROM tbl_organisations c
						  			INNER JOIN tbl_organisations g ON c.id = g.parent_id
						  			INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
									LEFT JOIN suivi_organisations s ON g.id=s.organisation_id
								WHERE tol.level=1
									AND s.annee={$annee}
								GROUP BY cooperative";

                break;

            default:
                break;
        }

        /* execution requête et envoi de l'array  */
        if (!empty($strSQL)) {
            $stmt = $em->getConnection()->prepare($strSQL);
            $stmt->execute();

            return [
                'fields' => $fields,
                'titre' => $titre,
                'recordset' => $stmt->fetchAll(),
            ];
        }
    }

    private function getPlanification(TranslatorInterface $translator, EntityManagerInterface $em, string $numero, string $annee): array
    {
        $titre = 'Sans titre';
        $fields = [];

        switch ($numero) {
            case 2:
                $titre = 'Surface emblavée par culture, par coopérative [351](année ' . $annee . ')';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['filiere_principale'] = $translator->trans('filiere_principale');
                $fields['surface'] = $translator->trans('surface');
                $strSQL = "SELECT c.denomination AS cooperative,
                    tcult.culture AS filiere_principale,
                            Sum(f.surface) AS surface
                        FROM tbl_organisations c
                            INNER JOIN tbl_organisations g ON c.id = g.parent_id
                            INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
                            INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
                            INNER JOIN exploitations e ON p.exploitation_id = e.id
                            INNER JOIN filieres f ON e.id=f.exploitation_id
                            INNER JOIN tbl_cultures tcult ON f.filiere_principale_id=tcult.id
                        WHERE f.annee={$annee} AND tol.level=1
                        GROUP BY cooperative, filiere_principale";

                break;
            case 3:
                $titre = 'Rendement moyens des cultures et des produits animaux, par coopérative [351](annee ' . $annee . ')';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['filiere_principale'] = $translator->trans('filiere_principale');
                $fields['surface'] = $translator->trans('surface');
                $fields['production'] = $translator->trans('production');
                $strSQL = "SELECT c.denomination AS cooperative,
						tcult.culture AS filiere_principale,
						Sum(f.surface) AS surface,
						Sum(f.quantite_produit) AS production
					  FROM tbl_organisations c
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
						INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN exploitations e ON p.exploitation_id = e.id
						INNER JOIN filieres f ON e.id=f.exploitation_id
						INNER JOIN tbl_cultures tcult ON f.filiere_principale_id=tcult.id
					  WHERE f.annee={$annee} AND tol.level=1
					  GROUP BY cooperative, filiere_principale";

                break;
            case 4:
                $titre = 'Nombre et pourcentage des produits commercialisés par production [351] (ne pas confondre production récoltée et produit commercialiser, ils peuvent correspondre ou non. Si non, il faut un coefficient de conversion)(annee ' . $annee . ')';
                $fields['cooperative'] = $translator->trans('cooperative');
                $fields['produit_commercialise'] = $translator->trans('produit_commercialise');
                $fields['quantite_commercialise'] = $translator->trans('quantite_commercialise');
                $strSQL = "SELECT c.denomination AS cooperative,
						tp.produit AS produit_commercialise,
						Sum(fc.quantite_commercialise) AS quantite_commercialise
					  FROM tbl_organisations c
						INNER JOIN tbl_organisations g ON c.id = g.parent_id
						INNER JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
						INNER JOIN tbl_organisation_level tol ON c.level_id=tol.id
						INNER JOIN exploitations e ON p.exploitation_id = e.id
						INNER JOIN filieres f ON e.id=f.exploitation_id
						INNER JOIN filiere_commercialisation fc ON f.id=fc.filiere_id
						INNER JOIN tbl_produits tp ON fc.produit_commercialise_id=tp.id
					  WHERE f.annee={$annee} AND tol.level=1
					  GROUP BY cooperative, produit_commercialise";

                break;
            case 5:
                $titre = 'Nombre et pourcentage des besoins en formation par occurrence des thèmes [9638](annee ' . $annee . ')';
                $fields['theme_formation'] = $translator->trans('theme_formation');
                $fields['ocurrences'] = $translator->trans('ocurrences');
                $strSQL = "SELECT ttf.theme_formation,
						Count(fb.id) AS ocurrences
					FROM formations_besoin fb INNER JOIN tbl_themes_formations ttf ON fb.formation_id=ttf.id
					WHERE fb.annee={$annee}
					GROUP BY theme_formation;";

                break;
            case 6:
                $titre = 'Nombre et pourcentage des occurrences de contraintes déclarées [90](annee ' . $annee . ')';
                $fields['contraint'] = $translator->trans('contraint');
                $fields['ocurrences'] = $translator->trans('ocurrences');
                $strSQL = "SELECT tc.contraint,
						Count(cd.id) AS ocurrences
					FROM contraintes_developpement cd INNER JOIN tbl_contraints tc ON cd.contrainte_id=tc.id
					WHERE cd.annee={$annee}
					GROUP BY contraint";

                break;
            case 7:
                $titre = 'Nombre et pourcentage des occurrences des attentes en service/appui déclarées [9639](annee ' . $annee . ')';
                $fields['service'] = $translator->trans('service');
                $fields['ocurrences'] = $translator->trans('ocurrences');
                $strSQL = "SELECT ts.service, COUNT(sb.id) AS ocurrences
					FROM services_besoin sb INNER JOIN tbl_services ts ON sb.service_id=ts.id
					WHERE sb.annee={$annee}
					GROUP BY service;";

                break;

            default:
                break;
        }

        /* execution requête et envoi de l'array  */
        $stmt = $em->getConnection()->prepare($strSQL);
        $stmt->execute();

        return [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $stmt->fetchAll(),
        ];
    }

    private function getspecialF0(TranslatorInterface $translator, EntityManagerInterface $em)
    {
        // Todo: Remove the use of $_POST
        $parametros = $_POST['rapports_special_f0'];
        $cooperative_id = $parametros['Denomination'];
        $annee = $parametros['exploitationListAnnee'];
        $strWhere = '';

        if (!empty($cooperative_id)) {
            $strWhere = " AND c.id='" . $cooperative_id . "'";
        }
        /* construction de la requete  */
        /*----------------------------------------------*/
        $strSql = "SELECT c.denomination AS cooperative,
					g.denomination AS groupement,
					e.numero_membre,
					pers.nom,
					pers.prenom,
					e.annee_adhesion,
					tec.etat_civil,
					e.personnes_charge,
					e.nombre_fils,
					e.nombre_filles,
					e.fils_scolarises,
					e.filles_scolarisess AS filles_scolarisees,
					if(h.toiture=1,'paille',if(h.toiture=2,'tuile','tole'))AS toiture,
					if(h.mur=1,'pise',if(h.mur=2,'adobe','brique'))AS mur,
					pd.kirundi_lire,
					pd.kirundi_ecrire,
					pd.francais_lire,
					pd.francais_ecrire,
					pd.primaire,
					pd.secondaire,
					pd.diplome,
					pd.superieur,
					tdes.domain,
					tp.profession AS prof_principale,
					tpr.profession AS prof_secondaire
			 FROM tbl_organisations c
				LEFT JOIN tbl_organisations g ON c.id = g.parent_id
				LEFT JOIN pivot_exploitation_organisations p ON g.id = p.organisation_id
				LEFT JOIN exploitations e ON p.exploitation_id = e.id
				LEFT JOIN tbl_organisation_level tol ON c.level_id=tol.id
				LEFT JOIN personnes pers ON pers.exploitation_id = e.id
				LEFT JOIN personne_details pd ON pd.personnes_id=pers.id
				LEFT JOIN tbl_etat_civil tec ON pers.etat_civil_id=tec.id
				LEFT JOIN habitat h ON h.exploitation_id=e.id
				LEFT JOIN tbl_professions tp ON pd.profession_principale_id=tp.id
				LEFT JOIN tbl_professions tpr ON pd.profession_secondaire_id=tpr.id
				LEFT JOIN tbl_domaines_etudes_superieures tdes ON pd.domaine_id=tdes.id
			 WHERE tol.level=1
			 		{$strWhere}
			 		AND pd.annee=(SELECT max({$annee}) FROM personne_details GROUP BY pd.personnes_id)
					AND h.annee={$annee}
			 ORDER BY cooperative, groupement";

        /*-----------------------------------------------*/
        $titre = "Rapport sur la fiche F0 Annee={$annee}";
        $fields['cooperative'] = $translator->trans('cooperative');
        $fields['groupement'] = $translator->trans('groupement');
        $fields['numero_membre'] = $translator->trans('numero_membre');
        $fields['nom'] = $translator->trans('nom');
        $fields['prenom'] = $translator->trans('prenom');
        $fields['annee_adhesion'] = $translator->trans('annee_adhesion');
        $fields['etat_civil'] = $translator->trans('etat_civil');
        $fields['personnes_charge'] = $translator->trans('personnes_charge');
        $fields['nombre_fils'] = $translator->trans('nombre_fils');
        $fields['nombre_filles'] = $translator->trans('nombre_filles');
        $fields['fils_scolarises'] = $translator->trans('fils_scolarises');
        $fields['filles_scolarisees'] = $translator->trans('filles_scolarisees');
        $fields['toiture'] = $translator->trans('toiture');
        $fields['mur'] = $translator->trans('mur');
        $fields['kirundi_lire'] = $translator->trans('kirundi_lire');
        $fields['kirundi_ecrire'] = $translator->trans('kirundi_ecrire');
        $fields['francais_lire'] = $translator->trans('francais_lire');
        $fields['francais_ecrire'] = $translator->trans('francais_ecrire');
        $fields['primaire'] = $translator->trans('primaire');
        $fields['secondaire'] = $translator->trans('secondaire');
        $fields['diplome'] = $translator->trans('diplome');
        $fields['superieur'] = $translator->trans('superieur');
        $fields['domain'] = $translator->trans('domain');
        $fields['prof_principale'] = $translator->trans('prof_principale');
        $fields['prof_secondaire'] = $translator->trans('prof_secondaire');

        /* execution requête et envoi de l'array  */
        $stmt = $em->getConnection()->prepare($strSql);
        $stmt->execute();

        return [
            'fields' => $fields,
            'titre' => $titre,
            'recordset' => $stmt->fetchAll(),
        ];
    }
}
