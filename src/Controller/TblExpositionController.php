<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblExposition;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblExposition controller.
 *
 * @Route("/{_locale}/references/tblexposition")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblExpositionController extends AbstractController
{
    /**
     * Deletes a TblExposition entity.
     *
     * @Route("/{id}", name="references_tblexposition_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblExposition $tblExposition)
    {
        $form = $this->createDeleteForm($tblExposition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblExposition);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblexposition_index');
    }

    /**
     * Displays a form to edit an existing TblExposition entity.
     *
     * @Route("/{id}/edit", name="references_tblexposition_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblExposition $tblExposition)
    {
        $deleteForm = $this->createDeleteForm($tblExposition);
        $editForm = $this->createForm('App\Form\TblExpositionType', $tblExposition);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblExposition);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblexposition_index', ['id' => $tblExposition->getId()]);
        }

        return $this->render('tblexposition/edit.html.twig', [
            'tblExposition' => $tblExposition,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblExposition entities.
     *
     * @Route("/", name="references_tblexposition_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblExpositions = $em->getRepository(TblExposition::class)->findAll();

        return $this->render('tblexposition/index.html.twig', [
            'tblExpositions' => $tblExpositions,
        ]);
    }

    /**
     * Creates a new TblExposition entity.
     *
     * @Route("/new", name="references_tblexposition_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblExposition = new TblExposition($user);
        $form = $this->createForm('App\Form\TblExpositionType', $tblExposition);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblExposition);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblexposition_index', ['id' => $tblExposition->getId()]);
        }

        return $this->render('tblexposition/new.html.twig', [
            'tblExposition' => $tblExposition,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblExposition entity.
     *
     * @Route("/{id}", name="references_tblexposition_show", methods={"GET"})
     */
    public function showAction(TblExposition $tblExposition)
    {
        $deleteForm = $this->createDeleteForm($tblExposition);

        return $this->render('tblexposition/show.html.twig', [
            'tblExposition' => $tblExposition,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblExposition entity.
     *
     * @param TblExposition $tblExposition The TblExposition entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblExposition $tblExposition)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblexposition_delete', ['id' => $tblExposition->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
