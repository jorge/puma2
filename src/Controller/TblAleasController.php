<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblAleas;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblAleas controller.
 *
 * @Route("/{_locale}/references/tblaleas")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblAleasController extends AbstractController
{
    /**
     * Deletes a TblAleas entity.
     *
     * @Route("/{id}", name="references_tblaleas_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblAleas $tblAlea)
    {
        $form = $this->createDeleteForm($tblAlea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblAlea);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblaleas_index');
    }

    /**
     * Displays a form to edit an existing TblAleas entity.
     *
     * @Route("/{id}/edit", name="references_tblaleas_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblAleas $tblAlea)
    {
        $deleteForm = $this->createDeleteForm($tblAlea);
        $editForm = $this->createForm('App\Form\TblAleasType', $tblAlea);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAlea);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblaleas_index', ['id' => $tblAlea->getId()]);
        }

        return $this->render('tblaleas/edit.html.twig', [
            'tblAlea' => $tblAlea,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblAleas entities.
     *
     * @Route("/", name="references_tblaleas_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblAleas = $em->getRepository(TblAleas::class)->findAll();

        return $this->render('tblaleas/index.html.twig', [
            'tblAleas' => $tblAleas,
        ]);
    }

    /**
     * Creates a new TblAleas entity.
     *
     * @Route("/new", name="references_tblaleas_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblAlea = new TblAleas($user);
        $form = $this->createForm('App\Form\TblAleasType', $tblAlea);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblAlea);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblaleas_index', ['id' => $tblAlea->getId()]);
        }

        return $this->render('tblaleas/new.html.twig', [
            'tblAlea' => $tblAlea,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblAleas entity.
     *
     * @Route("/{id}", name="references_tblaleas_show", methods={"GET"})
     */
    public function showAction(TblAleas $tblAlea)
    {
        $deleteForm = $this->createDeleteForm($tblAlea);

        return $this->render('tblaleas/show.html.twig', [
            'tblAlea' => $tblAlea,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblAleas entity.
     *
     * @param TblAleas $tblAlea The TblAleas entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblAleas $tblAlea)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblaleas_delete', ['id' => $tblAlea->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
