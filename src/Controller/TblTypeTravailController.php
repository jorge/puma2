<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblTypeTravail;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblTypeTravail controller.
 *
 * @Route("/{_locale}/references/tbltypetravail")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblTypeTravailController extends AbstractController
{
    /**
     * Deletes a TblTypeTravail entity.
     *
     * @Route("/{id}", name="references_tbltypetravail_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblTypeTravail $tblTypeTravail)
    {
        $form = $this->createDeleteForm($tblTypeTravail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblTypeTravail);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbltypetravail_index');
    }

    /**
     * Displays a form to edit an existing TblTypeTravail entity.
     *
     * @Route("/{id}/edit", name="references_tbltypetravail_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblTypeTravail $tblTypeTravail)
    {
        $deleteForm = $this->createDeleteForm($tblTypeTravail);
        $editForm = $this->createForm('App\Form\TblTypeTravailType', $tblTypeTravail);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeTravail);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypetravail_index', ['id' => $tblTypeTravail->getId()]);
        }

        return $this->render('tbltypetravail/edit.html.twig', [
            'tblTypeTravail' => $tblTypeTravail,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblTypeTravail entities.
     *
     * @Route("/", name="references_tbltypetravail_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblTypeTravails = $em->getRepository(TblTypeTravail::class)->findAll();

        return $this->render('tbltypetravail/index.html.twig', [
            'tblTypeTravails' => $tblTypeTravails,
        ]);
    }

    /**
     * Creates a new TblTypeTravail entity.
     *
     * @Route("/new", name="references_tbltypetravail_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblTypeTravail = new TblTypeTravail($user);
        $form = $this->createForm('App\Form\TblTypeTravailType', $tblTypeTravail);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblTypeTravail);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbltypetravail_index', ['id' => $tblTypeTravail->getId()]);
        }

        return $this->render('tbltypetravail/new.html.twig', [
            'tblTypeTravail' => $tblTypeTravail,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblTypeTravail entity.
     *
     * @Route("/{id}", name="references_tbltypetravail_show", methods={"GET"})
     */
    public function showAction(TblTypeTravail $tblTypeTravail)
    {
        $deleteForm = $this->createDeleteForm($tblTypeTravail);

        return $this->render('tbltypetravail/show.html.twig', [
            'tblTypeTravail' => $tblTypeTravail,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblTypeTravail entity.
     *
     * @param TblTypeTravail $tblTypeTravail The TblTypeTravail entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblTypeTravail $tblTypeTravail)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbltypetravail_delete', ['id' => $tblTypeTravail->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
