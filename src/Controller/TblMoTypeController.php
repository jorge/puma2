<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblMoType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblMoType controller.
 *
 * @Route("/{_locale}/references/tblmotype")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblMoTypeController extends AbstractController
{
    /**
     * Deletes a TblMoType entity.
     *
     * @Route("/{id}", name="references_tblmotype_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblMoType $tblMoType)
    {
        $form = $this->createDeleteForm($tblMoType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblMoType);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblmotype_index');
    }

    /**
     * Displays a form to edit an existing TblMoType entity.
     *
     * @Route("/{id}/edit", name="references_tblmotype_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblMoType $tblMoType)
    {
        $deleteForm = $this->createDeleteForm($tblMoType);
        $editForm = $this->createForm('App\Form\TblMoTypeType', $tblMoType);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblMoType);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblmotype_index', ['id' => $tblMoType->getId()]);
        }

        return $this->render('tblmotype/edit.html.twig', [
            'tblMoType' => $tblMoType,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblMoType entities.
     *
     * @Route("/", name="references_tblmotype_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblMoTypes = $em->getRepository(TblMoType::class)->findAll();

        return $this->render('tblmotype/index.html.twig', [
            'tblMoTypes' => $tblMoTypes,
        ]);
    }

    /**
     * Creates a new TblMoType entity.
     *
     * @Route("/new", name="references_tblmotype_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblMoType = new TblMoType($user);
        $form = $this->createForm('App\Form\TblMoTypeType', $tblMoType);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblMoType);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblmotype_index', ['id' => $tblMoType->getId()]);
        }

        return $this->render('tblmotype/new.html.twig', [
            'tblMoType' => $tblMoType,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblMoType entity.
     *
     * @Route("/{id}", name="references_tblmotype_show", methods={"GET"})
     */
    public function showAction(TblMoType $tblMoType)
    {
        $deleteForm = $this->createDeleteForm($tblMoType);

        return $this->render('tblmotype/show.html.twig', [
            'tblMoType' => $tblMoType,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblMoType entity.
     *
     * @param TblMoType $tblMoType The TblMoType entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblMoType $tblMoType)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblmotype_delete', ['id' => $tblMoType->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
