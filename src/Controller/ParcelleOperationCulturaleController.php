<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\ParcelleHistoriques;
use App\Entity\ParcelleOperationCulturale;
use App\Entity\TblOperationCulturaleType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
// use App\Form\ParcelleOperationCulturaleType;
use Symfony\Component\Routing\Annotation\Route;

/**
 * ParcelleOperationCulturale controller.
 *
 * @Route("/{_locale}/parcelleoperationculturale")
 */

use function count;

final class ParcelleOperationCulturaleController extends AbstractController
{
    /**
     * Deletes a ParcelleOperationCulturale entity.
     *
     * @Route("/{id}", name="parcelleoperationculturale_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, ParcelleOperationCulturale $parcelleOperationCulturelle)
    {
        $form = $this->createDeleteForm($parcelleOperationCulturelle);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $historique = $parcelleOperationCulturelle->getParcelleHistorique();
            $em = $this->getDoctrine()->getManager();
            $em->remove($parcelleOperationCulturelle);
            $em->flush();
        }

        $this->addFlash(
            'success',
            'Opération suppprimée'
        );

        return $this->redirectToRoute(
            'references_parcellehistoriques_show',
            [
                'id' => $historique->getId(),
            ]
        );
    }

    /**
     * Displays a form to edit an existing ParcelleOperationCulturale entity.
     *
     * @Route("/{id}/edit", name="parcelleoperationculturale_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, ParcelleOperationCulturale $parcelleOperationCulturelle)
    {
        $deleteForm = $this->createDeleteForm($parcelleOperationCulturelle);
        $editForm = $this->createForm('App\Form\ParcelleOperationCulturaleType', $parcelleOperationCulturelle);
        $editForm->handleRequest($request);
        $em = $this->getDoctrine()->getManager();

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($parcelleOperationCulturelle);
            $em->flush();

            $this->addFlash(
                'success',
                'Opération mise à jour'
            );

            return $this->redirectToRoute(
                'references_parcellehistoriques_show',
                [
                    'id' => $parcelleOperationCulturelle->getParcelleHistorique()->getId(),
                ]
            );
        }

        return $this->render('parcelleOperationCulturale/edit.html.twig', [
            'parcelleOperationCulturelle' => $parcelleOperationCulturelle,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),

            'exploitation_id' => $parcelleOperationCulturelle->getParcelleHistorique()->getParcelle()->getExploitation()->getId(),
            'annee' => $parcelleOperationCulturelle->getParcelleHistorique()->getAnnee(),
            'exploitation' => $parcelleOperationCulturelle->getParcelleHistorique()->getParcelle()->getExploitation(),
            'seul' => $this->getArrayByType(0), // seul
            'intran' => $this->getArrayByType(1), // intrans
            'extrant' => $this->getArrayByType(2), // extrants
        ]);
    }

    /**
     * Creates a new ParcelleOperationCulturale entity.
     *
     * @Route("/{id}/new", name="parcelleoperationculturale_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request, ParcelleHistoriques $historique)
    {
        $em = $this->getDoctrine()->getManager();
        $tblOperationCulturaleType = $em->getRepository(TblOperationCulturaleType::class)->findAll();

        $user = $this->getUser();
        $parcelleOperationCulturale = new ParcelleOperationCulturale($user);
        $parcelleOperationCulturale->setParcelleHistorique($historique);
        // $parcelleOperationCulturale->addParcelle setParcelleHistorique($historique); TODO
        $form = $this->createForm('App\Form\ParcelleOperationCulturaleType', $parcelleOperationCulturale);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($parcelleOperationCulturale);
            $em->flush();

            $this->addFlash(
                'success',
                'Opération ajoutée'
            );

            return $this->redirectToRoute(
                'references_parcellehistoriques_show',
                [
                    'id' => $historique->getId(),
                ]
            );
        }

        return $this->render('parcelleOperationCulturale/new.html.twig', [
            'parcelleOperationCulturelle' => $parcelleOperationCulturale,
            'form' => $form->createView(),
            'exploitation_id' => $historique->getParcelle()->getExploitation()->getId(),
            'annee' => $historique->getAnnee(),
            'exploitation' => $historique->getParcelle()->getExploitation(),
            'tblOperationCulturaleType' => $tblOperationCulturaleType,
            'seul' => $this->getArrayByType(0), // seul
            'intran' => $this->getArrayByType(1), // intrans
            'extrant' => $this->getArrayByType(2), // extrants
        ]);
    }

    /**
     * Creates a form to delete a ParcelleOperationCulturale entity.
     *
     * @param ParcelleOperationCulturale $parcelleOperationCulturelle The ParcelleOperationCulturale entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(ParcelleOperationCulturale $parcelleOperationCulturelle)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('parcelleoperationculturale_delete', ['id' => $parcelleOperationCulturelle->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }

    private function getArrayByType($varType)
    {
        $em = $this->getDoctrine()->getManager();
        $objArray = $em->getRepository(TblOperationCulturaleType::class)->findByType($varType); // autre
        $IdArray = [];
        $cpt = count($objArray);

        for ($i = 0; $i < $cpt; ++$i) {
            $IdArray[] = $objArray[$i]->getId();
        }
        //    $serializer = $this->get('serializer');
        //    $countries = $this->getDoctrine()->getRepository("QSCORBundle:CountryMaps")->findAll();
        //    $jsonCountries = $serializer->serialize($countries, 'json');

        return $IdArray;
    }
}
