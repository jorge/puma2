<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\MoyensTransport;
use App\Entity\PivotExploitationOrganisations;
use App\Entity\Exploitations;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * MoyensTransport controller.
 *
 * @Route("/{_locale}/references/moyenstransport")
 */

use Symfony\Component\Routing\Annotation\Route;

final class MoyensTransportController extends AbstractController
{
    /**
     * Deletes a MoyensTransport entity.
     *
     * @Route("/{id}", name="references_moyenstransport_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, MoyensTransport $moyensTransport)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $moyensTransport->getAnnee();
        $exploitation = $moyensTransport->getExploitation();
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $form = $this->createDeleteForm($moyensTransport, [
            'coop_id' => $coop->getId(),
            'group_id' => $groupe->getId(),
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
        ]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $moyensTransport->setRadie(TRUE);
            $em->persist($moyensTransport);
            $em->flush();
        }

        return $this->redirectToRoute('saisie_f1_membre', [
            'id' => $exploitation->getId(),
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
            'group_id' => $groupe->getId(),
            'coop_id' => $coop->getId(),
        ]);
    }

    /**
     * Displays a form to edit an existing MoyensTransport entity.
     *
     * @Route("/{id}/edit", name="references_moyenstransport_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, MoyensTransport $moyensTransport)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $moyensTransport->getAnnee();
        $exploitation = $moyensTransport->getExploitation();
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $deleteForm = $this->createDeleteForm($moyensTransport);
        $editForm = $this->createForm('App\Form\MoyensTransportType', $moyensTransport);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em->persist($moyensTransport);
            $em->flush();

            return $this->redirectToRoute('saisie_f1_membre', [
                'id' => $exploitation->getId(),
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
                'group_id' => $groupe->getId(),
                'coop_id' => $coop->getId(),
            ]);
        }

        return $this->render('moyenstransport/edit.html.twig', [
            'moyensTransport' => $moyensTransport,
            'edit_form' => $editForm->createView(),
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
            'group_id' => $groupe->getId(),
            'coop_id' => $coop->getId(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a new MoyensTransport entity.
     *
     * @Route("/new", name="references_moyenstransport_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $annee = $_GET['annee'];
        $exploitation_id = $_GET['exploitation_id'];
        $exploitation = $em->getRepository(Exploitations::class)->find($exploitation_id);
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        $user = $this->getUser();
        $moyensTransport = new MoyensTransport($user);
        $moyensTransport->setExploitation($exploitation);

        if (isset($annee)) {
            $moyensTransport->setAnnee($annee);
        }
        $form = $this->createForm('App\Form\MoyensTransportType', $moyensTransport);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $moyensTransport->setRadie(FALSE);
            $em->persist($moyensTransport);
            $em->flush();

            return $this->redirectToRoute('saisie_f1_membre', ['id' => $exploitation_id,
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
                'group_id' => $groupe->getId(),
                'coop_id' => $coop->getId(), ]);
        }

        return $this->render('moyenstransport/new.html.twig', [
            'moyensTransport' => $moyensTransport,
            'form' => $form->createView(),
            'exploitation_id' => $exploitation->getId(),
            'exploitation' => $exploitation,
            'annee' => $annee,
            'group_id' => $groupe->getId(),
            'coop_id' => $coop->getId(),
        ]);
    }

    /**
     * Creates a form to delete a MoyensTransport entity.
     *
     * @param MoyensTransport $moyensTransport The MoyensTransport entity
     * @param mixed $options
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(MoyensTransport $moyensTransport, $options = [])
    {
        $em = $this->getDoctrine()->getManager();
        $exploitation = $moyensTransport->getExploitation();
        $annee = $_GET['annee'];
        $pivot = $em->getRepository(PivotExploitationOrganisations::class)->findOneBy(
            ['exploitation' => $exploitation]
        );

        $groupe = $pivot->getOrganisation();
        $coop = $groupe->getParent();

        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_moyenstransport_delete', [
                'id' => $moyensTransport->getId(),
                'coop_id' => $coop->getId(),
                'group_id' => $groupe->getId(),
                'exploitation_id' => $exploitation->getId(),
                'exploitation' => $exploitation,
                'annee' => $annee,
            ]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
