<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblProduits;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblProduits controller.
 *
 * @Route("/{_locale}/references/tblproduits")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblProduitsController extends AbstractController
{
    /**
     * Deletes a TblProduits entity.
     *
     * @Route("/{id}", name="references_tblproduits_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblProduits $tblProduit)
    {
        $form = $this->createDeleteForm($tblProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tblproduits_index');
    }

    /**
     * Displays a form to edit an existing TblProduits entity.
     *
     * @Route("/{id}/edit", name="references_tblproduits_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblProduits $tblProduit)
    {
        $deleteForm = $this->createDeleteForm($tblProduit);
        $editForm = $this->createForm('App\Form\TblProduitsType', $tblProduit);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblproduits_index', ['id' => $tblProduit->getId()]);
        }

        return $this->render('tblproduits/edit.html.twig', [
            'tblProduit' => $tblProduit,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblProduits entities.
     *
     * @Route("/", name="references_tblproduits_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblProduits = $em->getRepository(TblProduits::class)->findAll();

        return $this->render('tblproduits/index.html.twig', [
            'tblProduits' => $tblProduits,
        ]);
    }

    /**
     * Creates a new TblProduits entity.
     *
     * @Route("/new", name="references_tblproduits_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblProduit = new TblProduits($user);
        $form = $this->createForm('App\Form\TblProduitsType', $tblProduit);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblProduit);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tblproduits_index', ['id' => $tblProduit->getId()]);
        }

        return $this->render('tblproduits/new.html.twig', [
            'tblProduit' => $tblProduit,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblProduits entity.
     *
     * @Route("/{id}", name="references_tblproduits_show", methods={"GET"})
     */
    public function showAction(TblProduits $tblProduit)
    {
        $deleteForm = $this->createDeleteForm($tblProduit);

        return $this->render('tblproduits/show.html.twig', [
            'tblProduit' => $tblProduit,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblProduits entity.
     *
     * @param TblProduits $tblProduit The TblProduits entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblProduits $tblProduit)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tblproduits_delete', ['id' => $tblProduit->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
