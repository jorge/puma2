<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Controller;

use App\Entity\TblLabourAnimaux;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * TblLabourAnimaux controller.
 *
 * @Route("/{_locale}/references/tbllabouranimaux")
 */

use Symfony\Component\Routing\Annotation\Route;

final class TblLabourAnimauxController extends AbstractController
{
    /**
     * Deletes a TblLabourAnimaux entity.
     *
     * @Route("/{id}", name="references_tbllabouranimaux_delete", methods={"DELETE"})
     */
    public function deleteAction(Request $request, TblLabourAnimaux $tblLabourAnimaux)
    {
        $form = $this->createDeleteForm($tblLabourAnimaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($tblLabourAnimaux);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );
        }

        return $this->redirectToRoute('references_tbllabouranimaux_index');
    }

    /**
     * Displays a form to edit an existing TblLabourAnimaux entity.
     *
     * @Route("/{id}/edit", name="references_tbllabouranimaux_edit", methods={"GET", "POST"})
     */
    public function editAction(Request $request, TblLabourAnimaux $tblLabourAnimaux)
    {
        $deleteForm = $this->createDeleteForm($tblLabourAnimaux);
        $editForm = $this->createForm('App\Form\TblLabourAnimauxType', $tblLabourAnimaux);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblLabourAnimaux);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbllabouranimaux_index', ['id' => $tblLabourAnimaux->getId()]);
        }

        return $this->render('tbllabouranimaux/edit.html.twig', [
            'tblLabourAnimaux' => $tblLabourAnimaux,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Lists all TblLabourAnimaux entities.
     *
     * @Route("/", name="references_tbllabouranimaux_index", methods={"GET"})
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $tblLabourAnimauxes = $em->getRepository(TblLabourAnimaux::class)->findAll();

        return $this->render('tbllabouranimaux/index.html.twig', [
            'tblLabourAnimauxes' => $tblLabourAnimauxes,
        ]);
    }

    /**
     * Creates a new TblLabourAnimaux entity.
     *
     * @Route("/new", name="references_tbllabourAnimaux_new", methods={"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        $user = $this->getUser();
        $tblLabourAnimaux = new TblLabourAnimaux($user);
        $form = $this->createForm('App\Form\TblLabourAnimauxType', $tblLabourAnimaux);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($tblLabourAnimaux);
            $em->flush();
            $this->addFlash(
                'success',
                'Opération réussie'
            );

            return $this->redirectToRoute('references_tbllabouranimaux_index', ['id' => $tblLabourAnimaux->getId()]);
        }

        return $this->render('tbllabouranimaux/new.html.twig', [
            'tblLabourAnimaux' => $tblLabourAnimaux,
            'form' => $form->createView(),
        ]);
    }

    /**
     * Finds and displays a TblLabourAnimaux entity.
     *
     * @Route("/{id}", name="references_tbllabouranimaux_show", methods={"GET"})
     */
    public function showAction(TblLabourAnimaux $tblLabourAnimaux)
    {
        $deleteForm = $this->createDeleteForm($tblLabourAnimaux);

        return $this->render('tbllabouranimaux/show.html.twig', [
            'tblLabourAnimaux' => $tblLabourAnimaux,
            'delete_form' => $deleteForm->createView(),
        ]);
    }

    /**
     * Creates a form to delete a TblLabourAnimaux entity.
     *
     * @param TblLabourAnimaux $tblLabourAnimaux The TblLabourAnimaux entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(TblLabourAnimaux $tblLabourAnimaux)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('references_tbllabouranimaux_delete', ['id' => $tblLabourAnimaux->getId()]))
            ->setMethod('DELETE')
            ->getForm();
    }
}
