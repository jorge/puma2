<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\MobilisationsCommunautaires;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class MobilisationsCommunautairesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MobilisationsCommunautaires::class);
    }

    public function updateRadieStatus($suiviId, $newStatus)
    {
        $qb = $this->createQueryBuilder('u');
        $q = $qb->update()
            ->set('u.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('u.suiviOrganisation = ?2')
            ->setParameter(2, $suiviId)
            ->getQuery();
        $p = $q->execute();
    }
}
