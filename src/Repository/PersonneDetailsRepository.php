<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PersonneDetails;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class PersonneDetailsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PersonneDetails::class);
    }

    public function getDetailsForPersonneAndAnnee($personne_id, $annee)
    {
        return $this->findBy([
            'personne' => $personne_id,
            'annee' => $annee,
        ]);
    }

    public function trouvertPersonneDetails($personne_id)
    {
        $q = $this->createQueryBuilder('p')
            ->where('p.personnes = :personne_id')
            ->setParameter('personne_id', $personne_id);

        return $q->getQuery()->getResult();
    }
}
