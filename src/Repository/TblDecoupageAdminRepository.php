<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblDecoupageAdmin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblDecoupageAdminRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblDecoupageAdmin::class);
    }

    public function getAllCollines($comm_id)
    {
        $q = $this->createQueryBuilder('t')
            ->innerJoin('t.level', 'l')
            ->where('l.level = 3')
            ->andWhere('t.parent = :parent_id')
            ->setParameter('parent_id', $comm_id)
            ->orderBy('t.denomination', 'ASC');
        $q->getQuery()->getResult();

        return $q;
    }

    public function getAllCommunes($prov_id)
    {
        $q = $this->createQueryBuilder('t')
            ->innerJoin('t.level', 'l')
            ->where('l.level = 2')
            ->andWhere('t.parent = :parent_id')
            ->setParameter('parent_id', $prov_id)
            ->orderBy('t.denomination', 'ASC');
        $q->getQuery()->getResult();

        return $q;
    }

    public function getAllProvinces()
    {
        $q = $this->createQueryBuilder('t')
            ->innerJoin('t.level', 'g')
            ->where('g.level = 1')
            ->orderBy('t.denomination', 'ASC');
        $q->getQuery()->getResult();

        return $q;
    }

    public function getCollinaById($decoupageAdmin_id)
    {
        $q = $this->createQueryBuilder('t')
            ->where('t.id = :decoupage_id')
            ->setParameter('decoupage_id', $decoupageAdmin_id);

        return $q->getQuery()->getResult();
    }
}
