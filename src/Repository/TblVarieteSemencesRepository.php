<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblVarieteSemences;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblVarieteSemencesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblVarieteSemences::class);
    }

    public function prendreVariete($culture_id)
    {
        return $this->createQueryBuilder('t')
            ->where('t.culture=:culture')
            ->setParameter('culture', $culture_id);
    }
}
