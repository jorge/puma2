<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblProduits;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblProduitsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblProduits::class);
    }

    public function prendreListe($elevage)
    {
        return $this->createQueryBuilder('t')
            ->innerJoin('t.culture', 'c')
            ->where('c.elevage=:me')
            ->setParameter('me', $elevage);
    }

    public function prendreProduits($espece_id)
    {
        return $this->createQueryBuilder('t')
            ->where('t.culture=:espece')
            ->setParameter('espece', $espece_id);
    }
}
