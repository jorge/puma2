<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblOrganisations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblOrganisationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblOrganisations::class);
    }

    public function filtreCooperatives($localisation)
    {
        $province = $localisation->getParent()->getParent();
        $q = $this->createQueryBuilder('c')
            ->innerJoin('c.level', 'g')
            ->innerJoin('c.decoupageAdmin', 'da', 'WITH', 'c.decoupageAdmin=da.id')
            ->innerJoin('da.parent', 'pa', 'WITH', 'da.parent=pa.id')
            ->where('g.level = 1')
            ->andWhere('pa.id = :localisation_id')
            ->setParameter('localisation_id', $province->getId())
            ->orderBy('c.denomination', 'ASC');
        $q->getQuery()->getResult();

        return $q;
    }

    public function getAllCooperatives()
    {
        // attentien pas filtre sur radié
        $q = $this->createQueryBuilder('c')
            ->innerJoin('c.level', 'g')
            ->where('g.level = 1')
            ->orderBy('c.denomination', 'ASC');
        $q->getQuery()->getResult();

        return $q;
    }

    public function getAllFilsOrganisation($level_id)
    {
        $q = $this->createQueryBuilder('t')
            ->where('t.level = :level_id')
            ->setParameter('level_id', $level_id);
        $q->getQuery()->getResult();

        return $q;
    }

    public function getAllGroupements($coop_id)
    {
        $qb = $this->getAllGroupementsQB($coop_id);

        return $qb->getQuery()->getResult();
    }

    public function getAllGroupementsQB($coop_id)
    {
        return $this
            ->createQueryBuilder('c')
            ->innerJoin('c.level', 'g')
            ->where('g.level = 2')
            ->andWhere('c.parent = :parent_id')
            ->setParameter('parent_id', $coop_id)
            ->orderBy('c.denomination', 'ASC');
    }

    public function getCooperativeById($coop_id)
    {
        $q = $this->createQueryBuilder('c')
            ->innerJoin('c.level', 'g')
            ->where('g.level = 1')
            ->andWhere('c.id = :coop_id')
            ->setParameter('coop_id', $coop_id);
        $q->getQuery()->getResult();

        return $q;
    }

    public function getGroupementById($group_id)
    {
        $q = $this->createQueryBuilder('c')
            ->innerJoin('c.level', 'g')
            ->where('g.level = 2')
            ->andWhere('c.id = :group_id')
            ->setParameter('group_id', $group_id);
        $q->getQuery()->getResult();

        return $q;
    }

    public function getGroupementForExploitation($exp)
    {
        $pivot = $this->findOneBy(['exploitation' => $exp]);

        return $pivot->getOrganisation();
    }

    public function getOneOrSubset($options, $offset = 0, $nb_rec = 1000)
    {
        $entity_id = null;

        if (null !== $options['data']->getOrganisation()) {
            $entity_id = $options['data']->getOrganisation()->getId();
        }

        if (null !== $entity_id) {
            $q = $this->createQueryBuilder('e')
                ->where('e.id = :entity_id')
                ->setParameter('entity_id', $entity_id);
            $q->getQuery()->getResult();
        } else {
            $q = $this->createQueryBuilder('e')
                ->setFirstResult($offset)
                ->setMaxResults($nb_rec);
            $q->getQuery()->getResult();
        }

        return $q;
    }

    public function getParentId($group_id)
    {
        $q = $this->createQueryBuilder('t')
            ->select('p.id')
            ->innerJoin('t.parent', 'p')
            ->Where('t.id = :group_id')
            ->setParameter('group_id', $group_id);
        $result = $q->getQuery()->getResult();

        return $result[0]['id'];
    }
}
