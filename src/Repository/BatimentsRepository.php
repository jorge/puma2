<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Batiments;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class BatimentsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Batiments::class);
    }

    public function getAllBatimentsForExploitation($expl_id, $annee)
    {
        $q = $this->createQueryBuilder('b')
            ->where('b.exploitation = :expl_id')
            ->setParameter('expl_id', $expl_id);
        $q->getQuery()->getResult();

        return $q;
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
