<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblOperationCulturaleTypeCategory;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblOperationCulturaleTypeCategoryRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblOperationCulturaleTypeCategory::class);
    }
}
