<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Outillage;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class OutillageRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Outillage::class);
    }

    public function getAllOutillageForExploitation($expl_id, $annee)
    {
        $q = $this->createQueryBuilder('m')
            ->where('m.exploitation_id = :expl_id')
            ->setParameter('expl_id', $expl_id);
        $q->getQuery()->getResult();

        return $q;
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
