<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblCreditObjet;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblCreditObjetRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblCreditObjet::class);
    }

    public function getOneObjetCredit($typecredit)
    {
        $q = $this->createQueryBuilder('t')
            ->where('t.creditobjet = :type_credit')
            ->setParameter('type_credit', $typecredit);
        $q->getQuery()->getResult();

        return $q;
    }
}
