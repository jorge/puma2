<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\SynchroStatus;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Exception;

use function count;

final class SynchroStatusRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SynchroStatus::class);
    }

    /**
     * Return the only element of AppBundle:SynchroStatus. An exception is
     * thrown is the number of element of this table is not 1.
     */
    public function findUnique()
    {
        $synchroStatusAll = $this->findAll();
        $synchroStatusNbr = count($synchroStatusAll);

        if (1 !== $synchroStatusNbr) {
            throw new Exception(
                'The number of AppBundle:SynchroStatus is ' . $synchroStatusNbr,
                1
            );
        }

        return $synchroStatusAll[0];
    }
}
