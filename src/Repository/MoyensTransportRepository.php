<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\MoyensTransport;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class MoyensTransportRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MoyensTransport::class);
    }

    public function getAllMoyensTransportForExploitation($expl_id, $annee)
    {
        $q = $this->createQueryBuilder('m')
            ->where('m.exploitation_id = :expl_id')
            ->setParameter('expl_id', $expl_id);
        $q->getQuery()->getResult();

        return $q;
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
