<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\CommercialisationProduits;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class CommercialisationProduitsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CommercialisationProduits::class);
    }

    public function prendreAnimales($explo_id, $annee)
    {
        $qb = $this->createQueryBuilder('c');
        $query = $this->_em->createQuery();
        $requete = "SELECT c
					FROM App:CommercialisationProduits c
					INNER JOIN App:TblProduits tp WITH c.produit = tp.id
					INNER JOIN App:TblCultures tc WITH tp.culture = tc.id
					WHERE tc.elevage =1 AND c.radie=0
					AND c.exploitation = '" . $explo_id . "'
					AND c.annee = '" . $annee . "'";
        $dql = $requete;
        $query->setDql($dql);

        return $query->getResult();
    }

    public function prendreVegetales($explo_id, $annee)
    {
        $query = $this->_em->createQuery();
        $requete = "SELECT c
					FROM App:CommercialisationProduits c
					INNER JOIN App:TblProduits tp WITH c.produit = tp.id
					INNER JOIN App:TblCultures tc WITH tp.culture = tc.id
					WHERE tc.elevage =0 AND c.radie=0
					AND c.exploitation = '" . $explo_id . "'
					AND c.annee = '" . $annee . "'";
        $dql = $requete;
        $query->setDql($dql);

        return $query->getResult();
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
