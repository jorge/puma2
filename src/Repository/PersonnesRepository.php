<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Personnes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class PersonnesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Personnes::class);
    }

    public function getCheffamille($explot_id)
    {
        $q = $this->createQueryBuilder('p')
            ->where('p.exploitation = :explot_id')
            ->setParameter('explot_id', $explot_id)
            ->andWhere('p.rolFamille = :rf')
            ->setParameter('rf', Personnes::role_chefDeFamille);

        return $q->getQuery()->getResult();
    }

    public function getCoexploitants($exploitation_id)
    {
        $q = $this->createQueryBuilder('p')
            ->where('p.exploitation = :explot_id')
            ->setParameter('explot_id', $exploitation_id)
            ->andWhere('p.radie=0');
        $res = $q->getQuery()->getResult();

        return $q;
    }

    public function getConjoint($explot_id)
    {
        $q = $this->createQueryBuilder('p')
            ->where('p.exploitation = :explot_id')
            ->setParameter('explot_id', $explot_id)
            ->andWhere('p.rolFamille = :rf')
            ->setParameter('rf', 'Conjoint');

        return $q->getQuery()->getResult();
    }

    public function getPersonContact($contact, $org_id)
    {
        $parametros = ['cont' => $contact, 'org_id' => $org_id];
        $q = $this->createQueryBuilder('p')
            ->select('p.id')
            ->innerJoin('App\Entity\PivotPersonnesOrganisations', 'pi', 'WITH', 'p.id = pi.personne')
            ->where('p.contact = :cont')
            ->andWhere('pi.organisation = :org_id')
            ->setParameters($parametros);

        return $q->getQuery()->getResult();
    }

    public function getPourcentHomFem()
    {
        $q = $this->createQueryBuilder('p')
            ->select('DISTINCT(pto.denomination), h.hommes, f.femmes, j.jeunes, t.total,(t.total-h.hommes-f.femmes) AS pas_defini')
            ->innerJoin('App\Entity\Exploitations', 'e', 'WITH', 'p.exploitation = e.id')
            ->innerJoin('App\Entity\PivotExploitationOrganisations', 'pivot', 'WITH', 'pivot.exploitation = e.id')
            ->innerJoin('App\Entity\TblOrganisations', 'g', 'WITH', 'pivot.organisation = g.id')
            ->innerJoin('App\Entity\TblOrganisations', 'c', 'WITH', 'c.id = g.parent');
        // TODO: $from_personnes is undefined.
        $innnerJoin = ' FROM tbl_organisations pto INNER JOIN
				(SELECT c.denomination, count( p.id ) AS hommes
				' . $from_personnes . "
				WHERE g.parent_id = c.id
				AND pers.nom <> ''
				AND pers.sexe = 'Masculin'
				GROUP BY c.denomination)h
				ON pto.denomination=h.denomination
				INNER JOIN
				(SELECT c.denomination, count( p.id ) AS femmes
				" . $from_personnes . "
				WHERE g.parent_id = c.id
				AND pers.nom <> ''
				AND pers.sexe = 'Féminin'
				GROUP BY c.denomination)f
				ON pto.denomination=f.denomination
				INNER JOIN
				(SELECT c.denomination, count( p.id ) AS jeunes
				" . $from_personnes . "
				WHERE g.parent_id = c.id
				AND pers.nom <> ''
				AND ((Pers.date_naissance)>YEAR(DATE_SUB(Now(),INTERVAL 35 YEAR)))
				GROUP BY c.denomination)j
				ON pto.denomination=j.denomination
				INNER JOIN
				(SELECT c.denomination, count( p.id ) AS total
				" . $from_personnes . "
				WHERE g.parent_id = c.id
				AND pers.nom <> ''
				GROUP BY c.denomination)t
				ON pto.denomination=t.denomination
				INNER JOIN tbl_organisation_level tol ON pto.level_id=tol.id
				WHERE tol.level=1	";
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
