<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\ParcelleHistoriques;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class ParcelleHistoriquesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ParcelleHistoriques::class);
    }

    public function getOneById($parhis_id)
    {
        $q = $this->createQueryBuilder('p')
            ->where('p.id = :pahi_id')
            ->setParameter('pahi_id', $parhis_id);
        $q->getQuery()->getResult();

        return $q;
    }

    public function updateRadieStatus($parcelle_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('h');
        $q = $qb->update()
            ->set('h.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('h.parcelle = ?2')
            ->setParameter(2, $parcelle_id)
            ->getQuery();
        $p = $q->execute();
    }
}
