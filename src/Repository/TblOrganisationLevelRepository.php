<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblOrganisationLevel;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblOrganisationLevelRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblOrganisationLevel::class);
    }

    public function getLevelId($level)
    {
        $q = $this->createQueryBuilder('t')
            ->select('t.id')
            ->where('t.level = :level')
            ->setParameter('level', $level);

        return $q->getQuery()->getResult();
    }
}
