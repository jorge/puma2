<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Filieres;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class FilieresRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Filieres::class);
    }

    public function getOneOrSubset($options, $offset = 0, $nb_rec = 1000)
    {
        $entity_id = null;

        if (null !== $options['data']->getFiliere()) {
            $entity_id = $options['data']->getFiliere()->getId();
        }

        if (null !== $entity_id) {
            $q = $this->createQueryBuilder('e')
                ->where('e.id = :entity_id')
                ->setParameter('entity_id', $entity_id);
            $q->getQuery()->getResult();
        } else {
            $q = $this->createQueryBuilder('e')
                ->setFirstResult($offset)
                ->setMaxResults($nb_rec);
            $q->getQuery()->getResult();
        }

        return $q;
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
