<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Collecte;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class CollecteRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Collecte::class);
    }

    public function getExistent($exploId, $annee)
    {
        $q = $this->createQueryBuilder('c')
            ->select('c')
            ->where('c.exploitation = :e_id')
            ->setParameter('e_id', $exploId)
            ->andWhere('c.anneeEnquete= :an')
            ->setParameter('an', $annee);

        return $q->getQuery()->getResult();
    }
}
