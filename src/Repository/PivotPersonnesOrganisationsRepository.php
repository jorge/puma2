<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\PivotPersonnesOrganisations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class PivotPersonnesOrganisationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, PivotPersonnesOrganisations::class);
    }

    public function getAllPersonnesByOrg($org_id)
    {
        $q = $this->createQueryBuilder('e')
            ->where('e.organisation = :org_id')
            ->setParameter('org_id', $org_id)
            ->orderBy('e.personne', 'ASC');
        $q->getQuery()->getResult();

        return $q;
    }

    public function getIdFromPersonneAndOrganisationtion($pers_id, $org_id)
    {
        $parametros = ['pers_id' => $pers_id, 'org_id' => $org_id];
        $q = $this->createQueryBuilder('p')
            ->select('p.id')
            ->where('p.organisation = :org_id')
            ->andWhere('p.personne = :pers_id')
            ->setParameters($parametros);
        $q->getQuery()->getResult();

        return $q->getQuery()->getResult();
        //  return $q;
    }

    public function updateRadieStatus($organisation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('p');
        $q = $qb->update()
            ->set('p.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('p.organisation = ?2')
            ->setParameter(2, $organisation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
