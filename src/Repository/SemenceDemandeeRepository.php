<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\SemenceDemandee;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class SemenceDemandeeRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SemenceDemandee::class);
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
