<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblCultures;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblCulturesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblCultures::class);
    }

    public function getCulturesList()
    {
        $qb = $this->createQueryBuilder('t');

        $qb->where('t.elevage = :elev')
            ->setParameter('elev', 0);

        return $qb;
    }

    public function getElevageList()
    {
        $qb = $this->createQueryBuilder('t');

        $qb->where('t.elevage = :elev')
            ->setParameter('elev', 1);

        return $qb;
    }
}
