<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblAchatAgricoles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblAchatAgricolesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblAchatAgricoles::class);
    }

    public function prendreObjet($achat, $typeachat)
    {
        $q = $this->createQueryBuilder('ta')
            ->where('ta.achat = :machat')
            ->setParameter('machat', $achat)
            ->andWhere('ta.typeAchat = :tachat')
            ->setParameter('tachat', $typeachat);
        $q->getQuery()->getResult();

        return $q;
    }
}
