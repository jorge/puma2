<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AnimauxStructure;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class AnimauxStructureRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnimauxStructure::class);
    }

    public function getMouvements($troupeau)
    {
        $qb = $this->createQueryBuilder('t')
            ->Where('t.animaux = :troupeau')
            ->setParameter('troupeau', $troupeau)
            ->andWhere('t.radie = :status')
            ->setParameter('status', 0)
            ->orderBy('t.dateExecution', 'ASC')
            ->getQuery();

        return $qb->execute();
    }

    public function updateRadieStatus($animaux_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.animaux = ?2')
            ->setParameter(2, $animaux_id)
            ->getQuery();
        $p = $q->execute();
    }
}
