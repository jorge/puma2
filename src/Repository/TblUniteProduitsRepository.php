<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblUniteProduits;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblUniteProduitsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblUniteProduits::class);
    }

    public function prendreListeUnite()
    {
        $q = $this->createQueryBuilder('t')
            ->select('t.uniteProduit');
        $q->getQuery()->getResult();

        return $q;
    }

    public function prendreUniteObjet($m_unite)
    {
        $q = $this->createQueryBuilder('t')
            ->where('t.uniteProduit = :unidad')
            ->setParameter('unidad', $m_unite);
        $q->getQuery()->getResult();

        return $q;
    }
}
