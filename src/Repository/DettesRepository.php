<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Dettes;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class DettesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Dettes::class);
    }

    public function getConsommations($exploitation_id, $annee)
    {
        $q = $this->createQueryBuilder('d')
            ->innerJoin('App\Entity\TblCreditObjet', 't', 'WITH', 'd.creditobjet = t.id')
            ->where('d.exploitation = :explo_id')
            ->setParameter('explo_id', $exploitation_id)
            ->andWhere('d.annee = :an')
            ->setParameter('an', $annee)
            ->andWhere('t.creditobjet = :co')
            ->setParameter('co', 'consommation')
            ->andWhere('d.radie =0');

        return $q->getQuery()->getResult();
    }

    public function getIntrants($exploitation_id, $annee)
    {
        $q = $this->createQueryBuilder('d')
            ->where('d.exploitation = :explo_id')
            ->setParameter('explo_id', $exploitation_id)
            ->andWhere('d.annee = :an')
            ->setParameter('an', $annee)
            ->andWhere('d.creditIntrantsDemande IS NOT NULL')
            ->andWhere('d.radie=0');

        return $q->getQuery()->getResult();
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
