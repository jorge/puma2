<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\FormationsBesoin;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class FormationsBesoinRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, FormationsBesoin::class);
    }

    public function trouverPriorite($exploitation_id, $annee)
    {
        $q = $this->createQueryBuilder('f')
            ->select('Max(f.priorite)')
            ->where('f.exploitation = :exploi_id')
            ->setParameter('exploi_id', $exploitation_id)
            ->andWhere('f.annee = :an')
            ->setParameter('an', $annee);

        return $q->getQuery()->getResult();
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
