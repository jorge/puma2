<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\AnimauxProduction;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class AnimauxProductionRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, AnimauxProduction::class);
    }

    public function updateRadieStatus($animaux_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('a');
        $q = $qb->update()
            ->set('a.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('a.animaux = ?2')
            ->setParameter(2, $animaux_id)
            ->getQuery();
        $p = $q->execute();
    }
}
