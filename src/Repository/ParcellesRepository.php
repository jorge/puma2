<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\Parcelles;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

use function array_key_exists;

final class ParcellesRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Parcelles::class);
    }

    public function getOneOrSubset($options, $offset = 0, $nb_rec = 1000)
    {
        $entity_id = null;

        if (null !== $options['data']->getParcelle()) {
            $entity_id = $options['data']->getParcelle()->getId();
        } elseif (array_key_exists('parcelle', $options)) {
            $entity_id = $options['parcelle']->getId();
        }

        if (null !== $entity_id) {
            $q = $this->createQueryBuilder('e')->where('e.id = :entity_id')->setParameter('entity_id', $entity_id);
            $q->getQuery()->getResult();
        } else {
            $q = $this->createQueryBuilder('e')->setFirstResult($offset)->setMaxResults($nb_rec);
            $q->getQuery()->getResult();
        }

        return $q;
    }

    public function getParcelles4PlanSaison($planSaison)
    {
        return $this->getParcelles4PlanSaisonQB($planSaison)->getQuery()->getResult();
    }

    public function getParcelles4PlanSaisonQB($planSaison)
    {
        $annee = $planSaison->getCollecte()->getExercice()->getAnnee();
        $anneeM1 = $annee - 1;

        return $this->createQueryBuilder('p')
            ->where('p.exploitation = :exploitation_id')
            ->andWhere('p.annee IN (:annees)')
                    // ->andwhere('p.annee = :annee') pbm annee n'existe pas deja (prendre 0 et -1)
            ->andWhere('p.radie = :radie')
            ->orderBy('p.numeroParcelle', 'ASC')
            ->setParameter('exploitation_id', $planSaison->getExploitation()->getId())
            ->setParameter('annees', [$annee, $anneeM1])
                    // ->setParameter('annee', $planSaison->getCollecte()->getExercice()->getAnnee())
            ->setParameter('radie', 0);
    }

    public function updateRadieStatus($exploitation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('p');
        $q = $qb->update()
            ->set('p.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('p.exploitation = ?2')
            ->setParameter(2, $exploitation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
