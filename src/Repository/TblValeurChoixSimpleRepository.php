<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\TblValeurChoixSimple;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblValeurChoixSimpleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblValeurChoixSimple::class);
    }

    public function findAll()
    {
        return $this->findBy([], ['entityClass' => 'ASC', 'tag' => 'ASC', 'displayOrder' => 'ASC']);
    }
}

/*
use App\Entity\TblValeurChoixSimple;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Common\Persistence\ManagerRegistry;


/**
 * @method TblValeurChoixSimple|null find($id, $lockMode = null, $lockVersion = null)
 * @method TblValeurChoixSimple|null findOneBy(array $criteria, array $orderBy = null)
 * @method TblValeurChoixSimple[]    findAll()
 * @method TblValeurChoixSimple[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class TblValeurChoixSimpleRepository extends \Doctrine\ORM\EntityRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, TblValeurChoixSimple::class);
    }

    // /**
    //  * @return TblValeurChoixSimple[] Returns an array of TblValeurChoixSimple objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('t.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
     * /

    /*
    public function findOneBySomeField($value): ?TblValeurChoixSimple
    {
        return $this->createQueryBuilder('t')
            ->andWhere('t.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
     * /
}
     */
