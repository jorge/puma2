<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\SuiviOrganisations;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

final class SuiviOrganisationsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, SuiviOrganisations::class);
    }

    public function updateRadieStatus($organisation_id, $newStatus)
    {
        $qb = $this->createQueryBuilder('s');
        $q = $qb->update()
            ->set('s.radie', '?1')
            ->setParameter(1, $newStatus)
            ->where('s.organisation = ?2')
            ->setParameter(2, $organisation_id)
            ->getQuery();
        $p = $q->execute();
    }
}
