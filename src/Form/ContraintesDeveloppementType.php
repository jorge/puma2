<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\ContraintesDeveloppement;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ContraintesDeveloppementType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('priorite', ChoiceType::class, [
                'choices' => [
                    'Non encodé' => null,
                    'Pas choisi' => ContraintesDeveloppement::PRIORITE_NO,
                    'Priorité 1' => ContraintesDeveloppement::PRIORITE_1,
                    'Priorité 2' => ContraintesDeveloppement::PRIORITE_2,
                    'Priorité 3' => ContraintesDeveloppement::PRIORITE_3,
                    'Priorité 4' => ContraintesDeveloppement::PRIORITE_4,
                ],
                'placeholder' => false,
            ])
            ->add('contrainte');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ContraintesDeveloppement',
        ]);
    }
}
