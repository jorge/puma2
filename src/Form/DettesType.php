<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblInstitutionsFinancieres;
use App\Entity\TblTypeGarantie;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DettesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('institutionFinanciere', EntityType::class, [
                'label' => 'Source crédit',
                'class' => TblInstitutionsFinancieres::class,
                'placeholder' => 'Choisir la source du crédit',
            ])
            ->add('valeurCredit', NumberType::class, [
                'label' => 'Montant',
            ])
            ->add('periodeRemboursement', null, [
                'label' => 'Durée (mois)',
            ])
            ->add('periodeReception', DateType::class, [
                'label' => 'Periode de reception',
                'placeholder' => [
                    'year' => 'Année',
                    'month' => 'Mois',
                    'day' => 'Jour', ],
            ])
            ->add('garantie', EntityType::class, [
                'label' => 'Type de garantie',
                'class' => TblTypeGarantie::class,
                'placeholder' => 'Choisir le type de garantie',
            ])
            ->add('tauxCredit')
            ->add('interetsPayes');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Dettes',
        ]);
    }
}
