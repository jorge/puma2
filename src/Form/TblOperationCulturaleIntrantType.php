<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TblOperationCulturaleIntrantType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('operationCulturaleType', null, [
                'group_by' => static function ($choiceValue, $key, $value) {
                    if ($choiceValue->getCategory()) {
                        return $choiceValue->getCategory()->getNom();
                    }

                    return 'Autre';
                },
                'attr' => ['class' => 'select2'], ])
            ->add('unite');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\TblOperationCulturaleIntrant',
        ]);
    }
}
