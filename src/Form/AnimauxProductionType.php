<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnimauxProductionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $prod = $options['data']->getProduction()->getProduction();

        if ('lait' === $prod) {
            $liste = ['litre' => 'litre', 'Fbu' => 'Fbu'];
        } elseif ('oeufs' === $prod) {
            $liste = ['piece' => 'piece', 'Fbu' => 'Fbu'];
        } elseif ('fumier' === $prod) {
            $liste = ['Fbu' => 'Fbu'];
        }
        $builder
            ->add('production') // todo supprimer
            ->add('unite', ChoiceType::class, [
                'placeholder' => 'Choisir unité',
                'choices' => $liste,
            ])
            ->add('quantite');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\AnimauxProduction',
        ]);
    }
}
