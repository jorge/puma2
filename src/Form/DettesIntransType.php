<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblTypeCreancier;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class DettesIntransType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('creditIntrantsDemande', NumberType::class, [
                'label' => 'Montant contracté au cours de l\'année pour achat des intrants (FBu)',
            ])
            ->add('typeCreancier', EntityType::class, [
                'label' => 'Type de creancier',
                'class' => TblTypeCreancier::class,
                'placeholder' => 'Choisir le type de creancier',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Dettes',
        ]);
    }
}
