<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblPlantsAgroFoEtFruitiers;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TblPlantsAgroFoEtFruitiersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('plant', null, [
                'label' => 'Nom du plant (et semence)',
            ])
            ->add('agroForestier', ChoiceType::class, [
                'label' => 'Agro Forestier',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => TblPlantsAgroFoEtFruitiers::class,
        ]);
    }
}
