<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblDecoupageAdmin;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TblOrganisationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('decoupageAdmin', EntityType::class, [
                'class' => TblDecoupageAdmin::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->orderBy('d.parent', 'ASC');
                },
            ])
            ->add('denomination', null, ['label' => 'Dénomination'])
            ->add('anneeCreation', null, ['label' => 'Année de création'])
            ->add('adresseSiege', null, ['label' => 'Adresse du siège'])
            ->add('contact1Telephone')
            ->add('contact2Telephone');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\TblOrganisations',
            'objet' => 'cooperative',
        ]);
    }
}
