<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblCultures;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcelleHistoriquesTblAleasType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parcelleHistoriquesTblAleas = $options['data'];
        $annee = (int) ($parcelleHistoriquesTblAleas->getParcelleHistorique()->getAnnee());
        $builder
            ->add('date', DateType::class, [
                'label' => 'Date',
                'format' => 'dd-MM-yyyy',
                'years' => range($annee, $annee -4)
            ])
            ->add('culture', EntityType::class, [
                'class' => TblCultures::class,
                'choices' => $parcelleHistoriquesTblAleas->getParcelleHistorique()->getCultures(),
                'expanded' => true,
                'multiple' => false,
            ])
            ->add('aleas', null, [
                'label' => 'Aléas',
            ])
            ->add('commentaire');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ParcelleHistoriquesTblAleas',
        ]);
    }
}
