<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SecuriteAlimentaireType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('repasCulture', ChoiceType::class, [
                'label' => 'Nombre de repas pris par jour durant les saisons de culture:',
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3, ],
                'placeholder' => 'Choisir',
            ])
            ->add('repasPostRecolte', ChoiceType::class, [
                'label' => 'Nombre de repas pris par jour en période en post récolte:',
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3, ],
                'placeholder' => 'Choisir',
            ])
            ->add('enfantsMalnutrition');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\SecuriteAlimentaire',
        ]);
    }
}
