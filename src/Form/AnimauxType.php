<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblCultures;
use App\Entity\TblProduits;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnimauxType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $elevage = 1;
        $builder
            ->add('espece', EntityType::class, [
                'label' => 'Espèce',
                // query choices from this entity
                'class' => TblCultures::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getElevageList();
                },
                'placeholder' => 'Veuillez sélectionner une espèce',
                'empty_data' => null,
                'required' => true,
                'attr' => ['class' => 'select2'],
            ])
            ->add('produits', null, ['attr' => ['class' => 'select2']]);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            // this will build the form with all the controls even if there is
            // no data
            $data = $event->getData();

            if ($data) {
                $this->addProduits($event->getForm(), $data->getEspece());
            } else {
                $this->addProduits($event->getForm());
            }
            $this->addOtherFields($event->getForm());
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            // this will build the controls taking into consideration the
            // selected data
            $form = $event->getForm();
            $data = $event->getData();
            // here we've added the listener to form
            if (isset($data['espece'])) {
                $this->addProduits($form, $data['espece']);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Animaux',
        ]);
    }

    private function addOtherFields(FormInterface $form)
    {
        $form->add('typeElevage');
        $form->add('nbrTotalTetes', null, [
            'label' => 'Nombre total de têtes',
        ]);
        $form->add('femellesReproductrices');
        $form->add('malesReproducteurs', null, [
            'label' => 'Mâles reproducteurs',
        ]);
        $form->add('jeunesFemelles');
        $form->add('jeunesMales', null, [
            'label' => 'Jeunes mâles',
        ]);
        $form->add('jeunesImmatures');
        $form->add('ageReformeFemelles', null, [
            'label' => 'Age de réforme femelles reproductrices',
        ]);
        $form->add('alimentations', null, [
            'label' => 'Alimentation',
        ]);
    }

    private function addProduits(FormInterface $form, $espece = null)
    {
        $espece_id = null === $espece ? '' : $espece;
        $form->add('produits', EntityType::class, [
            'label' => 'Produits:',
            // query choices from this entity
            'class' => TblProduits::class,
            'query_builder' => static function (EntityRepository $er) use ($espece_id) {
                return $er->prendreProduits($espece_id);
            },
            'placeholder' => 'Choisir les produits',
            'empty_data' => null,
            'required' => true,
            'multiple' => true,
            // 'attr' => array('class' => 'select2'),
        ]);
    }
}
