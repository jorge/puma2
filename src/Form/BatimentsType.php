<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class BatimentsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('type', null, [
                'placeholder' => 'Veuillez sélectionner le type du bâtiment',
                'required' => true, ])
            ->add('surface', NumberType::class, [
                'required' => false,
                'label' => 'Surface (m2)', ])
            ->add('coutConstruction', null, [
                'label' => 'Coût de construction (FBu)', ])
            // to do ajouter traitement antif erosif
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Batiments',
        ]);
    }
}
