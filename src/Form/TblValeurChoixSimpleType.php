<?php

declare(strict_types=1);

namespace App\Form;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TblValeurChoixSimpleType extends AbstractType
{
    private $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $allClassNames = $this->entityManager->getConfiguration()->getMetadataDriverImpl()->getAllClassNames();
        $choices = array_combine($allClassNames, $allClassNames);

        $builder
            ->add('entityClass', ChoiceType::class, [
                'choices' => $choices,
                'attr' => ['class' => 'select2'],
            ])
            ->add('tag')
            ->add('displayOrder')
            ->add('id')
            ->add('valeur')
            ->add('score');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\TblValeurChoixSimple',
        ]);
    }
}
