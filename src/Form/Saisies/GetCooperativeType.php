<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use App\Entity\TblOrganisations;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GetCooperativeType extends AbstractType
{
    /**
     * Form to choose a coop then a groupement.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['em_manager'];
        $builder
            ->add('cooperative', EntityType::class, [
                'label' => 'Coopérative',
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'placeholder' => 'Choisir une coopérative',
                'empty_data' => null,
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setRequired('em_manager');
    }
}
