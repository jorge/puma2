<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use App\Entity\Saisies\ActivityUser;
use App\Entity\User;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ActivityUserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('users', EntityType::class, [
                'label' => 'Utilisateur:',
                'class' => User::class,
                'choice_label' => 'username',
                'placeholder' => 'Choix de l\'utilisateur',
                'empty_data' => null,
                'required' => false,
                'multiple' => true,
                'attr' => ['class' => 'select2'],
            ])
            ->add('dateFrom', DateType::class, [
                'label' => 'Date debut',
                'format' => 'd-M-y',
                'years' => range(date('Y'), date('Y') - 10),
                'placeholder' => [
                    'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                ],
                'required' => false,
            ])
            ->add('dateTo', DateType::class, [
                'label' => 'Date fin',
                'format' => 'd-M-y',
                'years' => range(date('Y'), date('Y') - 10),
                'placeholder' => [
                    'year' => 'Année', 'month' => 'Mois', 'day' => 'Jour',
                ],
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ActivityUser::class,
        ]);
    }
}
