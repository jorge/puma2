<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use App\Entity\TblDecoupageAdmin;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LocalisationType extends AbstractType
{
    /**
     * Form to choose a coop then a groupement.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'province',
                EntityType::class,
                [
                    'label' => 'Province:',
                    'class' => TblDecoupageAdmin::class,
                    'query_builder' => static function (EntityRepository $er) {
                        return $er->getAllProvinces();
                    },
                    'placeholder' => 'Choisir la province',
                    'empty_data' => null,
                    'required' => true,
                ]
            );

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            // this will build the form with all the controls even if there is
            // no data
            $data = $event->getData();

            if ($data) {
                $this->addCommune($event->getForm(), $data->getProvince());
                $this->addColline($event->getForm(), $data->getCommune());
            } else {
                $this->addCommune($event->getForm());
                $this->addColline($event->getForm());
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            // this will build the controls taking into consideration the
            // selected data
            $form = $event->getForm();
            $data = $event->getData();
            // here we've added the listener to form
            if (isset($data['province'])) {
                $this->addCommune($form, $data['province']);
            }

            if (isset($data['commune'])) {
                $this->addColline($form, $data['commune']);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\localisation',
        ]);
    }

    private function addColline(FormInterface $form, $commune = null)
    {
        $comm_id = null === $commune ? '' : $commune;
        $form->add('colline', EntityType::class, [
            'label' => 'Colline:',
            // query choices from this entity
            'class' => TblDecoupageAdmin::class,
            'query_builder' => static function (EntityRepository $er) use ($comm_id) {
                return $er->getAllCollines($comm_id);
            },
            'placeholder' => 'Choisir une colline',
            'empty_data' => null,
            'required' => true,
        ]);
    }

    private function addCommune(FormInterface $form, $province = null)
    {
        $prov_id = null === $province ? '' : $province;
        $form->add('commune', EntityType::class, [
            'label' => 'Commune:',
            // query choices from this entity
            'class' => TblDecoupageAdmin::class,
            'query_builder' => static function (EntityRepository $er) use ($prov_id) {
                return $er->getAllCommunes($prov_id);
            },
            'placeholder' => 'Choisir une commune',
            'empty_data' => null,
            'required' => true,
        ]);
    }
}
