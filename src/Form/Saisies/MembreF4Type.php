<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * membre F4 Type
 * FIXME: incomplet.
 */
class MembreF4Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['exploitation_id']->getId();
        $annee = $options['annee'];
        $creditobjet = $options['creditobjet'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\MembreF4',
        ]);
        $resolver->setRequired('exploitation');
        $resolver->setRequired('annee');
        $resolver->setRequired('creditobjet');
    }
}
