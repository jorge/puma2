<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CooperativesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['my_em_manager'];
        $builder->add('cooperative', EntityType::class, [
            'label' => 'Coopératives',
            // query choices from this entity
            'class' => TblOrganisations::class,
            'query_builder' => static function (EntityRepository $er) {
                return $er->getAllCooperatives();
            },
            'placeholder' => 'Choisir une coopérative',
            'empty_data' => null,
            'expanded' => true,
            'required' => true,
        ])
            ->add('saisie_cooperatives_new', SubmitType::class, ['attr' => ['class' => 'btn-primary'],
                'label' => 'Saisie de une nouvelle coopérative',
            ])
            ->add('saisie_listGroupements', SubmitType::class, [
                'label' => 'lister les groupements',
            ])
            ->add('saisie_listExploitations', SubmitType::class, [
                'label' => 'Lister les exploitations',
            ])
            ->add('saisie_suivicooperatives', SubmitType::class, [
                'label' => 'Suivi de la cooperative',
            ])
            ->add('reset', SubmitType::class, [
                'label' => 'Reset',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\Cooperatives',
        ]);
        $resolver->setRequired('my_em_manager');
    }
}
