<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollecteExerciceAnneeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('exercice', ChoiceType::class, [
                'label' => "Année de l'exercice",
                'choices' => array_combine(range(2010, date('Y') + 1), range(2010, date('Y') + 1)),
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\CollecteExercice',
        ]);
    }
}
