<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use App\Entity\Saisies\Socies;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use App\Entity\TblEtatCivil;

class SociesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $debut = (int) (date('Y')) - 15;

        for ($i = 0; 80 > $i; ++$i) {
            $arrAnnees[(string) ($debut - $i)] = (string) ($debut - $i);
        }
        $builder
            ->add('nom', null, [
                'label' => 'Nom',
                'required' => true, ])
            ->add('prenom', null, [
                'label' => 'Prénom',
                'required' => true, ])
            ->add('dateNaissance', DateType::class, [
                'label' => 'Date de naissance',
                'format' => 'd-M-y',
                'years' => range(date('Y') - 14, date('Y') - 120),
            ])
            ->add('sexe', ChoiceType::class, [
                'choices' => [
                    'Masculin' => 'Masculin',
                    'Féminin' => 'Féminin', ],
                'placeholder' => 'Choisir le sexe',
                'required' => false, ])
            ->add('etatCivil', EntityType::class, [
                'label' => 'Etat civil',
                'class' => TblEtatCivil::class,
                'placeholder' => 'Choisir',
            ])
            ->add('cni', null, ['label' => 'CNI'])
            ->add('repondant', ChoiceType::class, [
                'choices' => [
                    'Vrai' => true,
                    'Faux' => false, ],
                'placeholder' => 'Choisir',
                'required' => true,
                'label' => 'Répondant?', ])
            ->add(
                'membreMenage',
                ChoiceType::class,
                [
                    'choices' => [
                        'Vrai' => true,
                        'Faux' => false, ],
                    'placeholder' => 'Choisir',
                    'required' => true,
                    'label' => 'Membre du menage?',
                ]
            )
            ->add('rolFamille', ChoiceType::class, [
                'choices' => [
                    Socies::role_chefDeFamille => Socies::role_chefDeFamille,
                    Socies::role_conjoint => Socies::role_conjoint,
                    Socies::role_pas => Socies::role_pas, ],
                'required' => false,
                'label' => 'Rôle dans la famille', ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\Socies',
        ]);
    }
}
