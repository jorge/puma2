<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

//use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
/**
 * membre F1 Type
 * FIXME: incomplet.
 */
class MembreF1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['exploitation']->getId();
        $annee = $options['annee'];
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\MembreF1',
        ]);
        $resolver->setRequired('exploitation');
        $resolver->setRequired('annee');
    }
}
