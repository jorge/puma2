<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollecteExerciceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('exercice', ChoiceType::class, [
                'label' => "Année de l'exercice",
                'choices' => array_combine(range(2010, date('Y') + 1), range(2010, date('Y') + 1)),
            ])
            ->add('type', ChoiceType::class, [
                'label' => 'Fiche récoltée',
                'choices' => [
                    'F0' => 0,
                    'F1' => 1,
                    'F2' => 2,
                    'F3' => 3,
                    'F4' => 4,
                    'F5' => 5,
                    'F6' => 6, ],
            ])
            ->add('date', DateType::class, [
                'label' => "Date du passage de l'enquêteur",
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\CollecteExercice',
        ]);
    }
}
