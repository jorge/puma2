<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use App\Entity\TblOrganisations;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GroupAdhesionTelType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['em_manager'];
        $builder
            ->add('cooperative', EntityType::class, [
                'label' => 'Coopérative',
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'placeholder' => 'Choisir une coopérative',
                'empty_data' => null,
                'required' => true,
                'attr' => ['class' => 'select2'],
            ])
            ->add('groupement', HiddenType::class);

        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            // this will build the form with all the controls even if there is
            // no data
            $data = $event->getData();

            if ($data) {
                $this->addGroupement($event->getForm(), $data->getCooperative());
            } else {
                $this->addGroupement($event->getForm());
            }
            $this->addOtherFields($event->getForm());
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            // this will build the controls taking into consideration the
            // selected data
            $form = $event->getForm();
            $data = $event->getData();

            if (isset($data['cooperative'])) {
                $this->addGroupement($form, $data['cooperative']);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\GroupAdhesionTel',
        ]);
        $resolver->setRequired('em_manager');
    }

    private function addGroupement(FormInterface $form, $cooperative = null)
    {
        $coop_id = null === $cooperative ? '' : $cooperative;
        $form->add('groupement', EntityType::class, [
            'label' => 'Groupements',
            // query choices from this entity
            'class' => TblOrganisations::class,
            'query_builder' => static function (EntityRepository $er) use ($cooperative) {
                return $er->getAllGroupementsQB($cooperative);
            },
            'placeholder' => 'Choisir un groupement',
            'empty_data' => null,
            'required' => true,
            'attr' => ['class' => 'select2'],
        ]);
    }

    private function addOtherFields(FormInterface $form)
    {
        $debut = (int) (date('Y'));
        $listAnnees['non-renseigné'] = 9999;

        for ($i = 0; 20 > $i; ++$i) {
            $listAnnees[(string) ($debut - $i)] = (string) ($debut - $i);
        }
        $form->add('anneeAdhesion', ChoiceType::class, [
            'choices' => $listAnnees,
            'placeholder' => "Choisir l'année d'adhesion",
            'required' => true,
            'label' => "Année d'adhésion", ])
            ->add('carteMembre', ChoiceType::class, [
                'label' => 'Carte de membre',
                'choices' => [
                    'Oui' => 1,
                    'Non' => 0,
                ],
            ])
            ->add('telephone', TextType::class, [
                'label' => 'téléphone du répondant',
                'required' => false,
            ]);
    }
}
