<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class GetOrganisationType extends AbstractType
{
    /**
     * Form to choose a coop then a groupement.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $em = $options['em_manager'];

        $builder
            ->add('cooperative', EntityType::class, [
                'label' => 'Coopérative',
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'placeholder' => 'Choisir une coopérative',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('groupement', HiddenType::class);
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            // this will build the form with all the controls even if there is
            // no data
            $data = $event->getData();

            if ($data) {
                $this->addGroupement($event->getForm(), $data->getGroupement());
            } else {
                $this->addGroupement($event->getForm());
            }
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            // this will build the controls taking into consideration the
            // selected data
            $form = $event->getForm();
            $data = $event->getData();
            // here we've added the listener to form
            if (isset($data['cooperative'])) {
                $this->addGroupement($form, $data['cooperative']);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\Organisations',
        ]);
        $resolver->setRequired('em_manager');
    }

    private function addGroupement(FormInterface $form, $cooperative = null)
    {
        $coop_id = null === $cooperative ? '' : $cooperative;

        $form->add('groupement', EntityType::class, [
            'label' => 'Groupements',
            // query choices from this entity
            'class' => TblOrganisations::class,
            'query_builder' => static function (EntityRepository $er) use ($coop_id) {
                return $er->getAllGroupementsQB($coop_id);
            },
            'placeholder' => 'Choisir un groupement',
            'empty_data' => null,
            'required' => false,
        ]);
    }
}
