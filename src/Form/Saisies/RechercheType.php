<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use App\Entity\Saisies\Recherche;
use App\Entity\TblDecoupageAdmin;
use App\Entity\TblOrganisations;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RechercheType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroMembre', TextType::class, [
                'label' => 'Numéro membre:',
                'data' => '___-___-___',
                'required' => false,
                'attr' => [
                    'data-mask' => '___-___-___',
                    'class' => 'form-control', ],
            ])
            ->add('province', EntityType::class, [
                'label' => 'Province:',
                'class' => TblDecoupageAdmin::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllProvinces();
                },
                'placeholder' => 'Choisir la province',
                'empty_data' => null,
                'required' => false,
            ]);
        $builder->addEventListener(FormEvents::PRE_SET_DATA, function (FormEvent $event) {
            // this will build the form with all the controls even if there is
            // no data
            $data = $event->getData();

            if ($data) {
                $this->addCommune($event->getForm(), $data->getProvince());
                $this->addColline($event->getForm(), $data->getCommune());
                //    $this->addGroupement($event->getForm(), $data->getCooperative());
                $this->addOtherFields($event->getForm(), $data->getCooperative());
            } else {
                $this->addCommune($event->getForm());
                $this->addColline($event->getForm());
                //            $this->addGroupement($event->getForm());
            }
            $this->addOtherFields($event->getForm(), $data->getCooperative());
        });

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
            // this will build the controls taking into consideration the
            // selected data
            $form = $event->getForm();
            $data = $event->getData();
            // here we've added the listener to form
            if (isset($data['province'])) {
                $this->addCommune($form, $data['province']);
            }

            if (isset($data['commune'])) {
                $this->addColline($form, $data['commune']);
            }

            if (isset($data['cooperative'])) {
                $this->addGroupement($form, $data['cooperative']);
            }
        });
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Recherche::class,
        ]);
    }

    private function addColline(FormInterface $form, $commune = null)
    {
        $comm_id = null === $commune ? '' : $commune;
        $form->add('colline', EntityType::class, [
            'label' => 'Colline:',
            // query choices from this entity
            'class' => TblDecoupageAdmin::class,
            'query_builder' => static function (EntityRepository $er) use ($comm_id) {
                return $er->getAllCollines($comm_id);
            },
            'placeholder' => 'Choisir une colline',
            'empty_data' => null,
            'required' => false,
        ]);
    }

    private function addCommune(FormInterface $form, $province = null)
    {
        $prov_id = null === $province ? '' : $province;
        $form->add('commune', EntityType::class, [
            'label' => 'Commune:',
            // query choices from this entity
            'class' => TblDecoupageAdmin::class,
            'query_builder' => static function (EntityRepository $er) use ($prov_id) {
                return $er->getAllCommunes($prov_id);
            },
            'placeholder' => 'Choisir une commune',
            'empty_data' => null,
            'required' => false,
        ]);
    }

    private function addGroupement(FormInterface $form, $cooperative = null)
    {
        $coop_id = null === $cooperative ? '' : $cooperative;
        $form->add('groupement', EntityType::class, [
            'label' => 'Groupement:',
            // query choices from this entity
            'class' => TblOrganisations::class,
            'query_builder' => static function (EntityRepository $er) use ($coop_id) {
                return $er->getAllGroupementsQB($coop_id);
            },
            'placeholder' => 'Choisir un groupement',
            'empty_data' => null,
            'required' => false,
        ]);
    }

    private function addOtherFields(FormInterface $form, $cooperative)
    {
        $coop_id = null === $cooperative ? '' : $cooperative;
        $form->add('nom', null, ['label' => 'Nom co-exploitant']);
        $form->add('prenom', null, ['label' => 'Prénom co-exploitant']);
        $form->add('anneeNaissance', ChoiceType::class, [
            'label' => 'Année de naissance co-exploitant',
            'required' => false,
            'choices' => array_combine(range(date('Y') - 15, date('Y') - 110), range(date('Y') - 15, date('Y') - 110)),
            'placeholder' => 'Choisir une année',
            'empty_data' => null,
        ]);
        $form->add('cni', null, ['label' => 'CNI']);
        $form->add('cooperative', EntityType::class, [
            'label' => 'Coopérative',
            // query choices from this entity
            'class' => TblOrganisations::class,
            'query_builder' => static function (EntityRepository $er) {
                return $er->getAllCooperatives();
            },
            // 'choice_label' => 'denomination',
            'placeholder' => 'Choisir une coopérative',
            'empty_data' => null,
            'required' => false,
            'attr' => ['class' => 'select2'], ]);
        $form->add('groupement', EntityType::class, [
            'label' => 'Groupement:',
            // query choices from this entity
            'class' => TblOrganisations::class,
            'query_builder' => static function (EntityRepository $er) use ($coop_id) {
                return $er->getAllGroupementsQB($coop_id);
            },
            'placeholder' => 'Choisir un groupement',
            'empty_data' => null,
            'required' => false,
        ]);
        $form->add('anneeAdhesion', ChoiceType::class, [
            'label' => 'Annee d\'adhesion',
            'required' => false,
            'choices' => array_combine(range(date('Y') - 25, date('Y')), range(date('Y') - 25, date('Y'))),
            'placeholder' => 'Choisir une année',
            'empty_data' => null,
            'required' => false,
        ]);
        $form->add('submit', SubmitType::class, [
            'label' => 'Rechercher', ]);
    }
}
