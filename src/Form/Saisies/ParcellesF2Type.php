<?php

declare(strict_types=1);

namespace App\Form\Saisies;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
// use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Saisie Parcelles F2 Type
 * FIXME: incomplet.
 */
class ParcellesF2Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parcelle_id = $options['parcelle']->getId();
        $annee = $options['annee'];

        $builder->add('parcelle', EntityType::class, [
            'label' => 'Parcelle',
            // query choices from this entity
            'class' => Parcelles::class,
            'query_builder' => static function (EntityRepository $er) use ($options) {
                return $er->getOneOrSubset($options, 0, 1000);
            },
            'placeholder' => 'Choisir une parcelle (liste incomplète)',
            'empty_data' => null,
            'required' => true,
        ])
            ->add('numeroParcelle')
            ->add('nomParcelle')
            ->add('surface')
            ->add('coordoneesGeo')
            ->add('situation')
            ->add('exposition')
            ->add('pente')
            ->add('propriete')
            ->add('coutLocation')
            ->add('submit', SubmitType::class, [
                'label' => 'Submit',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Saisies\ParcellesF2',
        ]);
        $resolver->setRequired('parcelle');
        $resolver->setRequired('annee');
    }
}
