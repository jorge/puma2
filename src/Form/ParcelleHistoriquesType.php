<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblSaisons;
use App\Entity\TblModeCulture;
use App\Entity\TblCultures;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcelleHistoriquesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add(
                'saison',
                EntityType::class,
                [
                    'label' => 'Saison',
                    'class' => TblSaisons::class,
                    'placeholder' => 'Choisir une saison',
                    'required' => true,
                ]
            )
            ->add('modeCulture', EntityType::class, [
                'class' => TblModeCulture::class,
                'required' => true,
                'placeholder' => 'Choisissez un mode culture',
                'label' => 'Mode culture',
            ])
            ->add('cultures', EntityType::class, [
                'label' => 'Cultures:',
                // query choices from this entity
                'class' => TblCultures::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getCulturesList();
                },
                'attr' => ['class' => 'grid_4cols'],
                'multiple' => true,
                'expanded' => true,
                'placeholder' => 'Choisir les cultures',
                'empty_data' => null,
                'required' => true,
                'multiple' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ParcelleHistoriques',
        ]);
    }
}
