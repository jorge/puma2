<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class HabitatType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mur', ChoiceType::class, [
                'choices' => [
                    'Mur pisé' => 1,
                    'Adobe' => 2,
                    'Brique cuite' => 3, ],
                'placeholder' => 'Choisir le type de mur',
                'required' => false, ])
            ->add('toiture', ChoiceType::class, [
                'choices' => [
                    'Paille' => 1,
                    'Tuile' => 2,
                    'Tôle' => 3, ],
                'placeholder' => 'Choisir le type de toiture',
                'required' => false, ])
            ->add('nombrePieces', TextType::class, [
                'required' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Habitat',
        ]);
    }
}
