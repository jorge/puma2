<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblDecoupageAdmin;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TblDecoupageAdminType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('denomination')
            ->add('parent', EntityType::class, [
                'class' => TblDecoupageAdmin::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->createQueryBuilder('d')
                        ->orderBy('d.parent', 'ASC');
                },
            ])
    //        ->add('parent')
            ->add('level');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\TblDecoupageAdmin',
        ]);
    }
}
