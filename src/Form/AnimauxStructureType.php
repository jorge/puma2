<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnimauxStructureType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $structure = $options['data'];
        $annee = $structure->getAnnee();
        $builder
            ->add('dateExecution', DateType::class, [
                'label' => 'Date execution',
                'format' => 'dd-MM-yyyy',
                'years' => range($annee, $annee),
            ])
            ->add('animauxTechnique')
            ->add('malesReproducteurs')
            ->add('femellesReproductrices')
            ->add('jeunesMales')
            ->add('jeunesFemelles')
            ->add('jeunesImmatures')
            ->add('total');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\AnimauxStructure',
        ]);
    }
}
