<?php

declare(strict_types=1);

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsultationF1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Denomination', EntityType::class, [
                'label' => 'Coopératives',
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false,
            ])
            ->add('groupecoop', CheckboxType::class, [
                'label' => 'Groupé par coopérative?',
                'required' => false,
            ])
            ->add('groupegroup', CheckboxType::class, [
                'label' => 'Groupé par groupement?',
                'required' => false,
            ])
            ->add('exploitationGroupAnnee', CheckboxType::class, [
                'label' => 'Groupé par année?',
                'required' => false,
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Année',
                'choices' => array_combine(range(date('Y') - 15, date('Y')), range(date('Y') - 15, date('Y'))),
                'required' => false, //pour éviter la validation
                'expanded' => false,
            ])
            ->add('animaux', ChoiceType::class, [
                'label' => 'Les Animaux',
                'choices' => [
                    'Répartition des membres groupé par elevage' => 'membres_elevage',
                    'Répartition des membres groupé par produit' => 'membres_produit',
                    'Animaux groupé par type d\'elevage' => 'animaux_elevage',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-animaux'],
            ])
            ->add('terres', ChoiceType::class, [
                'label' => 'Les parcelles',
                'choices' => [
                    'Rapport des exploitations avec la surface totale et moyenne' => 'surface',
                    'Rapport des exploitations groupés par type de amendement' => 'amendement',
                    'Rapport des exploitations avec la surface pour l\'année' => 'surfaceExploitation',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-terres'],
            ])
            ->add('queryTheme', HiddenType::class, [
                'label' => 'Theme de regroupement',
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-theme'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ConsultationF1',
        ]);
    }
}
