<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblOrganisations;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsultationF0Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('denomination', EntityType::class, [
                'label' => 'Coopératives',
                // query choices from this entity
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
            ])
            ->add('groupegroup', ChoiceType::class, [
                'label' => 'Groupé par ?',
                'choices' => [
                    'par coopérative' => 'cooperative',
                    'par groupement' => 'groupement',
                ],
                'expanded' => true,
                'required' => false,
            ])
                /* Lié à exploitation */
            ->add('exploitationGroupAnnee', CheckboxType::class, [
                'label' => 'Groupé par année?',
                'required' => false,
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Pour l\'année',
                'choices' => array_combine(range(date('Y') - 15, date('Y')), range(date('Y') - 15, date('Y'))),
                'expanded' => false,
            ])
            ->add('queryTheme', ChoiceType::class, [
                'label' => 'Theme de regroupement',
                'choices' => [
                    'Choisir une option' => 'Choisir une option',
                    'etudes' => 'etudes',
                    'Les professions' => 'professions',
                    'Cotisations' => 'cotisations',
                    'Les filières' => 'filieres',
                    'La commercialisation des produits' => 'commercialisation',
                    'Formations reçus' => 'forms_recus',
                    'Répartition de membres producteurs selon les thèmes de formation qu’eux et leurs conjoints ont besoin' => 'forms_besoins',
                    'Répartition de membres producteurs selon les contraintes majeures au développement des activités agricoles citées' => 'contraintes',
                    'Répartition de membres producteurs selon les services dont ils ont besoin' => 'services',
                    'Répartition de membres producteurs selon leurs types d’habitat par Coopérative' => 'habitat',
                ],
                'expanded' => false,
                'required' => true,
                'attr' => ['class' => 'show-theme'],
            ])
                /* 1 */
            ->add('groupeCible', ChoiceType::class, [
                'label' => 'Groupe Cible',
                'choices' => [
                    'Membres' => 'membres',
                    'Conjoints' => 'conjoints',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-groupeCible'],
            ])
                /* 2 */
            ->add('choixGroup', ChoiceType::class, [
                'label' => 'Groupements du resultat',
                'choices' => [
                    'Grouper qui lire et ecrire en kirundi' => 'kirundi',
                    'Grouper qui lire et ecrire en français' => 'francais',
                    'Grouper par niveau d\'études' => ['primaire' => 'primaire',
                        'secondaire' => 'secondaire',
                        'superieur' => 'superieur',
                    ],
                    'Grouper par diplôme' => ['Sans_Diplome' => 'Sans_Diplome',
                        'A2' => 'A2',
                        'A3' => 'A3',
                        'D4' => 'D4',
                        'D6' => 'D6',
                        'D7' => 'D7',
                        'superieur' => 'superieur',
                    ],
                    'Grouper par domaine' => 'domaine',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-choixGroup'],
            ])
                /* 3 */
            ->add('groupProf', ChoiceType::class, [
                'label' => 'Les professions',
                'choices' => [
                    'Grouper par profession principale' => 'principale',
                    'Grouper par profession secondaire' => 'secondaire',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-groupProf'],
            ])
                /* 4 */
            ->add('Terres', ChoiceType::class, [
                'label' => 'Les terres',
                'choices' => [
                    'Surface total et moyenne par exploitation' => 'surface',
                    'Grouper par type de fumure/amendement appliqué à la terre' => 'amendement',
                    'Surface exploitation' => 'surfaceExploitation',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-Terres'],
            ])
                /* 5 */
            ->add('Cotisations', ChoiceType::class, [
                'label' => 'Cotisations',
                'choices' => [
                    'Membres qui payent regulierement' => 'regulierement',
                    'Total cotisé' => 'total',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-Cotisations'],
            ])
                /* 6 */
            ->add('Filieres', ChoiceType::class, [
                'label' => 'Les Filières',
                'choices' => [
                    'Surface total par filiere' => 'surface_total',
                    'Production total par filière' => 'Production_total',
                    'Surface et production total par filière' => 'surface_production',
                    'Spécial: Rapport production de tomates sur 4 coopératives' => 'rapport_especial',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-Filieres'],
            ])
                /* 7 */
            ->add('Animaux', ChoiceType::class, [
                'label' => 'Les Animaux',
                'choices' => [
                    'Répartition des membres groupé par elevage' => 'membres_elevage',
                    'Répartition des membres groupé par produit' => 'membres_produit',
                    'Animaux groupé par type d\'elevage' => 'animaux_elevage',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-Animaux'],
            ])
                /* 8 */
            ->add('Commercialisation', ChoiceType::class, [
                'label' => 'La commercialisation des produits',
                'choices' => [
                    'Répartition des membres groupé par produit warranté' => 'warrante',
                    'Répartition des membres groupé par produit en vente groupé' => 'groupe',
                    'Répartition des membres groupé par produit transformé' => 'transforme',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-Commercialisation'],
            ])
                /* 9 */
            ->add('Formations_recus', ChoiceType::class, [
                'label' => 'Formations reçus',
                'choices' => [
                    'Nombre et pourcentage de membres ayant reçu des formations tous opérateurs confondu' => 'tous_operateurs',
                    'Nombre et pourcentage de membres ayant reçu des formations via la CAPAD' => 'capad',
                    'Nombre et pourcentage, par coopérative, des occurrences par thème de formation' => 'theme',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false,
                'attr' => ['class' => 'show-Formations_recus'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ConsultationF0',
        ]);
    }
}
