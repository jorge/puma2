<?php

declare(strict_types=1);

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoToutesApplisType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parcelleHistorique_id = $options['data']->getParcelleHistorique()->getId();
        $builder
            ->add('parcelleHistorique', EntityType::class, [
                'label' => 'Parcelle Historique',
                // query choices from this entity
                'class' => ParcelleHistoriques::class,
                'query_builder' => static function (EntityRepository $er) use ($parcelleHistorique_id) {
                    return $er->getOneById($parcelleHistorique_id);
                },
                'placeholder' => 'Choisir une exploitation',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('unite', ChoiceType::class, [
                'choices' => [
                    'Heures' => 'Heures',
                    'Jours' => 'Jours', ],
                'placeholder' => 'Choisir unité de temps',
                'required' => false, ])
            ->add('quantite')
            ->add('traitement')
            ->add('moType');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\MoToutesApplis',
        ]);
    }
}
