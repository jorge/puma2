<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblEquipementsAgricoles;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class OutillageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('type', EntityType::class, [
                'label' => 'Type d outillage',
                'class' => TblEquipementsAgricoles::class,
                'placeholder' => 'Veuillez sélectionner le type de outillage',
                'required' => true,
            ])
            ->add('quantite', null, [
                'label' => 'Nombre',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Outillage',
            'annee' => '2000',
            'exploitation' => null,
        ]);
    }
}
