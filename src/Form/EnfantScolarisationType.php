<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class EnfantScolarisationType extends AbstractType
{
    /**
     * Form to choose les infos du menage.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        for ($i = 0; 40 > $i; ++$i) {
            $listNombre[$i] = $i;
        }
        $builder
            ->add('filsScolarises', ChoiceType::class, [
                'label' => 'Nombre de fils scolarisés',
                'choices' => $listNombre,
                'placeholder' => 'Choisir un nombre',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('fillesScolarisees', ChoiceType::class, [
                'label' => 'Nombre de filles scolarisées',
                'choices' => $listNombre,
                'placeholder' => 'Choisir un nombre',
                'empty_data' => null,
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\EnfantScolarisation',
        ]);
    }
}
