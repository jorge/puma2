<?php

declare(strict_types=1);

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RapportsPlanificationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $nb_annees = $options['nb_annees_passees'];
        $builder
            ->add('Denomination', EntityType::class, [
                'label' => 'Coopératives',
                // query choices from this entity
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllFilsOrganisation('fedead58-79ad-11e6-abc5-b8ca3a965972');
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
                // used to render a select box, check boxes or radios
                //'multiple' => true,
                //'expanded' => true,
            ])
            ->add('groupecoop', CheckboxType::class, [
                'label' => 'Groupé par coopérative?',
                'required' => false,
            ])
            ->add('groupegroup', CheckboxType::class, [
                'label' => 'Groupé par groupement?',
                'required' => false,
            ])
            ->add('exploitationGroupAnnee', CheckboxType::class, [
                'label' => 'Groupé par année d\'enquête?',
                'required' => false,
                'attr' => ['class' => 'show-exploit'],
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Année d\'enquête',
                'choices' => array_combine(range(date('Y') - $nb_annees, date('Y')), range(date('Y') - $nb_annees, date('Y'))),
                'expanded' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('queryTheme', ChoiceType::class, [
                'label' => 'Theme de regroupement',
                'choices' => [
                    'Choisir un repport' => '',
                    'Nombre et pourcentage des surfaces selon le mode de faire valoir, par coopérative [213]' => '',
                    'Surface emblavée par culture, par coopérative [351]' => '',
                    'Rendement moyens des cultures et des produits animaux, par coopérative [351]' => '',
                    'Nombre et pourcentage des produits commercialisés par production [351] (ne pas confondre production récoltée et produit commercialiser, ils peuvent correspondre ou non. Si non, il faut un coefficient de conversion)' => '',
                    'Nombre et pourcentage des besoins en formation par occurrence des thèmes [9638]' => '',
                    'Nombre et pourcentage des occurrences de contraintes déclarées [90]' => '',
                    'Nombre et pourcentage des occurrences des attentes en service/appui déclarées [9639]' => '',
                ],
                'expanded' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\RapportsPlanification',
        ]);
        /* déclarer ici les options additionnelles passées au formulaire depuis le controleur */
        $resolver->setRequired('nb_annees_passees'); // Requires that nb_annees_passees be set by the caller.
        $resolver->setAllowedTypes('nb_annees_passees', 'integer'); // Validates the type(s) of option(s) passed.
    }
}
