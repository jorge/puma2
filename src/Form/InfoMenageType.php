<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class InfoMenageType extends AbstractType
{
    /**
     * Form to choose les infos du menage.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        for ($i = 0; 40 > $i; ++$i) {
            $listNombre[$i] = $i;
        }
        $builder
            ->add('nombreFils', ChoiceType::class, [
                'label' => 'Nombre total de fils à charge',
                'choices' => $listNombre,
                'placeholder' => 'Choisir un nombre',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('nombreFilles', ChoiceType::class, [
                'label' => 'Nombre total de filles à charge',
                'choices' => $listNombre,
                'placeholder' => 'Choisir un nombre',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('nombreAutres', IntegerType::class, [
                'label' => 'Nombre d\'autres personnes à charge',
                'empty_data' => 0,
                'invalid_message' => 'Le champ \'Nombre autres personnes à charge\' doit être un entier.',
                'required' => false,
                'error_bubbling' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\infoMenage',
        ]);
    }
}
