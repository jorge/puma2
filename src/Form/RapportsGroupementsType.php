<?php

declare(strict_types=1);

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RapportsGroupementsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $nb_annees = $options['nb_annees_passees'];
        $builder
            ->add('Denomination', EntityType::class, [
                'label' => 'Coopératives',
                // query choices from this entity
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllFilsOrganisation('fedead58-79ad-11e6-abc5-b8ca3a965972');
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
                // used to render a select box, check boxes or radios
                //'multiple' => true,
                //'expanded' => true,
            ])
            ->add('groupecoop', CheckboxType::class, [
                'label' => 'Groupé par coopérative?',
                'required' => false,
            ])
            ->add('groupegroup', CheckboxType::class, [
                'label' => 'Groupé par groupement?',
                'required' => false,
            ])
            ->add('exploitationGroupAnnee', CheckboxType::class, [
                'label' => 'Groupé par année d\'enquête?',
                'required' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Année d\'enquête',
                'choices' => array_combine(range(date('Y') - $nb_annees, date('Y')), range(date('Y') - $nb_annees, date('Y'))),
                'expanded' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('queryTheme', ChoiceType::class, [
                'label' => 'Theme de regroupement',
                'choices' => [
                    'Choisir un rapport' => '',
                    'Répartition spatiale, nombre de groupements par commune et coopérative [02, 03]' => '',
                    'Existence du ROI [I1], Année de renouvellement [I4]' => '',
                    'Existence de PV de réunion [II3], Rapport annuel d’activités [II5]' => '',
                    'L’enregistrement des cotisations [III4], le classement des justificatifs [III8], l’existence de rapport financier annuel [III9]' => '',
                    'Revenu total : [IV6]+[IV8]+[IV9] ; pourcentages de [IV6]/ total, [IV8]/total, [IV9]/total' => '',
                    'Fréquence de [IV1], [IV2], [IV3], [IV4], [IV7]' => '',
                    'Pourcentage des membres ayant contracté un crédit [V1] ; montant total des crédits contractés [V1]' => '',
                    'Part des prêts octroyés par la coopérative [V2] sur le montant total [V1]' => '',
                    'Fréquence des institutions ayant octroyé le crédit, pour l’ensemble des groupements d’une coopérative [V2]' => '',
                    'Montant total des crédits contractés (octroyés) par la coopérative à ses groupements [V2]' => '',
                ],
                'expanded' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\RapportsGroupements',
        ]);
        /* déclarer ici les options additionnelles passées au formulaire depuis le controleur */
        $resolver->setRequired('nb_annees_passees'); // Requires that nb_annees_passees be set by the caller.
        $resolver->setAllowedTypes('nb_annees_passees', 'integer'); // Validates the type(s) of option(s) passed.
    }
}
