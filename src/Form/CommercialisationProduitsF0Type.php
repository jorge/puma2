<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommercialisationProduitsF0Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('produit')
            ->add('quantite', null, ['label' => 'Quantité vendue coopérative (kg)'])
            ->add('quantiteCapad', null, ['label' => 'Quantité vendue autres marchés (kg)']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\CommercialisationProduits',
        ]);
    }
}
