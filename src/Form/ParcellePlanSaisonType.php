<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Exploitations;
use App\Entity\Parcelles;
use App\Entity\TblModeCulture;
use App\Entity\TblCultures;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcellePlanSaisonType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $planSaison = $options['data']->getExploitationPlanSaison();
        $builder
            ->add('parcelle', EntityType::class, [
                'class' => Parcelles::class,
                'label' => 'Parcelle',
                'query_builder' => static function (EntityRepository $er) use ($planSaison) {
                    return $er->getParcelles4PlanSaisonQB($planSaison);
                },
                'multiple' => false,
                'expanded' => false,
                'placeholder' => 'Choisir la parcelle',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('surfacePrevue', null, [
                'label' => 'Surface prévue (ares)',
            ])
            ->add('modeCulture', EntityType::class, [
                'class' => TblModeCulture::class,
                'required' => true,
                'placeholder' => 'Choisissez un mode culture',
                'label' => 'Mode culture',
            ])
            ->add('cultures', EntityType::class, [
                'label' => 'Cultures:',
                // query choices from this entity
                'class' => TblCultures::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getCulturesList();
                },
                'attr' => ['class' => 'grid_4cols'],
                'multiple' => true,
                'expanded' => true,
                'placeholder' => 'Choisir les cultures',
                //        'empty_data' => null,
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ParcellePlanSaison',
        ]);
    }
}
