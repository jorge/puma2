<?php

declare(strict_types=1);

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsultationF1Type extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Denomination', EntityType::class, [
                'label' => 'Coopératives',
                // query choices from this entity
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
                // used to render a select box, check boxes or radios
                //'multiple' => true,
                // 'expanded' => true,
            ])
            ->add('groupecoop', CheckboxType::class, [
                'label' => 'Groupé par coopérative?',
                'required' => false,
            ])
            ->add('groupegroup', CheckboxType::class, [
                'label' => 'Groupé par groupement?',
                'required' => false,
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Année',
                'choices' => array_combine(range(date('Y') - 15, date('Y')), range(date('Y') - 15, date('Y'))),
                'required' => false, //pour éviter la validation
                'expanded' => false,
            ])
            ->add('saison', ChoiceType::class, [
                'label' => 'Saison',
                'choices' => ['Les deux saisons' => '', 'saison A' => 'Saison A', 'saison B' => 'saison B'],
                'required' => false, //pour éviter la validation
                'expanded' => false,
            ])
            ->add('queryTheme', ChoiceType::class, [
                'label' => 'Theme de regroupement',
                'choices' => [
                    'PRODUCTION ET COÜT DE PRODUCTION DE LA SAISON PRÉCÉDENTE' => 'PRODUCTION',
                    'AUTRES SERVICES CONSOMMÉS, CHARGES DIVERSES, IMPÖTS ET TAXES, INTERETS' => 'SERVICES',
                    'ALÉAS RECONTRÉS (Importance des aleas rencontrés)' => 'ALEAS',
                    'PRODUITS DES VENTES ET AUTRES UTILISATIONS DE LA PRODUCTION (Ventes, autoconsommation et semences, groupés par produit)' => 'PRODUITS',
                    'ÉLEVAGE/ TROUPEAUX ET PRODUCTION ANNUELLE, CHARGES' => 'ELEVAGE',
                    'PLANIFICATION DE LA SAISON SUIVANTE' => 'PLANIFICATION',
                    'SITUATION DES COTISATIONS (Cotisations payés et impayés)' => 'COTISATIONS',
                    'CRÉDIT DEMANDÉ (Crédit cash et crédit intrants demandé)	' => 'CRÉDIT',
                    'CRÉDIT(DETTES) EN COURS' => 'DETTES',
                ],
                'placeholder' => false,
                'expanded' => false,
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-theme'],
            ])
            /* 1 */
            ->add('coutProduction', ChoiceType::class, [
                'choices' => [
                    'Quantité de semence acheté, groupé par fournisseur' => 'sem_achete_par_fournis',
                    'Quantité de semence autoproduit et acheté, groupé par culture' => 'sem_auto_par_culture',
                    'Quantité de engrais acheté, groupé par type d\'engrais' => 'engrais_par_type',
                    'Quantité de phytosanitaires acheté, groupé par type de phytosanitaire' => 'phyto_par_type',
                    'Quantité de produits récoltés, groupé par produit premier, deuxième et troisième' => 'produits_par_pdt',
                    'Rêquete special:Production de tomates sur 4 coopératives' => 'tomate_pour_4_coops',
                    'Evaluation du coût de production de la saison précedente (résumé partiel)' => 'eval_cout_precedente',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-production'],
            ])
            /* 2 */
            ->add('sommesCharges', ChoiceType::class, [
                'choices' => [
                    'Sommes des charges diverses ' => 'sommes_charges',
                    'Sommes des charges diverses, groupés par culture' => 'sommes_charges_par_culture',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-sommes'],
            ])
            /* 5 */
            ->add('coutElevage', ChoiceType::class, [
                'choices' => [
                    'Elevage groupés par espèce' => 'elevage_par_espece',
                    'Charges groupés par espèce' => 'charges_par_espece',
                    'Commercialisation groupés par espèce' => 'comm_par_espece',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-elevage'],
            ])

            /* 6 */
            ->add('Prevus', ChoiceType::class, [
                'choices' => [
                    'Cultures prevus' => 'culture_prevu',
                    'Engrais prevus' => 'engrais_prevu',
                    'Phytosanitaires prevus' => 'phyto_prevu',
                    'Production estimé' => 'prod_estime',
                    'Prévisions pour la saison suivante (résumé partiel)' => 'previsions',
                    'Semances demandés' => 'semances_demande',
                    'Engrais demandés' => 'engrais_demande',
                    'Phytosanitaires demandés' => 'phyto_demande',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-prevus'],
            ])

            /* 7 */
            ->add('Dettes', ChoiceType::class, [
                'choices' => [
                    'Dettes groupés par source de crédits' => 'dettes_par_source',
                    'Dettes groupés par type de garantie' => 'dettes_par_garantie',
                ],
                'placeholder' => false,
                'expanded' => true,
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-dettes'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ConsultationF1',
        ]);
    }
}
