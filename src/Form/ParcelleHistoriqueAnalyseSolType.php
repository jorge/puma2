<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcelleHistoriqueAnalyseSolType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parcelleHistoriquesAnalyseSol = $options['data'];
        $annee = $parcelleHistoriquesAnalyseSol->getParcelleHistorique()->getAnnee();
        $builder
            ->add('date', DateType::class, [
                'label' => 'Date',
                'format' => 'dd-MM-yyyy',
                'years' => range(date($annee), date($annee)),
            ])
            ->add('analyseSol', null, [
                'label' => 'Analyse de sol',
                'required' => true,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ParcelleHistoriqueAnalyseSol',
        ]);
    }
}
