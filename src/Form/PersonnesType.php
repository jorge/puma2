<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Personnes;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonnesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('nom', null, [
                'label' => 'Nom',
                'required' => true,
            ])
            ->add('prenom', null, [
                'label' => 'Prénom',
                'required' => true, ])
            ->add('photoUploadedFile', FileType::class, [
                'label' => 'Charger Photo déjà pris(png ou jpeg)',
                'required' => false,
                'attr' => ['accept' => 'image/*;capture=camera'], ])
            ->add('dateNaissance', DateType::class, [
                'label' => 'Date de naissance',
                'format' => 'd-M-y',
                'years' => range(date('Y') - 14, date('Y') - 120),
            ])
            ->add('sexe', ChoiceType::class, [
                'choices' => [
                    'Masculin' => 'Masculin',
                    'Féminin' => 'Féminin', ],
                'placeholder' => 'Choisir le sexe',
                'required' => false, ])
            ->add('etatCivil')
            ->add('cni', null, ['label' => 'CNI'])
            ->add('repondant', ChoiceType::class, [
                'choices' => [
                    'Vrai' => true,
                    'Faux' => false, ],
                'placeholder' => 'Choisir',
                'required' => false,
                'label' => 'Répondant?',
            ])
            ->add(
                'membreMenage',
                ChoiceType::class,
                [
                    'choices' => [
                        'Vrai' => true,
                        'Faux' => false, ],
                    'placeholder' => 'Choisir',
                    'required' => true,
                    'label' => 'Membre du menage?',
                ]
            )
            ->add('rolFamille', ChoiceType::class, [
                'choices' => [
                    Personnes::role_chefDeFamille => Personnes::role_chefDeFamille,
                    Personnes::role_conjoint => Personnes::role_conjoint,
                    Personnes::role_pas => Personnes::role_pas, ],
                'required' => false,
                'label' => 'Rôle dans la famille', ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Personnes',
        ]);
    }
}
