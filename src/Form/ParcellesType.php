<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcellesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('numeroParcelle', null, ['label' => 'Numéro de la parcelle'])
            ->add('surface', NumberType::class, [
                'label' => 'Surface (ares)',
                'required' => false,
            ])
            ->add('localisationAdmin', null, [
                'attr' => ['class' => 'select2'],
            ])
            ->add('propriete')
            // ->add('latitude')
            // ->add('longitude')
            ->add('situation')
            ->add('exposition')
            ->add('pente')
        //    ->add('antierosives', null, array('label' => 'Lutte antiérosive'))
            ->add('coutLocation', null, ['label' => 'Coût de location (BIF)'])
            // TODO mettre BIF dans une varaiable que l'on peut chosir !
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Parcelles',
        ]);
    }
}
