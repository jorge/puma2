<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class MoFamilialeExterneType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('membres', null, [
                'label' => 'Membres de la famille',
                'required' => true,
            ])
            ->add('typeTravail', null, [
                'label' => 'Nature du travail',
            ])
            ->add('lieu')
            ->add('revenu', null, [
                'label' => 'Revenu annuel (FBu)',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\MoFamilialeExterne',
        ]);
    }
}
