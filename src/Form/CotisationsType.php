<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CotisationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $pivotexploitationorganisation_id = $options['data']->getPivotExploitationOrganisation()->getId();
        $builder
            ->add('payeRegulierement', ChoiceType::class, [
                'choices' => [
                    'Oui' => -1,
                    'Non' => 0, ],
                'placeholder' => 'Choisir', ])
            ->add('payeAnnee', TextType::class, [
                'label' => 'Solde Payé l\'année passé (n-1) (BIF)',
            ])
            ->add('cotisationsImpaye', TextType::class, [
                'label' => 'Solde des cotisations dues (BIF)',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Cotisations',
        ]);
    }
}
