<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonnesLightType extends AbstractType
{
    /**
     * Formulaire "léger" pour ajouter une personne. Utilisé lors de la création d'une exploitation.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $anneeAjd = (int) (date('Y'));
        $choixAnnees = array_combine(
            range(1900, $anneeAjd),
            range(1900, $anneeAjd)
        );

        $builder
            ->add('nom', null, ['required' => true])
            ->add('prenom', null, ['required' => true, 'label' => 'Prénom'])
            ->add('sexe', ChoiceType::class, [
                'choices' => [
                    'Masculin' => 'Masculin',
                    'Féminin' => 'Féminin', ],
                'placeholder' => 'Choisir le sexe',
                'required' => false, ])
            ->add('dateNaissance', DateType::class, [
                'label' => 'Date de naissance',
                'format' => 'd-M-y',
                'years' => range(date('Y') - 14, date('Y') - 120),
            ])
            ->add('etatCivil')
            ->add('cni', null, ['label' => 'CNI']);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Personnes',
        ]);
    }
}
