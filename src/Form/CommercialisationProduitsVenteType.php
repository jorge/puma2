<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblProduits;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommercialisationProduitsVenteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $elevage = $_GET['elevage'];
        $unites = $options['unites'];

        $builder
            ->add('produit', EntityType::class, [
                'label' => 'Produits',
                'class' => TblProduits::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->prendreListe($_GET['elevage']);
                },
                'placeholder' => 'Choisir un produit',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('dateExecution', DateType::class, [
                'label' => 'date d\'execution',
                'disabled' => false,
                'placeholder' => [
                    'year' => 'Année',
                    'month' => 'Mois',
                    'day' => 'Jour', ],
            ])

            ->add('unite', ChoiceType::class, [
                'label' => 'Unité',
                'choices' => $unites,
                'placeholder' => 'Choisir une unité', ])
            ->add('quantite', TextType::class, [
                'label' => 'Quantité',
            ])
            ->add('prixVente');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\CommercialisationProduits',
            'elevage' => '',
            'unites' => null,
        ]);
    }
}
