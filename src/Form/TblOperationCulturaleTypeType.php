<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblOperationCulturaleType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TblOperationCulturaleTypeType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nom')
            ->add('category')
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'Seul' => TblOperationCulturaleType::TYPE_SEUL,
                    'intrant' => TblOperationCulturaleType::TYPE_AVEC_INTRANT,
                    'extrant' => TblOperationCulturaleType::TYPE_AVEC_EXTRANT,
                ],
                'placeholder' => false,
            ])
            ->add('agroEco', null, [
                'label' => 'Pratique agro-écologique',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\TblOperationCulturaleType',
        ]);
    }
}
