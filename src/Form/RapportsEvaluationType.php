<?php

declare(strict_types=1);

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RapportsEvaluationType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $nb_annees = $options['nb_annees_passees'];
        $builder
            ->add('Denomination', EntityType::class, [
                'label' => 'Coopératives',
                // query choices from this entity
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllFilsOrganisation('fedead58-79ad-11e6-abc5-b8ca3a965972');
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
            ])
            ->add('groupecoop', CheckboxType::class, [
                'label' => 'Groupé par coopérative?',
                'required' => false,
            ])
            ->add('groupegroup', CheckboxType::class, [
                'label' => 'Groupé par groupement?',
                'required' => false,
            ])
            ->add('exploitationGroupAnnee', CheckboxType::class, [
                'label' => 'Groupé par année d\'enquête?',
                'required' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Année d\'enquête',
                'choices' => array_combine(range(date('Y') - $nb_annees, date('Y')), range(date('Y') - $nb_annees, date('Y'))),
                'expanded' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('queryTheme', ChoiceType::class, [
                'label' => 'Liste de rapports',
                'choices' => [
                    'Choisir un rapport' => '',
                    'Nombre de membres et de groupement par coopérative (021, 022)' => 'rapp1',
                    'Nombre et pourcentage par coopérative d’hommes et de femmes et de jeunes (<35 ans) (031)' => 'rapp2',
                    'Moyenne par catégorie du nombre de personnes à charge par exploitation, par coopérative (04)' => 'rapp3',
                    '***Taux moyen d’alphabétisation par sexe pour les coopératives (051, 052)' => 'rapp4',
                    '***Classement et occurrence des cultures et élevages par coopérative pour les lignes 1, 2, 3 (351)' => 'rapp5',
                    'Nombre de groupement, de membres, de surface (213 par commune (SIG)' => 'rapp6',
                    'Nombre et pourcentage, par coopérative, de membres en ordre de cotisation (639)' => 'rapp7',
                    'Montant total des cotisations (2011) , par coopérative (639)' => 'rapp8',
                    'Pourcentage, par coopérative, de membres qui participent au warrantage (381), vente groupée (382), transformation (383)' => 'rapp9',
                    'Nombre et pourcentage de membres ayant reçu une, deux formations tous opérateurs confondu, et via la CAPAD (638)' => 'rapp10',
                    'Pourcentage relatif entre CAPAD et Autres opérateurs (638)' => 'rapp11',
                    'Nombre et pourcentage, par coopérative, des occurrences par thème de formation (638)' => 'rapp12',
                    'Classement et pourcentage, par coopérative, des exploitations selon le type de toiture et de mur (9221)' => 'rapp13',
                    'Nombre et pourcentage des exploitants exerçant l’agriculture à titre principal (061)' => 'rapp14',
                    'Pourcentage des autres métiers exercés par les membres de la CAPAD (061)' => 'rapp15',
                    'Répartition des membres producteurs par classe d’âge et par Coopérative' => 'rapp16',
                    'Répartition des membres producteurs par situation matrimoniale et par Coopérative' => 'rapp17',
                    'Répartition des membres producteurs par niveau d’études et par Coopérative' => 'rapp18',
                    'Répartition des membres producteurs par nombre moyen de personnes à charge et par Coopérative' => 'rapp19',
                    'Répartition des membres producteurs par nombre moyen de fils et par Coopérative' => 'rapp20',
                    'Répartition des membres producteurs par nombre moyen de filles et par Coopérative' => 'rapp21',
                    'Répartition des membres producteurs par nombre moyen de fils scolarisés et par Coopérative' => 'rapp22',
                    'Répartition des membres producteurs par nombre moyen de filles scolarisées et par Coopérative' => 'rapp23',
                    'Production par filière et par Coopérative' => 'rapp24',
                    'Production par filière et par Coopérative' => 'rapp25',
                    'Quantité commercialisée par produit et par Coopérative' => 'rapp26',
                    'Quantité commercialisée par produit et par Coopérative' => 'rapp27',
                    'Quantité commercialisée par produit et par Coopérative' => 'rapp28',
                    'Répartition de membres producteurs par moyen d’acquisition de terre et par coopérative' => 'rapp29',
                    'Répartition de membres producteurs selon l’existence de protection anti érosive par Coopérative' => 'rapp30',
                    'Répartition des membres producteurs selon l’utilisation d’engrais par Coopérative' => 'rapp31',
                    'Répartition des membres producteurs selon l’utilisation de l’amendement calcaire par Coopérative' => 'rapp32',
                    'Répartition de membres producteurs selon l’espèce animale possédée par Coopérative' => 'rapp33',
                    'Répartition de membres producteurs selon le nombre d’espèce animale possédée par Coopérative' => 'rapp34',
                    'Répartition de membres producteurs selon le type d’élevage pratiqué par Coopérative' => 'rapp35',
                    'Répartition de membres producteurs selon le principal produit de l’élevage par Coopérative' => 'rapp36',
                    'Répartition de membres producteurs selon le principal moyen de transport par Coopérative' => 'rapp37',
                    'Répartition de membres producteurs selon le principal produit warranté par Coopérative' => 'rapp38',
                    'Répartition de membres producteurs selon la quantité du produit warranté par Coopérative' => 'rapp39',
                    'Répartition de membres producteurs selon le principal produit vendu en groupe par Coopérative' => 'rapp40',
                    'Répartition de membres producteurs selon la quantité du produit vendu en groupe par Coopérative' => 'rapp41',
                    'Répartition de membres producteurs selon le principal produit transformé par Coopérative' => 'rapp42',
                    'Répartition de membres producteurs selon la quantité du produit transformé par Coopérative' => 'rapp43',
                    'Répartition de membres producteurs affirmant avoir cotisé régulièrement par Coopérative' => 'rapp44',
                    'Répartition de membres producteurs selon la cotisation moyenne payé en 2011 par Coopérative' => 'rapp45',
                    'Répartition de membres producteurs selon les thèmes de formation qu’eux et leurs conjoints ont bénéficié' => 'rapp46',
                    'Répartition de membres producteurs selon le bénéficiaire de la formation qu’eux et leurs conjoints ont bénéficié' => 'rapp47',
                    'Répartition de membres producteurs selon l’utilisation faite de la formation qu’eux et leurs conjoints ont bénéficié' => 'rapp48',
                    'Répartition de membres producteurs selon l’organisateur de la formation qu’eux et leurs conjoints ont bénéficié' => 'rapp49',
                    'Répartition de membres producteurs selon les thèmes de formation qu’eux et leurs conjoints ont besoin' => 'rapp50',
                    'Répartition de membres producteurs selon les contraintes majeures au développement des activités agricoles citées' => 'rapp51',
                    'Répartition de membres producteurs selon les services dont ils ont besoin' => 'rapp52',
                ],
                'expanded' => false,
            ])
            ->add('queryAjout', ChoiceType::class, [
                'label' => 'Liste de rapports ajoutés',
                'choices' => [
                    'Choisir un rapport...' => '',
                    'F0 - les superficies et la production de mais cultivé par coopérative' => 'AJOUT1',
                    'F0 - les superficies et la production de riz cultivé par coopérative' => 'AJOUT2',
                    'F0 - les superficies et la production de haricot cultivé par coopérative' => 'AJOUT3',
                    'F0 - les superficies et la production de légumes cultivé par coopérative' => 'AJOUT4',
                    'F0 - les exploitations qui ont déclaré avoir des dispositifs anti érosifs par coopérative' => 'AJOUT5',
                    'F1 - la part de la production qui a été autoconsommée par coopérative' => 'AJOUT6',
                    'F1 - les revenus issus de la vente de la production agricole' => 'AJOUT7',
                    'Suivi coopératives - les coopératives qui répondent OUI aux indicateurs institutionnels' => 'AJOUT8',
                    'Suivi coopératives - les coopératives qui répondent OUI aux indicateurs de gestion administrative et financière' => 'AJOUT9',
                    'F1 - Liste des exploitations avec F1, par cooperative' => 'AJOUT10',
                    'F0 - Liste des exploitations avec F0, par cooperative' => 'AJOUT11',
                ],
                'expanded' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\RapportsEvaluation',
        ]);
        /* déclarer ici les options additionnelles passées au formulaire depuis le controleur */
        $resolver->setRequired('nb_annees_passees'); // Requires that nb_annees_passees be set by the caller.
        $resolver->setAllowedTypes('nb_annees_passees', 'integer'); // Validates the type(s) of option(s) passed.
    }
}
