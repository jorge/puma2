<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\PersonneDetails;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonneDetailsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('kirundiLire', ChoiceType::class, [
                'choices' => [
                    PersonneDetails::NOT_ENCODED_STR => PersonneDetails::NOT_ENCODED_VALUE,
                    'Oui' => PersonneDetails::YES_VALUE,
                    'Non' => PersonneDetails::NO_VALUE, ],
                'placeholder' => false,
            ])
            ->add('kirundiEcrire', ChoiceType::class, [
                'choices' => [
                    PersonneDetails::NOT_ENCODED_STR => PersonneDetails::NOT_ENCODED_VALUE,
                    'Oui' => PersonneDetails::YES_VALUE,
                    'Non' => PersonneDetails::NO_VALUE, ],
                'placeholder' => false,
            ])
            ->add('francaisLire', ChoiceType::class, [
                'choices' => [
                    PersonneDetails::NOT_ENCODED_STR => PersonneDetails::NOT_ENCODED_VALUE,
                    'Oui' => PersonneDetails::YES_VALUE,
                    'Non' => PersonneDetails::NO_VALUE, ],
                'placeholder' => false,
            ])
            ->add('francaisEcrire', ChoiceType::class, [
                'choices' => [
                    PersonneDetails::NOT_ENCODED_STR => PersonneDetails::NOT_ENCODED_VALUE,
                    'Oui' => PersonneDetails::YES_VALUE,
                    'Non' => PersonneDetails::NO_VALUE, ],
                'placeholder' => false,
            ])
            ->add('primaire', ChoiceType::class, [
                'choices' => [
                    PersonneDetails::NOT_ENCODED_STR => PersonneDetails::NOT_ENCODED_VALUE,
                    'Oui' => PersonneDetails::YES_VALUE,
                    'Non' => PersonneDetails::NO_VALUE, ],
                'placeholder' => false,
            ])
            ->add('secondaire', ChoiceType::class, [
                'choices' => [
                    PersonneDetails::NOT_ENCODED_STR => PersonneDetails::NOT_ENCODED_VALUE,
                    'Oui' => PersonneDetails::YES_VALUE,
                    'Non' => PersonneDetails::NO_VALUE, ],
                'placeholder' => false,
            ])
            ->add('diplome', ChoiceType::class, [
                'choices' => [
                    'Non encodé' => null,
                    'Pas de diplome' => PersonneDetails::DIPLOME_NO,
                    'A2' => PersonneDetails::DIPLOME_A2,
                    'A3' => PersonneDetails::DIPLOME_A3,
                    'D4' => PersonneDetails::DIPLOME_D4,
                    'D6' => PersonneDetails::DIPLOME_D6,
                    'D7' => PersonneDetails::DIPLOME_D7,
                    'Supérieur' => PersonneDetails::DIPLOME_SUP,
                ],
                'placeholder' => false,
            ])
            ->add('domaine')
            ->add('professionPrincipale')
            ->add('professionSecondaire');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\PersonneDetails',
        ]);
        //$resolver->setRequired('coop_id');
//      $resolver->setRequired('group_id');
    }
}
