<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CollecteType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('enqueteur', null, [
                'label' => "Nom de l'enquêteur",
            ])
            ->add('date', null, [
                'label' => "Date du passage de l'enquêteur",
            ])
            ->add('type', ChoiceType::class, [
                'choices' => [
                    'F0' => 0,
                    'F1' => 1,
                    'F2' => 2,
                    'F3' => 3,
                    'F4' => 4,
                    'F5' => 5,
                    'F6' => 6, ],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Collecte',
        ]);
    }
}
