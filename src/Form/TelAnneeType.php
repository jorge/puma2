<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class TelAnneeType extends AbstractType
{
    /**
     * Form to choose les infos du menage.
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $debut = (int) (date('Y'));
        $listAnnees['non-renseigé'] = (string) 9999;

        for ($i = 0; 20 > $i; ++$i) {
            $listAnnees[(string) ($debut - $i)] = (string) ($debut - $i);
        }
        $builder
            ->add('anneeAdhesion', ChoiceType::class, [
                'label' => 'Année d\'adhesion',
                'choices' => $listAnnees,
                'placeholder' => 'Choisir un nombre',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('telephone');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\telAnnee',
        ]);
    }
}
