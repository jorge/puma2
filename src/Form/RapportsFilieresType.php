<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblOrganisations;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RapportsFilieresType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // $nb_annees = $options['nb_annees_passees'];
        $nb_annees = 15;
        $builder
            ->add('Denomination', EntityType::class, [
                'label' => 'Coopératives',
                // query choices from this entity
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
                // used to render a select box, check boxes or radios
                //'multiple' => true,
                //'expanded' => true,
            ])
            ->add('groupementType', ChoiceType::class, [
                'label' => 'Choix de groupement',
                'choices' => [
                    'province' => 'province',
                    'cooperative' => 'cooperative',
                    'groupement' => 'groupement',
                ],
                'expanded' => true,
                'attr' => ['class' => 'show-exploitant show-genre'],
            ])
            ->add('exploitationGroupAnnee', CheckboxType::class, [
                'label' => 'Groupé par année d\'enquête?',
                'required' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Année de l\'enquête',
                'choices' => array_combine(range(date('Y') - $nb_annees, date('Y')), range(date('Y') - $nb_annees, date('Y'))),
                'expanded' => false,
                'attr' => ['class' => 'show-exploitation'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\RapportsFilieres',
        ]);
        /* déclarer ici les options additionnelles passées au formulaire depuis le controleur */
    //    $resolver->setRequired('nb_annees_passees'); // Requires that nb_annees_passees be set by the caller.
    //    $resolver->setAllowedTypes('nb_annees_passees', 'integer'); // Validates the type(s) of option(s) passed.
    }
}
