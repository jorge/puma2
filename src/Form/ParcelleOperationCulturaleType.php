<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblOperationCulturaleType;
use App\Entity\TblCultures;
use App\Entity\TblProduits;
use App\Entity\TblOperationCulturaleIntrant;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ParcelleOperationCulturaleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $parcelleOperationCulturale = $options['data'];

        $parcelleHistorique = $parcelleOperationCulturale->getParcelleHistorique();
        $parcelleHistoriqueCultures = $parcelleHistorique->getCultures();

        $annee = $parcelleOperationCulturale->getParcelleHistorique()->getAnnee();

        $builder
            ->add('type', EntityType::class, [
                'class' => TblOperationCulturaleType::class,
                'choice_label' => 'nom',
                'group_by' => static function ($choiceValue, $key, $value) {
                    if ($choiceValue->getCategory()) {
                        return $choiceValue->getCategory()->getNom();
                    }

                    return 'Autre';
                },
                'label' => 'Operation culturale',
            ])
            ->add('cultures', EntityType::class, [
                'label' => 'Cultures:',
                'class' => TblCultures::class,
                'choices' => $parcelleOperationCulturale->getParcelleHistorique()->getCultures(),
                'attr' => ['class' => 'grid_6cols'],
                'multiple' => true,
                'expanded' => true,
                'placeholder' => 'Choisir les cultures',
                'empty_data' => null,
                'required' => true,
                'multiple' => true,
            ])
            ->add('date', DateType::class, [
                'label' => 'Date',
                'format' => 'dd-MM-yyyy',
                'years' => range($annee, $annee)

            ])
            ->add('mainOeuvreFamiliale', null, [
                'label' => "Main-d'œuvre familiale (h/j)",
            ])
            ->add('mainOeuvreSalarie', null, [
                'label' => "Main-d'œuvre salariée (h/j)",
            ])
            ->add('intrant')
            ->add('produitPrimaire') /// extrant
            ->add('quantiteUtiliseeAutoconsommation', null, [
                'label' => 'Quantité Utilisée Autoproduction',
                'required' => false,
                'attr' => ['class' => 'intrant'],
            ])
            ->add('quantiteRecolteeVente', null, [
                'label' => 'Quantité Recoltée Vente',
                'required' => false,
                'attr' => ['class' => 'extrant'],
            ])
            ->add('quantiteRecolteAutoconsommation', null, [
                'label' => 'Quantité Recoltée Autoconsommation',
                'required' => false,
                'attr' => ['class' => 'extrant'],
            ])
            ->add('quantiteUtiliseAchete', null, [
                'label' => 'Quantité Utilisée Acheté',
                'required' => false,
                'attr' => ['class' => 'intrant'],
            ]);

        $builder->addEventListener(
            FormEvents::PRE_SET_DATA,
            function (FormEvent $event) {
                // event triggered before the entity is passed to the form

                /** @var ParcelleOperationCulturale $data */
                $data = $event->getData(); // here $data has the type ParcelleOperationCulturale

                $this->addPrimaire($event->getForm(), $data->getCultures());
                $this->addIntrant($event->getForm(), $data->getType());
            }
        );

        $builder->get('type')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                // event triggered after the entity received the data from the form

                /** @var TblOperationCulturaleType $data */
                $data = $event->getForm()->getData(); // here $data has the type TblOperationCulturaleType
                $this->addIntrant($event->getForm()->getParent(), $data);
            }
        );

        $builder->get('cultures')->addEventListener(
            FormEvents::POST_SUBMIT,
            function (FormEvent $event) {
                // event triggered after the entity received the data from the form

                /** @var TblCultures $data */
                $data = $event->getForm()->getData(); // here $data has the type TblCultures
                $this->addPrimaire($event->getForm()->getParent(), $data);
            }
        );
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ParcelleOperationCulturale',
        ]);
    }

    /**
     * Add the field intrant.
     */
    private function addIntrant(FormInterface $form, ?TblOperationCulturaleType $type = null)
    {
        $form->add('intrant', EntityType::class, [
            'label' => 'Intrant (avec unité)',
            'choice_label' => 'nomAvecUnite',
            'class' => TblOperationCulturaleIntrant::class,
            'query_builder' => static function (EntityRepository $er) use ($type) {
                $opCuTypeId = -1;

                if (null !== $type) {
                    $opCuTypeId = $type->getId();
                }

                return $er->createQueryBuilder('intr')
                    ->join('intr.operationCulturaleType', 'octype')
                    ->where('octype.id=:op_cult')
                    ->setParameter('op_cult', $opCuTypeId);
            },
            'required' => false,
            'attr' => ['class' => 'intrant'],
        ]);
    }

    /**
     * Add the field produitPrimaire into the form.
     */
    private function addPrimaire(FormInterface $form, Collection $cultures)
    {
        $form->add('produitPrimaire', EntityType::class, [
            'class' => TblProduits::class,
            'query_builder' => static function (EntityRepository $er) use ($cultures) {
                $idCultures = [];

                foreach ($cultures->toArray() as $culture) {
                    $idCultures[] = $culture->getId();
                }

                if ([] !== $idCultures) {
                    return $er->createQueryBuilder('p')
                        ->where('p.culture IN(:cults)')
                        ->setParameter('cults', array_values($idCultures));
                }
            },
            'attr' => ['class' => 'extrant'],
            'placeholder' => false,
            'expanded' => false,
            'required' => false,
            'label' => 'Extrant (avec unité)',
            'choice_label' => 'nomAvecUnite',
        ]);
    }
}
