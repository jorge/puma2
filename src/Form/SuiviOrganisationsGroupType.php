<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblValeurChoixSimple;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SuiviOrganisationsGroupType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('annee')
            ->add('existenceROI', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
/*            ->add('existenceStatus', ChoiceType::class, array(
                'choices' => array(
                'Non' => 0,
                'Oui' => 1
                ),
                'expanded' => true,
                ))
 */
            ->add('existenceCE', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('nbrReunionsAG')
            ->add('nbrReunionsCE')
//            ->add('nbrReunionsCS')
            ->add('majListeDeMembres', EntityType::class, [
                'label' => 'Mise à jour de la liste des membres',
                'class' => TblValeurChoixSimple::class,
                'attr' => ['class' => 'select2'],
                'multiple' => true,
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.propiete = :p')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('p', 'majListeDeMembres');
                },
            ])
            ->add('classement', EntityType::class, [
                'label' => 'Système de classement',
                'class' => TblValeurChoixSimple::class,
                'attr' => ['class' => 'select2'],
                'multiple' => true,
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.propiete = :p')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('p', 'classement');
                },
            ])

            ->add('regAnnuPVReunionsAG', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('regAnnuPVReunionsCE', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
/*            ->add('pvReunionsCs', ChoiceType::class, array(
                'choices' => array(
                'Non' => 0,
                'Oui' => 1
                ),
                'expanded' => true,
                ))
 */
            ->add('planActionAnnuel', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('rapportAnnuelActivites', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('compteBancaire', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('soldeAnneeAvant')
            ->add('signaturesPourSortieFonds', ChoiceType::class, [
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                ],
                'expanded' => true,
                'required' => false,
            ])
            ->add('enregistrementCotisations', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('livreCaisse', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('carnetRecus', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('livretSocietaire', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('classementJustificatifs', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('rapportsFinanciereAnnuel', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('cotisationsAnneeAvant')
            ->add('membresContracteCredit')
            ->add('membresCreditMontant')
            ->add('creditPourOrganisationMontant')
            ->add('institutionBancaire')
            ->add('institutionCreditMembres')
            ->add('institutionFinancementCreditOrg')
            ->add('infrastructures', null, [
                'attr' => ['class' => 'select2'],
            ])
            ->add('uniteDeTransformation', null, [
                'attr' => ['class' => 'select2'],
            ])
            ->add('materielRoulant', null, [
                'attr' => ['class' => 'select2'],
            ])
            ->add('materielDeBureau', null, [
                'attr' => ['class' => 'select2'],
            ])
            ->add('equipementAgricole', null, [
                'attr' => ['class' => 'select2'],
            ])
            ->add('besoinsFormation', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Formations',
            ])
            ->add('besoinsServicesAuxMembres', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Services',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\SuiviOrganisations',
        ]);
    }
}
