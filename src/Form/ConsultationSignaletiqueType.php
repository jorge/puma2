<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblOrganisations;
use App\Entity\TblEtatCivil;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ConsultationSignaletiqueType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //  $nb_annees = $options['nb_annees_passees'];
        $nb_annees = 20;
        $builder
            ->add('denomination', EntityType::class, [
                'label' => 'Coopératives',
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
                // used to render a select box, check boxes or radios
                //'multiple' => true,
                //'expanded' => true,
            ])
            ->add('groupecoop', CheckboxType::class, [
                'label' => 'Groupé par coopérative?',
                'required' => false,
            ])
            ->add('groupegroup', CheckboxType::class, [
                'label' => 'Groupé par groupement?',
                'required' => false,
            ])
            ->add('exploitationGroupAnnee', CheckboxType::class, [
                'label' => 'Groupé par année d\'adhésion?',
                'required' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Année d\'adhésion',
                'choices' => array_combine(range(date('Y') - $nb_annees, date('Y')), range(date('Y') - $nb_annees, date('Y'))),
                'expanded' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('queryTheme', HiddenType::class)
            /* Lié à exploitation */
            ->add('exploitantTheme', ChoiceType::class, [
                'label' => 'Theme de regroupement pour l\'exploitant',
                'choices' => [
                    'Age' => 'age',
                    'Etat Civil' => 'etat civil',
                    'Genre' => 'genre',
                ],
                'expanded' => true,
                'attr' => ['class' => 'show-exploitant'],
            ])
            ->add('exploitantAgeMin', TextType::class, [
                'label' => 'Age minimum',
                'required' => true,
                'empty_data' => 20,
                'attr' => ['class' => 'show-exploitant show-age'],
            ])
            ->add('exploitantAgeMax', TextType::class, [
                'label' => 'Age maximum',
                'required' => true,
                'empty_data' => 70,
                'attr' => ['class' => 'show-exploitant show-age'],
            ])
            ->add('exploitantEtatCivilGroup', CheckboxType::class, [
                'label' => 'Groupé par état civil?',
                'required' => false,
                'attr' => ['class' => 'show-exploitant show-etatCivil'],
            ])
            ->add('exploitantEtatCivilList', EntityType::class, [
                'label' => 'Etat civil',
                'class' => TblEtatCivil::class,
                'choice_label' => 'etatCivil',
                'placeholder' => 'Choisir un état civil',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-exploitant show-etatCivil'],
                // used to render a select box, check boxes or radios
                //'multiple' => true,
                //'expanded' => true,
            ])
            ->add('exploitantGenreGroup', CheckboxType::class, [
                'label' => 'Groupé par genre?',
                'required' => false,
                'attr' => ['class' => 'show-exploitant show-genre'],
            ])
            ->add('exploitantGenreList', ChoiceType::class, [
                'label' => 'Genre',
                'choices' => [
                    'masculin' => 'masculin',
                    'féminin' => 'féminin',
                ],
                'expanded' => true,
                'attr' => ['class' => 'show-exploitant show-genre'],
            ])
            ->add('chargesTheme', ChoiceType::class, [
                'label' => 'Theme de regroupement pour les personnes à charge',
                'choices' => [
                    'Quantité' => 'nombre',
                    'Enfants scolarisés' => 'scolarise',
                ],
                'expanded' => true,
                'attr' => ['class' => 'show-charges'],
            ])
            ->add('chargesNombreGroup', CheckboxType::class, [
                'label' => 'Groupé par nombre de personnes à charge',
                'required' => false,
                'attr' => ['class' => 'show-charges show-nombre'],
            ])
            ->add('chargesNombreMin', NumberType::class, [
                'label' => 'Nombre de personnes à charge minimum',
                'required' => false,
                'attr' => ['class' => 'show-charges show-nombre'],
            ])
            ->add('chargesNombreMax', NumberType::class, [
                'label' => 'Nombre de personnes à charge maximum',
                'required' => false,
                'attr' => ['class' => 'show-charges show-nombre'],
            ])
            ->add('chargesScolariseFils', CheckboxType::class, [
                'label' => 'Total de fils, et pourcentage de fils scolarisés',
                'required' => false,
                'attr' => ['class' => 'show-charges show-scolarise'],
            ])
            ->add('chargesScolariseFilles', CheckboxType::class, [
                'label' => 'Total de filles, et pourcentage de filles scolarisées',
                'required' => false,
                'attr' => ['class' => 'show-charges show-scolarise'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ConsultationSignaletique',
        ]);
        /* déclarer ici les options additionnelles passées au formulaire depuis le controleur */
    //    $resolver->setRequired('nb_annees_passees'); // Requires that nb_annees_passees be set by the caller.
    //    $resolver->setAllowedTypes('nb_annees_passees', 'integer'); // Validates the type(s) of option(s) passed.
    }
}
