<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CommercialisationProduitsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('mois')
            ->add('saison')
            ->add('typeCommercialisation')
            ->add('produit')
            ->add('quantite')
            ->add('prixVente')
            ->add('qteAutoconsommation')
            ->add('prixRefAutoconsommation')
            ->add('qteSemences')
            ->add('prixRefSemences')
            ->add('anneeSaison');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\CommercialisationProduits',
        ]);
    }
}
