<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblLabourAnimaux;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class AnimauxLabourType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('labour', EntityType::class, [
                'label' => 'Travail',
                'class' => TblLabourAnimaux::class,
                'placeholder' => 'Veuillez sélectionner le labour',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('quantiteMOFam', IntegerType::class, [
                'label' => 'Quantite main d\'oeuvre familiale (h/j)',
            ])
            ->add('quantiteMOSal', IntegerType::class, [
                'label' => 'Quantite main d\'œuvre salariée (h/j)',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\AnimauxLabour',
        ]);
    }
}
