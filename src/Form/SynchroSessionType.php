<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SynchroSessionType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('dateDebutData', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, ['label' => 'ma date'])
            ->add('dateFin', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, ['label' => 'ma date'])
            ->add('nbtry')
            ->add('zipname')
            ->add('zipmd5')
            ->add('status')
            ->add('remoteDateDebut', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, ['label' => 'ma date'])
            ->add('remoteDateFin', \Symfony\Component\Form\Extension\Core\Type\DateTimeType::class, ['label' => 'ma date'])
            ->add('remoteNbtry')
            ->add('remoteZipname')
            ->add('remoteZipmd5')
            ->add('remoteStatus')
            ->add('closed');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\SynchroSession',
        ]);
    }
}
