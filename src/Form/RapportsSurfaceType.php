<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\TblOrganisations;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RapportsSurfaceType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Denomination', EntityType::class, [
                'label' => 'Coopératives',
                // query choices from this entity
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllCooperatives();
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Choisir une coopérative',
                'empty_data' => null,
                'required' => true, //pour éviter la validation
                // used to render a select box, check boxes or radios
                //'multiple' => true,
                //'expanded' => true,
            ])
//          ->add('groupecoop', CheckboxType::class, array(
//              'label'    => 'Groupé par coopérative?',
//              'required' => false,
//          ))
//      ->add('groupegroup', CheckboxType::class, array(
//              'label'    => 'Groupé par groupement?',
//              'required' => false,
//          ))
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\RapportsSurface',
        ]);

        $resolver->setDefault('method', 'GET')
            ->setDefault('csrf_protection', false);
        /* déclarer ici les options additionnelles passées au formulaire depuis le controleur */
//        $resolver->setRequired('nb_annees_passees'); // Requires that nb_annees_passees be set by the caller.
//        $resolver->setAllowedTypes('nb_annees_passees', 'integer'); // Validates the type(s) of option(s) passed.
    }
}
