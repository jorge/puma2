<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\Personnes;
use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormationsRecuType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $debut = (int) (date('Y'));

        for ($i = 0; 20 > $i; ++$i) {
            $listAnnees[(string) ($debut - $i)] = (string) ($debut - $i);
        }
        $expl_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('theme')
            ->add('niveauUtilite')
            ->add('organisateur')
            ->add('beneficiaires', EntityType::class, [
                'label' => 'Beneficiaires:',
                'class' => Personnes::class,
                'query_builder' => static function (EntityRepository $er) use ($expl_id) {
                    return $er->getCoexploitants($expl_id);
                },
                'attr' => ['class' => 'grid_1cols'],
                'multiple' => true,
                'expanded' => true,
                'placeholder' => 'Choisir les beneficiaires',
                'empty_data' => null,
                'required' => true,
            ])
            ->add('anneeFormation', ChoiceType::class, [
                'label' => 'Année de formation',
                'choices' => $listAnnees,
                'placeholder' => 'Choisir un année',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\FormationsRecu',
        ]);
    }
}
