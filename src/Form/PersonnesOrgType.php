<?php

declare(strict_types=1);

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class PersonnesOrgType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        //      $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('contact')
            ->add('nom')
            ->add('prenom');
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\Personnes',
        ]);
    }
}
