<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\SuiviOrganisations;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class SuiviOrganisationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('annee')
            ->add('existenceROI', ChoiceType::class, [
                'label' => 'Existence d\'un ROI',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true,
            ])
            ->add('existenceStatus', ChoiceType::class, [
                'label' => 'Existence de status',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true,
            ])
            ->add('existenceCE', ChoiceType::class, [
                'label' => 'CE',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true,
            ])
            ->add('existenceCS', ChoiceType::class, [
                'label' => 'CS',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true,
            ])
            ->add('niveauxReconnaissance', null, [
                'label' => 'Niveaux de reconnaissance',
                'attr' => ['class' => 'select2'],
                'required' => true,
            ])
            ->add('nbrReunionsAG', null, [
                'label' => 'Nbr. AG',
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'nbrReunionAG');
                },
            ])
            ->add('nbrReunionsCE', null, [
                'label' => 'Nbr. CE',
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'nbrReunionCECS');
                },
            ])
            ->add('nbrReunionsCS', null, [
                'label' => 'Nbr. CS',
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'nbrReunionCECS');
                },
            ])
            ->add('alternanceCSNbr', null, [
                'label' => 'alt. CS',
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'alternanceCECSNbr');
                },
            ])
            ->add('alternanceCENbr', null, [
                'label' => 'alt. CE',
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'alternanceCECSNbr');
                },
            ])
            ->add('planStrategiquePluriannuel', ChoiceType::class, [
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true,
            ])
            ->add('rapportSuiviPlanStrategique', ChoiceType::class, [
                'label' => 'Rapport de suivi-évaluation du plan stratégique',
                'choices' => [
                    'Oui' => true,
                    'Non' => false,
                ],
                'expanded' => true,
            ])
            ->add('evolutionNbrMembres', null, [
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'evolutionNbrMembres');
                },
                'label' => 'Évolution du nombre de membres',
                'attr' => ['class' => 'select2'],
            ])
            ->add('nombreTotalMembres', null, [
                'label' => 'Nombre total de membres',
            ])
            ->add('classement', null, [
                'label' => 'Système de classement',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'classement');
                },
            ])
            ->add('majListeDeMembres', null, [
                'label' => 'Mise à jour de la liste des membres',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'majListeDeMembres');
                },
            ])
            ->add('nbrPVManquantsAG', null, [
                'label' => 'AG',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'nbrPVManquants');
                },
            ])
            ->add('nbrPVManquantsCE', null, [
                'label' => 'CE',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'nbrPVManquants');
                },
            ])
            ->add('nbrPVManquantsCS', null, [
                'label' => 'CS',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'nbrPVManquants');
                },
            ])
            ->add('planActionAnnuel', ChoiceType::class, [
                'label' => 'Plan d\'action annuel',
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('rapportAnnuelActivites', ChoiceType::class, [
                'label' => 'Rapport annuel d\'activité',
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('comptabilite', null, [
                'label' => 'Comptabilité',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'comptabilite');
                },
            ])
            ->add('budget', null, [
                'label' => 'Budget annuel (Fbu)',
            ])
            ->add('chiffreAffaireParMembre', null, [
                'label' => "Chiffre d'affaires par membre (Fbu)",
            ])
            ->add('chiffreDAffaires', null, [
                'label' => 'Chiffre d\affaires (Fbu)',
            ])
            ->add('degrePriseEnChargeAG', null, [
                'label' => 'Degré de prise en charge du fonctionnement de l\'AG',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'degreePriseEnCharge');
                },
            ])
            ->add('degrePriseEnChargeCE', null, [
                'label' => 'Degré de prise en charge du fonctionnement du CE',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'degreePriseEnCharge');
                },
            ])
            ->add('degrePriseEnChargeCS', null, [
                'label' => 'Degré de prise en charge du fonctionnement du CS',
                'attr' => ['class' => 'select2'],
                'query_builder' => static function (EntityRepository $repository) {
                    return $repository->createQueryBuilder('v')
                        ->where('v.entityClass = :c')
                        ->andWhere('v.tag = :t')
                        ->orderBy('v.displayOrder')
                        ->setParameter('c', SuiviOrganisations::class)
                        ->setParameter('t', 'degreePriseEnCharge');
                },
            ])
            ->add('compteBancaire', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('soldeAnneeAvant', null, [
                'label' => 'Solde année avant (Fbu)',
            ])
            ->add('signaturesPourSortieFonds', ChoiceType::class, [ // todo pour val numeriques passer à ça
                'choices' => [
                    '1' => 1,
                    '2' => 2,
                    '3' => 3,
                ],
                'required' => true,
                'label' => 'Nombre de signatures pour sortie de fonds',
            ])
            ->add('enregistrementCotisations', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('livreCaisse', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('carnetRecus', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
                'label' => 'Carnet reçu',
            ])
            ->add('livretSocietaire', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('classementJustificatifs', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('institutionBancaire')
            ->add('rapportsFinanciereAnnuel', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
                'label' => 'Rapport financier annuel',
            ])
            ->add('auditAnnuelDesComptes', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
            ])
            ->add('nombreMembresPayantsCotisation', null, [
                'label' => 'Nombre de membres payant leur cotisation',
            ])
            ->add('ristourneParMembre')
            ->add('montantTotalCotisations', null, [
                'label' => 'Montant total des cotisations',
            ])
            ->add('montantServicesPayesParMembres', null, [
                'label' => 'Montant des services payés par les membres',
            ])
            ->add('montantServicesPayesParTiers', null, [
                'label' => 'Montant des services payés par un tiers',
            ])
            ->add('servicesAuxMembres1', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Services aux membres (1)',
            ])
            ->add('servicesAuxMembres2', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Services aux membres (2)',
            ])
            ->add('servicesAuxMembres3', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Services aux membres (3)',
            ])
            ->add('servicesPayesParMembres1', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Activités économiques payées par les membres (1)',
            ])
            ->add('servicesPayesParMembres2', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Activités économiques payées par les membres (2)',
            ])
            ->add('servicesPayesParMembres3', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Activités économiques payées par les membres (3)',
            ])
            ->add('servicesPayesParTiers1', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Activités économiques payées par les tiers (1)',
            ])
            ->add('servicesPayesParTiers2', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Activités économiques payées par les tiers (2)',
            ])
            ->add('servicesPayesParTiers3', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Activités économiques payées par les tiers (3)',
            ])
            ->add('nbrMUSO', null, [
                'label' => 'Nombre de MUSO',
            ])
            ->add('montantEpargneMUSO', null, [
                'label' => 'Montant déjà épargné pour MUSO (Fbu)',
            ])
            ->add('affiliationFederation', ChoiceType::class, [
                'choices' => [
                    'Non' => 0,
                    'Oui' => 1,
                ],
                'expanded' => true,
                'label' => 'Affiliation à une(des) fédération(s)',
            ])

            ->add('nbrContratsAvecActeursEco', null, [
                'label' => 'Nombre de contrats avec les acteurs économiques',
            ])
            ->add('nbrCreditAuxMembresParIMFOuBanque', null, [
                'label' => 'Nombre de crédits aux membres auprès des IMF ou banques',
            ])
            ->add('membresContracteCredit', null, [
                'label' => 'Nombre de membres ayant contracté un crédit',
            ])
            ->add('membresCreditMontant', null, [
                'label' => 'Montant total des crédits contractés par les membres (Fbu)',
            ])
            ->add('institutionCreditMembres', null, [
                'label' => 'Institution qui a octroyé les crédits aux  membres',
            ])

            ->add('nbrCreditAvecIMFOuBanque', null, [
                'label' => 'Nombre de crédits à la coopérative auprès des IMF ou banques',
            ])
            ->add('creditPourOrganisationMontant', null, [
                'label' => 'Montant total des crédits contractés par la coopérative (Fbu)',
            ])
            ->add('institutionFinancementCreditOrg', null, [
                'label' => 'Institution qui a octroyé les crédits à la coopérative',
            ])
            ->add('infrastructures', null, [
                'attr' => ['class' => 'select2'],
            ])
            ->add('uniteDeTransformation', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Unité de transformation',
            ])
            ->add('materielRoulant', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Matériel roulant',
            ])
            ->add('materielDeBureau', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Matériel de bureau',
            ])
            ->add('equipementAgricole', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Équipement agricole',
            ])

            ->add('nbrPersonnelAdmin', null, [
                'label' => 'Nombre du personnel administratif et financier',
            ])
            ->add('nbrPersonnelTechnique', null, [
                'label' => 'Nombre du personnel technique',
            ])
            ->add('nbrPersonnelAppui', null, [
                'label' => 'Nombre du personnel d\'appui',
            ])

            ->add('besoinsFormation', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Formations',
            ])
            ->add('besoinsServicesAuxMembres', null, [
                'attr' => ['class' => 'select2'],
                'label' => 'Services',
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\SuiviOrganisations',
        ]);
    }
}
