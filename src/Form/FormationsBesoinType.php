<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\FormationsBesoin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class FormationsBesoinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('formation')
            ->add('priorite', ChoiceType::class, [
                'choices' => [
                    'Non encodé' => null,
                    'Pas choisi' => FormationsBesoin::PRIORITE_NO,
                    'Priorité 1' => FormationsBesoin::PRIORITE_1,
                    'Priorité 2' => FormationsBesoin::PRIORITE_2,
                    'Priorité 3' => FormationsBesoin::PRIORITE_3,
                    'Priorité 4' => FormationsBesoin::PRIORITE_4,
                ],
                'placeholder' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\FormationsBesoin',
        ]);
    }
}
