<?php

declare(strict_types=1);

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class RapportsCooperativesType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $nb_annees = $options['nb_annees_passees'];
        $builder
            ->add('Denomination', EntityType::class, [
                'label' => 'Coopératives',
                // query choices from this entity
                'class' => TblOrganisations::class,
                'query_builder' => static function (EntityRepository $er) {
                    return $er->getAllFilsOrganisation('fedead58-79ad-11e6-abc5-b8ca3a965972');
                },
                'choice_label' => 'Denomination',
                'placeholder' => 'Toutes les coopératives',
                'empty_data' => null,
                'required' => false, //pour éviter la validation
                // used to render a select box, check boxes or radios
                //'multiple' => true,
                //'expanded' => true,
            ])
            ->add('groupecoop', CheckboxType::class, [
                'label' => 'Groupé par coopérative?',
                'required' => false,
            ])
            ->add('groupegroup', CheckboxType::class, [
                'label' => 'Groupé par groupement?',
                'required' => false,
            ])
            ->add('exploitationGroupAnnee', CheckboxType::class, [
                'label' => 'Groupé par année d\'enquête?',
                'required' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('exploitationListAnnee', ChoiceType::class, [
                'label' => 'Année d\'enquête',
                'choices' => array_combine(range(date('Y') - $nb_annees, date('Y')), range(date('Y') - $nb_annees, date('Y'))),
                'expanded' => false,
                'attr' => ['class' => 'show-exploitation'],
            ])
            ->add('queryTheme', ChoiceType::class, [
                'label' => 'Theme de regroupement',
                'choices' => [
                    'Choisir rapport' => '',
                    'Répartition spatiale, nombre de coopérative par commune et provinces [02, 01]' => 'Coop01',
                    'Respect de la norme de tenue des instances : AG=2 [I5], CE=6 [I5], CS =4[I5]. Voir si : respect des 3, de 2, de 1, de 0.' => 'Coop02',
                    'Existence de statuts [I1], Niveau de reconnaissance [I2] ; existence de ROI [I3]' => 'Coop03',
                    'Existence de PV de réunion [II3], Rapport annuel d’activités [II5]' => 'Coop04',
                    'L’enregistrement des cotisations [III4], le classement des justificatifs [III8], l’existence de rapport financier annuel [III9]' => 'Coop05',
                    'Nombre de MUSO/coopérative' => 'Coop06',
                    'Pourcentage des membres ayant contracté un crédit [V1]/membre total membres ; montant total des crédits contractés [V1]' => 'Coop07',
                    'Part des prêts octroyés par la coopérative [V2] sur le montant total [V1]' => 'Coop08',
                    'Fréquence des institutions ayant octroyé le crédit, pour l’ensemble des coopératives [V2]' => 'Coop09',
                    'Fréquence des besoins en formation [VII1], en appui institutionnel [VII2], en service [VII3]. Successivement  pour les1er 2me et 3me choix.' => 'Coop10',
                ],
                'empty_data' => null,
                'placeholder' => false,
                'expanded' => false,
                'required' => false, //pour éviter la validation
                'attr' => ['class' => 'show-theme'],
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\RapportsCooperatives',
        ]);
        /* déclarer ici les options additionnelles passées au formulaire depuis le controleur */
        $resolver->setRequired('nb_annees_passees'); // Requires that nb_annees_passees be set by the caller.
        $resolver->setAllowedTypes('nb_annees_passees', 'integer'); // Validates the type(s) of option(s) passed.
    }
}
