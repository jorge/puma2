<?php

declare(strict_types=1);

namespace App\Form;

use App\Entity\ServicesBesoin;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class ServicesBesoinType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $exploitation_id = $options['data']->getExploitation()->getId();
        $builder
            ->add('service')
            ->add('priorite', ChoiceType::class, [
                'choices' => [
                    'Non encodé' => null,
                    'Pas choisi' => ServicesBesoin::PRIORITE_NO,
                    'Priorité 1' => ServicesBesoin::PRIORITE_1,
                    'Priorité 2' => ServicesBesoin::PRIORITE_2,
                    'Priorité 3' => ServicesBesoin::PRIORITE_3,
                    'Priorité 4' => ServicesBesoin::PRIORITE_4,
                ],
                'placeholder' => false,
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => 'App\Entity\ServicesBesoin',
        ]);
    }
}
