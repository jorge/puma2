<?php

declare(strict_types=1);

namespace App\Twig;

use Twig_Extension;
use Twig_SimpleFilter;

class AppExtension extends Twig_Extension
{
    public function formatNumMemberFilter($numMembre)
    {
        if ($numMembre) {
            $numeroMembre = sprintf('%09d', $numMembre); // rejouter les zeros

            return wordwrap($numeroMembre, 3, '-', true);
        }

        return 'Non défini';
    }

    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('format_num_membre', function ($numMembre) {
                return $this->formatNumMemberFilter($numMembre);
            }),
        ];
    }
}
