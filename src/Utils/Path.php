<?php

declare(strict_types=1);

namespace App\Utils;

class Path
{
    public static function from($dir, $filename)
    {
        $dir = realpath($dir);

        if ($dir) {
            return $dir . '/' . $filename;
        }   // $dir is False not a realpath

        return $dir;
    }

    /**
     * Get a the path of a temporary file.
     *
     * @param string $filename The filename
     *
     * @return string The path to the file
     */
    public static function getPath4TmpFile(string $filename)
    {
        if ('\\' !== $filename[0] && '/' !== $filename[0]) {
            $filename = '/' . $filename;
        }

        return self::replaceBackslash2Slash(sys_get_temp_dir() . $filename);
    }

    /**
     * (for windows) transform the backslash to slash for
     * a given path.
     */
    public static function replaceBackslash2Slash(string $path)
    {
        return str_replace('\\', '/', $path);
    }
}
