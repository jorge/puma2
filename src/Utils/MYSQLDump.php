<?php

declare(strict_types=1);

namespace App\Utils;

use Exception;

use function count;
use function is_int;
use function is_object;

use const PHP_VERSION;

/*
 * Database MySQLDump Class File
 * Copyright (c) 2009 by James Elliott
 * James.d.Elliott@gmail.com
 * GNU General Public License v3 http://www.gnu.org/licenses/gpl.html
 */
$version1 = '1.3.2';
// This Scripts Version.
class MYSQLDump
{
    public $conn;

    public $connected = false;

    public $droptableifexists = false;

    /**
     * @var mixed[]
     */
    public array $fields;

    public $mysql_error;

    public $output;

    public $tables = [];

    public function cleanValue($fieldValue)
    {
        $problems = [
            ';',
        ];
        $solutions = [
            '',
        ];

        return str_replace($problems, $solutions, $fieldValue);
    }

    /* ok */
    public function connect($host, $user, $pass, $db)
    {
        $return = true;
        $this->conn = mysqli_connect($host, $user, $pass, $db);

        if (!$this->conn) {
            $this->mysql_error = mysqli_error();
            $return = false;
        }
        $this->connected = $return;
        mysqli_query($this->conn, "SET timezone = '+00:00'");

        return $return;
    }

    /* ok */
    public function dump_table($tablename, $log_date)
    {
        $this->output = '';
        $this->list_values($tablename, $log_date);
        $this->list_update($tablename, $log_date);
    }

    public function dump_table_into_file($tablename, $log_date, $filename, $date_fin)
    {
        try {
            $arr = [
                'bln' => false,
                'nochanges' => false,
                'msg' => '',
            ];
            file_put_contents($filename, '');
            /**
             *  Pour activer la création mise à jour en une opération UPSERT
             * décommenter le 4 lignes suivantes et commenter la méthode traditionnelle.
             */
            //          $result = $this->list_upsert_into_file($tablename, $log_date, $filename, $date_fin);
            //          if (! $result['bln']) {
            //              throw new Exception($result['msg']);
            //          }
            /* FIN de UPSERT */
            /**
             * méthode traditionnelle.
             */
            $result = $this->list_values_into_file($tablename, $log_date, $filename, $date_fin);

            if ($result['bln']) {
                $result = $this->list_update_into_file($tablename, $log_date, $filename, $date_fin);

                if (!$result['bln']) {
                    throw new Exception($result['msg']);
                }
            } else {
                throw new Exception($result['msg']);
            }
            /* FIN de méthode traditionnelle  */

            if (0 === filesize($filename)) {
                unlink($filename);
                $arr['nochanges'] = true;
            }
            $arr['bln'] = true;
        } catch (Exception $e) {
            $arr['bln'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
        }

        return $arr;
    }

    /* ok */
    public function get_table_structure($tablename)
    {
        $this->output .= "\n\n-- Dumping structure for table: {$tablename}\n\n";

        if ($this->droptableifexists) {
            $this->output .= "DROP TABLE IF EXISTS `{$tablename}`;\nCREATE TABLE `{$tablename}` (\n";
        } else {
            $this->output .= "CREATE TABLE `{$tablename}` (\n";
        }
        $sql = mysqli_query($this->conn, "DESCRIBE {$tablename}");
        $this->fields = [];

        while ($row = mysqli_fetch_array($sql)) {
            $name = $row[0];
            $type = $row[1];
            $null = $row[2];

            if (empty($null)) {
                $null = 'NOT NULL';
            }
            $key = $row[3];

            if ('PRI' === $key) {
                $primary = $name;
            }
            $default = $row[4];
            $extra = $row[5];

            if ('' !== $extra) {
                $extra .= ' ';
            }
            $this->output .= "  `{$name}` {$type} {$null} {$extra},\n";
        }
        $this->output .= "  PRIMARY KEY  (`{$primary}`)\n);\n";
    }

    /* ok */
    public function list_tables()
    {
        //      global $champID;
        //      $return = true;
        //      if (! $this->connected) {
        //          $return = false;
        //      }
        //      $this->tables = array();
        //      $a = array_keys($champID);
        //      for ($i = 0; $i < count($a); $i ++) {
        //          array_push($this->tables, $a[$i]);
        //      }
        //      return $return;
        $return = true;

        if (!$this->connected) {
            $return = false;
        }
        $this->tables = [];
        $this->tables[] = 'animaux';

        return $return;
    }

    /* ok */
    public function list_update($tablename, $log_date)
    {
        if (!$sql = mysqli_query($this->conn, "SELECT * FROM {$tablename} WHERE date_maj>'{$log_date}'")) {
            return;
        }
        // $this->output .= "\n\n-- Dumping data for table: $tablename\n\n";
        while ($row = mysqli_fetch_array($sql)) {
            $broj_polja = count($row) / 2;
            $this->output .= "UPDATE `{$tablename}` SET ";
            $buffer = '';

            for ($i = 0; $i < $broj_polja; ++$i) {
                $champname = '`' . $this->mysqli_field_name($sql, $i) . '`=';
                $vrednost = $row[$i];

                if (!is_int($vrednost)) {
                    $vrednost = "'" . addslashes($vrednost) . "'";
                }
                $buffer .= "{$champname}" . $vrednost . ', ';
            }
            $buffer = mb_substr($buffer, 0, count($buffer) - 3);
            $buffer .= ' WHERE `' . $this->mysqli_field_name($sql, 0) . '`=' . $row[0];
            $this->output .= $buffer . ";\n";
        }
    }

    public function list_update_into_file($tablename, $log_date, $filename, $date_fin)
    {
        try {
            $arr = [
                'bln' => false,
                'msg' => '',
            ];

            if (6 < (int) PHP_VERSION) {
                $sql = mysqli_query($this->conn, "SELECT * FROM {$tablename} WHERE date_maj>'{$log_date}'");

                if (!$sql) {
                    $arr['bln'] = true;

                    return $arr;
                }
                $fp = fopen($filename, 'ab');

                while ($row = mysqli_fetch_array($sql)) {
                    $this->sqlUpdateRow($sql, $row, $tablename, $fp, $date_fin, true);
                }
                fclose($fp);
                $arr['bln'] = true;
            } else {
                $sql = mysqli_query($this->conn, "SELECT * FROM {$tablename} WHERE date_maj>'{$log_date}'");

                if (!$sql) {
                    $arr['bln'] = true;

                    return $arr;
                }
                $fp = fopen($filename, 'ab');

                while ($row = mysqli_fetch_array($sql)) {
                    $this->sqlUpdateRow($sql, $row, $tablename, $fp, $date_fin, false);
                    // $broj_polja = count($row) / 2;
                    // $sql_output = "UPDATE `$tablename` SET ";
                    // $buffer = '';
                    // for ($i = 0; $i < $broj_polja; $i ++) {
                    // $champname = "`" . $this->mysqli_field_name($sql, $i) . "`=";
                    // $vrednost = $row[$i];
                    // if (! is_integer($vrednost)) {
                    // $vrednost = "'" . addslashes($vrednost) . "'";
                    // }
                    // $buffer .= "$champname" . $vrednost . ', ';
                    // }
                    // $buffer = substr($buffer, 0, count($buffer) - 3);
                    // $buffer .= " WHERE `" . $this->mysqli_field_name($sql, 0) . "`="
                    // . $row[0];
                    // $sql_output .= $buffer . ";\n";
                    // fwrite($fp, $sql_output);
                }
                fclose($fp);
                $arr['bln'] = true;
            }
        } catch (Exception $e) {
            $arr['bln'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
        }

        return $arr;
    }

    public function list_upsert_into_file($tablename, $log_date, $filename, $date_fin)
    {
        try {
            $arr = [
                'bln' => false,
                'msg' => '',
            ];

            if (!$sql = mysqli_query($this->conn, "SELECT * FROM {$tablename} WHERE date_creation>'{$log_date}'")) {
                $arr['bln'] = true;

                return $arr;
            }
            $k = 0;
            $kmax = 500;
            $sql_output = '';
            $fp = fopen($filename, 'ab');

            while ($row = mysqli_fetch_array($sql)) {
                $ondupssql = $this->sqlUpsertRow($sql, $sql_output, $row, $k, $kmax, $tablename, $fp, $date_fin, true);
            }

            if (0 < $k) {
                $sql_output .= "\n " . rtrim($ondupssql, ',');
                $sql_output .= ";\n";
            }
            fwrite($fp, $sql_output);
            fclose($fp);
            $arr['bln'] = true;
        } catch (Exception $e) {
            $arr['bln'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
        }

        return $arr;
    }

    /* ok */
    public function list_values($tablename, $log_date)
    {
        if (!$sql = mysqli_query($this->conn, "SELECT * FROM {$tablename} WHERE date_creation>'{$log_date}'")) {
            return;
        }
        // $this->output .= "\n\n-- Dumping data for table: $tablename\n\n";
        $kmax = 500;
        $k = 0;

        while ($row = mysqli_fetch_array($sql)) {
            $broj_polja = count($row) / 2;

            if (0 === $k) {
                $this->output .= "INSERT IGNORE INTO `{$tablename}` VALUES (";
            } else {
                $this->output .= ",\n(";
            }

            $buffer = '';

            for ($i = 0; $i < $broj_polja; ++$i) {
                $vrednost = $row[$i];

                if (!is_int($vrednost)) {
                    $vrednost = "'" . addslashes($vrednost) . "'";
                }
                $buffer .= $vrednost . ', ';
            }
            $buffer = mb_substr($buffer, 0, count($buffer) - 3);
            $this->output .= $buffer . ')';
            ++$k;

            if ($k >= $kmax) {
                $this->output .= ";\n";
                $k = 0;
            }
        }

        if (0 < $k) {
            $this->output .= ";\n";
        }
    }

    public function list_values_into_file($tablename, $log_date, $filename, $date_fin)
    {
        try {
            $arr = [
                'bln' => false,
                'msg' => '',
            ];

            if (6 < (int) PHP_VERSION) {
                if (!$sql = mysqli_query($this->conn, "SELECT * FROM {$tablename} WHERE date_creation>'{$log_date}'")) {
                    $arr['bln'] = true;

                    return $arr;
                }
                $k = 0;
                $kmax = 500;
                $sql_output = '';
                $fp = fopen($filename, 'ab');

                while ($row = mysqli_fetch_array($sql)) {
                    $this->sqlInsertRow($sql, $sql_output, $row, $k, $kmax, $tablename, $fp, $date_fin, true);
                }

                if (0 < $k) {
                    $sql_output .= ";\n";
                }
                fwrite($fp, $sql_output);
                fclose($fp);
                $arr['bln'] = true;
            } else {
                if (!$sql = mysqli_query($this->conn, "SELECT * FROM {$tablename} WHERE date_creation>'{$log_date}'")) {
                    $arr['bln'] = true;

                    return $arr;
                }
                $k = 0;
                $kmax = 500;
                $sql_output = '';
                $fp = fopen($filename, 'ab');

                while ($row = mysqli_fetch_array($sql)) {
                    $this->sqlInsertRow($sql, $sql_output, $row, $k, $kmax, $tablename, $fp, $date_fin, false);
                    // $broj_polja = count($row) / 2;

                    // if ($k == 0) {
                    // $sql_output .= "INSERT IGNORE INTO `$tablename` VALUES
                    // (";
                    // }
                    // else {
                    // $sql_output .= ",\n(";
                    // }

                    // // $sql_output = "INSERT IGNORE INTO `$tablename`
                    // VALUES(";
                    // $buffer = '';
                    // for ($i = 0; $i < $broj_polja; $i ++) {
                    // $vrednost = $row[$i];
                    // if (! is_integer($vrednost)) {
                    // $vrednost = "'" . addslashes($vrednost) . "'";
                    // }
                    // $buffer .= $vrednost . ', ';
                    // }
                    // $buffer = substr($buffer, 0, count($buffer) - 3);
                    // $sql_output .= $buffer . ")";
                    // $k ++;
                    // if ($k >= $kmax) {
                    // $sql_output .= ";\n";
                    // fwrite($fp, $sql_output);
                    // $sql_output = '';
                    // $k = 0;
                    // }
                }

                if (0 < $k) {
                    $sql_output .= ";\n";
                }
                fwrite($fp, $sql_output);
                fclose($fp);
                $arr['bln'] = true;
            }
        } catch (Exception $e) {
            $arr['bln'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
        }

        return $arr;
    }

    /**
     * this does not exists in mysqli, so I created it
     * see http://stackoverflow.com/questions/14629636/mysql-field-name-to-the-new-mysqli.
     *
     * @param unknown $result
     * @param unknown $field_offset
     *
     * @return bool
     */
    public function mysqli_field_name($result, $field_offset)
    {
        $properties = mysqli_fetch_field_direct($result, $field_offset);

        return is_object($properties) ? $properties->name : false;
    }

    public function sqlInsertRow($sql, &$sql_output, &$row, &$k, &$kmax, &$tablename, &$fp, $date_fin, $blnmysqli = false)
    {
        // $date_fin = '2016-11-18 00:00:00';
        $broj_polja = count($row) / 2;

        if (0 === $k) {
            $sql_output .= "INSERT IGNORE INTO `{$tablename}` VALUES (";
        } else {
            $sql_output .= ",\n(";
        }
        $buffer = '';

        for ($i = 0; $i < $broj_polja; ++$i) {
            $vrednost = $this->cleanValue($row[$i]);

            if ($blnmysqli) {
                $finfo = $sql->fetch_field_direct($i);
                $fieldName = $finfo->name;
            } else {
                $fieldName = $this->mysqli_field_name($sql, $i);
            }

            switch ($fieldName) {
                case 'date_creation':
                case 'date_maj':
                    $vrednost = 'original' !== $date_fin ? "'" . $date_fin . "'" : "'" . addslashes($vrednost) . "'";

                    break;

                default:
                    if (!is_int($vrednost)) {
                        $vrednost = "'" . addslashes($vrednost) . "'";
                    }

                    break;
            }
            $buffer .= $vrednost . ', ';
        }
        $buffer = mb_substr($buffer, 0, count($buffer) - 3);
        $sql_output .= $buffer . ')';
        ++$k;

        if ($k >= $kmax) {
            $sql_output .= ";\n";
            fwrite($fp, $sql_output);
            $sql_output = '';
            $k = 0;
        }
    }

    public function sqlUpdateRow($sql, &$row, &$tablename, &$fp, $date_fin, $blnmysqli = false)
    {
        $broj_polja = count($row) / 2;
        $sql_output = "UPDATE `{$tablename}` SET ";
        $buffer = '';

        for ($i = 0; $i < $broj_polja; ++$i) {
            $vrednost = $this->cleanValue($row[$i]);

            if ($blnmysqli) {
                $finfo = $sql->fetch_field_direct($i);
                $fieldName = $finfo->name;
                $champname = '`' . $fieldName . '`=';
            } else {
                $fieldName = $this->mysqli_field_name($sql, $i);
                $champname = '`' . $fieldName . '`=';
            }

            switch ($fieldName) {
                case 'date_creation':
                case 'date_maj':
                    $vrednost = 'original' !== $date_fin ? "'" . $date_fin . "'" : "'" . addslashes($vrednost) . "'";

                    break;

                default:
                    if (!is_int($vrednost)) {
                        $vrednost = "'" . addslashes($vrednost) . "'";
                    }

                    break;
            }
            $buffer .= "{$champname}" . $vrednost . ', ';
        }
        $buffer = mb_substr($buffer, 0, count($buffer) - 3);

        if ($blnmysqli) {
            $finfoId = $sql->fetch_field_direct(0);
            $idName = $finfoId->name;
            $buffer .= ' WHERE `' . $idName . '`=' . $row[0];
        } else {
            $buffer .= ' WHERE `' . $this->mysqli_field_name($sql, 0) . '`=' . $row[0];
        }
        $sql_output .= $buffer . ";\n";
        fwrite($fp, $sql_output);
    }

    public function sqlUpsertRow($sql, &$sql_output, &$row, &$k, &$kmax, &$tablename, &$fp, $date_fin, $blnmysqli = false)
    {
        $broj_polja = count($row) / 2;

        if (0 === $k) {
            $sql_output .= "INSERT INTO `{$tablename}` VALUES (";
        } else {
            $sql_output .= ",\n(";
        }
        $buffer = '';
        $ondupssql = 'ON DUPLICATE KEY UPDATE ';

        for ($i = 0; $i < $broj_polja; ++$i) {
            $vrednost = $this->cleanValue($row[$i]);
            $finfo = $sql->fetch_field_direct($i);
            $fieldName = $finfo->name;

            switch ($fieldName) {
                case 'date_creation':
                case 'date_maj':
                    $vrednost = 'original' !== $date_fin ? "'" . $date_fin . "'" : "'" . addslashes($vrednost) . "'";
                    $ondupssql .= ' ' . $fieldName . ' = VALUES(' . $fieldName . '),';

                    break;

                default:
                    if (!is_int($vrednost)) {
                        $vrednost = "'" . addslashes($vrednost) . "'";
                    }
                    $ondupssql .= ' ' . $fieldName . ' = VALUES(' . $fieldName . '),';

                    break;
            }
            $buffer .= $vrednost . ', ';
        }
        $buffer = mb_substr($buffer, 0, count($buffer) - 3);
        $sql_output .= $buffer . ')';
        ++$k;

        if ($k >= $kmax) {
            $sql_output .= "\n " . rtrim($ondupssql, ',');
            $sql_output .= ";\n";
            fwrite($fp, $sql_output);
            $sql_output = '';
            $k = 0;
        }

        return $ondupssql;
    }
}
