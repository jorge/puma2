<?php

declare(strict_types=1);

namespace App\Utils;

use Exception;
use ZipArchive;

/**
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>
 */

/**
 * Pour gérer les archives zip.
 *
 * @author      johan Girardin
 *
 * @version     1.0
 */
class ZipFile
{
    /**
     * main object.
     */
    private $zip;

    public function __construct()
    {
        /**
         * création d'un objet 'zipfile'.
         */
        $this->zip = new ZipArchive();
    }

    public function __destruct()
    {
        $this->zip = null;
    }

    public function farr_addFileToArchive($pstrPathfile, $pstrNameFile, $pstrNameZip)
    {
        //  try{
        if ($this->zip->open($pstrPathfile . $pstrNameZip, ZipArchive::CREATE) === true) {
            $this->zip->addFile($pstrPathfile . $pstrNameFile, $pstrNameFile);
            $arrFayot = [
                'bln' => 1,
                'filePath' => $pstrPathfile,
                'fileName' => $pstrNameZip,
                'msg' => 'file :\'' . $pstrNameFile . '\' has been compressed.', ];
            //'msg' => gettext('file :\'') . $pstrNameFile . gettext('\' has been compressed.'));
            $this->zip->close();
        } else {
            $arrFayot = [
                'bln' => 0,
                'msg' => 'error creating zip file', ];
            //'msg' => gettext('error creating zip file'));
        }
        //      } catch (\Exception $e){
        //          $arrFayot['bln'] = false;
        //          $arrFayot['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
        //          //$arrFayot['msg'] = gettext('Erreur dans ') . __FUNCTION__ . ' :' . $e->getMessage();
        //      }
        return $arrFayot;
    }

    /**
     * Extracts a ZIP archive to the specified extract path.
     *
     * @param mixed $pfile
     * @param mixed $pextractPath
     *
     * @return bool TRUE if the ZIP archive is successfully extracted, FALSE if there was an error
     */
    public function fbln_extract($pfile, $pextractPath)
    {
        try {
            $arrFayot['bln'] = false;
            $arrFayot['msg'] = 'start';
            $res = $this->zip->open($pfile);

            if (true === $res) {
                $msg = $this->zip->extractTo($pextractPath);
                sleep(5);
                $this->zip->close();
                $arrFayot['msg'] = $msg;
                $arrFayot['bln'] = true;
            } else {
                $arrFayot['msg'] = $res;
                $arrFayot['bln'] = false;
            }
        } catch (Exception $e) {
            $arrFayot['bln'] = false;
            $arrFayot['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
        }

        return $arrFayot;
    }

    public function ffile_getArchive()
    {
        /**
         * production de l'archive' Zip.
         */
        return $this->zip->file();
    }
}
