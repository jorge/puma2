<?php

declare(strict_types=1);

namespace App\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
final class ConsultationControllerTest extends WebTestCase
{
    public function testCompleteScenario()
    {
        // Create a new client to browse the application
        $client = self::createClient();

        // Create a new entry in the database
        $crawler = $client->request('GET', '/consultation/');
        self::assertEquals(200, $client->getResponse()->getStatusCode(), 'Unexpected HTTP status code for GET /consultation/');
        $crawler = $client->click($crawler->selectLink('Create a new entry')->link());

        // Fill in the form and submit it
        $form = $crawler->selectButton('Create')->form([
            'appbundle_consultation[field_name]' => 'Test',
            // ... other fields to fill
        ]);

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check data in the show view
        self::assertGreaterThan(0, $crawler->filter('td:contains("Test")')->count(), 'Missing element td:contains("Test")');

        // Edit the entity
        $crawler = $client->click($crawler->selectLink('Edit')->link());

        $form = $crawler->selectButton('Update')->form([
            'appbundle_consultation[field_name]' => 'Foo',
            // ... other fields to fill
        ]);

        $client->submit($form);
        $crawler = $client->followRedirect();

        // Check the element contains an attribute with value equals "Foo"
        self::assertGreaterThan(0, $crawler->filter('[value="Foo"]')->count(), 'Missing element [value="Foo"]');

        // Delete the entity
        $client->submit($crawler->selectButton('Delete')->form());
        $crawler = $client->followRedirect();

        // Check the entity has been delete on the list
        self::assertNotRegExp('/Foo/', $client->getResponse()->getContent());
    }
}
