<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * Verification pour voir si un seul chef de famille par exploitation.
 */
class UniqueChefFamille extends Constraint
{
    public $message = "Il ne peut avoir qu'un chef famille par exploitation !";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return UniqueChefFamilleValidator::class;
    }
}
