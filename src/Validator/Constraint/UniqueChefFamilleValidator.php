<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use App\Entity\Personnes;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueChefFamilleValidator extends ConstraintValidator
{
    /**
     * @param mixed $personne
     *
     * @throws Exception
     */
    public function validate($personne, Constraint $constraint)
    {
        if ($personne instanceof Personnes) {
            if ($personne->isChefFamille()) { // verification seulement si
                // la personne de dit comme repondant

                $exploitation = $personne->getExploitation();
                $coExploitants = $exploitation->getPersonnes();

                $autreCoExploitantEstChefFamille = null; // variable qui devient
                // qui va avoir la personne qui est co-exploitant et aussi chef famille

                foreach ($coExploitants as $coExploitant) {
                    if ($coExploitant !== $personne && $coExploitant->isChefFamille()) {
                        $autreCoExploitantEstChefFamille = $coExploitant;
                    }
                }

                if ($autreCoExploitantEstChefFamille) {
                    $this->context->buildViolation('Il existe déjà un co-exploitant Chef de famille renseigné pour cette exploitation. Veuillez modifier le statut de ce co-exploitant pour modifier le rol de famille')
                        ->setParameter('{{ rolFamille }}', $autreCoExploitantEstChefFamille)
                        ->atPath('rolFamille')
                        ->addViolation();
                }
            }
        } else {
            throw new Exception("La variable n\\'est pas une instance de Personnes", 1);
        }
    }
}
