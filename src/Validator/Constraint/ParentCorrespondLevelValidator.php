<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use App\Entity\TblDecoupageAdmin;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class ParentCorrespondLevelValidator extends ConstraintValidator
{
    /**
     * @param mixed $localisationAdmin
     *
     * @throws Exception
     */
    public function validate($localisationAdmin, Constraint $constraint)
    {
        if ($localisationAdmin instanceof TblDecoupageAdmin) {
            if ($localisationAdmin->getLevel()) { // verification seulement
                //le level de localisationAdmin
                $enfantLevel = $localisationAdmin->getLevel();
                $levEnfant = $enfantLevel->getLevel();
                $parent = $localisationAdmin->getParent();
                $parentLevel = $parent->getLevel();
                $levParent = $parentLevel->getLevel();

                if ($levParent + 1 !== $levEnfant) {
                    $this->context->buildViolation("Le niveau du parent doit être le niveau inmediatement superieur au niveau de l'entité que vous essayez de créer")
                        ->setParameter('{{ level }}', $enfantLevel->getDenomination())
                        ->atPath('level')
                        ->addViolation();
                }
            }
        } else {
            throw new Exception("La variable n\\'est pas une instance de TblDecoupageAdmin", 1);
        }
    }
}
