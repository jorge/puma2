<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * Verification pour voir si le champ a seulement des caracteres abecedaire et spaces.
 */
class VerifOnlyAbc extends Constraint
{
    public $message = 'Le champ accepte seulement de caracteres abecedaire et spaces !';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return VerifOnlyAbcValidator::class;
    }
}
