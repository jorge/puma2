<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use App\Entity\Personnes;
use App\Entity\Saisies\Socies;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class VerifOnlyAbcValidator extends ConstraintValidator
{
    /**
     * @param mixed $socies
     *
     * @throws Exception
     */
    public function validate($socies, Constraint $constraint)
    {
        if ($socies instanceof Personnes || $socies instanceof Socies) {
            if ($this->verificationAbc($socies->getPrenom()) === 0) {
                $this->context->buildViolation("Pour le champ 'prenom' seulement les lettres et les spaces sont acceptés")
                    ->atPath('prenom')
                    ->addViolation();
            } elseif ($this->verificationAbc($socies->getNom()) === 0) {
                $this->context->buildViolation("Pour le champ 'nom' seulement les lettres et les spaces sont acceptés")
                    ->atPath('modeCulture')
                    ->addViolation();
            }
        }
    }

    public function verificationAbc($str5)
    {
        return preg_match('/^[a-zA-Z\s]+$/', $str5);
    }
}
