<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use App\Entity\ParcelleHistoriques;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

use function count;

class VerifCulturePureAssocieValidator extends ConstraintValidator
{
    /**
     * @param mixed $parcelleHistorique
     *
     * @throws Exception
     */
    public function validate($parcelleHistorique, Constraint $constraint)
    {
        if ($parcelleHistorique instanceof ParcelleHistoriques) {
            if ($parcelleHistorique->getModeCulture() === 'pure') {
                $cultures = $parcelleHistorique->getCultures();
                $cpt = count($cultures);

                if (1 < $cpt || 0 === $cpt) {
                    $this->context->buildViolation("Pour le mode culture 'Pure' on peut choisir une seule culture, veulliez modifier le mode culture ou la quantité de cultures selectionnés")
                        ->atPath('modeCulture')
                        ->addViolation();
                }
            } elseif ($parcelleHistorique->getModeCulture() === 'associée') {
                $cultures = $parcelleHistorique->getCultures();
                $cpt = count($cultures);

                if (2 > $cpt) {
                    $this->context->buildViolation("Pour le mode culture 'Associé' on doit choisir plus d'une seule culture, veulliez modifier le mode culture ou la quantité de cultures selectionnés")
                        ->atPath('modeCulture')
                        ->addViolation();
                }
            }
        } else {
            throw new Exception("La variable n\\'est pas une instance de parcelleHistoriques", 1);
        }
    }
}
