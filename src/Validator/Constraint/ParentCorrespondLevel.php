<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * Verification pour voir si un seul chef de famille par exploitation.
 */
class ParentCorrespondLevel extends Constraint
{
    public $message = "Le parent doit correspondre au nivel inmediatement superieur de l'entité que vous étez en train d créer !";

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return ParentCorrespondLevelValidator::class;
    }
}
