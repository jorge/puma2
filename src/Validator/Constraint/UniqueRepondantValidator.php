<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use App\Entity\Personnes;
use Exception;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

class UniqueRepondantValidator extends ConstraintValidator
{
    /**
     * @param mixed $personne
     *
     * @throws Exception
     */
    public function validate($personne, Constraint $constraint)
    {
        if ($personne instanceof Personnes) {
            if ($personne->isRepondant()) { // verification seulement si
                // la personne de dit comme repondant

                $exploitation = $personne->getExploitation();
                $coExploitants = $exploitation->getPersonnes();

                $autreCoExploitantEstRepondant = null; // variable qui devient
                // qui va avoir la personne qui est co-exploitant et aussi repondant

                foreach ($coExploitants as $coExploitant) {
                    if ($coExploitant !== $personne && $coExploitant->isRepondant()) {
                        $autreCoExploitantEstRepondant = $coExploitant;
                    }
                }

                if ($autreCoExploitantEstRepondant) {
                    $this->context->buildViolation('Il existe déjà un co-exploitant répondant renseigné pour cette exploitation. Veuillez modifier le statut de ce co-exploitant pour modifier le répondant')
                        ->setParameter('{{ repondant }}', (string) ($autreCoExploitantEstRepondant))
                        ->atPath('repondant')
                        ->addViolation();
                }
            }
        } else {
            throw new Exception("La variable n\\'est pas une instance de Personnes", 1);
        }
    }
}
