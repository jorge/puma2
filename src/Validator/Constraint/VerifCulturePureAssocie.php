<?php

declare(strict_types=1);

namespace App\Validator\Constraint;

use Symfony\Component\Validator\Constraint;

/**
 * @Annotation
 *
 * Verification pour voir si le mode culture est d'accord avec la quantité de cultures choisis.
 */
class VerifCulturePureAssocie extends Constraint
{
    public $message = 'Il doit correspondre le mode culture avec la quantités de cultures selectionnées !';

    public function getTargets()
    {
        return self::CLASS_CONSTRAINT;
    }

    public function validatedBy()
    {
        return VerifCulturePureAssocieValidator::class;
    }
}
