<?php

declare(strict_types=1);

namespace App\Synchro;

use Exception;

use function count;

use const FILE_APPEND;

class SynchroLogger
{
    public string $synchro_log_file;

    public string $synchro_path;

    public function __construct(string $synchro_path)
    {
        $this->synchro_path = $synchro_path;
    }

    public function getLastXLogLines($linesNbr = 20)
    {
        if (file_exists($this->synchro_log_file)) {
            $ret = '';
            $file = file($this->synchro_log_file);
            $fileCount = count($file);

            for ($i = max(0, count($file) - $linesNbr); $fileCount > $i; ++$i) {
                $ret .= $file[$i] . '<br>';
            }
        } else {
            $ret = 'No synchro file';
        }

        return $ret;
    }

    public function getSynchroLogFileName()
    {
        return $this->synchro_log_file;
    }

    public function setSession($session)
    {
        $this->synchro_log_file = $this->synchro_path . 'session_' . $session->getId() . '.log';
        $this->write('-- setSession --');
    }

    public function write($msg)
    {
        /**
         * Write a message in the logs.
         *
         * @var string $msg The message to wrote
         */
        if ('' !== $this->synchro_log_file) {
            file_put_contents($this->synchro_log_file, date('Y-m-d H:i:s') . ' => ' . $msg . "\n", FILE_APPEND);
        } else {
            throw new Exception('Synchro session not given', 1);
        }
    }
}
