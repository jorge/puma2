<?php

declare(strict_types=1);

namespace App\Synchro;

use App\Utils\SFTP;
use Exception;

use function count;

class SynchroFileSender extends SynchroCodeCommun
{
    private $synchroLogger;

    public function __construct($parameterBag)
    {
        SynchroCodeCommun::__construct($parameterBag);
    }

    /*
     * Pour local va chercher les fichiers (  .txt .zip .md5  ) sur ftp master
     *
     * Normalement utilisé que par local
     */
    // TODO a renommer en getSynchroFiles
    // old : public function _download_archive_and_statusfile($local_path, $local_file, $remote_file) {
    public function _download_archive_and_statusfile($session)
    {
        /**
         * Dowload the archive et le status file (XXX_status.txt).
         *
         * @var string $local_path Chemin où sera stocké les fichiers téléchargés
         * @var string $local_file Nom du statusfile en local
         * @var string $remote_file Nom du statusfile à l'extérieur
         *
         * @return Array[succes: boolean, msg: String, dossier: String] où
         * - succes : vrai si tout s'est bien passé
         * - msg : message
         * - dossier : où ça été téléchargé (sur serveur)
         */
        $local_path = $this->getParameter('synchro_path');
        $local_file = $session->getMasterSessionFileName(); // on enregistre en temps que session_id_master.txt
        $remote_file = $session->getMasterSessionFileName();

        try {
            $arr = [
                'succes' => false, //devient vrai si donne mise dedans
                'msg' => '',
                //'dossier' => $this->getParameter('')
            ];

            $ftp = new SFTP($this->getParameter('ftphost'), $this->getParameter('ftpuser'), $this->getParameter('ftppwd'));
            $ftp->passive = true;
            // connect to server
            if ($ftp->connect()) {
                // download file from ftp server
                $fileToDownload = $this->getParameter('ftpfolder') . $remote_file;

                if ($ftp->get($fileToDownload, $local_path . $local_file, FTP_BINARY)) {
                    // read the name of the related archive

                    $maitre_synchro = $session->getMasterZipname();
                    $archive_name = $session->getMasterZipname();

                    if ('' !== $archive_name) {
                        if ($ftp->get($this->getParameter('ftpfolder') . $archive_name, $local_path . $archive_name, FTP_BINARY)) {
                            $arr['msg'] .= 'Archive t&eacute;l&eacute;charg&eacute; ';
                        } else {
                            throw new Exception($ftp->error);
                        }
                        $arr['msg'] .= '+ Fichier t&eacute;l&eacute;charg&eacute;';
                    } else {
                        $arr['msg'] .= '+ Pas de nouvelles donn&eacute;es en provenance du serveur - A JOUR';
                    }
                } else {
                    throw new Exception($ftp->error);
                }
            } else {
                $ftp->close();

                throw new Exception($ftp->error);
            }
            $ftp->close();
            $arr['succes'] = true;
        } catch (Exception $e) {
            $arr['succes'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
            $arr['dossier'] = '';
        }

        return $arr;
    }

    // OLD : public function _upload_archive($archive_path, $archive_name) {
    public function _upload_archive($session)
    {
        /**
         * Upload une archine qui se trouver dans le coin de l'appart.
         *
         *  @var string $archive_path Chemin où se trouve le fichier
         *  @var string $archive_name Le nom du fichier
         */
        $archive_path = $this->getParameter('synchro_path');
        $archive_name = $session->getZipName();
        $session_file_path = $this->getParameter('synchro_path');
        $session_file_name = $session->getSessionFileName();

        $this->synchroLogger->write('synchro_path: ' . $session_file_path);

        try {
            $arr = [
                'succes' => false,
                'msg' => '',
                'dossier' => $this->getParameter('ftpfolder'), //  "httpdocs/adcapad/synchro/MARCUS/"
            ];

            $synchro_role = $this->getParameter('synchro_role'); // TOUJOURS EN LOCAL

            if (SynchroCodeCommun::ROLE_LOCAL !== $synchro_role) {
                throw new Exception('Ne peut etre lancé que par local', 1);
            }

            $ftp = new SFTP($this->getParameter('ftphost'), $this->getParameter('ftpuser'), $this->getParameter('ftppwd'));
            $ftp->passive = true;

            // connect to server
            if ($ftp->connect()) {
                // Vérifier que le dossier existe
                $result = $ftp->cd($arr['dossier']);

                if (count($result) === 0) {
                    // Créer le directory
                    $result = $ftp->mkdir($arr['dossier']);

                    if (!$result) {
                        $this->synchroLogger->write('ftperror: impossible de créer le dossier ' . $arr['dossier']);

                        throw new Exception('ftperror: impossible de créer le dossier ' . $arr['dossier']);
                    }
                }
                // upload archive to ftp server

                $remotearchive = $arr['dossier'] . $archive_name;

                if ($ftp->put($archive_path . $archive_name, $remotearchive, FTP_BINARY)) {
                    // upload le synchro_status
                    $ftp->put($session_file_path . $session_file_name, $arr['dossier'] . $session_file_name, FTP_ASCII);
                    $arr['msg'] = 'Archive chargée dans dossier ftp';
                } else {
                    $this->synchroLogger->write('ftperreur de chargement: ' . $ftp->error);

                    throw new Exception('ftperreur de chargement: ' . $ftp->error);
                }
            } else {
                $this->synchroLogger->write('ftperreur: impossible de se connecter au site');
                $ftp->close();

                throw new Exception('ftperreur: impossible de se connecter au site');
            }
            $ftp->close();
            $arr['succes'] = true;
        } catch (Exception $e) {
            $arr['succes'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
            $arr['dossier'] = '';
        }

        return $arr;
    }

    /**
     * Nom du fichier absolu qui se trouve dans le synchro_path.
     *
     * @param mixed $filename
     */
    public function absoluteSynchroPath($filename)
    {
        return $this->getParameter('synchro_path') . $filename;
    }

    /*
     * Pour local envoie les fichiers (  .txt .zip .md5  ) sur ftp master
     *
     * Normalement utilisé que par local
     */
    public function sendSynchroFiles($session)
    {
    }

    public function setLogger($synchroLogger)
    {
        $this->synchroLogger = $synchroLogger;
    }
}
