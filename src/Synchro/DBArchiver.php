<?php

declare(strict_types=1);

namespace App\Synchro;

use App\Utils\ZipFile;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use mysqli;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

use function count;

use const FILE_APPEND;

/**
 * Service qui gère la transformation des données de la db dans un fichier zip
 * (et inversément).
 */
class DBArchiver extends SynchroCodeCommun
{
    private $em; // besoin dans DBArchiver -> mettre dans DBArchiver

    private SynchroLogger $synchroLogger;

    private string $synchroPath;

    public function __construct(ParameterBagInterface $parameterBag, EntityManagerInterface $em)
    {
        parent::__construct($parameterBag);
        $this->em = $em;
        $this->synchroPath = $this->getParameter('synchro_path');
    }

    /**
     * Cree une archive de la db pour la session.
     *
     * retourne true si Ok (sinon false)
     * retourne :
     *
     * $arr['succes'] = false;
     * $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
     * $arr['dossie
     *
     * @param mixed $synchroSession
     */
    public function _creation_archive($synchroSession)
    {
        /// ATTENTOON FOR LIER A LOCAL ALORS QUE PEUT AUSSI ETRE DEMANDER par
        // MASTEr
        try {
            $this->synchroLogger->write("initiation de l'application pour prendre les données");
            $this->synchroLogger->write('on cree le fichier zip');

            $archive_result = $this->_prepare_archive($synchroSession);

            if ('A JOUR' === $synchroSession->getStatus()) {
                throw new Exception('A GERER', 1);
                /**
                 * Inutile de continuer, il n'y a pas de changements à effectuer.
                 */

                return $archive_result;
            }

            if (!$archive_result['succes']) {
                $this->synchroLogger->write("Problemes dans la construction de l'archive zip");

                throw new Exception("Problemes dans la construction de l'archive zip");
            }
            $this->synchroLogger->write("sauvegarder l'empreinte md5 de l'archive: " . $archive_result['archive_name']);

            $zipmd5 = md5_file($this->synchroPath . $archive_result['archive_name']);

            /* TODO A METTRE DANS LE CONTROLLER  */
            if ($this->getParameter('synchro_role') === self::ROLE_MASTER) {
                $synchroSession->setMasterZipmd5($zipmd5);
            } else {
                $synchroSession->setZipmd5($zipmd5);
            }

            $this->synchroLogger->write('fichier: ' . $this->synchroPath . $archive_result['archive_name'] . "; md5={$zipmd5}");

            return $archive_result;
        } catch (Exception $e) {
            $arr['succes'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
            $arr['dossier'] = '';
        }

        return $arr;
    }

    /**
     * TODO.
     *
     * @param mixed $pfile
     * @param mixed $pextractPath
     */
    public function _extract_archive($pfile, $pextractPath)
    {
        /**
         * Extrat () the archive ().
         */
        /**
         * $pfile = fullfilename.
         */
        $myzipfile = new ZipFile();
        $result = $myzipfile->fbln_extract($pfile, $pextractPath);
        $result['succes'] = $result['bln'];

        return $result;
    }

    /**
     * Get the md5 of a file (absolute path).
     *
     * @param mixed $absolutePathArchive
     */
    public function _get_md5_file($absolutePathArchive)
    {
        return md5_file($absolutePathArchive);
    }

    /** Fichier ou on sauvegarder les sql (le zip ouvert)
     *ou alors se fait en fichie .sql.
     *
     * @param mixed $session
     */
    public function getMasterSqlRepositoryPath($session)
    {
        return $this->getParameter('synchro_path') . $session->getId() . '_master/';
    }

    /**
     * Fichier ou on sauvegarder les sql (le zip ouvert)
     * ou alors se fait en fichie .sql.
     *
     * @param mixed $session
     */
    public function getSqlRepositoryPath($session)
    {
        return $this->getParameter('synchro_path') . $session->getId() . '/';
    }

    /**
     * Charge en db l'archive.
     *
     * retourn True si Ok (false sinon)
     *
     * @param mixed $session
     */
    public function loadArchive($session)
    {
    }

    /**
     * TODO.
     *
     * @param mixed $sqlPath
     * @param mixed $tableNameFile
     */
    // charger les fichiers d'une archive ouverte :
    // $sqlPath -> le repertoire ou se trouve les fichiers sql à exécuter
    // $tableNameFile -> nom des tables pour lesquelles on synchronise
    public function loadSqlFct($sqlPath, $tableNameFile)
    {
        /** LIT LES DONNES DE MASTER ET MET DANS DB */
        //$this->synchroLogger->write("loadSqlFct");
        //$this->synchroLogger->write($sqlPath);
        //$this->synchroLogger->write($tableNameFile);

        $arr = [
            'succes' => false,
            'msg' => '',
        ];
        $farde = '';
        /**
         * Charger le fichier sql avec les noms des tables à traîter
         * il doit se trouver dans l'archive zip.
         */
        $sql = file($sqlPath . 'synchro.sql');

        foreach ($sql as $l) {
            if (mb_substr(trim($l), 0, 2) !== '--') {
                // suppression des commentaires
                $farde .= $l;
            }
        }
        $fiches = explode(';', $farde); // on sépare les fichiers

        try {
            $db_host = $this->getParameter('database_host') ?: 'localhost';
            $db_user = $this->getParameter('database_user') ?: 'root';
            $db_pwd = $this->getParameter('database_password');
            $db_name = $this->getParameter('database_name') ?: 'puma_03';
            $db_port = (int) ($this->getParameter('database_port') ?: 3306);

            $mysqli = new mysqli($db_host, $db_user, $db_pwd, $db_name, $db_port);
        } catch (Exception $e) { // pris dans mysqli_connect_errno
            $this->synchroLogger->write('Exception for mysqli connection: ' . $e->getMessage());
        }

        if (mysqli_connect_errno() !== 0) {
            $this->synchroLogger->write('pbm connection');
            $arr['succes'] = false;
            $arr['msg'] = 'Erreur de connection : ' . mysqli_connect_error();
        } else {
            $arr['succes'] = true;

            foreach ($fiches as $fiche) { // et on les éxécute
                if ('' !== $fiche) { // EVITER ""
                    $sqlfullfilename = realpath($sqlPath) . '/' . $fiche;
                    $sql = file_get_contents($sqlfullfilename);

                    if (!$mysqli->multi_query($sql)) {
                        $arr['succes'] = false;
                        $arr['msg'] = 'Erreur SQL pour ' . $fiche . ' ' . $mysqli->error;

                        break;
                    }

                    // Vider le retour de la requete
                    while ($mysqli->more_results()) {
                        $mysqli->next_result();

                        if ($res = $mysqli->store_result()) {
                            $res->free();
                        }
                    }
                }
            }
            $mysqli->close();
        }

        return $arr;
    }

    public function setLogger(SynchroLogger $synchroLogger)
    {
        $this->synchroLogger = $synchroLogger;
    }

    /**
     * TODO.
     *
     * @param mixed $fieldValue
     */
    private function _cleanValue($fieldValue)
    {
        /**
         * For a given string, transforme all the ";" en "".
         *
         * @var string $fieldValue the value of an element
         */
        $problems = [
            ';',
        ];
        $solutions = [
            '',
        ];

        return str_replace($problems, $solutions, $fieldValue);
    }

    /* construction du fichier zip */
    /*
     * parametres: $maj: la date de reference, $zipfileName: nom du fichier zip
     * a créer
     */
    private function _prepare_archive($synchroSession)
    {
        try {
            if ($this->getParameter('synchro_role') === self::ROLE_MASTER) {
                $dataDateDebut = $synchroSession->getMasterDateDebutData(); // DEBUT DES DONNES A CHARGER (peut etre null si première synchro)
                $dataDateFin = $synchroSession->getMasterDateFinData(); // DEBUT DES DONNES A CHARGER
                $zipfileName = $synchroSession->getMasterZipname();
            } else {
                $dataDateDebut = $synchroSession->getDateDebutData(); // DEBUT DES DONNES A CHARGER (peut etre null si première synchro)
                $dataDateFin = $synchroSession->getDateFinData(); // DEBUT DES DONNES A CHARGER
                $zipfileName = $synchroSession->getZipName();
            }

            $arr = [
                'succes' => false,
                'msg' => '',
                'count_tables' => 0,
                'archive_path' => $this->synchroPath,
                'archive_name' => $zipfileName, //sprintf('%s_%s.zip', $synchroSession->getId(), $zipfileName)
            ];

            $listTableFilename = 'synchro.sql';
            $myzipfile = new ZipFile();
            $listDumpedTables = $this->synchroPath . $listTableFilename;
            file_put_contents($listDumpedTables, '');
            //récupérer la liste des tables à dumper dans la configuration
            $listDumpableEntities = $this->getParameter('synchro_entities');

            //Pour chaque table, effectuer un dump de son contenu sous forme de fichier d'instructions sql
            $j = 0;

            foreach ($listDumpableEntities as $tablename) {
                $tmp_file_table = $this->synchroPath . $tablename . '.sql';
                $file_result = $this->dumpEntityIntoFile($tablename, $tmp_file_table, $dataDateDebut, $dataDateFin);

                if (!$file_result['succes']) {
                    $this->synchroLogger->write('erreur ' . $tablename);

                    throw new Exception($file_result['msg']);
                }

                if (!$file_result['nochanges']) {
                    ++$j;
                    file_put_contents($listDumpedTables, $tablename . '.sql;', FILE_APPEND);
                    $result = $myzipfile->farr_addFileToArchive($this->synchroPath, $tablename . '.sql', $arr['archive_name']);
                }
            }
            /*
            if ($j == 0){
                throw new \Exception("A GERER", 1);
                // TODO ERREUR ICI A MON AVIS PAS GERER
                $synchroSession->setStatus('A JOUR'); // ATTENTION A JOUR DU COTE  TODO VERIFIER SI PAS DE PBM
            } else {
             */
            $result = $myzipfile->farr_addFileToArchive($this->synchroPath, $listTableFilename, $arr['archive_name']);
            //}

            $this->synchroLogger->write('le dump des tables a réussi');
            $arr['succes'] = true;
        } catch (Exception $e) {
            // détruire l'archive partielle crée
            $this->synchroLogger->write('Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage());

            if (file_exists($this->synchroPath . $arr['archive_name'])) {
                $this->synchroLogger->write('le dump a echoué, on efface le fichier zip');
                unlink($this->synchroPath . $arr['archive_name']);
            }
            $arr['succes'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
            $arr['count_tables'] = 0;
        }

        $this->deleteSqlFiles();

        return $arr;
    }

    private function deleteSqlFiles()
    {
        /**
         * Delete all the sql files.
         */
        $files = glob($this->synchroPath . '*.sql'); // get all sql file names

        foreach ($files as $file) { // iterate files
            if (is_file($file)) {
                unlink($file); // delete file
            }
        }
    }

    /**
     * Dump le contenu d'une table dans un fichier.
     *
     * @var string TODO
     * @var string TODO
     * @var DateTime date à partir de laquelle sont extraites les données (pour la 1ere synchro est nulle) (>)
     * @var DateTime date jusqu'à laqeulle sont extraites les données (<=)
     *
     * Les données prises sont celles qui ont un udate > $dateDebut et udate <= $date
     *
     * @param mixed $tablename
     * @param mixed $filename
     * @param mixed $dateDebut
     * @param mixed $dateFin
     */
    private function dumpEntityIntoFile($tablename, $filename, $dateDebut, $dateFin)
    {
        try {
            $arr = [
                'succes' => false,
                'nochanges' => false,
                'msg' => '',
            ];

            if ($dateDebut) { // TODO retirer $dateDebut
                /// TODO  ok d'utiliser UDATE (veririfer que mis a jour quand insertion des donnes )
                $mysql = sprintf('SELECT *
                    FROM %s
                    WHERE udate > :start_date
                    AND udate <= :stop_date', $tablename);
            } else {
                $mysql = sprintf('SELECT *
                    FROM %s
                    WHERE udate <= :stop_date', $tablename);
            }

            $connection = $this->em->getConnection();

            $statement = $connection->prepare($mysql);
            $statement->bindValue('stop_date', date_format($dateFin, 'Y-m-d H:i:s'));

            if ($dateDebut) {
                $statement->bindValue('start_date', date_format($dateDebut, 'Y-m-d H:i:s'));
            }

            $statement->execute();
            $records = $statement->fetchAll();

            file_put_contents($filename, '');
            $k = 0;
            $kmax = 500;
            $sql_output = '';
            $fp = fopen($filename, 'ab');
            $ondupssql = null;

            foreach ($records as $rec) {
                $this->sqlUpsertRow($rec, $sql_output, $k, $kmax, $tablename, $fp, $ondupssql);
            }

            if (0 < $k) {
                $sql_output .= "\n " . $ondupssql;
                $sql_output .= ";\n";
            }
            fwrite($fp, $sql_output);
            fclose($fp);

            if (0 === filesize($filename)) {
                unlink($filename);
                $arr['nochanges'] = true;
            }
            $arr['succes'] = true;
        } catch (Exception $e) {
            $arr['succes'] = false;
            $arr['msg'] = 'Erreur dans ' . __FUNCTION__ . ' :' . $e->getMessage();
        }

        return $arr;
    }

    private function sqlUpsertRow($record, &$sql_output, &$k, &$kmax, &$tablename, &$fp, &$ondupssql)
    {
        /**
         * Ecrit dans un fichier
         * Upsert (Update or Insert) a line into the db : The UPSERT command
         * inserts rows that don’t exist and updates the rows that do exist.
         *
         * @var ? $record TODO //ligne dans la db
         * @var ? &$sql_output String dans laquelle on écrit du sql (qui va être mis dans un fichier)
         * @var ? &$k Compteur
         * @var ? $kmax Max pour compteur
         * @var ? &$tablename Nom de la table
         * @var ? &$fp Fichier dans lequel on écrit
         * @var le fin de la requete (pour etre upsert)
         */
        if (0 === $k) {
            $sql_output .= "INSERT INTO `{$tablename}` (";
            $ondupssql = 'ON DUPLICATE KEY UPDATE ';

            $it = 0;

            foreach (array_keys($record) as $fieldName) {
                if ('udate' !== $fieldName) {
                    if (0 === $it) {
                        $sql_output .= $fieldName;
                        $ondupssql .= $fieldName . ' = VALUES(' . $fieldName . ')';
                    } else {
                        $sql_output .= ', ' . $fieldName;
                        $ondupssql .= ', ' . $fieldName . ' = VALUES(' . $fieldName . ')';
                    }

                    ++$it;
                }
            }

            $sql_output .= ') VALUES (';
        } else {
            $sql_output .= ",\n(";
        }
        $buffer = '';

        foreach ($record as $fieldName => $fieldValue) {
            if ('udate' !== $fieldName) {
                $vrednost = 'fos_user' === $tablename && 'roles' === $fieldName ? $fieldValue : $this->_cleanValue($fieldValue);

                if ('' === $vrednost) {
                    if ('utilisateur' === $fieldName) {
                        $vrednost = "'blop'";
                    } elseif ('age_reforme_reprod' === $fieldName && 'animaux' === $tablename) {
                        $vrednost = "'blopblop'";
                    } else {
                        $vrednost = 'null';
                    }
                } else {
                    $vrednost = "'" . addslashes($vrednost) . "'";
                }

                $buffer .= $vrednost . ', ';
            }
        }

        $buffer = mb_substr($buffer, 0, count($buffer) - 3);
        $sql_output .= $buffer . ')';
        ++$k;

        if ($k >= $kmax) {
            $sql_output .= "\n " . $ondupssql;
            $sql_output .= ";\n";
            fwrite($fp, $sql_output);
            $sql_output = '';
            $k = 0;
        }
    }
}
