<?php

declare(strict_types=1);

namespace App\Synchro;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

/**
 * Code partagé entre toutes les classes de synchro.
 */
class SynchroCodeCommun
{
    public const ROLE_LOCAL = 'local';

    public const ROLE_MASTER = 'master';

    private ParameterBagInterface $parameterBag;

    private $synchroRole;

    public function __construct(ParameterBagInterface $parameterBag)
    {
        $this->parameterBag = $parameterBag;
        $this->synchroRole = $parameterBag->get('synchro_role');
    }

    /**
     * Get parameter from ParameterBag.
     *
     * @param string $name
     *
     * @return mixed
     */
    public function getParameter($name)
    {
        return $this->parameterBag->get($name);
    }

    /**
     * Set ParameterBag for repository.
     */
    public function setParameterBag(ParameterBagInterface $params)
    {
        $this->parameterBag = $params;
    }
}
