<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use App\Entity\Traits\RadieField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

use function count;

/**
 * TblOrganisations.
 *
 * @ORM\Table(name="tbl_organisations")
 * @ORM\Entity(repositoryClass="App\Repository\TblOrganisationsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TblOrganisations extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;
    use RadieField;


    /**
     * @var string
     *
     * @ORM\Column(name="adresse_siege", type="string", length=255, nullable=true)
     */
    private $adresseSiege;

    /**
     * @var ArrayCollection
     *
     * @ORM\ManyToMany(targetEntity="User", mappedBy="cooperatives")
     */
    private $agentsResponsables;

    /**
     * @var string
     *
     * @ORM\Column(name="annee_creation", type="string", length=4, nullable=true)
     */
    private $anneeCreation;

    /**
     * @OneToMany(targetEntity="TblOrganisations", mappedBy="parent")
     */
    private $children;

    /**
     * @var int
     *
     * @ORM\Column(name="code", type="integer", nullable=true, unique=true)
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="contact1_telephone", type="string", length=255, nullable=true)
     */
    private $contact1Telephone;

    /**
     * @var string
     *
     * @ORM\Column(name="contact2_telephone", type="string", length=255, nullable=true)
     */
    private $contact2Telephone;

    /**
     * @ORM\ManyToOne(targetEntity="TblDecoupageAdmin")
     */
    private $decoupageAdmin;

    /**
     * @var string
     *
     * @ORM\Column(name="denomination", type="string", length=255, nullable=true)
     */
    private $denomination;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\ManyToOne(targetEntity="TblOrganisationLevel")
     */
    private $level;

    /**
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @ORM\ManyToOne(targetEntity="TblOrganisations")
     */
    private $parent;

    /**
     * @OneToMany(targetEntity="PivotExploitationOrganisations", mappedBy="organisation")
     */
    private $pivots;

    public function __construct($user)
    {
        parent::__construct($user);
        $this->agentsResponsables = new ArrayCollection();
        $this->exploitations = new ArrayCollection();
        $this->children = new ArrayCollection();
    }

    public function __toString(): string
    {
        if (null === $this->getParent()) {
            return $this->getDenomination();
        }

        return sprintf(
            '%s / %s',
            $this->getParent(),
            $this->getDenomination()
        );
    }

    /**
     * Get adresseSiege.
     *
     * @return string
     */
    public function getAdresseSiege()
    {
        return $this->adresseSiege;
    }

    /**
     * Get anneeCreation.
     *
     * @return string
     */
    public function getAnneeCreation()
    {
        return $this->anneeCreation;
    }

    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Get code.
     *
     * @return int
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * Get contact1Telephone.
     *
     * @return string
     */
    public function getContact1Telephone()
    {
        return $this->contact1Telephone;
    }

    /**
     * Get contact2Telephone.
     *
     * @return string
     */
    public function getContact2Telephone()
    {
        return $this->contact2Telephone;
    }

    /**
     * Get decoupageAdmin.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getDecoupageAdmin()
    {
        return $this->decoupageAdmin;
    }

    /**
     * Get denomination.
     *
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * Recevoir les exploitations de l'organisation
     * + celles de ses descendants.
     *
     * @return ArrayCollection
     */
    public function getDescendantOrSelfExploitations()
    {
        if (count($this->children) === 0) {
            return $this->getExploitations();
        }
        $ret = [];

        foreach ($this->getChildren() as $child) {
            $ret = array_merge($ret, $child->getDescendantOrSelfExploitations()->toArray());
        }

        return new ArrayCollection($ret);
    }

    /**
     * @return Exploitations
     */
    public function getExploitations()
    {
        $ret = [];

        foreach ($this->pivots as $pivot) {
            $ret[] = $pivot->getExploitation();
        }

        return new ArrayCollection($ret);
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    public function getLatitude(): ?float
    {
        return $this->latitude;
    }

    /**
     * Get level.
     *
     * @return \App\Entity\TblOrganisationLevel
     */
    public function getLevel()
    {
        return $this->level;
    }

    public function getLongitude(): ?float
    {
        return $this->longitude;
    }

    /**
     * Get parent.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getParent()
    {
        return $this->parent;
    }

    public function isCooperative()
    {
        return $this->getLevel()->getDenomination() === TblOrganisationLevel::COOPERATIVE;
    }

    public function isFederation()
    {
        return $this->getLevel()->getDenomination() === TblOrganisationLevel::FEDERATION;
    }

    public function isGroupement()
    {
        return $this->getLevel()->getDenomination() === TblOrganisationLevel::GROUPEMENT;
    }

    /**
     * Set adresseSiege.
     *
     * @param string $adresseSiege
     *
     * @return TblOrganisations
     */
    public function setAdresseSiege($adresseSiege)
    {
        $this->adresseSiege = $adresseSiege;

        return $this;
    }

    /**
     * Set anneeCreation.
     *
     * @param string $anneeCreation
     *
     * @return TblOrganisations
     */
    public function setAnneeCreation($anneeCreation)
    {
        $this->anneeCreation = $anneeCreation;

        return $this;
    }

    /**
     * Set code.
     *
     * @return TblOrganisations
     */
    public function setCode(int $code)
    {
        $this->code = $code;

        return $this;
    }

    /**
     * Set contact1Telephone.
     *
     * @param string $contact1Telephone
     *
     * @return TblOrganisations
     */
    public function setContact1Telephone($contact1Telephone)
    {
        $this->contact1Telephone = $contact1Telephone;

        return $this;
    }

    /**
     * Set contact2Telephone.
     *
     * @param string $contact2Telephone
     *
     * @return TblOrganisations
     */
    public function setContact2Telephone($contact2Telephone)
    {
        $this->contact2Telephone = $contact2Telephone;

        return $this;
    }

    /**
     * Set decoupageAdmin.
     *
     * @param \App\Entity\TblDecoupageAdmin $decoupageAdmin
     *
     * @return TblOrganisations
     */
    public function setDecoupageAdmin(?TblDecoupageAdmin $decoupageAdmin = null)
    {
        $this->decoupageAdmin = $decoupageAdmin;

        return $this;
    }

    /**
     * Set denomination.
     *
     * @param string $denomination
     *
     * @return TblOrganisations
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    public function setLatitude(?float $latitude): self
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Set level.
     *
     * @param \App\Entity\TblOrganisationLevel $level
     *
     * @return TblOrganisations
     */
    public function setLevel(?TblOrganisationLevel $level = null)
    {
        $this->level = $level;

        return $this;
    }

    public function setLongitude(?float $longitude): self
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Set parent.
     *
     * @param \App\Entity\TblOrganisations $parent
     *
     * @return TblOrganisations
     */
    public function setParent(?TblOrganisations $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }
}
