<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Exercice.
 *
 * @ORM\Table(name="exercice", uniqueConstraints={@ORM\UniqueConstraint(name="unique_exploitation_annee",
 * columns={"exploitation_id", "annee"}) })
 * @ORM\Entity
 */
class Exercice extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var int
     *
     * @ORM\Column(name="annee", type="smallint")
     */
    private $annee;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="Collecte", mappedBy="exercice")
     */
    private $collectes;

    /**
     * @var Exploitations
     *
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation; // TODO  id = id exploitation + annee

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    public function __construct($user)
    {
        parent::__construct($user);
        $this->collectes = new ArrayCollection();
    }

    /**
     * Get annee.
     *
     * @return int
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get collectes.
     *
     * @return ArrayCollection
     */
    public function getCollectes()
    {
        return $this->collectes;
    }

    /**
     * Get exploitation.
     *
     * @return Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set annee.
     *
     * @param int $annee
     *
     * @return Exercice
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set collectes.
     *
     * @return Exercice
     */
    public function setCollectes(ArrayCollection $collectes)
    {
        $this->collectes = $collectes;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param Exploitations $exploitation
     *
     * @return Exercice
     */
    public function setExploitation($exploitation)
    {
        $this->exploitation = $exploitation;

        return $this;
    }
}
