<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * AnimauxProduction.
 *
 * @ORM\Table(name="animaux_production")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class AnimauxProduction extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="Animaux")
     */
    private $animaux;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblAnimauxProduction")
     */
    private $production;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="integer", nullable=true)
     */
    private $quantite;

    /**
     * @var string
     *
     * @ORM\Column(name="unite", type="string", length=30, nullable=true)
     */
    private $unite;

    /**
     * Get animaux.
     *
     * @return \App\Entity\Animaux
     */
    public function getAnimaux()
    {
        return $this->animaux;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get production.
     *
     * @return \App\Entity\TblAnimauxProduction
     */
    public function getProduction()
    {
        return $this->production;
    }

    /**
     * Get quantite.
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get unite.
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set animaux.
     *
     * @param \App\Entity\Animaux $animaux
     *
     * @return AnimauxProduction
     */
    public function setAnimaux(?Animaux $animaux = null)
    {
        $this->animaux = $animaux;

        return $this;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return AnimauxProduction
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set production.
     *
     * @param \App\Entity\TblAnimauxProduction $production
     *
     * @return AnimauxProduction
     */
    public function setProduction(?TblAnimauxProduction $production = null)
    {
        $this->production = $production;

        return $this;
    }

    /**
     * Set quantite.
     *
     * @param int $quantite
     *
     * @return AnimauxProduction
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Set unite
     *
     * @param string $unite
     *
     * @return AnimauxProduction
     */
    public function setUnite(string $unite)
    {
        $this->unite = $unite;

        return $this;
    }
}
