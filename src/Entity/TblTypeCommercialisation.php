<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblTypeCommercialisation.
 *
 * @ORM\Table(name="tbl_type_commercialisation")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblTypeCommercialisation
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type_commercialisation", type="string", length=255, unique=true)
     */
    private $typeCommercialisation;

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        if (null === $this->getTypeCommercialisation()) {
            return 'NULL';
        }

        return $this->getTypeCommercialisation();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get typeCommercialisation.
     *
     * @return string
     */
    public function getTypeCommercialisation()
    {
        return $this->typeCommercialisation;
    }

    /**
     * Set typeCommercialisation.
     *
     * @param string $typeCommercialisation
     *
     * @return TblTypeCommercialisation
     */
    public function setTypeCommercialisation($typeCommercialisation)
    {
        $this->typeCommercialisation = $typeCommercialisation;

        return $this;
    }
}
