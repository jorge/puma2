<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use App\Validator\Constraint\VerifCulturePureAssocie;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * ParcelleHistoriques / Saison d'exploitation d'une parcelle
 * TODO renommer ParcelleHistoriques -> ParcelleSaisonCulture.
 *
 * @VerifCulturePureAssocie
 * @ORM\Table(name="parcelle_historiques")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ParcelleHistoriques extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ParcelleHistoriqueAnalyseSol", mappedBy="parcelleHistorique")
     * @ORM\OrderBy({"date": "ASC"})
     */
    private $analyseSols;

    /**
     * @var string Annee de l'enquête
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToMany(targetEntity="TblCultures")
     */
    private $cultures;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblModeCulture")
     */
    private $modeCulture;

    /**
     * @ORM\OneToMany(targetEntity="MoToutesApplis", mappedBy="parcelleHistorique")
     */
    private $moToutesApplis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\ParcelleOperationCulturale", mappedBy="parcelleHistorique")
     * @ORM\OrderBy({"date": "ASC"})
     */
    private $operations;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Parcelles", inversedBy="parcelleHistoriques")
     */
    private $parcelle;

    /**
     * @ORM\OneToMany(targetEntity="ParcelleHistoriquesTblAleas", mappedBy="parcelleHistorique")
     * @ORM\OrderBy({"date": "ASC"})
     */
    private $parcelleAleas;

    /**
     * @ORM\ManyToOne(targetEntity="TblSaisons")
     */
    private $saison;

    public function __construct($user)
    {
        parent::__construct($user);
        $this->cultures = new ArrayCollection();
        $this->moToutesApplis = new ArrayCollection();
        $this->operations = new ArrayCollection();
        $this->analyseSols = new ArrayCollection();
        $this->parcelleAleas = new ArrayCollection();
    }

    /**
     * Add analyseSol.
     *
     * @param \App\Entity\ParcelleHistoriqueAnalyseSol $analyseSol
     *
     * @return ParcelleHistoriques
     */
    public function addAnalyseSol(ParcelleHistoriqueAnalyseSol $analyseSol)
    {
        $this->analyseSol[] = $analyseSol;

        return $this;
    }

    /**
     * Add culture.
     *
     * @param \App\Entity\TblCultures $culture
     *
     * @return ParcelleHistoriques
     */
    public function addCulture(TblCultures $culture)
    {
        $this->cultures[] = $culture;

        return $this;
    }

    /**
     * Add moToutesAppli.
     *
     * @param \App\Entity\MoToutesApplis $moToutesAppli
     *
     * @return ParcelleHistoriques
     */
    public function addMoToutesAppli(MoToutesApplis $moToutesAppli)
    {
        $this->moToutesApplis[] = $moToutesAppli;

        return $this;
    }

    /**
     * Add operation.
     *
     * @param \App\Entity\ParcelleOperationCulturale $operation
     *
     * @return ParcelleHistoriques
     */
    public function addOperation(ParcelleOperationCulturale $operation)
    {
        $this->operations[] = $operation;

        return $this;
    }

    /**
     * Add parcelleAleas.
     *
     * @param \App\Entity\ParcelleHistoriquesTblAleas $parcelleAleas
     *
     * @return ParcelleHistoriques
     */
    public function addParcelleAleas(ParcelleHistoriquesTblAleas $parcelleAleas)
    {
        $this->parcelleAleas[] = $parcelleAleas;

        return $this;
    }

    /**
     * Get $analyseSols.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAnalyseSols()
    {
        return $this->analyseSols;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get all the events :
     *  - opération culturale
     *  - aleas
     *  - analyse de sol.
     */
    public function getCalendrierEvenements()
    {
        $compareDateEvenement = static function ($ev1, $ev2) {
            $timestamp1 = $ev1->getDate()->getTimestamp();
            $timestamp2 = $ev2->getDate()->getTimestamp();

            return $timestamp1 - $timestamp2;
        };

        $evenementsList = [];

        foreach ($this->operations as $ev) {
            $evenementsList[] = $ev;
        }

        foreach ($this->parcelleAleas as $ev) {
            $evenementsList[] = $ev;
        }

        foreach ($this->analyseSols as $ev) {
            $evenementsList[] = $ev;
        }

        uasort($evenementsList, $compareDateEvenement);

        return $evenementsList;
    }

    /**
     * Get cultures.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCultures()
    {
        return $this->cultures;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get idParcelle.
     *
     * @return int
     */
    public function getIdParcelle()
    {
        return $this->idParcelle;
    }

    /**
     * Get modeCulture.
     *
     * @return \App\Entity\TblModeCulture
     */
    public function getModeCulture()
    {
        return $this->modeCulture;
    }

    /**
     * Get moToutesApplis.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMoToutesApplis()
    {
        return $this->moToutesApplis;
    }

    /**
     * Get operations culturales.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getOperations()
    {
        return $this->operations;
    }

    /**
     * Get parcelle.
     *
     * @return \App\Entity\Parcelles
     */
    public function getParcelle()
    {
        return $this->parcelle;
    }

    /**
     * Get parcelleAleas.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParcelleAleas()
    {
        return $this->parcelleAleas;
    }

    /**
     * Get problemesRencontres.
     *
     * @return string
     */
    public function getProblemesRencontres()
    {
        return $this->problemesRencontres;
    }

    /**
     * Get saison.
     *
     * @return \App\Entity\TblSaisons
     */
    public function getSaison()
    {
        return $this->saison;
    }

    /**
     * Remove analyseSol.
     *
     * @param \App\Entity\ParcelleHistoriqueAnalyseSol $analyseSol
     */
    public function removeAnalyseSol(ParcelleHistoriqueAnalyseSol $analyseSol)
    {
        $this->analyseSols->removeElement($analyseSol);
    }

    /**
     * Remove culture.
     *
     * @param \App\Entity\TblCultures $culture
     */
    public function removeCulture(TblCultures $culture)
    {
        $this->cultures->removeElement($culture);
    }

    /**
     * Remove moToutesAppli.
     *
     * @param \App\Entity\MoToutesApplis $moToutesAppli
     */
    public function removeMoToutesAppli(MoToutesApplis $moToutesAppli)
    {
        $this->moToutesApplis->removeElement($moToutesAppli);
    }

    /**
     * Remove operation.
     *
     * @param \App\Entity\ParcelleOperationCulturale $operation
     */
    public function removeOperation(ParcelleOperationCulturale $operation)
    {
        $this->operations->removeElement($operation);
    }

    /**
     * Remove parcelleSemence.
     *
     * @param \App\Entity\ParcelleHistoriquesTblAleas $parcelleAleas
     */
    public function removeParcelleAleas(ParcelleHistoriquesTblAleas $parcelleAleas)
    {
        $this->parcelleAleas->removeElement($parcelleAleas);
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return ParcelleHistoriques
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

//    public function __toString()
//    {
//
//      $expl_string = sprintf('Annee: %s - Saison: %s',  $this->getAnnee(), $this->getSaison());
//
//      return $expl_string;
//    }

    /**
     * Set idParcelle.
     *
     * @param int $idParcelle
     *
     * @return ParcelleHistoriques
     */
    public function setIdParcelle($idParcelle)
    {
        $this->idParcelle = $idParcelle;

        return $this;
    }

    /**
     * Set modeCulture.
     *
     * @param \App\Entity\TblModeCulture $modeCulture
     *
     * @return ParcelleHistoriques
     */
    public function setModeCulture(?TblModeCulture $modeCulture = null)
    {
        $this->modeCulture = $modeCulture;

        return $this;
    }

    /**
     * Set parcelle.
     *
     * @param \App\Entity\Parcelles $parcelle
     *
     * @return Parcelles
     */
    public function setParcelle(?Parcelles $parcelle = null)
    {
        $this->parcelle = $parcelle;

        return $this;
    }

    /**
     * Set problemesRencontres.
     *
     * @param string $problemesRencontres
     *
     * @return ParcelleHistoriques
     */
    public function setProblemesRencontres($problemesRencontres)
    {
        $this->problemesRencontres = $problemesRencontres;

        return $this;
    }

    /**
     * Set saison.
     *
     * @param \App\Entity\TblSaisons $saison
     *
     * @return ParcelleHistoriques
     */
    public function setSaison(?TblSaisons $saison = null)
    {
        $this->saison = $saison;

        return $this;
    }
}
