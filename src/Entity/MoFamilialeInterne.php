<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * MoFamilialeInterne.
 *
 * @ORM\Table(name="mo_familiale_interne")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class MoFamilialeInterne extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations", inversedBy="mo_familiale_internes")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="jours_an", type="integer")
     */
    private $joursAn;

    /**
     * @var int
     *
     * @ORM\Column(name="membres", type="string", nullable=true)
     */
    private $membres;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get joursAn.
     *
     * @return string
     */
    public function getJoursAn()
    {
        return $this->joursAn;
    }

    /**
     * Get membres.
     *
     * @return string
     */
    public function getMembres()
    {
        return $this->membres;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return MoFamilialeInterne
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return MoFamilialeInterne
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set joursAn.
     *
     * @param string $joursAn
     *
     * @return MoFamilialeInterne
     */
    public function setJoursAn($joursAn)
    {
        $this->joursAn = $joursAn;

        return $this;
    }

    /**
     * Set membres.
     *
     * @param string $membres
     *
     * @return MoFamilialeInterne
     */
    public function setMembres($membres)
    {
        $this->membres = $membres;

        return $this;
    }
}
