<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 *saisie get infoMenage.
 */
class infoMenage
{
    protected $exploitation;

    protected $nombreAutres;

    protected $nombreFilles;

    protected $nombreFils;

    protected $personnesCharge;

    protected $totalMenage;

    public function __construct($explot)
    {
        $this->setNombreFils($explot->getNombreFils());
        $this->setNombreFilles($explot->getNombreFilles());
        $this->setPersonnesCharge($explot->getPersonnesCharge());
        $this->setNombreAutres($explot->getNombreAutres());
        $this->setTotalMenage($explot->getTotalMenage());
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get nombreAutres.
     *
     * @return int
     */
    public function getNombreAutres()
    {
        return $this->nombreAutres;
    }

    /**
     * Get nombreFilles.
     *
     * @return int
     */
    public function getNombreFilles()
    {
        return $this->nombreFilles;
    }

    /**
     * Get nombreFils.
     *
     * @return int
     */
    public function getNombreFils()
    {
        return $this->nombreFils;
    }

    /**
     * Get personnesCharge.
     *
     * @return int
     */
    public function getPersonnesCharge()
    {
        return $this->personnesCharge;
    }

    /**
     * Get totalMenage.
     *
     * @return int
     */
    public function getTotalMenage()
    {
        return $this->totalMenage;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return infoMenage
     */
    public function setExploitation(Exploitations $exploitation)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set nombreAutres.
     *
     * @param int $nombreAutres
     *
     * @return infoMenage
     */
    public function setNombreAutres($nombreAutres)
    {
        $this->nombreAutres = $nombreAutres;

        return $this;
    }

    /**
     * Set nombreFilles.
     *
     * @param int $nombreFilles
     *
     * @return infoMenage
     */
    public function setNombreFilles($nombreFilles)
    {
        $this->nombreFilles = $nombreFilles;

        return $this;
    }

    /**
     * Set nombreFils.
     *
     * @param int $nombreFils
     *
     * @return infoMenage
     */
    public function setNombreFils($nombreFils)
    {
        $this->nombreFils = $nombreFils;

        return $this;
    }

    /**
     * Set personnesCharge.
     *
     * @param int $personnesCharge
     *
     * @return infoMenage
     */
    public function setPersonnesCharge($personnesCharge)
    {
        $this->personnesCharge = $personnesCharge;

        return $this;
    }

    /**
     * Set totalMenage.
     *
     * @param int $totalMenage
     *
     * @return infoMenage
     */
    public function setTotalMenage($totalMenage)
    {
        $this->totalMenage = $totalMenage;

        return $this;
    }
}
