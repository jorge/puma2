<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// NEXT utiliser creator & lastEditor
// avoir une variable ficheType ou fiche Content -> qui indique si c'est une fiche None, S, F0, F1, .... F2
// si ce n'est pas Null, alors on update la collecte liée à la donnée
//  quand ouverture de Collecte alors on rajoute l'utilisateur comme creator

// todo faire des tests ?

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PumaEntity.
 *
 * Abstract class to declare a common constructor for all entities
 *
 * @ORM\MappedSuperclass
 * @ORM\HasLifecycleCallbacks
 */
abstract class PumaEntity implements PumaEntityInterface
{
    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $creator;

    protected $fiche; // None, S, 0, ... 6

    /**
     * @ORM\ManyToOne(targetEntity="User")
     */
    protected $lastEditor;

    /** If the entity is associated to a record (fiche en Français ) :
     * accepted values : -1 -> Signalétique, 0 -> F0, ..., 6 -> F6.
     */
    protected $recordType;

    public function __construct($user)
    {
        // TODO REMOVE
        if (null === $this->utilisateur) {
            $this->utilisateur = $user->getUsername();
        }

        return $this;
    }

    /**
     * This is the default toString function to avoid blockages on queries
     * but it's often convenient to override this in each entity to display
     * a more human readable field.
     */
    public function __toString(): string
    {
        return (string) $this->getId();
    }

    /**
     * Get creator.
     *
     * @return User
     */
    public function getCreator(): ?User
    {
        return $this->creator;
    }

    abstract public function getId();

    /**
     * Get lastEditor.
     *
     * @return User
     */
    public function getLastEditor(): ?User
    {
        return $this->lastEditor;
    }

    /**
     * Get recordType.
     *
     * @return recordType
     */
    public function getRecordType()
    {
        return $this->recordType;
    }

    /**
     * Set creator.
     *
     * @return PumaEntity
     */
    public function setCreator(User $creator)
    {
        $this->creator = $creator;

        return $this;
    }

    /**
     * Set lastEditor.
     *
     * @return PumaEntity
     */
    public function setLastEditor(User $lastEditor)
    {
        $this->lastEditor = $lastEditor;

        return $this;
    }

    /**
     * set recordType.
     *
     * @param $recordType
     *
     * @return PumaEntity
     */
    public function setRecordType($recordType)
    {
        $this->recordType = $recordType;

        return $this;
    }
}
