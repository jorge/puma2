<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use App\Validator\Constraint\ParentCorrespondLevel;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblDecoupageAdmin.
 *
 * @ParentCorrespondLevel
 * @ORM\Table(name="tbl_decoupage_admin")
 * @ORM\Entity(repositoryClass="App\Repository\TblDecoupageAdminRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TblDecoupageAdmin extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="denomination", type="string", length=255, nullable=true)
     */
    private $denomination;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblDecoupageAdminLevel")
     */
    private $level;

    /**
     * @ORM\ManyToOne(targetEntity="TblDecoupageAdmin")
     */
    private $parent;

    public function __toString(): string
    {
        if (null === $this->getParent()) {
            return $this->getDenomination();
        }

        return sprintf(
            '%s / %s',
            $this->getParent(),
            $this->getDenomination()
        );
    }

    public function getByLevel($denomination)
    {
        if ($this->level->getDenomination() === $denomination) {
            return $this;
        }

        if (null === $this->parent) {
            return null;
        }

        return $this->parent->getByLevel($denomination);
    }

    /**
     * Get denomination.
     *
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get level.
     *
     * @return \App\Entity\TblDecoupageAdminLevel
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Get parent.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Set denomination.
     *
     * @param string $denomination
     *
     * @return TblDecoupageAdmin
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    /**
     * Set level.
     *
     * @param \App\Entity\TblDecoupageAdminLevel $level
     *
     * @return TblDecoupageAdmin
     */
    public function setLevel(?TblDecoupageAdminLevel $level = null)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Set parent.
     *
     * @param \App\Entity\TblDecoupageAdmin $parent
     *
     * @return TblDecoupageAdmin
     */
    public function setParent(?TblDecoupageAdmin $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }
}
