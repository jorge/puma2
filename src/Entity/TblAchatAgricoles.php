<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblAchatAgricoles.
 *
 * @ORM\Table(name="tbl_achat_agricoles")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblAchatAgricoles extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="achat", type="string", length=40)
     */
    private $achat;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type_achat", type="string", length=10)
     */
    private $typeAchat;

    public function __toString(): string
    {
        return $this->achat;
    }

    /**
     * Get achat.
     *
     * @return string
     */
    public function getAchat()
    {
        return $this->achat;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get typeAchat.
     *
     * @return string
     */
    public function getTypeAchat()
    {
        return $this->typeAchat;
    }

    /**
     * Set achat.
     *
     * @param string $achat
     *
     * @return TblAchatAgricoles
     */
    public function setAchat($achat)
    {
        $this->achat = $achat;

        return $this;
    }

    /**
     * Set typeAchat.
     *
     * @param string $typeAchat
     *
     * @return TblAchatAgricoles
     */
    public function setTypeAchat($typeAchat)
    {
        $this->typeAchat = $typeAchat;

        return $this;
    }
}
