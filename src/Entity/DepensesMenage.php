<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * DepensesMenage.
 *
 * @ORM\Table(name="depenses_menage")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class DepensesMenage extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var string
     *
     * @ORM\Column(name="depense", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $depense;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations", inversedBy="depensesMenages")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblTypeDepense")
     */
    private $typeDepense;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get depense.
     *
     * @return string
     */
    public function getDepense()
    {
        return $this->depense;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get typeDepense.
     *
     * @return \App\Entity\TblTypeDepense
     */
    public function getTypeDepense()
    {
        return $this->typeDepense;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return DepensesMenage
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set depense.
     *
     * @param string $depense
     *
     * @return DepensesMenage
     */
    public function setDepense($depense)
    {
        $this->depense = $depense;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return DepensesMenage
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set typeDepense.
     *
     * @param \App\Entity\TblTypeDepense $typeDepense
     *
     * @return DepensesMenage
     */
    public function setTypeDepense($typeDepense)
    {
        $this->typeDepense = $typeDepense;

        return $this;
    }
}
