<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * SecuriteAlimentaire.
 *
 * @ORM\Table(name="securite_alimentaire")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SecuriteAlimentaire extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var int
     *
     * @ORM\Column(name="enfants_malnutrition", type="integer")
     */
    private $enfantsMalnutrition;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations", inversedBy="securiteAlimentaires")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="repas_culture", type="integer")
     */
    private $repasCulture;

    /**
     * @var int
     *
     * @ORM\Column(name="repas_post_recolte", type="integer")
     */
    private $repasPostRecolte;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get enfantsMalnutrition.
     *
     * @return int
     */
    public function getEnfantsMalnutrition()
    {
        return $this->enfantsMalnutrition;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get repasCulture.
     *
     * @return int
     */
    public function getRepasCulture()
    {
        return $this->repasCulture;
    }

    /**
     * Get repasPostRecolte.
     *
     * @return int
     */
    public function getRepasPostRecolte()
    {
        return $this->repasPostRecolte;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return SecuriteAlimentaire
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set enfantsMalnutrition.
     *
     * @param int $enfantsMalnutrition
     *
     * @return SecuriteAlimentaire
     */
    public function setEnfantsMalnutrition($enfantsMalnutrition)
    {
        $this->enfantsMalnutrition = $enfantsMalnutrition;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return SecuriteAlimentaire
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set repasCulture.
     *
     * @param int $repasCulture
     *
     * @return SecuriteAlimentaire
     */
    public function setRepasCulture($repasCulture)
    {
        $this->repasCulture = $repasCulture;

        return $this;
    }

    /**
     * Set repasPostRecolte.
     *
     * @param int $repasPostRecolte
     *
     * @return SecuriteAlimentaire
     */
    public function setRepasPostRecolte($repasPostRecolte)
    {
        $this->repasPostRecolte = $repasPostRecolte;

        return $this;
    }
}
