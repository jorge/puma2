<?php

declare(strict_types=1);

namespace App\Entity;

use DateTimeInterface;

interface PumaEntityInterface
{
    public function getCDate(): DateTimeInterface;

    public function getUDate(): DateTimeInterface;

    public function getUtilisateur(): ?string;

    public function setCDate(DateTimeInterface $dateTime): void;

    public function setUDate(DateTimeInterface $dateTime): void;

    public function setUtilisateur(?string $user = null): void;
}
