<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblProduits.
 *
 * @ORM\Table(name="tbl_produits")
 * @ORM\Entity(repositoryClass="App\Repository\TblProduitsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TblProduits extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="code_produit", type="string", length=5, nullable=true)
     */
    private $codeProduit;

    /**
     * @ORM\ManyToOne(targetEntity="TblCultures")
     */
    private ?TblCultures $culture;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="produit", type="string", length=50, nullable=true)
     */
    private $produit;

    /**
     * @var string
     *
     * @ORM\Column(name="unite_produit", type="string", length=25, nullable=true)
     */
    private $uniteProduit;

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        return sprintf('%s - %s', $this->culture, $this->produit);
    }

    /**
     * Get codeProduit.
     *
     * @return string
     */
    public function getCodeProduit()
    {
        return $this->codeProduit;
    }

    /**
     * Get culture.
     *
     * @return \App\Entity\TblCultures
     */
    public function getCulture(): ?TblCultures
    {
        return $this->culture;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get produit.
     *
     * @return string
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Get uniteProduit.
     *
     * @return string
     */
    public function getUniteProduit()
    {
        return $this->uniteProduit;
    }

    public function nomAvecUnite(): string
    {
        return sprintf('%s (unité : %s)', $this->produit, $this->uniteProduit);
    }

    /**
     * Set codeProduit.
     *
     * @param string $codeProduit
     *
     * @return TblProduits
     */
    public function setCodeProduit($codeProduit)
    {
        $this->codeProduit = $codeProduit;

        return $this;
    }

    /**
     * Set culture.
     *
     * @param \App\Entity\TblCultures $culture
     *
     * @return TblProduits
     */
    public function setCulture(?TblCultures $culture)
    {
        $this->culture = $culture;

        return $this;
    }

    /**
     * Set produit.
     *
     * @param string $produit
     *
     * @return TblProduits
     */
    public function setProduit($produit)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Set uniteProduit.
     *
     * @param string $uniteProduit
     *
     * @return TblProduits
     */
    public function setUniteProduit($uniteProduit)
    {
        $this->uniteProduit = $uniteProduit;

        return $this;
    }
}
