<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * PersonneDetails.
 *
 * @ORM\Table(name="personne_details", uniqueConstraints={@ORM\UniqueConstraint(name="unique_personnes_annee", columns={"annee", "personnes_id"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity({"annee", "personnes"}, message="Il existe déjà le détails de cette personne pour l'année {{ value }}")
 */
class PersonneDetails extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    public const DIPLOME_A2 = 1;

    public const DIPLOME_A3 = 2;

    public const DIPLOME_CONVERTOR = [
        0 => 'Pas de diplome',
        1 => 'A2',
        2 => 'A3',
        3 => 'D4',
        4 => 'D6',
        5 => 'D7',
        6 => 'Supérieur',
    ];

    public const DIPLOME_D4 = 3;

    public const DIPLOME_D6 = 4;

    public const DIPLOME_D7 = 5;

    public const DIPLOME_NO = 0;

    public const DIPLOME_SUP = 6;

    public const NO_VALUE = 0;

    public const NOT_ENCODED_STR = 'Non encodé';

    public const NOT_ENCODED_VALUE = null;

    public const YES_VALUE = 1;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var int
     *
     * @ORM\Column(name="diplome", type="smallint", nullable=true)
     */
    private $diplome;

    /**
     * @ORM\ManyToOne(targetEntity="TblDomainesEtudesSuperieures")
     */
    private $domaine;

    /**
     * @var int
     *
     * @ORM\Column(name="francais_ecrire", type="smallint", nullable=true)
     */
    private $francaisEcrire;

    /**
     * @var int
     *
     * @ORM\Column(name="francais_lire", type="smallint", nullable=true)
     */
    private $francaisLire;

    /**
     * @var guid
     *
     * Details sur l'education de la personne
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="kirundi_ecrire", type="smallint", nullable=true)
     */
    private $kirundiEcrire;

    /**
     * @var int
     *
     * @ORM\Column(name="kirundi_lire", type="smallint", nullable=true)
     */
    private $kirundiLire;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Personnes", inversedBy="PersonneDetails")
     */
    private $personnes;

    /**
     * @var int
     *
     * @ORM\Column(name="primaire", type="smallint", nullable=true)
     */
    private $primaire;

    /**
     * @ORM\ManyToOne(targetEntity="TblProfessions")
     */
    private $professionPrincipale;

    /**
     * @ORM\ManyToOne(targetEntity="TblProfessions")
     */
    private $professionSecondaire;

    /**
     * @var int
     *
     * @ORM\Column(name="secondaire", type="smallint", nullable=true)
     */
    private $secondaire;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get diplome.
     *
     * @return int
     */
    public function getDiplome()
    {
        return $this->diplome;
    }

    /**
     * Get diplome in string.
     */
    public function getDiplomeInString()
    {
        if ($this->getDiplome() !== null) {
            return $this::DIPLOME_CONVERTOR[$this->getDiplome()];
        }

        return $this::NOT_ENCODED_STR;
    }

    /**
     * Get domaine.
     *
     * @return \App\Entity\TblDomainesEtudesSuperieures
     */
    public function getDomaine()
    {
        return $this->domaine;
    }

    /**
     * Get exploitation.
     */
    public function getExploitation()
    {
        if ($this->personnes) {
            return $this->personnes->getExploitation();
        }

        return null;
    }

    /**
     * Get francaisEcrire.
     *
     * @return int
     */
    public function getFrancaisEcrire()
    {
        return $this->francaisEcrire;
    }

    /**
     * Get francaisLire.
     *
     * @return int
     */
    public function getFrancaisLire()
    {
        return $this->francaisLire;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get kirundiEcrire.
     *
     * @return int
     */
    public function getKirundiEcrire()
    {
        return $this->kirundiEcrire;
    }

    /**
     * Get kirundiLire.
     *
     * @return int
     */
    public function getKirundiLire()
    {
        return $this->kirundiLire;
    }

    /**
     * Get personnes.
     *
     * @return \App\Entity\Personnes
     */
    public function getPersonnes()
    {
        return $this->personnes;
    }

    /**
     * Get primaire.
     *
     * @return int
     */
    public function getPrimaire()
    {
        return $this->primaire;
    }

    /**
     * Get professionPrincipale.
     *
     * @return \App\Entity\TblProfessions
     */
    public function getProfessionPrincipale()
    {
        return $this->professionPrincipale;
    }

    /**
     * Get professionSecondaire.
     *
     * @return \App\Entity\TblProfessions
     */
    public function getProfessionSecondaire()
    {
        return $this->professionSecondaire;
    }

    /**
     * Get secondaire.
     *
     * @return int
     */
    public function getSecondaire()
    {
        return $this->secondaire;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return PersonneDetails
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set diplome.
     *
     * @param int $diplome
     *
     * @return PersonneDetails
     */
    public function setDiplome($diplome)
    {
        $this->diplome = $diplome;

        return $this;
    }

    /**
     * Set domaine.
     *
     * @param \App\Entity\TblDomainesEtudesSuperieures $domaine
     *
     * @return PersonneDetails
     */
    public function setDomaine(?TblDomainesEtudesSuperieures $domaine = null)
    {
        $this->domaine = $domaine;

        return $this;
    }

    /**
     * Set francaisEcrire.
     *
     * @param int $francaisEcrire
     *
     * @return PersonneDetails
     */
    public function setFrancaisEcrire($francaisEcrire)
    {
        $this->francaisEcrire = $francaisEcrire;

        return $this;
    }

    /**
     * Set francaisLire.
     *
     * @param int $francaisLire
     *
     * @return PersonneDetails
     */
    public function setFrancaisLire($francaisLire)
    {
        $this->francaisLire = $francaisLire;

        return $this;
    }

    /**
     * Set kirundiEcrire.
     *
     * @param int $kirundiEcrire
     *
     * @return PersonneDetails
     */
    public function setKirundiEcrire($kirundiEcrire)
    {
        $this->kirundiEcrire = $kirundiEcrire;

        return $this;
    }

    /**
     * Set kirundiLire.
     *
     * @param int $kirundiLire
     *
     * @return PersonneDetails
     */
    public function setKirundiLire($kirundiLire)
    {
        $this->kirundiLire = $kirundiLire;

        return $this;
    }

    /**
     * Set personnes.
     *
     * @param \App\Entity\Personnes $personnes
     *
     * @return PersonneDetails
     */
    public function setPersonnes(?Personnes $personnes = null)
    {
        $this->personnes = $personnes;

        return $this;
    }

    /**
     * Set primaire.
     *
     * @param int $primaire
     *
     * @return PersonneDetails
     */
    public function setPrimaire($primaire)
    {
        $this->primaire = $primaire;

        return $this;
    }

    /**
     * Set professionPrincipale.
     *
     * @param \App\Entity\TblProfessions
     *
     * @return PersonneDetails
     */
    public function setProfessionPrincipale(?TblProfessions $professionPrincipale = null)
    {
        $this->professionPrincipale = $professionPrincipale;

        return $this;
    }

    /**
     * Set professionSecondaire.
     *
     * @param \App\Entity\TblProfessions $professionSecondaire
     *
     * @return PersonneDetails
     */
    public function setProfessionSecondaire(?TblProfessions $professionSecondaire = null)
    {
        $this->professionSecondaire = $professionSecondaire;

        return $this;
    }

    /**
     * Set secondaire.
     *
     * @param int $secondaire
     *
     * @return PersonneDetails
     */
    public function setSecondaire($secondaire)
    {
        $this->secondaire = $secondaire;

        return $this;
    }

    public function yesNoUndefinedInStr($data)
    {
        if ($data === $this::NOT_ENCODED_VALUE) {
            return 'Non encodé';
        }

        if ($data === $this::NO_VALUE) {
            return 'Non';
        }

        return 'Oui';
    }
}
