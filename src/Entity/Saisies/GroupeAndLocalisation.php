<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

use App\Entity\Exploitations;
use App\Entity\TblDecoupageAdmin;

/**
 *saisie get groupe.
 */
class GroupeAndLocalisation
{
    protected $anneeAdhesion;

    protected $colline;

    protected $commune;

    protected $cooperative;

    protected $exploitation;

    protected $groupement;

    protected $latitude;

    protected $longitude;

    protected $province;

    protected $telephone;

    /**
     * Get anneeAdhesion.
     *
     * @return string
     */
    public function getAnneeAdhesion()
    {
        return $this->anneeAdhesion;
    }

    /**
     * Get colline.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getColline()
    {
        return $this->colline;
    }

    /**
     * Get commune.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * Get cooperative.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getCooperative()
    {
        return $this->cooperative;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get groupement.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getGroupement()
    {
        return $this->groupement;
    }

    /**
     * Get latitude.
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Get longitude.
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Get province.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set anneeAdhesion.
     *
     * @param string $anneeAdhesion
     *
     * @return GroupeAndLocalisation
     */
    public function setAnneeAdhesion($anneeAdhesion)
    {
        $this->anneeAdhesion = $anneeAdhesion;

        return $this;
    }

    /**
     * Set colline.
     *
     * @param \App\Entity\TblDecoupageAdmin $colline
     *
     * @return GetLocalisation
     */
    public function setColline(?TblDecoupageAdmin $colline = null)
    {
        $this->colline = $colline;

        return $this;
    }

    /**
     * Set commune.
     *
     * @param \App\Entity\TblDecoupageAdmin $commune
     *
     * @return GetLocalisation
     */
    public function setCommune(?TblDecoupageAdmin $commune = null)
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * Set cooperative.
     *
     * @return InfoGen
     */
    public function setCooperative(?\App\Entity\TblOrganisations $cooperative = null)
    {
        $this->cooperative = $cooperative;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @return GetLocalisation
     */
    public function setExploitation(Exploitations $exploitation)
    {
        $this->exploitation = $exploitation;

        $localisation = $exploitation->getLocalisationAdmin();

        if (null !== $localisation) {
            $parent = $localisation->getParent();
            $grandparent = $parent->getParent();
            $level = $localisation->getLevel();
            $leveldenom = $level->getDenomination();

            if ('colline' === $leveldenom) {
                $this->colline = $localisation;
                $this->commune = $parent;
                $this->province = $grandparent;
            } elseif ('commune' === $leveldenom) {
                $this->commune = $localisation;
                $this->province = $parent;
            } else {
                $this->province = $localisation;
            }
        }

        return $this;
    }

    /**
     * Set groupement.
     *
     * @param \App\Entity\TblOrganisations $groupement
     *
     * @return InfoGen
     */
    public function setGroupement(?\App\Entity\TblOrganisations $groupement = null)
    {
        $this->groupement = $groupement;

        return $this;
    }

    /**
     * Set latitude.
     *
     * @param float $latitude
     *
     * @return GroupeAndLocalisation
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Set longitude.
     *
     * @param float $longitude
     *
     * @return GroupeAndLocalisation
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Set province.
     *
     * @param \App\Entity\TblDecoupageAdmin $province
     *
     * @return GetLocalisation
     */
    public function setProvince(?TblDecoupageAdmin $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return GroupeAndLocalisation
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }
}
