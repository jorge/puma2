<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

use App\Entity\Exploitations;
use App\Entity\TblDecoupageAdmin;

// FIXME DEBUG GetLocalisation.php et localisation sont hyper proche (à fusioner ?)

/**
 * Saisie get groupe.
 */
class GetLocalisation
{
    protected $colline;

    protected $commune;

    protected $exploitation;

    protected $province;

    public function __construct(Exploitations $exploitation)
    {
        return $this->setExploitation($exploitation);
    }

    /**
     * Get colline.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getColline()
    {
        return $this->colline;
    }

    /**
     * Get commune.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get province.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set colline.
     *
     * @param \App\Entity\TblDecoupageAdmin $colline
     *
     * @return GetLocalisation
     */
    public function setColline(?TblDecoupageAdmin $colline = null)
    {
        $this->colline = $colline;

        return $this;
    }

    /**
     * Set commune.
     *
     * @param \App\Entity\TblDecoupageAdmin $commune
     *
     * @return GetLocalisation
     */
    public function setCommune(?TblDecoupageAdmin $commune = null)
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @return GetLocalisation
     */
    public function setExploitation(Exploitations $exploitation)
    {
        $this->exploitation = $exploitation;

        $localisation = $exploitation->getLocalisationAdmin();

        if (null !== $localisation) {
            $parent = $localisation->getParent();
            $grandparent = $parent->getParent();
            $level = $localisation->getLevel();
            $leveldenom = $level->getDenomination();

            if ('colline' === $leveldenom) {
                $this->colline = $localisation;
                $this->commune = $parent;
                $this->province = $grandparent;
            } elseif ('commune' === $leveldenom) {
                $this->commune = $localisation;
                $this->province = $parent;
            } else {
                $this->province = $localisation;
            }
        }

        return $this;
    }

    /**
     * Set province.
     *
     * @param \App\Entity\TblDecoupageAdmin $province
     *
     * @return GetLocalisation
     */
    public function setProvince(?TblDecoupageAdmin $province = null)
    {
        $this->province = $province;

        return $this;
    }
}
