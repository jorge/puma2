<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

use DateTime;

class CollecteExercice
{
    private $date;

    private $enqueteur;

    private $exercice;

    private $type;

    /**
     * Get date d'enquete.
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get enqueteur.
     *
     * @return string
     */
    public function getEnqueteur()
    {
        return $this->enqueteur;
    }

    /**
     * Get exercice.
     *
     * @return int
     */
    public function getExercice()
    {
        return $this->exercice;
    }

    /**
     * Get type d'enquete.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set date d'enquete.
     *
     * @param DateTime $date
     *
     * @return $this
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set enqueteur.
     *
     * @param string $enqueteur
     *
     * @return $this
     */
    public function setEnqueteur($enqueteur)
    {
        $this->enqueteur = $enqueteur;

        return $this;
    }

    /**
     * Set exercice.
     *
     * @param int $exercice
     *
     * @return $this
     */
    public function setExercice($exercice)
    {
        $this->exercice = $exercice;

        return $this;
    }

    /**
     * Set type de collecte.
     *
     * @param int $type
     *
     * @return $this
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
