<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

use App\Entity\User;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;

/**
 * Contrôle du travail de creation ou mise à jour d'un users.
 */
class ActivityUser
{
    protected $dateFrom;

    protected $dateTo;

    protected $users;

    public function __construct()
    {
        $this->users = new ArrayCollection();

        return $this;
    }

    /**
     * Add user.
     *
     * @return ActivityUser
     */
    public function addUser(User $user)
    {
        $this->users->add($user);

        return $this;
    }

    /**
     * Get dateFrom.
     *
     * @return date
     */
    public function getDateFrom()
    {
        return $this->dateFrom;
    }

    /**
     * Get dateTo.
     *
     * @return date
     */
    public function getDateTo()
    {
        return $this->dateTo;
    }

    /**
     * Get users.
     *
     * @return string
     */
    public function getusers()
    {
        return $this->users;
    }

    /**
     * Set dateFrom.
     *
     * @param date $dateFrom
     *
     * @return ActivityUser
     */
    public function setDateFrom(?DateTime $dateFrom)
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * Set dateTo.
     *
     * @param date $dateTo
     *
     * @return ActivityUser
     */
    public function setDateTo(?DateTime $dateTo)
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * Set users.
     *
     * @param ArrayCollection $users
     *
     * @return ActivityUser
     */
    public function setUsers(Collection $users)
    {
        $this->users = $users;

        return $this;
    }
}
