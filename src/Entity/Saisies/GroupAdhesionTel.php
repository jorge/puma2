<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

use Symfony\Component\Validator\Constraints as Assert;

/**
 * This class is used for filling the form
 * about CAPAD data (for an exploitation).
 */
class GroupAdhesionTel
{
    private $anneeAdhesion;

    private $carteMembre;

    private $cooperative;

    private $groupement;

    /**
     * @Assert\Length(
     *     max=30,
     *     maxMessage="Le champ telephone ne doit être pas plus grand que {{ limit }} characters"
     * )
     *
     * @var string
     */
    private $telephone;

    public function __construct()
    {
        return $this;
    }

    /**
     * Get anneeAdhesion.
     *
     * @return string
     */
    public function getAnneeAdhesion()
    {
        return $this->anneeAdhesion;
    }

    /**
     * Get carteMembre.
     *
     * @return bool
     */
    public function getCarteMembre()
    {
        return $this->carteMembre;
    }

    /**
     * Get cooperative.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getCooperative()
    {
        return $this->cooperative;
    }

    /**
     * Get groupement.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getGroupement()
    {
        return $this->groupement;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set anneeAdhesion.
     *
     * @param string $anneeAdhesion
     *
     * @return GroupAdhesionTel
     */
    public function setAnneeAdhesion($anneeAdhesion)
    {
        $this->anneeAdhesion = $anneeAdhesion;

        return $this;
    }

    /**
     * Set carteMembre.
     *
     * @param bool $carteMembre
     *
     * @return GroupAdhesionTel
     */
    public function setCarteMembre($carteMembre)
    {
        $this->carteMembre = $carteMembre;

        return $this;
    }

    /**
     * Set cooperative.
     *
     * @param \App\Entity\TblOrganisations $cooperative
     *
     * @return GroupAdhesionTel
     */
    public function setCooperative(?\App\Entity\TblOrganisations $cooperative = null)
    {
        $this->cooperative = $cooperative;

        return $this;
    }

    /**
     * Set groupement.
     *
     * @param \App\Entity\TblOrganisations $groupement
     *
     * @return GroupAdhesionTel
     */
    public function setGroupement(?\App\Entity\TblOrganisations $groupement = null)
    {
        $this->groupement = $groupement;

        return $this;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return GroupAdhesionTel
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }
}
