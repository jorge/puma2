<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

/**
 * Saisie suivis cooperatives.
 */
class Organisations
{
    protected $cooperative;

    protected $groupement;

    public function __construct()
    {
        $this->cooperative = null;
        $this->groupement = null;

        return $this;
    }

    /**
     * Get cooperative.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getCooperative()
    {
        return $this->cooperative;
    }

    /**
     * Get groupement.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getGroupement()
    {
        return $this->groupement;
    }

    /**
     * Set cooperative.
     *
     * @param \App\Entity\TblOrganisations $cooperative
     *
     * @return Organisations
     */
    public function setCooperative(?\App\Entity\TblOrganisations $cooperative = null)
    {
        $this->cooperative = $cooperative;

        return $this;
    }

    /**
     * Set groupement.
     *
     * @param \App\Entity\TblOrganisations $groupement
     *
     * @return Organisations
     */
    public function setGroupement(?\App\Entity\TblOrganisations $groupement = null)
    {
        $this->groupement = $groupement;

        return $this;
    }
}
