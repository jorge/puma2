<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

use App\Validator\Constraint\VerifOnlyAbc;
use DateTime;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Socies.
 *
 * @VerifOnlyAbc
 */
class Socies
{
    public const role_chefDeFamille = 'Chef de famille';

    public const role_conjoint = 'Conjoint';

    public const role_pas = 'Pas chef, pas conjoint';

    /**
     * @var string
     * @Assert\Length(
     *     max=45,
     *     maxMessage="Le champ cni doit être pas plus grand que {{ limit }} characters"
     * )
     */
    private $cni;

    private $dateNaissance;

    private $etatCivil;

    private $membreMenage;

    private $nom;

    private $prenom;

    private $repondant;

    private $rolFamille;

    private $sexe;

    /**
     * Get cni.
     *
     * @return string
     */
    public function getCni()
    {
        return $this->cni;
    }

    /**
     * Get dateNaissance.
     *
     * @return DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Get etatCivil.
     *
     * @return \App\Entity\TblEtatCivil
     */
    public function getEtatCivil()
    {
        return $this->etatCivil;
    }

    /**
     * Get membreMenage.
     *
     * @return int
     */
    public function getMembreMenage()
    {
        return $this->membreMenage;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Get repondant.
     *
     * @return bool
     */
    public function getRepondant()
    {
        return $this->repondant;
    }

    /**
     * Get rolFamille.
     *
     * @return string
     */
    public function getRolFamille()
    {
        return $this->rolFamille;
    }

    /**
     * Get sexe.
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Is repondant ?
     *
     * @return bool
     */
    public function isRepondant()
    {
        return $this->repondant;
    }

    /**
     * Set cni.
     *
     * @param string $cni
     *
     * @return Socies
     */
    public function setCni($cni)
    {
        $this->cni = $cni;

        return $this;
    }

    /**
     * Set dateNaissance.
     *
     * @param mixed $dateNaissance
     *
     * @return Socies
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Set etatCivil.
     *
     * @param \App\Entity\TblEtatCivil $etatCivil
     *
     * @return Socies
     */
    public function setEtatCivil(?\App\Entity\TblEtatCivil $etatCivil = null)
    {
        $this->etatCivil = $etatCivil;

        return $this;
    }

    /**
     * Set membreMenage.
     *
     * @param int $membreMenage
     *
     * @return Socies
     */
    public function setMembreMenage($membreMenage)
    {
        $this->membreMenage = $membreMenage;

        return $this;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Socies
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return Socies
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Set repondant.
     *
     * @param bool $repondant
     *
     * @return Socies
     */
    public function setRepondant($repondant)
    {
        $this->repondant = $repondant;

        return $this;
    }

    /**
     * Set rolFamille.
     *
     * @param string $rolFamille
     *
     * @return Socies
     */
    public function setRolFamille($rolFamille)
    {
        $this->rolFamille = $rolFamille;

        return $this;
    }

    /**
     * Set sexe.
     *
     * @param string $sexe
     *
     * @return Socies
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }
}
