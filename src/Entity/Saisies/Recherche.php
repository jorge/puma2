<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

/**
 * Recherche d'une exploitation via co-exploitant.
 */
class Recherche
{
    protected $anneeAdhesion;

    protected $anneeNaissance;

    protected $cni;

    protected $colline;

    protected $commune;

    protected $cooperative;

    protected $groupement;

    protected $nom;

    protected $numeroMembre;

    protected $prenom;

    protected $province;

    public function __construct()
    {
        $this->nom = null;
        $this->prenom = null;
        $this->numeroMembre = null;
        $this->anneeNaissance = null;
        $this->anneeAdhesion = null;
        $this->cni = null;
        $this->cooperative = null;
        $this->groupement = null;
        $this->province = null;
        $this->commune = null;
        $this->colline = null;

        return $this;
    }

    /**
     * Get anneeAdhesion.
     *
     * @return string
     */
    public function getAnneeAdhesion()
    {
        return $this->anneeAdhesion;
    }

    /**
     * Get anneeNaissance.
     *
     * @return string
     */
    public function getAnneeNaissance()
    {
        return $this->anneeNaissance;
    }

    /**
     * Get cni.
     *
     * @return string
     */
    public function getCni()
    {
        return $this->cni;
    }

    /**
     * Get colline.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getColline()
    {
        return $this->colline;
    }

    /**
     * Get commune.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * Get cooperative.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getCooperative()
    {
        return $this->cooperative;
    }

    /**
     * Get groupement.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getGroupement()
    {
        return $this->groupement;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get numeroMembre.
     *
     * @return string
     */
    public function getNumeroMembre()
    {
        return $this->numeroMembre;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Get province.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Set anneeAdhesion.
     *
     * @param string $anneeAdhesion
     *
     * @return Recherche
     */
    public function setAnneeAdhesion($anneeAdhesion)
    {
        $this->anneeAdhesion = $anneeAdhesion;

        return $this;
    }

    /**
     * Set anneeNaissance.
     *
     * @param string $anneeNaissance
     *
     * @return Recherche
     */
    public function setAnneeNaissance($anneeNaissance)
    {
        $this->anneeNaissance = $anneeNaissance;

        return $this;
    }

    /**
     * Set cni.
     *
     * @param string $cni
     *
     * @return Recherche
     */
    public function setCni($cni)
    {
        $this->cni = $cni;

        return $this;
    }

    /**
     * Set colline.
     *
     * @param \App\Entity\TblDecoupageAdmin $colline
     *
     * @return Recherche
     */
    public function setColline(?\App\Entity\TblDecoupageAdmin $colline = null)
    {
        $this->colline = $colline;

        return $this;
    }

    /**
     * Set commune.
     *
     * @param \App\Entity\TblDecoupageAdmin $commune
     *
     * @return Recherche
     */
    public function setCommune(?\App\Entity\TblDecoupageAdmin $commune = null)
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * Set cooperative.
     *
     * @param \App\Entity\TblOrganisations $cooperative
     *
     * @return Recherche
     */
    public function setCooperative($cooperative)
    {
        $this->cooperative = $cooperative;

        return $this;
    }

    /**
     * Set groupement.
     *
     * @param \App\Entity\TblOrganisations $groupement
     *
     * @return Recherche
     */
    public function setGroupement($groupement)
    {
        $this->groupement = $groupement;

        return $this;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Recherche
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Set numeroMembre.
     *
     * @param string $numeroMembre
     *
     * @return Recherche
     */
    public function setNumeroMembre($numeroMembre)
    {
        $this->numeroMembre = $numeroMembre;

        return $this;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return Recherche
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Set province.
     *
     * @param \App\Entity\TblDecoupageAdmin $province
     *
     * @return Recherche
     */
    public function setProvince(?\App\Entity\TblDecoupageAdmin $province = null)
    {
        $this->province = $province;

        return $this;
    }
}
