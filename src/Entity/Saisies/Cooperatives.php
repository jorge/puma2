<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

/**
 * Saisie suivis cooperatives.
 */
class Cooperatives
{
    protected $coop;

    protected $datadate;

    protected $groupement;

    protected $telephone;

    public function __construct()
    {
        $this->coop = null;
        $this->groupement = null;
        $this->telephone = null;
        $this->datadate = date('Y');

        return $this;
    }

    /**
     * Get coop.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getCoop()
    {
        return $this->coop;
    }

    /**
     * Get date.
     *
     * @return string
     */
    public function getDatadate()
    {
        return $this->datadate;
    }

    /**
     * Get groupement.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getGroupement()
    {
        return $this->groupement;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set coop.
     *
     * @param \App\Entity\TblOrganisations $coop
     *
     * @return Cooperatives
     */
    public function setCoop(?\App\Entity\TblOrganisations $coop = null)
    {
        $this->coop = $coop;

        return $this;
    }

    /**
     * Set date.
     *
     * @param mixed $datadate
     *
     * @return Cooperatives
     */
    public function setDatadate($datadate)
    {
        $this->datadate = $datadate;

        return $this;
    }

    /**
     * Set groupement.
     *
     * @param \App\Entity\TblOrganisations $groupement
     *
     * @return Cooperatives
     */
    public function setGroupement(?\App\Entity\TblOrganisations $groupement = null)
    {
        $this->groupement = $groupement;

        return $this;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return Cooperatives
     */
    public function setTelephone($telephone = null)
    {
        $this->telephone = $telephone;

        return $this;
    }
}
