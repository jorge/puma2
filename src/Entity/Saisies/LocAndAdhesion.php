<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

// use \App\Entity\TblDecoupageAdmin;

/**
 *saisie localisation.
 */
class LocAndAdhesion
{
    protected $anneeAdhesion;

    protected $colline;

    protected $commune;

    protected $province;

    protected $telephone;

    public function __construct()
    {
        $this->province = null;
        $this->commune = null;
        $this->colline = null;

        return $this;
    }

    /**
     * Get anneeAdhesion.
     *
     * @return string
     */
    public function getAnneeAdhesion()
    {
        return $this->anneeAdhesion;
    }

    /**
     * Get colline.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getColline()
    {
        return $this->colline;
    }

    /**
     * Get commune.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getCommune()
    {
        return $this->commune;
    }

    /**
     * Get province.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getProvince()
    {
        return $this->province;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set anneeAdhesion.
     *
     * @param string $anneeAdhesion
     *
     * @return LocAndAdhesion
     */
    public function setAnneeAdhesion($anneeAdhesion)
    {
        $this->anneeAdhesion = $anneeAdhesion;

        return $this;
    }

    /**
     * Set colline.
     *
     * @param \App\Entity\TblDecoupageAdmin $colline
     *
     * @return LocAndAdhesion
     */
    public function setColline(?\App\Entity\TblDecoupageAdmin $colline = null)
    {
        $this->colline = $colline;

        return $this;
    }

    /**
     * Set commune.
     *
     * @param \App\Entity\TblDecoupageAdmin $commune
     *
     * @return LocAndAdhesion
     */
    public function setCommune(?\App\Entity\TblDecoupageAdmin $commune = null)
    {
        $this->commune = $commune;

        return $this;
    }

    /**
     * Set province.
     *
     * @param \App\Entity\TblDecoupageAdmin $province
     *
     * @return LocAndAdhesion
     */
    public function setProvince(?\App\Entity\TblDecoupageAdmin $province = null)
    {
        $this->province = $province;

        return $this;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return LocAndAdhesion
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }
}
