<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity\Saisies;

use App\Entity\Parcelles;

/**
 * Saisie parcelles F2
 * FIXME: incomplet.
 */
class ParcellesF2
{
    protected $coordoneesGeo;

    protected $coutLocation;

    protected $exposition;

    protected $nomParcelle;

    protected $numeroParcelle;

    protected $parcelle;

    protected $pente;

    protected $propriete;

    protected $situation;

    protected $surface;

    // to be continued

    public function __construct($parcelle)
    {
        $this->parcelle = $parcelle;
        $this->numeroParcelle = $parcelle->getNumeroParcelle();
        $this->nomParcelle = $parcelle->getNomParcelle();
        $this->surface = $parcelle->getSurface();
        $this->coordoneesGeo = $parcelle->getCoordoneesGeo();
        $this->situation = $parcelle->getSituation();
        $this->exposition = $parcelle->getExposition();
        $this->pente = $parcelle->getPente();
        $this->propriete = $parcelle->getPropriete();
        $this->coutLocation = $parcelle->getCoutLocation();

        return $this;
    }

    /**
     * Get coordoneesGeo.
     *
     * @return string
     */
    public function getCoordoneesGeo()
    {
        return $this->coordoneesGeo;
    }

    /**
     * Get coutLocation.
     *
     * @return string
     */
    public function getCoutLocation()
    {
        return $this->coutLocation;
    }

    /**
     * Get exposition.
     *
     * @return \App\Entity\TblExposition
     */
    public function getExposition()
    {
        return $this->exposition;
    }

    /**
     * Get nomParcelle.
     *
     * @return string
     */
    public function getNomParcelle()
    {
        return $this->nomParcelle;
    }

    /**
     * Get numeroParcelle.
     *
     * @return int
     */
    public function getNumeroParcelle()
    {
        return $this->numeroParcelle;
    }

    /**
     * Get parcelle.
     *
     * @return \App\Entity\Parcelles
     */
    public function getParcelle()
    {
        return $this->parcelle;
    }

    /**
     * Get pente.
     *
     * @return \App\Entity\TblPente
     */
    public function getPente()
    {
        return $this->pente;
    }

    /**
     * Get propriete.
     *
     * @return \App\Entity\TblPropriete
     */
    public function getPropriete()
    {
        return $this->propriete;
    }

    /**
     * Get situation.
     *
     * @return \App\Entity\TblSituation
     */
    public function getSituation()
    {
        return $this->situation;
    }

    /**
     * Get surface.
     *
     * @return string
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Set coordoneesGeo.
     *
     * @param string $coordoneesGeo
     *
     * @return ParcellesF2
     */
    public function setCoordoneesGeo($coordoneesGeo)
    {
        $this->coordoneesGeo = $coordoneesGeo;

        return $this;
    }

    /**
     * Set coutLocation.
     *
     * @param string $coutLocation
     *
     * @return ParcellesF2
     */
    public function setCoutLocation($coutLocation)
    {
        $this->coutLocation = $coutLocation;

        return $this;
    }

    /**
     * Set exposition.
     *
     * @param \App\Entity\TblExposition $exposition
     *
     * @return ParcellesF2
     */
    public function setExposition(?\App\Entity\TblExposition $exposition = null)
    {
        $this->exposition = $exposition;

        return $this;
    }

    /**
     * Set nomParcelle.
     *
     * @param string $nomParcelle
     *
     * @return ParcellesF2
     */
    public function setNomParcelle($nomParcelle)
    {
        $this->nomParcelle = $nomParcelle;

        return $this;
    }

    /**
     * Set numeroParcelle.
     *
     * @param int $numeroParcelle
     *
     * @return ParcellesF2
     */
    public function setNumeroParcelle($numeroParcelle)
    {
        $this->numeroParcelle = $numeroParcelle;

        return $this;
    }

    /**
     * Set parcelle.
     *
     * @param \App\Entity\Parcelles $parcelle
     *
     * @return ParcellesF2
     */
    public function setParcelle(?Parcelles $parcelle = null)
    {
        $this->parcelle = $parcelle;

        return $this;
    }

    /**
     * Set pente.
     *
     * @param \App\Entity\TblPente $pente
     *
     * @return ParcellesF2
     */
    public function setPente(?\App\Entity\TblPente $pente = null)
    {
        $this->pente = $pente;

        return $this;
    }

    /**
     * Set propriete.
     *
     * @param \App\Entity\TblPropriete $propriete
     *
     * @return ParcellesF2
     */
    public function setPropriete(?\App\Entity\TblPropriete $propriete = null)
    {
        $this->propriete = $propriete;

        return $this;
    }

    /**
     * Set situation.
     *
     * @param \App\Entity\TblSituation $situation
     *
     * @return ParcellesF2
     */
    public function setSituation(?\App\Entity\TblSituation $situation = null)
    {
        $this->situation = $situation;

        return $this;
    }

    /**
     * Set surface.
     *
     * @param string $surface
     *
     * @return ParcellesF2
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }
}
