<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

// TODO SUPPRIMER

namespace App\Entity\Saisies;

/**
 * Saisie Info générales.
 */
class InfoGen
{
    protected $cooperative;

    protected $datadate;

    protected $exploitation;

    protected $groupement;

    protected $parcelle;

    public function __construct()
    {
        $this->cooperative = null;
        $this->groupement = null;
        $this->exploitation = null;
        $this->datadate = date('Y');
        $this->parcelle = null;

        return $this;
    }

    /**
     * Get cooperative.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getCooperative()
    {
        return $this->cooperative;
    }

    /**
     * Get date.
     *
     * @return string
     */
    public function getDatadate()
    {
        return $this->datadate;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get groupement.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getGroupement()
    {
        return $this->groupement;
    }

    /**
     * Get parcelle.
     *
     * @return \App\Entity\Parcelles
     */
    public function getParcelle()
    {
        return $this->parcelle;
    }

    /**
     * Set cooperative.
     *
     * @param \App\Entity\TblOrganisations $cooperative
     *
     * @return InfoGen
     */
    public function setCooperative(?\App\Entity\TblOrganisations $cooperative = null)
    {
        $this->cooperative = $cooperative;

        return $this;
    }

    /**
     * Set date.
     *
     * @param mixed $datadate
     *
     * @return InfoGen
     */
    public function setDatadate($datadate)
    {
        $this->datadate = $datadate;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return InfoGen
     */
    public function setExploitation(?\App\Entity\Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set groupement.
     *
     * @param \App\Entity\TblOrganisations $groupement
     *
     * @return InfoGen
     */
    public function setGroupement(?\App\Entity\TblOrganisations $groupement = null)
    {
        $this->groupement = $groupement;

        return $this;
    }

    /**
     * Set parcelle.
     *
     * @param \App\Entity\Parcelles $parcelle
     *
     * @return InfoGen
     */
    public function setParcelle(?\App\Entity\Parcelles $parcelle = null)
    {
        $this->parcelle = $parcelle;

        return $this;
    }
}
