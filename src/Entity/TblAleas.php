<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblAleas.
 *
 * @ORM\Table(name="tbl_aleas")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblAleas extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="alea", type="string", length=255, unique=true)
     */
    private $alea;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __toString(): string
    {
        return $this->alea;
    }

    /**
     * Get alea.
     *
     * @return string
     */
    public function getAlea()
    {
        return $this->alea;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set alea.
     *
     * @param string $alea
     *
     * @return TblAleas
     */
    public function setAlea($alea)
    {
        $this->alea = $alea;

        return $this;
    }
}
