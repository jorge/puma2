<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * AnimauxLabour.
 *
 * @ORM\Table(name="animaux_labour")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class AnimauxLabour extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="Animaux")
     */
    private $animaux;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblLabourAnimaux")
     */
    private $labour;

    /**
     * Quantite de main d'oeuvre familaire (en Homme/Jour).
     *
     * @var string
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $quantiteMOFam;

    /**
     * Quantite de main d'oeuvre familaire (en Homme/Jour).
     *
     * @var string
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $quantiteMOSal;

    /**
     * Get animaux.
     *
     * @return \App\Entity\Animaux
     */
    public function getAnimaux()
    {
        return $this->animaux;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get labour.
     *
     * @return string
     */
    public function getLabour()
    {
        return $this->labour;
    }

    /**
     * Get quantiteMOFam.
     *
     * @return int
     */
    public function getQuantiteMOFam()
    {
        return $this->quantiteMOFam;
    }

    /**
     * Get quantiteMOSal.
     *
     * @return int
     */
    public function getQuantiteMOSal()
    {
        return $this->quantiteMOSal;
    }

    /**
     * Set animaux.
     *
     * @param \App\Entity\Animaux $animaux
     *
     * @return AnimauxLabour
     */
    public function setAnimaux(?Animaux $animaux = null)
    {
        $this->animaux = $animaux;

        return $this;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return AnimauxLabour
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set labour.
     *
     * @param string $labour
     *
     * @return \App\Entity\TblLabourAnimaux
     */
    public function setLabour($labour)
    {
        $this->labour = $labour;

        return $this;
    }

    /**
     * Set quantiteMOFam.
     *
     * @param int $quantiteMOFam
     *
     * @return AnimauxLabour
     */
    public function setQuantiteMOFam($quantiteMOFam)
    {
        $this->quantiteMOFam = $quantiteMOFam;

        return $this;
    }

    /**
     * Set quantiteMOSal.
     *
     * @param int $quantiteMOSal
     *
     * @return AnimauxLabour
     */
    public function setQuantiteMOSal($quantiteMOSal)
    {
        $this->quantiteMOSal = $quantiteMOSal;

        return $this;
    }
}
