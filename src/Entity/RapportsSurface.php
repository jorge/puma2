<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 * surface.
 */
class RapportsSurface
{
    protected $Animaux;

    protected $annee; /*---*/

    protected $choixGroup;

    protected $Commercialisation;

    protected $Cotisations;

    protected $Denomination;

    protected $exploitationGroupAnnee;

    protected $Filieres;

    protected $Formations_recus;

    protected $groupeCible;

    protected $groupecoop;

    protected $groupegroup;

    protected $groupProf;

    protected $queryTheme;

    protected $Terres;

    public function __construct()
    {
        $this->Denomination = null;
        $this->groupecoop = false;
        $this->groupegroup = false;
        $this->queryTheme = false;
        $this->exploitationGroupAnnee = true;
        $this->annee = null;
        $this->groupeCible = true;
        $this->choixGroup = true;
        $this->groupProf = true;
        $this->Terres = true;
        $this->Cotisations = true;
        $this->Filieres = true;
        $this->Animaux = true;
        $this->Commercialisation = true;
        $this->Formations_recus = true;

        return $this;
    }

    public function getAnimaux()
    {
        return $this->Animaux;
    }

    public function getAnnee()
    {
        return $this->annee;
    }

    public function getchoixGroup()
    {
        return $this->choixGroup;
    }

    public function getCommercialisation()
    {
        return $this->Commercialisation;
    }

    public function getCotisations()
    {
        return $this->Cotisations;
    }

    public function getDenomination()
    {
        return $this->Denomination;
    }

    public function getExploitationGroupAnnee()
    {
        return $this->exploitationGroupAnnee;
    }

    public function getFilieres()
    {
        return $this->Filieres;
    }

    public function getFormationsRecus()
    {
        return $this->Formations_recus;
    }

    public function getgroupeCible()
    {
        return $this->groupeCible;
    }

    public function getGroupecoop()
    {
        return $this->groupecoop;
    }

    public function getGroupegroup()
    {
        return $this->groupegroup;
    }

    public function getgroupProf()
    {
        return $this->groupProf;
    }

    public function getQueryTheme()
    {
        return $this->queryTheme;
    }

    public function getTerres()
    {
        return $this->Terres;
    }

    public function setAnimaux($Animaux = false)
    {
        $this->Animaux = $Animaux;

        return $this;
    }

    //annee
    public function setAnnee($annee = null)
    {
        $this->annee = $annee;

        return $this;
    }

    public function setchoixGroup($choixGroup = false)
    {
        $this->choixGroup = $choixGroup;

        return $this;
    }

    public function setCommercialisation($Commercialisation = false)
    {
        $this->Commercialisation = $Commercialisation;

        return $this;
    }

    public function setCotisations($Cotisations = false)
    {
        $this->Cotisations = $Cotisations;

        return $this;
    }

    /**
     * Set Denomination.
     *
     * @param \App\Entity\TblOrganisations $Denomination
     *
     * @return Exploitations
     */
    public function setDenomination(?TblOrganisations $Denomination = null)
    {
        $this->Denomination = $Denomination;

        return $this;
    }

    public function setExploitationGroupAnnee($exploitationGroupAnnee = false)
    {
        $this->exploitationGroupAnnee = $exploitationGroupAnnee;

        return $this;
    }

    public function setFilieres($Filieres = false)
    {
        $this->Filieres = $Filieres;

        return $this;
    }

    public function setFormationsRecus($Formations_recus = false)
    {
        $this->Formations_recus = $Formations_recus;

        return $this;
    }

    public function setgroupeCible($groupeCible = false)
    {
        $this->groupeCible = $groupeCible;

        return $this;
    }

    public function setGroupecoop($groupecoop = false)
    {
        $this->groupecoop = $groupecoop;

        return $this;
    }

    public function setGroupegroup($groupegroup = false)
    {
        $this->groupegroup = $groupegroup;

        return $this;
    }

    public function setgroupProf($groupProf = false)
    {
        $this->groupProf = $groupProf;

        return $this;
    }

    public function setQueryTheme($queryTheme = false)
    {
        $this->queryTheme = $queryTheme;

        return $this;
    }

    public function setTerres($Terres = false)
    {
        $this->Terres = $Terres;

        return $this;
    }
}
