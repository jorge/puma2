<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblDomainesEtudesSuperieures.
 *
 * @ORM\Table(name="tbl_domaines_etudes_superieures")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblDomainesEtudesSuperieures extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="domain", type="string", length=60, nullable=true, unique=true)
     */
    private $domain;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        return $this->domain;
    }

    /**
     * Get domain.
     *
     * @return string
     */
    public function getDomain()
    {
        return $this->domain;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set domain.
     *
     * @param string $domain
     *
     * @return TblDomainesEtudesSuperieures
     */
    public function setDomain($domain)
    {
        $this->domain = $domain;

        return $this;
    }
}
