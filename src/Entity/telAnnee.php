<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 *saisie get infoMenage.
 */
class telAnnee
{
    protected $anneeAdhesion;

    protected $exploitation;

    protected $telephone;

    /**
     * Get anneeAdhesion.
     *
     * @return string
     */
    public function getAnneeAdhesion()
    {
        return $this->anneeAdhesion;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get telephone.
     *
     * @return string
     */
    public function getTelephone()
    {
        return $this->telephone;
    }

    /**
     * Set anneeAdhesion.
     *
     * @param string $anneeAdhesion
     *
     * @return telAnnee
     */
    public function setAnneeAdhesion($anneeAdhesion)
    {
        $this->anneeAdhesion = $anneeAdhesion;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return telAnnee
     */
    public function setExploitation(Exploitations $exploitation)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set telephone.
     *
     * @param string $telephone
     *
     * @return telAnnee
     */
    public function setTelephone($telephone)
    {
        $this->telephone = $telephone;

        return $this;
    }
}
