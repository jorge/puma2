<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * MoToutesApplis.
 *
 * @ORM\Table(name="mo_toutes_applis")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class MoToutesApplis extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblMoType")
     */
    private $moType;

    /**
     * @ORM\ManyToOne(targetEntity="ParcelleHistoriques", inversedBy="moToutesApplis")
     */
    private $parcelleHistorique;

    /**
     * @var string
     *
     * @ORM\Column(name="quantite", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity="TblTraitements")
     */
    private $traitement;

    /**
     * @var string
     *
     * @ORM\Column(name="unite", type="string", length=20, nullable=true)
     */
    private $unite;

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get moType.
     *
     * @return \App\Entity\TblMoType
     */
    public function getMoType()
    {
        return $this->moType;
    }

    /**
     * Get parcelleHistorique.
     *
     * @return \App\Entity\ParcelleHistoriques
     */
    public function getParcelleHistorique()
    {
        return $this->parcelleHistorique;
    }

    /**
     * Get quantite.
     *
     * @return string
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get traitement.
     *
     * @return \App\Entity\TblTraitements
     */
    public function getTraitement()
    {
        return $this->traitement;
    }

    /**
     * Get unite.
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set moType.
     *
     * @param \App\Entity\TblMoType $moType
     *
     * @return MoToutesApplis
     */
    public function setMoType(?TblMoType $moType = null)
    {
        $this->moType = $moType;

        return $this;
    }

    /**
     * Set parcelleHistorique.
     *
     * @param \App\Entity\ParcelleHistoriques $parcelleHistorique
     *
     * @return MoToutesApplis
     */
    public function setParcelleHistorique(?ParcelleHistoriques $parcelleHistorique = null)
    {
        $this->parcelleHistorique = $parcelleHistorique;

        return $this;
    }

    /**
     * Set quantite.
     *
     * @param string $quantite
     *
     * @return MoToutesApplis
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Set traitement.
     *
     * @param \App\Entity\TblTraitements $traitement
     *
     * @return MoToutesApplis
     */
    public function setTraitement(?TblTraitements $traitement = null)
    {
        $this->traitement = $traitement;

        return $this;
    }

    /**
     * Set unite.
     *
     * @param string $unite
     *
     * @return MoToutesApplis
     */
}
