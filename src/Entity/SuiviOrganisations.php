<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019-2020, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * SuiviOrganisations.
 *
 * @ORM\Table(name="suivi_organisations",
 * uniqueConstraints={@ORM\UniqueConstraint(name="user_unique", columns={"organisation_id", "annee"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SuiviOrganisations extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    // ************************************* //
    // Partie 5. INDICATEURS DE FINANCEMENTS //

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $affiliationFederation;

    /**
     * @var TblValeurChoixSimple Nombre d'alternance de président depuis la création pour CE
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="nbr_alternance_ce_choix_simple", referencedColumnName="id")
     */
    private $alternanceCENbr;

    /**
     * @var TblValeurChoixSimple Nombre d'alternance de président depuis la création pour CS
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="nbr_alternance_cs_choix_simple", referencedColumnName="id")
     */
    private $alternanceCSNbr;

    /**
     * @var int
     *
     * @ORM\Column(name="annee", type="smallint", nullable=false)
     */
    private $annee;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=true)
     */
    private $auditAnnuelDesComptes;

    // ******************************************************************* //
    // Partie 7. BESOINS ADRESSES A LA CAPAD POUR LES 3 PROCHAINNES ANNEES

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TblThemesFormations")
     * @ORM\JoinTable(name="suivi_organisations_besoins_formation")
     */
    private $besoinsFormation;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TblServicesAuxMembres")
     * @ORM\JoinTable(name="suivi_organisations_besoins_serives_aux_membres")
     */
    private $besoinsServicesAuxMembres;

    /**
     * @var float Budget annuel (Fbu)
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $budget;

    /**
     * @var bool
     *
     * @ORM\Column(name="carnet_recus", type="boolean", nullable=true)
     */
    private $carnetRecus;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $chiffreAffaireParMembre;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $chiffreDAffaires;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="classement_choix_simple", referencedColumnName="id")
     */
    private $classement;

    /**
     * @var bool
     *
     * @ORM\Column(name="classement_justificatifs", type="boolean", nullable=true)
     */
    private $classementJustificatifs;

    // ************************************* //
    // Partie 3. INDICATEURS DE GESTION FINANCIERES

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="comptabilite_choix_simple", referencedColumnName="id")
     */
    private $comptabilite;

    /**
     * @var bool
     *
     * @ORM\Column(name="compte_bancaire", type="boolean", nullable=true)
     */
    private $compteBancaire;

    /**
     * @var float
     *
     * @ORM\Column(name="credit_pour_organisation_montant", type="float", nullable=true)
     */
    private $creditPourOrganisationMontant;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="degre_prise_en_charge_ag_choix_simple", referencedColumnName="id")
     */
    private $degrePriseEnChargeAG;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="degre_prise_en_charge_ce_choix_simple", referencedColumnName="id")
     */
    private $degrePriseEnChargeCE;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="degre_prise_en_charge_cs_choix_simple", referencedColumnName="id")
     */
    private $degrePriseEnChargeCS;

    /**
     * @var bool
     *
     * @ORM\Column(name="enregistrement_cotisations", type="boolean")
     */
    private $enregistrementCotisations;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TblEquipementsAgricoles")
     */
    private $equipementAgricole;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="evolution_nbr_membres_choix_simple", referencedColumnName="id")
     */
    private $evolutionNbrMembres;

    /**
     * @var bool
     *
     * @ORM\Column(name="existence_ce", type="boolean")
     */
    private $existenceCE;

    /**
     * @var bool
     *
     * @ORM\Column(name="existence_cs", type="boolean")
     */
    private $existenceCS;

    /**
     * @var bool
     *
     * @ORM\Column(name="existence_roi", type="boolean")
     */
    private $existenceROI = false;

    // ************************************* //
    // Partie 1. INDICATEURS INSTITUTIONNELS //
    // ************************************* //

    /**
     * @var bool
     *
     * @ORM\Column(name="existence_status", type="boolean")
     */
    private $existenceStatus;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TblInfrastructures")
     */
    private $infrastructures;

    /**
     * @ORM\ManyToOne(targetEntity="TblInstitutionsFinancieres")
     */
    private $institutionBancaire;

    // ************************************** //
    // Partie 6.  MOYENS D'ACTION DISPONIBLES //

    /**
     * @ORM\ManyToOne(targetEntity="TblInstitutionsFinancieres")
     */
    private $institutionCreditMembres;

    /**
     * @ORM\ManyToOne(targetEntity="TblInstitutionsFinancieres")
     */
    private $institutionFinancementCreditOrg;

    /**
     * @var bool
     *
     * @ORM\Column(name="livre_caisse", type="boolean", nullable=true)
     */
    private $livreCaisse;

    /**
     * @var bool
     *
     * @ORM\Column(name="livret_societaire", type="boolean", nullable=true)
     */
    private $livretSocietaire;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="maj_liste_de_membres_choix_simple", referencedColumnName="id")
     */
    private $majListeDeMembres;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TblMaterielsBureaux")
     */
    private $materielDeBureau;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TblMoyenTransport")
     */
    private $materielRoulant;

    /**
     * @var int ? Nombre de membres ayant contracté un crédit
     *
     * @ORM\Column(name="membres_contracte_credit", type="integer", nullable=true)
     */
    private $membresContracteCredit;

    /**
     * @var float
     *
     * @ORM\Column(name="membres_credit_montant", type="float", nullable=true)
     */
    private $membresCreditMontant;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantEpargneMUSO;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantServicesPayesParMembres;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantServicesPayesParTiers;

    // ************************************************************* //
    // Partie 4. INDICATEURS ORGANISATIONNELS ET SOURCES DE REVENUS //

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $montantTotalCotisations;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbrContratsAvecActeursEco;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbrCreditAuxMembresParIMFOuBanque;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbrCreditAvecIMFOuBanque;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbrMUSO;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $nbrPersonnelAdmin;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $nbrPersonnelAppui;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $nbrPersonnelTechnique;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="nbr_pv_manquants_ag_choix_simple", referencedColumnName="id")
     */
    private $nbrPVManquantsAG;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="nbr_pv_manquants_ce_choix_simple", referencedColumnName="id")
     */
    private $nbrPVManquantsCE;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="nbr_pv_manquants_cs_choix_simple", referencedColumnName="id")
     */
    private $nbrPVManquantsCS;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="nbr_reunions_ag_choix_simple", referencedColumnName="id")
     *
     * // voir les contraintes comment réglé ?
     */
    private $nbrReunionsAG;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="nbr_reunions_ce_choix_simple", referencedColumnName="id")
     */
    private $nbrReunionsCE;

    /**
     * @var TblValeurChoixSimple
     *
     * @ORM\ManyToOne(targetEntity="TblValeurChoixSimple")
     * @ORM\JoinColumn(name="nbr_reunions_cs_choix_simple", referencedColumnName="id")
     */
    private $nbrReunionsCS;

    /**
     * @var \Doctrine\Common\Collections\Collection
     * @ORM\ManyToOne(targetEntity="TblNiveauxReconnaissance")
     */
    private $niveauxReconnaissance;

    /**
     * @var float
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombreMembresPayantsCotisation;

    /**
     * @var float
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nombreTotalMembres;

    /**
     * @ORM\ManyToOne(targetEntity="TblOrganisations")
     */
    private $organisation;

    /**
     * @var bool
     *
     * @ORM\Column(name="plan_action_annuel", type="boolean", nullable=true)
     */
    private $planActionAnnuel;

    // *********************************************** //
    // Partie 2. INDICATEURS DE GESTION ADMINISTRATIVE //
    // *********************************************** //

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $planStrategiquePluriannuel;

    /**
     * @var bool
     *
     * @ORM\Column(name="rapport_annuel_activites", type="boolean", nullable=true)
     */
    private $rapportAnnuelActivites;

    /**
     * @var bool
     *
     * @ORM\Column(name="rapports_financiere_annuel", type="boolean", nullable=true)
     */
    private $rapportsFinanciereAnnuel;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean")
     */
    private $rapportSuiviPlanStrategique;

    /**
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $ristourneParMembre;

    /**
     * @var TblServicesAuxMembres
     *
     * @ORM\ManyToOne(targetEntity="TblServicesAuxMembres")
     */
    private $servicesAuxMembres1;

    /**
     * @var TblServicesAuxMembres
     *
     * @ORM\ManyToOne(targetEntity="TblServicesAuxMembres")
     */
    private $servicesAuxMembres2;

    /**
     * @var TblServicesAuxMembres
     *
     * @ORM\ManyToOne(targetEntity="TblServicesAuxMembres")
     */
    private $servicesAuxMembres3;

    /**
     * @var TblServicesPayants
     *
     * @ORM\ManyToOne(targetEntity="TblServicesPayants")
     */
    private $servicesPayesParMembres1;

    /**
     * @var TblServicesPayants
     *
     * @ORM\ManyToOne(targetEntity="TblServicesPayants")
     */
    private $servicesPayesParMembres2;

    /**
     * @var TblServicesPayants
     *
     * @ORM\ManyToOne(targetEntity="TblServicesPayants")
     */
    private $servicesPayesParMembres3;

    /**
     * @var TblServicesPayants
     *
     * @ORM\ManyToOne(targetEntity="TblServicesPayants")
     */
    private $servicesPayesParTiers1;

    /**
     * @var TblServicesPayants
     *
     * @ORM\ManyToOne(targetEntity="TblServicesPayants")
     */
    private $servicesPayesParTiers2;

    /**
     * @var TblServicesPayants
     *
     * @ORM\ManyToOne(targetEntity="TblServicesPayants")
     */
    private $servicesPayesParTiers3;

    /**
     * @var int
     *
     * @ORM\Column(name="signatures_pour_sortie_fonds", type="integer")
     */
    private $signaturesPourSortieFonds;

    /**
     * @var float
     *
     * @ORM\Column(name="solde_annee_avant", type="float", nullable=true)
     */
    private $soldeAnneeAvant;

    /**
     * @var \Doctrine\Common\Collections\Collection
     *
     * @ORM\ManyToMany(targetEntity="TblUnitesTransformations")
     */
    private $uniteDeTransformation;

    public function __construct($user)
    {
        if (!$this->utilisateur) {
            $this->utilisateur = $user->getUsername();
        }

        $this->infrastructures = new ArrayCollection();
        $this->uniteDeTransformation = new ArrayCollection();
        $this->materielRoulant = new ArrayCollection();
        $this->materielDeBureau = new ArrayCollection();
        $this->equipementAgricole = new ArrayCollection();

        $this->besoinsFormation = new ArrayCollection();
        $this->besoinsServicesAuxMembres = new ArrayCollection();
    }

    /**
     * Add besoinsFormation.
     *
     * @return SuiviOrganisations
     */
    public function addBesoinsFormation(TblThemesFormations $f)
    {
        $this->besoinsFormation[] = $f;

        return $this;
    }

    /**
     * Add besoinsServicesAuxMembres.
     *
     * @return SuiviOrganisations
     */
    public function addBesoinsServicesAuxMembres(TblServicesAuxMembres $s)
    {
        $this->besoinsServicesAuxMembres[] = $s;

        return $this;
    }

    /**
     * Add equipementAgricole.
     *
     * @return SuiviOrganisations
     */
    public function addEquipementAgricole(TblEquipementsAgricoles $e)
    {
        $this->equipementAgricole[] = $e;

        return $this;
    }

    /**
     * Add infrastructures.
     *
     * @return SuiviOrganisations
     */
    public function addInfrastructures(TblInfrastructures $i)
    {
        $this->infrastructures[] = $i;

        return $this;
    }

    /**
     * Add materielDeBureau.
     *
     * @return SuiviOrganisations
     */
    public function addMaterielDeBureau(TblMaterielsBureaux $m)
    {
        $this->materielDeBureau[] = $m;

        return $this;
    }

    /**
     * Add materielRoulant.
     *
     * @return SuiviOrganisations
     */
    public function addMaterielRoulant(TblMoyenTransport $m)
    {
        $this->materielRoulant[] = $m;

        return $this;
    }

    /**
     * Add uniteDeTransformation.
     *
     * @return SuiviOrganisations
     */
    public function addUniteDeTransformation(TblUnitesTransformations $u)
    {
        $this->uniteDeTransformation[] = $u;

        return $this;
    }

    /**
     * Get affiliationFederation.
     *
     * @return bool
     */
    public function getAffiliationFederation(): ?bool
    {
        return $this->affiliationFederation;
    }

    /**
     * Get alternanceCENbr.
     *
     * @return TblValeurChoixSimple
     */
    public function getAlternanceCENbr()
    {
        return $this->alternanceCENbr;
    }

    /**
     * Get alternanceCSNbr.
     *
     * @return TblValeurChoixSimple
     */
    public function getAlternanceCSNbr()
    {
        return $this->alternanceCSNbr;
    }

    /**
     * Get annee.
     *
     * @return int
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get auditAnnuelDesComptes.
     *
     * @return bool
     */
    public function getAuditAnnuelDesComptes(): ?bool
    {
        return $this->auditAnnuelDesComptes;
    }

    /**
     * Get besoinsFormation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBesoinsFormation()
    {
        return $this->besoinsFormation;
    }

    /**
     * Get besoinsServicesAuxMembres.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBesoinsServicesAuxMembres()
    {
        return $this->besoinsServicesAuxMembres;
    }

    /**
     * Get budget.
     *
     * @return float
     */
    public function getBudget(): ?float
    {
        return $this->budget;
    }

    /**
     * Get carnetRecus.
     *
     * @return int
     */
    public function getCarnetRecus()
    {
        return $this->carnetRecus;
    }

    /**
     * Get chiffreAffaireParMembre.
     *
     * @return float
     */
    public function getChiffreAffaireParMembre(): ?float
    {
        return $this->chiffreAffaireParMembre;
    }

    /**
     * Get chiffreDAffaires.
     *
     * @return float
     */
    public function getChiffreDAffaires(): ?float
    {
        return $this->chiffreDAffaires;
    }

    /**
     * Get classement.
     *
     * @return TblValeurChoixSimple
     */
    public function getClassement()
    {
        return $this->classement;
    }

    /**
     * Get classementJustificatifs.
     *
     * @return int
     */
    public function getClassementJustificatifs()
    {
        return $this->classementJustificatifs;
    }

    /**
     * Get comptabilite.
     *
     * @return TblValeurChoixSimple
     */
    public function getComptabilite()
    {
        return $this->comptabilite;
    }

    /**
     * Get compteBancaire.
     *
     * @return int
     */
    public function getCompteBancaire()
    {
        return $this->compteBancaire;
    }

    /**
     * Get creditPourOrganisationMontant.
     *
     * @return string
     */
    public function getCreditPourOrganisationMontant()
    {
        return $this->creditPourOrganisationMontant;
    }

    /**
     * Get degrePriseEnChargeAG.
     *
     * @return TblValeurChoixSimple
     */
    public function getDegrePriseEnChargeAG(): ?TblValeurChoixSimple
    {
        return $this->degrePriseEnChargeAG;
    }

    /**
     * Get degrePriseEnChargeCE.
     *
     * @return TblValeurChoixSimple
     */
    public function getDegrePriseEnChargeCE(): ?TblValeurChoixSimple
    {
        return $this->degrePriseEnChargeCE;
    }

    /**
     * Get degrePriseEnChargeCS.
     *
     * @return TblValeurChoixSimple
     */
    public function getDegrePriseEnChargeCS(): ?TblValeurChoixSimple
    {
        return $this->degrePriseEnChargeCS;
    }

    /**
     * Get enregistrementCotisations.
     *
     * @return int
     */
    public function getEnregistrementCotisations()
    {
        return $this->enregistrementCotisations;
    }

    /**
     * Get equipementAgricole.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getEquipementAgricole()
    {
        return $this->equipementAgricole;
    }

    /**
     * Get evolutionNbrMembres.
     *
     * @return TblValeurChoixSimple
     */
    public function getEvolutionNbrMembres(): ?TblValeurChoixSimple
    {
        return $this->evolutionNbrMembres;
    }

    /**
     * Get existenceCE.
     *
     * @return bool
     */
    public function getExistenceCE()
    {
        return $this->existenceCE;
    }

    /**
     * Get existenceCS.
     *
     * @return bool
     */
    public function getExistenceCS()
    {
        return $this->existenceCS;
    }

    /**
     * Get existenceROI.
     *
     * @return bool
     */
    public function getExistenceROI()
    {
        return $this->existenceROI;
    }

    /**
     * Get existenceStatus.
     *
     * @return bool
     */
    public function getExistenceStatus()
    {
        return $this->existenceStatus;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get infrastructures.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getInfrastructures()
    {
        return $this->infrastructures;
    }

    /**
     * Get institutionBancaire.
     *
     * @return \App\Entity\TblInstitutionsFinancieres
     */
    public function getInstitutionBancaire()
    {
        return $this->institutionBancaire;
    }

    /**
     * Get institutionCreditMembres.
     *
     * @return \App\Entity\TblInstitutionsFinancieres
     */
    public function getInstitutionCreditMembres()
    {
        return $this->institutionCreditMembres;
    }

    /**
     * Get institutionFinancementCreditOrg.
     *
     * @return \App\Entity\TblInstitutionsFinancieres
     */
    public function getInstitutionFinancementCreditOrg()
    {
        return $this->institutionFinancementCreditOrg;
    }

    /**
     * Get livreCaisse.
     *
     * @return int
     */
    public function getLivreCaisse()
    {
        return $this->livreCaisse;
    }

    /**
     * Get livretSocietaire.
     *
     * @return int
     */
    public function getLivretSocietaire()
    {
        return $this->livretSocietaire;
    }

    /**
     * Get majListeDeMembres.
     *
     * @return TblValeurChoixSimple
     */
    public function getMajListeDeMembres(): ?TblValeurChoixSimple
    {
        return $this->majListeDeMembres;
    }

    /**
     * Get materielDeBureau.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterielDeBureau()
    {
        return $this->materielDeBureau;
    }

    /**
     * Get materielRoulant.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMaterielRoulant()
    {
        return $this->materielRoulant;
    }

    /**
     * Get membresContracteCredit.
     *
     * @return int
     */
    public function getMembresContracteCredit()
    {
        return $this->membresContracteCredit;
    }

    /**
     * Get membresCreditMontant.
     *
     * @return string
     */
    public function getMembresCreditMontant()
    {
        return $this->membresCreditMontant;
    }

    /**
     * Get montantEpargneMUSO.
     *
     * @return ?float
     */
    public function getMontantEpargneMUSO(): ?float
    {
        return $this->montantEpargneMUSO;
    }

    /**
     * Get montantServicesPayesParMembres.
     *
     * @return float
     */
    public function getMontantServicesPayesParMembres(): ?float
    {
        return $this->montantServicesPayesParMembres;
    }

    /**
     * Get montantServicesPayesParTiers.
     *
     * @return float
     */
    public function getMontantServicesPayesParTiers(): ?float
    {
        return $this->montantServicesPayesParTiers;
    }

    /**
     * Get montantTotalCotisations.
     *
     * @return float
     */
    public function getMontantTotalCotisations(): ?float
    {
        return $this->montantTotalCotisations;
    }

    /**
     * Get nbrContratsAvecActeursEco.
     *
     * @return int
     */
    public function getNbrContratsAvecActeursEco(): ?int
    {
        return $this->nbrContratsAvecActeursEco;
    }

    /**
     * Get nbrCreditAuxMembresParIMFOuBanque.
     *
     * @return int
     */
    public function getNbrCreditAuxMembresParIMFOuBanque(): ?int
    {
        return $this->nbrCreditAuxMembresParIMFOuBanque;
    }

    /**
     * Get nbrCreditAvecIMFOuBanque.
     *
     * @return int
     */
    public function getNbrCreditAvecIMFOuBanque(): ?int
    {
        return $this->nbrCreditAvecIMFOuBanque;
    }

    /**
     * Get nbrMUSO.
     *
     * @return int
     */
    public function getNbrMUSO(): ?int
    {
        return $this->nbrMUSO;
    }

    /**
     * Get nbrPersonnelAdmin.
     *
     * @return int
     */
    public function getNbrPersonnelAdmin()
    {
        return $this->nbrPersonnelAdmin;
    }

    /**
     * Get nbrPersonnelAppui.
     *
     * @return int
     */
    public function getNbrPersonnelAppui()
    {
        return $this->nbrPersonnelAppui;
    }

    /**
     * Get nbrPersonnelTechnique.
     *
     * @return int
     */
    public function getNbrPersonnelTechnique()
    {
        return $this->nbrPersonnelTechnique;
    }

    /**
     * Get nbrPVManquantsAG.
     *
     * @return TblValeurChoixSimple
     */
    public function getNbrPVManquantsAG(): ?TblValeurChoixSimple
    {
        return $this->nbrPVManquantsAG;
    }

    /**
     * Get nbrPVManquantsCE.
     *
     * @return TblValeurChoixSimple
     */
    public function getNbrPVManquantsCE(): ?TblValeurChoixSimple
    {
        return $this->nbrPVManquantsCE;
    }

    /**
     * Get nbrPVManquantsCS.
     *
     * @return TblValeurChoixSimple
     */
    public function getNbrPVManquantsCS(): ?TblValeurChoixSimple
    {
        return $this->nbrPVManquantsCS;
    }

    /**
     * Get nbrReunionsAG.
     *
     * @return TblValeurChoixSimple
     */
    public function getNbrReunionsAG()
    {
        return $this->nbrReunionsAG;
    }

    /**
     * Get nbrReunionsCE.
     *
     * @return int
     */
    public function getNbrReunionsCE()
    {
        return $this->nbrReunionsCE;
    }

    /**
     * Get nbrReunionsCS.
     *
     * @return int
     */
    public function getNbrReunionsCS()
    {
        return $this->nbrReunionsCS;
    }

    /**
     * Get niveauxReconnaissance.
     *
     * @return TblNiveauxReconnaissance
     */
    public function getNiveauxReconnaissance()
    {
        return $this->niveauxReconnaissance;
    }

    /**
     * Get nombreMembresPayantsCotisation.
     *
     * @return int
     */
    public function getNombreMembresPayantsCotisation(): ?int
    {
        return $this->nombreMembresPayantsCotisation;
    }

    /**
     * Get nombreTotalMembres.
     *
     * @return TblValeurChoixSimple
     */
    public function getNombreTotalMembres(): ?int
    {
        return $this->nombreTotalMembres;
    }

    /**
     * Get organisation.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Get planActionAnnuel.
     *
     * @return int
     */
    public function getPlanActionAnnuel()
    {
        return $this->planActionAnnuel;
    }

    /**
     * Get planStrategiquePluriannuel.
     *
     * @return bool
     */
    public function getPlanStrategiquePluriannuel()
    {
        return $this->planStrategiquePluriannuel;
    }

    /**
     * Get rapportAnnuelActivites.
     *
     * @return int
     */
    public function getRapportAnnuelActivites()
    {
        return $this->rapportAnnuelActivites;
    }

    /**
     * Get rapportsFinanciereAnnuel.
     *
     * @return bolean
     */
    public function getRapportsFinanciereAnnuel()
    {
        return $this->rapportsFinanciereAnnuel;
    }

    /**
     * Get rapportSuiviPlanStrategique.
     *
     * @return bool
     */
    public function getRapportSuiviPlanStrategique()
    {
        return $this->rapportSuiviPlanStrategique;
    }

    /**
     * Get ristourneParMembre.
     *
     * @return float
     */
    public function getRistourneParMembre(): ?float
    {
        return $this->ristourneParMembre;
    }

    /**
     * Get servicesAuxMembres1.
     *
     * @return TblServicesAuxMembres
     */
    public function getServicesAuxMembres1()
    {
        return $this->servicesAuxMembres1;
    }

    /**
     * Get servicesAuxMembres2.
     *
     * @return TblServicesAuxMembres
     */
    public function getServicesAuxMembres2()
    {
        return $this->servicesAuxMembres2;
    }

    /**
     * Get servicesAuxMembres3.
     *
     * @return TblServicesAuxMembres
     */
    public function getServicesAuxMembres3()
    {
        return $this->servicesAuxMembres3;
    }

    /**
     * Get servicesPayesParMembres1.
     *
     * @return TblServicesPayants
     */
    public function getServicesPayesParMembres1(): ?TblServicesPayants
    {
        return $this->servicesPayesParMembres1;
    }

    /**
     * Get servicesPayesParMembres2.
     *
     * @return TblServicesPayants
     */
    public function getServicesPayesParMembres2(): ?TblServicesPayants
    {
        return $this->servicesPayesParMembres2;
    }

    /**
     * Get servicesPayesParMembres3.
     *
     * @return TblServicesPayants
     */
    public function getServicesPayesParMembres3(): ?TblServicesPayants
    {
        return $this->servicesPayesParMembres3;
    }

    /**
     * Get servicesPayesParTiers1.
     *
     * @return TblServicesPayants
     */
    public function getServicesPayesParTiers1(): ?TblServicesPayants
    {
        return $this->servicesPayesParTiers1;
    }

    /**
     * Get servicesPayesParTiers2.
     *
     * @return TblServicesPayants
     */
    public function getServicesPayesParTiers2(): ?TblServicesPayants
    {
        return $this->servicesPayesParTiers2;
    }

    /**
     * Get servicesPayesParTiers3.
     *
     * @return TblServicesPayants
     */
    public function getServicesPayesParTiers3(): ?TblServicesPayants
    {
        return $this->servicesPayesParTiers3;
    }

    /**
     * Get signaturesPourSortieFonds.
     *
     * @return int
     */
    public function getSignaturesPourSortieFonds()
    {
        return $this->signaturesPourSortieFonds;
    }

    /**
     * Get soldeAnneeAvant.
     *
     * @return string
     */
    public function getSoldeAnneeAvant()
    {
        return $this->soldeAnneeAvant;
    }

    /**
     * Get uniteDeTransformation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUniteDeTransformation()
    {
        return $this->uniteDeTransformation;
    }

    /**
     * Remove besoinsFormation.
     */
    public function removeBesoinsFormation(TblThemesFormations $f)
    {
        $this->besoinsFormation->removeElement($f);
    }

    /**
     * Remove besoinsServicesAuxMembres.
     */
    public function removeBesoinsServicesAuxMembres(TblServicesAuxMembres $s)
    {
        $this->besoinsServicesAuxMembres->removeElement($s);
    }

    /**
     * Remove equipementAgricole.
     */
    public function removeEquipementAgricole(TblEquipementsAgricoles $e)
    {
        $this->equipementAgricole->removeElement($e);
    }

    /**
     * Remove infrastructures.
     */
    public function removeInfrastructures(TblInfrastructures $i)
    {
        $this->infrastructures->removeElement($i);
    }

    /**
     * Remove materielDeBureau.
     */
    public function removeMaterielDeBureau(TblMaterielsBureaux $m)
    {
        $this->materielDeBureau->removeElement($m);
    }

    /**
     * Remove materielRoulant.
     */
    public function removeMaterielRoulant(TblMoyenTransport $m)
    {
        $this->materielRoulant->removeElement($m);
    }

    /**
     * Remove uniteDeTransformation.
     */
    public function removeUniteDeTransformation(TblUnitesTransformations $u)
    {
        $this->uniteDeTransformation->removeElement($u);
    }

    /**
     * Set affiliationFederation.
     *
     * @return SuiviOrganisations
     */
    public function setAffiliationFederation(bool $affiliationFederation)
    {
        $this->affiliationFederation = $affiliationFederation;

        return $this;
    }

    /**
     * Set alternanceCENbr.
     *
     * @param TblValeurChoixSimple $alternanceCENbr
     *
     * @return SuiviOrganisations
     */
    public function setAlternanceCENbr($alternanceCENbr)
    {
        $this->alternanceCENbr = $alternanceCENbr;

        return $this;
    }

    /**
     * Set alternanceCSNbr.
     *
     * @param TblValeurChoixSimple $alternanceCSNbr
     *
     * @return SuiviOrganisations
     */
    public function setAlternanceCSNbr($alternanceCSNbr)
    {
        $this->alternanceCSNbr = $alternanceCSNbr;

        return $this;
    }

    /**
     * Set annee.
     *
     * @param int $annee
     *
     * @return SuiviOrganisations
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set auditAnnuelDesComptes.
     *
     * @return SuiviOrganisations
     */
    public function setAuditAnnuelDesComptes(bool $auditAnnuelDesComptes)
    {
        $this->auditAnnuelDesComptes = $auditAnnuelDesComptes;

        return $this;
    }

    /**
     * Set budget.
     *
     * @return SuiviOrganisations
     */
    public function setBudget(float $budget)
    {
        $this->budget = $budget;

        return $this;
    }

    /**
     * Set carnetRecus.
     *
     * @param int $carnetRecus
     *
     * @return SuiviOrganisations
     */
    public function setCarnetRecus($carnetRecus)
    {
        $this->carnetRecus = $carnetRecus;

        return $this;
    }

    /**
     * Set chiffreAffaireParMembre.
     *
     * @return SuiviOrganisations
     */
    public function setChiffreAffaireParMembre(float $chiffreAffaireParMembre)
    {
        $this->chiffreAffaireParMembre = $chiffreAffaireParMembre;

        return $this;
    }

    /**
     * Set chiffreDAffaires.
     *
     * @return SuiviOrganisations
     */
    public function setChiffreDAffaires(float $chiffreDAffaires)
    {
        $this->chiffreDAffaires = $chiffreDAffaires;

        return $this;
    }

    /**
     * Set classement.
     *
     * @return SuiviOrganisations
     */
    public function setClassement(TblValeurChoixSimple $c)
    {
        $this->classement = $c;

        return $this;
    }

    /**
     * Set classementJustificatifs.
     *
     * @param int $classementJustificatifs
     *
     * @return SuiviOrganisations
     */
    public function setClassementJustificatifs($classementJustificatifs)
    {
        $this->classementJustificatifs = $classementJustificatifs;

        return $this;
    }

    /**
     * Set comptabilite.
     *
     * @return SuiviOrganisations
     */
    public function setComptabilite(TblValeurChoixSimple $c)
    {
        $this->comptabilite = $c;

        return $this;
    }

    /**
     * Set compteBancaire.
     *
     * @param int $compteBancaire
     *
     * @return SuiviOrganisations
     */
    public function setCompteBancaire($compteBancaire)
    {
        $this->compteBancaire = $compteBancaire;

        return $this;
    }

    /**
     * Set creditPourOrganisationMontant.
     *
     * @param string $creditPourOrganisationMontant
     *
     * @return SuiviOrganisations
     */
    public function setCreditPourOrganisationMontant($creditPourOrganisationMontant)
    {
        $this->creditPourOrganisationMontant = $creditPourOrganisationMontant;

        return $this;
    }

    /**
     * Set degrePriseEnChargeAG.
     *
     * @return SuiviOrganisations
     */
    public function setDegrePriseEnChargeAG(TblValeurChoixSimple $degrePriseEnChargeAG)
    {
        $this->degrePriseEnChargeAG = $degrePriseEnChargeAG;

        return $this;
    }

    /**
     * Set degrePriseEnChargeCE.
     *
     * @return SuiviOrganisations
     */
    public function setDegrePriseEnChargeCE(TblValeurChoixSimple $degrePriseEnChargeCE)
    {
        $this->degrePriseEnChargeCE = $degrePriseEnChargeCE;

        return $this;
    }

    /**
     * Set degrePriseEnChargeCS.
     *
     * @param int $degrePriseEnChargeCS
     *
     * @return SuiviOrganisations
     */
    public function setDegrePriseEnChargeCS(TblValeurChoixSimple $degrePriseEnChargeCS)
    {
        $this->degrePriseEnChargeCS = $degrePriseEnChargeCS;

        return $this;
    }

    /**
     * Set enregistrementCotisations.
     *
     * @param int $enregistrementCotisations
     *
     * @return SuiviOrganisations
     */
    public function setEnregistrementCotisations($enregistrementCotisations)
    {
        $this->enregistrementCotisations = $enregistrementCotisations;

        return $this;
    }

    /**
     * Set evolutionNbrMembres.
     *
     * @return SuiviOrganisations
     */
    public function setEvolutionNbrMembres(TblValeurChoixSimple $evolutionNbrMembres)
    {
        $this->evolutionNbrMembres = $evolutionNbrMembres;

        return $this;
    }

    /**
     * Set existenceCE.
     *
     * @param bool $existenceCE
     *
     * @return SuiviOrganisations
     */
    public function setExistenceCE($existenceCE)
    {
        $this->existenceCE = $existenceCE;

        return $this;
    }

    /**
     * Set existenceCS.
     *
     * @param bool $existenceCS
     *
     * @return SuiviOrganisations
     */
    public function setExistenceCS($existenceCS)
    {
        $this->existenceCS = $existenceCS;

        return $this;
    }

    /**
     * Set existenceROI.
     *
     * @param bool $existenceROI
     *
     * @return SuiviOrganisations
     */
    public function setExistenceROI($existenceROI)
    {
        $this->existenceROI = $existenceROI;

        return $this;
    }

    /**
     * Set existenceStatus.
     *
     * @param bool $existenceStatus
     *
     * @return SuiviOrganisations
     */
    public function setExistenceStatus($existenceStatus)
    {
        $this->existenceStatus = $existenceStatus;

        return $this;
    }

    /**
     * Set institutionBancaire.
     *
     * @param \App\Entity\TblInstitutionsFinancieres $institutionBancaire
     *
     * @return SuiviOrganisations
     */
    public function setInstitutionBancaire(?TblInstitutionsFinancieres $institutionBancaire = null)
    {
        $this->institutionBancaire = $institutionBancaire;

        return $this;
    }

    /**
     * Set institutionCreditMembres.
     *
     * @param \App\Entity\TblInstitutionsFinancieres $institutionCreditMembres
     *
     * @return SuiviOrganisations
     */
    public function setInstitutionCreditMembres(?TblInstitutionsFinancieres $institutionCreditMembres = null)
    {
        $this->institutionCreditMembres = $institutionCreditMembres;

        return $this;
    }

    /**
     * Set institutionFinancementCreditOrg.
     *
     * @param \App\Entity\TblInstitutionsFinancieres $institutionFinancementCreditOrg
     *
     * @return SuiviOrganisations
     */
    public function setInstitutionFinancementCreditOrg(?TblInstitutionsFinancieres $institutionFinancementCreditOrg = null)
    {
        $this->institutionFinancementCreditOrg = $institutionFinancementCreditOrg;

        return $this;
    }

    /**
     * Set livreCaisse.
     *
     * @param int $livreCaisse
     *
     * @return SuiviOrganisations
     */
    public function setLivreCaisse($livreCaisse)
    {
        $this->livreCaisse = $livreCaisse;

        return $this;
    }

    /**
     * Set livretSocietaire.
     *
     * @param int $livretSocietaire
     *
     * @return SuiviOrganisations
     */
    public function setLivretSocietaire($livretSocietaire)
    {
        $this->livretSocietaire = $livretSocietaire;

        return $this;
    }

    /**
     * Set majListeDeMembres.
     *
     * @return SuiviOrganisations
     */
    public function setMajListeDeMembres(TblValeurChoixSimple $c)
    {
        $this->majListeDeMembres = $c;

        return $this;
    }

    /**
     * Set membresContracteCredit.
     *
     * @param int $membresContracteCredit
     *
     * @return SuiviOrganisations
     */
    public function setMembresContracteCredit($membresContracteCredit)
    {
        $this->membresContracteCredit = $membresContracteCredit;

        return $this;
    }

    /**
     * Set membresCreditMontant.
     *
     * @param string $membresCreditMontant
     *
     * @return SuiviOrganisations
     */
    public function setMembresCreditMontant($membresCreditMontant)
    {
        $this->membresCreditMontant = $membresCreditMontant;

        return $this;
    }

    /**
     * Set montantEpargneMUSO.
     *
     * @param float $montantEpargneMUSO
     *
     * @return SuiviOrganisations
     */
    public function setMontantEpargneMUSO(?float $montantEpargneMUSO)
    {
        $this->montantEpargneMUSO = $montantEpargneMUSO;

        return $this;
    }

    /**
     * set montantServicesPayesParMembres.
     *
     * @return SuiviOrganisations
     */
    public function setMontantServicesPayesParMembres(float $montantServicesPayesParMembres)
    {
        $this->montantServicesPayesParMembres = $montantServicesPayesParMembres;

        return $this;
    }

    /**
     * set montantServicesPayesParTiers.
     *
     * @return SuiviOrganisations
     */
    public function setMontantServicesPayesParTiers(float $montantServicesPayesParTiers)
    {
        $this->montantServicesPayesParTiers = $montantServicesPayesParTiers;

        return $this;
    }

    /**
     * set montantTotalCotisations.
     *
     * @return SuiviOrganisations
     */
    public function setMontantTotalCotisations(float $montantTotalCotisations)
    {
        $this->montantTotalCotisations = $montantTotalCotisations;

        return $this;
    }

    /**
     * Set nbrContratsAvecActeursEco.
     *
     * @return SuiviOrganisations
     */
    public function setNbrContratsAvecActeursEco(int $nbrContratsAvecActeursEco)
    {
        $this->nbrContratsAvecActeursEco = $nbrContratsAvecActeursEco;

        return $this;
    }

    /**
     * Set nbrCreditAuxMembresParIMFOuBanque.
     *
     * @return SuiviOrganisations
     */
    public function setNbrCreditAuxMembresParIMFOuBanque(int $nbr)
    {
        $this->nbrCreditAuxMembresParIMFOuBanque = $nbr;

        return $this;
    }

    /**
     * Set nbrCreditAvecIMFOuBanque.
     *
     * @return SuiviOrganisations
     */
    public function setNbrCreditAvecIMFOuBanque(int $nbrCreditAvecIMFOuBanque)
    {
        $this->nbrCreditAvecIMFOuBanque = $nbrCreditAvecIMFOuBanque;

        return $this;
    }

    /**
     * Set nbrMUSO.
     *
     * @param int $nbrMUSO
     *
     * @return SuiviOrganisations
     */
    public function setNbrMUSO(?int $nbrMUSO)
    {
        $this->nbrMUSO = $nbrMUSO;

        return $this;
    }

    /**
     * Set nbrPersonnelAdmin.
     *
     * @param int $nbrPersonnelAdmin
     *
     * @return SuiviOrganisations
     */
    public function setNbrPersonnelAdmin($nbrPersonnelAdmin)
    {
        $this->nbrPersonnelAdmin = $nbrPersonnelAdmin;

        return $this;
    }

    /**
     * Set nbrPersonnelAppui.
     *
     * @param int $nbrPersonnelAppui
     *
     * @return SuiviOrganisations
     */
    public function setNbrPersonnelAppui($nbrPersonnelAppui)
    {
        $this->nbrPersonnelAppui = $nbrPersonnelAppui;

        return $this;
    }

    /**
     * Set nbrPersonnelTechnique.
     *
     * @param int $nbrPersonnelTechnique
     *
     * @return SuiviOrganisations
     */
    public function setNbrPersonnelTechnique($nbrPersonnelTechnique)
    {
        $this->nbrPersonnelTechnique = $nbrPersonnelTechnique;

        return $this;
    }

    /**
     * Set nbrPVManquantsAG.
     *
     * @return SuiviOrganisations
     */
    public function setNbrPVManquantsAG(TblValeurChoixSimple $c)
    {
        $this->nbrPVManquantsAG = $c;

        return $this;
    }

    /**
     * Set nbrPVManquantsCE.
     *
     * @return SuiviOrganisations
     */
    public function setNbrPVManquantsCE(TblValeurChoixSimple $c)
    {
        $this->nbrPVManquantsCE = $c;

        return $this;
    }

    /**
     * Set nbrPVManquantsCS.
     *
     * @return SuiviOrganisations
     */
    public function setNbrPVManquantsCS(TblValeurChoixSimple $c)
    {
        $this->nbrPVManquantsCS = $c;

        return $this;
    }

    /**
     * Set nbrReunionsAG.
     *
     * @param int $nbrReunionsAG
     *
     * @return SuiviOrganisations
     */
    public function setNbrReunionsAG($nbrReunionsAG)
    {
        $this->nbrReunionsAG = $nbrReunionsAG;

        return $this;
    }

    /**
     * Set nbrReunionsCE.
     *
     * @param int $nbrReunionsCE
     *
     * @return SuiviOrganisations
     */
    public function setNbrReunionsCE($nbrReunionsCE)
    {
        $this->nbrReunionsCE = $nbrReunionsCE;

        return $this;
    }

    /**
     * Set nbrReunionsCS.
     *
     * @param int $nbrReunionsCS
     *
     * @return SuiviOrganisations
     */
    public function setNbrReunionsCS($nbrReunionsCS)
    {
        $this->nbrReunionsCS = $nbrReunionsCS;

        return $this;
    }

    /**
     * Set niveauxReconnaissance.
     *
     * @param mixed $nivRec
     */
    public function setNiveauxReconnaissance($nivRec)
    {
        $this->niveauxReconnaissance = $nivRec;

        return $this;
    }

    /**
     * Set nombreMembresPayantsCotisation.
     *
     * @return SuiviOrganisations
     */
    public function setNombreMembresPayantsCotisation(int $nombreMembresPayantsCotisation)
    {
        $this->nombreMembresPayantsCotisation = $nombreMembresPayantsCotisation;

        return $this;
    }

    /**
     * Set nombreTotalMembres.
     *
     * @param TblValeurChoixSimple $nombreTotalMembres
     *
     * @return SuiviOrganisations
     */
    public function setNombreTotalMembres(int $nombreTotalMembres)
    {
        $this->nombreTotalMembres = $nombreTotalMembres;

        return $this;
    }

    /**
     * Set organisation.
     *
     * @param \App\Entity\TblOrganisations $organisation
     *
     * @return SuiviOrganisations
     */
    public function setOrganisation(?TblOrganisations $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Set planActionAnnuel.
     *
     * @param int $planActionAnnuel
     *
     * @return SuiviOrganisations
     */
    public function setPlanActionAnnuel($planActionAnnuel)
    {
        $this->planActionAnnuel = $planActionAnnuel;

        return $this;
    }

    /**
     * Set planStrategiquePluriannuel.
     *
     * @param bool $planStrategiquePluriannuel
     *
     * @return SuiviOrganisations
     */
    public function setPlanStrategiquePluriannuel($planStrategiquePluriannuel)
    {
        $this->planStrategiquePluriannuel = $planStrategiquePluriannuel;

        return $this;
    }

    /**
     * Set rapportAnnuelActivites.
     *
     * @param int $rapportAnnuelActivites
     *
     * @return SuiviOrganisations
     */
    public function setRapportAnnuelActivites($rapportAnnuelActivites)
    {
        $this->rapportAnnuelActivites = $rapportAnnuelActivites;

        return $this;
    }

    /**
     * Set rapportsFinanciereAnnuel.
     *
     * @param bool $rapportsFinanciereAnnuel
     *
     * @return SuiviOrganisations
     */
    public function setRapportsFinanciereAnnuel($rapportsFinanciereAnnuel)
    {
        $this->rapportsFinanciereAnnuel = $rapportsFinanciereAnnuel;

        return $this;
    }

    /**
     * Set rapportSuiviPlanStrategique.
     *
     * @param bool $rapportSuiviPlanStrategique
     *
     * @return SuiviOrganisations
     */
    public function setRapportSuiviPlanStrategique($rapportSuiviPlanStrategique)
    {
        $this->rapportSuiviPlanStrategique = $rapportSuiviPlanStrategique;

        return $this;
    }

    /**
     * Set ristourneParMembre.
     *
     * @return SuiviOrganisations
     */
    public function setRistourneParMembre(float $ristourneParMembre)
    {
        $this->ristourneParMembre = $ristourneParMembre;

        return $this;
    }

    /**
     * set servicesAuxMembres1.
     *
     * @return SuiviOrganisations
     */
    public function setServicesAuxMembres1(TblServicesAuxMembres $s)
    {
        $this->servicesAuxMembres1 = $s;

        return $this;
    }

    /**
     * set servicesAuxMembres2.
     *
     * @return SuiviOrganisations
     */
    public function setServicesAuxMembres2(TblServicesAuxMembres $s)
    {
        $this->servicesAuxMembres2 = $s;

        return $this;
    }

    /**
     * set servicesAuxMembres3.
     *
     * @return SuiviOrganisations
     */
    public function setServicesAuxMembres3(TblServicesAuxMembres $s)
    {
        $this->servicesAuxMembres3 = $s;

        return $this;
    }

    /**
     * set servicesPayesParMembres1.
     *
     * @return SuiviOrganisations
     */
    public function setServicesPayesParMembres1(TblServicesPayants $s)
    {
        $this->servicesPayesParMembres1 = $s;

        return $this;
    }

    /**
     * set servicesPayesParMembres2.
     *
     * @return SuiviOrganisations
     */
    public function setServicesPayesParMembres2(TblServicesPayants $s)
    {
        $this->servicesPayesParMembres2 = $s;

        return $this;
    }

    /**
     * set servicesPayesParMembres3.
     *
     * @return SuiviOrganisations
     */
    public function setServicesPayesParMembres3(TblServicesPayants $s)
    {
        $this->servicesPayesParMembres3 = $s;

        return $this;
    }

    /**
     * set servicesPayesParTiers1.
     *
     * @return SuiviOrganisations
     */
    public function setServicesPayesParTiers1(TblServicesPayants $s)
    {
        $this->servicesPayesParTiers1 = $s;

        return $this;
    }

    /**
     * set servicesPayesParTiers2.
     *
     * @return SuiviOrganisations
     */
    public function setServicesPayesParTiers2(TblServicesPayants $s)
    {
        $this->servicesPayesParTiers2 = $s;

        return $this;
    }

    /**
     * set servicesPayesParTiers3.
     *
     * @return SuiviOrganisations
     */
    public function setServicesPayesParTiers3(TblServicesPayants $s)
    {
        $this->servicesPayesParTiers3 = $s;

        return $this;
    }

    /**
     * Set signaturesPourSortieFonds.
     *
     * @param int $signaturesPourSortieFonds
     *
     * @return SuiviOrganisations
     */
    public function setSignaturesPourSortieFonds($signaturesPourSortieFonds)
    {
        $this->signaturesPourSortieFonds = $signaturesPourSortieFonds;

        return $this;
    }

    /**
     * Set soldeAnneeAvant.
     *
     * @param string $soldeAnneeAvant
     *
     * @return SuiviOrganisations
     */
    public function setSoldeAnneeAvant($soldeAnneeAvant)
    {
        $this->soldeAnneeAvant = $soldeAnneeAvant;

        return $this;
    }
}
