<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * Filieres.
 *
 * @ORM\Table(name="filieres")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Filieres extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @ORM\ManyToOne(targetEntity="TblCultures")
     */
    private $filierePrincipale;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="quantite_produit", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $quantiteProduit;

    /**
     * @var string
     *
     * @ORM\Column(name="surface", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $surface;

    /**
     * @var string
     *
     * @ORM\Column(name="unite_calcul", type="string", length=20, nullable=true)
     */
    private $uniteCalcul;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get filierePrincipale.
     *
     * @return \App\Entity\TblCultures
     */
    public function getFilierePrincipale()
    {
        return $this->filierePrincipale;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get quantiteProduit.
     *
     * @return string
     */
    public function getQuantiteProduit()
    {
        return $this->quantiteProduit;
    }

    /**
     * Get surface.
     *
     * @return string
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Get uniteCalcul.
     *
     * @return string
     */
    public function getUniteCalcul()
    {
        return $this->uniteCalcul;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return Filieres
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return Filieres
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set filierePrincipale.
     *
     * @param \App\Entity\TblCultures $filierePrincipale
     *
     * @return Filieres
     */
    public function setFilierePrincipale(?TblCultures $filierePrincipale = null)
    {
        $this->filierePrincipale = $filierePrincipale;

        return $this;
    }

    /**
     * Set quantiteProduit.
     *
     * @param string $quantiteProduit
     *
     * @return Filieres
     */
    public function setQuantiteProduit($quantiteProduit)
    {
        $this->quantiteProduit = $quantiteProduit;

        return $this;
    }

    /**
     * Set surface.
     *
     * @param string $surface
     *
     * @return Filieres
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Set uniteCalcul.
     *
     * @param string $uniteCalcul
     *
     * @return Filieres
     */
    public function setUniteCalcul($uniteCalcul)
    {
        $this->uniteCalcul = $uniteCalcul;

        return $this;
    }
}
