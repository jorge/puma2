<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use stdClass;

/**
 * ParcelleOperationCulturale.
 *
 * @ORM\Table(name="parcelle_operation_culturale")
 * @ORM\Entity
 */
class ParcelleOperationCulturale extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToMany(targetEntity="TblCultures")
     */
    private $cultures;

    /**
     * @var DateTime
     *
     * Date de l'opération
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblOperationCulturaleIntrant")
     */
    private $intrant;

    /**
     * @var int
     *
     * @ORM\Column(name="mainOeuvreFamiliale", type="smallint", nullable=true)
     */
    private $mainOeuvreFamiliale;

    /**
     * @var int
     *
     * Quantite de main d'oeuvre salariée (en Homme/Jour)
     * @ORM\Column(name="mainOeuvreSalarie", type="smallint", nullable=true)
     */
    private $mainOeuvreSalarie;

    /**
     * @var ParcelleHistoriques
     *
     * Historique de la parcelle (annee + saison). TODO renommer en Saison ?
     * @ORM\ManyToOne(targetEntity="App\Entity\ParcelleHistoriques", inversedBy="operations")
     */
    private $parcelleHistorique;

    /**
     * @ORM\ManyToOne(targetEntity="TblProduits")
     */
    private $produitPrimaire;

    /**
     * @var int
     *
     * Quantité de produit provenant de l'exploitation (interne) utilisée
     * ou récoltée pour l'exploitation (usage interne)
     * @ORM\Column(name="quantite_recolte_autoconsommation", type="float", nullable=true)
     */
    private $quantiteRecolteAutoconsommation;

    /**
     * Quantite récoltée (récolte primaire en kg).
     *
     * @var int
     *
     * * TODO ne plus utiliser
     *
     * @ORM\Column(name="quantite_recoltee_vente", type="float", nullable=true)
     */
    private $quantiteRecolteeVente;

    /**
     * @var int
     *
     * Quantité de produit provenant de l'extérieur de l'exploitation utilisée
     * ou récoltée pour l'extérieur
     * @ORM\Column(name="quantite_utilise_achete", type="float", nullable=true)
     */
    private $quantiteUtiliseAchete;

    /**
     * @var int
     *
     * Quantité de produit utilisée
     *
     * TODO ne plus utiliser
     *
     * @ORM\Column(name="quantite_utilisee_autoconsommation", type="float", nullable=true)
     */
    private $quantiteUtiliseeAutoconsommation;

    /**
     * @var TblOperationCulturaleType
     *
     * Operation effectuée
     * @ORM\ManyToOne(targetEntity="TblOperationCulturaleType")
     */
    private $type;

    public function __construct($user)
    {
        parent::__construct($user);
        $this->cultures = new ArrayCollection();
    }

    /**
     * Add culture.
     *
     * @param \App\Entity\TblCultures $culture
     *
     * @return ParcelleOperationCulturale
     */
    public function addCulture(TblCultures $culture)
    {
        $this->cultures[] = $culture;

        return $this;
    }

    /**
     * Get cultures.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCultures()
    {
        return $this->cultures;
    }

    /**
     * Get date.
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get intrant.
     *
     * @return \App\Entity\TblOperationCulturaleIntrant
     */
    public function getIntrant()
    {
        return $this->intrant;
    }

    /**
     * Get mainOeuvreFamiliale.
     *
     * @return int
     */
    public function getMainOeuvreFamiliale()
    {
        return $this->mainOeuvreFamiliale;
    }

    /**
     * Get mainOeuvreSalarie.
     *
     * @return int
     */
    public function getMainOeuvreSalarie()
    {
        return $this->mainOeuvreSalarie;
    }

    /**
     * Get parcelleHistorique.
     *
     * @return stdClass
     */
    public function getParcelleHistorique()
    {
        return $this->parcelleHistorique;
    }

    /**
     * Get produitPrimaire.
     *
     * @return \App\Entity\TblProduits
     */
    public function getProduitPrimaire()
    {
        return $this->produitPrimaire;
    }

    /**
     * Get quantiteRecolteAutoconsommation.
     *
     * @return int
     */
    public function getQuantiteRecolteAutoconsommation()
    {
        return $this->quantiteRecolteAutoconsommation;
    }

    /**
     * Get quantiteRecolteeVente.
     *
     * @return int
     */
    public function getQuantiteRecolteeVente()
    {
        return $this->quantiteRecolteeVente;
    }

    /**
     * Get quantiteUtiliseAchete.
     *
     * @return int
     */
    public function getQuantiteUtiliseAchete()
    {
        return $this->quantiteUtiliseAchete;
    }

    /**
     * Get quantiteUtiliseeAutoconsommation.
     *
     * @return int
     */
    public function getQuantiteUtiliseeAutoconsommation()
    {
        return $this->quantiteUtiliseeAutoconsommation;
    }

    /**
     * Get type.
     *
     * @return stdClass
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Remove culture.
     *
     * @param \App\Entity\TblCultures $culture
     */
    public function removeCulture(TblCultures $culture)
    {
        $this->cultures->removeElement($culture);
    }

    /**
     * Set date.
     *
     * @param DateTime $date
     *
     * @return ParcelleOperationCulturale
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set intrant.
     *
     * @param \App\Entity\TblOperationCulturaleIntrant $intrant
     *
     * @return ParcelleOperationCulturale
     */
    public function setIntrant($intrant)
    {
        $this->intrant = $intrant;

        return $this;
    }

    /**
     * Set mainOeuvreFamiliale.
     *
     * @param int $mainOeuvreFamiliale
     *
     * @return ParcelleOperationCulturale
     */
    public function setMainOeuvreFamiliale($mainOeuvreFamiliale)
    {
        $this->mainOeuvreFamiliale = $mainOeuvreFamiliale;

        return $this;
    }

    /**
     * Set mainOeuvreSalarie.
     *
     * @param int $mainOeuvreSalarie
     *
     * @return ParcelleOperationCulturale
     */
    public function setMainOeuvreSalarie($mainOeuvreSalarie)
    {
        $this->mainOeuvreSalarie = $mainOeuvreSalarie;

        return $this;
    }

    /**
     * Set parcelleHistorique.
     *
     * @param ParcelleHistorique $parcelleHistorique
     *
     * @return ParcelleOperationCulturale
     */
    public function setParcelleHistorique($parcelleHistorique)
    {
        $this->parcelleHistorique = $parcelleHistorique;

        return $this;
    }

    /**
     * Set produitPrimaire.
     *
     * @param \App\Entity\TblProduits $produit
     *
     * @return ParcelleOperationCulturale
     */
    public function setProduitPrimaire(?TblProduits $produit = null)
    {
        $this->produitPrimaire = $produit;

        return $this;
    }

    /**
     * Set quantiteRecolteAutoconsommation.
     *
     * @param int $quantiteRecolteAutoconsommation
     *
     * @return ParcelleOperationCulturale
     */
    public function setQuantiteRecolteAutoconsommation($quantiteRecolteAutoconsommation)
    {
        $this->quantiteRecolteAutoconsommation = $quantiteRecolteAutoconsommation;

        return $this;
    }

    /**
     * Set quantiteRecolteeVente.
     *
     * @param int $quantiteRecolteeVente
     *
     * @return ParcelleOperationCulturale
     */
    public function setQuantiteRecolteeVente($quantiteRecolteeVente)
    {
        $this->quantiteRecolteeVente = $quantiteRecolteeVente;

        return $this;
    }

    /**
     * Set quantiteUtiliseAchete.
     *
     * @param int $quantiteUtiliseAchete
     *
     * @return ParcelleOperationCulturale
     */
    public function setquantiteUtiliseAchete($quantiteUtiliseAchete)
    {
        $this->quantiteUtiliseAchete = $quantiteUtiliseAchete;

        return $this;
    }

    /**
     * Set quantiteUtiliseeAutoconsommation.
     *
     * @param int $quantiteUtiliseeAutoconsommation
     *
     * @return ParcelleOperationCulturale
     */
    public function setQuantiteUtiliseeAutoconsommation($quantiteUtiliseeAutoconsommation)
    {
        $this->quantiteUtiliseeAutoconsommation = $quantiteUtiliseeAutoconsommation;

        return $this;
    }

    /**
     * Set type.
     *
     * @param stdClass $type
     *
     * @return ParcelleOperationCulturale
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
