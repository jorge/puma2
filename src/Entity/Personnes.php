<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use App\Validator\Constraint\UniqueChefFamille;
use App\Validator\Constraint\UniqueRepondant;
use App\Validator\Constraint\VerifOnlyAbc;
use function count;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Exception;
use Intervention\Image\ImageManagerStatic as Image;
use Symfony\Component\HttpFoundation\File\Exception\FileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Personnes.
 *
 * @VerifOnlyAbc
 * @UniqueChefFamille
 * @UniqueRepondant
 * @ORM\Table(name="personnes")
 * @ORM\Entity(repositoryClass="App\Repository\PersonnesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Personnes extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    public const role_chefDeFamille = 'Chef de famille';

    public const role_conjoint = 'Conjoint';

    public const role_pas = 'Pas chef, pas conjoint';

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    public $id;

    /**
     * @Assert\Length(
     *     max=45,
     *     maxMessage="Le champ cni doit être pas plus grand que {{ limit }} characters"
     * )
     *
     * @var string
     *
     * @ORM\Column(name="cni", type="string", length=50, nullable=true)
     *
     * FIXME should be unique? if yes should be corrected in the current database
     */
    private $cni;

    /**
     * @var string
     *
     * @ORM\Column(name="contact", type="string", length=1, nullable=true)
     */
    private $contact;

    /**
     * @var DateTime
     * @ORM\Column(name="date_naissance", type="datetime", nullable=true)
     */
    private $dateNaissance;

    /**
     * @ORM\ManyToOne(targetEntity="TblEtatCivil")
     */
    private $etatCivil;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations", inversedBy="personnes")
     */
    private $exploitation;

    /**
     * @var int
     *
     * @ORM\Column(name="membre_menage", type="boolean", options={"default": true}, nullable=true)
     */
    private $membreMenage;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=50, nullable=true)
     */
    private $nom;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\PersonneDetails", mappedBy="personnes")
     */
    private $personneDetails;

    /**
     * @var string
     *
     * A photo
     *
     * @ORM\Column(name="photo", type="string")
     */
    private $photo = '';

    // Field where to store the uploaded photo by form submission

    /**
     * @Assert\File(mimeTypes={"image/png", "image/jpeg" })
     */
    private $photoUploadedFile;

    /**
     * @var string
     *
     * @ORM\Column(name="prenom", type="string", length=50, nullable=true)
     */
    private $prenom;

    /**
     * @var int
     *
     * @ORM\Column(name="repondant", type="boolean", options={"default": false})
     */
    private $repondant;

    /**
     * @var string
     *
     * @ORM\Column(name="rol_famille", type="string", length=15, nullable=true)
     */
    private $rolFamille;

    /**
     * @var string
     *
     * @ORM\Column(name="sexe", type="string", length=10, nullable=true)
     */
    private $sexe;

    /**
     * constructeur pour intialiser le lien inverse vers personnes.
     *
     * @param mixed $user
     */
    public function __construct($user)
    {
        parent::__construct($user);
        $this->personneDetails = new ArrayCollection();

        if (!$this->repondant) {
            $this->repondant = false;
        }
    }

    public function __toString(): string
    {
        return sprintf('%s - %s', $this->getNom(), $this->getPrenom());
    }

    /**
     * Add personneDetail.
     *
     * @return Personnes
     */
    public function addPersonneDetail(PersonneDetails $personneDetail)
    {
        $this->PersonneDetails[] = $personneDetail;

        return $this;
    }

    /**
     * Add personneDetails.
     *
     * @return Personnes
     */
    public function addPersonneDetails(PersonneDetails $personneDetail)
    {
        $this->personneDetails[] = $personneDetail;
        $personneDetail->setPersonnes($this);

        return $this;
    }

    /**
     * Get cni.
     *
     * @return string
     */
    public function getCni()
    {
        return $this->cni;
    }

    /**
     * Get contact.
     *
     * @return string
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Get dateNaissance.
     *
     * @return DateTime
     */
    public function getDateNaissance()
    {
        return $this->dateNaissance;
    }

    /**
     * Get details for a year/annee.
     *
     * @param mixed $annee
     */
    public function getDetailsFor($annee)
    {
        $ret = [];

        foreach ($this->personneDetails as $details) {
            if ($details->getAnnee() === $annee) {
                $ret[] = $details;
            }
        }

        if (count($ret) > 1) {
            throw new Exception('This personnes has multiple details for a year', 1);
        }

        if (1 === count($ret)) {
            return $ret[0];
        }

        return null;
    }

    /**
     * Get etatCivil.
     *
     * @return \App\Entity\TblEtatCivil
     */
    public function getEtatCivil()
    {
        return $this->etatCivil;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get membreMenage.
     *
     * @return int
     */
    public function getMembreMenage()
    {
        return $this->membreMenage;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get personneDetails.
     *
     * @return \App\Entity\personneDetails
     */
    public function getPersonneDetails()
    {
        return $this->personneDetails;
    }

    public function getPhoto(): string
    {
        return $this->photo;
    }

    public function getPhotoUploadedFile(): ?UploadedFile
    {
        return $this->photoUploadedFile;
    }

    public function getPhotoUrl($personnesPhotoWebdir)
    {
        if ($this->getPhoto()) {
            $ret = $personnesPhotoWebdir.'/'.$this->getPhoto();
        } else {
            $ret = '';
        }

        return $ret;
    }

    /**
     * Get prenom.
     *
     * @return string
     */
    public function getPrenom()
    {
        return $this->prenom;
    }

    /**
     * Get repondant.
     *
     * @return bool
     */
    public function getRepondant()
    {
        return $this->repondant;
    }

    /**
     * Get rolFamille.
     *
     * @return string
     */
    public function getRolFamille()
    {
        return $this->rolFamille;
    }

    /**
     * Get sexe.
     *
     * @return string
     */
    public function getSexe()
    {
        return $this->sexe;
    }

    /**
     * Is chef famille ?
     *
     * @return bool
     */
    public function isChefFamille()
    {
        if ($this->rolFamille === $this::role_chefDeFamille) {
            return true;
        }

        return false;
    }

    /**
     * Is repondant ?
     *
     * @return bool
     */
    public function isRepondant()
    {
        return $this->repondant;
    }

    /**
     * Remove personneDetail.
     */
    public function removePersonneDetail(PersonneDetails $personneDetail)
    {
        $this->PersonneDetails->removeElement($personneDetail);
    }

    /**
     * Remove personneDetails.
     */
    public function removePersonneDetails(PersonneDetails $personneDetail)
    {
        $this->personneDetails->removeElement($personneDetail);
    }

    /**
     * Set cni.
     *
     * @param string $cni
     *
     * @return Personnes
     */
    public function setCni($cni)
    {
        $this->cni = $cni;

        return $this;
    }

    /**
     * Set contact.
     *
     * @param string $contact
     *
     * @return Personnes
     */
    public function setContact($contact)
    {
        $this->contact = $contact;

        return $this;
    }

    /**
     * Set dateNaissance.
     *
     * @param mixed $dateNaissance
     *
     * @return Personnes
     */
    public function setDateNaissance($dateNaissance)
    {
        $this->dateNaissance = $dateNaissance;

        return $this;
    }

    /**
     * Set etatCivil.
     *
     * @param \App\Entity\TblEtatCivil $etatCivil
     *
     * @return Exploitations
     */
    public function setEtatCivil(?TblEtatCivil $etatCivil = null)
    {
        $this->etatCivil = $etatCivil;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return Personnes
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set membreMenage.
     *
     * @param int $membreMenage
     *
     * @return Personnes
     */
    public function setMembreMenage($membreMenage)
    {
        $this->membreMenage = $membreMenage;

        return $this;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return Personnes
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    public function setPhoto(string $photo): Personnes
    {
        $this->photo = $photo;

        return $this;
    }

    public function setPhotoUploadedFile(UploadedFile $photoUploadedFile)
    {
        $this->photoUploadedFile = $photoUploadedFile;

        return $this;
    }

    /**
     * Set prenom.
     *
     * @param string $prenom
     *
     * @return Personnes
     */
    public function setPrenom($prenom)
    {
        $this->prenom = $prenom;

        return $this;
    }

    /**
     * Set repondant.
     *
     * @param bool $repondant
     *
     * @return Personnes
     */
    public function setRepondant($repondant)
    {
        if ($repondant) {
            /*    $exploitation = $this->exploitation;

                foreach($exploitation->getPersonnes() as $personne) {
                    $personne->setRepondant(false);
                }

             */
        }

        $this->repondant = $repondant;

        return $this;
    }

    /**
     * Set rolFamille.
     *
     * @param string $rolFamille
     *
     * @return Personnes
     */
    public function setRolFamille($rolFamille)
    {
        $this->rolFamille = $rolFamille;

        return $this;
    }

    /**
     * Set sexe.
     *
     * @param string $sexe
     *
     * @return Personnes
     */
    public function setSexe($sexe)
    {
        $this->sexe = $sexe;

        return $this;
    }

    public function storeUploadedPhoto($absolutePersonnesPhotoDirectory)
    {
        $file = $this->getPhotoUploadedFile();

        if ($file && $file->isValid()) { // todo be sure the the id is defined
            $fileNameForStorage = $this->getId().'.'.$file->guessExtension();

            try {
                $image = Image::make(realpath($file));

                // the photo has max a width of 400px and a height of 400px
                if ($image->width() > $image->height()) {
                    $image->widen(400);
                } else {
                    $image->heighten(400);
                }
                $image->save($absolutePersonnesPhotoDirectory.'/'.$fileNameForStorage);
            } catch (FileException $e) {
                $fileNameForStorage = '';
            }

            $this->setPhoto($fileNameForStorage);

            return true;
        }

        return false;
    }
}
