<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

trait CDateField
{
    /**
     * @ORM\Column(name="cdate", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    protected DateTimeInterface $cdate;

    public function getCDate(): DateTimeInterface
    {
        return $this->cdate;
    }

    public function setCDate(DateTimeInterface $dateTime): void
    {
        $this->cdate = $dateTime;
    }
}
