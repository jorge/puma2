<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait RadieField
{
    /**
     * @ORM\Column(name="radie", type="boolean", options={"default": false})
     */
    protected bool $radie = FALSE;

    public function getRadie(): bool
    {
        return $this->radie;
    }

    public function setRadie(bool $radie): void
    {
        $this->radie = $radie;
    }
}
