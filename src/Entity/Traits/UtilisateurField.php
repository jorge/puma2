<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use Doctrine\ORM\Mapping as ORM;

trait UtilisateurField
{
    /**
     * @ORM\Column(name="utilisateur", type="string", length=30, nullable=true)
     */
    protected ?string $utilisateur = null;

    public function getUtilisateur(): ?string
    {
        return $this->utilisateur;
    }

    public function setUtilisateur(?string $user = null): void
    {
        $this->utilisateur = $user;
    }
}
