<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

trait UDateField
{
    /**
     * @ORM\Column(name="udate", type="datetime", options={"default": "CURRENT_TIMESTAMP"})
     */
    protected DateTimeInterface $udate;

    public function getUDate(): DateTimeInterface
    {
        return $this->udate;
    }

    public function setUDate(DateTimeInterface $dateTime): void
    {
        $this->udate = $dateTime;
    }
}
