<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * DemandeCredit.
 *
 * @ORM\Table(name="demande_credit")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class DemandeCredit extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="ExploitationPlanSaison", inversedBy="demandeCredit")
     */
    private $exploitationPlanSaison;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="montant", type="decimal")
     */
    private $montant;

    /**
     * @ORM\ManyToOne(targetEntity=TblTypeCreancier::class)
     */
    private $source;

    /**
     * Get exploitationPlanSaison.
     *
     * @return \App\Entity\ExploitationPlanSaison
     */
    public function getExploitationPlanSaison()
    {
        return $this->exploitationPlanSaison;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get montant.
     *
     * @return float
     */
    public function getMontant()
    {
        return $this->montant;
    }

    public function getSource(): ?TblTypeCreancier
    {
        return $this->source;
    }

    /**
     * Set exploitationPlanSaison.
     *
     * @param \App\Entity\ExploitationPlanSaison $exploitationPlanSaison
     *
     * @return DemandeCredit
     */
    public function setExploitationPlanSaison(ExploitationPlanSaison $exploitationPlanSaison)
    {
        $this->exploitationPlanSaison = $exploitationPlanSaison;

        return $this;
    }

    /**
     * Set montant.
     *
     * @param float $montant
     *
     * @return DemandeCredit
     */
    public function setMontant($montant)
    {
        $this->montant = $montant;

        return $this;
    }

    public function setSource(?TblTypeCreancier $source): self
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Set udate.
     *
     * @param mixed $udate
     *
     * @return DemandeCredit
     */
}
