<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblProduitPhyto.
 *
 * @ORM\Table(name="tbl_produit_phyto")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblProduitPhyto extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="phyto_name", type="string", length=255, nullable=true, unique=true)
     */
    private $phytoName;

    /**
     * @ORM\ManyToOne(targetEntity="TblUniteProduits")
     */
    private $unite;

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        return sprintf('%s', $this->getPhytoName());
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nom avec unité.
     */
    public function getNomAvecUnite()
    {
        return sprintf('%s (unité : %s)', $this->phytoName, $this->unite);
    }

    /**
     * Get phytoName.
     *
     * @return string
     */
    public function getPhytoName()
    {
        return $this->phytoName;
    }

    /**
     * Get unite.
     *
     * @return \App\Entity\TblUniteProduits
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set phytoName.
     *
     * @param string $phytoName
     *
     * @return TblProduitPhyto
     */
    public function setPhytoName($phytoName)
    {
        $this->phytoName = $phytoName;

        return $this;
    }

    /**
     * Set unite.
     *
     * @param \App\Entity\TblUniteProduits $unite
     *
     * @return TblProduitPhyto
     */
    public function setUnite(TblUniteProduits $unite)
    {
        $this->unite = $unite;

        return $this;
    }
}
