<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblCategoriesAgricoles.
 *
 * @ORM\Table(name="tbl_categories_agricoles")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblCategoriesAgricoles extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="categorie", type="string", length=30, nullable=true)
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="code_categorie", type="string", length=2, nullable=true)
     */
    private $codeCategorie;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        return $this->categorie;
    }

    /**
     * Get categorie.
     *
     * @return string
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Get codeCategorie.
     *
     * @return string
     */
    public function getCodeCategorie()
    {
        return $this->codeCategorie;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set categorie.
     *
     * @param string $categorie
     *
     * @return TblCategoriesAgricoles
     */
    public function setCategorie($categorie)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Set codeCategorie.
     *
     * @param string $codeCategorie
     *
     * @return TblCategoriesAgricoles
     */
    public function setCodeCategorie($codeCategorie)
    {
        $this->codeCategorie = $codeCategorie;

        return $this;
    }
}
