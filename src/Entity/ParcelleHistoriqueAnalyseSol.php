<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * ParcelleHistoriqueAnalyseSol.
 *
 * @ORM\Table(name="parcelle_historique_analyse_sol")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ParcelleHistoriqueAnalyseSol extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="TblAnalyseSol")
     */
    private $analyseSol;

    /**
     * @var DateTime
     *
     * Date de l'opération
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ParcelleHistoriques")
     */
    private $parcelleHistorique;

    public function __construct($user)
    {
        parent::__construct($user);
    }

    /**
     * Get analyseSol.
     *
     * @return \App\Entity\TblAnalyseSol
     */
    public function getAnalyseSol()
    {
        return $this->analyseSol;
    }

    /**
     * Get date.
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get parcelleHistorique.
     *
     * @return \App\Entity\ParcelleHistoriques
     */
    public function getParcelleHistorique()
    {
        return $this->parcelleHistorique;
    }

    /**
     * Set analyseSol.
     *
     * @param \App\Entity\TblAnalyseSol analyseSol
     *
     * @return ParcelleHistoriqueAnalyseSol
     */
    public function setAleas(?TblAnalyseSol $analyseSol = null)
    {
        $this->analyseSol = $analyseSol;

        return $this;
    }

    /**
     * Set analyseSol.
     *
     * @param \App\Entity\TblAnalyseSol|null $analyseSol
     *
     * @return ParcelleHistoriqueAnalyseSol
     */
    public function setAnalyseSol(?TblAnalyseSol $analyseSol = null)
    {
        $this->analyseSol = $analyseSol;

        return $this;
    }

    /**
     * Set date.
     *
     * @param DateTime $date
     *
     * @return ParcelleHistoriqueAnalyseSol
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set parcelleHistorique.
     *
     * @param \App\Entity\ParcelleHistoriques $parcelleHistorique
     *
     * @return ParcelleHistoriqueAnalyseSol
     */
    public function setParcelleHistorique(?ParcelleHistoriques $parcelleHistorique = null)
    {
        $this->parcelleHistorique = $parcelleHistorique;

        return $this;
    }
}
