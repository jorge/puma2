<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblContraints.
 *
 * @ORM\Table(name="tbl_credit_objet")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblCreditObjet extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="creditobjet", type="string", length=255, nullable=true, unique=true)
     */
    private $creditobjet;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        if (null === $this->getCreditobjet()) {
            return 'NULL';
        }

        return $this->getCreditobjet();
    }

    /**
     * Get creditobjet.
     *
     * @return string
     */
    public function getCreditobjet()
    {
        return $this->creditobjet;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set creditobjet.
     *
     * @param string $creditobjet
     *
     * @return TblCreditObjet
     */
    public function setCreditobjet($creditobjet)
    {
        $this->creditobjet = $creditobjet;

        return $this;
    }
}
