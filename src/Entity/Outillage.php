<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * Outillage.
 *
 * @ORM\Table(name="outillage")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Outillage extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F1;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var int
     *
     * @ORM\Column(name="anneeAcquisition", type="integer", nullable=true)
     */
    private $anneeAcquisition;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="quantite", type="smallint", nullable=true)
     */
    private $quantite;

    /**
     * @ORM\ManyToOne(targetEntity="TblEquipementsAgricoles")
     */
    private $type;

    /**
     * @var string
     *
     * @ORM\Column(name="valeurAcquisition", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $valeurAcquisition;

    /**
     * @var string
     *
     * @ORM\Column(name="valeurResiduelle", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $valeurResiduelle;


    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get anneeAcquisition.
     *
     * @return int
     */
    public function getAnneeAcquisition()
    {
        return $this->anneeAcquisition;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get quantite.
     *
     * @return int
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get type.
     *
     * @return \App\Entity\TblEquipementsAgricoles
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get valeurAcquisition.
     *
     * @return string
     */
    public function getValeurAcquisition()
    {
        return $this->valeurAcquisition;
    }

    /**
     * Get valeurResiduelle.
     *
     * @return string
     */
    public function getValeurResiduelle()
    {
        return $this->valeurResiduelle;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return Outillage
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set anneeAcquisition.
     *
     * @param int $anneeAcquisition
     *
     * @return Outillage
     */
    public function setAnneeAcquisition($anneeAcquisition)
    {
        $this->anneeAcquisition = $anneeAcquisition;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return Outillage
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set quantite.
     *
     * @param int $quantite
     *
     * @return Outillage
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Set type.
     *
     * @param \App\Entity\TblEquipementsAgricoles $type
     *
     * @return Outillage
     */
    public function setType(?TblEquipementsAgricoles $type = null)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set valeurAcquisition.
     *
     * @param string $valeurAcquisition
     *
     * @return Outillage
     */
    public function setValeurAcquisition($valeurAcquisition)
    {
        $this->valeurAcquisition = $valeurAcquisition;

        return $this;
    }

    /**
     * Set valeurResiduelle.
     *
     * @param string $valeurResiduelle
     *
     * @return Outillage
     */
    public function setValeurResiduelle($valeurResiduelle)
    {
        $this->valeurResiduelle = $valeurResiduelle;

        return $this;
    }
}
