<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * MoFamilialeExterne.
 *
 * @ORM\Table(name="mo_familiale_externe")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class MoFamilialeExterne extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint")
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations", inversedBy="mo_familiale_externes")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="lieu", type="string", length=50, nullable=true)
     */
    private $lieu;

    /**
     * @var string
     *
     * @ORM\Column(name="membres", type="string", nullable=true)
     */
    private $membres;

    /**
     * @var float Revenu Annuel
     *
     * @ORM\Column(name="revenu", type="integer", nullable=true)
     */
    private $revenu;

    /**
     * @ORM\ManyToOne(targetEntity="TblProfessions")
     */
    private $typeTravail;


    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get lieu.
     *
     * @return string
     */
    public function getLieu()
    {
        return $this->lieu;
    }

    /**
     * Get membres.
     *
     * @return string
     */
    public function getMembres()
    {
        return $this->membres;
    }

    /**
     * Get revenu.
     *
     * @return string
     */
    public function getRevenu()
    {
        return $this->revenu;
    }

    /**
     * Get typeTravail.
     *
     * @return \App\Entity\TblProfessions
     */
    public function getTypeTravail()
    {
        return $this->typeTravail;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return MoFamilialeExterne
     */
    public function setAnnee(int $annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return MoFamilialeExterne
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set lieu.
     *
     * @param string $lieu
     *
     * @return MoFamilialeExterne
     */
    public function setLieu($lieu)
    {
        $this->lieu = $lieu;

        return $this;
    }

    /**
     * Set membres.
     *
     * @param string $membres
     *
     * @return MoFamilialeExterne
     */
    public function setMembres($membres)
    {
        $this->membres = $membres;

        return $this;
    }

    /**
     * Set revenu.
     *
     * @param string $revenu
     *
     * @return MoFamilialeExterne
     */
    public function setRevenu($revenu)
    {
        $this->revenu = $revenu;

        return $this;
    }

    /**
     * Set typeTravail.
     *
     * @param \App\Entity\TblProfessions $typeTravail
     *
     * @return MoFamilialeExterne
     */
    public function setTypeTravail($typeTravail)
    {
        $this->typeTravail = $typeTravail;

        return $this;
    }
}
