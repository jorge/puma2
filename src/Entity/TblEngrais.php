<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblEngrais.
 *
 * @ORM\Table(name="tbl_engrais")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblEngrais extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="engrais_nom", type="string", length=255, nullable=true, unique=true)
     */
    private $engraisNom;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="type", type="string", length=20, nullable=true, unique=false)
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="TblUniteProduits")
     */
    private $unite;

    public function __toString(): string
    {
        return sprintf('%s', $this->engraisNom);
    }

    /**
     * Get engraisNom.
     *
     * @return string
     */
    public function getEngraisNom()
    {
        return $this->engraisNom;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nom avec unité.
     */
    public function getNomAvecUnite()
    {
        return sprintf('%s (unité : %s)', $this->engraisNom, $this->unite);
    }

    /**
     * Get type.
     *
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get unite.
     *
     * @return \App\Entity\TblUniteProduits
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set engraisNom.
     *
     * @param string $engraisNom
     *
     * @return TblEngrais
     */
    public function setEngraisNom($engraisNom)
    {
        $this->engraisNom = $engraisNom;

        return $this;
    }

    /**
     * Set type.
     *
     * @param string $type
     *
     * @return TblEngrais
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set unite.
     *
     * @param \App\Entity\TblUniteProduits $unite
     *
     * @return TblEngrais
     */
    public function setUnite(TblUniteProduits $unite)
    {
        $this->unite = $unite;

        return $this;
    }
}
