<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblValeurChoixSimple extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $displayOrder;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $entityClass;

    /**
     * @ORM\Id
     * @ORM\Column(type="string", length=32)
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    private $score;

    /**
     * @var string
     * @ORM\Column(type="string")
     */
    private $tag;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    private $valeur;

    /**
     * To String fct.
     */
    public function __toString(): string
    {
        return $this->valeur;
    }

    /**
     * Get displayOrder.
     *
     * @return int
     */
    public function getDisplayOrder()
    {
        return $this->displayOrder;
    }

    /**
     * Get entityClass.
     *
     * @return string
     */
    public function getEntityClass()
    {
        return $this->entityClass;
    }

    public function getId(): ?string
    {
        return $this->id;
    }

    /**
     * Get score.
     *
     * @return int
     */
    public function getScore(): ?int
    {
        return $this->score;
    }

    /**
     * Get tag.
     *
     * @return string
     */
    public function getTag()
    {
        return $this->tag;
    }

    /**
     * Get valeur.
     *
     * @return string
     */
    public function getValeur()
    {
        return $this->valeur;
    }

    /**
     * Set displayOrder.
     *
     * @param int $displayOrder
     *
     * @return TblValeurChoixSimple
     */
    public function setDisplayOrder($displayOrder)
    {
        $this->displayOrder = $displayOrder;

        return $this;
    }

    /**
     * Set entityClass.
     *
     * @param string $entityClass
     *
     * @return TblValeurChoixSimple
     */
    public function setEntityClass($entityClass)
    {
        $this->entityClass = $entityClass;

        return $this;
    }

    public function setId(string $id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Set score.
     *
     * @return TblValeurChoixSimple
     */
    public function setScore(int $score)
    {
        $this->score = $score;

        return $this;
    }

    /**
     * Set tag.
     *
     * @param string $tag
     *
     * @return TblValeurChoixSimple
     */
    public function setTag($tag)
    {
        $this->tag = $tag;

        return $this;
    }

    /**
     * Set valeur.
     *
     * @param string $valeur
     *
     * @return TblValeurChoixSimple
     */
    public function setValeur($valeur)
    {
        $this->valeur = $valeur;

        return $this;
    }
}
