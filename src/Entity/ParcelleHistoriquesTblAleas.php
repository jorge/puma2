<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Table(name="parcelle_historiques_tbl_aleas")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class ParcelleHistoriquesTblAleas extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="TblAleas")
     */
    private $aleas;

    /**
     * @var string
     *
     * @ORM\Column(name="commentaire", type="string", length=255, nullable=true)
     */
    private $commentaire;

    /**
     * @ORM\ManyToOne(targetEntity="TblCultures")
     */
    private $culture;

    /**
     * @var DateTime
     *
     * Date de l'opération
     * @ORM\Column(name="date", type="date")
     */
    private $date;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="ParcelleHistoriques")
     */
    private $parcelleHistorique;

    public function __construct($user)
    {
        parent::__construct($user);
    }

    /**
     * Get aleas.
     *
     * @return \App\Entity\TblAleas
     */
    public function getAleas()
    {
        return $this->aleas;
    }

    /**
     * Get commentaire.
     *
     * @return string
     */
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Get culture.
     *
     * @return TblCultures
     */
    public function getCulture()
    {
        return $this->culture;
    }

    /**
     * Get date.
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get parcelleHistorique.
     *
     * @return \App\Entity\ParcelleHistoriques
     */
    public function getParcelleHistorique()
    {
        return $this->parcelleHistorique;
    }

    /**
     * Set aleas.
     *
     * @param \App\Entity\TblAleas aleas
     *
     * @return ParcelleHistoriquesTblAleas
     */
    public function setAleas(?TblAleas $aleas = null)
    {
        $this->aleas = $aleas;

        return $this;
    }

    /**
     * Set commentaire.
     *
     * @param string $commentaire
     *
     * @return ParcelleHistoriquesTblAleas
     */
    public function setCommentaire($commentaire)
    {
        $this->commentaire = $commentaire;

        return $this;
    }

    /**
     * Set culture.
     *
     * @param TblCultures $culture
     *
     * @return ParcelleHistoriquesTblAleas
     */
    public function setCulture($culture)
    {
        $this->culture = $culture;

        return $this;
    }

    /**
     * Set date.
     *
     * @param DateTime $date
     *
     * @return ParcelleHistoriquesTblAleas
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set parcelleHistorique.
     *
     * @param \App\Entity\ParcelleHistoriques $parcelleHistorique
     *
     * @return ParcelleHistoriquesTblAleas
     */
    public function setParcelleHistorique(?ParcelleHistoriques $parcelleHistorique = null)
    {
        $this->parcelleHistorique = $parcelleHistorique;

        return $this;
    }
}
