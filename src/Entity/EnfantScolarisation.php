<?php

declare(strict_types=1);

/*
 * This file is part of Puma2.
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Puma2 is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with Puma2.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 *saisie get infoMenage.
 */
class EnfantScolarisation
{
    protected $exploitation;

    protected $fillesScolarisees;

    protected $filsScolarises;

    public function __construct($explot)
    {
        $this->setFilsScolarises($explot->getFilsScolarises()); // à effacer
        $this->setFillesScolarisees($explot->getFillesScolarisess()); // à effacer
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get fillesScolarisees.
     *
     * @return int
     */
    public function getFillesScolarisees()
    {
        return $this->fillesScolarisees;
    }

    /**
     * Get filsScolarises.
     *
     * @return int
     */
    public function getFilsScolarises()
    {
        return $this->filsScolarises;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return infoMenage
     */
    public function setExploitation(Exploitations $exploitation)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set fillesScolarisees.
     *
     * @param int $fillesScolarisees
     *
     * @return infoMenage
     */
    public function setFillesScolarisees($fillesScolarisees)
    {
        $this->fillesScolarisees = $fillesScolarisees;

        return $this;
    }

    /**
     * Set filsScolarises.
     *
     * @param int $filsScolarises
     *
     * @return infoMenage
     */
    public function setFilsScolarises($filsScolarises)
    {
        $this->filsScolarises = $filsScolarises;

        return $this;
    }
}
