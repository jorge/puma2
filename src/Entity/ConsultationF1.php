<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 * ConsultationF1.
 */
class ConsultationF1
{
    protected $animaux;

    protected $Denomination;

    protected $exploitationGroupAnnee;

    protected $exploitationListAnnee;

    protected $groupecoop;

    protected $groupegroup;

    protected $queryTheme;

    protected $terres;

    public function __construct()
    {
        $this->Denomination = null;
        $this->groupecoop = false;
        $this->groupegroup = false;
        $this->queryTheme = 'exploitation';
        $this->exploitationGroupAnnee = false;
        $this->exploitationListAnnee = null;
        $this->animaux = null;
        $this->terres = null;

        return $this;
    }

    public function getAnimaux()
    {
        return $this->animaux;
    }

    public function getDenomination()
    {
        return $this->Denomination;
    }

    public function getExploitationGroupAnnee()
    {
        return $this->exploitationGroupAnnee;
    }

    public function getExploitationListAnnee()
    {
        return $this->exploitationListAnnee;
    }

    public function getGroupecoop()
    {
        return $this->groupecoop;
    }

    public function getGroupegroup()
    {
        return $this->groupegroup;
    }

    public function getQueryTheme()
    {
        return $this->queryTheme;
    }

    public function getSaison()
    {
        return $this->saison;
    }

    public function getTerres()
    {
        return $this->terres;
    }

    public function setAnimaux($animaux = 'false')
    {
        $this->animaux = $animaux;

        return $this;
    }

    /**
     * Set Denomination.
     *
     * @param \App\Entity\TblOrganisations $Denomination
     *
     * @return Exploitations
     */
    public function setDenomination(?TblOrganisations $Denomination = null)
    {
        $this->Denomination = $Denomination;

        return $this;
    }

    public function setExploitationGroupAnnee($exploitationGroupAnnee = 'false')
    {
        $this->exploitationGroupAnneel = $exploitationGroupAnnee;

        return $this;
    }

    //exploitationListAnnee
    public function setExploitationListAnnee($exploitationListAnnee = null)
    {
        $this->exploitationListAnneel = $exploitationListAnnee;

        return $this;
    }

    public function setGroupecoop($groupecoop = false)
    {
        $this->groupecoop = $groupecoop;

        return $this;
    }

    public function setGroupegroup($groupegroup = false)
    {
        $this->groupegroup = $groupegroup;

        return $this;
    }

    public function setQueryTheme($queryTheme = 'exploitation')
    {
        $this->queryTheme = $queryTheme;

        return $this;
    }

    //saison
    public function setSaison($saison = null)
    {
        $this->saison = $saison;

        return $this;
    }

    public function setTerres($terres = 'false')
    {
        $this->terres = $terres;

        return $this;
    }

    /*   public function setCoutProduction($coutProduction = null)
       {
        $this->coutProduction = $coutProduction;

        return $this;
       }
     */
/*    public function getCoutProduction()
    {
        return $this->coutProduction;
    }
 */
/*   public function getsommesCharges()
    {
        return $this->sommesCharges;
    }

    public function setsommesCharges($sommesCharges = false)
    {
        $this->sommesCharges = $sommesCharges;

        return $this;
    }
 */
/*     public function getAleas()
        {
            return $this->Aleas;
        }

        public function setAleas($Aleas = false)
        {
            $this->Aleas = $Aleas;

            return $this;
    }
/*
/*     public function getVentes()
        {
            return $this->Ventes;
        }

        public function setVentes($Ventes = false)
        {
            $this->Ventes = $Ventes;

            return $this;
        }

     public function getcoutElevage()
        {
            return $this->coutElevage;
        }

        public function setcoutElevage($coutElevage = false)
        {
            $this->coutElevage = $coutElevage;

            return $this;
        }

     public function getPrevus()
        {
            return $this->Prevus;
        }

        public function setPrevus($Prevus = false)
        {
            $this->Prevus = $Prevus;

            return $this;
        }

     public function getDettes()
        {
            return $this->Dettes;
        }

        public function setDettes($Dettes = false)
        {
            $this->Dettes = $Dettes;

            return $this;
        }

 */
}
