<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblAnimauxMouvements.
 *
 * @ORM\Table(name="tbl_animaux_mouvements")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblAnimauxMouvements extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var int
     *
     * @ORM\Column(name="ajout", type="integer", nullable=true)
     */
    private $ajout;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="technique", type="string", length=255, nullable=true, unique=true)
     */
    private $technique;

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        if (null === $this->getTechnique()) {
            return 'NULL';
        }

        return $this->getTechnique();
    }

    /**
     * Get ajout.
     *
     * @return int
     */
    public function getAjout()
    {
        return $this->ajout;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get technique.
     *
     * @return string
     */
    public function getTechnique()
    {
        return $this->technique;
    }

    /**
     * Set ajout.
     *
     * @param int $ajout
     *
     * @return TblAnimauxMouvements
     */
    public function setAjout($ajout)
    {
        $this->ajout = $ajout;

        return $this;
    }

    /**
     * Set technique.
     *
     * @param string $technique
     *
     * @return TblAnimauxMouvements
     */
    public function setTechnique($technique)
    {
        $this->technique = $technique;

        return $this;
    }
}
