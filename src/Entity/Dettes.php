<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Dettes.
 *
 * @ORM\Table(name="dettes")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Dettes extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="TblCreditObjet")
     */
    protected $creditobjet;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F4;

    /**
     * @var string Année de l'enquête
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var string Année de la dette
     *
     * @ORM\Column(name="annee_dette", type="string", length=4, nullable=true)
     */
    private $anneeDette;

    /**
     * @var string
     *
     * @ORM\Column(name="credit_cash_demande", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $creditCashDemande;

    /**
     * @var string
     *
     * @ORM\Column(name="credit_intrants_demande", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $creditIntrantsDemande;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations", inversedBy="dettes")
     */
    private $exploitation;

    /**
     * @ORM\ManyToOne(targetEntity="TblTypeGarantie")
     */
    private $garantie;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblInstitutionsFinancieres")
     */
    private $institutionFinanciere;

    /**
     * @var string
     *
     * @ORM\Column(name="interets_payes", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $interetsPayes;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_nantissement", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $montantNantissement;

    /**
     * @var string
     *
     * @ORM\Column(name="montant_rembourse", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $montantRembourse;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="periode_reception", type="date", nullable=true)
     */
    private $periodeReception;

    /**
     * @var int
     *
     * @ORM\Column(name="periode_remboursement", type="integer", nullable=true)
     */
    private $periodeRemboursement;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="periode_remboursement_old", type="date", nullable=true)
     */
    private $periodeRemboursementOld;

    /**
     * @ORM\ManyToOne(targetEntity="TblSaisons")
     */
    private $saison;

    /**
     * @var string
     *
     * @ORM\Column(name="taux_credit", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $tauxCredit;

    /**
     * @ORM\ManyToOne(targetEntity="TblTypeCreancier")
     */
    private $typeCreancier;

    /**
     * @var int
     *
     * @ORM\Column(name="valeur_credit", type="integer", nullable=true)
     */
    private $valeurCredit;

    public function __construct($user)
    {
        parent::__construct($user);

        $this->creditobjet = new ArrayCollection();
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get creditCashDemande.
     *
     * @return string
     */
    public function getCreditCashDemande()
    {
        return $this->creditCashDemande;
    }

    /**
     * Get creditIntrantsDemande.
     *
     * @return string
     */
    public function getCreditIntrantsDemande()
    {
        return $this->creditIntrantsDemande;
    }

    /**
     * Get creditobjet.
     *
     * @return \App\Entity\TblCreditObjet
     */
    public function getCreditobjet()
    {
        return $this->creditobjet;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get garantie.
     *
     * @return \App\Entity\TblTypeGarantie
     */
    public function getGarantie()
    {
        return $this->garantie;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get institutionFinanciere.
     *
     * @return \App\Entity\TblInstitutionsFinancieres
     */
    public function getInstitutionFinanciere()
    {
        return $this->institutionFinanciere;
    }

    /**
     * Get interetsPayes.
     *
     * @return string
     */
    public function getInteretsPayes()
    {
        return $this->interetsPayes;
    }

    /**
     * Get montantNantissement.
     *
     * @return string
     */
    public function getMontantNantissement()
    {
        return $this->montantNantissement;
    }

    /**
     * Get montantRembourse.
     *
     * @return string
     */
    public function getMontantRembourse()
    {
        return $this->montantRembourse;
    }

    /**
     * Get periodeReception.
     *
     * @return DateTime
     */
    public function getPeriodeReception()
    {
        return $this->periodeReception;
    }

    /**
     * Get periodeRemboursement.
     *
     * @return int
     */
    public function getPeriodeRemboursement()
    {
        return $this->periodeRemboursement;
    }

    /**
     * Get periodeRemboursementOld.
     *
     * @return DateTime
     */
    public function getPeriodeRemboursementOld()
    {
        return $this->periodeRemboursementOld;
    }

    /**
     * Get saison.
     *
     * @return \App\Entity\TblSaisons
     */
    public function getSaison()
    {
        return $this->saison;
    }

    /**
     * Get tauxCredit.
     *
     * @return string
     */
    public function getTauxCredit()
    {
        return $this->tauxCredit;
    }

    /**
     * Get typeCreancier.
     *
     * @return \App\Entity\TblTypeCreancier
     */
    public function getTypeCreancier()
    {
        return $this->typeCreancier;
    }

    /**
     * Get valeurCredit.
     *
     * @return int
     */
    public function getValeurCredit()
    {
        return $this->valeurCredit;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return Dettes
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set creditCashDemande.
     *
     * @param string $creditCashDemande
     *
     * @return Dettes
     */
    public function setCreditCashDemande($creditCashDemande)
    {
        $this->creditCashDemande = $creditCashDemande;

        return $this;
    }

    /**
     * Set creditIntrantsDemande.
     *
     * @param string $creditIntrantsDemande
     *
     * @return Dettes
     */
    public function setCreditIntrantsDemande($creditIntrantsDemande)
    {
        $this->creditIntrantsDemande = $creditIntrantsDemande;

        return $this;
    }

    /**
     * Set creditobjet.
     *
     * @param \App\Entity\TblCreditObjet $creditobjet
     *
     * @return Dettes
     */
    public function setCreditobjet(?TblCreditObjet $creditobjet = null)
    {
        $this->creditobjet = $creditobjet;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return Dettes
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set garantie.
     *
     * @param \App\Entity\TblTypeGarantie $garantie
     *
     * @return Dettes
     */
    public function setGarantie(?TblTypeGarantie $garantie = null)
    {
        $this->garantie = $garantie;

        return $this;
    }

    /**
     * Set institutionFinanciere.
     *
     * @param \App\Entity\TblInstitutionsFinancieres $institutionFinanciere
     *
     * @return Dettes
     */
    public function setInstitutionFinanciere(?TblInstitutionsFinancieres $institutionFinanciere = null)
    {
        $this->institutionFinanciere = $institutionFinanciere;

        return $this;
    }

    /**
     * Set interetsPayes.
     *
     * @param string $interetsPayes
     *
     * @return Dettes
     */
    public function setInteretsPayes($interetsPayes)
    {
        $this->interetsPayes = $interetsPayes;

        return $this;
    }

    /**
     * Set montantNantissement.
     *
     * @param string $montantNantissement
     *
     * @return Dettes
     */
    public function setMontantNantissement($montantNantissement)
    {
        $this->montantNantissement = $montantNantissement;

        return $this;
    }

    /**
     * Set montantRembourse.
     *
     * @param string $montantRembourse
     *
     * @return Dettes
     */
    public function setMontantRembourse($montantRembourse)
    {
        $this->montantRembourse = $montantRembourse;

        return $this;
    }

    /**
     * Set periodeReception.
     *
     * @param DateTime $periodeReception
     *
     * @return Dettes
     */
    public function setPeriodeReception($periodeReception)
    {
        $this->periodeReception = $periodeReception;

        return $this;
    }

    /**
     * Set periodeRemboursement.
     *
     * @param int $periodeRemboursement
     *
     * @return Dettes
     */
    public function setPeriodeRemboursement($periodeRemboursement)
    {
        $this->periodeRemboursement = $periodeRemboursement;

        return $this;
    }

    /**
     * Set periodeRemboursementOld.
     *
     * @param DateTime $periodeRemboursementOld
     *
     * @return Dettes
     */
    public function setPeriodeRemboursementOld($periodeRemboursementOld)
    {
        $this->periodeRemboursementOld = $periodeRemboursementOld;

        return $this;
    }

    /**
     * Set saison.
     *
     * @param \App\Entity\TblSaisons $saison
     *
     * @return Dettes
     */
    public function setSaison(?TblSaisons $saison = null)
    {
        $this->saison = $saison;

        return $this;
    }

    /**
     * Set tauxCredit.
     *
     * @param string $tauxCredit
     *
     * @return Dettes
     */
    public function setTauxCredit($tauxCredit)
    {
        $this->tauxCredit = $tauxCredit;

        return $this;
    }

    /**
     * Set typeCreancier.
     *
     * @param \App\Entity\TblTypeCreancier typeCreancier
     *
     * @return Dettes
     */
    public function setTypeCreancier(?TblTypeCreancier $typeCreancier = null)
    {
        $this->typeCreancier = $typeCreancier;

        return $this;
    }

    /**
     * Set valeurCredit.
     *
     * @param int $valeurCredit
     *
     * @return Dettes
     */
    public function setValeurCredit($valeurCredit)
    {
        $this->valeurCredit = $valeurCredit;

        return $this;
    }
}
