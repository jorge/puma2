<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * Une collecte d'information (une enquête).
 *
 * TODO supprimer la contrainte  exercice_type_uniq UniqueConstraint("exercice", "type") qui existe tant l'UI pas developpé sur ce pont
 *
 * @ORM\Table(name="collecte", uniqueConstraints={@ORM\UniqueConstraint(name="exercice_type_uniq", columns={"exercice_id", "fiche_type"})})
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 * @UniqueEntity(
 *     fields={"exercice", "type"},
 *     errorPath="type",
 *     message="Cette fiche a déjà été ouverte pour cet exercice !"
 * )
 */
class Collecte extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    public const F0 = 0;

    public const F1 = 1;

    public const F2 = 2;

    public const F3 = 3;

    public const F4 = 4;

    public const F5 = 5;

    public const F6 = 6;

    /**
     * @var DateTime
     *
     * Date de la collecte (où l'enquêteur était présent)
     * @ORM\Column(name="date_enquete", type="date", nullable=true)
     * EST une date qui peut être null
     */
    private $date;

    /**
     * @var string
     *
     * @ORM\Column(name="enqueteur", type="string", length=255)
     */
    private $enqueteur;

    /**
     * @var Exercice
     *
     * @ORM\ManyToOne(targetEntity="Exercice", inversedBy="collectes")
     */
    private $exercice;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int / Pour savoir si F0, F1, ... F6
     *
     * @ORM\Column(name="fiche_type", type="smallint")
     */
    private $type;

    /**
     * Get Annee exercice.
     */
    public function getAnneeExercice()
    {
        return $this->exercice->getAnnee();
    }

    /**
     * Get date d'enquete.
     *
     * @return DateTime
     */
    public function getDate()
    {
        return $this->date;
    }

    /**
     * Get enqueteur.
     *
     * @return string
     */
    public function getEnqueteur()
    {
        return $this->enqueteur;
    }

    /**
     * Get exercice.
     *
     * @return Exercice
     */
    public function getExercice()
    {
        return $this->exercice;
    }

    /**
     *  Get the exploitation.
     *
     * @return Exploitations
     */
    public function getExploitation()
    {
        return $this->getExercice()->getExploitation();
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get type d'enquete.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set date d'enquete.
     *
     * @param DateTime $date
     *
     * @return Collecte
     */
    public function setDate($date)
    {
        $this->date = $date;

        return $this;
    }

    /**
     * Set enqueteur.
     *
     * @param string $enqueteur
     *
     * @return Collecte
     */
    public function setEnqueteur($enqueteur)
    {
        $this->enqueteur = $enqueteur;

        return $this;
    }

    /**
     * Set exercice.
     *
     * @param Exercice $exercice
     *
     * @return Collecte
     */
    public function setExercice($exercice)
    {
        $this->exercice = $exercice;

        return $this;
    }

    /**
     * Set type de collecte.
     *
     * @param int $type
     *
     * @return Collecte
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }
}
