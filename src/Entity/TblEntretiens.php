<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * TblEntretiens.
 *
 * @ORM\Table(name="tbl_entretiens")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblEntretiens
{
    /**
     * @var string
     *
     * @ORM\Column(name="entretien", type="string", length=20, unique=true)
     */
    private $entretien;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __toString(): string
    {
        return sprintf('%s', $this->entretient);
    }

    /**
     * Get entretien.
     *
     * @return string
     */
    public function getEntretien()
    {
        return $this->entretien;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set entretien.
     *
     * @param string $entretien
     *
     * @return TblEntretiens
     */
    public function setEntretien($entretien)
    {
        $this->entretien = $entretien;

        return $this;
    }
}
