<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * FormationsBesoin.
 *
 * @ORM\Table(name="formations_besoin")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FormationsBesoin extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    public const PRIORITE_1 = 1;

    public const PRIORITE_2 = 2;

    public const PRIORITE_3 = 3;

    public const PRIORITE_4 = 4;

    public const PRIORITE_CONVERTOR = [
        0 => 'Pas choisi',
        1 => 'Priorité 1',
        2 => 'Priorité 2',
        3 => 'Priorité 3',
        4 => 'Priorité 4',
    ];

    public const PRIORITE_NO = 0;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @ORM\ManyToOne(targetEntity="TblThemesFormations")
     */
    private $formation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="priorite", type="smallint", nullable=true)
     */
    private $priorite;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get formation.
     *
     * @return \App\Entity\TblThemesFormations
     */
    public function getFormation()
    {
        return $this->formation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get priorite.
     *
     * @return int
     */
    public function getPriorite()
    {
        return $this->priorite;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return FormationsBesoin
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return FormationsBesoin
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set formation.
     *
     * @param \App\Entity\TblThemesFormations $formation
     *
     * @return FormationsBesoin
     */
    public function setFormation(?TblThemesFormations $formation = null)
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * Set priorite.
     *
     * @param int $priorite
     *
     * @return FormationsBesoin
     */
    public function setPriorite($priorite)
    {
        $this->priorite = $priorite;

        return $this;
    }
}
