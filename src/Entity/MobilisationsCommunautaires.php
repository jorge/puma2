<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * MobilisationsCommunautaires.
 *
 * @ORM\Table(name="mobilisations_communautaires")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class MobilisationsCommunautaires extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblActivitesCommunautaires")
     */
    private $mobilisation;

    /**
     * @ORM\ManyToOne(targetEntity="SuiviOrganisations")
     */
    private $suiviOrganisation;

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get mobilisation.
     *
     * @return \App\Entity\TblActivitesCommunautaires
     */
    public function getMobilisation()
    {
        return $this->mobilisation;
    }

    /**
     * Get suiviOrganisation.
     *
     * @return \App\Entity\SuiviOrganisations
     */
    public function getSuiviOrganisation()
    {
        return $this->suiviOrganisation;
    }

    /**
     * Set mobilisation.
     *
     * @param \App\Entity\TblActivitesCommunautaires $mobilisation
     *
     * @return MobilisationsCommunautaires
     */
    public function setMobilisation(?TblActivitesCommunautaires $mobilisation = null)
    {
        $this->mobilisation = $mobilisation;

        return $this;
    }

    /**
     * Set suiviOrganisation.
     *
     * @param \App\Entity\SuiviOrganisations $suiviOrganisation
     *
     * @return MobilisationsCommunautaires
     */
    public function setSuiviOrganisation(?SuiviOrganisations $suiviOrganisation = null)
    {
        $this->suiviOrganisation = $suiviOrganisation;

        return $this;
    }
}
