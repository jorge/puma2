<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

/**
 * ConsultationSignaletique.
 */
class ConsultationSignaletique
{
    protected $chargesNombreGroup;

    protected $chargesNombreMax;

    protected $chargesNombreMin;

    protected $chargesScolariseFilles;

    protected $chargesScolariseFils;

    protected $chargesTheme; // nombre-scolarise

    protected $Denomination;

    protected $exploitantAgeMax;

    protected $exploitantAgeMin;

    protected $exploitantEtatCivilGroup;

    protected $exploitantEtatCivilList;

    protected $exploitantGenreGroup;

    protected $exploitantGenreList; //m-f

    protected $exploitantTheme; // age-etatcivil-genre

    protected $exploitationGroupAnnee;

    protected $exploitationListAnnee;

    protected $groupecoop;

    protected $groupegroup;

    protected $queryTheme; //exploitation-exploitant-charges

    public function __construct()
    {
        $this->Denomination = null;
        $this->groupecoop = false;
        $this->groupegroup = false;
        $this->queryTheme = null;
        $this->exploitationGroupAnnee = true;
        $this->exploitationListAnnee = date('Y');

        $this->exploitantTheme = 'age'; // age-etatcivil-genre
        $this->exploitantAgeMin = 0;
        $this->exploitantAgeMax = 0;

        $this->exploitantEtatCivilGroup = false;
        $this->exploitantEtatCivilList = 0;

        $this->exploitantGenreGroup = true;
        $this->exploitantGenreList = 'masculin'; //m-f

        $this->chargesTheme = 'nombre'; // nombre-scolarise
        $this->chargesNombreGroup = true;
        $this->chargesNombreMin = 0;
        $this->chargesNombreMax = 0;

        $this->chargesScolariseFils = false;
        $this->chargesScolariseFilles = false;

        return $this;
    }

    public function getChargesNombreGroup()
    {
        return $this->chargesNombreGroup;
    }

    public function getChargesNombreMax()
    {
        return $this->chargesNombreMax;
    }

    public function getChargesNombreMin()
    {
        return $this->chargesNombreMin;
    }

    public function getChargesScolariseFilles()
    {
        return $this->chargesScolariseFilles;
    }

    public function getChargesScolariseFils()
    {
        return $this->chargesScolariseFils;
    }

    public function getChargesTheme()
    {
        return $this->chargesTheme;
    }

    public function getDenomination()
    {
        return $this->Denomination;
    }

    public function getExploitantAgeMax()
    {
        return $this->exploitantAgeMax;
    }

    public function getExploitantAgeMin()
    {
        return $this->exploitantAgeMin;
    }

    public function getExploitantEtatCivilGroup()
    {
        return $this->exploitantEtatCivilGroup;
    }

    public function getExploitantEtatCivilList()
    {
        return $this->exploitantEtatCivilList;
    }

    public function getExploitantGenreGroup()
    {
        return $this->exploitantGenreGroup;
    }

    public function getExploitantGenreList()
    {
        return $this->exploitantGenreList;
    }

    public function getExploitantTheme()
    {
        return $this->exploitantTheme;
    }

    public function getExploitationGroupAnnee()
    {
        return $this->exploitationGroupAnnee;
    }

    public function getExploitationListAnnee()
    {
        return $this->exploitationListAnnee;
    }

    public function getGroupecoop()
    {
        return $this->groupecoop;
    }

    public function getGroupegroup()
    {
        return $this->groupegroup;
    }

    public function getQueryTheme()
    {
        return $this->queryTheme;
    }

    public function setChargesNombreGroup($chargesNombreGroup = true)
    {
        $this->chargesNombreGroup = $chargesNombreGroup;

        return $this;
    }

    public function setChargesNombreMax($chargesNombreMax = null)
    {
        $this->chargesNombreMax = $chargesNombreMax;

        return $this;
    }

    public function setChargesNombreMin($chargesNombreMin = null)
    {
        $this->chargesNombreMin = $chargesNombreMin;

        return $this;
    }

    public function setChargesScolariseFilles($chargesScolariseFilles = null)
    {
        $this->chargesScolariseFilles = $chargesScolariseFilles;

        return $this;
    }

    public function setChargesScolariseFils($chargesScolariseFils = null)
    {
        $this->chargesScolariseFils = $chargesScolariseFils;

        return $this;
    }

    public function setChargesTheme($chargesTheme = 'nombre')
    {
        $this->chargesTheme = $chargesTheme;

        return $this;
    }

    /**
     * Set Denomination.
     *
     * @param \App\Entity\TblOrganisations $Denomination
     *
     * @return Exploitations
     */
    public function setDenomination(?TblOrganisations $Denomination = null)
    {
        $this->Denomination = $Denomination;

        return $this;
    }

    public function setExploitantAgeMax($exploitantAgeMax = null)
    {
        $this->exploitantAgeMax = $exploitantAgeMax;

        return $this;
    }

    public function setExploitantAgeMin($exploitantAgeMin = null)
    {
        $this->exploitantAgeMin = $exploitantAgeMin;

        return $this;
    }

    public function setExploitantEtatCivilGroup($exploitantEtatCivilGroup = null)
    {
        $this->exploitantEtatCivilGroup = $exploitantEtatCivilGroup;

        return $this;
    }

    public function setExploitantEtatCivilList($exploitantEtatCivilList = null)
    {
        $this->exploitantEtatCivilList = $exploitantEtatCivilList;

        return $this;
    }

    public function setExploitantGenreGroup($exploitantGenreGroup = null)
    {
        $this->exploitantGenreGroup = $exploitantGenreGroup;

        return $this;
    }

    public function setExploitantGenreList($exploitantGenreList = 'masculin')
    {
        $this->exploitantGenreList = $exploitantGenreList;

        return $this;
    }

    public function setExploitantTheme($exploitantTheme = 'age')
    {
        $this->exploitantTheme = $exploitantTheme;

        return $this;
    }

    public function setExploitationGroupAnnee($exploitationGroupAnnee = true)
    {
        $this->exploitationGroupAnnee = $exploitationGroupAnnee;

        return $this;
    }

    //exploitationListAnnee
    public function setExploitationListAnnee($exploitationListAnnee = null)
    {
        $this->exploitationListAnnee = $exploitationListAnnee;

        return $this;
    }

    public function setGroupecoop($groupecoop = false)
    {
        $this->groupecoop = $groupecoop;

        return $this;
    }

    public function setGroupegroup($groupegroup = false)
    {
        $this->groupegroup = $groupegroup;

        return $this;
    }

    public function setQueryTheme($queryTheme = 'exploitation')
    {
        $this->queryTheme = $queryTheme;

        return $this;
    }
}
