<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * Habitat.
 *
 * @ORM\Table(name="habitat")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Habitat extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="mur", type="smallint", nullable=true)
     */
    private $mur;

    /**
     * @var int
     *
     * @ORM\Column(name="nombre_pieces", type="smallint", nullable=true)
     */
    private $nombrePieces;

    /**
     * @var int
     *
     * @ORM\Column(name="toiture", type="smallint", nullable=true)
     */
    private $toiture;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get mur.
     *
     * @return int
     */
    public function getMur()
    {
        return $this->mur;
    }

    /**
     * Get nombrePieces.
     *
     * @return int
     */
    public function getNombrePieces()
    {
        return $this->nombrePieces;
    }

    /**
     * Get toiture.
     *
     * @return int
     */
    public function getToiture()
    {
        return $this->toiture;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return Habitat
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return Habitat
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set mur.
     *
     * @param int $mur
     *
     * @return Habitat
     */
    public function setMur($mur)
    {
        $this->mur = $mur;

        return $this;
    }

    /**
     * Set nombrePieces.
     *
     * @param int $nombrePieces
     *
     * @return Habitat
     */
    public function setNombrePieces($nombrePieces)
    {
        $this->nombrePieces = $nombrePieces;

        return $this;
    }

    /**
     * Set toiture.
     *
     * @param int $toiture
     *
     * @return Habitat
     */
    public function setToiture($toiture)
    {
        $this->toiture = $toiture;

        return $this;
    }
}
