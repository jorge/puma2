<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * ParcelleHistoriqueData.
 *
 * @ORM\MappedSuperclass
 */
class ParcelleHistoriqueRecordBase extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\ParcelleHistoriques")
     */
    protected $parcelleHistorique;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_execution", type="datetime", nullable=true)
     */
    private $dateExecution;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * Quantite de main d'oeuvre familaire (en Homme/Jour).
     *
     * @var string
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $quantiteMOFam;

    /**
     * Quantite de main d'oeuvre familaire (en Homme/Jour).
     *
     * @var string
     *
     * @ORM\Column(type="smallint", nullable=true)
     */
    private $quantiteMOSal;

    /**
     * @ORM\ManyToOne(targetEntity="TblUniteProduits")
     */
    private $unite;

    /**
     * Get dateExecution.
     *
     * @return DateTime
     */
    public function getDateExecution()
    {
        return $this->dateExecution;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get parcelleHistorique.
     *
     * @return \App\Entity\ParcelleHistoriques
     */
    public function getParcelleHistorique()
    {
        return $this->parcelleHistorique;
    }

    /**
     * Get quantiteMOFam.
     *
     * @return int
     */
    public function getQuantiteMOFam()
    {
        return $this->quantiteMOFam;
    }

    /**
     * Get quantiteMOSal.
     *
     * @return int
     */
    public function getQuantiteMOSal()
    {
        return $this->quantiteMOSal;
    }

    /**
     * Get unite.
     *
     * @return \App\Entity\TblUniteProduits
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set dateExecution.
     *
     * @param DateTime $dateExecution
     *
     * @return ParcelleHistoriqueRecordBase
     */
    public function setDateExecution($dateExecution)
    {
        $this->dateExecution = $dateExecution;

        return $this;
    }

    /**
     * Set parcelleHistorique.
     *
     * @param \App\Entity\ParcelleHistoriques $parcelleHistorique
     *
     * @return ParcelleHistoriques
     */
    public function setParcelleHistorique(?ParcelleHistoriques $parcelleHistorique = null)
    {
        $this->parcelleHistorique = $parcelleHistorique;

        return $this;
    }

    /**
     * Set quantiteMOFam.
     *
     * @param int $quantiteMOFam
     *
     * @return ParcelleHistoriqueRecordBase
     */
    public function setQuantiteMOFam($quantiteMOFam)
    {
        $this->quantiteMOFam = $quantiteMOFam;

        return $this;
    }

    /**
     * Set quantiteMOSal.
     *
     * @param int $quantiteMOSal
     *
     * @return ParcelleHistoriqueRecordBase
     */
    public function setQuantiteMOSal($quantiteMOSal)
    {
        $this->quantiteMOSal = $quantiteMOSal;

        return $this;
    }

    /**
     * Set unite.
     *
     * @param \App\Entity\TblUniteProduits $unite
     *
     * @return ParcelleHistoriqueRecordBase
     */
    public function setUnite(?TblUniteProduits $unite = null)
    {
        $this->unite = $unite;

        return $this;
    }
}
