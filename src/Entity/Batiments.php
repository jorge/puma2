<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

// TODO SUPPRESSION CHAMPS LONGUEUR, LARGEUR
/**
 * Batiments.
 *
 * @ORM\Table(name="batiments")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Batiments extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F1;

    /**
     * @ORM\Column(name="annee", type="smallint", length=4, nullable=false)
     */
    private int $annee;

    /**
     * @var string
     *
     * @ORM\Column(name="cout_construction", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $coutConstruction;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var float
     *
     * @ORM\Column(name="surface", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $surface;

    /**
     * @ORM\ManyToOne(targetEntity="TblInfrastructures")
     */
    private $type;

    public function __construct($user)
    {
        parent::__construct($user);

        if (!$this->radie) {
            $this->radie = FALSE;
        }
    }

    /**
     * Get annee.
     *
     */
    public function getAnnee(): int
    {
        return $this->annee;
    }

    /**
     * Get coutConstruction.
     *
     * @return string
     */
    public function getCoutConstruction()
    {
        return $this->coutConstruction;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get surface.
     *
     * @return string
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Get type.
     *
     * @return \App\Entity\TblInfrastructures
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return Batiments
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set coutConstruction.
     *
     * @param string $coutConstruction
     *
     * @return Batiments
     */
    public function setCoutConstruction($coutConstruction)
    {
        $this->coutConstruction = $coutConstruction;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return Batiments
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set surface.
     *
     * @param string $surface
     *
     * @return Batiments
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }

    /**
     * Set type.
     *
     * @param \App\Entity\TblInfrastructures $type
     *
     * @return Batiments
     */
    public function setType(?TblInfrastructures $type = null)
    {
        $this->type = $type;

        return $this;
    }
}
