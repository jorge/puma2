<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * PhytoDemande.
 *
 * @ORM\Table(name="phyto_demande")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PhytoDemande extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=20, nullable=true)
     */
    private $mois;

    /**
     * @ORM\ManyToOne(targetEntity="TblProduitPhyto")
     */
    private $phytosanitaire;

    /**
     * @var string
     *
     * @ORM\Column(name="quantite", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $quantite;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get mois.
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Get phytosanitaire.
     *
     * @return \App\Entity\TblProduitPhyto
     */
    public function getPhytosanitaire()
    {
        return $this->phytosanitaire;
    }

    /**
     * Get quantite.
     *
     * @return string
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return PhytoDemande
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return PhytoDemande
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set mois.
     *
     * @param string $mois
     *
     * @return PhytoDemande
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Set phytosanitaire.
     *
     * @param \App\Entity\TblProduitPhyto $phytosanitaire
     *
     * @return PhytoDemande
     */
    public function setPhytosanitaire(?TblProduitPhyto $phytosanitaire = null)
    {
        $this->phytosanitaire = $phytosanitaire;

        return $this;
    }

    /**
     * Set quantite.
     *
     * @param string $quantite
     *
     * @return PhytoDemande
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }
}
