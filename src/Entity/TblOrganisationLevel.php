<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblOrganisationLevel.
 *
 * @ORM\Table(name="tbl_organisation_level")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblOrganisationLevel extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    public const COOPERATIVE = 'coopérative';

    public const FEDERATION = 'féderation';

    public const GROUPEMENT = 'groupement';

    /**
     * @var string
     *
     * @ORM\Column(name="denomination", type="string", length=255, nullable=true)
     */
    private $denomination;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="level", type="integer", unique=true)
     */
    private $level;

    public function __toString(): string
    {
        return sprintf('%s', $this->getDenomination());
    }

    /**
     * Get denomination.
     *
     * @return string
     */
    public function getDenomination()
    {
        return $this->denomination;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get level.
     *
     * @return int
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set denomination.
     *
     * @param string $denomination
     *
     * @return TblOrganisationLevel
     */
    public function setDenomination($denomination)
    {
        $this->denomination = $denomination;

        return $this;
    }

    /**
     * Set level.
     *
     * @param int $level
     *
     * @return TblOrganisationLevel
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }
}
