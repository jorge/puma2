<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblActivitesCommunautaires.
 *
 * @ORM\Table(name="tbl_activites_communautaires")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblActivitesCommunautaires extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="activite_communautaire", type="string", length=255, nullable=true)
     */
    private $activiteCommunautaire;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    public function __toString(): string
    {
        return sprintf('%s', $this->activiteCommunautaire);
    }

    /**
     * Get activiteCommunautaire.
     *
     * @return string
     */
    public function getActiviteCommunautaire()
    {
        return $this->activiteCommunautaire;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set activiteCommunautaire.
     *
     * @param string $activiteCommunautaire
     *
     * @return TblActivitesCommunautaires
     */
    public function setActiviteCommunautaire($activiteCommunautaire)
    {
        $this->activiteCommunautaire = $activiteCommunautaire;

        return $this;
    }
}
