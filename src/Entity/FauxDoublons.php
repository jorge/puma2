<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FauxDoublons.
 *
 * @ORM\Table(name="faux_doublons")
 * @ORM\Entity
 */
class FauxDoublons
{
    /**
     * @var array
     *
     * @ORM\Column(name="exploitationIds", type="simple_array", unique=true)
     */
    private $exploitationIds;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * Get exploitationIds.
     *
     * @return array
     */
    public function getExploitationIds()
    {
        return $this->exploitationIds;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set exploitationIds.
     *
     * @param array $exploitationIds
     *
     * @return FauxDoublons
     */
    public function setExploitationIds($exploitationIds)
    {
        $this->exploitationIds = $exploitationIds;

        return $this;
    }
}
