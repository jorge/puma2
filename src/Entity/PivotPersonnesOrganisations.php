<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * PivotPersonnesOrganisations.
 *
 * @ORM\Table(name="pivot_personnes_organisations")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class PivotPersonnesOrganisations extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblOrganisations")
     */
    private $organisation;

    /**
     * @ORM\ManyToOne(targetEntity="Personnes")
     */
    private $personne;

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get organisation.
     *
     * @return \App\Entity\TblOrganisations
     */
    public function getOrganisation()
    {
        return $this->organisation;
    }

    /**
     * Get personne.
     *
     * @return \App\Entity\Personnes
     */
    public function getPersonne()
    {
        return $this->personne;
    }

    /**
     * Set organisation.
     *
     * @param \App\Entity\TblOrganisations $organisation
     *
     * @return PivotPersonnesOrganisations
     */
    public function setOrganisation(?TblOrganisations $organisation = null)
    {
        $this->organisation = $organisation;

        return $this;
    }

    /**
     * Set personne.
     *
     * @param \App\Entity\Personnes $personne
     *
     * @return PivotPersonnesOrganisations
     */
    public function setPersonne(?Personnes $personne = null)
    {
        $this->personne = $personne;

        return $this;
    }
}
