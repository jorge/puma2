<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * CommercialisationProduits.
 *
 * @ORM\Table(name="commercialisation_produits")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class CommercialisationProduits extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var string
     *
     * @ORM\Column(name="annee_saison", type="string", length=4, nullable=true)
     */
    private $anneeSaison;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_execution", type="datetime", nullable=true)
     */
    private $dateExecution;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=20, nullable=true)
     */
    private $mois;

    /**
     * @var string
     *
     * @ORM\Column(name="prix_ref_autoconsommation", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $prixRefAutoconsommation;

    /**
     * @var string
     *
     * @ORM\Column(name="prix_ref_semences", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $prixRefSemences;

    /**
     * @var string
     *
     * @ORM\Column(name="prix_vente", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $prixVente;

    /**
     * @ORM\ManyToOne(targetEntity="TblProduits")
     */
    private $produit;

    /**
     * @var string
     *
     * @ORM\Column(name="qte_autoconsommation", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $qteAutoconsommation;

    /**
     * @var string
     *
     * @ORM\Column(name="qte_semences", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $qteSemences;

    /**
     * @var string
     *
     * @ORM\Column(name="quantite", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $quantite;

    /**
     * @var string
     *
     * @ORM\Column(name="quantite_capad", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $quantiteCapad;

    /**
     * @ORM\ManyToOne(targetEntity="TblSaisons")
     */
    private $saison;

    /**
     * @ORM\ManyToOne(targetEntity="TblTypeCommercialisation")
     */
    private $typeCommercialisation;

    /**
     * @var string
     *
     * @ORM\Column(name="unite", type="string", length=32, nullable=true)
     */
    private $unite;

    public function __construct($user)
    {
        parent::__construct($user);
        $this->typeCommercialisation = new ArrayCollection();
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get anneeSaison.
     *
     * @return string
     */
    public function getAnneeSaison()
    {
        return $this->anneeSaison;
    }

    /**
     * Get dateExecution.
     *
     * @return DateTime
     */
    public function getDateExecution()
    {
        return $this->dateExecution;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get mois.
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Get prixRefAutoconsommation.
     *
     * @return string
     */
    public function getPrixRefAutoconsommation()
    {
        return $this->prixRefAutoconsommation;
    }

    /**
     * Get prixRefSemences.
     *
     * @return string
     */
    public function getPrixRefSemences()
    {
        return $this->prixRefSemences;
    }

    /**
     * Get prixVente.
     *
     * @return string
     */
    public function getPrixVente()
    {
        return $this->prixVente;
    }

    /**
     * Get produit.
     *
     * @return \App\Entity\TblProduits
     */
    public function getProduit()
    {
        return $this->produit;
    }

    /**
     * Get qteAutoconsommation.
     *
     * @return string
     */
    public function getQteAutoconsommation()
    {
        return $this->qteAutoconsommation;
    }

    /**
     * Get qteSemences.
     *
     * @return string
     */
    public function getQteSemences()
    {
        return $this->qteSemences;
    }

    /**
     * Get quantite.
     *
     * @return string
     */
    public function getQuantite()
    {
        return $this->quantite;
    }

    /**
     * Get quantiteCapad.
     *
     * @return string
     */
    public function getQuantiteCapad()
    {
        return $this->quantiteCapad;
    }

    /**
     * Get saison.
     *
     * @return \App\Entity\TblSaisons
     */
    public function getSaison()
    {
        return $this->saison;
    }

    /**
     * Get typeCommercialisation.
     *
     * @return \App\Entity\TblTypeCommercialisation
     */
    public function getTypeCommercialisation()
    {
        return $this->typeCommercialisation;
    }

    /**
     * Get unite.
     *
     * @return string
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return CommercialisationProduits
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set anneeSaison.
     *
     * @param string $anneeSaison
     *
     * @return CommercialisationProduits
     */
    public function setAnneeSaison($anneeSaison)
    {
        $this->anneeSaison = $anneeSaison;

        return $this;
    }

    /**
     * Set dateExecution.
     *
     * @param DateTime $dateExecution
     *
     * @return CommercialisationProduits
     */
    public function setDateExecution($dateExecution)
    {
        $this->dateExecution = $dateExecution;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return CommercialisationProduits
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set mois.
     *
     * @param string $mois
     *
     * @return CommercialisationProduits
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Set prixRefAutoconsommation.
     *
     * @param string $prixRefAutoconsommation
     *
     * @return CommercialisationProduits
     */
    public function setPrixRefAutoconsommation($prixRefAutoconsommation)
    {
        $this->prixRefAutoconsommation = $prixRefAutoconsommation;

        return $this;
    }

    /**
     * Set prixRefSemences.
     *
     * @param string $prixRefSemences
     *
     * @return CommercialisationProduits
     */
    public function setPrixRefSemences($prixRefSemences)
    {
        $this->prixRefSemences = $prixRefSemences;

        return $this;
    }

    /**
     * Set prixVente.
     *
     * @param string $prixVente
     *
     * @return CommercialisationProduits
     */
    public function setPrixVente($prixVente)
    {
        $this->prixVente = $prixVente;

        return $this;
    }

    /**
     * Set produit.
     *
     * @param \App\Entity\TblProduits $produit
     *
     * @return CommercialisationProduits
     */
    public function setProduit(?TblProduits $produit = null)
    {
        $this->produit = $produit;

        return $this;
    }

    /**
     * Set qteAutoconsommation.
     *
     * @param string $qteAutoconsommation
     *
     * @return CommercialisationProduits
     */
    public function setQteAutoconsommation($qteAutoconsommation)
    {
        $this->qteAutoconsommation = $qteAutoconsommation;

        return $this;
    }

    /**
     * Set qteSemences.
     *
     * @param string $qteSemences
     *
     * @return CommercialisationProduits
     */
    public function setQteSemences($qteSemences)
    {
        $this->qteSemences = $qteSemences;

        return $this;
    }

    /**
     * Set quantite.
     *
     * @param string $quantite
     *
     * @return CommercialisationProduits
     */
    public function setQuantite($quantite)
    {
        $this->quantite = $quantite;

        return $this;
    }

    /**
     * Set quantiteCapad.
     *
     * @param string $quantiteCapad
     *
     * @return CommercialisationProduits
     */
    public function setQuantiteCapad($quantiteCapad)
    {
        $this->quantiteCapad = $quantiteCapad;

        return $this;
    }

    /**
     * Set saison.
     *
     * @param \App\Entity\TblSaisons $saison
     *
     * @return CommercialisationProduits
     */
    public function setSaison(TblSaisons $saison)
    {
        $this->saison = $saison;

        return $this;
    }

    /**
     * Set typeCommercialisation.
     *
     * @param \App\Entity\TblTypeCommercialisation $typeCommercialisation
     *
     * @return CommercialisationProduits
     */
    public function setTypeCommercialisation(?TblTypeCommercialisation $typeCommercialisation = null)
    {
        $this->typeCommercialisation = $typeCommercialisation;

        return $this;
    }

        /**
     * Set unite
     *
     * @param string $unite
     *
     * @return AnimauxProduction
     */
    public function setUnite(string $unite) : CommercialisationProduits    {
        $this->unite = $unite;

        return $this;
    }
}
