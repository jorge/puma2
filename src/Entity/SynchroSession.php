<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

use function is_array;

/**
 * SynchroSession.
 *
 * Une session concerne un échange entre local et master === toutes les data entre deux datetimes
 *
 * TJS DU COTE LOCAL DE L'ORDI
 *
 * @ORM\Table(name="synchro_session")
 * @ORM\Entity
 */
class SynchroSession
{
    public const DATE_FORMAT = 'Y-m-d H:i:s';

    public const NBTRY_LIMIT = -1; // quand à -1 pas de limite

    public const STATUS_ARCHIVE_CREE = 'archive cree';

    public const STATUS_ARCHIVE_ENVOYEE = 'archive envoyee'; // FIN DE PHASE 1

    public const STATUS_ARCHIVE_KO = 'archive ko'; // CREATION ARCHIVE

    public const STATUS_FICHIERS_NON_RECUS = 'fichiers non-recus';

    public const STATUS_FICHIERS_RECUS = 'fichiers recus';

    public const STATUS_INIT_OK = 'init ok'; // ON DEMARRE LA SYNCHRO

    public const STATUS_LOAD_SQL_KO = 'load load sql ko';

    public const STATUS_LOAD_SQL_OK = 'load load sql ok'; // CHARGEMENT DU SQL DU REMOTE

    public const STATUS_READY = 'ready'; //par defaut (si pas de fichier existant / procédure en route)

    public const STATUS_REMOTE_INTROUVABLES = 'remote introuvables'; // FICHIERS REMOTES SONT INTROUVABLES (PAS TELECHARGES)

    public const STATUS_REMOTE_MD5_KO = 'remote md5 ko'; // QUAND ON RECOIT LE MD5

    public const STATUS_REMOTE_MD5_OK = 'remote md5 ok';

    public const STATUS_REMOTE_SESSION_FILE_CORRECT = 'fichiers remote lisibles'; // LES FICHIERS de session de remote sont OK

    public const STATUS_REMOTE_SESSION_FILE_CORROMPUS = 'fichiers remote corrompus'; // LES FICHIERS REçus de REMOTE POSENT PBM

    public const STATUS_REMOTE_SYNC_NOT_FINISH = 'sync de remote pas finie'; // REMOTE N'A PAS FINI SA SYNC

    public const STATUS_REMOTE_TELECHARGES = 'remote telecharges'; // FICHIER REMOTE ONT ETE TELECHARGES

    public const STATUS_REMOTE_ZIP_KO = 'remote zip ko';

    public const STATUS_REMOTE_ZIP_OK = 'remote zip ok'; // QUAND ON RECOIT LE ZIP

    public const STATUS_TO_INIT = 'to init'; // ON DOIT INITIALIZER LA SESSION

    /**
     * @var bool
     *
     * @ORM\Column(name="closed", type="boolean")
     */
    private $closed = false;

    /**
     * @var DateTime date au debut de laquelle on prend les données (pour
     * la session) et qui seront envoyées à l'autre ordi.
     *
     * @ORM\Column(name="date_debut_data", type="datetime", nullable=True)
     */
    private $dateDebutData;

    // TODO check que  null par defaut

    /**
     * @var DateTime date à la fin de laquelle on prend les données (pour
     * la session) et qui seront envoyées à l'autre ordi.
     *
     * @ORM\Column(name="date_fin_data", type="datetime", nullable=True)
     */
    private $dateFinData;

    // TODO check que  null par defaut avant: options={"default": "CURRENT_TIMESTAMP"}

    /**
     * @var DateTime date à la fin de la MAJ du sql (moment où on a chargé les
     * données de l'autre ordinateur), c'est à dire le chargement des données
     * se fait entre $dateFinData et $dateFinMajSql. Cette date est
     * >= $dateFinData.  Pour le master, la création des numéros de membre se
     * fait aussi entre ces deux dates.
     *
     * @ORM\Column(name="date_fin_maj_sql", type="datetime", nullable=True)
     */
    private $dateFinMajSql;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="master_date_debut_data", type="datetime", nullable=True)
     */
    private $masterDateDebutData;

    // TODO check que  null par defaut

    /**
     * @var DateTime
     *
     * @ORM\Column(name="master_date_fin_data", type="datetime", nullable=True)
     */
    private $masterDateFinData;

    // TODO check que  null par defaut avant: options={"default": "CURRENT_TIMESTAMP"}

    /**
     * @var DateTime
     *
     * @ORM\Column(name="master_date_fin_maj_sql", type="datetime", nullable=True)
     */
    private $masterDateFinMajSql;

    // TODO check que  null par defaut

    /**
     * @var int
     *
     * @ORM\Column(name="master_nbtry", type="integer")
     */
    private $masterNbtry = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="master_status", type="string", length=255)
     */
    private $masterStatus = self::STATUS_TO_INIT;

    /**
     * @var string
     *
     * @ORM\Column(name="master_zipmd5", type="string", length=255, nullable=True)
     */
    private $masterZipmd5;

    // TODO check que  null par defaut

    /**
     * @var int
     *
     * @ORM\Column(name="nbtry", type="integer", options={"default": 0})
     */
    private $nbtry = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="status", type="string", length=50)
     */
    private $status = self::STATUS_TO_INIT;

    /**
     * @var string
     *
     * @ORM\Column(name="zipmd5", type="string", length=255, nullable=True)
     */
    private $zipmd5;

    /**
     * Get closed.
     *
     * @return bool
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * Get dateDebutData.
     *
     * @return DateTime
     */
    public function getDateDebutData()
    {
        return $this->dateDebutData;
    }

    /**
     * Get dateFinData.
     *
     * @return DateTime
     */
    public function getDateFinData()
    {
        return $this->dateFinData;
    }

    /**
     * Get dateFinMajSql.
     *
     * @return DateTime
     */
    public function getDateFinMajSql()
    {
        return $this->dateFinMajSql;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get masterDateDebutData.
     *
     * @return DateTime
     */
    public function getMasterDateDebutData()
    {
        return $this->masterDateDebutData;
    }

    /**
     * Get masterDateFinData.
     *
     * @return DateTime
     */
    public function getMasterDateFinData()
    {
        return $this->masterDateFinData;
    }

    /**
     * Get masterDateFinMajSql.
     *
     * @return DateTime
     */
    public function getMasterDateFinMajSql()
    {
        return $this->masterDateFinMajSql;
    }

    /**
     * Get masterNbtry.
     *
     * @return int
     */
    public function getMasterNbtry()
    {
        return $this->masterNbtry;
    }

    /**
     * Get master session file name // quand session est mise en fichier pour echange.
     *
     * @return string
     */
    public function getMasterSessionFileName()
    {
        return self::masterSessionFileNameFor($this->getId());
    }

    /**
     * Get masterStatus.
     *
     * @return string
     */
    public function getMasterStatus()
    {
        return $this->masterStatus;
    }

    /**
     * Get masterZipmd5.
     *
     * @return string
     */
    public function getMasterZipmd5()
    {
        return $this->masterZipmd5;
    }

    /**
     * Get masterZipname.
     *
     * @return string
     */
    public function getMasterZipname()
    {
        return $this->getId() . '_master.zip';
    }

    /**
     * Get nbtry.
     *
     * @return int
     */
    public function getNbtry()
    {
        return $this->nbtry;
    }

    /**
     * Get session file name // quand session est mise en fichier pour echange (cote local).
     *
     * @return string
     */
    public function getSessionFileName()
    {
        return self::sessionFileNameFor($this->getId());
    }

    /**
     * Get status.
     *
     * @return string
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Get zipmd5.
     *
     * @return string
     */
    public function getZipmd5()
    {
        return $this->zipmd5;
    }

    /**
     * Get zipname.
     *
     * @return string
     */
    public function getZipname()
    {
        return self::zipNameFor($this->getId());
    }

    /**
     * Increment masterNbrTry (càd masterNbrTry devient masterNbrTry + 1).
     */
    public function incrementMasterNbtry()
    {
        return $this->setMasterNbtry($this->getMasterNbtry() + 1);
    }

    /**
     * Increment nbrTry (càd nbtry devient nbtry + 1).
     */
    public function incrementNbtry()
    {
        return $this->setNbtry($this->getNbtry() + 1);
    }

    public function inJSON()
    {
        return json_encode($this->toArray());
    }

    public static function loadFromFile($fullpathname)
    {
        $synchro_status_values = parse_ini_file($fullpathname);
        $newSession = new SynchroSession();
        $newSession->setId($synchro_status_values['session_id']);

        if ($synchro_status_values['date_debut_data']) {
            $newSession->setDateDebutData(
                DateTime::createFromFormat(
                    self::DATE_FORMAT,
                    $synchro_status_values['date_debut_data']
                )
            );
        } else {
            $newSession->setDateDebutData(null);
        }

        $newSession->setDateFinData(
            DateTime::createFromFormat(
                self::DATE_FORMAT,
                $synchro_status_values['date_fin_data']
            )
        );
        $newSession->setDateFinMajSql(
            DateTime::createFromFormat(
                self::DATE_FORMAT,
                $synchro_status_values['date_fin_maj_sql']
            )
        );

        $newSession->setNbtry($synchro_status_values['nbtry']);
        $newSession->setZipmd5($synchro_status_values['zipmd5']);
        $newSession->setStatus($synchro_status_values['status']);

        if ($synchro_status_values['master_date_debut_data']) {
            $newSession->setMasterDateDebutData(
                DateTime::createFromFormat(
                    self::DATE_FORMAT,
                    $synchro_status_values['master_date_debut_data']
                )
            );
        } else {
            $newSession->setDateDebutData(null);
        }

        $newSession->setMasterDateFinData(
            DateTime::createFromFormat(
                self::DATE_FORMAT,
                $synchro_status_values['master_date_fin_data']
            )
        );
        $newSession->setMasterDateFinMajSql(
            DateTime::createFromFormat(
                self::DATE_FORMAT,
                $synchro_status_values['master_date_fin_maj_sql']
            )
        );

        $newSession->setMasterNbtry($synchro_status_values['master_nbtry']);
        $newSession->setMasterZipmd5($synchro_status_values['master_zipmd5']);
        $newSession->setMasterStatus($synchro_status_values['master_status']);

        return $newSession;
    }

    /**
     * Nom du fichier d'echange de donnes de la session généré par le master.
     *
     * @param mixed $sessionId
     */
    public static function masterSessionFileNameFor($sessionId)
    {
        return $sessionId . '_master.txt';
    }

    public function read_file($fullpathname)
    {
        return parse_ini_file($fullpathname);
    }

    public function saveIntoAFile($fullFileName)
    {
        /**
         * Create a file of the session for ftp echange between local & maste.
         *
         * @param Strng $fullFileName L'endroit ou doit être stocké le fichier
         */
        $arrparams = $this->toArray();

        $res = [];

        foreach ($arrparams as $key => $val) { // TODO ICI QUAND STRING EST NULL METTRRE NULL
            if (is_array($val)) { // TODO PBM POUR LES ARRAY $skey n'existe pas
                $res[] = "[{$key}]";

                foreach ($val as $skey => $sval) {
                    if (is_numeric($sval)) {
                        $res[] = "{$skey} = " . $sval;
                    } elseif (null === $sval) {
                        $res[] = "{$skey} = null";
                    } else {
                        $res[] = "{$skey} = " . '"' . $sval . '"';
                    }
                }
            } else {
                if (is_numeric($val)) {
                    $res[] = "{$key} = " . $val;
                } elseif (null === $val) {
                    $res[] = "{$key} = null";
                } else {
                    $res[] = "{$key} = " . '"' . $val . '"';
                }
            }
        }

        file_put_contents($fullFileName, implode("\r\n", $res));
    }

    /**
     * Nom du fichier d'echange de donnes de la session généré par le local.
     *
     * @param mixed $sessionId
     */
    public static function sessionFileNameFor($sessionId)
    {
        return $sessionId . '.txt';
    }

    /**
     * Set closed.
     *
     * @param bool $closed
     *
     * @return SynchroSession
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Set dateDebutData.
     *
     * @param DateTime $dateDebutData
     *
     * @return SynchroSession
     */
    public function setDateDebutData($dateDebutData)
    {
        $this->dateDebutData = $dateDebutData;

        return $this;
    }

    /**
     * Set dateFinData.
     *
     * @param DateTime $dateFinData
     *
     * @return SynchroSession
     */
    public function setDateFinData($dateFinData)
    {
        $this->dateFinData = $dateFinData;

        return $this;
    }

    /**
     * Set dateFinMajSql.
     *
     * @param DateTime $dateFinMajSql
     *
     * @return SynchroSession
     */
    public function setDateFinMajSql($dateFinMajSql)
    {
        $this->dateFinMajSql = $dateFinMajSql;

        return $this;
    }

    /**
     * Set id.
     *
     * @param $id guid
     */
    public function setId($id)
    {
        return $this->id = $id;

        return $this;
    }

    /**
     * Set masterDateDebutData.
     *
     * @param DateTime $masterDateDebutData
     *
     * @return SynchroSession
     */
    public function setMasterDateDebutData($masterDateDebutData)
    {
        $this->masterDateDebutData = $masterDateDebutData;

        return $this;
    }

    /**
     * Set masterDateFinData.
     *
     * @param DateTime $masterDateFinData
     *
     * @return SynchroSession
     */
    public function setMasterDateFinData($masterDateFinData)
    {
        $this->masterDateFinData = $masterDateFinData;

        return $this;
    }

    /**
     * Set masterDateFinMajSql.
     *
     * @param DateTime $masterDateFinMajSql
     *
     * @return SynchroSession
     */
    public function setMasterDateFinMajSql($masterDateFinMajSql)
    {
        $this->masterDateFinMajSql = $masterDateFinMajSql;

        return $this;
    }

    /**
     * Set masterNbtry.
     *
     * @param int $masterNbtry
     *
     * @return SynchroSession
     */
    public function setMasterNbtry($masterNbtry)
    {
        $this->masterNbtry = $masterNbtry;

        return $this;
    }

    /**
     * Set masterStatus.
     *
     * @param string $masterStatus
     *
     * @return SynchroSession
     */
    public function setMasterStatus($masterStatus)
    {
        $this->masterStatus = $masterStatus;

        return $this;
    }

    /**
     * Set masterZipmd5.
     *
     * @param string $masterZipmd5
     *
     * @return SynchroSession
     */
    public function setMasterZipmd5($masterZipmd5)
    {
        $this->masterZipmd5 = $masterZipmd5;

        return $this;
    }

    /**
     * Set nbtry.
     *
     * @param int $nbtry
     *
     * @return SynchroSession
     */
    public function setNbtry($nbtry)
    {
        $this->nbtry = $nbtry;

        return $this;
    }

    /**
     * Set status.
     *
     * @param string $status
     *
     * @return SynchroSession
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Set zipmd5.
     *
     * @param string $zipmd5
     *
     * @return SynchroSession
     */
    public function setZipmd5($zipmd5)
    {
        $this->zipmd5 = $zipmd5;

        return $this;
    }

    public function toArray()
    {
        return [
            'session_id' => $this->getId(),
            'date_debut_data' => $this->nullOrStringDate($this->getDateDebutData()),
            'date_fin_data' => $this->nullOrStringDate($this->getDateFinData()),
            'date_fin_maj_sql' => $this->nullOrStringDate($this->getDateFinMajSql()),
            'nbtry' => $this->getNbTry(),
            'zipmd5' => $this->getZipmd5(),
            'status' => $this->getStatus(),
            'master_date_debut_data' => $this->nullOrStringDate($this->getMasterDateDebutData()),
            'master_date_fin_data' => $this->nullOrStringDate($this->getMasterDateFinData()),
            'master_date_fin_maj_sql' => $this->nullOrStringDate($this->getMasterDateFinMajSql()),
            'master_nbtry' => $this->getMasterNbTry(),
            'master_zipmd5' => $this->getMasterZipmd5(),
            'master_status' => $this->getMasterStatus(), ];
    }

    public static function zipNameFor($sessionId)
    {
        return $sessionId . '.zip';
    }

    private function nullOrStringDate($date)
    {
        if ($date) {
            return date_format($date, self::DATE_FORMAT);
        }

        return $date;
    }
}
