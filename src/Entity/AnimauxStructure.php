<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * AnimauxStructure.
 *
 * @ORM\Table(name="animaux_structure")
 * @ORM\Entity(repositoryClass="App\Repository\AnimauxStructureRepository")
 * @ORM\HasLifecycleCallbacks
 */
class AnimauxStructure extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="Animaux")
     */
    private $animaux;

    /**
     * @ORM\ManyToOne(targetEntity="TblAnimauxMouvements")
     */
    private $animauxTechnique;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date_execution", type="datetime", nullable=true)
     */
    private $dateExecution;

    /**
     * @var int
     *
     * @ORM\Column(name="femelles_reproductrices", type="integer", nullable=true)
     */
    private $femellesReproductrices;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="jeunes_femelles", type="integer", nullable=true)
     */
    private $jeunesFemelles;

    /**
     * @var int
     *
     * @ORM\Column(name="jeunes_immatures", type="integer", nullable=true)
     */
    private $jeunesImmatures;

    /**
     * @var int
     *
     * @ORM\Column(name="jeunes_males", type="integer", nullable=true)
     */
    private $jeunesMales;

    /**
     * @var int
     *
     * @ORM\Column(name="males_reproducteurs", type="integer", nullable=true)
     */
    private $malesReproducteurs;

    /**
     * @var int
     *
     * @ORM\Column(name="total", type="integer", nullable=true)
     */
    private $total;

    /**
     * Get animaux.
     *
     * @return \App\Entity\Animaux
     */
    public function getAnimaux()
    {
        return $this->animaux;
    }

    /**
     * Get animauxTechnique.
     *
     * @return \App\Entity\TblAnimauxMouvements
     */
    public function getAnimauxTechnique()
    {
        return $this->animauxTechnique;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get dateExecution.
     *
     * @return DateTime
     */
    public function getDateExecution()
    {
        return $this->dateExecution;
    }

    /**
     * Get femellesReproductrices.
     *
     * @return int
     */
    public function getFemellesReproductrices()
    {
        return $this->femellesReproductrices;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get jeunesFemelles.
     *
     * @return int
     */
    public function getJeunesFemelles()
    {
        return $this->jeunesFemelles;
    }

    /**
     * Get jeunesImmatures.
     *
     * @return int
     */
    public function getJeunesImmatures()
    {
        return $this->jeunesImmatures;
    }

    /**
     * Get jeunesMales.
     *
     * @return int
     */
    public function getJeunesMales()
    {
        return $this->jeunesMales;
    }

    /**
     * Get malesReproducteurs.
     *
     * @return int
     */
    public function getMalesReproducteurs()
    {
        return $this->malesReproducteurs;
    }

    /**
     * Get total.
     *
     * @return int
     */
    public function getTotal()
    {
        return $this->total;
    }

    /**
     * Set animaux.
     *
     * @param \App\Entity\Animaux $animaux
     *
     * @return AnimauxStructure
     */
    public function setAnimaux(?Animaux $animaux = null)
    {
        $this->animaux = $animaux;

        return $this;
    }

    /**
     * Set animauxTechnique.
     *
     * @param \App\Entity\TblAnimauxMouvements $animauxTechnique
     *
     * @return AnimauxStructure
     */
    public function setAnimauxTechnique(?TblAnimauxMouvements $animauxTechnique = null)
    {
        $this->animauxTechnique = $animauxTechnique;

        return $this;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return AnimauxStructure
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set dateExecution.
     *
     * @param DateTime $dateExecution
     *
     * @return AnimauxStructure
     */
    public function setDateExecution($dateExecution)
    {
        $this->dateExecution = $dateExecution;

        return $this;
    }

    /**
     * Set femellesReproductrices.
     *
     * @param int $femellesReproductrices
     *
     * @return AnimauxStructure
     */
    public function setFemellesReproductrices($femellesReproductrices)
    {
        $this->femellesReproductrices = $femellesReproductrices;

        return $this;
    }

    /**
     * Set jeunesFemelles.
     *
     * @param int $jeunesFemelles
     *
     * @return AnimauxStructure
     */
    public function setJeunesFemelles($jeunesFemelles)
    {
        $this->jeunesFemelles = $jeunesFemelles;

        return $this;
    }

    /**
     * Set jeunesImmatures.
     *
     * @param int $jeunesImmatures
     *
     * @return AnimauxStructure
     */
    public function setJeunesImmatures($jeunesImmatures)
    {
        $this->jeunesImmatures = $jeunesImmatures;

        return $this;
    }

    /**
     * Set jeunesMales.
     *
     * @param int $jeunesMales
     *
     * @return AnimauxStructure
     */
    public function setJeunesMales($jeunesMales)
    {
        $this->jeunesMales = $jeunesMales;

        return $this;
    }

    /**
     * Set malesReproducteurs.
     *
     * @param int $malesReproducteurs
     *
     * @return AnimauxStructure
     */
    public function setMalesReproducteurs($malesReproducteurs)
    {
        $this->malesReproducteurs = $malesReproducteurs;

        return $this;
    }

    /**
     * Set total.
     *
     * @param int $total
     *
     * @return AnimauxStructure
     */
    public function setTotal($total)
    {
        $this->total = $total;

        return $this;
    }
}
