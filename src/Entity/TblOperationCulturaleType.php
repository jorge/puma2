<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;
use stdClass;

/**
 * TblOperationCulturaleType.
 *
 * @ORM\Table(name="tbl_operation_culturale_type")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblOperationCulturaleType extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    public const TYPE_AVEC_EXTRANT = 2;

    public const TYPE_AVEC_INTRANT = 1;

    public const TYPE_SEUL = 0;

    /**
     * @var bool Pour indiquer si l'operation est "agro ecologique"
     *
     * @ORM\Column(name="agro_eco", type="boolean", options={"default": false})
     */
    private $agroEco;

    /**
     * @var stdClass
     *
     * @ORM\ManyToOne(targetEntity="TblOperationCulturaleTypeCategory")
     */
    private $category;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255, unique=true)
     */
    private $nom;

    /**
     * @var int Savoir si c'est une recolte, l'utilisation d'un produit ou sans rien (sert à paramétrer le formulaire)
     *
     *  il y a 3 types :
     *   - avec intrant (+ liste des intrant (genre composte, ....))
     *   - avec extrant (récolte) + liste de la récolte
     *   - ni intrant, ni extrant
     *
     * @ORM\Column(type="smallint")
     */
    private $type;

    /**
     * @ORM\ManyToOne(targetEntity="TblUniteProduits")
     */
    private $unite; // todo supprimer or delete

    public function __toString(): string
    {
        return $this->nom;
    }

    /**
     * Get surType.
     *
     * @return TblOperationCulturaleTypeCategory
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    public function getStrType()
    {
        if (0 === $this->type) {
            $ret = 'seul';
        } elseif (1 === $this->type) {
            $ret = 'intrant';
        } elseif ($this->type = 2) {
            $ret = 'extrant';
        }

        return $ret;
    }

    /**
     * Get type.
     *
     * @return int
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Get unite.
     *
     * @return \App\Entity\TblUniteProduits
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Is the op-cult is agro-ecologique ?
     *
     * @return bool
     */
    public function isAgroEco()
    {
        return $this->agroEco;
    }

    /**
     * Set if the op-cult is agro-ecologique.
     *
     * @param bool $agroEco
     *
     * @return TblOperationCulturaleType
     */
    public function setAgroEco($agroEco)
    {
        $this->agroEco = $agroEco;

        return $this;
    }

    /**
     * Set category.
     *
     * @param \App\Entity\TblOperationCulturaleTypeCategory|null $category
     *
     * @return TblOperationCulturaleType
     */
    public function setCategory(?TblOperationCulturaleTypeCategory $category = null)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return TblOperationCulturaleType
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Set surType.
     *
     * @param mixed $category
     *
     * @return TblOperationCulturaleType
     */
    public function setSurType($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Set type.
     *
     * @param int $type
     *
     * @return TblOperationCulturaleType
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Set unite.
     *
     * @param \App\Entity\TblUniteProduits $unite
     *
     * @return TblOperationCulturaleType
     */
    public function setUnite(?TblUniteProduits $unite = null)
    {
        $this->unite = $unite;

        return $this;
    }
}
