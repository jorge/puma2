<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * SemenceDemandee.
 *
 * @ORM\Table(name="semence_demandee")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class SemenceDemandee extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var string
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="mois", type="string", length=20, nullable=true)
     */
    private $mois;

    /**
     * @var decimal
     *
     * @ORM\Column(name="quantite_semence", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $quantiteSemence;

    /**
     * @ORM\ManyToOne(targetEntity="TblSemences")
     */
    private $semence;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get mois.
     *
     * @return string
     */
    public function getMois()
    {
        return $this->mois;
    }

    /**
     * Get quantiteSemence.
     *
     * @return string
     */
    public function getQuantiteSemence()
    {
        return $this->quantiteSemence;
    }

    /**
     * Get semence.
     *
     * @return \App\Entity\TblSemences
     */
    public function getSemence()
    {
        return $this->semence;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return SemenceDemandee
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return SemenceDemandee
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set mois.
     *
     * @param string $mois
     *
     * @return SemenceDemandee
     */
    public function setMois($mois)
    {
        $this->mois = $mois;

        return $this;
    }

    /**
     * Set quantiteSemence.
     *
     * @param string $quantiteSemence
     *
     * @return SemenceDemandee
     */
    public function setQuantiteSemence($quantiteSemence)
    {
        $this->quantiteSemence = $quantiteSemence;

        return $this;
    }

    /**
     * Set semence.
     *
     * @param \App\Entity\TblSemences $semence
     *
     * @return SemenceDemandee
     */
    public function setSemence(?TblSemences $semence = null)
    {
        $this->semence = $semence;

        return $this;
    }
}
