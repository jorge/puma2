<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
// use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Cotisations.
 *
 * @ORM\Table(name="cotisations", uniqueConstraints={@ORM\UniqueConstraint(name="unique_idx",
 * columns={"pivot_exploitation_organisation_id", "annee_cotisation"}) })
 * @ORM\Entity(repositoryClass="App\Repository\CotisationsRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Cotisations extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string Annee de l'enquête effectuée pour récolter les données
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var string Année de la cotisation (payé / impayée)
     *
     * @ORM\Column(name="annee_cotisation", type="string", length=4, nullable=true)
     */
    private $anneeCotisation;

    /**
     * @var string
     * @Assert\Regex("/^\d+(\.\d+)?/")
     *
     * @ORM\Column(name="cotisations_impaye", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $cotisationsImpaye;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     * @Assert\Regex("/^\d+(\.\d+)?/")
     * @ORM\Column(name="paye_annee", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $payeAnnee;

    /**
     * @var int
     *
     * @ORM\Column(name="paye_regulierement", type="smallint", options={"default": 0})
     */
    private $payeRegulierement;

    /**
     * @ORM\ManyToOne(targetEntity="PivotExploitationOrganisations")
     */
    private $pivotExploitationOrganisation;

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get cotisationsImpaye.
     *
     * @return string
     */
    public function getCotisationsImpaye()
    {
        return $this->cotisationsImpaye;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get payeAnnee.
     *
     * @return string
     */
    public function getPayeAnnee()
    {
        return $this->payeAnnee;
    }

    /**
     * Get payeRegulierement.
     *
     * @return int
     */
    public function getPayeRegulierement()
    {
        return $this->payeRegulierement;
    }

    /**
     * Get pivotExploitationOrganisation.
     *
     * @return \App\Entity\PivotExploitationOrganisations
     */
    public function getPivotExploitationOrganisation()
    {
        return $this->pivotExploitationOrganisation;
    }

    /**
     * Get exploitations.
     */
    public function getExploitation(): Exploitations
    {
        return $this->pivotExploitationOrganisation->getExploitation();
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return Cotisations
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set cotisationsImpaye.
     *
     * @param string $cotisationsImpaye
     *
     * @return Cotisations
     */
    public function setCotisationsImpaye($cotisationsImpaye)
    {
        $this->cotisationsImpaye = $cotisationsImpaye;

        return $this;
    }

    /**
     * Set payeAnnee.
     *
     * @param string $payeAnnee
     *
     * @return Cotisations
     */
    public function setPayeAnnee($payeAnnee)
    {
        $this->payeAnnee = $payeAnnee;

        return $this;
    }

    /**
     * Set payeRegulierement.
     *
     * @param int $payeRegulierement
     *
     * @return Cotisations
     */
    public function setPayeRegulierement($payeRegulierement)
    {
        $this->payeRegulierement = $payeRegulierement;

        return $this;
    }

    /**
     * Set pivotExploitationOrganisation.
     *
     * @return Cotisations
     */
//    public function setPivotExploitationOrganisation(\App\Entity\PivotExploitationOrganisations $pivotExploitationOrganisation = null)
    public function setPivotExploitationOrganisation(PivotExploitationOrganisations $pivotExploitationOrganisation)
    {
        $this->pivotExploitationOrganisation = $pivotExploitationOrganisation;

        return $this;
    }
}
