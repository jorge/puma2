<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblVarieteSemences.
 *
 * @ORM\Table(name="tbl_variete_semences")
 * @ORM\Entity(repositoryClass="App\Repository\TblVarieteSemencesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TblVarieteSemences extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="TblCultures")
     */
    private $culture;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=100)
     */
    private $nom;

    /**
     * @ORM\ManyToOne(targetEntity="TblUniteProduits")
     */
    private $unite;

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        return $this->nom . ' (' . $this->unite . ')';
    }

    /**
     * Get culture.
     *
     * @return \App\Entity\TblCultures
     */
    public function getCulture()
    {
        return $this->culture;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get unite.
     *
     * @return \App\Entity\TblUniteProduits
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set culture.
     *
     * @param \App\Entity\TblCultures $culture
     *
     * @return TblVarieteSemences
     */
    public function setCulture(TblCultures $culture)
    {
        $this->culture = $culture;

        return $this;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return TblVarieteSemences
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Set unite.
     *
     * @param \App\Entity\TblUniteProduits $unite
     *
     * @return TblVarieteSemences
     */
    public function setUnite(TblUniteProduits $unite)
    {
        $this->unite = $unite;

        return $this;
    }
}
