<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblOperationCulturaleIntrant.
 *
 * @ORM\Table(name="tbl_operation_culturale_intrant")
 * @ORM\Entity
 */
class TblOperationCulturaleIntrant extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="nom", type="string", length=255)
     */
    private $nom;

    /**
     * @var TblOperationCulturaleType // pour asocier à un type d'op-cu
     *
     * @ORM\ManyToMany(targetEntity="TblOperationCulturaleType")
     */
    private $operationCulturaleType;

    /**
     * @var TblOperationCulturaleTypeCategory // pour associer à une catégorie de types d'op-cu
     *
     * @ORM\ManyToOne(targetEntity="TblOperationCulturaleTypeCategory")
     * @ORM\JoinColumn(name="operation_culturale_category_id", referencedColumnName="id")
     */
    private $operationCulturaleTypeCategory;

    /**
     * @ORM\ManyToOne(targetEntity="TblUniteProduits")
     */
    private $unite;

    public function __toString(): string
    {
        return $this->nom;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get nom.
     *
     * @return string
     */
    public function getNom()
    {
        return $this->nom;
    }

    /**
     * Get nom avec unité.
     */
    public function getNomAvecUnite()
    {
        return sprintf('%s (unité : %s)', $this->nom, $this->unite);
    }

    /**
     * Get operationCulturaleType.
     *
     * @return TblOperationCulturaleType
     */
    public function getOperationCulturaleType()
    {
        return $this->operationCulturaleType;
    }

    /**
     * Get operationCulturale.
     *
     * @return TblOperationCulturaleTypeCategory
     */
    public function getOperationCulturaleTypeCategory()
    {
        return $this->operationCulturaleTypeCategory;
    }

    /**
     * Get unite.
     *
     * @return TblUniteProduit
     */
    public function getUnite()
    {
        return $this->unite;
    }

    /**
     * Set nom.
     *
     * @param string $nom
     *
     * @return TblOperationCulturaleIntrant
     */
    public function setNom($nom)
    {
        $this->nom = $nom;

        return $this;
    }

    /**
     * Set operationCulturaleType.
     *
     * @param TblOperationCulturaleType $operationCulturaleType
     *
     * @return TblOperationCulturaleIntrant
     */
    public function setOperationCulturaleType($operationCulturaleType)
    {
        $this->operationCulturaleType = $operationCulturaleType;

        return $this;
    }

    /**
     * Set operationCulturale.
     *
     * @param TblOperationCulturaleTypeCategory $operationCulturaleTypeCategory
     *
     * @return TblOperationCulturaleIntrant
     */
    public function setOperationCulturaleTypeCategory($operationCulturaleTypeCategory)
    {
        $this->operationCulturaleTypeCategory = $operationCulturaleTypeCategory;

        return $this;
    }

    /**
     * Set unite.
     *
     * @param TblUniteProduit $unite
     *
     * @return TblOperationCulturaleIntrant
     */
}
