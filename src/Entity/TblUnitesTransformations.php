<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblUnitesTransformations.
 *
 * @ORM\Table(name="tbl_unites_transformations")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class TblUnitesTransformations extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="unite_transformation", type="string", length=255, nullable=true)
     */
    private $uniteTransformation;

    public function __toString(): string
    {
        return sprintf('%s', $this->uniteTransformation);
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get uniteTransformation.
     *
     * @return string
     */
    public function getUniteTransformation()
    {
        return $this->uniteTransformation;
    }

    /**
     * Set uniteTransformation.
     *
     * @param string $uniteTransformation
     *
     * @return TblUnitesTransformations
     */
    public function setUniteTransformation($uniteTransformation)
    {
        $this->uniteTransformation = $uniteTransformation;

        return $this;
    }
}
