<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * Un troupeau (ensemble d'animaux).
 *
 * @ORM\Table(name="animaux")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class Animaux extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @var float
     *
     * @ORM\Column(type="decimal", nullable=true)
     */
    protected $ageReformeFemelles;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F3;

    /**
     * @ORM\ManyToMany(targetEntity="TblAlimentationBetail")
     */
    private $alimentations;

    /**
     * @var string
     * L'année qui est associé au troupeau (pour cette année les données
     * récoltées sont correctes (du moins à a fin de l'année))
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @ORM\ManyToOne(targetEntity="TblCultures")
     */
    private $espece;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $femellesReproductrices;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $jeunesFemelles;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $jeunesImmatures;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $jeunesMales;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\AnimauxLabour", mappedBy="animaux")
     * @ORM\OrderBy({"labour": "ASC"})
     */
    private $labours;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $malesReproducteurs;

    /**
     * @var int
     *
     * @ORM\Column(type="integer", nullable=true)
     */
    private $nbrTotalTetes;

    /**
     * @ORM\ManyToMany(targetEntity="TblProduits")
     */
    private $produits;

    /**
     * @ORM\ManyToOne(targetEntity="TblTypesElevage")
     */
    private $typeElevage;

    public function __construct($user)
    {
        parent::__construct($user);
        $this->alimentations = new ArrayCollection();
        $this->produits = new ArrayCollection();
    }

    /**
     * Add alimentation.
     *
     * @return Animaux
     */
    public function addAlimentation(TblAlimentationBetail $alimentation)
    {
        $this->alimentations[] = $alimentation;

        return $this;
    }

    /**
     * Add labour.
     *
     * @param \App\Entity\AnimauxLabour $labour
     *
     * @return Animaux
     */
    public function addLabour(AnimauxLabour $labour)
    {
        $this->labours[] = $labour;

        return $this;
    }

    /**
     * Add produit.
     *
     * @return Animaux
     */
    public function addProduit(TblProduits $animauxProduit)
    {
        $this->produits[] = $animauxProduit;

        return $this;
    }

    /**
     * Get ageReformeFemelles.
     *
     * @return float
     */
    public function getAgeReformeFemelles()
    {
        return $this->ageReformeFemelles;
    }

    /**
     * Get alimentation.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAlimentations()
    {
        return $this->alimentations;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get espece.
     *
     * @return \App\Entity\TblCultures
     */
    public function getEspece()
    {
        return $this->espece;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get femellesReproductrices.
     *
     * @return int
     */
    public function getFemellesReproductrices()
    {
        return $this->femellesReproductrices;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get jeunesFemelles.
     *
     * @return int
     */
    public function getJeunesFemelles()
    {
        return $this->jeunesFemelles;
    }

    /**
     * Get jeunesImmatures.
     *
     * @return int
     */
    public function getJeunesImmatures()
    {
        return $this->jeunesImmatures;
    }

    /**
     * Get jeunesMales.
     *
     * @return int
     */
    public function getJeunesMales()
    {
        return $this->jeunesMales;
    }

    /**
     * Get labours.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getLabours()
    {
        return $this->labours;
    }

    /**
     * Get malesReproducteurs.
     *
     * @return int
     */
    public function getMalesReproducteurs()
    {
        return $this->malesReproducteurs;
    }

    /**
     * Get nbrTotalTetes.
     *
     * @return int
     */
    public function getNbrTotalTetes()
    {
        return $this->nbrTotalTetes;
    }

    /**
     * Get produits.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Get typeElevage.
     *
     * @return \App\Entity\TblTypesElevage
     */
    public function getTypeElevage()
    {
        return $this->typeElevage;
    }

    /**
     * Remove alimentation.
     *
     * @param alimentation $alimentation
     */
    public function removeAlimentation(TblAlimentationBetail $alimentation)
    {
        $this->alimentations->removeElement($alimentation);
    }

    /**
     * Remove labour.
     *
     * @param \App\Entity\AnimauxLabour $labour
     */
    public function removeLabour(AnimauxLabour $labour)
    {
        $this->labours->removeElement($labour);
    }

    /**
     * Remove produit.
     */
    public function removeProduit(TblProduits $animauxProduit)
    {
        $this->produits->removeElement($animauxProduit);
    }

    /**
     * Set age Reforme Femelles Reproductrices.
     *
     * @param float $ageReformeFemelles
     *
     * @return Animaux
     */
    public function setAgeReformeFemelles($ageReformeFemelles)
    {
        $this->ageReformeFemelles = $ageReformeFemelles;

        return $this;
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return Animaux
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set espece.
     *
     * @param \App\Entity\TblCultures $espece
     *
     * @return Animaux
     */
    public function setEspece(?TblCultures $espece = null)
    {
        $this->espece = $espece;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return Animaux
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set femellesReproductrices.
     *
     * @param int $femellesReproductrices
     *
     * @return Animaux
     */
    public function setFemellesReproductrices($femellesReproductrices)
    {
        $this->femellesReproductrices = $femellesReproductrices;

        return $this;
    }

    /**
     * Set jeunesFemelles.
     *
     * @param int $jeunesFemelles
     *
     * @return Animaux
     */
    public function setJeunesFemelles($jeunesFemelles)
    {
        $this->jeunesFemelles = $jeunesFemelles;

        return $this;
    }

    /**
     * Set jeunesImmatures.
     *
     * @param int $jeunesImmatures
     *
     * @return Animaux
     */
    public function setJeunesImmatures($jeunesImmatures)
    {
        $this->jeunesImmatures = $jeunesImmatures;

        return $this;
    }

    /**
     * Set jeunesMales.
     *
     * @param int $jeunesMales
     *
     * @return Animaux
     */
    public function setJeunesMales($jeunesMales)
    {
        $this->jeunesMales = $jeunesMales;

        return $this;
    }

    /**
     * Set malesReproducteurs.
     *
     * @param int $malesReproducteurs
     *
     * @return Animaux
     */
    public function setMalesReproducteurs($malesReproducteurs)
    {
        $this->malesReproducteurs = $malesReproducteurs;

        return $this;
    }

    /**
     * Set nbrTotalTetes.
     *
     * @param int $nbrTotalTetes
     *
     * @return Animaux
     */
    public function setNbrTotalTetes($nbrTotalTetes)
    {
        $this->nbrTotalTetes = $nbrTotalTetes;

        return $this;
    }

    /**
     * Set produits.
     *
     * @param \App\Entity\TblProduits $produits
     *
     * @return Animaux
     */
    public function setProduits(?TblProduits $produits = null)
    {
        $this->produits = $produits;

        return $this;
    }

    /**
     * Set typeElevage.
     *
     * @param \App\Entity\TblTypesElevage $typeElevage
     *
     * @return Animaux
     */
    public function setTypeElevage(?TblTypesElevage $typeElevage = null)
    {
        $this->typeElevage = $typeElevage;

        return $this;
    }
}
