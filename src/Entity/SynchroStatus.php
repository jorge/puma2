<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Exception;

/**
 * SynchroStatus.
 *
 * Cette classse permet d'initialiser les sessions de syncho :
 * - masterLastUdate contient (via update de la dernière date où il a mis ses données à jour)
 * - quand localInitialized est à vrai, localInitUdate contient la date du la première utilisation
 * de local
 * la premire session a juste besoin d'utiliser le masterLastUdate et le localInitUpdate pour
 * commencer.
 *
 * localInitialized et localInitUpdate sont mis à jour par le service Synchro/Initialization
 *
 * // TODO a renommer en localInitUpdate
 *
 * @ORM\Table(name="synchro_status")
 * @ORM\Entity
 */
class SynchroStatus
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var guid
     *
     * @ORM\Column(name="local_id", type="guid", nullable=true)
     *
     * Id pour identifier un ordi local (id unique qui devrait être
     * différent sur chaque ordi)
     */
    private $localId;

    /**
     * @var bool
     *
     * @ORM\Column(name="localInitialized", type="boolean")
     */
    private $localInitialized = false;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="localInitUdate", type="datetime")
     */
    private $localInitUdate;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="masterLastUdate", type="datetime")
     */
    private $masterLastUdate;

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get localId.
     *
     * @return guid
     */
    public function getLocalId()
    {
        return $this->localId;
    }

    /**
     * Get localInitUdate.
     *
     * @return DateTime
     */
    public function getLocalInitUdate()
    {
        return $this->localInitUdate;
    }

    /**
     * Get masterLastUdate.
     *
     * @return DateTime
     */
    public function getMasterLastUdate()
    {
        return $this->masterLastUdate;
    }

    /**
     * Get localInitialized.
     *
     * @return bool
     */
    public function isLocalInitialized()
    {
        return $this->localInitialized;
    }

    /**
     * Set the localId. It only works when the id is not defined (null).
     *
     * @param guid localId.
     * @param mixed $localId
     *
     * @return SynchroStatus
     */
    public function setLocalId($localId)
    {
        if (null === $this->localId) {
            $this->localId = $localId;
        } else {
            throw new Exception('It is only possible to set the local id once !', 1);
        }

        return $this;
    }

    /**
     * Set localInitialized.
     *
     * @param bool $localInitialized
     *
     * @return SynchroStatus
     */
    public function setLocalInitialized($localInitialized)
    {
        $this->localInitialized = $localInitialized;

        return $this;
    }

    /**
     * Set localInitUdate.
     *
     * @param DateTime $localInitUdate
     *
     * @return SynchroStatus
     */
    public function setLocalInitUdate($localInitUdate)
    {
        $this->localInitUdate = $localInitUdate;

        return $this;
    }

    /**
     * Set masterLastUdate.
     *
     * @param DateTime $masterLastUdate
     *
     * @return SynchroStatus
     */
    public function setMasterLastUdate($masterLastUdate)
    {
        $this->masterLastUdate = $masterLastUdate;

        return $this;
    }
}
