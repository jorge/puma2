<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * FormationsRecu.
 *
 * @ORM\Table(name="formations_recu")
 * @ORM\Entity
 * @ORM\HasLifecycleCallbacks
 */
class FormationsRecu extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F0;

    /**
     * @var string Annee d'enquête (soit 2015, soit 2012)
     *
     * @ORM\Column(name="annee", type="smallint", nullable=true)
     */
    private $annee;

    /**
     * @var string Annee de la formation
     *
     * @ORM\Column(name="anneee_formation", type="string", length=4, nullable=true)
     */
    private $anneeFormation;

    /**
     * @ORM\ManyToMany(targetEntity="Personnes")
     */
    private $beneficiaires;

    /**
     * @ORM\ManyToOne(targetEntity="Exploitations")
     */
    private $exploitation;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblUtilites")
     */
    private $niveauUtilite;

    /**
     * @ORM\ManyToOne(targetEntity="TblOrganisateurs")
     */
    private $organisateur;

    /**
     * @ORM\ManyToOne(targetEntity="TblThemesFormations")
     */
    private $theme;

    public function __construct($user)
    {
        parent::__construct($user);
        $this->beneficiaires = new ArrayCollection();
    }

    /**
     * Add beneficiaire.
     *
     * @param \App\Entity\Personnes $beneficiaire
     *
     * @return FormationsRecu
     */
    public function addBeneficiaire(Personnes $beneficiaire)
    {
        $this->beneficiaires[] = $beneficiaire;

        return $this;
    }

    /**
     * Get annee.
     *
     * @return string
     */
    public function getAnnee()
    {
        return $this->annee;
    }

    /**
     * Get anneeFormation.
     *
     * @return string
     */
    public function getAnneeFormation()
    {
        return $this->anneeFormation;
    }

    /**
     * Get beneficiaires.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getBeneficiaires()
    {
        return $this->beneficiaires;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get niveauUtilite.
     *
     * @return \App\Entity\TblUtilites
     */
    public function getNiveauUtilite()
    {
        return $this->niveauUtilite;
    }

    /**
     * Get organisateur.
     *
     * @return \App\Entity\TblOrganisateurs
     */
    public function getOrganisateur()
    {
        return $this->organisateur;
    }

    /**
     * Get theme.
     *
     * @return \App\Entity\TblThemesFormations
     */
    public function getTheme()
    {
        return $this->theme;
    }

    /**
     * Remove beneficiaire.
     *
     * @param \App\Entity\Personnes $beneficiaire
     */
    public function removeBeneficiaire(Personnes $beneficiaire)
    {
        $this->cultures->removeElement($beneficiaire);
    }

    /**
     * Set annee.
     *
     * @param string $annee
     *
     * @return FormationsRecu
     */
    public function setAnnee($annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set anneeFormation.
     *
     * @param string $annee
     *
     * @return FormationsRecu
     */
    public function setAnneeFormation($annee)
    {
        $this->anneeFormation = $annee;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return FormationsRecu
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set niveauUtilite.
     *
     * @param \App\Entity\TblUtilites $niveauUtilite
     *
     * @return FormationsRecu
     */
    public function setNiveauUtilite(?TblUtilites $niveauUtilite = null)
    {
        $this->niveauUtilite = $niveauUtilite;

        return $this;
    }

    /**
     * Set organisateur.
     *
     * @param \App\Entity\TblOrganisateurs $organisateur
     *
     * @return FormationsRecu
     */
    public function setOrganisateur(?TblOrganisateurs $organisateur = null)
    {
        $this->organisateur = $organisateur;

        return $this;
    }

    /**
     * Set theme.
     *
     * @param \App\Entity\TblThemesFormations $theme
     *
     * @return FormationsRecu
     */
    public function setTheme(?TblThemesFormations $theme = null)
    {
        $this->theme = $theme;

        return $this;
    }
}
