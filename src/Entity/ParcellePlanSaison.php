<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * ParcellePlanSaison.
 *
 * @ORM\Entity
 * @UniqueEntity(
 *     fields={"exploitationPlanSaison", "parcelle"},
 *     errorPath="port",
 *     message="Cette parcelle est déjà configuré pour ce planSaison de l'exploitation"
 * )
 * @ORM\Table(name="parcelle_plan_saison")
 * @ORM\Entity(repositoryClass="App\Repository\ParcellePlanSaisonRepository")
 * @ORM\HasLifecycleCallbacks
 */
class ParcellePlanSaison extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\TblCultures")
     */
    private $cultures;

    /**
     * @ORM\ManyToOne(targetEntity="ExploitationPlanSaison", inversedBy="parcellePlanSaison")
     */
    private $exploitationPlanSaison;

    /**
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="TblModeCulture")
     */
    private $modeCulture;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Parcelles", inversedBy="parcellePlanSaison")
     */
    private $parcelle;

    /**
     * @var string
     *
     * @ORM\Column(name="surface_prevue", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $surfacePrevue;

    public function __construct($user)
    {
        parent::__construct($user);
        $this->cultures = new ArrayCollection();
    }

    /**
     * Add culture.
     *
     * @param \App\Entity\TblCultures $culture
     *
     * @return ParcellePlanSaison
     */
    public function addCulture(TblCultures $culture)
    {
        $this->cultures[] = $culture;

        return $this;
    }

    /**
     * Get cultures.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getCultures()
    {
        return $this->cultures;
    }

    /**
     * Get exploitationPlanSaison.
     *
     * @return \App\Entity\ExploitationPlanSaison
     */
    public function getExploitationPlanSaison()
    {
        return $this->exploitationPlanSaison;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get modeCulture.
     *
     * @return \App\Entity\TblModeCulture
     */
    public function getModeCulture()
    {
        return $this->modeCulture;
    }

    /**
     * Get parcelle.
     *
     * @return \App\Entity\Parcelles
     */
    public function getParcelle()
    {
        return $this->parcelle;
    }

    /**
     * Get surfacePrevue.
     *
     * @return int
     */
    public function getSurfacePrevue()
    {
        return $this->surfacePrevue;
    }

    /**
     * Remove culture.
     *
     * @param \App\Entity\TblCultures $culture
     */
    public function removeCulture(TblCultures $culture)
    {
        $this->cultures->removeElement($culture);
    }

    /**
     * Set cultures.
     *
     * @param \App\Entity\TblCultures|null $cultures
     *
     * @return ParcellePlanSaison
     */
    public function setCultures(?TblCultures $cultures = null)
    {
        $this->cultures = $cultures;

        return $this;
    }

    /**
     * Set exploitationPlanSaison.
     *
     * @param \App\Entity\ExploitationPlanSaison $exploitationPlanSaison
     *
     * @return ParcellePlanSaison
     */
    public function setExploitationPlanSaison(ExploitationPlanSaison $exploitationPlanSaison)
    {
        $this->exploitationPlanSaison = $exploitationPlanSaison;

        return $this;
    }

    /**
     * Set modeCulture.
     *
     * @param \App\Entity\TblModeCulture $modeCulture
     *
     * @return ParcellePlanSaison
     */
    public function setModeCulture(?TblModeCulture $modeCulture = null)
    {
        $this->modeCulture = $modeCulture;

        return $this;
    }

    /**
     * Set parcelle.
     *
     * @param \App\Entity\Parcelles $parcelle
     *
     * @return ParcellePlanSaison
     */
    public function setParcelle(?Parcelles $parcelle = null)
    {
        $this->parcelle = $parcelle;

        return $this;
    }

    /**
     * Set surfacePrevue.
     *
     * @param int $surfacePrevue
     *
     * @return ParcellePlanSaison
     */
    public function setSurfacePrevue($surfacePrevue)
    {
        $this->surfacePrevue = $surfacePrevue;

        return $this;
    }
}
