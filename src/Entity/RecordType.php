<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

class RecordType
{
    public const F0 = 'F0';

    public const F1 = 'F1';

    public const F2 = 'F2';

    public const F3 = 'F3';

    public const F4 = 'F4';

    public const F5 = 'F5';

    public const F6 = 'F6';

    public const Signaletique = 'Signaletique';

    public static function fRecordToCollectIdType($type)
    {
        if (self::isFRecord($type)) {
            return (int) ($type[1]);
        }

        throw Error('Method fRecordToCollectIdType called on a non fRecord');
    }

    public static function isFRecord($type)
    {
        return self::F0 === $type | self::F1 === $type | self::F2 === $type | self::F3 === $type | self::F4 === $type | self::F5 === $type;
    }
}
