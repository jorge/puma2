<?php

declare(strict_types=1);

/*
 * Copyright (C) 2020, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class TblPlantsAgroFoEtFruitiers extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\Column(type="boolean")
     */
    private $agroForestier = false;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $plant;

    public function __toString(): string
    {
        $planNameOrNull = $this->getPlant();

        return $planNameOrNull ?: 'undefined';
    }

    public function getAgroForestier(): ?bool
    {
        return $this->agroForestier;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getPlant(): ?string
    {
        return $this->plant;
    }

    public function setAgroForestier(bool $agroForestier): self
    {
        $this->agroForestier = $agroForestier;

        return $this;
    }

    public function setPlant(string $plant): self
    {
        $this->plant = $plant;

        return $this;
    }
}
