<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;

/**
 * TblCultures.
 *
 * @ORM\Table(name="tbl_cultures")
 * @ORM\Entity(repositoryClass="App\Repository\TblCulturesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class TblCultures extends PumaEntity
{
    use CDateField;
    use UDateField;
    use UtilisateurField;

    /**
     * @ORM\ManyToOne(targetEntity="TblCategoriesAgricoles")
     */
    private $categorie;

    /**
     * @var string
     *
     * @ORM\Column(name="code_culture", type="string", length=4, nullable=true)
     */
    private $codeCulture;

    /**
     * @var string
     *
     * @ORM\Column(name="culture", type="string", length=30, nullable=true)
     */
    private $culture;

    /**
     * @var int
     *
     * @ORM\Column(name="elevage", type="integer", options={"default": 0})
     */
    private $elevage;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="poids", type="string", length=25, nullable=true)
     */
    private $poids;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="TblProduits", mappedBy="culture")
     */
    private $produits;

    /**
     * @var string
     *
     * @ORM\Column(name="unite_culture", type="string", length=25, nullable=true)
     */
    private $uniteCulture;

    public function __construct()
    {
        $this->produits = new ArrayCollection();
    }

    /**
     * Stringify this entity.
     */
    public function __toString(): string
    {
        return $this->culture;
    }

    /**
     * Get categorie.
     *
     * @return \App\Entity\TblCategoriesAgricoles
     */
    public function getCategorie()
    {
        return $this->categorie;
    }

    /**
     * Get codeCulture.
     *
     * @return string
     */
    public function getCodeCulture()
    {
        return $this->codeCulture;
    }

    /**
     * Get culture.
     *
     * @return string
     */
    public function getCulture()
    {
        return $this->culture;
    }

    /**
     * Get elevage.
     *
     * @return int
     */
    public function getElevage()
    {
        return $this->elevage;
    }

    /**
     * Get id.
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get poids.
     *
     * @return string
     */
    public function getPoids()
    {
        return $this->poids;
    }

    /**
     * Get produits.
     *
     * @return ArrayCollection
     */
    public function getProduits()
    {
        return $this->produits;
    }

    /**
     * Get uniteCulture.
     *
     * @return string
     */
    public function getUniteCulture()
    {
        return $this->uniteCulture;
    }

    /**
     * Set categorie.
     *
     * @param \App\Entity\TblCategoriesAgricoles $categorie
     *
     * @return TblCultures
     */
    public function setCategorie(?TblCategoriesAgricoles $categorie = null)
    {
        $this->categorie = $categorie;

        return $this;
    }

    /**
     * Set codeCulture.
     *
     * @param string $codeCulture
     *
     * @return TblCultures
     */
    public function setCodeCulture($codeCulture)
    {
        $this->codeCulture = $codeCulture;

        return $this;
    }

    /**
     * Set culture.
     *
     * @param string $culture
     *
     * @return TblCultures
     */
    public function setCulture($culture)
    {
        $this->culture = $culture;

        return $this;
    }

    /**
     * Set elevage.
     *
     * @param int $elevage
     *
     * @return TblCultures
     */
    public function setElevage($elevage)
    {
        $this->elevage = $elevage;

        return $this;
    }

    /**
     * Set poids.
     *
     * @param string $poids
     *
     * @return TblCultures
     */
    public function setPoids($poids)
    {
        $this->poids = $poids;

        return $this;
    }

    /**
     * Set uniteCulture.
     *
     * @param string $uniteCulture
     *
     * @return TblCultures
     */
    public function setUniteCulture($uniteCulture)
    {
        $this->uniteCulture = $uniteCulture;

        return $this;
    }
}
