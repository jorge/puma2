<?php

declare(strict_types=1);

/*
 * Copyright (C) 2019, Collectif Stratégies Alimentaires ASBL, <http://www.csa-be.org>
 *
 * This file is part of Puma2.
 *
 * Puma2 is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App\Entity;

use App\Entity\Traits\CDateField;
use App\Entity\Traits\RadieField;
use App\Entity\Traits\UDateField;
use App\Entity\Traits\UtilisateurField;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\Mapping\OneToMany;

/**
 * Parcelle.
 *
 * Une parelle est liée à une année / exercice (raison : pas bcp de changement sur la
 * propriété mais bcp de changement sur la location)
 *
 * @ORM\Table(name="parcelles")
 * @ORM\Entity(repositoryClass="App\Repository\ParcellesRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Parcelles extends PumaEntity
{
    use CDateField;
    use RadieField;
    use UDateField;
    use UtilisateurField;

    /**
     * Indicates that this entity is linked to the record F0.
     */
    protected $recordType = RecordType::F2;

    /**
     * @var int Annee de l'enquête
     *
     * @ORM\Column(name="annee", type="smallint", length=4, nullable=false)
     */
    private $annee;

    /**
     * @ORM\ManyToMany(targetEntity="TblAntierosives")
     */
    private $antierosives;

    /**
     * @var string
     *
     * @ORM\Column(name="cout_location", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $coutLocation;

    /**
     * Exploitation à qui appartient la parcelle.
     *
     * @ORM\ManyToOne(targetEntity="Exploitations", inversedBy="parcelles")
     */
    private $exploitation;

    /**
     * @ORM\ManyToOne(targetEntity="TblExposition")
     */
    private $exposition;

    /**
     * Une parcelle au sein d'une exploitation.
     *
     * Les données stockées au sein de cet objet sont donc les données les
     * récentes et associée à la date de mise à jour de l'object.
     *
     * @var guid
     *
     * @ORM\Column(name="id", type="guid")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="UUID")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="largueur", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $largeur;

    /** Latitude de l'exploitation (remplace coordonnées géom).
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $latitude;

    /**
     * @ORM\ManyToOne(targetEntity="TblDecoupageAdmin")
     */
    private $localisationAdmin;

    /** Longitude de l'exploitation (remplace coordonnées géom).
     * @var float
     *
     * @ORM\Column(type="float", nullable=true)
     */
    private $longitude;

    /**
     * @var string
     *
     * @ORM\Column(name="longueur", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $longueur;

    /**
     * @var string
     *
     * @ORM\Column(name="nom_parcelle", type="string", length=30, nullable=true)
     */
    private $nomParcelle;

    /**
     * @var int
     *
     * @ORM\Column(name="numero_parcelle", type="smallint", nullable=true)
     */
    private $numeroParcelle;

    /**
     * @OneToMany(targetEntity="ParcelleHistoriques", mappedBy="parcelle")
     */
    private $parcelleHistoriques;

    /**
     * @ORM\ManyToOne(targetEntity="TblPente")
     */
    private $pente;

    /**
     * @ORM\Column(type="boolean", options={"default": false})
     */
    private $planification = false;

    /**
     * Propriété ou locatation.
     *
     * @ORM\ManyToOne(targetEntity="TblPropriete")
     */
    private $propriete;

    /**
     * @ORM\ManyToOne(targetEntity="TblSituation")
     */
    private $situation;

    /**
     * @var string
     *
     * @ORM\Column(name="surface", type="decimal", precision=18, scale=2, nullable=true)
     */
    private $surface;

    /**
     * constructeur pour intialiser le lien inverse vers antierosives.
     *
     * @param mixed $user
     */
    public function __construct($user)
    {
        parent::__construct($user);
        $this->antierosives = new ArrayCollection();
    }

    public function __toString(): string
    {
        return sprintf('Parcelle %s', $this->getNumeroParcelle());
    }

    /**
     * Add antierosive.
     *
     * @param \App\Entity\TblAntierosives $antierosive
     *
     * @return Parcelles
     */
    public function addAntierosive(TblAntierosives $antierosive)
    {
        $this->antierosives[] = $antierosive;

        return $this;
    }

    /**
     * Add parcelleHistorique.
     *
     * @return Parcelles
     */
    public function addParcelleHistorique(ParcelleHistoriques $parcelleHistorique)
    {
        $this->parcelleHistoriques[] = $parcelleHistorique;

        return $this;
    }

    /**
     * Get annee.
     */
    public function getAnnee(): int
    {
        return $this->annee;
    }

    /**
     * Get antierosives.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAntierosives()
    {
        return $this->antierosives;
    }

    /**
     * Get coutLocation.
     *
     * @return string
     */
    public function getCoutLocation()
    {
        return $this->coutLocation;
    }

    /**
     * Get exploitation.
     *
     * @return \App\Entity\Exploitations
     */
    public function getExploitation()
    {
        return $this->exploitation;
    }

    /**
     * Get exposition.
     *
     * @return \App\Entity\TblExposition
     */
    public function getExposition()
    {
        return $this->exposition;
    }

    /**
     * Get id.
     *
     * @return guid
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get largeur.
     *
     * @return string
     */
    public function getLargeur()
    {
        return $this->largeur;
    }

    /**
     * Get latitude.
     *
     * @return float
     */
    public function getLatitude()
    {
        return $this->latitude;
    }

    /**
     * Get localisationAdmin.
     *
     * @return \App\Entity\TblDecoupageAdmin
     */
    public function getLocalisationAdmin()
    {
        return $this->localisationAdmin;
    }

    /**
     * Get longitude.
     *
     * @return float
     */
    public function getLongitude()
    {
        return $this->longitude;
    }

    /**
     * Get longueur.
     *
     * @return string
     */
    public function getLongueur()
    {
        return $this->longueur;
    }

    /**
     * Get nomParcelle.
     *
     * @return string
     */
    public function getNomParcelle()
    {
        return $this->nomParcelle;
    }

    /**
     * Get numeroParcelle.
     *
     * @return int
     */
    public function getNumeroParcelle()
    {
        return $this->numeroParcelle;
    }

    /**
     * Get parcelleHistoriques.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getParcelleHistoriques()
    {
        return $this->parcelleHistoriques;
    }

    /**
     * Get pente.
     *
     * @return \App\Entity\TblPente
     */
    public function getPente()
    {
        return $this->pente;
    }

    public function getPlanification(): ?bool
    {
        return $this->planification;
    }

    /**
     * Get propriete.
     *
     * @return \App\Entity\TblPropriete
     */
    public function getPropriete()
    {
        return $this->propriete;
    }

    /**
     * Get situation.
     *
     * @return \App\Entity\TblSituation
     */
    public function getSituation()
    {
        return $this->situation;
    }

    /**
     * Get surface.
     *
     * @return string
     */
    public function getSurface()
    {
        return $this->surface;
    }

    /**
     * Remove antierosive.
     *
     * @param \App\Entity\TblAntierosives $antierosive
     */
    public function removeAntierosive(TblAntierosives $antierosive)
    {
        $this->antierosives->removeElement($antierosive);
    }

    /**
     * Remove parcelleHistorique.
     */
    public function removeParcelleHistorique(ParcelleHistoriques $parcelleHistorique)
    {
        $this->parcelleHistoriques->removeElement($parcelleHistorique);
    }

    /**
     * Set annee.
     *
     * @return ParcelleHistoriques
     */
    public function setAnnee(int $annee)
    {
        $this->annee = $annee;

        return $this;
    }

    /**
     * Set coutLocation.
     *
     * @param string $coutLocation
     *
     * @return Parcelles
     */
    public function setCoutLocation($coutLocation)
    {
        $this->coutLocation = $coutLocation;

        return $this;
    }

    /**
     * Set exploitation.
     *
     * @param \App\Entity\Exploitations $exploitation
     *
     * @return Parcelles
     */
    public function setExploitation(?Exploitations $exploitation = null)
    {
        $this->exploitation = $exploitation;

        return $this;
    }

    /**
     * Set exposition.
     *
     * @param \App\Entity\TblExposition $exposition
     *
     * @return Parcelles
     */
    public function setExposition(?TblExposition $exposition = null)
    {
        $this->exposition = $exposition;

        return $this;
    }

    /**
     * Set largeur.
     *
     * @param string $largeur
     *
     * @return Parcelles
     */
    public function setLargeur($largeur)
    {
        $this->largeur = $largeur;

        return $this;
    }

    /**
     * Set Latitude.
     *
     * @param float $latitude
     *
     * @return Parcelles
     */
    public function setLatitude($latitude)
    {
        $this->latitude = $latitude;

        return $this;
    }

    /**
     * Set localisationAdmin.
     *
     * @param \App\Entity\TblDecoupageAdmin $localisationAdmin
     *
     * @return Parcelles
     */
    public function setLocalisationAdmin(?TblDecoupageAdmin $localisationAdmin = null)
    {
        $this->localisationAdmin = $localisationAdmin;

        return $this;
    }

    /**
     * Set Longitude.
     *
     * @param float $longitude
     *
     * @return Parcelles
     */
    public function setLongitude($longitude)
    {
        $this->longitude = $longitude;

        return $this;
    }

    /**
     * Set longueur.
     *
     * @param string $longueur
     *
     * @return Parcelles
     */
    public function setLongueur($longueur)
    {
        $this->longueur = $longueur;

        return $this;
    }

    /**
     * Set nomParcelle.
     *
     * @param string $nomParcelle
     *
     * @return Parcelles
     */
    public function setNomParcelle($nomParcelle)
    {
        $this->nomParcelle = $nomParcelle;

        return $this;
    }

    /**
     * Set numeroParcelle.
     *
     * @param int $numeroParcelle
     *
     * @return Parcelles
     */
    public function setNumeroParcelle($numeroParcelle)
    {
        $this->numeroParcelle = $numeroParcelle;

        return $this;
    }

    /**
     * Set pente.
     *
     * @param \App\Entity\TblPente $pente
     *
     * @return Parcelles
     */
    public function setPente(?TblPente $pente = null)
    {
        $this->pente = $pente;

        return $this;
    }

    public function setPlanification(bool $planification): self
    {
        $this->planification = $planification;

        return $this;
    }

    /**
     * Set antierosives.
     *
     * @param \App\Entity\TblAntierosives $antierosives
     *
     * @return parcelles
     */
    public function setProduits(?TblAntierosives $antierosives = null)
    {
        $this->antierosives = $antierosives;

        return $this;
    }

    /**
     * Set propriete.
     *
     * @param \App\Entity\TblPropriete $propriete
     *
     * @return Parcelles
     */
    public function setPropriete(?TblPropriete $propriete = null)
    {
        $this->propriete = $propriete;

        return $this;
    }

    /**
     * Set situation.
     *
     * @param \App\Entity\TblSituation $situation
     *
     * @return Parcelles
     */
    public function setSituation(?TblSituation $situation = null)
    {
        $this->situation = $situation;

        return $this;
    }

    /**
     * Set surface.
     *
     * @param string $surface
     *
     * @return Parcelles
     */
    public function setSurface($surface)
    {
        $this->surface = $surface;

        return $this;
    }
}
