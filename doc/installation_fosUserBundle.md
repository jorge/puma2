Installation de FOSUserBundle
=============================

Module pour la gestion des utilisateurs.

Cette installation se passe en 9 étapes [voir ici](https://symfony.com/doc/master/bundles/FOSUserBundle/index.html).

1) Activer les traducteur dans la config:

	# app/config/config.yml

	framework:
	    translator: { fallbacks: ["%locale%"] }

2) Importer le code source dans le projet:

	composer require friendsofsymfony/user-bundle "~2.0@dev"
	
3) Activer le module dans l'application:

	<?php
	// app/AppKernel.php
	
	public function registerBundles()
	{
	    $bundles = array(
	        // ...
	        new FOS\UserBundle\FOSUserBundle(),
	        // ...
	    );
	}
	
4) Créer l'entité User (nb nous avons remplacé l'id autoincrement par un guid):

	<?php
	// src/AppBundle/Entity/User.php
	
	namespace AppBundle\Entity;
	
	use FOS\UserBundle\Model\User as BaseUser;
	use Doctrine\ORM\Mapping as ORM;
	
	/**
	 * @ORM\Entity
	 * @ORM\Table(name="fos_user")
	 */
	class User extends BaseUser
	{
	    /**
	     * @var guid
	     * 
	     * @ORM\Id
	     * @ORM\Column(type="guid")
	     * @ORM\GeneratedValue(strategy="UUID")
	     */
	    protected $id;
	
	    public function __construct()
	    {
	        parent::__construct();
	        // your own logic
	    }
	}

5) Configurer les paramètres de gestion de la sécurité.

Nous remplaçons le fichier par défaut par celui conseillé par le module choisi:

	# app/config/security.yml
	security:
	    encoders:
	        FOS\UserBundle\Model\UserInterface: bcrypt
	
	    role_hierarchy:
	        ROLE_ADMIN:       ROLE_USER
	        ROLE_SUPER_ADMIN: ROLE_ADMIN
	
	    providers:
	        fos_userbundle:
	            id: fos_user.user_provider.username
	
	    firewalls:
	        main:
	            pattern: ^/
	            form_login:
	                provider: fos_userbundle
	                csrf_token_generator: security.csrf.token_manager
	                # if you are using Symfony < 2.8, use the following config instead:
	                # csrf_provider: form.csrf_provider
	
	            logout:       true
	            anonymous:    true
	
	    access_control:
	        - { path: ^/login$, role: IS_AUTHENTICATED_ANONYMOUSLY }
	        - { path: ^/register, role: IS_AUTHENTICATED_ANONYMOUSLY }
	        - { path: ^/resetting, role: IS_AUTHENTICATED_ANONYMOUSLY }
	        - { path: ^/admin/, role: ROLE_ADMIN }
	        
Il sera peut-être nécessaire de le revoir par la suite, notamment pour ce qui concerne le dernier paragraphe 'access_control'.

6) Configurer le module:

	# app/config/config.yml
	fos_user:
	    db_driver: orm # other valid values are 'mongodb', 'couchdb' and 'propel'
	    firewall_name: main
	    user_class: AppBundle\Entity\User

7) Importer les routes de FOSUserBundle:

	# app/config/routing.yml
	fos_user:
	    resource: "@FOSUserBundle/Resources/config/routing/all.xml"

*ATTENTION:*  pour pouvoir utiliser les fonctionamités d'envoi de emails, il faut que le module SwiftmailerBundle soit activé (ce qui est le cas dans l'installation de départ, mais à vérifier).

8) Mettre à jour le schema de la base de données.

Nous avons créé une nouvelle entité 'User' il faut maintenant l'intégrer dans la base de données:

	 php bin/console doctrine:schema:update --force
	 
9) Utiliser la console pour ajouter/activer/ajouter à un groupe/changer le mot de passe d'un utilisateur.

Créer un utilisateur 'testuser' avec mot de passe 'p@ssword' et une adresse email 'test@example.com':

	php bin/console fos:user:create testuser test@example.com p@ssword
	
Créer un utilisateur avec role super-admin:
	
	php bin/console fos:user:create adminuser --super-admin
	
Créer un utilisateur 'testuser' mais en le laissant inactif. Il ne sera en mesure de se connecter que lorsqu'il sera actvé:
	
	php bin/console fos:user:create testuser --inactive
	
Activer l'utilisateur 'testuser':
	
	php bin/console fos:user:activate testuser
	
Désactiver l'utilisateur 'testuser':
	
	php bin/console fos:user:deactivate testuser
	
Ajouter l'utilisateur 'testuser' au groupe des administrateurs:
	
	php bin/console fos:user:promote testuser ROLE_ADMIN	
	# alternative	
	php bin/console fos:user:promote testuser --super
	
Retirer l'utilisateur 'testuser' du groupe des administrateurs:
	
	php bin/console fos:user:demote testuser ROLE_ADMIN
	# alternative
	php bin/console fos:user:demote testuser --super
	
Modifier le mot de passe de l'utilisateur 'testuser':

	php bin/console fos:user:change-password testuser newp@ssword

Voilà, il ne reste plus qu'à essayer le résultat [se connecter](http://localhost:8000/app_dev.php/login)

Fonctions avancées
==================

