#!/bin/bash
for entry in "/home/jph/dev/symfony/puma2/src/AppBundle/Entity"/*
do
  tempfile="${entry##*/}"
  class="${tempfile%.*}"
  echo php bin/console doctrine:generate:entities AppBundle/Entity/"$class" --no-backup
done > /home/jph/dev/symfony/puma2/doc/scripts/settersgetters.txt

for entry in "/home/jph/dev/symfony/puma2/src/AppBundle/Entity"/*
do
  tempfile="${entry##*/}"
  class="${tempfile%.*}"
  echo php bin/console generate:doctrine:crud --entity=AppBundle:"$class" --route-prefix=/references/"${class,,}" --format=annotation --with-write --no-interaction --overwrite
done > /home/jph/dev/symfony/puma2/doc/scripts/filename.txt

for entry in "/home/jph/dev/symfony/puma2/src/AppBundle/Entity"/*
do
  tempfile="${entry##*/}"
  class="${tempfile%.*}"
  echo \<li\>\<a href=\"/references/"${class,,}"\"\>"$class"\<\/a\>\<\/li\>
done > /home/jph/dev/symfony/puma2/doc/scripts/listofrefs.txt
