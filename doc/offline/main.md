Ré-activation et réparation du mode offline de PUMA
===================================================

## Architecture

L'application Puma en ligne, servira de noeud principal (**master**).

L'application Puma hors ligne, sera installée sur des Raspberry Pi qui peuvent être emportés sur le terrain (+-30 RPI): ce sont les noeuds secondaires (**slave**).

#### En mode déconnecté
Chaque noeud slave aura une version à jour de Puma, et les machines environnantes pourront utiliser Puma pour l'encodage. Les requêtes SQL seront littéralement enregistrées de manière à pouvoir être rejouées lors de la phase de synchronisation.
(voir le bundle Symfony qui pourrait être utilisé: [Doctrine SQL Logger](https://support.champs-libres.coop/t/symfony-doctrine-sql-logger/512))

![diagram001](https://framagit.org/jorge/puma2/uploads/2838a8abe24366870d221bc8b458e204/diagram001.png)

#### De retour en mode connecté
Chaque noeud slave sera en mesure de transmettre au noeud master l'ensemble des requêtes sql qui seront rejouées sur le serveur principal. Une fois l'opération terminée, le noeud master synchronisera alors la version de Puma sur le noeud slave. Celle-ci sera prête à retourner sur le terrain en mode déconnecté.

![diagram](https://framagit.org/jorge/puma2/uploads/366c6e3b4601f17704b6a645d9418e82/diagram.png)


## Limites

On ne parlera pas à proprement parler d'une synchronisation bidirectionnelle. 

Lorsque **master** échange avec **slave_1**, il récupère les requêtes qui sont rejouées, ainsi sa db est à jour, et il renvoie la db à jour à **slave_1**. 

Mais que se passe-t-il ensuite avec **slave_2** ? celui-ci bénéficie des données à jour de **slave_1**, mais à l'inverse **slave_1** n'a pas les infos de **slave_2**.

Et **slave_1** et **slave_2** peuvent-ils communiquer simultanément avec **master** ?

![sequence](https://framagit.org/jorge/puma2/uploads/7627aaa930fd42fec32839607cb08282/sequence.png)

## Installation des Raspberry pi

* installation de _Raspbian_ sur des cartes _microSD_. Une partition spécifique rw doit contenir la db (toutes les données) ;
* chaque Raspberry Pi équipé d'une carte wifi (dongle usb ou chip rpi) qui émet en mode master (`wl essid puma-rpi mode master`). Dnsmasq distribuera des adresses ip aux machines qui s'y connectent (serveur dhcp) ;
* clone en série des images avec `dd` ;
* installation de Puma dans un container docker ;


### En pratique

Création de l'image qui sera dupliquée: 1,5 jour (cela ne comprends pas le travail d'adaptation de l'application Puma slave, mais seulement le réglage de l'OS -> on met la microsd dans le RPI, on branche et 1 minute après il sert l'application)
Duplication de l'image: compter +- 30min par duplication (soit 15h pour 30 rpi)
Champs-Libres achèterait les micro SD mais quid pour les raspberry pi ?



## Synchronisation entre Puma master et Puma slave

voir [syncho-note-technique.md](./doc/offline/synchro-note-technique.md)