


## Données à syncrhoniser

### Données de confiuration

Les données de configuration (principalement gérées par les entitées `Tbl*`) ne peuvent être ajoutées / modifiées que via le `master`.

La synchronisation pour ces données n'ira donc que dans un seul sens : de `master` à `slave`.

Pour ces données l'`id` des données peut être un champ `int` qui s'auto-incrémente.


#### Liste des données des entités de configuration

Cette liste est donnée en exemple et est à compléter :

- Coopératives
- Groupements
- Provinces
- Collines
- Achats agricoles
- Alimentation bétail
- Engrais
- État civil
- ...

### Données d'encodage

Les données d'encodage qui permettent d'encoder l'état d'une exploitation agricole peuvent être 
ajoutées / modifiées via le `slave` ou le `master`.

Pour permettre le bon fonctionnement de la synchronisation ces données ont les caractéristiques suivantes :

- les ids utilistés sont des `ùuid`
- chaque donnée est associée à deux date : `cdate`, date de la création de la donnée, et `udate`, date de la mise à jour de la donnée.

#### Liste des données d'encodage

Cette liste est donnée en exemple et est à compléter :

- Animaux (description d'un troupeau)
- Dettes
- Formatuion (besoin de)
- Formations reçues
- ....

## Processus de synchronisation 

Chaque installation `slave` comporte des infos sur l'état de sa synchronisation. Cet état, permet
d'identifier les données à synchroniser.

### Etat de la syncrhonisation

Pour chaque `slave` l'état de la synchronisation est stockée en db dans l'entité `SynchroStatus`.

Chaque `slave` retient 
- la date à laquelle elle a pris toutes les données du `master` (`masterLastUdate` de `SynchroStatus` (de l'unique syncrhonisation non-terminée))
- la date à laquelle elle a envoyé toutes ses données au `master` (`localInitUdate` de `SynchroStatus` (de la dernière syncrhonisation terminée))

Quand une procédure de syncrhonisation est lancée sur un `slave`,
le `slave` se met dans un été tel qu'il n'est plus possible d'ajouter / modifier des données dessus.


### Déroulement d'une syncrhonisation

Schéma d'un syncrhonisation se déroulant bien :

![diagram](http://www.plantuml.com/plantuml/png/XL9BRiCW4DrpYf6wIBhe1RgeqatkGSmaHiBW3c0fFKrt3UVWOXtQM26kIrcXqRpFUvgT3v9278pehGi8Ra82R-I4yB3Vl_8Je78wn023KmZ-RBkUd83jQA87jScf8nqY8OpEUtqm-2ZGAYfiGj22L417EScWdOKR6x2947qJTmzKoDhEsfpuapy0xUJElBoLrFMQpR_hiVRna-Zf1GRf0-yzVoDqeJARAwPMlUwnx9GJwQFkPkbAbp1_Hgnjzqk2VqkKfKCP0w6VnyqYQskdd6o-44UeNOqkpgMruTg6d4vBkC8bk9KAZjhUPRh1LagVoqHuYpXFPU7GiBPEENLnPCplybTFf2Iwh2KgZ8thQDp-t_bs_8k3-G80)


A chaque instant, il ne peut avoir qu'un seul `slave` faisant une procédure de `syncrhonisation`.

La syncrhonisation se déroule selon les étapes suivantes :

- 1. Le `slave` se met en mode `syncrhonisation`
    - il crée une entité `SynchroStatus` avec `localInitUdate` à la date du moment
    - (à partir de ce moment, il n'est plus possible d'ajouter ou de modifier des données sur le `slave`)
- 2. Le `slave` un fichier contenant l'ensemble des données qu'il veut envoyer au master et l'envoie


- 3. Le `master` reçoit des données d'un `slave`
    - se met en mode  `syncrhonisation` si
        - les données reçues sont valides
        - il n'est pas déjà en mode `synchrnisation`
    - génère une archive des données à envoyer à `slave`
    - intègre les données reçues de `slave`

- 4. Le `slave` vérifie l'état du `master` et attend que celui-ci 
     soit arrivé à la fin de l'intégratioon des données reçues
- 5. Le `slave` récupère les données du `master` et les intrègre
     dnas sa db
- 6. Le processus de syncrhonisation est terminé, le `slave` sort du mode `syncrhonisation` 



Pour chaque `slave` il y a besoin de savoir :

- dernière date où toutes les données du `slave` on été envoyées à `master`, (`localInitUdate` dans l'entiée
- dernière date où les données du `master` ont été envoyées à `slave`,




Schéma détaillé du processus de syncrhonisation (gestion des erreurs) ;

![diagram](http://www.plantuml.com/plantuml/png/dLJ1JWCt43spNp4I1uY4Ukibg2gbq5FefLMlaV5EiWQllHsFKuBoCRtcqf_O7wkz4zFi9cq5ES1wFFFcpRFzxW8hujZQYWrRX1zghJfLGzWwtP1t9YWsti5rgpJvMLNj5buhkwk6kigiu7e8G5fp3B0o5W7VJ62S0JUOAdqqi-fY_ck_j_rUDuhMI70T33nZyCRDe5Kfcmu8jzFKilrkkhotC8Gd5Pd6ayH-4HxVmN99gEhjSZdcSJ7N3UhN0j7Mtzxc8BunASqbh4WtPeF3rfDw0JlC-72V0xcPV_s-m3hl4cpEmn3K6g6-GYAC19rFVqGTOPMhqGOiN5BH0rfafDE-9tjbMkdbQxCoE8O7TFzCFVHoAQkIbiBp43gA04prbbbBDoaGsHUZdUgFnjW48-kaXmbfC6u-gCxaOFxtH7BIR_sUPA9mPIniGK6D0H8djyOM7OUqXbgnWlvFGBVnMwJf4LnQisxutaQSBrBzCs3WdFxY8msw1VRTwHcqHKoB8RT0wTNldoC3SbwiZ3iID6kc2LEcY3MwBh6pNjNZ3L2kBY-4FRJAC4dsVnnagPLXqucEJn9WgcufUFKd4lyhtyyG7j_ASRw0O2qvdviAO1Zi_3HBJOZZUJxF6k2Bd4Ji334LMy2pVvpHUtmNVp897WaE9TeJ-RQBDzK6ex4Mtu7GAgUpg6hglBdt4PK9TZ4TyHfx5TdON8YHAhL13HP541OSkR_b8G2KYFpBZ_tmg4z5cLN91OGba-h_ulmiYs4cDzXT_fX-ZuyxCUATUCR4JSHy9IZUMvntV98va_ez5iq-mjcDIp3RyLapdZ-A0TVA1H0w7mvyzFQFxeGadVTHqUPhcWywLqs2Gge0VhwVPqlxFIAil0cGFwpAxW9rX8wyJj2PG61Gp6WSuqq5myzOMAd8q2LTDjuGH0VjWOcsoj0vFb9QqZ49Ps6355AQgblZJ60QCgKuGQkreIo0zYxq-qJGQVmcQm27AUxITsnjzJS0)


### Données de synchronisation

Lorsque les entités communiquent ensemble, elle le font via 
des archives zip. Il y a toujours une vérification du md5 pour vérifier
que les données ne sont pas corrompues.

Ces dossiers contiennent :

- une ficher d'info sur le processus de synchronisation
- un fichier sql pour chaque table à synchroniser

#### Fichier sql pour une table

Pour une table `tableX` le fichier sera nommé `tableX.sql` et aura le contenu suivant :

```
INSERT INTO table ("col1", "col2") VALUES ("data1", "data2);
INSERT INTO table ("col1", "col2) VALUES ("data1", "data2);
```

Se fichier se fait à l'aide du `udate` il intrègre toutes les lignes
dont le `ùdate` est compris entre la dernière date de synchro et la date où le processus de synchronisation a été lancé.
