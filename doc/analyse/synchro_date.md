# Décalage temps entre master et local

Pas grave, c'est géré par l'algo. A expliquer

# Décalage temps entre php et mysql (très grave) :

## cdate et udate

Pour synchroniser nous utilisons cdate (creation date) et udate (update date).

Quand les objects sont gérés par Symfony, ces dates sont générées par Symfony
(voir PumaEntity: prePersist et preUpdate).

Lors de la synchronisation, on fait des requêtes sql brutes. Ces dates
sont donc gérées par mysql. Chaque colonne doit avoir comme déclaration :

``` sql
-- voir Version20170503073609.php
ALTER TABLE XXX MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();
```

Quand on fait un update ou un insert et que `udate` n'est pas mentionné, il
recevra la valeur `ǸOW()`. Il ne faut donc pas inclure la date `udate` dans
les requêtes sql de syncho.

Le point hyper important et que php et mysql soient syncho. Pour ça, l'idée
est d'utiliser les dates en UTC.

(voir http://docs.doctrine-project.org/projects/doctrine-orm/en/latest/cookbook/working-with-datetime.html

Test pour voir si on a les mêmes données :

``` php
$conn = $this->getDoctrine()->getManager()->getConnection();
$time = $conn->query('SELECT NOW() as now;')->fetch();
var_dump($time['now']);

$now = new \DateTime();
$now->setTimeZone(new \DateTimeZone('UTC'));
var_dump($now->format('Y-m-d H:i:s'));
```

# conclusion

Besoin de :
- utiliser prePersist et preUpdate de PumaEntity (pour cdate et udate quand obj géré par symfony)
- avoir la colonne en `DEFAULT NOW() ON UPDATE NOW()` et ne pas inclure dans la requête (comme ça géré par mysql)
