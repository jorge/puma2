-- PUMA
-- version 		2.0
-- author 		Jean Pierre Huart, Jorge Palma
-- email  		jph@openjph.be, jorge.palma@csa-be.org
-- copyright	Copyright 2016, Jean Pierre Huart, Jorge Palma
-- license 		GPLv3
-- date 		2016-05-24
--
-- Pour visualiser les éléments de la db
--

SELECT tables.TABLE_NAME AS 'table',
tables.TABLE_COMMENT AS 'table description', 
columns.COLUMN_NAME AS 'column',
columns.COLUMN_TYPE AS 'data type',
columns.COLUMN_COMMENT AS 'column description'
FROM information_schema.TABLES AS tables
inner join information_schema.COLUMNS AS columns ON tables.TABLE_NAME = columns.TABLE_NAME
AND columns.table_schema=tables.table_schema
WHERE tables.TABLE_TYPE NOT IN ('VIEW')
AND tables.table_schema="puma_02";
