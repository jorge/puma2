# Installer composer 

(voir le site officiel)[https://getcomposer.org/download/] car cela change pour chaque nouvelle version de composer.

# Cloner le projet depuis le repository
 cd mon_chemin/
 git clone git@framagit.org:jorge/puma2.git

# Mettre à jour les composant avec composer
 cd puma2/
 composer install

