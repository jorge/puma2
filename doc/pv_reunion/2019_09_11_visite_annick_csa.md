
(retour réunion en bleu dans le texte / voir https://mensuel.framapad.org/p/puma-or-nllvsjrowq )

##Retour sur l'utilisation PUMA

   * quelles sont les données/fiches qui sont encodées ?
       * pour l'instant fiche signalétique + données de 2015
   * l'encodage se fait-il via la tablette ?
       * se fait via tablette et ordinateur
   * y a t il des problèmes de connexion ?
       * semble être ok
   * y a t il des besoins non couverts ?
       * il semble qu'il n'est pas possible de retrouver un membre à partir d'une coopérative
       * besoin d'installer Puma sur les ordis des coopératives afin qu'une coopérative puisse consulter les données de ses membres
               * si pas de connexion internet, besoin de remettre la synchronisation des données (qui pour l'instant est en pause) ; la remise en marche peut demander un certain temps
               * une alternative est de créer des comptes utilisateurs associés aux coopératives tel que l'utilisateur associé à une coopérative ne pourra ne voir que les données que de ses membres ; cette option est plus rapide à mettre en place

##Vérification de la qualité de données encodées

   * 14217 exploitations encodées dans PUMA
   * 0 exploitations sans chef de famille
   * 0 exploitations avec >1 chef de famille
   * 0 cas potentiels de doublons

##Objectifs et calendrier de PUMA

   * Quelles campagnes d'encodage vont être lancées
       * quand ?
       * quelles données (fiches) ?
           * quels vont être les formulaires utilisés ?
       * quels sont les indicateurs (rapports) qu'on veut pouvoir calculer ?
Nous allons tester les fiches F1, F2, ... sur un petit groupe afin de voir si tout est ok.

##Production des cartes de membre

   * quel est le programme d'action ?
       * impression en interne ou via externe ?
       * quelles sont les étapes prévues (par ex 1 étape de rodage avec 100 exploitations, puis...)
       * pour l'instant des contacts sont pris avec des externes
       * une fois la carte imprimée elle sera plastifiée pour durer plus longtemps
       * la création des cartes va se faire petit à petit (par groupe)
   * prise des photos
       * se fait elle bien via la tablette ? (afin d'avoir un format uniforme)
       * pour info, une vérification des données par le programme est prévue
           * vérification que la photo est en portrait
           * vérification du ratio (400 x 533)
       * la prise de photo avec la tablette via le site (et non l'application appareil photo) est privilégiée
       * il y a un besoin d'avoir un listing des exploitations sans photo
   * mise en place de requêtes
       * indication des exploitations sans photo
       * indication des exploitations sans CNI (qui est affiché sur la carte de membre)
           * est-ce que le CNI peut changer ?
       * tout le monde a un CNI et normalement il ne change pas 

##Aide à l'utilisation du programme PUMA

   * petit à petit de l'aide à l'utilisation du programme va être rédigée
       * est-ce que cette aide doit être en format digital (html / pdf) ?
           * ( afin d'avoir un lien direct entre PUMA et l'aide à la bonne page )
       * est-ce que cette aide doit être en format papier ?
       * pouvoir imprimer l'aide sur papier est vraiment un plus