<?php

declare(strict_types=1);

namespace Tests\AppBundle;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\BrowserKit\Cookie;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use function array_key_exists;

/**
 * @internal
 * @coversNothing
 */
final class PumaTestBasic extends WebTestCase
{
    private $params = [];

    private $randomExploitations = [];

    /**
     * associative array containing condition to select a special exploitation.
     */
    private $randomExploitationsQueries = [];

    public function addRandomExploitationsQuery($key, $query)
    {
        $this->randomExploitationsQueries[$key] = $query;
    }

    public function getParam($paramName)
    {
        if (!array_key_exists($paramName, $this->params)) {
            $client = self::createClient();
            $this->params[$paramName] = $client->getKernel()->getContainer()
                ->getParameter($paramName);
        }

        return $this->params[$paramName];
    }

    public function getRandomExploitation($requestKey = '')
    {
        if (!array_key_exists($requestKey, $this->randomExploitations)) {
            self::bootKernel();
            $em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');

            $customRequestElement = null;

            if (array_key_exists($requestKey, $this->randomExploitationsQueries)) {
                $customRequestElement = $this->randomExploitationsQueries[$requestKey];
            }

            $rawQuery = $this->getExploitationQueryFor($requestKey, true);
            $query = $em->createQuery($rawQuery);
            $exploitationsCount = $query->getSingleScalarResult();

            $rawQuery = $this->getExploitationQueryFor($requestKey);
            $query = $em->createQuery($rawQuery);

            $this->randomExploitations[$requestKey] = $query
                ->setMaxResults(1)
                ->setFirstResult(mt_rand(0, $exploitationsCount))
                ->getSingleResult();
        }

        return $this->randomExploitations[$requestKey];
    }

    public function logIn($client, $username)
    {
        $session = $client->getContainer()->get('session');

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');
        $user = $em->getRepository('AppBundle:User')
            ->findOneByUsername($username);

        $firewall = 'main';

        $token = new UsernamePasswordToken($user, null, $firewall, $user->getRoles());
        $session->set('_security_main', serialize($token));
        $session->save();

        $client->getCookieJar()->set(new Cookie($session->getName(), $session->getId()));
    }

    private function getExploitationQueryFor($requestKey, $count = false)
    {
        if ($count) {
            $query = 'SELECT count(e.id) FROM AppBundle:Exploitations e';
        } else {
            $query = 'SELECT e FROM AppBundle:Exploitations e';
        }

        if (array_key_exists($requestKey, $this->randomExploitationsQueries)) {
            $query = $query . ' ' . $this->randomExploitationsQueries[$requestKey];
        }

        return $query;
    }
}
