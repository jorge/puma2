<?php

declare(strict_types=1);

namespace Tests\AppBundle;

/**
 * @internal
 * @coversNothing
 */
final class PageAccessTest extends PumaTestBasic
{
    /**
     * @dataProvider urlAuthProvider
     * check access to page when the user is logged
     *
     * @param mixed $url
     */
    public function testloggedUser($url)
    {
        $clientAuth = self::createClient([], [
            'PHP_AUTH_USER' => $this->getParam('test_user'),
            'PHP_AUTH_PW' => $this->getParam('test_pwd'),
        ]);
        $clientAuth->request('GET', $url);

        self::assertTrue($clientAuth->getResponse()->isSuccessful());
    }

    public function testSignaletiquePageAccess()
    {
        self::bootKernel();
        $router = self::$kernel->getContainer()->get('router');

        $e = $this->getRandomExploitation();

        $clientAuth = self::createClient([], [
            'PHP_AUTH_USER' => $this->getParam('test_user'),
            'PHP_AUTH_PW' => $this->getParam('test_pwd'),
        ]);

        $url = $router->generate(
            'references_exploitations_show_sans_annee',
            ['id' => $e->getId()]
        );

        $clientAuth->request('GET', $url);
        self::assertTrue(
            $clientAuth->getResponse()->isSuccessful(),
            'Pbm chagement de ' . $url
        );
    }

    /**
     * Check that the page signaletique is still correct even if the poeple
     * has set a province as localisationAdmin (instead of sous-colline).
     */
    public function testSignaletiquePageAccessForExploitationWithEmptyCommune()
    {
        self::bootKernel();
        $router = self::$kernel->getContainer()->get('router');

        $e = $this->getRandomExploitation();

        $em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $provinceLevel = $em->getRepository('AppBundle:TblDecoupageAdminLevel')->findOneBy(['level' => 2]);
        $province = $em->getRepository('AppBundle:TblDecoupageAdmin')->findOneBy(['level' => $provinceLevel->getId()]);

        $e->setLocalisationAdmin($province); // we associate temporary $e to $province (to have a pbm)

        $clientAuth = self::createClient([], [
            'PHP_AUTH_USER' => $this->getParam('test_user'),
            'PHP_AUTH_PW' => $this->getParam('test_pwd'),
        ]);

        $url = $router->generate(
            'references_exploitations_show_sans_annee',
            ['id' => $e->getId()]
        );

        $clientAuth->request('GET', $url);
        self::assertTrue(
            $clientAuth->getResponse()->isSuccessful(),
            'Pbm chagement de ' . $url
        );
    }

    /**
     * @dataProvider urlProvider
     * Check if access when not logged
     *
     * @param mixed $url
     */
    public function testUnloggedUser($url)
    {
        $client = self::createClient();
        $client->request('GET', $url);

        self::assertTrue($client->getResponse()->isSuccessful());
    }

    public function urlAuthProvider()
    {
        return [
            ['/saisie/recherche'],
            ['/rapports'],
            ['/synchro'],
        ];
    }

    public function urlProvider()
    {
        return [
            ['/'],
            ['/login'],
        ];
    }
}
