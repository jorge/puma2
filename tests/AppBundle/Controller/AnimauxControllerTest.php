<?php

declare(strict_types=1);

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\PumaTestBasic;

/**
 * @internal
 * @coversNothing
 */
final class AnimauxControllerTest extends PumaTestBasic
{
    private $exploitation;

    protected function setUp()
    {
        $this->exploitation = $this->getRandomExploitation();
    }

    /**
     * check if the page 'Saisie F2 (parcelle)' is accessible.
     */
    public function testPageAccess()
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => $this->getParam('test_user'),
            'PHP_AUTH_PW' => $this->getParam('test_pwd'),
        ]);

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');

        $url = $client->getContainer()->get('router')->generate(
            'references_animaux_index',
            [
                'exploitation_id' => $this->exploitation->getId(),
                'annee' => $this->exploitation->getLastAnneeEncodage(),
            ],
            false
        );

        self::markTestSkipped('A réparer en même temps que F3');

        $crawler = $client->request('GET', $url);

        /* vérifie que la page répond */
        self::assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            'La page Saisie F3 (troupeaux) est inacessible'
        );
    }
}
