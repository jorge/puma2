<?php

declare(strict_types=1);
// TODO ajouter une Parcelle
// TODO supprimer une Parcelle
// TODO tester voir historique
// TODO tester ajout de antierosive
// TODO tester edit antierosive
// TODO suppression antierosive

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\PumaTestBasic;

/**
 * @internal
 * @coversNothing
 */
final class ParcellesControllerTest extends PumaTestBasic
{
    /**
     * check if the Parcelles's pages are accessible.
     *
     * @dataProvider urlConfProvider
     *
     * @param mixed $url
     * @param mixed $conf
     */
    public function testPageAccess($url, $conf)
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => $this->getParam('test_user'),
            'PHP_AUTH_PW' => $this->getParam('test_pwd'),
        ]);

        $em = $client->getContainer()->get('doctrine.orm.entity_manager');

        $url = $client->getContainer()->get('router')->generate($url, $conf);

        $crawler = $client->request('GET', $url);

        /* vérifie que la page répond */
        self::assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            'La page ' . $url . '  est inacessible'
        );
    }

    public function urlConfProvider()
    {
        $this->addRandomExploitationsQuery(
            'with_parcelle',
            ' JOIN e.parcelles p'
        );
        $exploitation = $this->getRandomExploitation('with_parcelle');
        $parcelle = $exploitation->getParcelles()->first();
        $annee = $exploitation->getExercices()->first()->getAnnee();

        return [
            ['references_parcelles_index', [
                'exploitation' => $exploitation->getId(),
                'annee' => $annee,
            ]],
            ['references_parcelles_new', [
                'exploitation' => $exploitation->getId(),
                'annee' => $annee,
            ]],
            ['references_parcelles_show', [
                'parcelle' => $parcelle->getId(),
                'annee' => $annee,
            ]],
            ['references_parcelles_edit', [
                'parcelle' => $parcelle->getId(),
                'annee' => $annee,
            ]],
        ];
    }
}
