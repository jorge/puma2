<?php

declare(strict_types=1);

namespace Tests\AppBundle\Controller;

use AppBundle\Controller\RapportsController;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use function count;

/**
 * @internal
 * @coversNothing
 */
final class RapportsControllerTest extends WebTestCase
{
    public function surfaceDataProvider()
    {
        return [
            [
                '7c825370-2698-11e7-98c8-b8ca3a965972',  //'8d0a3e72-2698-11e7-98c8-b8ca3a965972',
                'ASSO PRO NINGA',
                $this->computeSurfaceMoyenne('7c825370-2698-11e7-98c8-b8ca3a965972', 'ASSO PRO NINGA'),
            ],
            [
                '7c828956-2698-11e7-98c8-b8ca3a965972',
                'GISURU 1',
                118.50, // surface moyenne
            ],
        ];
    }

    public function surfaceexploActionTest()
    {
        $coop_id = '7c825ee4-2698-11e7-98c8-b8ca3a965972';
        $sqlRequest = sprintf(RapportsController::$strSql_surfaceexploAction, $coop_id);

        self::bootKernel();
        $em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $stmt = $em->getConnection()->prepare($sqlRequest);
        $stmt->execute();
        $sqlResult = $stmt->fetchAll();

        $sqlRow = $sqlResult[0];
        $expNumeroMembre = $sqlRow['numero_membre'];
        $expSurface = $sqlRow['total'];
        $expNbrParcels = $sqlRow['nombre_parcelles'];

        $exploitation = $em->getRepository('AppBundle:Exploitations')->findByNumeroMembre($expNumeroMembre);
        // $exploitationId = $exploitation->getId();

        $parcels = $em->getRepository('AppBundle:Parcelles')->findByExploitation($exploitation);
        $exploitationSurface = 0;
        $exploitationNumParcels = 0;

        foreach ($parcels as $parcel) {
            $exploitationSurface = $exploitationSurface + $parcel->getSurface();
            $exploitationNumParcels = $exploitationNumParcels + 1;
        }

        self::assertEquals($expSurface, $exploitationSurface);
        self::assertEquals($expNbrParcels, $exploitationNumParcels);
    }

    public function surfaceexploWithNoParcelTest()
    {
        /*
         * Tester pour une exploitation qui n'a pas de parcelle (doit
         * apparaitre dans la lsite. Quand la requete sql utiliser
         * des left-join ces exploitations ne sont pas dans la liste de
         * retour (ça été corrigé avec des inner join). ce serait bien de
         * tester
         */

        self::markTestSkipped('A faire');
    }

    public function testSql()
    {
        $this->surfaceexploActionTest();
        //$this->assertEquals(55.15, $this->computeSurfaceMoyenne('8d0a3e72-2698-11e7-98c8-b8ca3a965972', 'ABAKORANABUSHAKE'));
    }

    /**
     * Test la page "Rapport à valider > Liste de surfaces moyennes des
     * exploitations et totales par groupement (Unité: Ares).
     *
     * Ce test vérifie :
     *
     * - que la page est inaccessible si l'on n'es pas authentifié (redirige vers
     *   le formulaire de login)
     * - que la page répond avec un statut 200 si on est bien authentifié
     * - ensuite, le test prend une ligne dans le tableau et vérifie les valeurs
     *   attendues.
     *
     * @dataProvider surfaceDataProvider()
     *
     * @param mixed $id
     * @param mixed $groupement
     * @param mixed $moyenneSurface
     */
    public function testSurfaceAction($id, $groupement, $moyenneSurface)
    {
        self::markTestSkipped('A faire');

        $client = self::createClient();

        $crawler = $client->request('GET', '/rapports/surfacegroup', [
            'rapports_surface' => [
                'Denomination' => $id,
            ],
        ]);

        self::assertEquals(302, $client->getResponse()->getStatusCode());

        $clientAuth = self::createClient([], [
            'PHP_AUTH_USER' => $client->getKernel()->getContainer()->getParameter('test_user'),
            'PHP_AUTH_PW' => $client->getKernel()->getContainer()->getParameter('test_pwd'),
        ]);

        $crawler = $clientAuth->request('GET', '/rapports/surfacegroup', [
            'rapports_surface' => [
                'Denomination' => $id,
            ],
        ]);

        /* vérifie que la page répond */
        self::assertEquals(200, $clientAuth->getResponse()->getStatusCode());
        self::assertContains('Liste de surfaces moyennes des exploitations et totales par groupement', $crawler->filter('h1')->text());

        // on vérifie une ligne du tableau
        $rowGroupements = $crawler->filter('tr:contains("' . $groupement . '")')->each(static function ($node, $i) use ($groupement) {
            $cell0 = $node->filter('td')->first();

            if (trim($cell0->text()) === $groupement) {
                return $node;
            }

            return null;
        });

        $rowGroupements = array_filter($rowGroupements, static function ($el) {
            return null !== $el;
        });

        self::assertCount(1, $rowGroupements);

        $cellMoyenne = $rowGroupements[array_keys($rowGroupements)[0]]->filter('td')->eq(1);
        $moyenneFormatted = number_format($moyenneSurface, 2, ',', ' ');
        self::assertContains($moyenneFormatted, $cellMoyenne->text());
    }

    protected function computeSurfaceMoyenne($cooperative_id, $groupement)
    {
        self::bootKernel();

        $em = self::$kernel->getContainer()->get('doctrine.orm.entity_manager');
        $organisationRepository = $em->getRepository(\AppBundle\Entity\TblOrganisations::class);

        $cooperative = $organisationRepository
         //       ->getAllGroupements($denomination)
            ->find($cooperative_id);
        $groupements = $organisationRepository->getAllGroupements($cooperative);

        $groupements = array_filter($groupements, static function (\AppBundle\Entity\TblOrganisations $o) use ($groupement) {
            return $o->getDenomination() === $groupement;
        });

        // la fonction array_filter préserve les clés. Il faut donc reprendre le
        // premier élément du tableau, mais sans connaitre la clé de cet élément.

        // première manière de faire :
        //\reset($groupements); // on remet le pointeur du tableau au début de celui-ci
        //$groupement = \current($groupements); // on prend le premier élément

        // deuxième méthode
        $groupement = $groupements[array_keys($groupements)[0]]; //ici, on liste les clés, et on récupère la première clé

    //    calcul des exploitations
        $exploitations = $em->createQuery(
            ''
                . 'SELECT e '
                . 'FROM AppBundle:PivotExploitationOrganisations peo '
                     . 'INNER Join AppBundle:Exploitations e WITH peo.exploitation = e.id '
                . 'WHERE peo.organisation= :groupement'
        )
            ->setParameter('groupement', $groupement->getId())
            ->getResult();

        $parcelles = [];

        foreach ($exploitations as $exploitation) {
            foreach ($exploitation->getParcelles() as $p) {
                $parcelles[] = $p;
            }
        }

        $surfaces = array_map(static function (\AppBundle\Entity\Parcelles $p) {
            return $p->getSurface();
        }, $parcelles);
        $total_surface = array_sum($surfaces);
        $count_exploitations = count($exploitations);

        return array_sum($surfaces) / count($exploitations);
    }

    /*
       public function testTestSql()
       {
           $client = static::createClient();
       	$client = static::createClient(array(), array(
               'PHP_AUTH_USER' => $client->getKernel()->getContainer()->getParameter('test_user'),
               'PHP_AUTH_PW'   => $client->getKernel()->getContainer()->getParameter('test_pwd'),
           ));
           $this->markTestSkipped('Le sql n\'est plus du tout à jour. Aie. Aie. Aie.');
           $crawler = $client->request('GET', '/rapports/testsql');
           $this->assertEquals(200, $client->getResponse()->getStatusCode());
           $result = $crawler->filter('#testresult')->text();
           dump(json_decode($result));
       	$this->assertEquals(2304, count(json_decode($result)));
       }
     */
}
