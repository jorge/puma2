<?php

declare(strict_types=1);

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\PumaTestBasic;
use function count;

/**
 * @internal
 * @coversNothing
 */
final class PersonnesControllerTest extends PumaTestBasic
{
    public function testEditAction()
    {
        $e = $this->getRandomExploitation();

        $clientAuth = self::createClient([], [
            'PHP_AUTH_USER' => $this->getParam('test_user'),
            'PHP_AUTH_PW' => $this->getParam('test_pwd'),
        ]);

        $router = $clientAuth->getContainer()->get('router');

        if (count($e->getPersonnes()) > 1) {
            $person = $e->getPersonnes()[0];
            $url = $router->generate(
                'references_personnes_edit',
                ['id' => $person->getId()]
            ); //TOODE

            $crawler = $clientAuth->request('GET', $url);

            self::assertEquals(
                200,
                $clientAuth->getResponse()->getStatusCode(),
                'La page ' . $url . ' est inaxcessible'
            );
        }
    }
}
