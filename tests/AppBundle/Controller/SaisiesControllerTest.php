<?php

declare(strict_types=1);

namespace Tests\AppBundle\Controller;

use Tests\AppBundle\PumaTestBasic;

/**
 * @internal
 * @coversNothing
 */
final class SaisiesControllerTest extends PumaTestBasic
{
    private $exploitation;

    /**
     * @dataProvider testPageAccessDataProvider
     * check if the pages of saisieController are accessible
     *
     * @param mixed $route
     * @param mixed $pageName
     * @param mixed $routeParameters
     */
    public function testPageAccess($route, $pageName, $routeParameters)
    {
        $client = self::createClient([], [
            'PHP_AUTH_USER' => $this->getParam('test_user'),
            'PHP_AUTH_PW' => $this->getParam('test_pwd'),
        ]);

        $url = $client->getContainer()->get('router')->generate(
            $route,
            $routeParameters,
            false
        );

        $crawler = $client->request('GET', $url);

        self::assertEquals(
            200,
            $client->getResponse()->getStatusCode(),
            'La page ' . $pageName . ' est inacessible'
        );
    }

    public function testPageAccessDataProvider()
    {
        $this->exploitation = $this->getRandomExploitation();
        $expl_id = $this->exploitation->getId();
        $annee = $this->exploitation->getLastAnneeEncodage();

        return [
            ['saisie_new_exp_getcolline', 'Création d\'une exploitation', []],
            ['saisie_recherche', 'Recherche ou modification d\'une exploitations', []],
            ['saisie_info_gen', 'InfoGen', ['annee' => $annee, 'exploitation' => $expl_id]],
            ['saisie_f0_membre', 'F0', ['annee' => $annee, 'id' => $expl_id]],
            ['saisie_f1_membre', 'F1', ['annee' => $annee, 'id' => $expl_id]],
            ['saisie_f4_membre', 'F4', ['annee' => $annee, 'id' => $expl_id]],
            ['saisie_f5_membre', 'F5', ['annee' => $annee, 'id' => $expl_id]],
        ];
    }

    public function testSearchExploitation()
    {
        $exploitation = $this->getRandomExploitation();
        $numeroMembre = $exploitation->getNumeroMembre();

        $client = self::createClient([], [
            'PHP_AUTH_USER' => $this->getParam('test_user'),
            'PHP_AUTH_PW' => $this->getParam('test_pwd'),
        ]);

        $crawler = $client->request('GET', '/saisie/recherche');

        $form = $crawler->selectButton('Rechercher')->form();
        $form['recherche[numeroMembre]']->setValue($numeroMembre);

        $client->submit($form);
        $response = $client->getResponse();

        self::assertEquals(
            302,
            $response->getStatusCode(),
            'La coopérative ' . $numeroMembre . ' n\' a pas été trouvée'
        );

        // suivre toutes les redirections
        $client->followRedirects();
        $crawler = $client->followRedirect();

        // vérifie qu'on affiche 1 seule ligne de résultat (la coop avec l'id)
        // 2 car ligne titre + ligne de résultat

        // pas tjs car si pas d'année ouverte :/
        $this->marktestSkipped('car pbm si pas de collectes / enquêtes ouvertes');

        self::assertEquals(
            2,
            $crawler->filter('tr.index-row')->count(),
            'Il n\'y a pas une seule ligne de résultat'
        );

        // verifie que numéro XXX-XXX-XXX est dans la page
        self::assertContains(
            $exploitation->getNumeroMembreForHuman(),
            $client->getResponse()->getContent(),
            'Le numéro ' . $exploitation->getNumeroMembreForHuman() . ' ne se trouve pas dans la page'
        );
    }
}
