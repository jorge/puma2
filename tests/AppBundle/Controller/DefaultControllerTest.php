<?php

declare(strict_types=1);

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
final class DefaultControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/');

        /* vérifie que la page répond */
        self::assertEquals(200, $client->getResponse()->getStatusCode());

        /* vérifie que le contenu du titre est celui attendu */
        self::assertContains('Bienvenue sur PUMA 2', $crawler->text());
    }
}
