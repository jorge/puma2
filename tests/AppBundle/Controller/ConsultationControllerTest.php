<?php

declare(strict_types=1);

namespace Tests\AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

/**
 * @internal
 * @coversNothing
 */
final class ConsultationControllerTest extends WebTestCase
{
    public function testGetRecordsetSignaletique()
    {
        $client = self::createClient();

        $crawler = $client->request('GET', '/consultation/signaletique');

        self::markTestSkipped('Manque le parametre annee_passees');
        /* vérifie que la page répond */
        self::assertEquals(200, $client->getResponse()->getStatusCode());
        /* submit the form */
        $form = $crawler->selectButton('consultation_signaletique_submit')->form();

        // submit the form
        $crawler = $client->submit($form);
        self::assertEquals(340, $crawler->filter('tr.index-row')->count());
    }
}
