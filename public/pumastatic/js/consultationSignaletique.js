/**
 * javascript lié au formulaire consultation signaletique
 */
var ConsultationSignaletique = {
        onReady : function () {            
            // ConsultationSignaletique.showDefault();
            $('#consultation_signaletique_exploitationGroupAnnee').click(ConsultationSignaletique.showDefault);
            $('#consultation_signaletique_queryTheme').change(ConsultationSignaletique.queryTheme);
            $('#consultation_signaletique_exploitantTheme').change(ConsultationSignaletique.showExploitant);
            $('#consultation_signaletique_chargesTheme').change(ConsultationSignaletique.showCharges);
            // ConsultationSignaletique.showDefault();
        },
        
        hideAll : function(){
            $(".form-group:has(.show-exploitation)").hide();
            $(".form-group:has(.show-exploitant)").hide();
            $(".form-group:has(.show-charges)").hide();
            return
        },
        
        hideChargesAll : function(){
            $(".form-group:has(.show-scolarise)").hide();
            $(".form-group:has(.show-nombre)").hide();
            return
        }, 
        
        hideExploitantAll : function(){
            $(".form-group:has(.show-age)").hide();
            $(".form-group:has(.show-etatCivil)").hide();
            $(".form-group:has(.show-genre)").hide();
        },
        
        queryTheme : function(e){
            var valToDisplay;
            valToDisplay = e.target.value;
            ConsultationSignaletique.hideAll();
            switch(valToDisplay) {
            case 'exploitation':
                ConsultationSignaletique.showExploitation();
                break;
            case 'exploitant':
                ConsultationSignaletique.showExploitant();
                break;
            case 'charges':
                ConsultationSignaletique.showCharges();
                break;
            } 
            return;
        },
        
        showCharges : function(e){
            var valToDisplay;            
            $(".form-group:has(.show-charges)").show();  
            if(e){
                valToDisplay = e.target.value;
            }
            else{
                /* valeur par défaut */
                valToDisplay = 'nombre';
                $('input:radio[name="consultation_signaletique[chargesTheme]"][value=nombre]').click();
            }
            ConsultationSignaletique.hideChargesAll();
            switch(valToDisplay) {
            case 'scolarise':
                ConsultationSignaletique.showChargesScolarise();
                break;
            default:
                ConsultationSignaletique.showChargesNombre();
                break;
            } 
            return;
        },
        
        showChargesNombre : function(){
            $(".form-group:has(.show-nombre)").show();
        },
        
        showChargesScolarise : function(){
            $(".form-group:has(.show-scolarise)").show();
        },
        
        showExploitant : function(e){            
            var valToDisplay;            
            $(".form-group:has(.show-exploitant)").show();
            if(e){
                valToDisplay = e.target.value;
            }
            else{
                /* valeur par défaut */
                valToDisplay = 'age';
                $('input:radio[name="consultation_signaletique[exploitantTheme]"][value=age]').click();
            }
            ConsultationSignaletique.hideExploitantAll();
            switch(valToDisplay) {
            case 'etat civil':
                ConsultationSignaletique.showExploitantEtatCivil();
                break;
            case 'genre':
                ConsultationSignaletique.showExploitantGenre();
                break;
            default:
                ConsultationSignaletique.showExploitantAge();
                break;
            } 
            return;
        },
        
        showExploitantAge : function(){
            $(".form-group:has(.show-age)").show();
        },
        
        showExploitantEtatCivil : function(){
            $(".form-group:has(.show-etatCivil)").show();
        },
        
        showExploitantGenre : function(){
            $(".form-group:has(.show-genre)").show();
        },
        
        showExploitation : function(){
            $(".form-group:has(.show-exploitation)").show();
            if ($('#consultation_signaletique_exploitationGroupAnnee:checked').val()){
                $('.form-group:has(#consultation_signaletique_exploitationListAnnee)').hide();
            }else {
                $('.form-group:has(#consultation_signaletique_exploitationListAnnee)').show();
            }
        },
        
        showDefault : function(){
            ConsultationSignaletique.hideAll();
            ConsultationSignaletique.showExploitation();
        },
        
}
$(document).on("ready", function () {
    ConsultationSignaletique.onReady();
    
});