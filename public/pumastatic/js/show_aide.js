var helpElem = document.getElementById('help');

if(helpElem != undefined) {
    helpElem.style.display = 'none';
}

function toggle_visibility() {
    if(helpElem != undefined) {
        helpElem.style.display = 'none';
    }
}

function change_visibility() {
    if(helpElem != undefined) {
        if(helpElem.style.display == 'none') {
            helpElem.style.display='block';
        } else {
            helpElem.style.display='none';
        }
    }
}

function change_visibility_details() {
    var elements = document.getElementsByClassName('alert');
    for(var i=0; i < elements.length; i++) {
        if(elements[i].style.display == 'none'){
            elements[i].style.display = 'block';
        }else{
            elements[i].style.display='none';
        }
    }       
}