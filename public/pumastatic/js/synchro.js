/** TODO
    - chopper les status à partir de PHP
    - avoir l'url du master à partir de PHP
    - pbm de timout quand on interroge le fichier


    - ERREUR SI remote introuvables -> bloqué


    - pbm avec STATUS_REMOTE_INTROUVABLES
*/

function rmClass(el, className) {
    if (el.classList) {
        el.classList.remove(className);
    } else {
        el.className = el.className.replace(new RegExp('(^|\\b)' + className.split(' ').join('|') + '(\\b|$)', 'gi'), ' ');
    }
}


var Synchro = (function(synchro_master_url) {
    // variables
    var mytimer= 0;
    var loadCounts= 0;

    // init & events
    function exec(cmdName, arg) {
        var cmd = this[cmdName];
        $(".letsynchro").click(function() { cmd(arg); });
    }

    function creationEnvoiFichierLocal() {
        $.ajax({method: "GET",
            url: "/synchro/creation-envoi-fichier-local",
            timeout: 0,
            success: function(data) {
                var session_id = data.session_id;

                if(data.status === 'archive envoyee') {
                    rmClass(
                        document.getElementById("container-creation-envoi-fichier-local-ok"),
                        "hidden");
                    masterSynchronize(session_id);
                } else {
                    rmClass(
                        document.getElementById("container-creation-envoi-fichier-local-ko"),
                        "hidden");
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                rmClass(
                    document.getElementById("container-creation-envoi-fichier-local-ko"),
                    "hidden");
            },
        });
    }

    function masterSynchronize(session_id) {// TODO si pbm du cote MD5 recommencer envoi
        console.log("Le master utilisé pour la synch se trouve " + synchro_master_url);
        if(synchro_master_url) {
            // on retire le / à la fin de l'url (si il a été mis)
            if(synchro_master_url.slice(-1) === '/') {
                synchro_master_url = synchro_master_url.slice(0,-1);
            }

            $.ajax({method: "GET",
                url: synchro_master_url + "/synchro/master/synchronize/" + session_id,
                //timeout: 0,
                success: function(data) {
                    var session_id = data.session_id;

                    if(data.master_status === 'load load sql ok') {
                        rmClass(
                            document.getElementById("container-synchronize-master-ok"),
                            "hidden");
                        checkFichierMasterBienRecu(); //TODO
                    } else {
                        rmClass(
                            document.getElementById("container-synchronize-master-ko"),
                            "hidden");
                    }
                },
                error: function(jqXHR, exception, errorThrown ) {
                    var msg = '';
                    console.log(jqXHR);
                    console.log(jqXHR.status);
                    console.log(exception);
                    console.log(errorThrown);
                    if (jqXHR.status === 0) {
                        msg = 'Not connect.\n Verify Network.';
                    } else if (jqXHR.status == 404) {
                        msg = 'Requested page not found. [404]';
                    } else if (jqXHR.status == 500) {
                        msg = 'Internal Server Error [500].';
                    } else if (exception === 'parsererror') {
                        msg = 'Requested JSON parse failed.';
                    } else if (exception === 'timeout') {
                        msg = 'Time out error.';
                    } else if (exception === 'abort') {
                        msg = 'Ajax request aborted.';
                    } else {
                        msg = 'Uncaught Error.\n' + jqXHR.responseText;
                    }
                    console.log(msg);
                    rmClass(
                        document.getElementById("container-synchronize-master-ko"),
                        "hidden");
                },
            });
        } else {
            console.log("La variable synchro_master_url n'existe pas, impossible d'executer la fonction masterSynchronize");
            rmClass(
                document.getElementById("container-synchronize-master-ko"),
                "hidden");
        }
    }

    function checkFichierMasterBienRecu() {
        console.log('checkFichierMasterBienRecu'); // TODO ?
        return traitementDonneesMaster();
    }

    function traitementDonneesMaster() {// TODO si pbm du cote MD5 recommencer envoi
        $.ajax({method: "GET",
            url: "/synchro/traitement-donnees-master",
            timeout: 0,
            success: function(data) {
                var session_id = data.session_id;
                console.log(data);

                if(data.status === 'load load sql ok') {
                    console.log('ok');
                    rmClass(
                        document.getElementById("container-traitement-data-from-master-ok"),
                        "hidden");
                } else {
                    console.log('error');
                    rmClass(
                        document.getElementById("container-traitement-data-from-master-ko"),
                        "hidden");
                }
            },
            error: function(jqXHR, textStatus, errorThrown ) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);
                rmClass(
                    document.getElementById("container-traitement-data-from-master-ko"),
                    "hidden");
            },
        });
    }

    return {
        exec: exec,
        creationEnvoiFichierLocal: creationEnvoiFichierLocal,
        masterSynchronize: masterSynchronize,
        traitementDonneesMaster: traitementDonneesMaster,
    };
});
