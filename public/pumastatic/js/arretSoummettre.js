/**
 * javascript lié au formulaire consultation f0
 *
 * 
 * attention a besoin des variables avecInrant, avecExtrant (et sansIntrantExtrant (optionnel))
 */

var socies = {
        onReady : function () {
			$(".form-group:has(.btn)").prop( "disabled", true );
		//	socies.envoyer.disabled=false;
            $('.permettre').click($(".form-group:has(.btn)").prop( "disabled", false ));
            $('.permettre').change($(".form-group:has(.btn)").prop( "disabled", false ));
        },

}
$(document).on("ready", function () {
    socies.onReady();
});