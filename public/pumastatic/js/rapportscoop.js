/**
 * javascript lié au formulaire consultation f0
 */

function afficherInput(numero){
	var res=prompt("definir l'année de la recherche","2015");
	
	var myan="&annee="+res;

	document.getElementById("l"+numero).href="coopnum?num="+numero+myan
}


var rapports_cooperatives = {
        onReady : function () {            
            rapports_cooperatives.hideAll();
			$('#rapports_cooperatives_exploitationGroupAnnee').click(rapports_cooperatives.showDefault);
        },
        
        hideAll : function(){
            $(".form-group:has(.show-Formations_recus)").hide();
            return
        },
		showExploitation : function(){
            $(".form-group:has(.show-exploitation)").show();
            if ($('#rapports_cooperatives_exploitationGroupAnnee:checked').val()){
                $('.form-group:has(#rapports_cooperatives_exploitationListAnnee)').hide();
            }else {
                $('.form-group:has(#rapports_cooperatives_exploitationListAnnee)').show();
            }
        },
        showDefault : function(){
            rapports_cooperatives.hideAll();
            rapports_cooperatives.showExploitation();
        },
}
$(document).on("ready", function () {
    rapports_cooperatives.onReady();
});