const divCurrentPhoto = document.querySelector('#current-photo');
const runCameraButton = document.querySelector('#run-camera-button');
const takePictureButton = document.querySelector('#take-picture-button');
const uploadPictureButton = document.querySelector('#upload-picture-button');
const cameraImg = document.querySelector('#camera-img');
const cameraVideo = document.querySelector('#camera-video');
const canvas = document.createElement('canvas');

const STATE_CAMERA_OFF = 0;
const STATE_CAMERA_ON = 1;
const STATE_CAMERA_SHOW_PICTURE = 2;

let status = STATE_CAMERA_OFF;

let display = function(el) {
    el.style.display = "initial";
}

let hide = function(el) {
    el.style.display = "none";
}

runCameraButton.onclick = function() {
    // try to start camera stream
    navigator.mediaDevices.getUserMedia({video: { facingMode: "environment" }}).
        then(videoHandleSuccess).catch(videoHandleError);
};

function videoHandleSuccess(stream) {
    // start display camera stream
    cameraVideo.srcObject = stream;
    status = STATE_CAMERA_ON;
    displayInterface();
}

function videoHandleError(error) {
    console.error('Error: ', error);
}

takePictureButton.onclick = cameraVideo.onclick = function() {
    canvas.width = cameraVideo.videoWidth;
    canvas.height = cameraVideo.videoHeight;
    canvas.getContext('2d').drawImage(cameraVideo, 0, 0);
     // Other browsers will fall back to image/png
    cameraImg.src = canvas.toDataURL('image/webp');

    cameraVideo.srcObject.getVideoTracks()[0].stop(); // stop the stream

    status = STATE_CAMERA_SHOW_PICTURE;
    displayInterface();
};

function b64ToUint8Array(b64Image) {
    var img = atob(b64Image.split(',')[1]);
    var img_buffer = [];
    var i = 0;
    while (i < img.length) {
       img_buffer.push(img.charCodeAt(i));
       i++;
    }
    return new Uint8Array(img_buffer);
}
 
uploadPictureButton.onclick = function() {
    let b64Image = canvas.toDataURL('image/jpeg');
    let u8Image  = b64ToUint8Array(b64Image);
    
    let formData = new FormData();
    formData.append("photo", new Blob([ u8Image ], {type: "image/jpg"}));
    
    let xhr = new XMLHttpRequest();
    xhr.open("POST", uploadUrl, true);

    xhr.onload = function() {
        location.reload(); // refresh the page to display the result
    };

    xhr.send(formData);
}

let displayInterface = function() {
    if(status == STATE_CAMERA_OFF) {
        display(divCurrentPhoto);
        display(runCameraButton);
        hide(takePictureButton);
        hide(cameraImg);
        hide(cameraVideo);
        hide(uploadPictureButton);
    } else if (status == STATE_CAMERA_ON) {
        hide(divCurrentPhoto);
        hide(runCameraButton);
        display(takePictureButton);
        hide(cameraImg);
        display(cameraVideo);
        hide(uploadPictureButton);
    } else { // if (status == STATE_CAMERA_SHOW_PICTURE) {
        hide(divCurrentPhoto);
        hide(runCameraButton);
        hide(takePictureButton);
        display(cameraImg);
        hide(cameraVideo);
        display(uploadPictureButton);
    }
}

displayInterface();




