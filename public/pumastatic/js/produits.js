/**
 * javascript lié au formulaire get_localisation
 */
var animaux = {
    onReady : function () {
        $('#animaux_espece').change(animaux.updProduitsSelectors);
    },

    updProduitsSelectors : function (e) {
        var myid, $form, data;
        myid = e.target.id;
        $form = $('#' + myid).closest('form');
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
        //    url : '/saisies/getlocalisationform',
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current groupement field ...
                $('#animaux_produits').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#animaux_produits'));
            }
        });
    }
}

$(document).on("ready", function () {
    animaux.onReady();
});
