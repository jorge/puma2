/**
 * javascript lié au formulaire consultation f0
 */


var rapports_groupements = {
        onReady : function () {            
            rapports_groupements.hideAll();
			$('#rapports_groupements_exploitationGroupAnnee').click(rapports_groupements.showDefault);
        },
        
        hideAll : function(){
            $(".form-group:has(.show-Formations_recus)").hide();
            return
        },
		showExploitation : function(){
            $(".form-group:has(.show-exploitation)").show();
            if ($('#rapports_groupements_exploitationGroupAnnee:checked').val()){
                $('.form-group:has(#rapports_groupements_exploitationListAnnee)').hide();
            }else {
                $('.form-group:has(#rapports_groupements_exploitationListAnnee)').show();
            }
        },
        showDefault : function(){
            rapports_groupements.hideAll();
            rapports_groupements.showExploitation();
        },
}
$(document).on("ready", function () {
    rapports_groupements.onReady();
});