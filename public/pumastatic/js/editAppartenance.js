/**
 * javascript lié au formulaire get_localisation
 */
var getAppartenance = {
    onReady : function () {
        $('#pivot_exploitation_organisations_cooperative').change(
            getAppartenance.updateOrganisationSelector);
    },

    updateOrganisationSelector: function(e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();

        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#pivot_exploitation_organisations_organisation').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#pivot_exploitation_organisations_organisation'));
            }
        });
    },

};

$(document).on("ready", function () {
    getAppartenance.onReady();
});
