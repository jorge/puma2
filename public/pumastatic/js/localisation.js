/**
 * javascript lié au formulaire get_localisation
 */
var localisation = {
    onReady : function () {
        $('#localisation_province').change(localisation.updCommSelectors);
        $('#localisation_commune').change(localisation.updCollSelectors);
    },

    updCollSelectors : function (e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
        //    url : '/saisies/getlocalisationform',
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#localisation_colline').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#localisation_colline'));
            }
        });
    },

    updCommSelectors : function (e) {
        var myid, $form, data;
        myid = e.target.id;
        $form = $('#' + myid).closest('form');
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
        //    url : '/saisies/getlocalisationform',
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current groupement field ...
                $('#localisation_commune').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#localisation_commune'));
                $('#localisation_commune').change(localisation.updCollSelectors);
                localisation.updCollSelectors($('#localisation_commune'));
				// activer boutton new exploitation
	//			getlocalisation.updcollSelectors($('#newexpl'));
	//			$('#newexpl').attr('disabled',false);
            }
        });
    }
}

$(document).on("ready", function () {
    localisation.onReady();
});
