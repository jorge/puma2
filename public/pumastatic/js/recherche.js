/**
 * javascript lié au formulaire recherche
 */
var recherche = {
    onReady : function () {
        $('#recherche_province').change(recherche.updCommSelectors);
        $('#recherche_commune').change(recherche.updCollSelectors);
        $('#recherche_cooperative').change(recherche.updGroupSelectors);
    },

    updGroupSelectors : function (e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#recherche_groupement').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#recherche_groupement'));
                $('#recherche_groupement').select2();
            }
        });
    },

    updCollSelectors : function (e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#recherche_colline').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#recherche_colline'));
            }
        });
    },

    updCommSelectors : function (e) {
        var myid, $form, data;
        myid = e.target.id;
        $form = $('#' + myid).closest('form');
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current groupement field ...
                $('#recherche_commune').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#recherche_commune'));
                $('#recherche_commune').change(recherche.updCollSelectors);
                recherche.updCollSelectors($('#recherche_commune'));
				// activer boutton new exploitation
            }
        });
    }
}

$(document).on("ready", function () {
    recherche.onReady();
});
