var intrants = [];
var extrants = [];


/**
 * javascript lié au formulaire consultation f0
 */
var parcelle_operation_culturale = {
        onReady : function () {
            
            parcelle_operation_culturale.hideAll();
			$('#parcelle_operation_culturale_type').click(parcelle_operation_culturale.queryTheme);
            $('#parcelle_operation_culturale_type').change(parcelle_operation_culturale.queryTheme);
            $('#parcelle_operation_culturale_type').trigger("click");
            
            $('#parcelle_operation_culturale_cultures').change(parcelle_operation_culturale.updateExtrans);
            $('#parcelle_operation_culturale_cultures').click(parcelle_operation_culturale.updateExtrans);
            $('#parcelle_operation_culturale_type').change(parcelle_operation_culturale.updateIntrants);
        },

        hideAll : function(){
            $(".form-group:has(.show-recoltes)").hide();
            $(".form-group:has(.show-option1)").hide();
            $(".form-group:has(.show-option2)").hide();
            return
        },
        updateExtrans : function(e){
            var elem_id , form, data, selected_cultures;
            if(e.target){
                elem_id = e.target.id;
            }else{
                elem_id = e.id;
            }
            // ... retrieve the corresponding form.
            form = $('#' + elem_id).closest('form');
            // Simulate form data, but only include the selected value.
            data = {};
            selected_cultures =[];
            $("#parcelle_operation_culturale_cultures input").each(function(){
                if($(this).is(':checked')) {
                    selected_cultures.push(parseInt($(this).val()));
                }
            });

            data["parcelle_operation_culturale"] = {
                "cultures": selected_cultures
            };
 
            // Submit data via AJAX to the form's action path.
            $.ajax({
                url : form.attr('action'),
                type : form.attr('method'),
                data : data,
                success : function (html) {
                    $('#parcelle_operation_culturale_produitPrimaire').replaceWith(
                       $(html).find('#parcelle_operation_culturale_produitPrimaire'));
                }
            });
 
        },
        updateIntrants : function(e){
            var op_cultu = $(this);
            var form = $(this).closest('form');
            var data = {};
            data[op_cultu.attr('name')] = op_cultu.val();
 
            // Submit data via AJAX to the form's action path.
            $.ajax({
                url : form.attr('action'),
                type : form.attr('method'),
                data : data,
                success : function (html) {
                    $('#parcelle_operation_culturale_intrant').replaceWith(
                       $(html).find('#parcelle_operation_culturale_intrant'));
                }
            });
        },
        queryTheme : function(e){
            var valToDisplay;
            valToDisplay = e.target.value;
            parcelle_operation_culturale.hideAll();

            if (extrants.indexOf(valToDisplay) != -1) { // valToDisplay se trouve dans extrants
                parcelle_operation_culturale.recoltes();
            } else if (intrants.indexOf(valToDisplay) != - 1) {
                parcelle_operation_culturale.semences();
            } else {
                parcelle_operation_culturale.amendement();
            }
        },
        recoltes : function(e) {
            $(".form-group:has(.show-recoltes)").show(); // extrants
		},
        semences : function(e) {
            $(".form-group:has(.show-option1)").show(); // seul
		},
		amendement : function(e) {
			 $(".form-group:has(.show-option2)").show(); // intrants
		},
        showDefault : function(){
            parcelle_operation_culturale.hideAll();
        },
}
$(document).on("ready", function () {
    parcelle_operation_culturale.onReady(seuls, intr);
});