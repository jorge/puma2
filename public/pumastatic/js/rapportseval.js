/**
 * javascript lié au formulaire consultation f0
 */

function afficherInput(numero){
	var annee=prompt("definir l'année de la recherche","2015");
	document.getElementById("l"+numero).href="/fr/rapports/evalnum/" + numero + "?annee=" + annee;
}

var rapports_special_f0 = {
        onReady : function () {            
            rapports_special_f0.hideAll();
			$('#rapports_special_f0_exploitationGroupAnnee').click(rapports_special_f0.showDefault);
        },
        
        hideAll : function(){
            $(".form-group:has(.show-Formations_recus)").hide();
            return
        },
		showExploitation : function(){
            $(".form-group:has(.show-exploitation)").show();
            if ($('#rapports_special_f0_exploitationGroupAnnee:checked').val()){
                $('.form-group:has(#rapports_special_f0_exploitationListAnnee)').hide();
            }else {
                $('.form-group:has(#rapports_special_f0_exploitationListAnnee)').show();
            }
        },
        showDefault : function(){
            rapports_special_f0.hideAll();
            rapports_special_f0.showExploitation();
        },
}
$(document).on("ready", function () {
    rapports_special_f0.onReady();
});