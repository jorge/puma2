/**
 * javascript lié au formulaire get_localisation
 */
var getlocalisation = {
    onReady : function () {
        $('#get_groupe_and_localisation_cooperative').change(
            getlocalisation.updateGroupementSelector);
        $('#get_groupe_and_localisation_province').change(
            getlocalisation.updCommSelectors);
        $('#get_groupe_and_localisation_commune').change(getlocalisation.updCollSelectors);
    },

    updateGroupementSelector: function(e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();

        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#get_groupe_and_localisation_groupement').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#get_groupe_and_localisation_groupement'));
            }
        });
    },

    updCollSelectors : function (e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#get_groupe_and_localisation_colline').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#get_groupe_and_localisation_colline'));
            }
        });
    },


    updCommSelectors : function (e) {
        var myid, $form, data;
        myid = e.target.id;
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                console.log('Succes');
                // Replace current groupement field ...
                $('#get_groupe_and_localisation_commune').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#get_groupe_and_localisation_commune'));
                $('#get_groupe_and_localisation_commune').change(getlocalisation.updCollSelectors);
                getlocalisation.updCollSelectors($('#get_groupe_and_localisation_commune'));
				// activer boutton new exploitation
	//			getlocalisation.updcollSelectors($('#newexpl'));
	//			$('#newexpl').attr('disabled',false);
            }
        });
    }
};

$(document).on("ready", function () {
    getlocalisation.onReady();
});
