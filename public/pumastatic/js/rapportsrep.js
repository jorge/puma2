/**
 * javascript lié au formulaire consultation f0
 */


var rapports_filieres = {
        onReady : function () {            
            rapports_filieres.hideAll();
			$('#rapports_filieres_exploitationGroupAnnee').click(rapports_filieres.showDefault);
        },
        
        hideAll : function(){
            $(".form-group:has(.show-Formations_recus)").hide();
            return
        },
		showExploitation : function(){
            $(".form-group:has(.show-exploitation)").show();
            if ($('#rapports_filieres_exploitationGroupAnnee:checked').val()){
                $('.form-group:has(#rapports_filieres_exploitationListAnnee)').hide();
            }else {
                $('.form-group:has(#rapports_filieres_exploitationListAnnee)').show();
            }
        },
        showDefault : function(){
            rapports_filieres.hideAll();
            rapports_filieres.showExploitation();
        },
}
$(document).on("ready", function () {
    rapports_filieres.onReady();
});