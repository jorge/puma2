/**
 * javascript lié au formulaire get_localisation
 */
var get_groupe = {
    onReady : function () {
        $('#get_groupe_cooperative').change(
            get_groupe.updateGroupementSelector);
    },

    updateGroupementSelector: function(e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();

        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#get_groupe_groupement').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#get_groupe_groupement'));
            }
        });
    },

};

$(document).on("ready", function () {
    get_groupe.onReady();
});
