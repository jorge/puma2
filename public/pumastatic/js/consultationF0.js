/**
 * javascript lié au formulaire consultation f0
 */
var ConsultationF0 = {
        onReady : function () {            
            ConsultationF0.hideAll();
			$('#consultation_f0_exploitationGroupAnnee').click(ConsultationF0.showDefault);
            $('#consultation_f0_queryTheme').change(ConsultationF0.queryTheme);
        },
        
        hideAll : function(){
            $(".form-group:has(.show-groupeCible)").hide();
            $(".form-group:has(.show-choixGroup)").hide();
            $(".form-group:has(.show-groupProf)").hide();
            $(".form-group:has(.show-Terres)").hide();
            $(".form-group:has(.show-Cotisations)").hide();
            $(".form-group:has(.show-Filieres)").hide();
            $(".form-group:has(.show-Animaux)").hide();
            $(".form-group:has(.show-Commercialisation)").hide();
            $(".form-group:has(.show-Formations_recus)").hide();
            return
        },
        queryTheme : function(e){
            var valToDisplay;
            valToDisplay = e.target.value;
			ConsultationF0.hideAll();
		//	alert(valToDisplay);
            switch(valToDisplay) {
            	case 'etudes':
                	ConsultationF0.groupeCible();
                	ConsultationF0.choixGroup();
               		break;
				case 'professions':
                	ConsultationF0.groupProf();
               		break;
				case 'terres':
                	ConsultationF0.Terres();
               		break;
				case 'cotisations':
                	ConsultationF0.Cotisations();
               		break;
				case 'filieres':
                	ConsultationF0.Filieres();
              		break;
            	case 'animaux':
                	ConsultationF0.Animaux();
                	break;
				case 'commercialisation':
                	ConsultationF0.Commercialisation();
               		break;
				case 'forms_recus':
                	ConsultationF0.Formations_recus();
               		break;
            } 
			
            return;
        },
        groupeCible : function(e) {
			 $(".form-group:has(.show-groupeCible)").show();	
		},
		choixGroup : function(e) {
			 $(".form-group:has(.show-choixGroup)").show();	
		},
		groupProf : function(e) {
			 $(".form-group:has(.show-groupProf)").show();	
		},
		Terres : function(e) {
			 $(".form-group:has(.show-Terres)").show();	
		},
		Cotisations : function(e) {
			 $(".form-group:has(.show-Cotisations)").show();	
		},
        Filieres : function(e) {
			 $(".form-group:has(.show-Filieres)").show();	
		},
		Animaux : function(e) {
			 $(".form-group:has(.show-Animaux)").show();	
		},
		Commercialisation : function(e) {
			 $(".form-group:has(.show-Commercialisation)").show();	
		},
		Formations_recus : function(e) {
			 $(".form-group:has(.show-Formations_recus)").show();	
		},
		showExploitation : function(){
            $(".form-group:has(.show-exploitation)").show();
            if ($('#consultation_f0_exploitationGroupAnnee:checked').val()){
                $('.form-group:has(#consultation_f0_exploitationListAnnee)').hide();
            }else {
                $('.form-group:has(#consultation_f0_exploitationListAnnee)').show();
            }
        },
        showDefault : function(){
            ConsultationF0.hideAll();
            ConsultationF0.showExploitation();
        },
}
$(document).on("ready", function () {
    ConsultationF0.onReady();
});