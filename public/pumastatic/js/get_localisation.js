/**
 * javascript lié au formulaire get_localisation
 */
var getlocalisation = {
    onReady : function () {
        
        $('#getlocalisation_province').change(getlocalisation.updCommSelectors);
        $('#getlocalisation_commune').change(getlocalisation.updCollSelectors);
    },

    updCollSelectors : function (e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#getlocalisation_colline').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#getlocalisation_colline'));
            }
        });
    },

    updCommSelectors : function (e) {
        var myid, $form, data;
        myid = e.target.id;
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();    
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current groupement field ...
                $('#getlocalisation_commune').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#getlocalisation_commune'));
                $('#getlocalisation_commune').change(getlocalisation.updCollSelectors);
                getlocalisation.updCollSelectors($('#localisation_commune'));
				// activer boutton new exploitation
	//			localisation.updcollSelectors($('#newexpl'));
	//			$('#newexpl').attr('disabled',false);
            }
        });
    }
}

$(document).on("ready", function () {
    getlocalisation.onReady();
});




