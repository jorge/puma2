/**
 * javascript lié au formulaire get_localisation
 */
var get_organisation = {
    onReady : function () {
        $('#get_organisation_cooperative').change(
            get_organisation.updateGroupementSelector);
    },

    updateGroupementSelector: function(e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();

        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#get_organisation_groupement').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#get_organisation_groupement'));
            }
        });
    },

};

$(document).on("ready", function () {
    get_organisation.onReady();
});
