/**
 * javascript lié au formulaire saisie_info_gen
 */

var PourImprimerCarte = {
    onReady : function () {
        
        $('#imprimecartes').click(PourImprimerCarte.showDefault);
        $('#test').click(PourImprimerCarte.showTest);
    },
    hideAll : function(){
	//	$('#imprimecartes').hide();
		return
    },

	showTest : function(){
        $('#imprimecartes').show();
    },
	
    showDefault : function(){
        PourImprimerCarte.hideAll();
    },

}

$(document).on("ready", function () {
    PourImprimerCarte.onReady();
});