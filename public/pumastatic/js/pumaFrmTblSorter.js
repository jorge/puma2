/**
 * On utilise uniquement le js principal
 */
var PumaFrmTblSorter = {
        onReady : function (sortList) {
            $.tablesorter.themes.bootstrap = {
                    // these classes are added to the table. To see other table classes available,
                    // look here: http://getbootstrap.com/css/#tables
                    table        : 'table table-bordered table-striped',
                    caption      : 'caption',
                    // header class names
                    header       : 'bootstrap-header', // give the header a gradient background (theme.bootstrap_2.css)
                    sortNone     : '',
                    sortAsc      : '',
                    sortDesc     : '',
                    active       : '', // applied when column is sorted
                    hover        : '', // custom css required - a defined bootstrap style may not override other classes
                    // icon class names
                    icons        : '', // add "icon-white" to make them white; this icon class is added to the <i> in the header
                    iconSortNone : 'bootstrap-icon-unsorted', // class name added to icon when column is not sorted
                    iconSortAsc  : 'glyphicon glyphicon-chevron-up', // class name added to icon when column has ascending sort
                    iconSortDesc : 'glyphicon glyphicon-chevron-down', // class name added to icon when column has descending sort
                    filterRow    : '', // filter row class; use widgetOptions.filter_cssFilter for the input/select element
                    footerRow    : '',
                    footerCells  : '',
                    even         : '', // even row zebra striping
                    odd          : ''  // odd row zebra striping
                  };
            $("#index-table").tablesorter({
                theme : "bootstrap",
                widthFixed: false,
                headerTemplate : '{content} {icon}',
                widgets : [ "uitheme", "filter"],
                sortList: sortList,
                widgetOptions : {
                    // using the default zebra striping class name, so it actually isn't included in the theme variable above
                    // this is ONLY needed for bootstrap theming if you are using the filter widget, because rows are hidden
                    //zebra : ["even", "odd"],

                    // reset filters button
                    filter_reset : ".reset",

                    // extra css class name (string or array) added to the filter element (input or select)
                    //filter_cssFilter: "form-control",

                    // set the uitheme widget to use the bootstrap theme class names
                    // this is no longer required, if theme is set
                    // ,uitheme : "bootstrap"

                  }
            });
            $("#exp2excel").click(PumaFrmTblSorter.export2excel);
        } ,

        export2excel: function(e){
            var myname;
            myname = $(e.target).attr('xlfilename');
            $("#index-table").table2excel({
            // exclude CSS class
            exclude: ".noExcelExport",
            name: "fm puma2",
            filename: myname //do not include extension
            });
        }
}


if ((typeof(defaultTblSort)  == 'undefined') || (defaultTblSort)) {
    // par defaut pour éviter le code  initJS à faux
    $(document).on("ready", function () {
        PumaFrmTblSorter.onReady([[1,0], [2,0]]);
    });
}