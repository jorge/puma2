/**
 * javascript lié au formulaire saisie_info_gen
 * TODO DELETE
 */

var SaisieInfoGen = {
    onReady : function () {

        $('#info_gen_cooperative').change(SaisieInfoGen.updGrpSelectors);
        $('#info_gen_groupement').change(SaisieInfoGen.updExpSelectors);
        $('#info_gen_exploitation').change(SaisieInfoGen.updParcelles);
        SaisieInfoGen.viewParcellesButton();
    },

    updExpSelectors : function (e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();

//        console.log(myid + ' - ' + $form + ' - ' + data);

        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#info_gen_exploitation').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#info_gen_exploitation'));
                $('#info_gen_exploitation').change(SaisieInfoGen.updParcelles);
                SaisieInfoGen.updParcelles($('#info_gen_exploitation'));
            }
        });
    },

    updGrpSelectors : function (e) {
        var myid, $form, data;
        myid = e.target.id;
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
//        console.log(myid + ' - ' + $form + ' - ' + data);

        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current groupement field ...
                $('#info_gen_groupement').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#info_gen_groupement'));
                $('#info_gen_groupement').change(SaisieInfoGen.updExpSelectors);
                SaisieInfoGen.updExpSelectors($('#info_gen_groupement'));
				// activer boutton new exploitation
				SaisieInfoGen.updExpSelectors($('#newexpl'));
	//			$('#newexpl').attr('disabled',false);
            }
        });
    },

    updParcelles : function (e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current parcelle field ...
                $('#info_gen_parcelle').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#info_gen_parcelle'));
                SaisieInfoGen.viewParcellesButton();
                // Replace current annee field ...
                $('#info_gen_datadate').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#info_gen_datadate'));
            }
        });
    },

    viewParcellesButton : function() {
        if($('#info_gen_parcelle').html() == ''){
            $('#info_gen_saisie_parcelles').hide();
        }else{
            $('#info_gen_saisie_parcelles').show();
        }
        return;
    }

}

$(document).on("ready", function () {
    SaisieInfoGen.onReady();
});
