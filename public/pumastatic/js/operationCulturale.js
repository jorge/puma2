/**
 * javascript lié au formulaire consultation f0
 *
 * 
 * attention a besoin des variables avecInrant, avecExtrant (et sansIntrantExtrant (optionnel))
 */

var parcelle_operation_culturale = {
        onReady : function () {
            parcelle_operation_culturale.hideAll();
			$('#parcelle_operation_culturale_type').click(parcelle_operation_culturale.hideShowIntrantExtrantForm);
            $('#parcelle_operation_culturale_type').change(parcelle_operation_culturale.hideShowIntrantExtrantForm);
            $('#parcelle_operation_culturale_type').trigger("click");
            
            $('#parcelle_operation_culturale_cultures').change(parcelle_operation_culturale.updateExtrants);
            $('#parcelle_operation_culturale_cultures').click(parcelle_operation_culturale.updateExtrants);
            $('#parcelle_operation_culturale_cultures').trigger("change");

            $('#parcelle_operation_culturale_type').change(parcelle_operation_culturale.updateIntrants);
            $('#parcelle_operation_culturale_type').trigger("change");
        },

        hideAll : function(){
            $(".form-group:has(.intrant)").hide();
            $(".form-group:has(.extrant)").hide();
        },

        updateExtrants : function(e){
            var elem_id , form, data, selected_cultures;
            if(e.target){
                elem_id = e.target.id;
            }else{
                elem_id = e.id;
            }
            // ... retrieve the corresponding form.
            form = $('#' + elem_id).closest('form');
            // Simulate form data, but only include the selected value.
            data = {};
            selected_cultures =[];
            $("#parcelle_operation_culturale_cultures input").each(function(){
                if($(this).is(':checked')) {
                    selected_cultures.push(parseInt($(this).val()));
                }
            });

            data["parcelle_operation_culturale"] = {
                "cultures": selected_cultures
            };
 
            // Submit data via AJAX to the form's action path.
            $.ajax({
                url : form.attr('action'),
                type : form.attr('method'),
                data : data,
                success : function (html) {
                    $('#parcelle_operation_culturale_produitPrimaire').replaceWith(
                       $(html).find('#parcelle_operation_culturale_produitPrimaire'));
                    $('#parcelle_operation_culturale_produitPrimaire').select2();
                }
            });
 
        },
        updateIntrants : function(e){
            var op_cultu = $(this);
            var form = $(this).closest('form');
            var data = {};
            data[op_cultu.attr('name')] = op_cultu.val();
 
            // Submit data via AJAX to the form's action path.
            $.ajax({
                url : form.attr('action'),
                type : form.attr('method'),
                data : data,
                success : function (html) {
                    $('#parcelle_operation_culturale_intrant').replaceWith(
                       $(html).find('#parcelle_operation_culturale_intrant'));
                    $('#parcelle_operation_culturale_intrant').select2();
                }
            });
        },
        hideShowIntrantExtrantForm : function(e){
            var valToDisplay;
            valToDisplay = parseInt(e.target.value, 10);

            if (avecExtrant.indexOf(valToDisplay) !== -1) { // valToDisplay se trouve dans extrants
                parcelle_operation_culturale.showExtrant(); //todo verif
            } else if (avecIntrant.indexOf(valToDisplay) !== - 1) {
                parcelle_operation_culturale.showIntrant(); //todo verif
            } else {
                parcelle_operation_culturale.hideIntrantExtrant(); //todo verif
            }
        },
        showIntrant : function(e) {
            parcelle_operation_culturale.hideAll();
            $(".form-group:has(.intrant)").show();
            $("select.intrant").each(function(iter, item){
                $(item).select2();
            });

		},
        showExtrant : function(e) {
            parcelle_operation_culturale.hideAll();
            $(".form-group:has(.extrant)").show();
            $("select.extrant").each(function(iter, item){
                $(item).select2();
            });
		},
        hideIntrantExtrant : function(){
            parcelle_operation_culturale.hideAll();
        },
}
$(document).on("ready", function () {
    parcelle_operation_culturale.onReady();
});