/**
 * javascript lié au formulaire get_localisation
 */
var exploitation_commande_semences = {
    onReady : function () {
        $('#exploitation_commande_semences_culture').change(exploitation_commande_semences.updVarieteSelectors);
    },

    updVarieteSelectors : function (e) {
        var myid, $form, data;
        myid = e.target.id;
        $form = $('#' + myid).closest('form');
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current groupement field ...
                $('#exploitation_commande_semences_variete').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#exploitation_commande_semences_variete'));
            }
        });
    }
}

$(document).on("ready", function () {
    exploitation_commande_semences.onReady();
});
