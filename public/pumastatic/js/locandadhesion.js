/**
 * javascript lié au formulaire get_localisation
 */
var locandadhesion = {
    onReady : function () {
        
        $('#locandadhesion_province').change(locandadhesion.updCommSelectors);
        $('#locandadhesion_commune').change(locandadhesion.updCollSelectors);
    },

    updCollSelectors : function (e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current exploitation field ...
                $('#locandadhesion_colline').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#locandadhesion_colline'));
            }
        });
    },

    updCommSelectors : function (e) {
        var myid, $form, data;
        myid = e.target.id;
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();    
        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                // Replace current groupement field ...
                $('#locandadhesion_commune').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#locandadhesion_commune'));
                $('#locandadhesion_commune').change(locandadhesion.updCollSelectors);
                locandadhesion.updCollSelectors($('#localisation_commune'));
				// activer boutton new exploitation
	//			localisation.updcollSelectors($('#newexpl'));
	//			$('#newexpl').attr('disabled',false);
            }
        });
    }
}

$(document).on("ready", function () {
    locandadhesion.onReady();
});




