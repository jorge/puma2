/**
 * javascript lié au formulaire get_localisation
 */
var groupadhesiontel = {
    onReady : function () {
        $('#groupadhesiontel_cooperative').change(
           groupadhesiontel.updateGroupementSelector);
        $('#groupadhesiontel_cooperative').trigger("click");    
    },

    updateGroupementSelector: function(e) {
        var myid, $form, data;
        if(e.target){
            myid = e.target.id;
        }else{
            myid = e.id;
        }
        // ... retrieve the corresponding form.
        $form = $('#' + myid).closest('form');
        // Simulate form data, but only include the selected value.
        data = {};
        data[$('#' + myid).attr('name')] = $('#' + myid).val();

        // Submit data via AJAX to the form's action path.
        $.ajax({
            url : $form.attr('action'),
            type : $form.attr('method'),
            data : data,
            success : function (html) {
                
                // Replace current exploitation field ...
                $('#groupadhesiontel_groupement').replaceWith(
                // ... with the returned one from the AJAX response.
                $(html).find('#groupadhesiontel_groupement'));
                $('#groupadhesiontel_groupement').select2();
            }
        });
    },

};

$(document).on("ready", function () {
    groupadhesiontel.onReady();
});
