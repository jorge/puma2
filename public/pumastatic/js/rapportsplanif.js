/**
 * javascript lié au formulaire consultation f0
 */
function afficherInput(numero){
	var res=prompt("definir l'année de la recherche","2015");
	
	var myan="&annee="+res;

	document.getElementById("l"+numero).href="plannum?num="+numero+myan
}


var rapports_planification = {
        onReady : function () {            
            rapports_planification.hideAll();
			$('#rapports_planification_exploitationGroupAnnee').click(rapports_planification.showDefault);
        },
        
        hideAll : function(){
            $(".form-group:has(.show-exploitation)").hide();
            return
        },
		showExploitation : function(){
            $(".form-group:has(.show-exploitation)").show();
            if ($('#rapports_planification_exploitationGroupAnnee:checked').val()){
                $('.form-group:has(#rapports_planification_exploitationListAnnee)').hide();
            }else {
                $('.form-group:has(#rapports_planification_exploitationListAnnee)').show();
            }
        },
        showDefault : function(){
            rapports_planification.hideAll();
            rapports_planification.showExploitation();
        },
}
$(document).on("ready", function () {
    rapports_planification.onReady();
});