/**
 * javascript lié au formulaire consultation f1
 */
var ConsultationF1 = {
        onReady : function () {            
            ConsultationF1.hideAll();
            $('#consultation_f1_queryTheme').text(ConsultationF1.queryTheme); 
        },
        
        hideAll : function(){
            $(".form-group:has(.show-terres)").hide();
            $(".form-group:has(.show-sommes)").hide();
            $(".form-group:has(.show-elevage)").hide();
            $(".form-group:has(.show-prevus)").hide();
            $(".form-group:has(.show-animaux)").hide();
            return
        },
        queryTheme : function(e){
            var valToDisplay;
            valToDisplay = $('#consultation_f1_queryTheme').val();
            ConsultationF1.hideAll();
            switch(valToDisplay) {
            	case 'troupeau':
                    ConsultationF1.animaux();
                    break;
                case 'parcelles':
                    ConsultationF1.parcelles();
                    break;
            } 
			
            return;
        },
        animaux : function(e) {
             $(".form-group:has(.show-animaux)").show();	
        },
        parcelles : function(e) {
             $(".form-group:has(.show-terres)").show();	
        },
        showDefault : function(){
            ConsultationF1.hideAll();
            ConsultationF1.showExploitation();
        },
}
$(document).on("ready", function () {
    ConsultationF1.onReady();
});