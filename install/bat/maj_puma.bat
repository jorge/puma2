@echo off
echo "                                                "
echo "                                                "
echo "    ----     Mise a jour de puma           ---  "
echo "                                                "
echo "                                                "
cd C:\puma\puma3
git pull origin prod
php C:\puma\puma3\bin/console doctrine:migrations:migrate --no-interaction
php C:\puma\puma3\bin/console puma:send-debug-info
pause