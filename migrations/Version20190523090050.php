<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190523090050 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE personnes CHANGE COLUMN date_naissance date_naissance_old VARCHAR(4);');
        $this->addSql('ALTER TABLE personnes ADD COLUMN date_naissance DATE DEFAULT NULL;');
        $this->addSql("UPDATE personnes SET date_naissance=STR_TO_DATE(CONCAT('1,1,',date_naissance_old),'%d,%m,%Y');");
        $this->addSql('ALTER TABLE personnes DROP COLUMN date_naissance_old;');
    }
}
