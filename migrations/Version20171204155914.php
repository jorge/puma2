<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171204155914 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE parcelle_phytosanitaire ADD historique_culture_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', ADD quantite_mofam SMALLINT DEFAULT NULL, ADD quantite_mosal SMALLINT DEFAULT NULL, ADD quantite_utilisee SMALLINT DEFAULT NULL, DROP quantite;");
        $this->addSql('ALTER TABLE parcelle_phytosanitaire ADD CONSTRAINT FK_1C09AB53C95C9E74 FOREIGN KEY (historique_culture_id) REFERENCES historique_cultures (id);');
        $this->addSql('CREATE INDEX IDX_1C09AB53C95C9E74 ON parcelle_phytosanitaire (historique_culture_id);');

        $this->addSql("ALTER TABLE parcelle_amendements ADD historique_culture_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', ADD quantite_mofam SMALLINT DEFAULT NULL, ADD quantite_mosal SMALLINT DEFAULT NULL, ADD quantite_utilisee SMALLINT DEFAULT NULL, DROP quantite;");
        $this->addSql('ALTER TABLE parcelle_amendements ADD CONSTRAINT FK_C328AFE4C95C9E74 FOREIGN KEY (historique_culture_id) REFERENCES historique_cultures (id);');
        $this->addSql('CREATE INDEX IDX_C328AFE4C95C9E74 ON parcelle_amendements (historique_culture_id);');

        $this->addSql('ALTER TABLE parcelle_semences DROP FOREIGN KEY FK_C47CB41A906F1B44;');
        $this->addSql('DROP INDEX IDX_C47CB41A906F1B44 ON parcelle_semences;');
        $this->addSql("ALTER TABLE parcelle_semences ADD quantite_mofam SMALLINT DEFAULT NULL, ADD quantite_mosal SMALLINT DEFAULT NULL, ADD quantite_achetee SMALLINT DEFAULT NULL, ADD quantite_non_achetee SMALLINT DEFAULT NULL, DROP quantite_mo, DROP quantite, CHANGE mo_type_id unite_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)';");
        $this->addSql('ALTER TABLE parcelle_semences ADD CONSTRAINT FK_C47CB41AEC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');
        $this->addSql('CREATE INDEX IDX_C47CB41AEC4A74AB ON parcelle_semences (unite_id);');

        $this->addSql("ALTER TABLE parcelle_herbicide ADD historique_culture_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', ADD quantite_mofam SMALLINT DEFAULT NULL, ADD quantite_mosal SMALLINT DEFAULT NULL, ADD quantite_utilisee SMALLINT DEFAULT NULL, DROP quantite;");
        $this->addSql('ALTER TABLE parcelle_herbicide ADD CONSTRAINT FK_EC35E45C95C9E74 FOREIGN KEY (historique_culture_id) REFERENCES historique_cultures (id);');
        $this->addSql('CREATE INDEX IDX_EC35E45C95C9E74 ON parcelle_herbicide (historique_culture_id);');

        $this->addSql("ALTER TABLE parcelle_fumure_org ADD historique_culture_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', ADD quantite_mofam SMALLINT DEFAULT NULL, ADD quantite_mosal SMALLINT DEFAULT NULL, ADD quantite_achetee SMALLINT DEFAULT NULL, ADD quantite_non_achetee SMALLINT DEFAULT NULL, DROP quantite, DROP adquisition;");
        $this->addSql('ALTER TABLE parcelle_fumure_org ADD CONSTRAINT FK_884055C0C95C9E74 FOREIGN KEY (historique_culture_id) REFERENCES historique_cultures (id);');
        $this->addSql('CREATE INDEX IDX_884055C0C95C9E74 ON parcelle_fumure_org (historique_culture_id);');

        $this->addSql('ALTER TABLE parcelle_recoltes DROP FOREIGN KEY FK_CB0CB02B906F1B44;');
        $this->addSql('ALTER TABLE parcelle_recoltes DROP FOREIGN KEY FK_CB0CB02BF347EFB;');
        $this->addSql('ALTER TABLE parcelle_recoltes DROP FOREIGN KEY FK_CB0CB02BF4A8104B;');
        $this->addSql('DROP INDEX IDX_CB0CB02BF347EFB ON parcelle_recoltes;');
        $this->addSql('DROP INDEX IDX_CB0CB02B906F1B44 ON parcelle_recoltes;');
        $this->addSql('DROP INDEX IDX_CB0CB02BF4A8104B ON parcelle_recoltes;');
        $this->addSql("ALTER TABLE parcelle_recoltes ADD produit_primaire_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', ADD produit_secondaire_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', ADD quantite_mofam SMALLINT DEFAULT NULL, ADD quantite_mosal SMALLINT DEFAULT NULL, ADD quantite_recoltee_primaire SMALLINT DEFAULT NULL, ADD quantite_recoltee_secondaire SMALLINT DEFAULT NULL, DROP mo_type_id, DROP produit_id, DROP mo_unite_id, DROP type_produit, DROP quantite, DROP quantite_mo;");
        $this->addSql('ALTER TABLE parcelle_recoltes ADD CONSTRAINT FK_CB0CB02BB6FFC1D4 FOREIGN KEY (produit_primaire_id) REFERENCES tbl_produits (id);');
        $this->addSql('ALTER TABLE parcelle_recoltes ADD CONSTRAINT FK_CB0CB02BABEF01C2 FOREIGN KEY (produit_secondaire_id) REFERENCES tbl_produits (id);');
        $this->addSql('CREATE INDEX IDX_CB0CB02BB6FFC1D4 ON parcelle_recoltes (produit_primaire_id);');
        $this->addSql('CREATE INDEX IDX_CB0CB02BABEF01C2 ON parcelle_recoltes (produit_secondaire_id);');

        $this->addSql("ALTER TABLE parcelle_fumure_min ADD historique_culture_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', ADD quantite_mofam SMALLINT DEFAULT NULL, ADD quantite_mosal SMALLINT DEFAULT NULL, ADD quantite_utilisee SMALLINT DEFAULT NULL, DROP quantite;");
        $this->addSql('ALTER TABLE parcelle_fumure_min ADD CONSTRAINT FK_5B2EF290C95C9E74 FOREIGN KEY (historique_culture_id) REFERENCES historique_cultures (id);');
        $this->addSql('CREATE INDEX IDX_5B2EF290C95C9E74 ON parcelle_fumure_min (historique_culture_id);');

        $this->addSql('ALTER TABLE parcelle_techniques DROP FOREIGN KEY FK_EBB928F9F4A8104B;');
        $this->addSql('DROP INDEX IDX_EBB928F9F4A8104B ON parcelle_techniques;');
        $this->addSql("ALTER TABLE parcelle_techniques CHANGE quantite_mofam quantite_mofam SMALLINT DEFAULT NULL, CHANGE quantite_mosal quantite_mosal SMALLINT DEFAULT NULL, DROP mo_unite_id, ADD unite_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)';");
        $this->addSql('ALTER TABLE parcelle_techniques ADD CONSTRAINT FK_EBB928F9EC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');
        $this->addSql('CREATE INDEX IDX_EBB928F9EC4A74AB ON parcelle_techniques (unite_id);');
    }
}
