<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Entity for storing the syncho status.
 */
class Version20170503122200 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE  IF EXISTS synchro_status;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE IF NOT EXISTS synchro_status (
                id INT AUTO_INCREMENT NOT NULL,
                localLastUdate DATETIME DEFAULT NOW() ON UPDATE NOW(),
                masterLastUdate DATETIME NOT NULL,
                PRIMARY KEY(id));');

        $this->addSql('INSERT INTO synchro_status SET masterLastUdate = NOW();'); //True de base
    }
}
