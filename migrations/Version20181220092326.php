<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181220092326 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        // $this->addSql("RENAME TABLE tbl_animaux_technique TO tbl_animaux_mouvements;");
        $this->addSql('DELETE FROM puma_03.animaux_structure WHERE animaux_technique_id=2;');
        $this->addSql('DELETE FROM tbl_animaux_mouvements WHERE id=2;');
        $this->addSql('ALTER TABLE animaux_structure CHANGE males jeunes_males int(11);');
        $this->addSql('ALTER TABLE animaux_structure CHANGE femelles jeunes_femelles int(11);');
        $this->addSql('ALTER TABLE animaux_structure CHANGE jeunes jeunes_immatures int(11);');
        $this->addSql('ALTER TABLE tbl_animaux_mouvements ADD COLUMN ajout TinyInt(1) DEFAULT 0;');
        $this->addSql('UPDATE tbl_animaux_mouvements SET ajout=1 WHERE id IN (1,6,7,8,9);');
        $this->addSql('ALTER TABLE animaux_structure ADD COLUMN date_execution DATETIME NULL DEFAULT NULL AFTER id;');
    }
}
