<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200115115257 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_operation_culturale_type DROP COLUMN agro_eco');
    }

    public function getDescription(): string
    {
        return 'Add column agro_eco to the table tbl_operation_culturale_type';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE tbl_operation_culturale_type ADD COLUMN agro_eco TINYINT(1) DEFAULT '0' NOT NULL");
    }
}
