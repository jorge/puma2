<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190426133106 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE formations_recu_personnes (
            formations_recu_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            personnes_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            INDEX IDX_63C74EBC72CE06BF (formations_recu_id),
            INDEX IDX_63C74EBC146AD7BC (personnes_id),
            PRIMARY KEY(formations_recu_id, personnes_id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
        $this->addSql('ALTER TABLE formations_recu_personnes
            ADD CONSTRAINT FK_63C74EBC72CE06BF
            FOREIGN KEY (formations_recu_id) REFERENCES formations_recu (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE formations_recu_personnes
            ADD CONSTRAINT FK_63C74EBC146AD7BC
            FOREIGN KEY (personnes_id) REFERENCES personnes (id) ON DELETE CASCADE;');
        $this->addSql(' ALTER TABLE formations_recu DROP beneficiaire;');
    }
}
