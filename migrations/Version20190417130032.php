<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190417130032 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('UPDATE collecte SET fiche_type = 0 WHERE fiche_type IS NULL');
        $this->addSql("ALTER TABLE collecte CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL, CHANGE fiche_type fiche_type SMALLINT NOT NULL;");
        // $this->addSql("ALTER TABLE collecte ADD CONSTRAINT FK_55AE4A3D89D40298 FOREIGN KEY (exercice_id) REFERENCES exercice (id);"); // pbm latin1 and utf8
        $this->addSql('CREATE UNIQUE INDEX exercice_type_uniq ON collecte (exercice_id, fiche_type);');
        // $this->addSql("ALTER TABLE exercice ADD CONSTRAINT FK_E418C74DD967A16D FOREIGN KEY (exploitation_id) REFERENCES exploitations (id);"); // idem pbm latin1 and utf8
    }
}
