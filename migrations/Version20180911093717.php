<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180911093717 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        /* tbl_achat_agricoles */
        $this->addSql('ALTER TABLE exploitation_charges DROP FOREIGN KEY FK_65FD526CFE95D117;');

        $this->addSql('ALTER TABLE tbl_achat_agricoles CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_achat_agricoles DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_achat_agricoles ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_achat_agricoles MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE exploitation_charges ADD COLUMN old_achat_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE exploitation_charges SET old_achat_id=achat_id;');
        $this->addSql('UPDATE exploitation_charges SET achat_id=(SELECT tbl_achat_agricoles.id FROM tbl_achat_agricoles WHERE tbl_achat_agricoles.old_id=exploitation_charges.old_achat_id);');
        $this->addSql('ALTER TABLE exploitation_charges MODIFY achat_id int(10);');
        $this->addSql('ALTER TABLE exploitation_charges ADD FOREIGN KEY (achat_id) REFERENCES tbl_achat_agricoles(id);');

        //* tbl_aleas / // pas de tables relationnés

        $this->addSql('ALTER TABLE tbl_aleas CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_aleas DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_aleas ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_aleas MODIFY old_id CHAR(36) DEFAULT NULL;');

        //* tbl_alimentation /
        $this->addSql('ALTER TABLE animaux_alimentation DROP FOREIGN KEY FK_645AD6148441D4D9;');

        $this->addSql('ALTER TABLE tbl_alimentation CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_alimentation DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_alimentation ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_alimentation MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE animaux_alimentation ADD COLUMN old_alimentation_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE animaux_alimentation SET old_alimentation_id=alimentation_id;');
        $this->addSql('UPDATE animaux_alimentation SET alimentation_id=(SELECT tbl_alimentation.id FROM tbl_alimentation WHERE tbl_alimentation.old_id=animaux_alimentation.old_alimentation_id);');
        $this->addSql('ALTER TABLE animaux_alimentation MODIFY alimentation_id int(10);');
        $this->addSql('ALTER TABLE animaux_alimentation ADD FOREIGN KEY (alimentation_id) REFERENCES tbl_alimentation(id);');

        // tbl_amendements

        $this->addSql('ALTER TABLE parcelle_amendements DROP FOREIGN KEY FK_C328AFE4F4C20978;');
        $this->addSql('ALTER TABLE tbl_amendements CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_amendements DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_amendements ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_amendements MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_amendements ADD COLUMN old_amendement_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelle_amendements SET old_amendement_id=amendement_id;');
        $this->addSql('UPDATE parcelle_amendements SET amendement_id=(SELECT tbl_amendements.id FROM tbl_amendements WHERE tbl_amendements.old_id=parcelle_amendements.old_amendement_id);');
        $this->addSql('ALTER TABLE parcelle_amendements MODIFY amendement_id int(10);');
        $this->addSql('ALTER TABLE parcelle_amendements ADD FOREIGN KEY (amendement_id) REFERENCES tbl_alimentation(id);');
        //tbl_animaux_production
        $this->addSql('ALTER TABLE animaux_production DROP FOREIGN KEY FK_48270C64ECC6147F;');
        $this->addSql('ALTER TABLE tbl_animaux_production CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_animaux_production DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_animaux_production ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_animaux_production MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE animaux_production ADD COLUMN old_production_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE animaux_production SET old_production_id=production_id;');
        $this->addSql('UPDATE animaux_production SET production_id=(SELECT tbl_animaux_production.id FROM tbl_animaux_production WHERE tbl_animaux_production.old_id=animaux_production.old_production_id);');
        $this->addSql('ALTER TABLE animaux_production MODIFY production_id int(10);');
        $this->addSql('ALTER TABLE animaux_production ADD FOREIGN KEY (production_id) REFERENCES tbl_animaux_production(id);');

        //tbl_animaux_technique
        $this->addSql('ALTER TABLE animaux_structure DROP FOREIGN KEY FK_C17E032AF5B5C292;');
        $this->addSql('ALTER TABLE tbl_animaux_technique CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_animaux_technique DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_animaux_technique ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_animaux_technique MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE animaux_structure ADD COLUMN old_animaux_technique_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE animaux_structure SET old_animaux_technique_id=animaux_technique_id;');
        $this->addSql('UPDATE animaux_structure SET animaux_technique_id=(SELECT tbl_animaux_technique.id FROM tbl_animaux_technique WHERE tbl_animaux_technique.old_id=animaux_structure.old_animaux_technique_id);');
        $this->addSql('ALTER TABLE animaux_structure MODIFY animaux_technique_id int(10);');
        $this->addSql('ALTER TABLE animaux_structure ADD FOREIGN KEY (animaux_technique_id) REFERENCES tbl_animaux_technique(id);');

        //tbl_antierosives
        $this->addSql('ALTER TABLE antierosives DROP FOREIGN KEY FK_A61A12699F2BF6FC;');
        $this->addSql('ALTER TABLE tbl_antierosives CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_antierosives DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_antierosives ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_antierosives MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE antierosives ADD COLUMN old_type_antierosive_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE antierosives SET old_type_antierosive_id=type_antierosive_id;');
        $this->addSql('UPDATE antierosives SET type_antierosive_id=(SELECT tbl_antierosives.id FROM tbl_antierosives WHERE tbl_antierosives.old_id=antierosives.old_type_antierosive_id);');
        $this->addSql('ALTER TABLE antierosives MODIFY type_antierosive_id int(10);');
        $this->addSql('ALTER TABLE antierosives ADD FOREIGN KEY (type_antierosive_id) REFERENCES tbl_antierosives(id);');

        //tbl_credit_objet
        $this->addSql('ALTER TABLE dettes DROP FOREIGN KEY FK_15565CF1BBEC4908;');
        $this->addSql('ALTER TABLE tbl_credit_objet CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_credit_objet DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_credit_objet ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_credit_objet MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE dettes ADD COLUMN old_creditobjet_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE dettes SET old_creditobjet_id=creditobjet_id;');
        $this->addSql('UPDATE dettes SET creditobjet_id=(SELECT tbl_credit_objet.id FROM tbl_credit_objet WHERE tbl_credit_objet.old_id=dettes.old_creditobjet_id);');
        $this->addSql('ALTER TABLE dettes MODIFY creditobjet_id int(10);');
        $this->addSql('ALTER TABLE dettes ADD FOREIGN KEY (creditobjet_id) REFERENCES tbl_credit_objet(id);');

        //tbl_decoupage_admin_level
        $this->addSql('ALTER TABLE tbl_decoupage_admin DROP FOREIGN KEY FK_A24CB6235FB14BA7;');
        $this->addSql('ALTER TABLE tbl_decoupage_admin_level CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_decoupage_admin_level DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_decoupage_admin_level ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_decoupage_admin_level MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE tbl_decoupage_admin ADD COLUMN old_level_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE tbl_decoupage_admin SET old_level_id=level_id;');
        $this->addSql('UPDATE tbl_decoupage_admin SET level_id=(SELECT tbl_decoupage_admin_level.id FROM tbl_decoupage_admin_level WHERE tbl_decoupage_admin_level.old_id=tbl_decoupage_admin.old_level_id);');
        $this->addSql('ALTER TABLE tbl_decoupage_admin MODIFY level_id int(10);');
        $this->addSql('ALTER TABLE tbl_decoupage_admin ADD FOREIGN KEY (level_id) REFERENCES tbl_decoupage_admin_level(id);');
        //tbl_engrais
        $this->addSql('ALTER TABLE engrais_demande DROP FOREIGN KEY FK_18A591A8563C4F47;');

        $this->addSql('ALTER TABLE tbl_engrais CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_engrais DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_engrais ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_engrais MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE engrais_demande ADD COLUMN old_engrais_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE engrais_demande SET old_engrais_id=engrais_id;');
        $this->addSql('UPDATE engrais_demande SET engrais_id=(SELECT tbl_engrais.id FROM tbl_engrais WHERE tbl_engrais.old_id=engrais_demande.old_engrais_id);');
        $this->addSql('ALTER TABLE engrais_demande MODIFY engrais_id int(10);');
        $this->addSql('ALTER TABLE engrais_demande ADD FOREIGN KEY (engrais_id) REFERENCES tbl_engrais(id);');

        //tbl_entretiens (pas de table lié)
        $this->addSql('ALTER TABLE tbl_entretiens CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_entretiens DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_entretiens ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_entretiens MODIFY old_id CHAR(36) DEFAULT NULL;');

        //tbl_exposition
        $this->addSql('ALTER TABLE parcelles DROP FOREIGN KEY FK_4F15F60E88ED476F;');
        $this->addSql('ALTER TABLE tbl_exposition CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_exposition DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_exposition ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_exposition MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelles ADD COLUMN old_exposition_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelles SET old_exposition_id=exposition_id;');
        $this->addSql('UPDATE parcelles SET exposition_id=(SELECT tbl_exposition.id FROM tbl_exposition WHERE tbl_exposition.old_id=parcelles.old_exposition_id);');
        $this->addSql('ALTER TABLE parcelles MODIFY exposition_id int(10);');
        $this->addSql('ALTER TABLE parcelles ADD FOREIGN KEY (exposition_id) REFERENCES tbl_exposition(id);');

        //tbl_fumure_min
        $this->addSql('ALTER TABLE parcelle_fumure_min DROP FOREIGN KEY FK_5B2EF2908AA46A9F;');
        $this->addSql('ALTER TABLE tbl_fumure_min CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_fumure_min DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_fumure_min ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_fumure_min MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_fumure_min ADD COLUMN old_fumure_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelle_fumure_min SET old_fumure_id=fumure_id;');
        $this->addSql('UPDATE parcelle_fumure_min SET fumure_id=(SELECT tbl_fumure_min.id FROM tbl_fumure_min WHERE tbl_fumure_min.old_id=parcelle_fumure_min.old_fumure_id);');
        $this->addSql('ALTER TABLE parcelle_fumure_min MODIFY fumure_id int(10);');
        $this->addSql('ALTER TABLE parcelle_fumure_min ADD FOREIGN KEY (fumure_id) REFERENCES tbl_fumure_min(id);');

        //tbl_fumure_org
        $this->addSql('ALTER TABLE parcelle_fumure_org DROP FOREIGN KEY FK_884055C08AA46A9F;');
        $this->addSql('ALTER TABLE tbl_fumure_org CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_fumure_org DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_fumure_org ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_fumure_org MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_fumure_org ADD COLUMN old_fumure_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelle_fumure_org SET old_fumure_id=fumure_id;');
        $this->addSql('UPDATE parcelle_fumure_org SET fumure_id=(SELECT tbl_fumure_org.id FROM tbl_fumure_org WHERE tbl_fumure_org.old_id=parcelle_fumure_org.old_fumure_id);');
        $this->addSql('ALTER TABLE parcelle_fumure_org MODIFY fumure_id int(10);');
        $this->addSql('ALTER TABLE parcelle_fumure_org ADD FOREIGN KEY (fumure_id) REFERENCES tbl_fumure_org(id);');

        //tbl_labour_animaux
        $this->addSql('ALTER TABLE animaux_labour DROP FOREIGN KEY FK_D17D0995CC903DF2;');
        $this->addSql('ALTER TABLE tbl_labour_animaux CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_labour_animaux DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_labour_animaux ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_labour_animaux MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE animaux_labour ADD COLUMN old_labour_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE animaux_labour SET old_labour_id=labour_id;');
        $this->addSql('UPDATE animaux_labour SET labour_id=(SELECT tbl_labour_animaux.id FROM tbl_labour_animaux WHERE tbl_labour_animaux.old_id=animaux_labour.old_labour_id);');
        $this->addSql('ALTER TABLE animaux_labour MODIFY labour_id int(10);');
        $this->addSql('ALTER TABLE animaux_labour ADD FOREIGN KEY (labour_id) REFERENCES tbl_labour_animaux(id);');

        //tbl_mo_type
        $this->addSql('ALTER TABLE mo_toutes_applis DROP FOREIGN KEY FK_67C8D6E0906F1B44;');
        $this->addSql('ALTER TABLE tbl_mo_type CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_mo_type DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_mo_type ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_mo_type MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE mo_toutes_applis ADD COLUMN old_mo_type_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE mo_toutes_applis SET old_mo_type_id=mo_type_id;');
        $this->addSql('UPDATE mo_toutes_applis SET mo_type_id=(SELECT tbl_mo_type.id FROM tbl_mo_type WHERE tbl_mo_type.old_id=mo_toutes_applis.old_mo_type_id);');
        $this->addSql('ALTER TABLE mo_toutes_applis MODIFY mo_type_id int(10);');
        $this->addSql('ALTER TABLE mo_toutes_applis ADD FOREIGN KEY (mo_type_id) REFERENCES tbl_mo_type(id);');

        //tbl_mode_culture
        $this->addSql('ALTER TABLE parcelle_historiques DROP FOREIGN KEY FK_F6AA485374D5C10E;');
        $this->addSql('ALTER TABLE tbl_mode_culture CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_mode_culture DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_mode_culture ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_mode_culture MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_historiques ADD COLUMN old_mode_culture_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelle_historiques SET old_mode_culture_id=mode_culture_id;');
        $this->addSql('UPDATE parcelle_historiques SET mode_culture_id=(SELECT tbl_mode_culture.id FROM tbl_mode_culture WHERE tbl_mode_culture.old_id=parcelle_historiques.old_mode_culture_id);');
        $this->addSql('ALTER TABLE parcelle_historiques MODIFY mode_culture_id int(10);');
        $this->addSql('ALTER TABLE parcelle_historiques ADD FOREIGN KEY (mode_culture_id) REFERENCES tbl_mode_culture(id);');

        //tbl_organisation_level
        $this->addSql('ALTER TABLE tbl_organisations DROP FOREIGN KEY FK_CE763CA75FB14BA7;');
        $this->addSql('ALTER TABLE tbl_organisation_level CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_organisation_level DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_organisation_level ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_organisation_level MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE tbl_organisations ADD COLUMN old_level_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE tbl_organisations SET old_level_id=level_id;');
        $this->addSql('UPDATE tbl_organisations SET level_id=(SELECT tbl_organisation_level.id FROM tbl_organisation_level WHERE tbl_organisation_level.old_id=tbl_organisations.old_level_id);');
        $this->addSql('ALTER TABLE tbl_organisations MODIFY level_id int(10);');
        $this->addSql('ALTER TABLE tbl_organisations ADD FOREIGN KEY (level_id) REFERENCES tbl_organisation_level(id);');

        //tbl_pente
        $this->addSql('ALTER TABLE parcelles DROP FOREIGN KEY FK_4F15F60EBBA81E8D;');
        $this->addSql('ALTER TABLE tbl_pente CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_pente DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_pente ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_pente MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelles ADD COLUMN old_pente_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelles SET old_pente_id=pente_id;');
        $this->addSql('UPDATE parcelles SET pente_id=(SELECT tbl_pente.id FROM tbl_pente WHERE tbl_pente.old_id=parcelles.old_pente_id);');
        $this->addSql('ALTER TABLE parcelles MODIFY pente_id int(10);');
        $this->addSql('ALTER TABLE parcelles ADD FOREIGN KEY (pente_id) REFERENCES tbl_pente(id);');

        //tbl_produit_phyto
        $this->addSql('ALTER TABLE parcelle_phytosanitaire DROP FOREIGN KEY FK_1C09AB538CCDE425;');
        $this->addSql('ALTER TABLE phyto_demande DROP FOREIGN KEY FK_5537CEA08CCDE425;');

        $this->addSql('ALTER TABLE tbl_produit_phyto CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_produit_phyto DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_produit_phyto ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_produit_phyto MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE parcelle_phytosanitaire ADD COLUMN old_phytosanitaire_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelle_phytosanitaire SET old_phytosanitaire_id=phytosanitaire_id;');
        $this->addSql('UPDATE parcelle_phytosanitaire SET phytosanitaire_id=(SELECT tbl_produit_phyto.id FROM tbl_produit_phyto WHERE tbl_produit_phyto.old_id=parcelle_phytosanitaire.old_phytosanitaire_id);');
        $this->addSql('ALTER TABLE parcelle_phytosanitaire MODIFY phytosanitaire_id int(10);');
        $this->addSql('ALTER TABLE parcelle_phytosanitaire ADD FOREIGN KEY (phytosanitaire_id) REFERENCES tbl_produit_phyto(id);');

        $this->addSql('ALTER TABLE phyto_demande ADD COLUMN old_phytosanitaire_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE phyto_demande SET old_phytosanitaire_id=phytosanitaire_id;');
        $this->addSql('UPDATE phyto_demande SET phytosanitaire_id=(SELECT tbl_produit_phyto.id FROM tbl_produit_phyto WHERE tbl_produit_phyto.old_id=phyto_demande.old_phytosanitaire_id);');
        $this->addSql('ALTER TABLE phyto_demande MODIFY phytosanitaire_id int(10);');
        $this->addSql('ALTER TABLE phyto_demande ADD FOREIGN KEY (phytosanitaire_id) REFERENCES tbl_produit_phyto(id);');

        //tbl_propriete
        $this->addSql('ALTER TABLE parcelles DROP FOREIGN KEY FK_4F15F60E18566CAF;');
        $this->addSql('ALTER TABLE tbl_propriete CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_propriete DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_propriete ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_propriete MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelles ADD COLUMN old_propriete_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelles SET old_propriete_id=propriete_id;');
        $this->addSql('UPDATE parcelles SET propriete_id=(SELECT tbl_propriete.id FROM tbl_propriete WHERE tbl_propriete.old_id=parcelles.old_propriete_id);');
        $this->addSql('ALTER TABLE parcelles MODIFY propriete_id int(10);');
        $this->addSql('ALTER TABLE parcelles ADD FOREIGN KEY (propriete_id) REFERENCES tbl_propriete(id);');

        //tbl_provinces_communes (pas des relations avec des autres tables)
        $this->addSql('ALTER TABLE tbl_provinces_communes CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_provinces_communes DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_provinces_communes ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_provinces_communes MODIFY old_id CHAR(36) DEFAULT NULL;');

        //tbl_semences
        $this->addSql('ALTER TABLE semence_demandee DROP FOREIGN KEY FK_FD4549D63F43C8A;');
        $this->addSql('ALTER TABLE tbl_semences CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_semences DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_semences ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_semences MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE semence_demandee ADD COLUMN old_semence_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE semence_demandee SET old_semence_id=semence_id;');
        $this->addSql('UPDATE semence_demandee SET semence_id=(SELECT tbl_semences.id FROM tbl_semences WHERE tbl_semences.old_id=semence_demandee.old_semence_id);');
        $this->addSql('ALTER TABLE semence_demandee MODIFY semence_id int(10);');
        $this->addSql('ALTER TABLE semence_demandee ADD FOREIGN KEY (semence_id) REFERENCES tbl_semences(id);');

        //tbl_situation
        $this->addSql('ALTER TABLE parcelles DROP FOREIGN KEY FK_4F15F60E3408E8AF;');
        $this->addSql('ALTER TABLE tbl_situation CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_situation DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_situation ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_situation MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelles ADD COLUMN old_situation_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelles SET old_situation_id=situation_id;');
        $this->addSql('UPDATE parcelles SET situation_id=(SELECT tbl_situation.id FROM tbl_situation WHERE tbl_situation.old_id=parcelles.old_situation_id);');
        $this->addSql('ALTER TABLE parcelles MODIFY situation_id int(10);');
        $this->addSql('ALTER TABLE parcelles ADD FOREIGN KEY (situation_id) REFERENCES tbl_situation(id);');

        //tbl_terre_protection (non tables liées)
        $this->addSql('ALTER TABLE tbl_terre_protection CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_terre_protection DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_terre_protection ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_terre_protection MODIFY old_id CHAR(36) DEFAULT NULL;');

        //tbl_traitements
        $this->addSql('ALTER TABLE mo_toutes_applis DROP FOREIGN KEY FK_67C8D6E0DDA344B6;');
        $this->addSql('ALTER TABLE tbl_traitements CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_traitements DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_traitements ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_traitements MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE mo_toutes_applis ADD COLUMN old_traitement_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE mo_toutes_applis SET old_traitement_id=traitement_id;');
        $this->addSql('UPDATE mo_toutes_applis SET traitement_id=(SELECT tbl_traitements.id FROM tbl_traitements WHERE tbl_traitements.old_id=mo_toutes_applis.old_traitement_id);');
        $this->addSql('ALTER TABLE mo_toutes_applis MODIFY traitement_id int(10);');
        $this->addSql('ALTER TABLE mo_toutes_applis ADD FOREIGN KEY (traitement_id) REFERENCES tbl_traitements(id);');

        //tbl_travail_type
        $this->addSql('ALTER TABLE mo_familiale_externe DROP FOREIGN KEY FK_4DC62DE2F3FDA88C;');
        $this->addSql('ALTER TABLE mo_familiale_interne DROP FOREIGN KEY FK_5FE9A610F3FDA88C;');
        $this->addSql('ALTER TABLE parcelle_techniques DROP FOREIGN KEY FK_EBB928F99A394A7A;');

        $this->addSql('ALTER TABLE tbl_travail_type CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_travail_type DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_travail_type ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_travail_type MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE mo_familiale_externe ADD COLUMN old_type_travail_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE mo_familiale_externe SET old_type_travail_id=type_travail_id;');
        $this->addSql('UPDATE mo_familiale_externe SET type_travail_id=(SELECT tbl_travail_type.id FROM tbl_travail_type WHERE tbl_travail_type.old_id=mo_familiale_externe.old_type_travail_id);');
        $this->addSql('ALTER TABLE mo_familiale_externe MODIFY type_travail_id int(10);');
        $this->addSql('ALTER TABLE mo_familiale_externe ADD FOREIGN KEY (type_travail_id) REFERENCES tbl_travail_type(id);');

        $this->addSql('ALTER TABLE mo_familiale_interne ADD COLUMN old_type_travail_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE mo_familiale_interne SET old_type_travail_id=type_travail_id;');
        $this->addSql('UPDATE mo_familiale_interne SET type_travail_id=(SELECT tbl_travail_type.id FROM tbl_travail_type WHERE tbl_travail_type.old_id=mo_familiale_interne.old_type_travail_id);');
        $this->addSql('ALTER TABLE mo_familiale_interne MODIFY type_travail_id int(10);');
        $this->addSql('ALTER TABLE mo_familiale_interne ADD FOREIGN KEY (type_travail_id) REFERENCES tbl_travail_type(id);');

        $this->addSql('ALTER TABLE parcelle_techniques ADD COLUMN old_travail_type_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE parcelle_techniques SET old_travail_type_id=travail_type_id;');
        $this->addSql('UPDATE parcelle_techniques SET travail_type_id=(SELECT tbl_travail_type.id FROM tbl_travail_type WHERE tbl_travail_type.old_id=parcelle_techniques.old_travail_type_id);');
        $this->addSql('ALTER TABLE parcelle_techniques MODIFY travail_type_id int(10);');
        $this->addSql('ALTER TABLE parcelle_techniques ADD FOREIGN KEY (travail_type_id) REFERENCES tbl_travail_type(id);');

        //tbl_type_commercialisation
        $this->addSql('ALTER TABLE commercialisation_produits DROP FOREIGN KEY FK_C8BFE3217F44101E;');
        $this->addSql('ALTER TABLE tbl_type_commercialisation CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_type_commercialisation DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_type_commercialisation ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_type_commercialisation MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE commercialisation_produits ADD COLUMN old_type_commercialisation_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE commercialisation_produits SET old_type_commercialisation_id=type_commercialisation_id;');
        $this->addSql('UPDATE commercialisation_produits SET type_commercialisation_id=(SELECT tbl_type_commercialisation.id FROM tbl_type_commercialisation WHERE tbl_type_commercialisation.old_id=commercialisation_produits.old_type_commercialisation_id);');
        $this->addSql('ALTER TABLE commercialisation_produits MODIFY type_commercialisation_id int(10);');
        $this->addSql('ALTER TABLE commercialisation_produits ADD FOREIGN KEY (type_commercialisation_id) REFERENCES tbl_type_commercialisation(id);');

        //tbl_type_creancier
        $this->addSql('ALTER TABLE dettes DROP FOREIGN KEY FK_15565CF120E24518;');
        $this->addSql('ALTER TABLE tbl_type_creancier CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_type_creancier DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_type_creancier ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_type_creancier MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE dettes ADD COLUMN old_type_creancier_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE dettes SET old_type_creancier_id=type_creancier_id;');
        $this->addSql('UPDATE dettes SET type_creancier_id=(SELECT tbl_type_creancier.id FROM tbl_type_creancier WHERE tbl_type_creancier.old_id=dettes.old_type_creancier_id);');
        $this->addSql('ALTER TABLE dettes MODIFY type_creancier_id int(10);');
        $this->addSql('ALTER TABLE dettes ADD FOREIGN KEY (type_creancier_id) REFERENCES tbl_type_creancier(id);');

        //tbl_type_depense
        $this->addSql('ALTER TABLE depenses_menage DROP FOREIGN KEY FK_83B040F55CDBC346;');
        $this->addSql('ALTER TABLE tbl_type_depense CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_type_depense DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_type_depense ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_type_depense MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE depenses_menage ADD COLUMN old_type_depense_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE depenses_menage SET old_type_depense_id=type_depense_id;');
        $this->addSql('UPDATE depenses_menage SET type_depense_id=(SELECT tbl_type_depense.id FROM tbl_type_depense WHERE tbl_type_depense.old_id=depenses_menage.old_type_depense_id);');
        $this->addSql('ALTER TABLE depenses_menage MODIFY type_depense_id int(10);');
        $this->addSql('ALTER TABLE depenses_menage ADD FOREIGN KEY (type_depense_id) REFERENCES tbl_type_depense(id);');

        //tbl_type_travail (pas de tables relationnés trouvés)
        $this->addSql('ALTER TABLE tbl_type_travail CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_type_travail DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_type_travail ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_type_travail MODIFY old_id CHAR(36) DEFAULT NULL;');

        //tbl_unite_mo
        $this->addSql('ALTER TABLE tbl_unite_mo CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_unite_mo DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_unite_mo ADD COLUMN id INT(10) NOT NULL AUTO_INCREMENT PRIMARY KEY FIRST;');
        $this->addSql('ALTER TABLE tbl_unite_mo MODIFY old_id CHAR(36) DEFAULT NULL;');
    }
}
