<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190424125937 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelle_operation_culturale
            ADD quantite_recolte_autoconsommation DOUBLE PRECISION DEFAULT NULL,
            ADD quantite_utilise_achete DOUBLE PRECISION DEFAULT NULL,
            ADD quantite_utilisee_autoconsommation DOUBLE PRECISION DEFAULT NULL,
            ADD quantite_recoltee_vente DOUBLE PRECISION DEFAULT NULL,
            DROP produitInterne, DROP produitExterne,
            DROP quantite_utilisee, DROP quantite_recoltee_primaire;');
    }
}
