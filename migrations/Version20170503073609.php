<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Copie du retour de la commande doctrine:schema:update le 03 mai 2017.
 */
class Version20170503073609 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        throw new Exception('Pas de retour en arriere possible', 1);
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs

        $this->addSql('ALTER TABLE engrais_demande CHANGE id_engrais_demande id_engrais_demande BIGINT DEFAULT NULL;');
        $this->addSql('ALTER TABLE commercialisation_produits DROP unite;');
        $this->addSql('ALTER TABLE synchro_session ADD date_debut_data DATETIME DEFAULT NULL, ADD date_fin_data DATETIME DEFAULT NULL, ADD date_fin_maj_sql DATETIME DEFAULT NULL, ADD master_date_debut_data DATETIME DEFAULT NULL, ADD master_date_fin_data DATETIME DEFAULT NULL, ADD master_date_fin_maj_sql DATETIME DEFAULT NULL, DROP date_debut, DROP date_fin, DROP master_date_debut, DROP master_date_fin, CHANGE zipmd5 zipmd5 VARCHAR(255) DEFAULT NULL, CHANGE status status VARCHAR(50) NOT NULL, CHANGE master_nbtry master_nbtry INT NOT NULL, CHANGE master_zipmd5 master_zipmd5 VARCHAR(255) DEFAULT NULL, CHANGE master_status master_status VARCHAR(255) NOT NULL, CHANGE closed closed TINYINT(1) NOT NULL;');
        $this->addSql("ALTER TABLE tbl_decoupage_admin CHANGE old_code old_code VARCHAR(6) NOT NULL COMMENT 'A effacer après upgrade définitif.';");
        $this->addSql('ALTER TABLE equipement_agricole CHANGE id_equipement_agricole id_equipement_agricole BIGINT DEFAULT NULL;');
    }
}
