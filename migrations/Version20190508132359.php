<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190508132359 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO tbl_unite_produits (id,unite_produit,cdate,udate,utilisateur) VALUE
                     (10,'Bouture','2019-05-08 13:12:32','2019-05-08 13:12:32','jorge');");
        $this->addSql("INSERT INTO tbl_variete_semences (id,nom,culture_id,unite_id,cdate,udate,utilisateur) VALUES
        (1, 'Grain long',1,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (2, 'Grain court',1,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (3,'Eleusine',2,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (4,'Maïs',3,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (5,'Iron local',4,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (6,'Karundi',4,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (7,'Sorgho',5,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (8,'Arachide',6,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (9,'Haricot jaune',7,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (10,'Haricot mélangé',8,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (11,'Haricot volubile',9,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (12,'Soja',NULL,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (13,'Niebe',NULL,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (14,'Colocase',10,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (15,'Igname',11,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (16,'Manioc',12,10,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (17,'Chair blanche',13,10,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (18,'Chair orange',13,10,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (19,'Ndinamagara',14,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (20,'Victoria',14,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (21,'Palmier à huile',15,6,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (22,'Tournesol',16,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (23,'Ail',20,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (24,'Amarante',21,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (25,'Aubergine',22,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (26,'Carotte',23,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (27,'Céleri',24,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (28,'Choux Blanc',25,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (29,'Choux fleur',26,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (30,'Concombre',27,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (31,'Courge',28,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (32,'Oignon blanc',NULL,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (33,'Oignon rouge',NULL,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (34,'Persil',34,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (35,'Petit pois',35,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (36,'Tomate',40,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (37,'Ananas',42,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (38,'Avocat',43,4,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script'),
        (39,'Banane ',44,6,'2019-05-08 10:10:18','2019-05-08 10:10:18','Init script');");
    }
}
