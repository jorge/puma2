<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180219144117 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE VIEW base_bis AS
                        SELECT c.denomination,
                            c.id as coop_id,
                            e.id AS expl_id,
                            pers.id AS pers_id,
                            pers.sexe,
                            pers.date_naissance
                        FROM personnes pers
                            INNER JOIN exploitations e ON 	e.id=pers.exploitation_id
                            INNER JOIN pivot_exploitation_organisations p ON e.id = p.exploitation_id
                            INNER JOIN	tbl_organisations g ON p.organisation_id=g.id
                            INNER JOIN tbl_organisations c ON c.id=g.parent_id
                        WHERE pers.nom<>'';");
    }
}
