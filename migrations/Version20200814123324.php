<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200814123324 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE demande_credit DROP FOREIGN KEY FK_5E852811953C1C61');
        $this->addSql('DROP INDEX IDX_5E852811953C1C61 ON demande_credit');
        $this->addSql('ALTER TABLE demande_credit DROP source_id');

        $this->addSql('ALTER TABLE engrais_demande ADD old_engrais_id CHAR(36) CHARACTER SET utf8 DEFAULT NULL COLLATE `utf8_unicode_ci`');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE demande_credit ADD source_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE demande_credit ADD CONSTRAINT FK_5E852811953C1C61 FOREIGN KEY (source_id) REFERENCES tbl_type_creancier (id)');
        $this->addSql('CREATE INDEX IDX_5E852811953C1C61 ON demande_credit (source_id)');
    }
}
