<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190403101006 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE parcelle_operation_culturale_tbl_cultures;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD culture_id INT NOT NULL;');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE parcelle_operation_culturale_tbl_cultures (
             parcelle_operation_culturale_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
             tbl_cultures_id INT NOT NULL,
             INDEX IDX_3805E2D8519C0B8A (parcelle_operation_culturale_id),
             INDEX IDX_3805E2D8EE1FBA60 (tbl_cultures_id),
             PRIMARY KEY(parcelle_operation_culturale_id, tbl_cultures_id))
             DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
        $this->addSql('ALTER TABLE parcelle_operation_culturale_tbl_cultures
            ADD CONSTRAINT FK_3805E2D8519C0B8A FOREIGN KEY (parcelle_operation_culturale_id)
            REFERENCES parcelle_operation_culturale (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale_tbl_cultures
            ADD CONSTRAINT FK_3805E2D8EE1FBA60 FOREIGN KEY (tbl_cultures_id)
            REFERENCES tbl_cultures (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale DROP FOREIGN KEY FK_123C9057B108249D;');
        $this->addSql('DROP INDEX IDX_123C9057B108249D ON parcelle_operation_culturale;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale DROP culture_id;');
    }
}
