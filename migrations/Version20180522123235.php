<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180522123235 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        /* effacer les enregistrement dans la table */
        $this->addSql('DELETE FROM tbl_animaux_production;');
    }

    public function up(Schema $schema): void
    {
        /* populate table tbl_animaux_production */
        $this->addSql("INSERT INTO tbl_animaux_production (id, production, cdate, udate, utilisateur) VALUES
                    ('a21a1148-5dbb-11e8-9b41-b8ca3a965972', 'lait', '2018-05-22 12:28:32', '2018-05-22 12:28:32', 'jorge'),
                    ('a21ca699-5dbb-11e8-9b41-b8ca3a965972', 'oeufs', '2018-05-22 12:28:32', '2018-05-22 12:28:32', 'jorge'),
                    ('a21ca951-5dbb-11e8-9b41-b8ca3a965972', 'fumier', '2018-05-22 12:28:32', '2018-05-22 12:28:32', 'jorge');");
    }
}
