<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181127123347 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_produits ADD COLUMN old_culture_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE tbl_produits SET old_culture_id=culture_id;');
        $this->addSql('UPDATE tbl_produits SET culture_id=(SELECT tbl_cultures.id FROM tbl_cultures WHERE tbl_cultures.old_id=tbl_produits.old_culture_id);');
        $this->addSql('ALTER TABLE tbl_produits MODIFY culture_id int(10);');
        $this->addSql('ALTER TABLE tbl_produits ADD FOREIGN KEY (culture_id) REFERENCES tbl_cultures(id);');
    }
}
