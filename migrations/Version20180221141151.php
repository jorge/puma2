<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180221141151 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO tbl_travail_type (id, cdate, udate, type_travail, utilisateur) VALUES
                    ('d8381d85-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Semences / Achat semences', 'Init script'),
                    ('d87fc79c-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Semis', 'Init script'),
                    ('d886d8b9-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Fertilisation / Fumure organique', 'Init script'),
                    ('d886db5b-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Fertilisation / Urée', 'Init script'),
                    ('d886dd42-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Fertilisation / DAP', 'Init script'),
                    ('d886de8c-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Fertilisation / KCI', 'Init script'),
                    ('d886e000-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Fertilisation / NPK', 'Init script'),
                    ('d886e1b1-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Récolte', 'Init script'),
                    ('d886e322-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Stockage', 'Init script'),
                    ('d886e4a3-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Battage', 'Init script'),
                    ('d886e5f7-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Vente', 'Init script'),
                    ('d886e6f8-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Transport des intrants', 'Init script'),
                    ('d886e875-170b-11e8-a525-b8ca3a965972', '2018-02-21 13:33:47', '2018-02-21 13:33:47','Transport des récoltes', 'Init script');");
        $this->addSql("UPDATE tbl_travail_type SET type_travail='Préparation du sol/Labour' WHERE id='792dc4c3-2698-11e7-98c8-b8ca3a965972';");
    }
}
