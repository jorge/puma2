<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190417163153 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        throw new Exception('Impossible to go back', 1);
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD unite_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD CONSTRAINT FK_3129BF6CEC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');
        $this->addSql('CREATE INDEX IDX_3129BF6CEC4A74AB ON tbl_operation_culturale_intrant (unite_id);');
    }
}
