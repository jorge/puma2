<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180314142121 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('TRUNCATE TABLE collecte;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO collecte ( id, exploitation_id, annee_enquete,enqueteur,date_enquete,utilisateur)
                            SELECT UUID(), personnes.exploitation_id, personne_details.annee, NULL, NULL, GROUP_CONCAT(distinct personne_details.utilisateur)
                            FROM personne_details, personnes
                            WHERE personne_details.personnes_id=personnes.id
                            GROUP BY personnes.exploitation_id, personne_details.annee;');
        $this->addSql('UPDATE collecte c, exploitations e
                    SET c.enqueteur=e.faite_par, c.date_enquete=e.date_collecte WHERE c.exploitation_id = e.id;');
    }
}
