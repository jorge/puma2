<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181207152725 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM parcelle_operation_culturale;');
        $this->addSql('DELETE FROM tbl_operation_culturale_type;');
        $this->addSql('DELETE FROM tbl_operation_culturale_type_category;');
    }

    public function up(Schema $schema): void
    {
        $data = [
            'Préparation du sol / Labour' => [['Labour', null]],
            'Semis' => [['Plantation', null], ['Semences', null], ['Repicage', null]],
            'Entretiens' => [['Paillage', null], ['Démariage', null], ['Sarclage', null], ['Buttage ??', null], ['Tuteurage', null], ['Travail antiérosif', null]],
            'Amadement' => [['Chaux', null]],
            'Fumure organique' => [['Composte', null], ['Fiente', null], ['Fumier', 'kg']],
            'Fumure chimique' => [['Sorgho', null], ['Haricot mélancé', null], ['Eleusine', null]],
            'Herbicides' => [],
            'Phytosanitaires' => [],
        ];

        foreach ($data as $category => $type_and_unit_array) {
            $sql = sprintf(
                "INSERT INTO tbl_operation_culturale_type_category (nom) VALUES  ('%s');",
                $category
            );
            $this->addSql($sql);

            foreach ($type_and_unit_array as $type_and_unit) {
                $type_name = $type_and_unit[0];
                $unit = $type_and_unit[1];
                $sql = sprintf(
                    "INSERT INTO tbl_operation_culturale_type (nom, unite, category_id)
                        VALUES ('%s', %s, (SELECT id FROM tbl_operation_culturale_type_category WHERE nom = '%s'));",
                    $type_name,
                    ($unit ? "'" . $unit . "'" : 'NULL'),
                    $category
                );
                $this->addSql($sql);
            }
        }
    }
}
