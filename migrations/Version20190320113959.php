<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190320113959 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE collecte DROP fiche_type;');
        $this->addSql('ALTER TABLE collecte CHANGE annee_enquete annee_enquete SMALLINT NOT NULL;');
    }

    public function getDescription(): string
    {
        return 'Updating collecte for working with exercice';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE collecte ADD fiche_type SMALLINT NULL;');
        $this->addSql('ALTER TABLE collecte CHANGE annee_enquete annee_enquete SMALLINT NULL;');
    }
}
