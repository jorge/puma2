<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191004080049 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE parcelle_historique_analyse_sol;');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE parcelle_historique_analyse_sol (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            parcelle_historique_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            analyse_sol_id INT DEFAULT NULL, date DATE NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(255) NOT NULL,
            INDEX IDX_607123F67FFE94B (parcelle_historique_id),
            INDEX IDX_607123F2D854847 (analyse_sol_id),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
        $this->addSql('ALTER TABLE parcelle_historique_analyse_sol ADD CONSTRAINT FK_607123F67FFE94B
                FOREIGN KEY (parcelle_historique_id) REFERENCES parcelle_historiques (id);');
        $this->addSql('ALTER TABLE parcelle_historique_analyse_sol ADD CONSTRAINT FK_607123F2D854847
                FOREIGN KEY (analyse_sol_id) REFERENCES tbl_analyse_sol (id);');
    }
}
