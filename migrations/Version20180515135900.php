<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180515135900 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        /* delete coopérative doublon */
        $this->addSql("DELETE FROM pivot_personnes_organisations WHERE organisation_id='7c8288b5-2698-11e7-98c8-b8ca3a965972'");
        $this->addSql("DELETE FROM tbl_organisations WHERE id='7c8288b5-2698-11e7-98c8-b8ca3a965972';");
    }
}
