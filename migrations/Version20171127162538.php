<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171127162538 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelle_herbicide DROP FOREIGN KEY FK_EC35E45576A6380;');
        $this->addSql('DROP INDEX IDX_EC35E45576A6380 ON parcelle_herbicide;');
        $this->addSql('ALTER TABLE parcelle_herbicide ADD herbicide LONGTEXT NOT NULL, DROP herbicide_id;');
        $this->addSql('DROP TABLE tbl_herbicides;');
        // this up() migration is auto-generated, please modify it to your needs
    }
}
