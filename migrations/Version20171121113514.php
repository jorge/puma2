<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171121113514 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE formations_besoin DROP priorite');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE formations_besoin ADD priorite SMALLINT DEFAULT NULL');
    }
}
