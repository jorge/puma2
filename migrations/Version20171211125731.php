<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171211125731 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE personne_details ADD CONSTRAINT FK_ACA9EF77146AD7BC FOREIGN KEY (personnes_id) REFERENCES personnes (id);');
        $this->addSql('ALTER TABLE personne_details ADD CONSTRAINT FK_ACA9EF774272FC9F FOREIGN KEY (domaine_id) REFERENCES tbl_domaines_etudes_superieures (id);');
        $this->addSql('CREATE UNIQUE INDEX unique_personnes_annee ON personne_details (annee, personnes_id);');
    }
}
