<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429053636 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'Suppression de tables inutilitées (gérées par symfony directement) pour suivis coop';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP TABLE besoins_formations;');
        $this->addSql('DROP TABLE besoins_institutionel;');
        $this->addSql('DROP TABLE besoin_service_aux_membres;');
    }
}
