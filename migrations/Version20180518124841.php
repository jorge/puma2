<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180518124841 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM tbl_organisateurs WHERE id='0cd312ae-5a99-11e8-b22b-b8ca3a965972';");
        $this->addSql("DELETE FROM tbl_organisateurs WHERE id='0cdca684-5a99-11e8-b22b-b8ca3a965972';");
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO puma_03.tbl_organisateurs (id, organisateur,cdate,udate,utilisateur) VALUES ('0cd312ae-5a99-11e8-b22b-b8ca3a965972', 'PAM','2018-05-18 12:43:23','2018-05-18 12:43:23', 'jorge');");
        $this->addSql("INSERT INTO puma_03.tbl_organisateurs (id, organisateur,cdate,udate,utilisateur) VALUES ('0cdca684-5a99-11e8-b22b-b8ca3a965972', 'IFDC','2018-05-18 12:43:23','2018-05-18 12:43:23', 'jorge');");
    }
}
