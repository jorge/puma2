<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181126144033 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_organisations DROP COLUMN repertoire');
    }

    public function up(Schema $schema): void
    {
        /* etablir les groupements qui ne sont pas repertorié comme membres de la coopérative */
        $this->addSql("ALTER TABLE tbl_organisations ADD repertoire TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql('UPDATE tbl_organisations t, xrepertoire x SET t.repertoire=x.repertoire WHERE t.id=x.id;');
    }
}
