<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181210154240 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelles DROP annee SMALLINT NOT NULL;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelles ADD annee SMALLINT NOT NULL;');
    }
}
