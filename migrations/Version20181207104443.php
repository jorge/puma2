<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181207104443 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE parcelle_operation_culturale;');
        $this->addSql('DROP TABLE tbl_operation_culturale_type;');
        $this->addSql('DROP TABLE tbl_operation_culturale_type_category;');
        $this->addSql('ALTER TABLE historique_cultures DROP FOREIGN KEY FK_ECB82595B108249D;');
        $this->addSql("ALTER TABLE historique_cultures ADD old_culture CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)';");
    }

    public function up(Schema $schema): void
    {
        // TODO verifier udate et cdate
        $this->addSql('CREATE TABLE tbl_operation_culturale_type_category (
            id INT AUTO_INCREMENT NOT NULL,
            nom VARCHAR(255) NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            UNIQUE INDEX UNIQ_22B2EC96C6E55B5 (nom),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;');

        $this->addSql("CREATE TABLE parcelle_operation_culturale (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            parcelle_historique_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            type_id INT DEFAULT NULL,
            culture_id INT DEFAULT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            mainOeuvreFamiliale SMALLINT DEFAULT NULL,
            mainOeuvreSalarie SMALLINT DEFAULT NULL, date DATE NOT NULL,
            recolte_id INT DEFAULT NULL,
            produitInterne INT DEFAULT NULL,
            produitExterne INT DEFAULT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            INDEX IDX_123C905767FFE94B (parcelle_historique_id),
            INDEX IDX_123C9057C54C8C93 (type_id),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql('CREATE TABLE tbl_operation_culturale_type (
            id INT AUTO_INCREMENT NOT NULL,
            category_id INT DEFAULT NULL,
            nom VARCHAR(255) NOT NULL,
            unite VARCHAR(5) DEFAULT NULL,
            UNIQUE INDEX UNIQ_9F54E47C6C6E55B5 (nom), INDEX IDX_9F54E47C12469DE2 (category_id),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;');

        $this->addSql('ALTER TABLE historique_cultures DROP old_culture, CHANGE culture_id culture_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE historique_cultures ADD CONSTRAINT FK_ECB82595B108249D FOREIGN KEY (culture_id) REFERENCES tbl_cultures (id)');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_123C905767FFE94B FOREIGN KEY (parcelle_historique_id) REFERENCES parcelle_historiques (id);');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_123C9057C54C8C93 FOREIGN KEY (type_id) REFERENCES tbl_operation_culturale_type (id);');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_123C9057B108249D FOREIGN KEY (culture_id) REFERENCES tbl_cultures (id);');
        $this->addSQl('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_123C9057C2C4F051 FOREIGN KEY (recolte_id) REFERENCES tbl_produits (id);');
        // $this->addSql("ALTER TABLE parcelle_historiques ADD CONSTRAINT FK_F6AA48534433ED66 FOREIGN KEY (parcelle_id) REFERENCES parcelles (id);");
    }
}
