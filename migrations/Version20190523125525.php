<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190523125525 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exploitation_livraisons ADD date_debut DATE DEFAULT NULL,
                                ADD date_fin DATE DEFAULT NULL;');
        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails ADD date_debut DATE DEFAULT NULL,
                                ADD date_fin DATE DEFAULT NULL;');
        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails DROP COLUMN periode;');
    }
}
