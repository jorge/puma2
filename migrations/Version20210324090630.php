<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210324090630 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf(
            'mysql' !== $this->connection->getDatabasePlatform()->getName(),
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql('ALTER TABLE animaux CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE animaux_labour CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE animaux_production CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE animaux_structure CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE commercialisation_produits CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE contraintes_developpement CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE cotisations CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE depenses_menage CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE dettes CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE engrais_demande CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE exploitation_charges CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE filieres CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE formations_besoin CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE habitat CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE mo_familiale_externe CHANGE annee annee SMALLINT NOT NULL');
        $this->addSql('ALTER TABLE mo_familiale_interne CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE moyens_transport CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE outillage CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE parcelle_historiques CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE personne_details CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE phyto_demande CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE securite_alimentaire CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE semence_demandee CHANGE annee annee SMALLINT DEFAULT NULL');
        $this->addSql('ALTER TABLE services_besoin CHANGE annee annee SMALLINT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        throw new Exception('No return is possible', 1);
    }
}
