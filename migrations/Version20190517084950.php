<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190517084950 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_produit_phyto ADD unite_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE tbl_produit_phyto
            ADD CONSTRAINT FK_25125945EC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');
        $this->addSql('CREATE INDEX IDX_25125945EC4A74AB ON tbl_produit_phyto (unite_id);');
        $this->addSql("INSERT INTO tbl_unite_produits (id,unite_produit,cdate,udate,utilisateur) VALUES
                (11,'Litre','2019-05-17 09:04:47','2019-05-17 09:04:47', 'jorge');");
    }
}
