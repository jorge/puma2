<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170512132550 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exploitations CHANGE annee_adhesion annee_adhesion VARCHAR(4), CHANGE annee_data annee_data VARCHAR(4);');
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exploitations CHANGE annee_adhesion annee_adhesion VARCHAR(4) NOT NULL, CHANGE annee_data annee_data VARCHAR(4) NOT NULL;');
    }
}
