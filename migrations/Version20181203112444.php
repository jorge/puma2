<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181203112444 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        // rename des groupements
        $this->addSql("UPDATE tbl_organisations SET denomination='ABAJAMUGAMBI', decoupage_admin_id='7c5b00bd-2698-11e7-98c8-b8ca3a965972' WHERE id='8d27bf66-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("INSERT INTO tbl_organisations (id, decoupage_admin_id, denomination, utilisateur) VALUES ('a500fd29-f6eb-11e8-aad8-b8ca3a965972', '7c5a96ac-2698-11e7-98c8-b8ca3a965972', 'TURWANYUBUNEBWE','jorge');");
    }
}
