<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190109154810 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tbl_operation_culturale_type ADD type SMALLINT NOT NULL;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_type ADD CONSTRAINT FK_9F54E47C12469DE2 FOREIGN KEY (category_id) REFERENCES tbl_operation_culturale_type_category (id);');
    }
}
