<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200429055746 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'Suppression de tables inutilitées (gérées par symfony directement) pour suivis coop';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your need
        $this->addSql("CREATE TABLE suivi_organisations_besoins_formation (suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)', tbl_themes_formations_id INT NOT NULL, INDEX IDX_CAA436B0E8E858E2 (suivi_organisations_id), INDEX IDX_CAA436B043733EE7 (tbl_themes_formations_id), PRIMARY KEY(suivi_organisations_id, tbl_themes_formations_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");
        $this->addSql("CREATE TABLE suivi_organisations_besoins_institutionnels (suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)', tbl_themes_formations_id INT NOT NULL, INDEX IDX_B8F491E8E858E2 (suivi_organisations_id), INDEX IDX_B8F49143733EE7 (tbl_themes_formations_id), PRIMARY KEY(suivi_organisations_id, tbl_themes_formations_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");
        $this->addSql("CREATE TABLE suivi_organisations_besoins_serives_aux_membres (suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)', tbl_services_aux_membres_id INT NOT NULL, INDEX IDX_A32DF1F4E8E858E2 (suivi_organisations_id), INDEX IDX_A32DF1F43B8329E (tbl_services_aux_membres_id), PRIMARY KEY(suivi_organisations_id, tbl_services_aux_membres_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");
        $this->addSql('ALTER TABLE suivi_organisations_besoins_formation ADD CONSTRAINT FK_CAA436B0E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE suivi_organisations_besoins_formation ADD CONSTRAINT FK_CAA436B043733EE7 FOREIGN KEY (tbl_themes_formations_id) REFERENCES tbl_themes_formations (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE suivi_organisations_besoins_institutionnels ADD CONSTRAINT FK_B8F491E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE suivi_organisations_besoins_institutionnels ADD CONSTRAINT FK_B8F49143733EE7 FOREIGN KEY (tbl_themes_formations_id) REFERENCES tbl_themes_formations (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE suivi_organisations_besoins_serives_aux_membres ADD CONSTRAINT FK_A32DF1F4E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE suivi_organisations_besoins_serives_aux_membres ADD CONSTRAINT FK_A32DF1F43B8329E FOREIGN KEY (tbl_services_aux_membres_id) REFERENCES tbl_services_aux_membres (id) ON DELETE CASCADE;');
    }
}
