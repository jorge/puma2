<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * MAJ du champ udate automatiquement par MYSQL.
 */
class Version20170503101601 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        throw new Exception('Pas de retour en arriere', 1);
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE animaux MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE animaux_alimentation MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE animaux_produits MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE animaux_structure MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE batiments MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE besoin_service_aux_membres MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE besoins_formations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE besoins_institutionel MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE commercialisation_produits MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE contraintes_developpement MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE cotisations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE depenses_menage MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE dettes MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE engrais_demande MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE equipement_agricole MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE exploitation_charges MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE exploitations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE filiere_commercialisation MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE filieres MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE formations_besoin MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE formations_recu MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE habitat MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE historique_cultures MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE infrastructures MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE materiel_de_bureau MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE materiel_roulant MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE mo_familiale_externe MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE mo_familiale_interne MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE mo_toutes_applis MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE mobilisations_communautaires MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE moyens_transport MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE niveau_de_reconnaissance MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE outillage MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_amendements MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_fumure_min MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_fumure_org MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_herbicide MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_historiques MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_phytosanitaire MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_recoltes MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_semences MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelle_techniques MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE parcelles MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE personne_details MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE personnes MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE phpspusers MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE phyto_demande MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE pivot_exploitation_organisations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE pivot_personnes_organisations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE prestation_de_services MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE securite_alimentaire MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE semence_demandee MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE services_aux_membres MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE services_besoin MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE services_payants_aux_membres MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE suivi_organisations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_achat_agricoles MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_activites_communautaires MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_aleas MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_alimentation MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_amendements MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_animaux_technique MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_categories_agricoles MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_contraints MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_credit_objet MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_cultures MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_decoupage_admin MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_decoupage_admin_level MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_engrais MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_entretiens MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_etat_civil MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_exposition MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_filieres MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_fournisseurs MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_fumure_min MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_fumure_org MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_herbicides MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_infrastructures MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_institutions_financieres MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_mo_type MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_mode_culture MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_mois MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_moyen_transport MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_organisateurs MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_organisation_level MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_organisations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_pente MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_presentation MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_prestations_services MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_produit_phyto MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_produits MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_professions MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_propriete MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_provinces_communes MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_rapports MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_saisons MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_semences MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_services MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_services_aux_membres MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_services_payants MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_situation MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_terre_protection MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_themes_formations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_traitements MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_travail_type MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_type_commercialisation MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_type_depense MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_type_garantie MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_type_travail MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_types_elevage MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_unite_filieres MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_unite_mo MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_unite_produits MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_unites_transformations MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE tbl_utilites MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
        $this->addSql('ALTER TABLE unite_de_transformation MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
    }
}
