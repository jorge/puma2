<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190201154753 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE parcelles_tbl_antierosives (
            parcelles_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_antierosives_id INT NOT NULL, INDEX IDX_7777715A9JORGE (parcelles_id),
            INDEX IDX_88888154A719501 (tbl_antierosives_id),
            PRIMARY KEY(parcelles_id, tbl_antierosives_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE parcelles_tbl_antierosives ADD CONSTRAINT FK_7777715A9JORGE FOREIGN KEY (parcelles_id) REFERENCES parcelles (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE parcelles_tbl_antierosives ADD CONSTRAINT FK_88888154A719501 FOREIGN KEY (tbl_antierosives_id) REFERENCES tbl_antierosives (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE parcelles_tbl_antierosives ADD COLUMN cdate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;');
        $this->addSql('ALTER TABLE parcelles_tbl_antierosives ADD COLUMN udate datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;');
    }
}
