<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180910135858 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_services_aux_membres MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_services_aux_membres MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE besoin_service_aux_membres ADD COLUMN old_service_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE services_aux_membres ADD COLUMN old_service_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE besoin_service_aux_membres SET old_service_id=service_id;');
        $this->addSql('UPDATE services_aux_membres SET old_service_id=service_id;');

        $this->addSql('UPDATE besoin_service_aux_membres SET service_id=(SELECT tbl_services_aux_membres.id FROM tbl_services_aux_membres WHERE
                 tbl_services_aux_membres.old_id=besoin_service_aux_membres.old_service_id);');
        $this->addSql('UPDATE services_aux_membres SET service_id=(SELECT tbl_services_aux_membres.id FROM tbl_services_aux_membres WHERE
                 tbl_services_aux_membres.old_id=services_aux_membres.old_service_id);');

        $this->addSql('ALTER TABLE besoin_service_aux_membres MODIFY service_id int(10);');
        $this->addSql('ALTER TABLE services_besoin MODIFY service_id int(10);');

        $this->addSql('ALTER TABLE besoin_service_aux_membres ADD FOREIGN KEY (service_id) REFERENCES tbl_services(id);');
        $this->addSql('ALTER TABLE services_besoin ADD FOREIGN KEY (service_id) REFERENCES tbl_services(id);');

        //tbl_services_payants");

        $this->addSql('ALTER TABLE services_payants_aux_membres DROP FOREIGN KEY FK_63EEA3BFB37DE6D6;');

        $this->addSql('ALTER TABLE tbl_services_payants CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_services_payants DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_services_payants CHANGE id_service_payant id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_services_payants ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_services_payants MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_services_payants ADD UNIQUE INDEX UNIQ_79EF5137B94D4135 (id);');
        $this->addSql('ALTER TABLE tbl_services_payants MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_services_payants MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE services_payants_aux_membres ADD COLUMN old_service_payant_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE services_payants_aux_membres SET old_service_payant_id=service_payant_id;');
        $this->addSql('UPDATE services_payants_aux_membres SET service_payant_id=(SELECT tbl_services_payants.id FROM tbl_services_payants WHERE
                 tbl_services_payants.old_id=services_payants_aux_membres.old_service_payant_id);');

        $this->addSql('ALTER TABLE services_payants_aux_membres MODIFY service_payant_id int(10);');

        $this->addSql('ALTER TABLE services_payants_aux_membres ADD FOREIGN KEY (service_payant_id) REFERENCES tbl_services(id);');

        //tbl_themes_formations");

        $this->addSql('ALTER TABLE besoins_formations DROP FOREIGN KEY FK_2F41C3A95200282E;');
        $this->addSql('ALTER TABLE besoins_institutionel DROP FOREIGN KEY FK_AA9B60973426FBE9;');
        $this->addSql('ALTER TABLE formations_besoin DROP FOREIGN KEY FK_930D3DF05200282E;');
        $this->addSql('ALTER TABLE formations_recu DROP FOREIGN KEY FK_9B11283459027487;');
        $this->addSql('ALTER TABLE tbl_themes_formations CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_themes_formations CHANGE id_theme_formation id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_themes_formations DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_themes_formations ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_themes_formations MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_themes_formations ADD UNIQUE INDEX UNIQ_79E32536664D4258 (id);');
        $this->addSql('ALTER TABLE tbl_themes_formations MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_themes_formations MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE besoins_formations ADD COLUMN old_formation_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE besoins_institutionel ADD COLUMN old_institutionel_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE formations_besoin ADD COLUMN old_formation_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE formations_recu ADD COLUMN old_theme_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE besoins_formations SET old_formation_id=formation_id;');
        $this->addSql('UPDATE besoins_institutionel SET old_institutionel_id=institutionel_id;');
        $this->addSql('UPDATE formations_besoin SET old_formation_id=formation_id;');
        $this->addSql('UPDATE formations_recu SET old_theme_id=theme_id;');

        $this->addSql('UPDATE besoins_formations SET formation_id=(SELECT tbl_themes_formations.id FROM tbl_themes_formations WHERE
                 tbl_themes_formations.old_id=besoins_formations.old_formation_id);');
        $this->addSql('UPDATE besoins_institutionel SET institutionel_id=(SELECT tbl_themes_formations.id FROM tbl_themes_formations WHERE
                 tbl_themes_formations.old_id=besoins_institutionel.old_institutionel_id);');
        $this->addSql('UPDATE formations_besoin SET formation_id=(SELECT tbl_themes_formations.id FROM tbl_themes_formations WHERE
                 tbl_themes_formations.old_id=formations_besoin.old_formation_id);');
        $this->addSql('UPDATE formations_recu SET theme_id=(SELECT tbl_themes_formations.id FROM tbl_themes_formations WHERE
                 tbl_themes_formations.old_id=formations_recu.old_theme_id);');

        $this->addSql('ALTER TABLE besoins_formations MODIFY formation_id int(10);');
        $this->addSql('ALTER TABLE besoins_institutionel MODIFY institutionel_id int(10);');
        $this->addSql('ALTER TABLE formations_besoin MODIFY formation_id int(10);');
        $this->addSql('ALTER TABLE formations_recu MODIFY theme_id int(10);');

        $this->addSql('ALTER TABLE besoins_formations ADD FOREIGN KEY (formation_id) REFERENCES tbl_themes_formations(id);');
        $this->addSql('ALTER TABLE besoins_institutionel ADD FOREIGN KEY (institutionel_id) REFERENCES tbl_themes_formations(id);');
        $this->addSql('ALTER TABLE formations_besoin ADD FOREIGN KEY (formation_id) REFERENCES tbl_themes_formations(id);');
        $this->addSql('ALTER TABLE formations_recu ADD FOREIGN KEY (theme_id) REFERENCES tbl_themes_formations(id);');

        //tbl_type_garantie");

        $this->addSql('ALTER TABLE dettes DROP FOREIGN KEY FK_15565CF1A4B9602F;');

        $this->addSql('ALTER TABLE tbl_type_garantie CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_type_garantie DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_type_garantie CHANGE id_type_garantie id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_type_garantie ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_type_garantie MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_type_garantie ADD UNIQUE INDEX UNIQ_79EF5977B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_type_garantie MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_type_garantie MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE dettes ADD COLUMN old_garantie_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE dettes SET old_garantie_id=garantie_id;');
        $this->addSql('UPDATE dettes SET garantie_id=(SELECT tbl_type_garantie.id FROM tbl_type_garantie WHERE
                 tbl_type_garantie.old_id=dettes.old_garantie_id);');

        $this->addSql('ALTER TABLE dettes MODIFY garantie_id int(10);');

        $this->addSql('ALTER TABLE dettes ADD FOREIGN KEY (garantie_id) REFERENCES tbl_type_garantie(id);');

        //tbl_types_elevage");

        $this->addSql('ALTER TABLE animaux DROP FOREIGN KEY FK_9ABE194D19E124A8;');

        $this->addSql('ALTER TABLE tbl_types_elevage CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_types_elevage DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_types_elevage CHANGE id_type_elevage id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_types_elevage ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_types_elevage MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_types_elevage ADD UNIQUE INDEX UNIQ_79EF5137236D4258 (id);');
        $this->addSql('ALTER TABLE tbl_types_elevage MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_types_elevage MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE animaux ADD COLUMN old_type_elevage_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE animaux SET old_type_elevage_id=type_elevage_id;');
        $this->addSql('UPDATE animaux SET type_elevage_id=(SELECT tbl_types_elevage.id FROM tbl_types_elevage WHERE
                 tbl_types_elevage.old_id=animaux.old_type_elevage_id);');

        $this->addSql('ALTER TABLE animaux MODIFY type_elevage_id int(10);');

        $this->addSql('ALTER TABLE animaux ADD FOREIGN KEY (type_elevage_id) REFERENCES tbl_types_elevage(id);');

        $this->addSql('ALTER TABLE parcelle_amendements DROP FOREIGN KEY FK_C328AFE4EC4A74AB;');
        $this->addSql('ALTER TABLE parcelle_fumure_min DROP FOREIGN KEY FK_5B2EF290EC4A74AB;');
        $this->addSql('ALTER TABLE parcelle_fumure_org DROP FOREIGN KEY FK_884055C0EC4A74AB;');
        $this->addSql('ALTER TABLE parcelle_herbicide DROP FOREIGN KEY FK_EC35E45EC4A74AB;');
        $this->addSql('ALTER TABLE parcelle_phytosanitaire DROP FOREIGN KEY FK_1C09AB53EC4A74AB;');
        $this->addSql('ALTER TABLE parcelle_recoltes DROP FOREIGN KEY FK_CB0CB02BEC4A74AB;');
        $this->addSql('ALTER TABLE parcelle_semences DROP FOREIGN KEY FK_C47CB41AEC4A74AB;');
        $this->addSql('ALTER TABLE parcelle_techniques DROP FOREIGN KEY FK_EBB928F9EC4A74AB;');

        $this->addSql('ALTER TABLE tbl_unite_produits CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_unite_produits DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_unite_produits CHANGE id_unite_produit id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_unite_produits ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_unite_produits MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_unite_produits ADD UNIQUE INDEX UNIQ_79EF5137B94D6548 (id);');
        $this->addSql('ALTER TABLE tbl_unite_produits MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_unite_produits MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE parcelle_amendements ADD COLUMN old_unite_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_fumure_min ADD COLUMN old_unite_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_fumure_org ADD COLUMN old_unite_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_herbicide ADD COLUMN old_unite_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_phytosanitaire ADD COLUMN old_unite_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_recoltes ADD COLUMN old_unite_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_semences ADD COLUMN old_unite_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_techniques ADD COLUMN old_unite_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE parcelle_amendements SET old_unite_id=unite_id;');
        $this->addSql('UPDATE parcelle_fumure_min SET old_unite_id=unite_id;');
        $this->addSql('UPDATE parcelle_fumure_org SET old_unite_id=unite_id;');
        $this->addSql('UPDATE parcelle_herbicide SET old_unite_id=unite_id;');
        $this->addSql('UPDATE parcelle_phytosanitaire SET old_unite_id=unite_id;');
        $this->addSql('UPDATE parcelle_recoltes SET old_unite_id=unite_id;');
        $this->addSql('UPDATE parcelle_semences SET old_unite_id=unite_id;');
        $this->addSql('UPDATE parcelle_techniques SET old_unite_id=unite_id;');

        $this->addSql('UPDATE parcelle_amendements SET unite_id=(SELECT tbl_unite_produits.id FROM tbl_unite_produits WHERE
                 tbl_unite_produits.old_id=parcelle_amendements.old_unite_id);');
        $this->addSql('UPDATE parcelle_fumure_min SET unite_id=(SELECT tbl_unite_produits.id FROM tbl_unite_produits WHERE
                 tbl_unite_produits.old_id=parcelle_fumure_min.old_unite_id);');
        $this->addSql('UPDATE parcelle_fumure_org SET unite_id=(SELECT tbl_unite_produits.id FROM tbl_unite_produits WHERE
                 tbl_unite_produits.old_id=parcelle_fumure_org.old_unite_id);');
        $this->addSql('UPDATE parcelle_herbicide SET unite_id=(SELECT tbl_unite_produits.id FROM tbl_unite_produits WHERE
                 tbl_unite_produits.old_id=parcelle_herbicide.old_unite_id);');
        $this->addSql('UPDATE parcelle_phytosanitaire SET unite_id=(SELECT tbl_unite_produits.id FROM tbl_unite_produits WHERE
                 tbl_unite_produits.old_id=parcelle_phytosanitaire.old_unite_id);');
        $this->addSql('UPDATE parcelle_recoltes SET unite_id=(SELECT tbl_unite_produits.id FROM tbl_unite_produits WHERE
                 tbl_unite_produits.old_id=parcelle_recoltes.old_unite_id);');
        $this->addSql('UPDATE parcelle_semences SET unite_id=(SELECT tbl_unite_produits.id FROM tbl_unite_produits WHERE
                 tbl_unite_produits.old_id=parcelle_semences.old_unite_id);');
        $this->addSql('UPDATE parcelle_techniques SET unite_id=(SELECT tbl_unite_produits.id FROM tbl_unite_produits WHERE
                 tbl_unite_produits.old_id=parcelle_techniques.old_unite_id);');

        $this->addSql('ALTER TABLE parcelle_amendements MODIFY unite_id int(10);');
        $this->addSql('ALTER TABLE parcelle_fumure_min MODIFY unite_id int(10);');
        $this->addSql('ALTER TABLE parcelle_fumure_org MODIFY unite_id int(10);');
        $this->addSql('ALTER TABLE parcelle_herbicide MODIFY unite_id int(10);');
        $this->addSql('ALTER TABLE parcelle_phytosanitaire MODIFY unite_id int(10);');
        $this->addSql('ALTER TABLE parcelle_recoltes MODIFY unite_id int(10);');
        $this->addSql('ALTER TABLE parcelle_semences MODIFY unite_id int(10);');
        $this->addSql('ALTER TABLE parcelle_techniques MODIFY unite_id int(10);');

        $this->addSql('ALTER TABLE parcelle_amendements ADD FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits(id);');
        $this->addSql('ALTER TABLE parcelle_fumure_min ADD FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits(id);');
        $this->addSql('ALTER TABLE parcelle_fumure_org ADD FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits(id);');
        $this->addSql('ALTER TABLE parcelle_herbicide ADD FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits(id);');
        $this->addSql('ALTER TABLE parcelle_phytosanitaire ADD FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits(id);');
        $this->addSql('ALTER TABLE parcelle_recoltes ADD FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits(id);');
        $this->addSql('ALTER TABLE parcelle_semences ADD FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits(id);');
        $this->addSql('ALTER TABLE parcelle_techniques ADD FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits(id);');

        //tbl_unites_transformations");

        $this->addSql('ALTER TABLE unite_de_transformation DROP FOREIGN KEY FK_B32598E8EC4A74AB;');

        $this->addSql('ALTER TABLE tbl_unites_transformations CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_unites_transformations DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_unites_transformations CHANGE id_unite_transformation id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_unites_transformations ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_unites_transformations MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_unites_transformations ADD UNIQUE INDEX UNIQ_74445137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_unites_transformations MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_unites_transformations MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE unite_de_transformation ADD COLUMN old_unite_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE unite_de_transformation SET old_unite_id=unite_id;');
        $this->addSql('UPDATE unite_de_transformation SET unite_id=(SELECT tbl_unites_transformations.id FROM tbl_unites_transformations WHERE
                 tbl_unites_transformations.old_id=unite_de_transformation.old_unite_id);');

        $this->addSql('ALTER TABLE unite_de_transformation MODIFY unite_id int(10);');
        $this->addSql('ALTER TABLE unite_de_transformation ADD FOREIGN KEY (unite_id) REFERENCES tbl_unites_transformations(id);');

        //tbl_utilites");

        $this->addSql('Alter TABLE formations_recu DROP FOREIGN KEY FK_9B1128343FCAAC31;');
        $this->addSql('ALTER TABLE tbl_utilites CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_utilites CHANGE id_utilite id int NOT NULL ;');

        $this->addSql('ALTER TABLE tbl_utilites DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_utilites ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_utilites MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_utilites ADD UNIQUE INDEX UNIQ_79EF5111194D4258 (id);');
        $this->addSql('ALTER TABLE tbl_utilites MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_utilites MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE formations_recu ADD COLUMN old_utilite char(36) DEFAULT NULL;');
        $this->addSql('UPDATE formations_recu SET old_utilite=niveau_utilite_id;');
        $this->addSql('UPDATE formations_recu SET niveau_utilite_id=(SELECT tbl_utilites.id FROM tbl_utilites WHERE
                 tbl_utilites.old_id=formations_recu.old_utilite);');

        $this->addSql('ALTER TABLE formations_recu MODIFY niveau_utilite_id int(10);');
        $this->addSql('ALTER TABLE formations_recu ADD FOREIGN KEY (niveau_utilite_id) REFERENCES tbl_utilites(id);');
    }
}
