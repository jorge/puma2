<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171128140827 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE tbl_type_creancier');
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        // ne pas mettre dans config.synchro_entities : créer une migration qui ajoute les éléments
        $this->addSql("CREATE TABLE tbl_type_creancier (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            creancier VARCHAR(100) NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(255) NOT NULL,
            UNIQUE INDEX UNIQ_BAAF64FE99A26E03 (creancier),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
        // this up() migration is auto-generated, please modify it to your needs
    }
}
