<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180523124826 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE animaux_production DROP COLUMN unite;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE animaux_production ADD unite VARCHAR(30) DEFAULT NULL;');
    }
}
