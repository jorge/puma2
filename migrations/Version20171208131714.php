<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171208131714 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function up(Schema $schema): void
    {
        /* insertion des données sur table tbl_type_creancier */
        $this->addSql("INSERT tbl_type_creancier (id,creancier,cdate,udate,utilisateur) VALUES
                        ('a623ecd5-d5e8-11e7-b7b8-b8ca3a965972','IMF','2017-11-30 16:08:05','2017-11-30 16:08:05','jorge'),
                        ('bd5d21c3-d5e8-11e7-b7b8-b8ca3a965972','Commerçants','2017-11-30 16:08:44','2017-11-30 16:08:44','jorge'),
                        ('e53180d0-d5e8-11e7-b7b8-b8ca3a965972','Parents','2017-11-30 16:09:51','2017-11-30 16:09:51','jorge'),
                        ('f06d509e-d5e8-11e7-b7b8-b8ca3a965972','Voisins','2017-11-30 16:10:10','2017-11-30 16:10:10','jorge');");
        /* insertion des données sur la table tbl_antierosives */
        $this->addSql("INSERT INTO tbl_antierosives (id,antierosive,cdate,udate,utilisateur,dossier) VALUES
                        ('0dd0c430-d8d1-11e7-b875-b8ca3a965972','Haies','2017-12-04 08:56:45','2017-12-04 08:56:45','jorge','000'),
                        ('27342cf8-d8d1-11e7-b875-b8ca3a965972','Fossés antiérosifs','2017-12-04 08:57:28','2017-12-04 08:57:28','jorge','000'),
                        ('5b6954d7-d8cf-11e7-b875-b8ca3a965972','Associations de cultures','2017-12-04 08:44:36','2017-12-04 08:44:36','jorge','000'),
                        ('c22943fe-d8d0-11e7-b875-b8ca3a965972','Plantes de couverture','2017-12-04 08:54:38','2017-12-04 08:54:38','jorge','000'),
                        ('d70f08be-d8d0-11e7-b875-b8ca3a965972','Paillage','2017-12-04 08:55:13','2017-12-04 08:55:13','jorge','000'),
                        ('f7082f06-d8d0-11e7-b875-b8ca3a965972','Plants agroforestiers','2017-12-04 08:56:07','2017-12-04 08:56:07','jorge','000');");
    }
}
