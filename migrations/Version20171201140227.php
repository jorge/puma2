<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171201140227 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        /* debut pour une proposition de rectification du champ annee dans les tables */
    //        $strSql="SELECT DISTINCT TABLE_NAME, COLUMN_NAME
    //                       FROM INFORMATION_SCHEMA.COLUMNS
    //                       WHERE column_name = 'annee'
    //                        AND TABLE_SCHEMA='puma_03';";

        /*
        $this->addSql("UPDATE animaux SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE animaux_structure SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE antierosives SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE batiments SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE commercialisation_produits SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE contraintes_developpement SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE cotisations SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE depenses_menage SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE dettes SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE engrais_demande SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE exploitation_charges SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE filiere_commercialisation SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE filieres SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE formations_besoin SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE formations_recu SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE habitat SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE mo_familiale_externe SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE mo_familiale_interne SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE moyens_transport SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE outillage SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE parcelle_historiques SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE personne_details SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE phyto_demande SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE securite_alimentaire SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE semence_demandee SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE services_besoin SET annee=2012 WHERE annee<2015;");
        $this->addSql("UPDATE suivi_organisations SET annee=2012 WHERE annee<2015;");
         */

        // this up() migration is auto-generated, please modify it to your needs
    }
}
