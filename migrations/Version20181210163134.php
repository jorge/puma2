<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181210163134 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_decoupage_admin DROP COLUMN old_code;');
        $this->addSql('ALTER TABLE tbl_organisations DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_unite_filieres DROP COLUMN id_unite_filiere;');
        $this->addSql('ALTER TABLE suivi_organisations DROP COLUMN old_id_suivi;');
        $this->addSql('ALTER TABLE unite_de_transformation DROP COLUMN id_unite_transformation;');
        $this->addSql('ALTER TABLE tbl_utilites DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_unites_transformations DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_unite_produits DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_unite_mo DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_types_elevage DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_type_travail DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_type_garantie DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_type_depense DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_type_creancier DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_type_commercialisation DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_travail_type DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_traitements DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_themes_formations DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_terre_protection DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_situation DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_services_payants DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_services_aux_membres DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_services DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_semences DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_saisons DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_propriete DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_professions DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_produits DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_produit_phyto DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_prestations_services DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_pente DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_organisations DROP COLUMN old_level_id;');
        $this->addSql('ALTER TABLE tbl_organisation_level DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_organisateurs DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_moyen_transport DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_mode_culture DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_mo_type DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_labour_animaux DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_institutions_financieres DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_infrastructures DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_fumure_org DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_fumure_min DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_fournisseurs DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_filieres DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_exposition DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_etat_civil DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_entretiens DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_engrais DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_decoupage_admin_level DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_decoupage_admin DROP COLUMN old_level_id;');
        $this->addSql('ALTER TABLE tbl_cultures DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_cultures DROP COLUMN old_category_id;');
        $this->addSql('ALTER TABLE tbl_credit_objet DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_contraints DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_categories_agricoles DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_antierosives DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_animaux_technique DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_animaux_production DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_amendements DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_alimentation DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_aleas DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_activites_communautaires DROP COLUMN old_id;');
        $this->addSql('ALTER TABLE tbl_achat_agricoles DROP COLUMN old_id;');
    }
}
