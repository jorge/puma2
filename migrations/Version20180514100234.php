<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180514100234 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        /* ajout de champs nombre_autres, membres_menage dans exploitations */
        $this->addSql('ALTER TABLE exploitations ADD nombre_autres SMALLINT DEFAULT NULL,
                            ADD membres_menage SMALLINT DEFAULT NULL;');

        /* Ajout de champ membre_menage dans personnes */
        $this->addSql("ALTER TABLE personnes ADD membre_menage TINYINT(1) DEFAULT '1' NOT NULL;");
    }
}
