<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200131183127 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exploitation_commande_engrais DROP FOREIGN KEY FK_92A0B864563C4F47;');
        $this->addSql('ALTER TABLE exploitation_commande_engrais CHANGE quantite quantite NUMERIC(10, 0) NOT NULL;');
        $this->addSql('ALTER TABLE exploitation_commande_engrais ADD CONSTRAINT FK_92A0B864563C4F47 FOREIGN KEY (engrais_id) REFERENCES tbl_operation_culturale_intrant (id);');
    }
}
