<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180518090430 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM tbl_alimentation WHERE id='e24ac3d3-5a79-11e8-b22b-b8ca3a965972';");
    }

    public function up(Schema $schema): void
    {
        /* mise à jour de la tbl_alimentation */

        $this->addSql("UPDATE tbl_alimentation SET alimentation='Achat des céréales' WHERE id='782bad1f-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("UPDATE tbl_alimentation SET alimentation='Achat des légumineuses' WHERE id='782baf0d-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("UPDATE tbl_alimentation SET alimentation='Achat des tubercules' WHERE id='782baf90-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("UPDATE tbl_alimentation SET alimentation='Achat des oléagineux' WHERE id='782bafd0-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("UPDATE tbl_alimentation SET alimentation='Achat des plantes stimulantes' WHERE id='782bb007-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("UPDATE tbl_alimentation SET alimentation='Achat des légumes' WHERE id='782bb041-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("UPDATE tbl_alimentation SET alimentation='Achat des fruits' WHERE id='782bb07a-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("INSERT INTO tbl_alimentation (`id`,`alimentation`,`cdate`,`udate`,`utilisateur`) VALUES ('e24ac3d3-5a79-11e8-b22b-b8ca3a965972', 'Achat autres', '2018-05-18 09:00:16', '2018-05-18 09:00:16', 'jorge');");
    }
}
