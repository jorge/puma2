<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190211132339 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelle_operation_culturale DROP FOREIGN KEY FK_123C9057C2C4F051;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale DROP FOREIGN KEY FK_POCCB02BABEF01C2;');
        $this->addSql('DROP INDEX FK_123C9057C2C4F051 ON parcelle_operation_culturale;');
        $this->addSql('DROP INDEX IDX_POCCB02BABEF01C2 ON parcelle_operation_culturale;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale DROP recolte_id, DROP produit_secondaire_id, DROP quantite_recoltee_secondaire, CHANGE quantite_utilisee quantite_utilisee INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale RENAME INDEX fk_123c9057b108249d TO IDX_123C9057B108249D;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale RENAME INDEX idx_poccb02bb6ffc1d4 TO IDX_123C9057B6FFC1D4;');
    }
}
