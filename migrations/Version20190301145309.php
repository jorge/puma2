<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190301145309 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE
            parcelle_plan_saison_tblcultures,
            demande_credit,
            exploitation_commande_aliment_betails ,
            exploitation_commande_engrais ,
            exploitation_commande_phytosanitaires ,
            exploitation_commande_semences,
            exploitation_livraisons,
            parcelle_plan_saison,
            exploitation_plan_saison ;');

        $this->addSql('DROP TABLE
            tbl_alimentation_betail,
            tbl_variete_produit,
            tbl_variete_semences;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE demande_credit (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_plan_saison_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            montant NUMERIC(10, 0) NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(255) NOT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            INDEX IDX_5E8528112852355 (exploitation_plan_saison_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE exploitation_commande_aliment_betails (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_plan_saison_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            aliment_id INT DEFAULT NULL,
            quantite NUMERIC(18, 2) DEFAULT NULL,
            periode INT NOT NULL, radie TINYINT(1) DEFAULT '0' NOT NULL,
            cdate DATETIME NOT NULL,
            udate DATETIME NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            INDEX IDX_4943CF142852355 (exploitation_plan_saison_id),
            INDEX IDX_4943CF14415B9F11 (aliment_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE exploitation_commande_engrais (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_plan_saison_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            engrais_id INT DEFAULT NULL,
            quantite NUMERIC(18, 2) DEFAULT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            cdate DATETIME NOT NULL,
            udate DATETIME NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            INDEX IDX_92A0B8642852355 (exploitation_plan_saison_id),
            INDEX IDX_92A0B864563C4F47 (engrais_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE exploitation_commande_phytosanitaires (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_plan_saison_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            phytosanitaire_id INT DEFAULT NULL,
            quantite NUMERIC(18, 2)DEFAULT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            cdate DATETIME NOT NULL,
            udate DATETIME NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            INDEX IDX_7187311F2852355 (exploitation_plan_saison_id),
            INDEX IDX_7187311F8CCDE425 (phytosanitaire_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE exploitation_commande_semences (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_plan_saison_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            culture_id INT DEFAULT NULL,
            qualite_semence_id INT DEFAULT NULL,
            unite_id INT DEFAULT NULL,
            variete VARCHAR(30) NOT NULL,
            quantite NUMERIC(10, 2) NOT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            cdate DATETIME NOT NULL,
            udate DATETIME NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            INDEX IDX_D3A7EE072852355 (exploitation_plan_saison_id),
            INDEX IDX_D3A7EE07B108249D (culture_id),
            INDEX IDX_D3A7EE07A2D63F77 (qualite_semence_id),
            INDEX IDX_D3A7EE07EC4A74AB (unite_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE exploitation_livraisons (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_plan_saison_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            produit_id INT DEFAULT NULL,
            variete VARCHAR(30) DEFAULT NULL,
            quantite NUMERIC(18, 2) DEFAULT NULL,
            periode VARCHAR(30) DEFAULT NULL,
            avance INT NOT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            cdate DATETIME NOT NULL,
            udate DATETIME NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            INDEX IDX_753C9162852355 (exploitation_plan_saison_id),
            INDEX IDX_753C916F347EFB (produit_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE exploitation_plan_saison (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            saison_plan_id INT DEFAULT NULL,
            annee VARCHAR(4) DEFAULT NULL,
            annee_plan VARCHAR(4) DEFAULT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            cdate DATETIME NOT NULL, udate DATETIME NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            INDEX IDX_BA78DA6CD967A16D (exploitation_id),
            INDEX IDX_BA78DA6CB55B7AD (saison_plan_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE parcelle_plan_saison (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_plan_saison_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            parcelle_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            surface_prevue NUMERIC(18, 2) DEFAULT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            cdate DATETIME NOT NULL,
            udate DATETIME NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            INDEX IDX_307D5B2A2852355 (exploitation_plan_saison_id),
            INDEX IDX_307D5B2A4433ED66 (parcelle_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE parcelle_plan_saison_tblcultures (
            parcelle_plan_saison_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tblcultures_id INT NOT NULL,
            INDEX IDX_D9D9A19324608F28 (parcelle_plan_saison_id),
            INDEX IDX_D9D9A1936376E011 (tblcultures_id),
            PRIMARY KEY(parcelle_plan_saison_id, tblcultures_id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE tbl_alimentation_betail (
            id INT AUTO_INCREMENT NOT NULL,
            unite_id INT DEFAULT NULL,
            nom VARCHAR(100) NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            INDEX IDX_C6508682EC4A74AB (unite_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
        /*
               $this->addSql("CREATE TABLE tbl_herbicides (
                   id INT AUTO_INCREMENT NOT NULL,
                   herbicide VARCHAR(100) NOT NULL,
                   cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                   udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                   utilisateur VARCHAR(255) NOT NULL,
                   UNIQUE INDEX UNIQ_854E5EB7618C7094 (herbicide),
                   PRIMARY KEY(id))
                   DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
         */
        $this->addSql("CREATE TABLE tbl_variete_produit (
                id INT AUTO_INCREMENT NOT NULL,
                produit_id INT DEFAULT NULL,
                unite_id INT DEFAULT NULL,
                nom VARCHAR(100) NOT NULL,
                cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                utilisateur VARCHAR(30) NOT NULL,
                radie TINYINT(1) DEFAULT '0' NOT NULL,
                INDEX IDX_A3C2A07EF347EFB (produit_id),
                INDEX IDX_A3C2A07EEC4A74AB (unite_id),
                PRIMARY KEY(id))
                DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE tbl_variete_semences (
            id INT AUTO_INCREMENT NOT NULL,
            culture_id INT DEFAULT NULL,
            unite_id INT DEFAULT NULL ,
            nom VARCHAR(100) NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            radie TINYINT(1) DEFAULT '0' NOT NULL,
            INDEX IDX_29180AD0B108249D(culture_id),
            INDEX IDX_29180AD0EC4A74AB (unite_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE demande_credit
            ADD CONSTRAINT FK_5E8528112852355 FOREIGN KEY (exploitation_plan_saison_id)
            REFERENCES exploitation_plan_saison (id);');

        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails
            ADD CONSTRAINT FK_4943CF142852355 FOREIGN KEY (exploitation_plan_saison_id)
            REFERENCES exploitation_plan_saison (id);');

        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails
            ADD CONSTRAINT FK_4943CF14415B9F11 FOREIGN KEY (aliment_id)
            REFERENCES tbl_alimentation (id);');

        $this->addSql('ALTER TABLE exploitation_commande_engrais
            ADD CONSTRAINT FK_92A0B8642852355 FOREIGN KEY (exploitation_plan_saison_id)
            REFERENCES exploitation_plan_saison (id);');

        $this->addSql('ALTER TABLE exploitation_commande_engrais
            ADD CONSTRAINT FK_92A0B864563C4F47 FOREIGN KEY (engrais_id)
            REFERENCES tbl_engrais (id);');

        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires
            ADD CONSTRAINT FK_7187311F2852355 FOREIGN KEY (exploitation_plan_saison_id)
            REFERENCES exploitation_plan_saison (id);');

        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires
            ADD CONSTRAINT FK_7187311F8CCDE425 FOREIGN KEY (phytosanitaire_id)
            REFERENCES tbl_produit_phyto (id);');

        $this->addSql('ALTER TABLE exploitation_commande_semences
            ADD CONSTRAINT FK_D3A7EE072852355 FOREIGN KEY (exploitation_plan_saison_id)
            REFERENCES exploitation_plan_saison(id);');

        $this->addSql('ALTER TABLE exploitation_commande_semences
            ADD CONSTRAINT FK_D3A7EE07B108249D FOREIGN KEY (culture_id) REFERENCES tbl_cultures (id);');

        $this->addSql('ALTER TABLE exploitation_commande_semences
            ADD CONSTRAINT FK_D3A7EE07A2D63F77 FOREIGN KEY (qualite_semence_id) REFERENCES tbl_semences (id);');

        $this->addSql('ALTER TABLE exploitation_commande_semences
            ADD CONSTRAINT FK_D3A7EE07EC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');

        $this->addSql('ALTER TABLE exploitation_livraisons
            ADD CONSTRAINT FK_753C9162852355 FOREIGN KEY (exploitation_plan_saison_id) REFERENCES exploitation_plan_saison (id);');

        $this->addSql('ALTER TABLE exploitation_livraisons
            ADD CONSTRAINT FK_753C916F347EFB FOREIGN KEY (produit_id) REFERENCES tbl_produits (id);');

        $this->addSql('ALTER TABLE exploitation_plan_saison
            ADD CONSTRAINT FK_BA78DA6CD967A16D FOREIGN KEY (exploitation_id) REFERENCES exploitations (id);');

        $this->addSql('ALTER TABLE exploitation_plan_saison
            ADD CONSTRAINT FK_BA78DA6CB55B7AD FOREIGN KEY (saison_plan_id) REFERENCES tbl_saisons (id);');

        $this->addSql('ALTER TABLE parcelle_plan_saison
            ADD CONSTRAINT FK_307D5B2A2852355 FOREIGN KEY (exploitation_plan_saison_id) REFERENCES exploitation_plan_saison (id);');

        $this->addSql('ALTER TABLE parcelle_plan_saison
            ADD CONSTRAINT FK_307D5B2A4433ED66 FOREIGN KEY (parcelle_id) REFERENCES parcelles (id);');

        $this->addSql('ALTER TABLE parcelle_plan_saison_tblcultures
            ADD CONSTRAINT FK_D9D9A19324608F28 FOREIGN KEY (parcelle_plan_saison_id)
            REFERENCES parcelle_plan_saison (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE parcelle_plan_saison_tblcultures
            ADD CONSTRAINT FK_D9D9A1936376E011 FOREIGN KEY (tblcultures_id) REFERENCES tbl_cultures (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE tbl_alimentation_betail
            ADD CONSTRAINT FK_C6508682EC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');

        $this->addSql('ALTER TABLE tbl_variete_produit
            ADD CONSTRAINT FK_A3C2A07EF347EFB FOREIGN KEY (produit_id) REFERENCES tbl_produits (id);');

        $this->addSql('ALTER TABLE tbl_variete_produit
            ADD CONSTRAINT FK_A3C2A07EEC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');

        $this->addSql('ALTER TABLE tbl_variete_semences
            ADD CONSTRAINT FK_29180AD0B108249D FOREIGN KEY (culture_id) REFERENCES tbl_cultures (id);');

        $this->addSql('ALTER TABLE tbl_variete_semences
            ADD CONSTRAINT FK_29180AD0EC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');
    }
}
