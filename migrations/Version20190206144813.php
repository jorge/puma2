<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190206144813 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP TABLE parcelle_amendements;');
        $this->addSql('DROP TABLE parcelle_fumure_min;');
        $this->addSql('DROP TABLE parcelle_fumure_org;');
        $this->addSql('DROP TABLE parcelle_herbicide;');
        $this->addSql('DROP TABLE parcelle_phytosanitaire;');
        $this->addSql('DROP TABLE parcelle_recoltes;');
        $this->addSql('DROP TABLE parcelle_semences;');
        $this->addSql('DROP TABLE parcelle_techniques;');
    }
}
