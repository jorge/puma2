<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180524090615 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE commercialisation_produits DROP COLUMN unite;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE commercialisation_produits ADD `unite` VARCHAR( 32 ) DEFAULT NULL;');
    }
}
