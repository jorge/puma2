<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171127173550 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE mo_familiale_interne DROP FOREIGN KEY FK_5FE9A610F3FDA88C;');
        $this->addSql('ALTER TABLE mo_familiale_interne CHANGE membres membres VARCHAR(255) DEFAULT NULL;');
        $this->addSql('UPDATE mo_familiale_interne SET type_travail_id=NULL;');
        $this->addSql('ALTER TABLE mo_familiale_interne ADD CONSTRAINT FK_5FE9A610F3FDA88C FOREIGN KEY (type_travail_id) REFERENCES tbl_travail_type (id);');
    }
}
