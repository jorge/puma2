<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180522120753 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        /* effacer la table */
        $this->addSql('DROP TABLE IF EXISTS tbl_animaux_production;');
    }

    public function up(Schema $schema): void
    {
        /* creation table tbl_animaux_production  */
        $this->addSql("CREATE TABLE tbl_animaux_production (id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            production VARCHAR(100) NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(255) NOT NULL,
            UNIQUE INDEX UNIQ_5698FFC2D3EDB1E0 (production),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
    }
}
