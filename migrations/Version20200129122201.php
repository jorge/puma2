<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200129122201 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return 'Changing table for link intrant with op-cultu type';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD operation_culturale_type_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD CONSTRAINT FK_3129BF6C640543F3 FOREIGN KEY (operation_culturale_type_id) REFERENCES tbl_operation_culturale_type (id);');
        $this->addSql('CREATE INDEX IDX_3129BF6C640543F3 ON tbl_operation_culturale_intrant (operation_culturale_type_id);');

        $this->addSql('ALTER TABLE tbl_operation_culturale_type CHANGE utilisateur utilisateur VARCHAR(255) NOT NULL;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_type ADD CONSTRAINT FK_9F54E47CEC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);');
        // $this->addSql("ALTER TABLE tbl_operation_culturale_type RENAME INDEX idx_4625333391c37da8 TO IDX_9F54E47CEC4A74AB;");

        $this->addSql('ALTER TABLE tbl_operation_culturale_type_category CHANGE utilisateur utilisateur VARCHAR(255) NOT NULL;');
    }
}
