<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181213132836 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE animaux_tbl_alimentation DROP COLUMN cdate;');
        $this->addSql('ALTER TABLE animaux_tbl_alimentation DROP COLUMN udate;');
        $this->addSql('ALTER TABLE animaux_tbl_produits DROP COLUMN cdate;');
        $this->addSql('ALTER TABLE animaux_tbl_produits DROP COLUMN udate;');
    }

    public function up(Schema $schema): void
    {
        // recréation tbl_alimentation
        $this->addSql('DELETE FROM animaux_alimentation;');
        $this->addSql('DELETE FROM tbl_alimentation;');
        $this->addSql('ALTER TABLE tbl_alimentation AUTO_INCREMENT = 1;');
        $this->addSql("INSERT INTO tbl_alimentation (id, alimentation,utilisateur) VALUES
                     (1, 'cultures fourragères',''),
                     (2, 'pâtures',''),
                     (3, 'parcours - bords de chemin',''),
                     (4, 'arbres fourragers',''),
                     (5, 'sous-produit de culture',''),
                     (6, 'concentrés','');");
        // add champs udate et cdate aux tables animaux_tbl_alimentation animaux_tbl_produits
        $this->addSql('ALTER TABLE animaux_tbl_alimentation ADD COLUMN cdate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;');
        $this->addSql('ALTER TABLE animaux_tbl_alimentation ADD COLUMN udate datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;');
        $this->addSql('ALTER TABLE animaux_tbl_produits ADD COLUMN cdate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP;');
        $this->addSql('ALTER TABLE animaux_tbl_produits ADD COLUMN udate datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP;');
    }
}
