<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171121131142 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM tbl_type_depense WHERE id='62f77990-cb9e-11e7-9124-b8ca3a965972';");
        $this->addSql("UPDATE tbl_type_depense SET type_depense='Santé' WHERE id='792fbbf9-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("INSERT INTO tbl_type_depense (id, type_depense, utilisateur) VALUES ('792fbace-2698-11e7-98c8-b8ca3a965972', 'Habillement', 'init script');");
    }

    public function up(Schema $schema): void
    {
        // a faire car tbl_type_depense ne se trouve pas dans le tableau synchro_entities de la config
        $this->addSql("INSERT INTO tbl_type_depense (id, type_depense, utilisateur) VALUES ('62f77990-cb9e-11e7-9124-b8ca3a965972', 'Alimentaire', 'Jorge');");
        $this->addSql("UPDATE tbl_type_depense SET type_depense='Santé et habillement' WHERE id='792fbbf9-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("UPDATE `depenses_menage` SET type_depense_id='792fbbf9-2698-11e7-98c8-b8ca3a965972' WHERE `type_depense_id`='792fbace-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("DELETE FROM tbl_type_depense WHERE id='792fbace-2698-11e7-98c8-b8ca3a965972';");
    }
}
