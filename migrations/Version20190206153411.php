<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190206153411 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas DROP FOREIGN KEY FK_5555515A9JORGE;');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas DROP FOREIGN KEY FK_6666654A719501;');
        $this->addSql("ALTER TABLE parcelle_historiques_tbl_aleas ADD culture_id INT DEFAULT NULL, CHANGE parcelle_historique_id parcelle_historique_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', CHANGE aleas_id aleas_id INT DEFAULT NULL, CHANGE utilisateur utilisateur VARCHAR(255) NOT NULL;");
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas ADD CONSTRAINT FK_C41101B108249D FOREIGN KEY (culture_id) REFERENCES tbl_cultures (id);');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas ADD CONSTRAINT FK_C4110167FFE94B FOREIGN KEY (parcelle_historique_id) REFERENCES parcelle_historiques (id);');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas ADD CONSTRAINT FK_C4110129948A99 FOREIGN KEY (aleas_id) REFERENCES tbl_aleas (id);');
        $this->addSql('CREATE INDEX IDX_C41101B108249D ON parcelle_historiques_tbl_aleas (culture_id);');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas RENAME INDEX idx_5555515a9jorge TO IDX_C4110167FFE94B;');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas RENAME INDEX idx_66666154a719501 TO IDX_C4110129948A99;');
    }
}
