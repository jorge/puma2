<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180518100113 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql("UPDATE tbl_produits SET produit='Vifs', udate='2018-05-18 09:49:02', utilisateur='jorge' WHERE id='78e21944-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("DELETE FROM tbl_produits WHERE id = 'b217ae61-5a80-11e8-b22b-b8ca3a965972';");
        $this->addSql("DELETE FROM tbl_produits WHERE id = 'b21fae0b-5a80-11e8-b22b-b8ca3a965972';");
        $this->addSql("DELETE FROM tbl_produits WHERE id = 'b2267b5e-5a80-11e8-b22b-b8ca3a965972';");
        $this->addSql("DELETE FROM tbl_produits WHERE id = 'b2302500-5a80-11e8-b22b-b8ca3a965972';");
    }

    public function up(Schema $schema): void
    {
        /* mise à jour tbl_produits proposition Richard */
        $this->addSql("UPDATE tbl_produits SET produit='Vifs adultes', udate='2018-05-18 09:49:02', utilisateur='jorge' WHERE id='78e21944-2698-11e7-98c8-b8ca3a965972';");
        $this->addSql("INSERT INTO tbl_produits (`id`,`culture_id`,`produit`,`cdate`,`udate`,`utilisateur`) VALUES ('b217ae61-5a80-11e8-b22b-b8ca3a965972','78521105-2698-11e7-98c8-b8ca3a965972','Veaux','2018-05-18 09:49:02','2018-05-18 09:49:02','jorge');");
        $this->addSql("INSERT INTO tbl_produits (`id`,`culture_id`,`produit`,`cdate`,`udate`,`utilisateur`) VALUES ('b21fae0b-5a80-11e8-b22b-b8ca3a965972','78521105-2698-11e7-98c8-b8ca3a965972','Taurillons','2018-05-18 09:49:02','2018-05-18 09:49:02','jorge');");
        $this->addSql("INSERT INTO tbl_produits (`id`,`culture_id`,`produit`,`cdate`,`udate`,`utilisateur`) VALUES ('b2267b5e-5a80-11e8-b22b-b8ca3a965972','78521105-2698-11e7-98c8-b8ca3a965972','Fumier','2018-05-18 09:49:02','2018-05-18 09:49:02','jorge');");
        $this->addSql("INSERT INTO tbl_produits (`id`,`culture_id`,`produit`,`cdate`,`udate`,`utilisateur`) VALUES ('b2302500-5a80-11e8-b22b-b8ca3a965972','78521105-2698-11e7-98c8-b8ca3a965972','Services de traction bovine','2018-05-18 09:49:02','2018-05-18 09:49:02','jorge');");
    }
}
