<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180309085816 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE personnes CHANGE radie radie INT(1) DEFAULT 0;');
        $this->addSql('ALTER TABLE personne_details CHANGE radie radie INT(1) DEFAULT 0;');
        $this->addSql('ALTER TABLE exploitations CHANGE radie radie INT(1) DEFAULT 0;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE personnes CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE personne_details CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE exploitations CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL;");
    }
}
