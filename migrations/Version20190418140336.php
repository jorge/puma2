<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190418140336 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant DROP FOREIGN KEY FK_3129BF6CB1B67A5B;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD CONSTRAINT FK_3129BF6CB1B67A5B
            FOREIGN KEY (operation_culturale_id) REFERENCES tbl_operation_culturale_type_category (id);');
    }
}
