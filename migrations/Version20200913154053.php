<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200913154053 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE exploitation_commande_plant_aff');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE exploitation_commande_plant_aff (
            id INT AUTO_INCREMENT NOT NULL,
            exploitation_plan_saison_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\',
            plant_id INT NOT NULL, nbr_plants INT NOT NULL,
            cdate DATETIME NOT NULL,
            utilisateur VARCHAR(60) NOT NULL,
            udate DATETIME NOT NULL,
            INDEX IDX_C3D022062852355 (exploitation_plan_saison_id),
            INDEX IDX_C3D022061D935652 (plant_id),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB');

        $this->addSql('ALTER TABLE exploitation_commande_plant_aff ADD CONSTRAINT FK_C3D022062852355 FOREIGN KEY (exploitation_plan_saison_id) REFERENCES exploitation_plan_saison (id)');

        $this->addSql('ALTER TABLE exploitation_commande_plant_aff ADD CONSTRAINT FK_C3D022061D935652 FOREIGN KEY (plant_id) REFERENCES tbl_plants_agro_fo_et_fruitiers (id)');
    }
}
