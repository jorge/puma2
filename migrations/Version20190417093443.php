<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190417093443 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE besoins_formations DROP old_formation_id;');
        $this->addSql('ALTER TABLE commercialisation_produits DROP id_commercialisation_produit, DROP old_produit_id, DROP old_saison_id, DROP old_type_commercialisation_id;');
        $this->addSql('ALTER TABLE contraintes_developpement DROP id_contrainte_developpement;');
        $this->addSql('ALTER TABLE depenses_menage DROP old_type_depense_id;');
        $this->addSql('ALTER TABLE engrais_demande DROP id_engrais_demande;');
        $this->addSql('ALTER TABLE equipement_agricole DROP id_equipement_agricole, DROP old_equipement;');
        $this->addSql('ALTER TABLE exploitation_charges DROP old_achat_id;');
        $this->addSql('ALTER TABLE filieres DROP id_filiere, DROP old_culture;');
        $this->addSql('ALTER TABLE formations_recu DROP id_formation, DROP old_organisateur_id, DROP old_theme_id, DROP old_utilite;');
        $this->addSql('ALTER TABLE mo_toutes_applis DROP old_mo_type_id, DROP old_traitement_id;');
        $this->addSql('ALTER TABLE mobilisations_communautaires DROP id_mobilisation_communautaire, DROP old_mobilisation;');
        $this->addSql('ALTER TABLE moyens_transport DROP id_moyen_transport, DROP old_type_id;');
        $this->addSql('ALTER TABLE outillage DROP old_equipement;');
        $this->addSql('ALTER TABLE parcelle_historiques DROP id_parcelle, DROP old_saison_id, DROP old_mode_culture_id;');
        $this->addSql('ALTER TABLE parcelles DROP id_parcelle;');
        $this->addSql('ALTER TABLE personne_details DROP id_suivi_exploitation_f0,DROP old_domaine, DROP old_profession_principale_id, DROP old_profession_secondaire_id;');
        $this->addSql('ALTER TABLE phyto_demande DROP id_phyto_demande, DROP old_phytosanitaire_id;');
        $this->addSql('ALTER TABLE prestation_de_services DROP id_prestation_de_services, DROP old_prestation_id;');
        $this->addSql('ALTER TABLE services_payants_aux_membres DROP FOREIGN KEY services_payants_aux_membres_ibfk_1;');
        $this->addSql('ALTER TABLE services_payants_aux_membres DROP id_services_payants_aux_membres, DROP old_service_payant_id;');
        $this->addSql('DROP INDEX UNIQ_E91E39A1F7384557 ON tbl_produits;');
        $this->addSql('ALTER TABLE tbl_produits DROP old_culture_id;');
    }
}
