<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190314160258 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE exercice;');
        $this->addSql('ALTER TABLE collecte DROP exercice_id;');
        $this->addSql('ALTER TABLE collecte
            ADD CONSTRAINT FK_55AE4A3DD967A16D FOREIGN KEY (exploitation_id)
            REFERENCES exploitations (id);');

        $this->addSql('ALTER TABLE collecte
    	       ADD INDEX IDX_55AE4A3DD967A16D (exploitation_id);');

        $this->addSql('ALTER TABLE collecte
       	    ADD UNIQUE INDEX unique_exploitation_annee
                (exploitation_id, annee_enquete);');
    }

    public function getDescription(): string
    {
        return 'Split collecte into exercice & collecte';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE exercice (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            annee SMALLINT NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(30) NOT NULL,
            INDEX IDX_E418C74DD967A16D (exploitation_id),
            UNIQUE INDEX unique_exploitation_annee (exploitation_id, annee),
            PRIMARY KEY(id))");

        $this->addSql('INSERT INTO
            exercice (id, exploitation_id, annee, utilisateur)
            (
                SELECT uuid(), exploitation_id, annee_enquete, utilisateur
                FROM collecte);
        ');

        $this->addSql('ALTER TABLE collecte
            DROP FOREIGN KEY FK_55AE4A3DD967A16D;');

        $this->addSql('DROP INDEX IDX_55AE4A3DD967A16D ON collecte;');

        $this->addSql("ALTER TABLE collecte
        	CHANGE enqueteur enqueteur VARCHAR(255),
            CHANGE utilisateur utilisateur VARCHAR(30) NOT NULL,
            CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL,
            ADD exercice_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)';");

        $this->addSql('UPDATE collecte c
            INNER JOIN exercice e
            ON c.annee_enquete=e.annee
                AND c.exploitation_id = e.exploitation_id
	        SET c.exercice_id=e.id;');

        /* /!\ pbm */
        $this->addSql('CREATE INDEX IDX_55AE4A3D89D40298
            ON collecte (exercice_id);');

        /* /!\ pbm */
        $this->addSql('ALTER TABLE collecte
            ADD CONSTRAINT FK_55AE4A3D89D40298 FOREIGN KEY (exercice_id)
                REFERENCES exercice (id);');

        $this->addSql('DROP INDEX unique_exploitation_annee ON collecte;');
    }
}
