<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181212205854 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // NOPE
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE animaux_labour DROP old_labour_id;');
        $this->addSql('ALTER TABLE animaux_alimentation DROP old_alimentation_id;');
        $this->addSql('ALTER TABLE animaux_structure DROP old_animaux_technique_id;');
        $this->addSql('ALTER TABLE animaux_production DROP old_production_id;');
        $this->addSql('ALTER TABLE animaux_produits DROP old_produit_id;');
    }
}
