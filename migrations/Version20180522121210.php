<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180522121210 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        /* effacer table animaux_labour */
        $this->addSql('DROP TABLE IF EXISTS animaux_labour;');
    }

    public function up(Schema $schema): void
    {
        /* creation table animaux_labour */
        $this->addSql("CREATE TABLE animaux_labour (id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            animaux_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            labour_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            quantite_mofam SMALLINT DEFAULT NULL,
            quantite_mosal SMALLINT DEFAULT NULL,
            annee VARCHAR(4) DEFAULT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(255) NOT NULL,
            INDEX IDX_D17D0995A9DAECAA (animaux_id),
            INDEX IDX_D17D0995CC903DF2 (labour_id),
            PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
    }
}
