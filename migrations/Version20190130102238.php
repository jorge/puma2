<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190130102238 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exploitations
            DROP latitude,
            DROP longitude,
            ADD id_exploitation BIGINT,
            ADD coordonnees_geographiques VARCHAR(100);');

        $this->addSql('ALTER TABLE parcelles
            DROP latitude,
            DROP longitude,
            ADD coordonees_geo VARCHAR(100),
            ADD old_exposition_id CHAR(36),
            ADD old_pente_id CHAR(36),
            ADD old_propriete_id CHAR(36),
            ADD old_situation_id CHAR(36);');
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE exploitations
            ADD latitude DOUBLE PRECISION DEFAULT NULL,
            ADD longitude DOUBLE PRECISION DEFAULT NULL,
            DROP id_exploitation,
            DROP coordonnees_geographiques;');

        $this->addSql('ALTER TABLE parcelles
            ADD latitude DOUBLE PRECISION DEFAULT NULL,
            ADD longitude DOUBLE PRECISION DEFAULT NULL,
            DROP coordonees_geo,
            DROP old_exposition_id,
            DROP old_pente_id,
            DROP old_propriete_id,
            DROP old_situation_id;');
    }
}
