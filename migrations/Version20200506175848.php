<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200506175848 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'Suite mise à jour de fiche "suivi coop" selon email richard';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE suivi_organisations
            ADD niveaux_reconnaissance_id INT DEFAULT NULL,
            ADD nbr_reunions_ag_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD nbr_reunions_ce_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD nbr_reunions_cs_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD nbr_alternance_cs_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD nbr_alternance_ce_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD classement_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD evolution_nbr_membres_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD maj_liste_de_membres_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD nbr_pv_manquants_ag_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD nbr_pv_manquants_ce_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD nbr_pv_manquants_cs_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD comptabilite_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD degre_prise_en_charge_ag_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD degre_prise_en_charge_ce_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD degre_prise_en_charge_cs_choix_simple VARCHAR(32) DEFAULT NULL,
            ADD services_aux_membres1_id INT DEFAULT NULL,
            ADD services_aux_membres2_id INT DEFAULT NULL,
            ADD services_aux_membres3_id INT DEFAULT NULL,
            ADD services_payes_par_membres1_id INT DEFAULT NULL,
            ADD services_payes_par_membres2_id INT DEFAULT NULL,
            ADD services_payes_par_membres3_id INT DEFAULT NULL,
            ADD services_payes_par_tiers1_id INT DEFAULT NULL,
            ADD services_payes_par_tiers2_id INT DEFAULT NULL,
            ADD services_payes_par_tiers3_id INT DEFAULT NULL,
            ADD nbr_credit_aux_membres_par_imfou_banque INT DEFAULT NULL,
            ADD nbr_personnel_admin INT NOT NULL,
            ADD nbr_personnel_technique INT NOT NULL,
            ADD nbr_personnel_appui INT NOT NULL;');

        $this->addSql('ALTER TABLE suivi_organisations
            CHANGE signatures_pour_sortie_fonds signatures_pour_sortie_fonds INT DEFAULT 0;');

        $this->addSql('DROP TABLE suivi_organisations_classement');
        $this->addSql('DROP TABLE suivi_organisations_maj_list_membre');
        $this->addSql('DROP TABLE suivi_organisations_comptabilite');
        $this->addSql('DROP TABLE suivi_organisations_besoins_institutionnels;');
        $this->addSql('DROP TABLE suivi_organisations_tbl_niveaux_reconnaissance');
        $this->addSql('DROP TABLE suivi_organisations_tbl_prestations_services');
        $this->addSql('DROP TABLE suivi_organisations_tbl_services_aux_membres');
        $this->addSql('DROP TABLE suivi_organisations_tbl_services_payants');

        $this->addSql('ALTER TABLE tbl_valeur_choix_simple
            ADD `display_order` INT NOT NULL,
            CHANGE propiete tag VARCHAR(255) NOT NULL,
            CHANGE id id VARCHAR(32) NOT NULL;');

        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B88B94C9C8 FOREIGN KEY (niveaux_reconnaissance_id) REFERENCES tbl_niveaux_reconnaissance (id);');

        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8E40DAD01 FOREIGN KEY (nbr_reunions_ag_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');

        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8ADE5D83D FOREIGN KEY (nbr_reunions_ce_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');

        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B83395AFE7 FOREIGN KEY (nbr_reunions_cs_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');

        $this->addSql('CREATE INDEX IDX_2FE520B8E40DAD01 ON suivi_organisations (nbr_reunions_ag_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B8ADE5D83D ON suivi_organisations (nbr_reunions_ce_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B83395AFE7 ON suivi_organisations (nbr_reunions_cs_choix_simple);');

        $this->addSql('ALTER TABLE suivi_organisations RENAME INDEX fk_2fe520b88b94c9c8 TO IDX_2FE520B88B94C9C8;');

        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8FBA5882B FOREIGN KEY (nbr_alternance_cs_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');

        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B865D5FFF1 FOREIGN KEY (nbr_alternance_ce_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');

        $this->addSql('CREATE INDEX IDX_2FE520B8FBA5882B ON suivi_organisations (nbr_alternance_cs_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B865D5FFF1 ON suivi_organisations (nbr_alternance_ce_choix_simple);');

        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8F3976E4 FOREIGN KEY (classement_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8D7BCEFF8 FOREIGN KEY (evolution_nbr_membres_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8E696648 FOREIGN KEY (maj_liste_de_membres_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8FD9B480C FOREIGN KEY (nbr_pv_manquants_ag_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8B4733D30 FOREIGN KEY (nbr_pv_manquants_ce_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B82A034AEA FOREIGN KEY (nbr_pv_manquants_cs_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8FC0353B2 FOREIGN KEY (comptabilite_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');

        $this->addSql('CREATE INDEX IDX_2FE520B8F3976E4 ON suivi_organisations (classement_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B8D7BCEFF8 ON suivi_organisations (evolution_nbr_membres_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B8E696648 ON suivi_organisations (maj_liste_de_membres_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B8FD9B480C ON suivi_organisations (nbr_pv_manquants_ag_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B8B4733D30 ON suivi_organisations (nbr_pv_manquants_ce_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B82A034AEA ON suivi_organisations (nbr_pv_manquants_cs_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B8FC0353B2 ON suivi_organisations (comptabilite_choix_simple);');

        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B89CB9B78E FOREIGN KEY (degre_prise_en_charge_ag_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8D551C2B2 FOREIGN KEY (degre_prise_en_charge_ce_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B84B21B568 FOREIGN KEY (degre_prise_en_charge_cs_choix_simple) REFERENCES tbl_valeur_choix_simple (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B852A0232A FOREIGN KEY (services_aux_membres1_id) REFERENCES tbl_services_aux_membres (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B840158CC4 FOREIGN KEY (services_aux_membres2_id) REFERENCES tbl_services_aux_membres (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8F8A9EBA1 FOREIGN KEY (services_aux_membres3_id) REFERENCES tbl_services_aux_membres (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8349DDACD FOREIGN KEY (services_payes_par_membres1_id) REFERENCES tbl_services_payants (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B826287523 FOREIGN KEY (services_payes_par_membres2_id) REFERENCES tbl_services_payants (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B89E941246 FOREIGN KEY (services_payes_par_membres3_id) REFERENCES tbl_services_payants (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B83367BA34 FOREIGN KEY (services_payes_par_tiers1_id) REFERENCES tbl_services_payants (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B821D215DA FOREIGN KEY (services_payes_par_tiers2_id) REFERENCES tbl_services_payants (id);');
        $this->addSql('ALTER TABLE suivi_organisations
            ADD CONSTRAINT FK_2FE520B8996E72BF FOREIGN KEY (services_payes_par_tiers3_id) REFERENCES tbl_services_payants (id);');

        $this->addSql('CREATE INDEX IDX_2FE520B89CB9B78E ON suivi_organisations (degre_prise_en_charge_ag_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B8D551C2B2 ON suivi_organisations (degre_prise_en_charge_ce_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B84B21B568 ON suivi_organisations (degre_prise_en_charge_cs_choix_simple);');
        $this->addSql('CREATE INDEX IDX_2FE520B852A0232A ON suivi_organisations (services_aux_membres1_id);');
        $this->addSql('CREATE INDEX IDX_2FE520B840158CC4 ON suivi_organisations (services_aux_membres2_id);');
        $this->addSql('CREATE INDEX IDX_2FE520B8F8A9EBA1 ON suivi_organisations (services_aux_membres3_id);');
        $this->addSql('CREATE INDEX IDX_2FE520B8349DDACD ON suivi_organisations (services_payes_par_membres1_id);');
        $this->addSql('CREATE INDEX IDX_2FE520B826287523 ON suivi_organisations (services_payes_par_membres2_id);');
        $this->addSql('CREATE INDEX IDX_2FE520B8996E72BF ON suivi_organisations (services_payes_par_tiers3_id);');
        $this->addSql('CREATE INDEX IDX_2FE520B89E941246 ON suivi_organisations (services_payes_par_membres3_id);');
        $this->addSql('CREATE INDEX IDX_2FE520B83367BA34 ON suivi_organisations (services_payes_par_tiers1_id);');
        $this->addSql('CREATE INDEX IDX_2FE520B821D215DA ON suivi_organisations (services_payes_par_tiers2_id);');

        $this->addSql('ALTER TABLE suivi_organisations
        DROP montant_financement,
        DROP participation_activites_parent,
        DROP besoins_intrans,
        DROP groupage_produits,
        DROP chaine_solidarite_elevage,
        DROP recouvrement_credits,
        DROP collecte_cotisations,
        DROP cotisations_annee_avant,
        DROP no_reunions_ag,
        DROP no_reunions_ce,
        DROP no_reunions_cs,
        DROP agrement_par_authorite,
        DROP alternance_cs_nbr,
        DROP alternance_ce_nbr,
        DROP reg_annu_pvreunions_ag,
        DROP reg_annu_pvreunions_ce,
        DROP reg_annu_pvreunions_cs,
        DROP evolution_nbr_membres,
        DROP agr,
        DROP agr_montant,
        DROP prestation_services_montant,
        DROP cotisations,
        DROP manuel_procedures,
        DROP doubles_signatures,
        DROP rapport_cotisation_agrtotal_produit,
        DROP degre_prise_en_charge_ag,
        DROP degre_prise_en_charge_ca,
        DROP degre_prise_en_charge_cs,
        DROP personnel_salarie;');
    }
}
