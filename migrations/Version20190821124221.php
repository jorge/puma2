<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190821124221 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE faux_doublons');
    }

    public function getDescription(): string
    {
        return 'Create table for entity FauxDoublons';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE faux_doublons (
            id INT AUTO_INCREMENT NOT NULL,
            exploitationIds LONGTEXT NOT NULL COMMENT '(DC2Type:simple_array)',
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
    }
}
