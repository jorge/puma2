<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190307133617 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exploitation_commande_semences
            ADD variete_id INT DEFAULT NULL, DROP variete;');
        $this->addSql('ALTER TABLE exploitation_commande_semences
            ADD CONSTRAINT FK_D3A7EE07620D5460
            FOREIGN KEY (variete_id) REFERENCES tbl_variete_semences (id);');
        $this->addSql('CREATE INDEX IDX_D3A7EE07620D5460 ON exploitation_commande_semences (variete_id);');
        $this->addSql('ALTER TABLE parcelle_plan_saison_tblcultures RENAME TO parcelle_plan_saison_tbl_cultures;');
        $this->addSql('ALTER TABLE parcelle_plan_saison_tbl_cultures CHANGE tblcultures_id tbl_cultures_id INT;');
    }
}
