<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170511182418 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE synchro_status DROP COLUMN localInitialized');
        $this->addSql('ALTER TABLE synchro_status CHANGE COLUMN localInitUdate localLastUdate DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE synchro_status ADD COLUMN localInitialized TINYINT(1) NOT NULL DEFAULT 0');
        $this->addSql('ALTER TABLE synchro_status CHANGE COLUMN localLastUdate localInitUdate DATETIME NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP');
    }
}
