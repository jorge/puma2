<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190424124728 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        throw new Exception('Pas de retour en arrière', 1);
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE exploitation_plan_saison ADD collecte_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)', CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql('ALTER TABLE exploitation_plan_saison ADD CONSTRAINT FK_BA78DA6C710A9AC6 FOREIGN KEY (collecte_id) REFERENCES collecte (id);');
        $this->addSql('CREATE INDEX IDX_BA78DA6C710A9AC6 ON exploitation_plan_saison (collecte_id);');
        $this->addSql("ALTER TABLE exploitation_plan_saison DROP annee, DROP annee_plan, CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL;");
    }
}
