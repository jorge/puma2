<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180313103056 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE collecte;');
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE collecte (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            exploitation_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            enqueteur VARCHAR(255),
            date_enquete DATE,
            annee_enquete SMALLINT NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(255),
            INDEX IDX_55AE4A3DD967A16D (exploitation_id),
            UNIQUE INDEX unique_exploitation_annee (exploitation_id, annee_enquete),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;");
    }
}
