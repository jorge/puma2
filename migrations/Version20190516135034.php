<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190516135034 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires DROP FOREIGN KEY FK_7187311FEC4A74AB;');
        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires DROP INDEX IDX_7187311FEC4A74AB;');
        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires DROP COLUMN unite_id');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
//        $this->addSql("ALTER TABLE exploitation_commande_phytosanitaires
//            ADD unite_id INT DEFAULT NULL, CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL;");
//        $this->addSql("ALTER TABLE exploitation_commande_phytosanitaires
//            ADD CONSTRAINT FK_7187311FEC4A74AB FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);");
//        $this->addSql("CREATE INDEX IDX_7187311FEC4A74AB ON exploitation_commande_phytosanitaires (unite_id);");
    }
}
