<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210120155822 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        throw new Exception('No return possible', 1);
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exploitations ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE cdate cdate DATETIME NOT NULL, CHANGE udate udate DATETIME NOT NULL');
        $this->addSql('ALTER TABLE exploitations ADD CONSTRAINT FK_A763A7DE61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitations ADD CONSTRAINT FK_A763A7DE7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_A763A7DE61220EA6 ON exploitations (creator_id)');
        $this->addSql('CREATE INDEX IDX_A763A7DE7E5A734A ON exploitations (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_decoupage_admin ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_decoupage_admin ADD CONSTRAINT FK_A24CB62361220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_decoupage_admin ADD CONSTRAINT FK_A24CB6237E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_A24CB62361220EA6 ON tbl_decoupage_admin (creator_id)');
        $this->addSql('CREATE INDEX IDX_A24CB6237E5A734A ON tbl_decoupage_admin (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_organisation_level ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_organisation_level ADD CONSTRAINT FK_977BE3E361220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_organisation_level ADD CONSTRAINT FK_977BE3E37E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_977BE3E361220EA6 ON tbl_organisation_level (creator_id)');
        $this->addSql('CREATE INDEX IDX_977BE3E37E5A734A ON tbl_organisation_level (last_editor_id)');

        $this->addSql('ALTER TABLE phyto_demande ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE phyto_demande ADD CONSTRAINT FK_5537CEA061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE phyto_demande ADD CONSTRAINT FK_5537CEA07E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_5537CEA061220EA6 ON phyto_demande (creator_id)');
        $this->addSql('CREATE INDEX IDX_5537CEA07E5A734A ON phyto_demande (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_organisations ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_organisations ADD CONSTRAINT FK_CE763CA761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_organisations ADD CONSTRAINT FK_CE763CA77E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_CE763CA761220EA6 ON tbl_organisations (creator_id)');
        $this->addSql('CREATE INDEX IDX_CE763CA77E5A734A ON tbl_organisations (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_etat_civil ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_etat_civil ADD CONSTRAINT FK_D10DCD5E61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_etat_civil ADD CONSTRAINT FK_D10DCD5E7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_D10DCD5E61220EA6 ON tbl_etat_civil (creator_id)');
        $this->addSql('CREATE INDEX IDX_D10DCD5E7E5A734A ON tbl_etat_civil (last_editor_id)');

        $this->addSql('ALTER TABLE pivot_exploitation_organisations ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE pivot_exploitation_organisations ADD CONSTRAINT FK_2E1F3C8761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE pivot_exploitation_organisations ADD CONSTRAINT FK_2E1F3C877E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_2E1F3C8761220EA6 ON pivot_exploitation_organisations (creator_id)');
        $this->addSql('CREATE INDEX IDX_2E1F3C877E5A734A ON pivot_exploitation_organisations (last_editor_id)');

        $this->addSql('ALTER TABLE mo_familiale_interne ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE mo_familiale_interne ADD CONSTRAINT FK_5FE9A61061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE mo_familiale_interne ADD CONSTRAINT FK_5FE9A6107E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_5FE9A61061220EA6 ON mo_familiale_interne (creator_id)');
        $this->addSql('CREATE INDEX IDX_5FE9A6107E5A734A ON mo_familiale_interne (last_editor_id)');

        $this->addSql('ALTER TABLE personne_details ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE domaine_id domaine_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE personne_details ADD CONSTRAINT FK_ACA9EF774272FC9F FOREIGN KEY (domaine_id) REFERENCES tbl_domaines_etudes_superieures (id)');
        $this->addSql('ALTER TABLE personne_details ADD CONSTRAINT FK_ACA9EF7761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE personne_details ADD CONSTRAINT FK_ACA9EF777E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_ACA9EF774272FC9F ON personne_details (domaine_id)');
        $this->addSql('CREATE INDEX IDX_ACA9EF7761220EA6 ON personne_details (creator_id)');
        $this->addSql('CREATE INDEX IDX_ACA9EF777E5A734A ON personne_details (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_themes_formations ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_themes_formations ADD CONSTRAINT FK_68CA9AAB61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_themes_formations ADD CONSTRAINT FK_68CA9AAB7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_68CA9AAB61220EA6 ON tbl_themes_formations (creator_id)');
        $this->addSql('CREATE INDEX IDX_68CA9AAB7E5A734A ON tbl_themes_formations (last_editor_id)');

        $this->addSql('ALTER TABLE pivot_personnes_organisations ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE pivot_personnes_organisations ADD CONSTRAINT FK_E8FB66A661220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE pivot_personnes_organisations ADD CONSTRAINT FK_E8FB66A67E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_E8FB66A661220EA6 ON pivot_personnes_organisations (creator_id)');
        $this->addSql('CREATE INDEX IDX_E8FB66A67E5A734A ON pivot_personnes_organisations (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_decoupage_admin_level ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_decoupage_admin_level ADD CONSTRAINT FK_553C42861220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_decoupage_admin_level ADD CONSTRAINT FK_553C4287E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_553C42861220EA6 ON tbl_decoupage_admin_level (creator_id)');
        $this->addSql('CREATE INDEX IDX_553C4287E5A734A ON tbl_decoupage_admin_level (last_editor_id)');

        $this->addSql('ALTER TABLE personnes ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE personnes ADD CONSTRAINT FK_2BB4FE2B61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE personnes ADD CONSTRAINT FK_2BB4FE2B7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_2BB4FE2B61220EA6 ON personnes (creator_id)');
        $this->addSql('CREATE INDEX IDX_2BB4FE2B7E5A734A ON personnes (last_editor_id)');

        $this->addSql('DROP INDEX UNIQ_79EF5137B94D4258 ON tbl_categories_agricoles');
        $this->addSql('ALTER TABLE tbl_categories_agricoles ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_categories_agricoles ADD CONSTRAINT FK_8D95004461220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_categories_agricoles ADD CONSTRAINT FK_8D9500447E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_8D95004461220EA6 ON tbl_categories_agricoles (creator_id)');
        $this->addSql('CREATE INDEX IDX_8D9500447E5A734A ON tbl_categories_agricoles (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_variete_produit ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_variete_produit ADD CONSTRAINT FK_A3C2A07E61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_variete_produit ADD CONSTRAINT FK_A3C2A07E7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_A3C2A07E61220EA6 ON tbl_variete_produit (creator_id)');
        $this->addSql('CREATE INDEX IDX_A3C2A07E7E5A734A ON tbl_variete_produit (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_aleas ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_aleas ADD CONSTRAINT FK_4612D8EF61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_aleas ADD CONSTRAINT FK_4612D8EF7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_4612D8EF61220EA6 ON tbl_aleas (creator_id)');
        $this->addSql('CREATE INDEX IDX_4612D8EF7E5A734A ON tbl_aleas (last_editor_id)');

        $this->addSql('ALTER TABLE exercice ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');

        $this->addSql('ALTER TABLE tbl_analyse_sol ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_analyse_sol ADD CONSTRAINT FK_B8E28D1061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_analyse_sol ADD CONSTRAINT FK_B8E28D107E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_B8E28D1061220EA6 ON tbl_analyse_sol (creator_id)');
        $this->addSql('CREATE INDEX IDX_B8E28D107E5A734A ON tbl_analyse_sol (last_editor_id)');

        $this->addSql('ALTER TABLE exploitation_livraisons ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE exploitation_livraisons ADD CONSTRAINT FK_753C91661220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitation_livraisons ADD CONSTRAINT FK_753C9167E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_753C91661220EA6 ON exploitation_livraisons (creator_id)');
        $this->addSql('CREATE INDEX IDX_753C9167E5A734A ON exploitation_livraisons (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_materiels_bureaux ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux ADD CONSTRAINT FK_484E768C61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux ADD CONSTRAINT FK_484E768C7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_484E768C61220EA6 ON tbl_materiels_bureaux (creator_id)');
        $this->addSql('CREATE INDEX IDX_484E768C7E5A734A ON tbl_materiels_bureaux (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_produit_phyto ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_produit_phyto ADD CONSTRAINT FK_2512594561220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_produit_phyto ADD CONSTRAINT FK_251259457E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_2512594561220EA6 ON tbl_produit_phyto (creator_id)');
        $this->addSql('CREATE INDEX IDX_251259457E5A734A ON tbl_produit_phyto (last_editor_id)');

        $this->addSql('ALTER TABLE cotisations ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE cotisations ADD CONSTRAINT FK_C51B351CD5C384C0 FOREIGN KEY (pivot_exploitation_organisation_id) REFERENCES pivot_exploitation_organisations (id)');
        $this->addSql('ALTER TABLE cotisations ADD CONSTRAINT FK_C51B351C61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE cotisations ADD CONSTRAINT FK_C51B351C7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C51B351C61220EA6 ON cotisations (creator_id)');
        $this->addSql('CREATE INDEX IDX_C51B351C7E5A734A ON cotisations (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_exposition ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_exposition ADD CONSTRAINT FK_D572B78261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_exposition ADD CONSTRAINT FK_D572B7827E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_D572B78261220EA6 ON tbl_exposition (creator_id)');
        $this->addSql('CREATE INDEX IDX_D572B7827E5A734A ON tbl_exposition (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_animaux_mouvements ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_animaux_mouvements ADD CONSTRAINT FK_5F41CD7E61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_animaux_mouvements ADD CONSTRAINT FK_5F41CD7E7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_5F41CD7E61220EA6 ON tbl_animaux_mouvements (creator_id)');
        $this->addSql('CREATE INDEX IDX_5F41CD7E7E5A734A ON tbl_animaux_mouvements (last_editor_id)');

        $this->addSQl('DROP INDEX IF EXISTS uniq_c7456213d73b9841 ON tbl_animaux_mouvements');
        $this->addSql('CREATE INDEX UNIQ_5F41CD7ED73B9841 ON tbl_animaux_mouvements (technique)');

        $this->addSql('ALTER TABLE commercialisation_produits ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE commercialisation_produits ADD CONSTRAINT FK_C8BFE32161220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE commercialisation_produits ADD CONSTRAINT FK_C8BFE3217E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C8BFE32161220EA6 ON commercialisation_produits (creator_id)');
        $this->addSql('CREATE INDEX IDX_C8BFE3217E5A734A ON commercialisation_produits (last_editor_id)');

        $this->addSql('ALTER TABLE suivi_organisations ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE suivi_organisations ADD CONSTRAINT FK_2FE520B861220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE suivi_organisations ADD CONSTRAINT FK_2FE520B87E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_2FE520B861220EA6 ON suivi_organisations (creator_id)');
        $this->addSql('CREATE INDEX IDX_2FE520B87E5A734A ON suivi_organisations (last_editor_id)');

        // $this->addSql('ALTER TABLE  RENAME INDEX fk_2fe520b88b94c9c8 TO ');

        $this->addSql('ALTER TABLE tbl_organisateurs ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE mobilisations_communautaires ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE mobilisation_id mobilisation_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE mobilisations_communautaires ADD CONSTRAINT FK_CFAB5402A4EA940F FOREIGN KEY (mobilisation_id) REFERENCES tbl_activites_communautaires (id)');
        $this->addSql('ALTER TABLE mobilisations_communautaires ADD CONSTRAINT FK_CFAB540261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE mobilisations_communautaires ADD CONSTRAINT FK_CFAB54027E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_CFAB540261220EA6 ON mobilisations_communautaires (creator_id)');
        $this->addSql('CREATE INDEX IDX_CFAB54027E5A734A ON mobilisations_communautaires (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures ADD CONSTRAINT FK_F497DCE561220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures ADD CONSTRAINT FK_F497DCE57E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_F497DCE561220EA6 ON tbl_domaines_etudes_superieures (creator_id)');
        $this->addSql('CREATE INDEX IDX_F497DCE57E5A734A ON tbl_domaines_etudes_superieures (last_editor_id)');

        $this->addSql('ALTER TABLE filieres ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE filiere_principale_id filiere_principale_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE filieres ADD CONSTRAINT FK_C97A115920C327B FOREIGN KEY (filiere_principale_id) REFERENCES tbl_cultures (id)');
        $this->addSql('ALTER TABLE filieres ADD CONSTRAINT FK_C97A11561220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE filieres ADD CONSTRAINT FK_C97A1157E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C97A11561220EA6 ON filieres (creator_id)');
        $this->addSql('CREATE INDEX IDX_C97A1157E5A734A ON filieres (last_editor_id)');

        $this->addSql('ALTER TABLE parcelles ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE parcelles ADD CONSTRAINT FK_4F15F60E61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE parcelles ADD CONSTRAINT FK_4F15F60E7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_4F15F60E61220EA6 ON parcelles (creator_id)');
        $this->addSql('CREATE INDEX IDX_4F15F60E7E5A734A ON parcelles (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_services_aux_membres ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_services_aux_membres ADD CONSTRAINT FK_9DDA99EE61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_services_aux_membres ADD CONSTRAINT FK_9DDA99EE7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_9DDA99EE61220EA6 ON tbl_services_aux_membres (creator_id)');
        $this->addSql('CREATE INDEX IDX_9DDA99EE7E5A734A ON tbl_services_aux_membres (last_editor_id)');
        $this->addSql('DROP INDEX UNIQ_74445137B94D4258 ON tbl_unites_transformations');

        $this->addSql('ALTER TABLE tbl_unites_transformations ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', CHANGE udate udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE tbl_unites_transformations ADD CONSTRAINT FK_9BFFF6C261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_unites_transformations ADD CONSTRAINT FK_9BFFF6C27E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_9BFFF6C261220EA6 ON tbl_unites_transformations (creator_id)');
        $this->addSql('CREATE INDEX IDX_9BFFF6C27E5A734A ON tbl_unites_transformations (last_editor_id)');

        $this->addSql('ALTER TABLE animaux_labour ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE animaux_labour ADD CONSTRAINT FK_D17D099561220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE animaux_labour ADD CONSTRAINT FK_D17D09957E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_D17D099561220EA6 ON animaux_labour (creator_id)');
        $this->addSql('CREATE INDEX IDX_D17D09957E5A734A ON animaux_labour (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_type_creancier ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_type_creancier ADD CONSTRAINT FK_BAAF64FE61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_type_creancier ADD CONSTRAINT FK_BAAF64FE7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_BAAF64FE61220EA6 ON tbl_type_creancier (creator_id)');
        $this->addSql('CREATE INDEX IDX_BAAF64FE7E5A734A ON tbl_type_creancier (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_professions ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_professions ADD CONSTRAINT FK_A8BB656261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_professions ADD CONSTRAINT FK_A8BB65627E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_A8BB656261220EA6 ON tbl_professions (creator_id)');
        $this->addSql('CREATE INDEX IDX_A8BB65627E5A734A ON tbl_professions (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_pente ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_pente ADD CONSTRAINT FK_A9AE95F361220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_pente ADD CONSTRAINT FK_A9AE95F37E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_A9AE95F361220EA6 ON tbl_pente (creator_id)');
        $this->addSql('CREATE INDEX IDX_A9AE95F37E5A734A ON tbl_pente (last_editor_id)');

        $this->addSql('ALTER TABLE parcelle_historique_analyse_sol ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE parcelle_historique_analyse_sol ADD CONSTRAINT FK_607123F61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE parcelle_historique_analyse_sol ADD CONSTRAINT FK_607123F7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_607123F61220EA6 ON parcelle_historique_analyse_sol (creator_id)');
        $this->addSql('CREATE INDEX IDX_607123F7E5A734A ON parcelle_historique_analyse_sol (last_editor_id)');
        $this->addSql('DROP INDEX UNIQ_79EF5137B94D4258 ON tbl_niveaux_reconnaissance');

        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance ADD CONSTRAINT FK_125927F761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance ADD CONSTRAINT FK_125927F77E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_125927F761220EA6 ON tbl_niveaux_reconnaissance (creator_id)');
        $this->addSql('CREATE INDEX IDX_125927F77E5A734A ON tbl_niveaux_reconnaissance (last_editor_id)');

        $this->addSql('ALTER TABLE outillage ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE type_id type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE outillage ADD CONSTRAINT FK_C5FC74D2C54C8C93 FOREIGN KEY (type_id) REFERENCES tbl_equipements_agricoles (id)');
        $this->addSql('ALTER TABLE outillage ADD CONSTRAINT FK_C5FC74D261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE outillage ADD CONSTRAINT FK_C5FC74D27E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C5FC74D261220EA6 ON outillage (creator_id)');
        $this->addSql('CREATE INDEX IDX_C5FC74D27E5A734A ON outillage (last_editor_id)');

        $this->addSql('ALTER TABLE parcelle_plan_saison ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE parcelle_plan_saison ADD CONSTRAINT FK_307D5B2A61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE parcelle_plan_saison ADD CONSTRAINT FK_307D5B2A7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_307D5B2A61220EA6 ON parcelle_plan_saison (creator_id)');
        $this->addSql('CREATE INDEX IDX_307D5B2A7E5A734A ON parcelle_plan_saison (last_editor_id)');

        $this->addSQl('DROP INDEX IF EXISTS idx_d9d9a19324608f28 ON parcelle_plan_saison_tbl_cultures');
        $this->addSql('CREATE INDEX IDX_B88597FE24608F28 ON parcelle_plan_saison_tbl_cultures (parcelle_plan_saison_id)');

        $this->addSQl('DROP INDEX IF EXISTS idx_d9d9a1936376e011 ON parcelle_plan_saison_tbl_cultures');
        $this->addSql('CREATE INDEX IDX_B88597FEEE1FBA60 ON parcelle_plan_saison_tbl_cultures (tbl_cultures_id)');

        $this->addSql('ALTER TABLE exploitation_charges ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE exploitation_charges ADD CONSTRAINT FK_65FD526C61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitation_charges ADD CONSTRAINT FK_65FD526C7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_65FD526C61220EA6 ON exploitation_charges (creator_id)');
        $this->addSql('CREATE INDEX IDX_65FD526C7E5A734A ON exploitation_charges (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_achat_agricoles ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_achat_agricoles ADD CONSTRAINT FK_BC3734D61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_achat_agricoles ADD CONSTRAINT FK_BC3734D7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_BC3734D61220EA6 ON tbl_achat_agricoles (creator_id)');
        $this->addSql('CREATE INDEX IDX_BC3734D7E5A734A ON tbl_achat_agricoles (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_produits ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_produits ADD CONSTRAINT FK_E91E39A161220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_produits ADD CONSTRAINT FK_E91E39A17E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_E91E39A161220EA6 ON tbl_produits (creator_id)');
        $this->addSql('CREATE INDEX IDX_E91E39A17E5A734A ON tbl_produits (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_institutions_financieres ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_institutions_financieres ADD CONSTRAINT FK_6F35E66F61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_institutions_financieres ADD CONSTRAINT FK_6F35E66F7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_6F35E66F61220EA6 ON tbl_institutions_financieres (creator_id)');
        $this->addSql('CREATE INDEX IDX_6F35E66F7E5A734A ON tbl_institutions_financieres (last_editor_id)');

        $this->addSql('ALTER TABLE demande_credit ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE demande_credit ADD CONSTRAINT FK_5E85281161220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE demande_credit ADD CONSTRAINT FK_5E8528117E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_5E85281161220EA6 ON demande_credit (creator_id)');
        $this->addSql('CREATE INDEX IDX_5E8528117E5A734A ON demande_credit (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_moyen_transport ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_moyen_transport ADD CONSTRAINT FK_2A09625661220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_moyen_transport ADD CONSTRAINT FK_2A0962567E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_2A09625661220EA6 ON tbl_moyen_transport (creator_id)');
        $this->addSql('CREATE INDEX IDX_2A0962567E5A734A ON tbl_moyen_transport (last_editor_id)');

        $this->addSql('ALTER TABLE semence_demandee ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE semence_demandee ADD CONSTRAINT FK_FD4549D661220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE semence_demandee ADD CONSTRAINT FK_FD4549D67E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_FD4549D661220EA6 ON semence_demandee (creator_id)');
        $this->addSql('CREATE INDEX IDX_FD4549D67E5A734A ON semence_demandee (last_editor_id)');

        $this->addSql('ALTER TABLE moyens_transport ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE moyens_transport ADD CONSTRAINT FK_F3FD441C61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE moyens_transport ADD CONSTRAINT FK_F3FD441C7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_F3FD441C61220EA6 ON moyens_transport (creator_id)');
        $this->addSql('CREATE INDEX IDX_F3FD441C7E5A734A ON moyens_transport (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_animaux_production ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_animaux_production ADD CONSTRAINT FK_5698FFC261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_animaux_production ADD CONSTRAINT FK_5698FFC27E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_5698FFC261220EA6 ON tbl_animaux_production (creator_id)');
        $this->addSql('CREATE INDEX IDX_5698FFC27E5A734A ON tbl_animaux_production (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_operation_culturale_type ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_operation_culturale_type ADD CONSTRAINT FK_9F54E47C61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_operation_culturale_type ADD CONSTRAINT FK_9F54E47C7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_9F54E47C61220EA6 ON tbl_operation_culturale_type (creator_id)');
        $this->addSql('CREATE INDEX IDX_9F54E47C7E5A734A ON tbl_operation_culturale_type (last_editor_id)');

        $this->addSQl('DROP INDEX IF EXISTS idx_4625333391c37da8 ON tbl_operation_culturale_type');
        $this->addSql('CREATE INDEX IDX_9F54E47CEC4A74AB ON parcelle_plan_saison_tbl_cultures (unite_id)');

        $this->addSql('ALTER TABLE tbl_infrastructures ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_infrastructures ADD CONSTRAINT FK_35FC207061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_infrastructures ADD CONSTRAINT FK_35FC20707E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_35FC207061220EA6 ON tbl_infrastructures (creator_id)');
        $this->addSql('CREATE INDEX IDX_35FC20707E5A734A ON tbl_infrastructures (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_plants_agro_fo_et_fruitiers ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE utilisateur utilisateur VARCHAR(30) NOT NULL');
        $this->addSql('ALTER TABLE tbl_plants_agro_fo_et_fruitiers ADD CONSTRAINT FK_A2A905A961220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_plants_agro_fo_et_fruitiers ADD CONSTRAINT FK_A2A905A97E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_A2A905A961220EA6 ON tbl_plants_agro_fo_et_fruitiers (creator_id)');
        $this->addSql('CREATE INDEX IDX_A2A905A97E5A734A ON tbl_plants_agro_fo_et_fruitiers (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_fumure_min ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_fumure_min ADD CONSTRAINT FK_C3D6156D61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_fumure_min ADD CONSTRAINT FK_C3D6156D7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C3D6156D61220EA6 ON tbl_fumure_min (creator_id)');
        $this->addSql('CREATE INDEX IDX_C3D6156D7E5A734A ON tbl_fumure_min (last_editor_id)');

        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails ADD CONSTRAINT FK_4943CF1461220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails ADD CONSTRAINT FK_4943CF147E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_4943CF1461220EA6 ON exploitation_commande_aliment_betails (creator_id)');
        $this->addSql('CREATE INDEX IDX_4943CF147E5A734A ON exploitation_commande_aliment_betails (last_editor_id)');

        $this->addSql('ALTER TABLE habitat ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE habitat ADD CONSTRAINT FK_3B37B2E861220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE habitat ADD CONSTRAINT FK_3B37B2E87E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_3B37B2E861220EA6 ON habitat (creator_id)');
        $this->addSql('CREATE INDEX IDX_3B37B2E87E5A734A ON habitat (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_variete_semences ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE cdate cdate DATETIME NOT NULL');
        $this->addSql('ALTER TABLE tbl_variete_semences ADD CONSTRAINT FK_29180AD061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_variete_semences ADD CONSTRAINT FK_29180AD07E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_29180AD061220EA6 ON tbl_variete_semences (creator_id)');
        $this->addSql('CREATE INDEX IDX_29180AD07E5A734A ON tbl_variete_semences (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_situation ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_situation ADD CONSTRAINT FK_A9A5F55961220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_situation ADD CONSTRAINT FK_A9A5F5597E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_A9A5F55961220EA6 ON tbl_situation (creator_id)');
        $this->addSql('CREATE INDEX IDX_A9A5F5597E5A734A ON tbl_situation (last_editor_id)');

        $this->addSql('DROP INDEX UNIQ_79EF5111194D4258 ON tbl_utilites');
        $this->addSql('ALTER TABLE tbl_utilites ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_utilites ADD CONSTRAINT FK_79EF513761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_utilites ADD CONSTRAINT FK_79EF51377E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_79EF513761220EA6 ON tbl_utilites (creator_id)');
        $this->addSql('CREATE INDEX IDX_79EF51377E5A734A ON tbl_utilites (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_mode_culture ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_mode_culture ADD CONSTRAINT FK_996C71FF61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_mode_culture ADD CONSTRAINT FK_996C71FF7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_996C71FF61220EA6 ON tbl_mode_culture (creator_id)');
        $this->addSql('CREATE INDEX IDX_996C71FF7E5A734A ON tbl_mode_culture (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD CONSTRAINT FK_3129BF6C61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD CONSTRAINT FK_3129BF6C7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_3129BF6C61220EA6 ON tbl_operation_culturale_intrant (creator_id)');
        $this->addSql('CREATE INDEX IDX_3129BF6C7E5A734A ON tbl_operation_culturale_intrant (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_equipements_agricoles ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles ADD CONSTRAINT FK_605F824161220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles ADD CONSTRAINT FK_605F82417E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_605F824161220EA6 ON tbl_equipements_agricoles (creator_id)');
        $this->addSql('CREATE INDEX IDX_605F82417E5A734A ON tbl_equipements_agricoles (last_editor_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_44DE5E07F7B18525 ON faux_doublons (exploitationIds)');

        $this->addSql('ALTER TABLE tbl_propriete ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_propriete ADD CONSTRAINT FK_3620340061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_propriete ADD CONSTRAINT FK_362034007E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_3620340061220EA6 ON tbl_propriete (creator_id)');
        $this->addSql('CREATE INDEX IDX_362034007E5A734A ON tbl_propriete (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_amendements ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_amendements ADD CONSTRAINT FK_BCD9A261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_amendements ADD CONSTRAINT FK_BCD9A27E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_BCD9A261220EA6 ON tbl_amendements (creator_id)');
        $this->addSql('CREATE INDEX IDX_BCD9A27E5A734A ON tbl_amendements (last_editor_id)');

        $this->addSql('ALTER TABLE contraintes_developpement ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE contraintes_developpement ADD CONSTRAINT FK_3F1CF93B61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE contraintes_developpement ADD CONSTRAINT FK_3F1CF93B7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_3F1CF93B61220EA6 ON contraintes_developpement (creator_id)');
        $this->addSql('CREATE INDEX IDX_3F1CF93B7E5A734A ON contraintes_developpement (last_editor_id)');

        $this->addSql('ALTER TABLE services_besoin DROP FOREIGN KEY services_besoin_ibfk_2');
        $this->addSql('ALTER TABLE services_besoin ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE services_besoin ADD CONSTRAINT FK_19762A1761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE services_besoin ADD CONSTRAINT FK_19762A177E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_19762A1761220EA6 ON services_besoin (creator_id)');
        $this->addSql('CREATE INDEX IDX_19762A177E5A734A ON services_besoin (last_editor_id)');

        $this->addSql('DROP INDEX UNIQ_79EF5137B94D4258 ON tbl_fournisseurs');
        $this->addSql('ALTER TABLE tbl_fournisseurs ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_fournisseurs ADD CONSTRAINT FK_2DBC7AD761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_fournisseurs ADD CONSTRAINT FK_2DBC7AD77E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_2DBC7AD761220EA6 ON tbl_fournisseurs (creator_id)');
        $this->addSql('CREATE INDEX IDX_2DBC7AD77E5A734A ON tbl_fournisseurs (last_editor_id)');

        $this->addSql('ALTER TABLE formations_besoin ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE formations_besoin ADD CONSTRAINT FK_930D3DF061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE formations_besoin ADD CONSTRAINT FK_930D3DF07E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_930D3DF061220EA6 ON formations_besoin (creator_id)');
        $this->addSql('CREATE INDEX IDX_930D3DF07E5A734A ON formations_besoin (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_valeur_choix_simple ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE cdate cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE tbl_valeur_choix_simple ADD CONSTRAINT FK_28B14CD61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_valeur_choix_simple ADD CONSTRAINT FK_28B14CD7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_28B14CD61220EA6 ON tbl_valeur_choix_simple (creator_id)');
        $this->addSql('CREATE INDEX IDX_28B14CD7E5A734A ON tbl_valeur_choix_simple (last_editor_id)');

        $this->addSql('ALTER TABLE exploitation_plan_saison ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE cdate cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE udate udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE exploitation_plan_saison ADD CONSTRAINT FK_BA78DA6C61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitation_plan_saison ADD CONSTRAINT FK_BA78DA6C7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_BA78DA6C61220EA6 ON exploitation_plan_saison (creator_id)');
        $this->addSql('CREATE INDEX IDX_BA78DA6C7E5A734A ON exploitation_plan_saison (last_editor_id)');

        $this->addSql('ALTER TABLE animaux_structure ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE animaux_structure ADD CONSTRAINT FK_C17E032A61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE animaux_structure ADD CONSTRAINT FK_C17E032A7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C17E032A61220EA6 ON animaux_structure (creator_id)');
        $this->addSql('CREATE INDEX IDX_C17E032A7E5A734A ON animaux_structure (last_editor_id)');

        $this->addSql('ALTER TABLE exploitation_commande_semences ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE exploitation_commande_semences ADD CONSTRAINT FK_D3A7EE0761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitation_commande_semences ADD CONSTRAINT FK_D3A7EE077E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_D3A7EE0761220EA6 ON exploitation_commande_semences (creator_id)');
        $this->addSql('CREATE INDEX IDX_D3A7EE077E5A734A ON exploitation_commande_semences (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_credit_objet ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_credit_objet ADD CONSTRAINT FK_E0F1B80061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_credit_objet ADD CONSTRAINT FK_E0F1B8007E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_E0F1B80061220EA6 ON tbl_credit_objet (creator_id)');
        $this->addSql('CREATE INDEX IDX_E0F1B8007E5A734A ON tbl_credit_objet (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_filieres ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_filieres ADD CONSTRAINT FK_5BA4473861220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_filieres ADD CONSTRAINT FK_5BA447387E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_5BA4473861220EA6 ON tbl_filieres (creator_id)');
        $this->addSql('CREATE INDEX IDX_5BA447387E5A734A ON tbl_filieres (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_mo_type ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_mo_type ADD CONSTRAINT FK_C225978F61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_mo_type ADD CONSTRAINT FK_C225978F7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C225978F61220EA6 ON tbl_mo_type (creator_id)');
        $this->addSql('CREATE INDEX IDX_C225978F7E5A734A ON tbl_mo_type (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_engrais ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_engrais ADD CONSTRAINT FK_F34D485761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_engrais ADD CONSTRAINT FK_F34D48577E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_F34D485761220EA6 ON tbl_engrais (creator_id)');
        $this->addSql('CREATE INDEX IDX_F34D48577E5A734A ON tbl_engrais (last_editor_id)');

        $this->addSql('ALTER TABLE dettes ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE dettes ADD CONSTRAINT FK_15565CF161220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE dettes ADD CONSTRAINT FK_15565CF17E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_15565CF161220EA6 ON dettes (creator_id)');
        $this->addSql('CREATE INDEX IDX_15565CF17E5A734A ON dettes (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_activites_communautaires ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_activites_communautaires ADD CONSTRAINT FK_C7AEEE6F61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_activites_communautaires ADD CONSTRAINT FK_C7AEEE6F7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C7AEEE6F61220EA6 ON tbl_activites_communautaires (creator_id)');
        $this->addSql('CREATE INDEX IDX_C7AEEE6F7E5A734A ON tbl_activites_communautaires (last_editor_id)');
        $this->addSql('DROP INDEX UNIQ_79EF5137B94D6548 ON tbl_unite_produits');

        $this->addSql('ALTER TABLE tbl_unite_produits ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_unite_produits ADD CONSTRAINT FK_66830F7261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_unite_produits ADD CONSTRAINT FK_66830F727E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_66830F7261220EA6 ON tbl_unite_produits (creator_id)');
        $this->addSql('CREATE INDEX IDX_66830F727E5A734A ON tbl_unite_produits (last_editor_id)');

        $this->addSql('ALTER TABLE exploitation_commande_engrais ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE exploitation_commande_engrais ADD CONSTRAINT FK_92A0B86461220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitation_commande_engrais ADD CONSTRAINT FK_92A0B8647E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_92A0B86461220EA6 ON exploitation_commande_engrais (creator_id)');
        $this->addSql('CREATE INDEX IDX_92A0B8647E5A734A ON exploitation_commande_engrais (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_services_payants ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_services_payants ADD CONSTRAINT FK_E6C7F2A961220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_services_payants ADD CONSTRAINT FK_E6C7F2A97E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_E6C7F2A961220EA6 ON tbl_services_payants (creator_id)');
        $this->addSql('CREATE INDEX IDX_E6C7F2A97E5A734A ON tbl_services_payants (last_editor_id)');

        $this->addSql('ALTER TABLE mo_toutes_applis ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE mo_toutes_applis ADD CONSTRAINT FK_67C8D6E061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE mo_toutes_applis ADD CONSTRAINT FK_67C8D6E07E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_67C8D6E061220EA6 ON mo_toutes_applis (creator_id)');
        $this->addSql('CREATE INDEX IDX_67C8D6E07E5A734A ON mo_toutes_applis (last_editor_id)');

        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_123C905761220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_123C90577E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_123C905761220EA6 ON parcelle_operation_culturale (creator_id)');
        $this->addSql('CREATE INDEX IDX_123C90577E5A734A ON parcelle_operation_culturale (last_editor_id)');

        $this->addSql('ALTER TABLE securite_alimentaire ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE securite_alimentaire ADD CONSTRAINT FK_8755733261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE securite_alimentaire ADD CONSTRAINT FK_875573327E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_8755733261220EA6 ON securite_alimentaire (creator_id)');
        $this->addSql('CREATE INDEX IDX_875573327E5A734A ON securite_alimentaire (last_editor_id)');

        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires ADD CONSTRAINT FK_7187311F61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires ADD CONSTRAINT FK_7187311F7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_7187311F61220EA6 ON exploitation_commande_phytosanitaires (creator_id)');
        $this->addSql('CREATE INDEX IDX_7187311F7E5A734A ON exploitation_commande_phytosanitaires (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_type_travail ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_type_travail ADD CONSTRAINT FK_46FB6FD861220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_type_travail ADD CONSTRAINT FK_46FB6FD87E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_46FB6FD861220EA6 ON tbl_type_travail (creator_id)');
        $this->addSql('CREATE INDEX IDX_46FB6FD87E5A734A ON tbl_type_travail (last_editor_id)');

        $this->addSql('ALTER TABLE formations_recu ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE formations_recu ADD CONSTRAINT FK_9B11283461220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE formations_recu ADD CONSTRAINT FK_9B1128347E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_9B11283461220EA6 ON formations_recu (creator_id)');
        $this->addSql('CREATE INDEX IDX_9B1128347E5A734A ON formations_recu (last_editor_id)');
        $this->addSql('DROP INDEX UNIQ_79EF5137236D4258 ON tbl_types_elevage');

        $this->addSql('ALTER TABLE tbl_types_elevage ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_types_elevage ADD CONSTRAINT FK_BEC36FFD61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_types_elevage ADD CONSTRAINT FK_BEC36FFD7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_BEC36FFD61220EA6 ON tbl_types_elevage (creator_id)');
        $this->addSql('CREATE INDEX IDX_BEC36FFD7E5A734A ON tbl_types_elevage (last_editor_id)');

        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas ADD CONSTRAINT FK_C4110161220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas ADD CONSTRAINT FK_C411017E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C4110161220EA6 ON parcelle_historiques_tbl_aleas (creator_id)');
        $this->addSql('CREATE INDEX IDX_C411017E5A734A ON parcelle_historiques_tbl_aleas (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_semences ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_semences ADD CONSTRAINT FK_6C1662E161220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_semences ADD CONSTRAINT FK_6C1662E17E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_6C1662E161220EA6 ON tbl_semences (creator_id)');
        $this->addSql('CREATE INDEX IDX_6C1662E17E5A734A ON tbl_semences (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_rapports ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD utilisateur VARCHAR(30) NOT NULL');
        $this->addSql('ALTER TABLE tbl_rapports ADD CONSTRAINT FK_B53AC2E961220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_rapports ADD CONSTRAINT FK_B53AC2E97E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_B53AC2E961220EA6 ON tbl_rapports (creator_id)');
        $this->addSql('CREATE INDEX IDX_B53AC2E97E5A734A ON tbl_rapports (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_alimentation_betail ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_alimentation_betail ADD CONSTRAINT FK_C650868261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_alimentation_betail ADD CONSTRAINT FK_C65086827E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C650868261220EA6 ON tbl_alimentation_betail (creator_id)');
        $this->addSql('CREATE INDEX IDX_C65086827E5A734A ON tbl_alimentation_betail (last_editor_id)');

        $this->addSql('ALTER TABLE depenses_menage ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE depenses_menage ADD CONSTRAINT FK_83B040F561220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE depenses_menage ADD CONSTRAINT FK_83B040F57E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_83B040F561220EA6 ON depenses_menage (creator_id)');
        $this->addSql('CREATE INDEX IDX_83B040F57E5A734A ON depenses_menage (last_editor_id)');
        $this->addSql('DROP INDEX UNIQ_79EF5977B94D4258 ON tbl_type_garantie');

        $this->addSql('ALTER TABLE tbl_type_garantie ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_type_garantie ADD CONSTRAINT FK_A5FE84DF61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_type_garantie ADD CONSTRAINT FK_A5FE84DF7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_A5FE84DF61220EA6 ON tbl_type_garantie (creator_id)');
        $this->addSql('CREATE INDEX IDX_A5FE84DF7E5A734A ON tbl_type_garantie (last_editor_id)');

        $this->addSql('ALTER TABLE parcelle_historiques ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE parcelle_historiques ADD CONSTRAINT FK_F6AA48534433ED66 FOREIGN KEY (parcelle_id) REFERENCES parcelles (id)');
        $this->addSql('ALTER TABLE parcelle_historiques ADD CONSTRAINT FK_F6AA485361220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE parcelle_historiques ADD CONSTRAINT FK_F6AA48537E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_F6AA485361220EA6 ON parcelle_historiques (creator_id)');
        $this->addSql('CREATE INDEX IDX_F6AA48537E5A734A ON parcelle_historiques (last_editor_id)');

        $this->addSql('ALTER TABLE parcelle_historiques_tbl_cultures ADD CONSTRAINT FK_ADA75D40618DAD4 FOREIGN KEY (parcelle_historiques_id) REFERENCES parcelle_historiques (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_cultures ADD CONSTRAINT FK_ADA75D40EE1FBA60 FOREIGN KEY (tbl_cultures_id) REFERENCES tbl_cultures (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE tbl_type_depense ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_type_depense ADD CONSTRAINT FK_E277823461220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_type_depense ADD CONSTRAINT FK_E27782347E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_E277823461220EA6 ON tbl_type_depense (creator_id)');
        $this->addSql('CREATE INDEX IDX_E27782347E5A734A ON tbl_type_depense (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_traitements ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_traitements ADD CONSTRAINT FK_394FAF9561220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_traitements ADD CONSTRAINT FK_394FAF957E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_394FAF9561220EA6 ON tbl_traitements (creator_id)');
        $this->addSql('CREATE INDEX IDX_394FAF957E5A734A ON tbl_traitements (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_fumure_org ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_fumure_org ADD CONSTRAINT FK_10B8B23D61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_fumure_org ADD CONSTRAINT FK_10B8B23D7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_10B8B23D61220EA6 ON tbl_fumure_org (creator_id)');
        $this->addSql('CREATE INDEX IDX_10B8B23D7E5A734A ON tbl_fumure_org (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_antierosives ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_antierosives ADD CONSTRAINT FK_584968FF61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_antierosives ADD CONSTRAINT FK_584968FF7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_584968FF61220EA6 ON tbl_antierosives (creator_id)');
        $this->addSql('CREATE INDEX IDX_584968FF7E5A734A ON tbl_antierosives (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_labour_animaux ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_labour_animaux ADD CONSTRAINT FK_71993D4A61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_labour_animaux ADD CONSTRAINT FK_71993D4A7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_71993D4A61220EA6 ON tbl_labour_animaux (creator_id)');
        $this->addSql('CREATE INDEX IDX_71993D4A7E5A734A ON tbl_labour_animaux (last_editor_id)');

        $this->addSql('ALTER TABLE animaux_production ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE animaux_production ADD CONSTRAINT FK_48270C6461220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE animaux_production ADD CONSTRAINT FK_48270C647E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_48270C6461220EA6 ON animaux_production (creator_id)');
        $this->addSql('CREATE INDEX IDX_48270C647E5A734A ON animaux_production (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_cultures ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE categorie_id categorie_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE tbl_cultures ADD CONSTRAINT FK_7B53BB4ABCF5E72D FOREIGN KEY (categorie_id) REFERENCES tbl_categories_agricoles (id)');
        $this->addSql('ALTER TABLE tbl_cultures ADD CONSTRAINT FK_7B53BB4A61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_cultures ADD CONSTRAINT FK_7B53BB4A7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_7B53BB4A61220EA6 ON tbl_cultures (creator_id)');
        $this->addSql('CREATE INDEX IDX_7B53BB4A7E5A734A ON tbl_cultures (last_editor_id)');

        $this->addSql('ALTER TABLE engrais_demande ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE engrais_demande ADD CONSTRAINT FK_18A591A861220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE engrais_demande ADD CONSTRAINT FK_18A591A87E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_18A591A861220EA6 ON engrais_demande (creator_id)');
        $this->addSql('CREATE INDEX IDX_18A591A87E5A734A ON engrais_demande (last_editor_id)');

        $this->addSql('ALTER TABLE animaux ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE animaux ADD CONSTRAINT FK_9ABE194D61220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE animaux ADD CONSTRAINT FK_9ABE194D7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_9ABE194D61220EA6 ON animaux (creator_id)');
        $this->addSql('CREATE INDEX IDX_9ABE194D7E5A734A ON animaux (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_contraints ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_contraints ADD CONSTRAINT FK_8A05A5B061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_contraints ADD CONSTRAINT FK_8A05A5B07E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_8A05A5B061220EA6 ON tbl_contraints (creator_id)');
        $this->addSql('CREATE INDEX IDX_8A05A5B07E5A734A ON tbl_contraints (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_operation_culturale_type_category ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_operation_culturale_type_category ADD CONSTRAINT FK_22B2EC961220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_operation_culturale_type_category ADD CONSTRAINT FK_22B2EC97E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_22B2EC961220EA6 ON tbl_operation_culturale_type_category (creator_id)');
        $this->addSql('CREATE INDEX IDX_22B2EC97E5A734A ON tbl_operation_culturale_type_category (last_editor_id)');

        $this->addSql('ALTER TABLE mo_familiale_externe ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE mo_familiale_externe ADD CONSTRAINT FK_4DC62DE261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE mo_familiale_externe ADD CONSTRAINT FK_4DC62DE27E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_4DC62DE261220EA6 ON mo_familiale_externe (creator_id)');
        $this->addSql('CREATE INDEX IDX_4DC62DE27E5A734A ON mo_familiale_externe (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_services ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_services ADD CONSTRAINT FK_2401074461220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_services ADD CONSTRAINT FK_240107447E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_2401074461220EA6 ON tbl_services (creator_id)');
        $this->addSql('CREATE INDEX IDX_240107447E5A734A ON tbl_services (last_editor_id)');

        $this->addSql('ALTER TABLE collecte ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', DROP annee_enquete, CHANGE enqueteur enqueteur VARCHAR(255) NOT NULL, CHANGE exploitation_id creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE collecte ADD CONSTRAINT FK_55AE4A3D7E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_55AE4A3D61220EA6 ON collecte (creator_id)');
        $this->addSql('CREATE INDEX IDX_55AE4A3D7E5A734A ON collecte (last_editor_id)');

        $this->addSql('ALTER TABLE tbl_prestations_services ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE tbl_prestations_services ADD CONSTRAINT FK_504DB04261220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE tbl_prestations_services ADD CONSTRAINT FK_504DB0427E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_504DB04261220EA6 ON tbl_prestations_services (creator_id)');
        $this->addSql('CREATE INDEX IDX_504DB0427E5A734A ON tbl_prestations_services (last_editor_id)');

        $this->addSql('ALTER TABLE batiments ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', DROP longueur, DROP largueur');
        $this->addSql('ALTER TABLE batiments ADD CONSTRAINT FK_124D7990C54C8C93 FOREIGN KEY (type_id) REFERENCES tbl_infrastructures (id)');
        $this->addSql('ALTER TABLE batiments ADD CONSTRAINT FK_124D799061220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE batiments ADD CONSTRAINT FK_124D79907E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_124D799061220EA6 ON batiments (creator_id)');
        $this->addSql('CREATE INDEX IDX_124D79907E5A734A ON batiments (last_editor_id)');
    }
}
