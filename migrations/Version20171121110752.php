<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171121110752 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE animaux DROP annee;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE animaux ADD annee VARCHAR(4) DEFAULT NULL;');
    }
}
