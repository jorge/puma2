<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170511165904 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE fos_user DROP COLUMN cdate;');
        $this->addSql('ALTER TABLE fos_user DROP COLUMN udate;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE fos_user ADD COLUMN cdate datetime DEFAULT NOW();');
        $this->addSql('ALTER TABLE fos_user ADD COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');
    }
}
