<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210325155511 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exploitation_commande_plant_aff ADD creator_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', ADD last_editor_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\', CHANGE cdate cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL, CHANGE utilisateur utilisateur VARCHAR(30) DEFAULT NULL, CHANGE udate udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL');
        $this->addSql('ALTER TABLE exploitation_commande_plant_aff ADD CONSTRAINT FK_C3D0220661220EA6 FOREIGN KEY (creator_id) REFERENCES fos_user (id)');
        $this->addSql('ALTER TABLE exploitation_commande_plant_aff ADD CONSTRAINT FK_C3D022067E5A734A FOREIGN KEY (last_editor_id) REFERENCES fos_user (id)');
        $this->addSql('CREATE INDEX IDX_C3D0220661220EA6 ON exploitation_commande_plant_aff (creator_id)');
        $this->addSql('CREATE INDEX IDX_C3D022067E5A734A ON exploitation_commande_plant_aff (last_editor_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exploitation_commande_plant_aff DROP FOREIGN KEY FK_C3D0220661220EA6');
        $this->addSql('ALTER TABLE exploitation_commande_plant_aff DROP FOREIGN KEY FK_C3D022067E5A734A');
        $this->addSql('DROP INDEX IDX_C3D0220661220EA6 ON exploitation_commande_plant_aff');
        $this->addSql('DROP INDEX IDX_C3D022067E5A734A ON exploitation_commande_plant_aff');
        $this->addSql('ALTER TABLE exploitation_commande_plant_aff DROP creator_id, DROP last_editor_id, CHANGE cdate cdate DATETIME NOT NULL, CHANGE udate udate DATETIME NOT NULL, CHANGE utilisateur utilisateur VARCHAR(60) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`');        
    }
}
