<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180903083515 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP INDEX UNIQ_3F1CF93B6412DF6B ON contraintes_developpement;');
        $this->addSql('DROP INDEX UNIQ_A8BB65621F198DBE ON tbl_professions;');
        $this->addSql('DROP INDEX UNIQ_5503F6F455D24163 ON prestation_de_services;');
        $this->addSql('DROP INDEX UNIQ_18A591A8F1FB5220 ON engrais_demande;');
        $this->addSql('DROP INDEX UNIQ_2DBC7AD72E8C07C5 ON tbl_fournisseurs;');
        $this->addSql('DROP INDEX UNIQ_A5FE84DF24430B1 ON tbl_type_garantie;');
        $this->addSql('DROP INDEX UNIQ_B32598E8AAE840D9 ON unite_de_transformation;');
        $this->addSql('DROP INDEX UNIQ_484E768C4B3851F0 ON tbl_materiels_bureaux;');
        $this->addSql('DROP INDEX UNIQ_66830F722DE47CE1 ON tbl_unite_produits;');
        $this->addSql('DROP INDEX UNIQ_63EEA3BF5F828FBC ON services_payants_aux_membres;');
        $this->addSql('DROP INDEX UNIQ_19762A1782B95021 ON services_besoin;');
        $this->addSql('DROP INDEX UNIQ_EBD075FEEC4F0C96 ON infrastructures;');
        $this->addSql('DROP INDEX UNIQ_289AD39E103F8E8 ON materiel_roulant;');
        $this->addSql('DROP INDEX UNIQ_504DB042A129DBE4 ON tbl_prestations_services;');
        $this->addSql('DROP INDEX UNIQ_9DDA99EE3F0033A2 ON tbl_services_aux_membres;');
        $this->addSql('DROP INDEX UNIQ_9BFFF6C2AAE840D9 ON tbl_unites_transformations;');
        $this->addSql('DROP INDEX UNIQ_2A0962561D7BF450 ON tbl_moyen_transport;');
        $this->addSql('DROP INDEX UNIQ_6F35E66F228D5512 ON tbl_institutions_financieres;');
        $this->addSql('DROP INDEX UNIQ_240107443F0033A2 ON tbl_services;');
        $this->addSql('DROP INDEX UNIQ_41333F984DC3CE3C ON services_aux_membres;');
        $this->addSql('DROP INDEX UNIQ_BEC36FFD71C3A747 ON tbl_types_elevage;');
        $this->addSql('DROP INDEX UNIQ_C97A115F04DF4EE ON filieres;');
        $this->addSql('DROP INDEX UNIQ_7B53BB4A6834359B ON tbl_cultures;');
        $this->addSql('DROP INDEX UNIQ_930D3DF0C0759D98 ON formations_besoin;');
        $this->addSql('DROP INDEX UNIQ_C7AEEE6F69B029F3 ON tbl_activites_communautaires;');
        $this->addSql('DROP INDEX UNIQ_8D950044C9486A13 ON tbl_categories_agricoles;');
        $this->addSql('DROP INDEX UNIQ_3B37B2E8E4395983 ON habitat;');
        $this->addSql('DROP INDEX UNIQ_2FE520B8AB214F92 ON suivi_organisations;');
        $this->addSql('DROP INDEX UNIQ_3625AD7F33EFC77B ON materiel_de_bureau;');
        $this->addSql('DROP INDEX UNIQ_79EF5137B94D4258 ON tbl_utilites;');
        $this->addSql('DROP INDEX UNIQ_59930160C661C64E ON niveau_de_reconnaissance;');
        $this->addSql('DROP INDEX UNIQ_68CA9AAB67DD38BA ON tbl_themes_formations;');
        $this->addSql('DROP INDEX UNIQ_204538E44C970E9C ON besoin_service_aux_membres;');
        $this->addSql('DROP INDEX UNIQ_B53AC2E960A909EC ON tbl_rapports;');
        $this->addSql('DROP INDEX UNIQ_D43971EB2A91CD58 ON tbl_unite_filieres;');
        $this->addSql('DROP INDEX UNIQ_D10DCD5E1D8E4969 ON tbl_etat_civil;');
        $this->addSql('DROP INDEX UNIQ_F3FD441C1D7BF450 ON moyens_transport;');
        $this->addSql('DROP INDEX UNIQ_7366F8E468161836 ON tbl_organisateurs;');
        $this->addSql('DROP INDEX UNIQ_5537CEA0EB1F1FC5 ON phyto_demande;');
        $this->addSql('DROP INDEX UNIQ_65359205B8A79DE1 ON tbl_presentation;');
        $this->addSql('DROP INDEX UNIQ_5BA44738F04DF4EE ON tbl_filieres;');
        $this->addSql('DROP INDEX UNIQ_F497DCE57F449E57 ON tbl_domaines_etudes_superieures;');
        $this->addSql('DROP INDEX UNIQ_605F8241220943B2 ON tbl_equipements_agricoles;');
        $this->addSql('DROP INDEX UNIQ_64E3DF79220943B2 ON equipement_agricole;');
        $this->addSql('DROP INDEX UNIQ_8A05A5B0AABBFFFA ON tbl_contraints;');
        $this->addSql('DROP INDEX UNIQ_9B112834C0759D98 ON formations_recu;');
        $this->addSql('DROP INDEX UNIQ_2F41C3A926EBD78E ON besoins_formations;');
        $this->addSql('DROP INDEX UNIQ_35FC2070EC4F0C96 ON tbl_infrastructures;');
        $this->addSql('DROP INDEX UNIQ_6390D95FE05BE676 ON tbl_mois;');
        $this->addSql('DROP INDEX UNIQ_E6C7F2A9EE27F7A2 ON tbl_services_payants;');
        $this->addSql('DROP INDEX UNIQ_FD4549D61ECF1C07 ON semence_demandee;');
        $this->addSql('DROP INDEX UNIQ_444631BFE6E7606B ON tbl_saisons;');
        $this->addSql('DROP INDEX UNIQ_AA9B6097D076B7C7 ON besoins_institutionel;');
        $this->addSql('DROP INDEX UNIQ_CFAB5402DC759BEE ON mobilisations_communautaires;');
        $this->addSql('DROP INDEX UNIQ_125927F7C661C64E ON tbl_niveaux_reconnaissance;');

        $this->addSql('ALTER TABLE tbl_antierosives DROP dossier;');
        $this->addSql('ALTER TABLE personne_details DROP superieur;');

        $this->addSql("ALTER TABLE personnes CHANGE membre_menage membre_menage TINYINT(1) DEFAULT '1';");
        $this->addSql('ALTER TABLE animaux_structure CHANGE total total INT DEFAULT NULL;');

        $this->addSql('ALTER TABLE collecte ADD CONSTRAINT FK_55AE4A3DD967A16D FOREIGN KEY (exploitation_id) REFERENCES exploitations (id);');
        $this->addSql('ALTER TABLE animaux_labour ADD CONSTRAINT FK_D17D0995A9DAECAA FOREIGN KEY (animaux_id) REFERENCES animaux (id);');
        $this->addSql('ALTER TABLE animaux_labour ADD CONSTRAINT FK_D17D0995CC903DF2 FOREIGN KEY (labour_id) REFERENCES tbl_labour_animaux (id);');
        $this->addSql('ALTER TABLE parcelle_historiques ADD CONSTRAINT FK_F6AA485374D5C10E FOREIGN KEY (mode_culture_id) REFERENCES tbl_mode_culture (id);');
        $this->addSql('ALTER TABLE parcelle_historiques ADD CONSTRAINT FK_F6AA4853F965414C FOREIGN KEY (saison_id) REFERENCES tbl_saisons (id);');
        $this->addSql('ALTER TABLE personne_details ADD CONSTRAINT FK_ACA9EF77B96414BB FOREIGN KEY (profession_principale_id) REFERENCES tbl_professions (id);');
        $this->addSql('ALTER TABLE personne_details ADD CONSTRAINT FK_ACA9EF77E92381CE FOREIGN KEY (profession_secondaire_id) REFERENCES tbl_professions (id);');
        $this->addSql('ALTER TABLE animaux_production ADD CONSTRAINT FK_48270C64A9DAECAA FOREIGN KEY (animaux_id) REFERENCES animaux (id);');
        $this->addSql('ALTER TABLE animaux_production ADD CONSTRAINT FK_48270C64ECC6147F FOREIGN KEY (production_id) REFERENCES tbl_animaux_production (id);');

        $this->addSql('CREATE UNIQUE INDEX unique_personnes_annee ON personne_details (annee, personnes_id);');
    }
}
