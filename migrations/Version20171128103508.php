<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171128103508 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
        $this->addSql('ALTER TABLE dettes DROP COLUMN periode_remboursement;');
        $this->addSql('ALTER TABLE dettes CHANGE COLUMN periode_remboursement_old periode_remboursement Date;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE dettes CHANGE COLUMN periode_remboursement periode_remboursement_old Datetime;');
        $this->addSql('ALTER TABLE dettes ADD COLUMN periode_remboursement int(11);');
    }
}
