<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190220103046 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelle_operation_culturale CHANGE COLUMN quantite_utilisee quantite_utilisee INT(10) NULL DEFAULT NULL ;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale CHANGE COLUMN produitExterne produitExterne INT(10) NULL DEFAULT NULL ;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale CHANGE COLUMN produitInterne produitInterne INT(10) NULL DEFAULT NULL ;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelle_operation_culturale CHANGE COLUMN quantite_utilisee quantite_utilisee FLOAT(7,2) NULL DEFAULT NULL ;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale CHANGE COLUMN produitExterne produitExterne FLOAT(7,2) NULL DEFAULT NULL ;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale CHANGE COLUMN produitInterne produitInterne FLOAT(7,2) NULL DEFAULT NULL ;');
        $this->addSql('ALTER TABLE parcelle_historiques DROP COLUMN parcelle_historique_annee;');
    }
}
