<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180517143334 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        /* ajout du element "sans dans tbl_antierosives */
        $this->addSql("INSERT INTO puma_03.tbl_antierosives (`id`,`antierosive`,`cdate`,`udate`,`utilisateur`,`dossier`)
                       VALUES ('6aae6769-59de-11e8-b5be-b8ca3a965972','Sans','2018-05-17 14:27:25','2018-05-17 14:27:25','jorge','000');");
    }
}
