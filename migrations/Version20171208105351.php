<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171208105351 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE dettes ADD type_creancier_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)';");
        $this->addSql('ALTER TABLE dettes ADD CONSTRAINT FK_15565CF120E24518 FOREIGN KEY (type_creancier_id) REFERENCES tbl_type_creancier (id);');
        $this->addSql("CREATE INDEX IDX_15565CF120E24518 ON dettes (type_creancier_id);')");
    }
}
