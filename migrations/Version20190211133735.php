<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20190211133735 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE dettes DROP old_institution_financiere_id, DROP old_saison_id, DROP old_garantie_id, DROP old_creditobjet_id, DROP old_type_creancier_id, CHANGE periode_remboursement_old periode_remboursement_old DATE DEFAULT NULL;');
        $this->addSql('ALTER TABLE services_besoin DROP id_service_besoin, DROP old_service_id;');
        $this->addSql('ALTER TABLE infrastructures DROP id_infrastructure, DROP old_infrastructure, CHANGE infrastructure_id infrastructure_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE materiel_roulant DROP id_materiel_roulant, DROP old_materiel_id;');
        $this->addSql('ALTER TABLE services_aux_membres DROP id_services_aux_membres, DROP old_service_id, CHANGE service_id service_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE formations_besoin DROP id_formation, DROP old_formation_id;');
        $this->addSql('ALTER TABLE personnes DROP id_personne, DROP old_etat_civil, CHANGE etat_civil_id etat_civil_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE mo_familiale_externe DROP old_type_travail_id;');
        $this->addSql('ALTER TABLE suivi_organisations DROP old_institution_bancaire_id, DROP old_institution_credit_membres_id, DROP old_institution_financement_credit_org_id;');
        $this->addSql('ALTER TABLE materiel_de_bureau DROP id_materiel_bureau, DROP old_materiel_id;');
        $this->addSql('ALTER TABLE niveau_de_reconnaissance DROP id_niveau_reconnaissance, DROP old_reconnaissance_id;');
        $this->addSql('ALTER TABLE batiments DROP old_type, CHANGE type_id type_id INT DEFAULT NULL, CHANGE annee annee VARCHAR(4) NOT NULL;');
        $this->addSql('ALTER TABLE besoin_service_aux_membres DROP FOREIGN KEY besoin_service_aux_membres_ibfk_1;');
        $this->addSql('ALTER TABLE besoin_service_aux_membres DROP id_service_a_membres, DROP old_service_id;');
        $this->addSql('ALTER TABLE besoin_service_aux_membres ADD CONSTRAINT FK_204538E4ED5CA9E6 FOREIGN KEY (service_id) REFERENCES tbl_services_aux_membres (id);');
        $this->addSql('ALTER TABLE antierosives DROP dossier, DROP old_type_antierosive_id;');
        $this->addSql('DROP INDEX UNIQ_79EF5137B94D4135 ON tbl_services_payants;');
        $this->addSql('ALTER TABLE semence_demandee DROP id_semence_demandee, DROP old_semence_id;');
        $this->addSql('DROP INDEX UNIQ_79EF5137B94D4258 ON tbl_saisons;');
        $this->addSql('ALTER TABLE mo_familiale_interne DROP old_type_travail_id;');
        $this->addSql('ALTER TABLE besoins_institutionel DROP id_besoin_institutionel, DROP old_institutionel_id;');
    }
}
