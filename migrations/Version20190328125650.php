<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190328125650 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parcelle_operation_culturale DROP FOREIGN KEY FK_123C905791C37DA8');
        $this->addSql('ALTER TABLE parcelle_operation_culturale DROP COLUMN intrant_id;');
        $this->addSql('DROP TABLE tbl_operation_culturale_intrant;');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('CREATE TABLE tbl_operation_culturale_intrant (id INT AUTO_INCREMENT NOT NULL, operation_culturale_id INT DEFAULT NULL, nom VARCHAR(255) NOT NULL, cdate DATETIME NOT NULL, udate DATETIME NOT NULL, utilisateur VARCHAR(255) NOT NULL, INDEX IDX_3129BF6CB1B67A5B (operation_culturale_id), PRIMARY KEY(id));');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD CONSTRAINT FK_3129BF6CB1B67A5B FOREIGN KEY (operation_culturale_id) REFERENCES tbl_operation_culturale_type (id);');
        $this->addSql('ALTER TABLE parcelle_operation_culturale
            ADD intrant_id INT DEFAULT NULL,
            CHANGE quantite_recoltee_primaire quantite_recoltee_primaire DOUBLE PRECISION DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_123C905791C37DA8 FOREIGN KEY (intrant_id) REFERENCES tbl_operation_culturale_intrant (id);');
        $this->addSql('CREATE INDEX IDX_123C905791C37DA8 ON parcelle_operation_culturale (intrant_id);');
    }
}
