<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170523152512 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE synchro_status DROP local_id;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE synchro_status ADD local_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)';");
    }
}
