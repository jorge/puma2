<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180903080425 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE contraintes_developpement ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_phytosanitaire ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_amendements ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE prestation_de_services ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE exploitation_charges ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE engrais_demande ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE animaux ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE dettes ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE unite_de_transformation ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE collecte ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE services_payants_aux_membres ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE services_besoin ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE formations_besoin ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE infrastructures ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE materiel_roulant ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE animaux_labour ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_semences ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_herbicide ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE services_aux_membres ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE cotisations ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE mo_familiale_externe ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE outillage ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE animaux_alimentation ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE pivot_personnes_organisations ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_fumure_org ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE habitat ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE suivi_organisations ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE materiel_de_bureau ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE commercialisation_produits ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE niveau_de_reconnaissance ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE batiments ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE besoin_service_aux_membres ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE antierosives ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE securite_alimentaire ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_recoltes ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_historiques ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE moyens_transport ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE phyto_demande ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_fumure_min ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE mo_toutes_applis ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE depenses_menage ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE animaux_structure ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE animaux_production ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE equipement_agricole ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE historique_cultures ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE formations_recu ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE pivot_exploitation_organisations ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelle_techniques ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE besoins_formations ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE animaux_produits ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE semence_demandee ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE mo_familiale_interne ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE parcelles ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE besoins_institutionel ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql("ALTER TABLE mobilisations_communautaires ADD radie TINYINT(1) DEFAULT '0' NOT NULL;");
    }
}
