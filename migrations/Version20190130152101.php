<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190130152101 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE parcelle_historiques_tbl_cultures (
                parcelle_historiques_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
                tbl_cultures_id INT NOT NULL,
                INDEX IDX_ADA75D40618DAD4 (parcelle_historiques_id),
                INDEX IDX_ADA75D40EE1FBA60 (tbl_cultures_id),
                PRIMARY KEY(parcelle_historiques_id, tbl_cultures_id))
                DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
    }
}
