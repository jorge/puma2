<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190204145219 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE parcelle_historiques_tbl_aleas;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE parcelle_historiques_tbl_aleas (
            id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            parcelle_historique_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            aleas_id INT NOT NULL,
            date DATE NOT NULL,
            commentaire varchar(255) DEFAULT NULL,
            cdate datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
            udate datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
            utilisateur varchar(30),
            INDEX IDX_5555515A9JORGE (parcelle_historique_id),
            INDEX IDX_66666154A719501 (aleas_id),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas ADD CONSTRAINT FK_5555515A9JORGE FOREIGN KEY (parcelle_historique_id) REFERENCES parcelle_historiques (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE parcelle_historiques_tbl_aleas ADD CONSTRAINT FK_6666654A719501 FOREIGN KEY (aleas_id) REFERENCES tbl_aleas (id) ON DELETE CASCADE;');
    }
}
