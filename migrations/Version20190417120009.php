<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190417120009 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_operation_culturale_type DROP unite_id;');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_operation_culturale_type DROP unite;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_type
            ADD unite_id INT DEFAULT NULL;');
        //   $this->addSql("ALTER TABLE tbl_operation_culturale_type ADD CONSTRAINT FK_4625333391C37DA8 FOREIGN KEY (unite_id) REFERENCES tbl_unite_produits (id);");
        $this->addSql('CREATE INDEX IDX_4625333391C37DA8 ON tbl_operation_culturale_type (unite_id);');
    }
}
