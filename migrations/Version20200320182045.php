<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200320182045 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE exploitations DROP carte_membre;');
    }

    public function getDescription(): string
    {
        return 'Add carte membre';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE exploitations ADD carte_membre TINYINT(1) DEFAULT '0' NOT NULL;");
    }
}
