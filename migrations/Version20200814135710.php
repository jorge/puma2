<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200814135710 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails DROP ou_est_adressee');
        $this->addSql('ALTER TABLE exploitation_commande_engrais DROP ou_est_adressee');
        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires DROP ou_est_adressee');
        $this->addSql('ALTER TABLE exploitation_commande_semences DROP ou_est_adressee');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE exploitation_commande_engrais ADD ou_est_adressee VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE exploitation_commande_phytosanitaires ADD ou_est_adressee VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE exploitation_commande_semences ADD ou_est_adressee VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails ADD ou_est_adressee VARCHAR(255) DEFAULT NULL');
    }
}
