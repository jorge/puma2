<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200131102632 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return 'Travail externe lié à profession';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE mo_familiale_interne DROP FOREIGN KEY mo_familiale_interne_ibfk_1;');
        $this->addSql('DROP INDEX IDX_5FE9A610F3FDA88C ON mo_familiale_interne;');
        $this->addSql('ALTER TABLE mo_familiale_interne DROP type_travail_id;');

        $this->addSql('ALTER TABLE mo_familiale_externe DROP FOREIGN KEY mo_familiale_externe_ibfk_1;');
        $this->addSql('UPDATE mo_familiale_externe SET type_travail_id = NULL;');
        $this->addSql('ALTER TABLE mo_familiale_externe ADD CONSTRAINT FK_4DC62DE2F3FDA88C FOREIGN KEY (type_travail_id) REFERENCES tbl_professions (id);');
        $this->addSql('DROP INDEX UNIQ_79EF5137B94D4258 ON tbl_professions;');
    }
}
