<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171113085148 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parcelles DROP FOREIGN KEY FK_4F15F60EED9CE593, DROP INDEX IDX_4F15F60EED9CE593;');
        $this->addSql('ALTER TABLE parcelles DROP localisation_admin_id;');
        $this->addSql("DELETE FROM tbl_infrastructures WHERE id = '13432a6f-c3c9-11e7-b898-b8ca3a965972';");

        //$this->addSql("TODO");
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("INSERT INTO tbl_infrastructures (id,id_infrastructure,infrastructure, utilisateur) VALUES ('13432a6f-c3c9-11e7-b898-b8ca3a965972',7,'Etable', 'jorge');"); // a faire car tbl_infrastructures ne se trouve pas dans le tableau synchro_entities de la config
        $this->addSql("ALTER TABLE parcelles ADD localisation_admin_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)';");
        $this->addSql('ALTER TABLE parcelles ADD CONSTRAINT FK_4F15F60EED9CE593 FOREIGN KEY (localisation_admin_id) REFERENCES tbl_decoupage_admin (id);');
        $this->addSql('CREATE INDEX IDX_4F15F60EED9CE593 ON parcelles (localisation_admin_id);');
    }
}
