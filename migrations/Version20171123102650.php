<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171123102650 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelles DROP longueur, DROP largueur;');
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelles ADD longueur NUMERIC(18, 2) DEFAULT NULL, ADD largueur NUMERIC(18, 2) DEFAULT NULL');
        // this up() migration is auto-generated, please modify it to your needs
    }
}
