<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190508133013 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        throw new Exception('Pas de retour en arrière !', 1);
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // mettre données de tbl_alim vers tbl_alim_bet
        $this->addSql('INSERT INTO tbl_alimentation_betail (nom, cdate, udate, utilisateur) SELECT alimentation, cdate, udate, utilisateur FROM tbl_alimentation;');
        $this->addSql("CREATE TABLE animaux_tbl_alimentation_betail (animaux_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)', tbl_alimentation_betail_id INT NOT NULL, INDEX IDX_368F96E3A9DAECAA (animaux_id), INDEX IDX_368F96E382426421 (tbl_alimentation_betail_id), PRIMARY KEY(animaux_id, tbl_alimentation_betail_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
        $this->addSql('ALTER TABLE animaux_tbl_alimentation_betail ADD CONSTRAINT FK_368F96E3A9DAECAA FOREIGN KEY (animaux_id) REFERENCES animaux (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE animaux_tbl_alimentation_betail ADD CONSTRAINT FK_368F96E382426421 FOREIGN KEY (tbl_alimentation_betail_id) REFERENCES tbl_alimentation_betail (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails DROP  FOREIGN KEY FK_4943CF14415B9F11');
        $this->addSql('ALTER TABLE exploitation_commande_aliment_betails ADD CONSTRAINT FK_4943CF14415B9F11 FOREIGN KEY (aliment_id) REFERENCES tbl_alimentation_betail (id);');

        $this->addSql('DROP TABLE animaux_tbl_alimentation;');
        $this->addSql('DROP TABLE animaux_alimentation;');
        $this->addSql('DROP TABLE tbl_alimentation;');
    }
}
