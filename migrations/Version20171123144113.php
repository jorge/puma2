<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171123144113 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE antierosives DROP FOREIGN KEY FK_A61A12694433ED66, DROP INDEX IDX_A61A12694433ED66;');
        $this->addSql('ALTER TABLE antierosives DROP FOREIGN KEY FK_A61A12699F2BF6FC, DROP INDEX IDX_A61A12699F2BF6FC;');
        $this->addSql('DROP TABLE antierosives;');
        $this->addSql('DROP TABLE tbl_antierosives;');
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        // ne pas mettre dans config.synchro_entities : créer une migration qui l'ajoute
        $this->addSql("CREATE TABLE tbl_antierosives (id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            antierosive VARCHAR(255) NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(255) NOT NULL,
            dossier VARCHAR(3) DEFAULT '000' NOT NULL,
            UNIQUE INDEX UNIQ_584968FFA92F7EF0 (antierosive),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        // a ajouter dans config.synchro (la donnée doit être synchronisée)
        $this->addSql("CREATE TABLE antierosives (id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            parcelle_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            type_antierosive_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
            annee VARCHAR(4) NOT NULL,
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            utilisateur VARCHAR(30) DEFAULT NULL,
            dossier VARCHAR(3) DEFAULT '000' NOT NULL,
            INDEX IDX_A61A12694433ED66 (parcelle_id),
            INDEX IDX_A61A12699F2BF6FC (type_antierosive_id),
            PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE antierosives ADD CONSTRAINT FK_A61A12699F2BF6FC FOREIGN KEY (type_antierosive_id) REFERENCES tbl_antierosives (id);');
        $this->addSql('ALTER TABLE antierosives ADD CONSTRAINT FK_A61A12694433ED66 FOREIGN KEY (parcelle_id) REFERENCES parcelles (id);');
    }
}
