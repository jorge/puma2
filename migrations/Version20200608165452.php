<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200608165452 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'Maj fiche suivi coop';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE suivi_organisations
            ADD nombre_total_membres INT DEFAULT NULL,
            ADD chiffre_daffaires DOUBLE PRECISION DEFAULT NULL,
            ADD ristourne_par_membre DOUBLE PRECISION DEFAULT NULL,
            ADD nombre_membres_payants_cotisation INT DEFAULT NULL,
            ADD audit_annuel_des_comptes TINYINT(1) DEFAULT NULL,
            ADD montant_total_cotisations DOUBLE PRECISION DEFAULT NULL,
            ADD montant_services_payes_par_membres DOUBLE PRECISION DEFAULT NULL,
            ADD montant_services_payes_par_tiers DOUBLE PRECISION DEFAULT NULL;');

        $this->addSql('ALTER TABLE tbl_valeur_choix_simple ADD score INT NOT NULL;');
    }
}
