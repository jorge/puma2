<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180330135315 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE personnes DROP repondant;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE personnes ADD repondant TINYINT(1) DEFAULT '0' NOT NULL;");
    }
}
