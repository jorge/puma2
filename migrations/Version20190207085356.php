<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190207085356 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        // $this->addSql("DELETE FROM parcelle_operation_culturale;"); (attention besoin de ça)
        $this->addSql('DELETE FROM tbl_operation_culturale_type;');
        $this->addSql('DELETE FROM tbl_operation_culturale_type_category;');
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (1,'Préparation du sol / Labour','2019-01-30 13:17:11','2019-01-30 13:17:11');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (2,'Semis','2019-01-30 13:17:12','2019-01-30 13:17:12');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (3,'Entretiens','2019-01-30 13:17:12','2019-01-30 13:17:12');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (4,'Amadement','2019-01-30 13:17:12','2019-01-30 13:17:12');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (5,'Fumure organique','2019-01-30 13:17:12','2019-01-30 13:17:12');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (6,'Fumure mineral','2019-01-30 13:17:12','2019-01-30 13:17:12');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (7,'Herbicides','2019-01-30 13:17:12','2019-01-30 13:17:12');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (8,'Phytosanitaires','2019-01-30 13:17:12','2019-01-30 13:17:12');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type_category (id,nom,cdate,udate) VALUES (9,'récoltes','2019-02-04 12:38:49','2019-02-04 12:38:49');");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (1,1,'Labour',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (2,2,'Plantation',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (3,2,'Semences',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (4,2,'Repicage',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (5,3,'Paillage',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (6,3,'Démariage',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (7,3,'Sarclage',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (8,3,'Buttage',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (9,3,'Tuteurage',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (10,3,'Travail antiérosif',NULL,0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (11,4,'Chaux','kg',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (12,5,'Composte','kg',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (13,5,'Fiente','kg',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (14,5,'Fumier','kg',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (15,6,'dap','kg',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (16,6,'kci','kg',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (17,6,'npk','kg',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (18,7,'Herbicide','litre',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (19,8,'dithane','litre',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (20,8,'ridomil','litre',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (21,8,'dimethoate','litre',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (22,8,'dursiban','litre',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (23,8,'autres','litre',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (24,6,'urée','kg',0);");
        $this->addSql("INSERT INTO tbl_operation_culturale_type (id,category_id,nom,unite,type) VALUES (25,9,'récolte',NULL,0);");
    }
}
