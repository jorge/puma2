<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180522121720 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        /* effacer table animaux_production */
        $this->addSql('DROP TABLE IF EXISTS animaux_production;');
    }

    public function up(Schema $schema): void
    {
        /* creatio table animaux_production */
        $this->addSql("CREATE TABLE animaux_production (id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
                animaux_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
                production_id CHAR(36) DEFAULT NULL COMMENT '(DC2Type:guid)',
                annee VARCHAR(4) DEFAULT NULL,
                quantite INT DEFAULT NULL,
                cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                utilisateur VARCHAR(255) NOT NULL,
                INDEX IDX_48270C64A9DAECAA (animaux_id),
                INDEX IDX_48270C64ECC6147F (production_id),
                PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
    }
}
