<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181212205343 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // NOPE
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE animaux_tbl_alimentation (
            animaux_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_alimentation_id INT NOT NULL, INDEX IDX_F5F6662EA9DAECAA (animaux_id),
            INDEX IDX_F5F6662E5A6D8157 (tbl_alimentation_id),
            PRIMARY KEY(animaux_id, tbl_alimentation_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
        ");

        $this->addSql("CREATE TABLE animaux_tbl_produits (
            animaux_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_produits_id INT NOT NULL, INDEX IDX_3213015A9DAECAA (animaux_id),
            INDEX IDX_32130154A704257 (tbl_produits_id),
            PRIMARY KEY(animaux_id, tbl_produits_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;
        ");

        $this->addSql('ALTER TABLE animaux_tbl_alimentation ADD CONSTRAINT FK_F5F6662EA9DAECAA FOREIGN KEY (animaux_id) REFERENCES animaux (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE animaux_tbl_alimentation ADD CONSTRAINT FK_F5F6662E5A6D8157 FOREIGN KEY (tbl_alimentation_id) REFERENCES tbl_alimentation (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE animaux_tbl_produits ADD CONSTRAINT FK_3213015A9DAECAA FOREIGN KEY (animaux_id) REFERENCES animaux (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE animaux_tbl_produits ADD CONSTRAINT FK_32130154A704257 FOREIGN KEY (tbl_produits_id) REFERENCES tbl_produits (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE animaux
            ADD nbr_total_tetes INT DEFAULT NULL,
            ADD femelles_reproductrices INT DEFAULT NULL,
            ADD males_reproducteurs INT DEFAULT NULL,
            ADD jeunes_femelles INT DEFAULT NULL,
            ADD jeunes_males INT DEFAULT NULL,
            ADD jeunes_immatures INT DEFAULT NULL,
            ADD age_reforme_femelles NUMERIC(10, 0) DEFAULT NULL,
            DROP age_reforme_reprod,
            DROP id_animaux,
            DROP old_culture,
            DROP old_type_elevage_id,
            CHANGE espece_id espece_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE animaux
            ADD CONSTRAINT FK_9ABE194D2D191E7A FOREIGN KEY (espece_id) REFERENCES tbl_cultures (id);');
    }
}
