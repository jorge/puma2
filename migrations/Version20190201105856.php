<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190201105856 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD COLUMN quantite_utilisee Int(10) DEFAULT 0;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD produit_primaire_id INT(10) DEFAULT NULL, ADD produit_secondaire_id INT(10) DEFAULT NULL, ADD quantite_recoltee_primaire SMALLINT DEFAULT NULL, ADD quantite_recoltee_secondaire SMALLINT DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_POCCB02BB6FFC1D4 FOREIGN KEY (produit_primaire_id) REFERENCES tbl_produits (id);');
        $this->addSql('ALTER TABLE parcelle_operation_culturale ADD CONSTRAINT FK_POCCB02BABEF01C2 FOREIGN KEY (produit_secondaire_id) REFERENCES tbl_produits (id);');
        $this->addSql('CREATE INDEX IDX_POCCB02BB6FFC1D4 ON parcelle_operation_culturale (produit_primaire_id);');
        $this->addSql('CREATE INDEX IDX_POCCB02BABEF01C2 ON parcelle_operation_culturale (produit_secondaire_id);');
    }
}
