<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180518121457 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE personnes CHANGE cni cni VARCHAR(20) DEFAULT NULL;');
    }
}
