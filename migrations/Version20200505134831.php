<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200505134831 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'Mise à jour suivi coop selon email richard';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE suivi_organisations
            ADD montant_epargne_muso DOUBLE PRECISION DEFAULT NULL,
            CHANGE solde_annee_avant solde_annee_avant DOUBLE PRECISION DEFAULT NULL,
            CHANGE cotisations_annee_avant cotisations_annee_avant DOUBLE PRECISION DEFAULT NULL,
            CHANGE agr_montant agr_montant DOUBLE PRECISION DEFAULT NULL,
            CHANGE prestation_services_montant prestation_services_montant DOUBLE PRECISION DEFAULT NULL,
            CHANGE cotisations cotisations DOUBLE PRECISION DEFAULT NULL,
            CHANGE membres_credit_montant membres_credit_montant DOUBLE PRECISION DEFAULT NULL,
            CHANGE montant_financement montant_financement DOUBLE PRECISION DEFAULT NULL,
            CHANGE credit_pour_organisation_montant credit_pour_organisation_montant DOUBLE PRECISION DEFAULT NULL,
            CHANGE muso nbr_muso INT DEFAULT NULL;');
    }
}
