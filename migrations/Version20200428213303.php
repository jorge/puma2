<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200428213303 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'Suppression de tables inutilitées (gérées par symfony directement)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP TABLE equipement_agricole;');
        $this->addSql('DROP TABLE infrastructures;');
        $this->addSql('DROP TABLE materiel_de_bureau;');
        $this->addSql('DROP TABLE materiel_roulant;');
        $this->addSql('DROP TABLE prestation_de_services;');
        $this->addSql('DROP TABLE services_aux_membres;');
        $this->addSql('DROP TABLE services_payants_aux_membres');
        $this->addSql('DROP TABLE unite_de_transformation;');
    }
}
