<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190424115344 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        throw new Exception('Pas de retour en arrière', 1);
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant DROP FOREIGN KEY FK_3129BF6CB1B67A5B;');
        $this->addSql('DROP INDEX IDX_3129BF6CB1B67A5B ON tbl_operation_culturale_intrant;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant CHANGE operation_culturale_id operation_culturale_category_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant ADD CONSTRAINT FK_3129BF6C33F9DCAE FOREIGN KEY (operation_culturale_category_id) REFERENCES tbl_operation_culturale_type_category (id);');
        $this->addSql('CREATE INDEX IDX_3129BF6C33F9DCAE ON tbl_operation_culturale_intrant (operation_culturale_category_id);');
    }
}
