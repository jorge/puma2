<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180522120124 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        /* effacer la table */
        $this->addSql('DROP TABLE IF EXISTS tbl_labour_animaux;');
    }

    public function up(Schema $schema): void
    {
        /* creation table tbl_labour_animaux */
        $this->addSql("CREATE TABLE tbl_labour_animaux (id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
                labour VARCHAR(100) NOT NULL,
                cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                utilisateur VARCHAR(255) NOT NULL,
                UNIQUE INDEX UNIQ_71993D4A54411E49 (labour),
                PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");
    }
}
