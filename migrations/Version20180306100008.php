<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180306100008 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE personnes DROP radie;');
        $this->addSql('ALTER TABLE personne_details DROP radie;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE personnes ADD radie INT DEFAULT 0;');
        $this->addSql('ALTER TABLE personne_details ADD radie INT DEFAULT 0;');
    }
}
