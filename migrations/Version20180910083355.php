<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180910083355 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        /**
         * adding integer id for object having a name tbl* : theses tables
         * are parameters of the tool and they can only edited by the
         * administration (in the db section) (TODO).
         */

        // tbl_activites_communautaires ********>");
        $this->addSql('ALTER TABLE mobilisations_communautaires DROP FOREIGN KEY FK_CFAB5402A4EA940F;');
        $this->addSql('ALTER TABLE tbl_activites_communautaires CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_activites_communautaires DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_activites_communautaires CHANGE id_activites_communautaire id int NOT NULL;');
        $this->addSql('ALTER TABLE tbl_activites_communautaires ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_activites_communautaires MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_activites_communautaires MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_activites_communautaires MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE mobilisations_communautaires ADD COLUMN old_mobilisation char(36) DEFAULT NULL;');
        $this->addSql('UPDATE mobilisations_communautaires SET old_mobilisation=mobilisation_id;');
        $this->addSql('UPDATE mobilisations_communautaires SET mobilisation_id=(SELECT tbl_activites_communautaires.id FROM tbl_activites_communautaires WHERE
                       tbl_activites_communautaires.old_id=mobilisations_communautaires.old_mobilisation);');

        //tbl_categories_agricoles *********>");
        $this->addSql('ALTER TABLE tbl_cultures DROP FOREIGN KEY FK_7B53BB4ABCF5E72D;');
        $this->addSql('ALTER TABLE tbl_categories_agricoles CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_categories_agricoles DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_categories_agricoles CHANGE id_categorie id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_categories_agricoles ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_categories_agricoles MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_categories_agricoles ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_categories_agricoles MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_categories_agricoles MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE tbl_cultures ADD COLUMN old_category_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE tbl_cultures SET old_category_id=categorie_id;');
        $this->addSql('UPDATE tbl_cultures SET categorie_id=(SELECT tbl_categories_agricoles.id FROM tbl_categories_agricoles WHERE
                tbl_categories_agricoles.old_id=tbl_cultures.old_category_id);');

        //tbl_contraints");
        $this->addSql('ALTER TABLE contraintes_developpement DROP FOREIGN KEY FK_3F1CF93B6CE5EAC1;');
        $this->addSql('ALTER TABLE tbl_contraints CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_contraints DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_contraints CHANGE id_contraint id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_contraints ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_contraints MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_contraints MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_contraints MODIFY old_id CHAR(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE contraintes_developpement ADD COLUMN old_contrainte char(36) DEFAULT NULL;');
        $this->addSql('UPDATE contraintes_developpement SET old_contrainte=contrainte_id;');
        $this->addSql('UPDATE contraintes_developpement SET contrainte_id=(SELECT tbl_contraints.id FROM tbl_contraints WHERE
                tbl_contraints.old_id=contraintes_developpement.old_contrainte);');

        //tbl_cultures *************>");
        $this->addSql('ALTER TABLE animaux DROP FOREIGN KEY FK_9ABE194D2D191E7A;');
        $this->addSql('ALTER TABLE filieres DROP FOREIGN KEY FK_C97A115920C327B;');
        $this->addSql('ALTER TABLE historique_cultures DROP FOREIGN KEY FK_ECB82595B108249D;');

        $this->addSql('ALTER TABLE tbl_cultures CHANGE id old_id char(36);');

        $this->addSql('DROP TABLE tbl_presentation;');
        $this->addSql('ALTER TABLE tbl_produits DROP FOREIGN KEY FK_E91E39A1B108249D;');
        $this->addSql('ALTER TABLE tbl_cultures DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_cultures CHANGE id_culture id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_cultures ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_cultures MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_cultures MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_cultures MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE animaux ADD COLUMN old_culture char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE filieres ADD COLUMN old_culture char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE historique_cultures ADD COLUMN old_culture char(36) DEFAULT NULL;');

        $this->addSql('UPDATE animaux SET old_culture=espece_id;');
        $this->addSql('UPDATE filieres SET old_culture=filiere_principale_id;');
        $this->addSql('UPDATE historique_cultures SET old_culture=culture_id;');

        $this->addSql('UPDATE animaux SET espece_id=(SELECT tbl_cultures.id FROM tbl_cultures WHERE
                tbl_cultures.old_id=animaux.old_culture);');

        $this->addSql('UPDATE filieres SET filiere_principale_id=(SELECT tbl_cultures.id FROM tbl_cultures WHERE
                tbl_cultures.old_id=filieres.old_culture);');

        $this->addSql('UPDATE historique_cultures SET culture_id=(SELECT tbl_cultures.id FROM tbl_cultures WHERE
                tbl_cultures.old_id=historique_cultures.old_culture);');

        // tbl_domaines_etudes_superieures *****************>");
        $this->addSql('ALTER TABLE personne_details DROP FOREIGN KEY FK_ACA9EF774272FC9F;');
        $this->addSql('ALTER TABLE personne_details DROP INDEX IDX_ACA9EF774272FC9F;');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures CHANGE id_id id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_domaines_etudes_superieures MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE personne_details ADD COLUMN old_domaine char(36) DEFAULT NULL;');
        $this->addSql('UPDATE personne_details SET old_domaine=domaine_id;');
        $this->addSql('UPDATE personne_details SET domaine_id=(SELECT tbl_domaines_etudes_superieures.id FROM tbl_domaines_etudes_superieures WHERE
                tbl_domaines_etudes_superieures.old_id=personne_details.old_domaine);');

        // tbl_equipements_agricoles");
        $this->addSql('ALTER TABLE equipement_agricole DROP FOREIGN KEY FK_64E3DF79806F0F5C;');
        $this->addSql('ALTER TABLE outillage DROP FOREIGN KEY FK_C5FC74D2C54C8C93;');

        $this->addSql('ALTER TABLE tbl_equipements_agricoles CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles CHANGE id_equipement_agricole id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_equipements_agricoles MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE equipement_agricole ADD COLUMN old_equipement char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE outillage ADD COLUMN old_equipement char(36) DEFAULT NULL;');

        $this->addSql('UPDATE equipement_agricole SET old_equipement=equipement_id;');
        $this->addSql('UPDATE outillage SET old_equipement=type_id;');

        $this->addSql('UPDATE equipement_agricole SET equipement_id=(SELECT tbl_equipements_agricoles.id FROM tbl_equipements_agricoles WHERE
                tbl_equipements_agricoles.old_id=equipement_agricole.old_equipement);');
        $this->addSql('UPDATE outillage SET type_id=(SELECT tbl_equipements_agricoles.id FROM tbl_equipements_agricoles WHERE
                tbl_equipements_agricoles.old_id=outillage.old_equipement);');

        // tbl_etat_civil");
        $this->addSql('ALTER TABLE personnes DROP FOREIGN KEY FK_2BB4FE2B191476EE;');
        $this->addSql('ALTER TABLE tbl_etat_civil CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_etat_civil DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_etat_civil CHANGE oldid id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_etat_civil ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_etat_civil MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_etat_civil MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_etat_civil MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE personnes ADD COLUMN old_etat_civil char(36) DEFAULT NULL;');
        $this->addSql('UPDATE personnes SET old_etat_civil=etat_civil_id;');
        $this->addSql('UPDATE personnes SET etat_civil_id=(SELECT tbl_etat_civil.id FROM tbl_etat_civil WHERE
                tbl_etat_civil.old_id=personnes.old_etat_civil);');

        // tbl_filieres **** pas trouvé ****");

        $this->addSql('ALTER TABLE tbl_filieres CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_filieres DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_filieres CHANGE id_filiere id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_filieres ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_filieres MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_filieres ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_filieres MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_filieres MODIFY old_id CHAR(36) DEFAULT NULL;');

        //tbl_fournisseurs ***** pas trouvé *****");
        $this->addSql('ALTER TABLE tbl_fournisseurs CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_fournisseurs DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_fournisseurs CHANGE id_fournisseur id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_fournisseurs ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_fournisseurs MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_fournisseurs ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_fournisseurs MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_fournisseurs MODIFY old_id CHAR(36) DEFAULT NULL;');

        //tbl_infrastructures");
        $this->addSql('ALTER TABLE batiments DROP FOREIGN KEY FK_124D7990C54C8C93;');
        $this->addSql('ALTER TABLE infrastructures DROP FOREIGN KEY FK_EBD075FE243E7A84;');

        $this->addSql('ALTER TABLE tbl_infrastructures CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_infrastructures DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_infrastructures CHANGE id_infrastructure id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_infrastructures ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_infrastructures MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_infrastructures MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_infrastructures MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE batiments ADD COLUMN old_type char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE infrastructures ADD COLUMN old_infrastructure char(36) DEFAULT NULL;');

        $this->addSql('UPDATE batiments SET old_type=type_id;');
        $this->addSql('UPDATE infrastructures SET old_infrastructure=infrastructure_id;');

        $this->addSql('UPDATE batiments SET type_id=(SELECT tbl_infrastructures.id FROM tbl_infrastructures WHERE
                tbl_infrastructures.old_id=batiments.old_type);');
        $this->addSql('UPDATE infrastructures SET infrastructure_id=(SELECT tbl_infrastructures.id FROM tbl_infrastructures WHERE
                tbl_infrastructures.old_id=infrastructures.old_infrastructure);');

        //tbl_institutions_financieres");
        $this->addSql('ALTER TABLE dettes DROP FOREIGN KEY FK_15565CF1C9B1538;');
        $this->addSql('ALTER TABLE suivi_organisations DROP FOREIGN KEY FK_2FE520B837C0BBA8;');
        $this->addSql('ALTER TABLE suivi_organisations DROP FOREIGN KEY FK_2FE520B8E5EFF275;');
        $this->addSql('ALTER TABLE suivi_organisations DROP FOREIGN KEY FK_2FE520B8F200E2A4;');

        $this->addSql('ALTER TABLE tbl_institutions_financieres CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_institutions_financieres DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_institutions_financieres CHANGE id_institution id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_institutions_financieres ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_institutions_financieres MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_institutions_financieres MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_institutions_financieres MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE dettes ADD COLUMN old_institution_financiere_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE suivi_organisations ADD COLUMN old_institution_bancaire_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE suivi_organisations ADD COLUMN old_institution_credit_membres_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE suivi_organisations ADD COLUMN old_institution_financement_credit_org_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE dettes SET old_institution_financiere_id=institution_financiere_id;');
        $this->addSql('UPDATE suivi_organisations SET old_institution_bancaire_id=institution_bancaire_id;');
        $this->addSql('UPDATE suivi_organisations SET old_institution_credit_membres_id=institution_credit_membres_id;');
        $this->addSql('UPDATE suivi_organisations SET old_institution_financement_credit_org_id=institution_financement_credit_org_id;');

        $this->addSql('UPDATE dettes SET institution_financiere_id=(SELECT tbl_institutions_financieres.id FROM tbl_institutions_financieres WHERE
                tbl_institutions_financieres.old_id=dettes.old_institution_financiere_id);');
        $this->addSql('UPDATE suivi_organisations SET institution_bancaire_id=(SELECT tbl_institutions_financieres.id FROM tbl_institutions_financieres WHERE
                tbl_institutions_financieres.old_id=suivi_organisations.old_institution_bancaire_id);');
        $this->addSql('UPDATE suivi_organisations SET institution_credit_membres_id=(SELECT tbl_institutions_financieres.id FROM tbl_institutions_financieres WHERE
                tbl_institutions_financieres.old_id=suivi_organisations.old_institution_credit_membres_id);');
        $this->addSql('UPDATE suivi_organisations SET institution_financement_credit_org_id=(SELECT tbl_institutions_financieres.id FROM tbl_institutions_financieres WHERE
                tbl_institutions_financieres.old_id=suivi_organisations.old_institution_financement_credit_org_id);');

        $this->addSql('ALTER TABLE dettes MODIFY institution_financiere_id int(10);');

        $this->addSql('ALTER TABLE dettes ADD FOREIGN KEY (institution_financiere_id) REFERENCES tbl_institutions_financieres(id);');
        $this->addSql('ALTER TABLE suivi_organisations MODIFY institution_bancaire_id int(10);');
        $this->addSql('ALTER TABLE suivi_organisations MODIFY institution_credit_membres_id int(10);');
        $this->addSql('ALTER TABLE suivi_organisations MODIFY institution_financement_credit_org_id int(10);');
        $this->addSql('ALTER TABLE suivi_organisations ADD FOREIGN KEY (institution_bancaire_id) REFERENCES tbl_institutions_financieres(id);');
        $this->addSql('ALTER TABLE suivi_organisations ADD FOREIGN KEY (institution_credit_membres_id) REFERENCES tbl_institutions_financieres(id);');
        $this->addSql('ALTER TABLE suivi_organisations ADD FOREIGN KEY (institution_financement_credit_org_id) REFERENCES tbl_institutions_financieres(id);');

        // tbl_materiels_bureaux");
        $this->addSql('ALTER TABLE materiel_de_bureau DROP FOREIGN KEY FK_3625AD7F16880AAF;');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux CHANGE id_materiel_bureaux id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_materiels_bureaux MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE materiel_de_bureau ADD COLUMN old_materiel_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE materiel_de_bureau SET old_materiel_id=materiel_id;');
        $this->addSql('UPDATE materiel_de_bureau SET materiel_id=(SELECT tbl_materiels_bureaux.id FROM tbl_materiels_bureaux WHERE
                tbl_materiels_bureaux.old_id=materiel_de_bureau.old_materiel_id);');
        $this->addSql('ALTER TABLE materiel_de_bureau MODIFY materiel_id int(10);');
        $this->addSql('ALTER TABLE materiel_de_bureau ADD FOREIGN KEY (materiel_id) REFERENCES tbl_materiels_bureaux(id);');

        // tbl_moyen_transport");
        $this->addSql('ALTER TABLE materiel_roulant DROP FOREIGN KEY FK_289AD3916880AAF;');
        $this->addSql('ALTER TABLE moyens_transport DROP FOREIGN KEY FK_F3FD441CC54C8C93;');

        $this->addSql('ALTER TABLE tbl_moyen_transport CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_moyen_transport DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_moyen_transport CHANGE id_moyen_transport id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_moyen_transport ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_moyen_transport MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_moyen_transport MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_moyen_transport MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE materiel_roulant ADD COLUMN old_materiel_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE moyens_transport ADD COLUMN old_type_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE materiel_roulant SET old_materiel_id=materiel_id;');
        $this->addSql('UPDATE moyens_transport SET old_type_id=type_id;');

        $this->addSql('UPDATE materiel_roulant SET materiel_id=(SELECT tbl_moyen_transport.id FROM tbl_moyen_transport WHERE
                tbl_moyen_transport.old_id=materiel_roulant.old_materiel_id);');
        $this->addSql('UPDATE moyens_transport SET type_id=(SELECT tbl_moyen_transport.id FROM tbl_moyen_transport WHERE
                tbl_moyen_transport.old_id=moyens_transport.old_type_id);');

        $this->addSql('ALTER TABLE materiel_roulant MODIFY materiel_id int(10);');
        $this->addSql('ALTER TABLE moyens_transport MODIFY type_id int(10);');

        $this->addSql('ALTER TABLE materiel_roulant ADD FOREIGN KEY (materiel_id) REFERENCES tbl_moyen_transport(id);');
        $this->addSql('ALTER TABLE moyens_transport ADD FOREIGN KEY (type_id) REFERENCES tbl_moyen_transport(id);');

        $this->addSql('ALTER TABLE niveau_de_reconnaissance DROP FOREIGN KEY FK_59930160850FB2F1;');
        // tbl_niveaux_reconnaissance");
        $this->addSql('ALTER TABLE suivi_organisations DROP FOREIGN KEY FK_2FE520B8B2D7D02D;');
        $this->addSql('ALTER TABLE suivi_organisations DROP FOREIGN KEY FK_2FE520B8A0627FC3;');
        $this->addSql('ALTER TABLE suivi_organisations DROP FOREIGN KEY FK_2FE520B818DE18A6;');

        $this->addSql('ALTER TABLE suivi_organisations DROP COLUMN niveau_reconnaissance1_id, DROP COLUMN niveau_reconnaissance2_id, DROP COLUMN niveau_reconnaissance3_id;');

        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance CHANGE id_niveau_reconnaissance id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_niveaux_reconnaissance MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE niveau_de_reconnaissance ADD COLUMN old_reconnaissance_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE niveau_de_reconnaissance SET old_reconnaissance_id=reconnaissance_id;');
        $this->addSql('UPDATE niveau_de_reconnaissance SET reconnaissance_id=(SELECT tbl_niveaux_reconnaissance.id FROM tbl_niveaux_reconnaissance WHERE
                tbl_niveaux_reconnaissance.old_id=niveau_de_reconnaissance.old_reconnaissance_id);');

        $this->addSql('ALTER TABLE niveau_de_reconnaissance MODIFY reconnaissance_id int(10);');
        $this->addSql('ALTER TABLE niveau_de_reconnaissance ADD FOREIGN KEY (reconnaissance_id) REFERENCES tbl_niveaux_reconnaissance(id);');

        // tbl_organisateurs");
        $this->addSql('ALTER TABLE formations_recu DROP FOREIGN KEY FK_9B112834D936B2FA;');

        $this->addSql('ALTER TABLE tbl_organisateurs CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_organisateurs DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_organisateurs CHANGE id_organisateur id int;');
        $this->addSql("UPDATE puma_03.tbl_organisateurs SET id=14 WHERE old_id='0cd312ae-5a99-11e8-b22b-b8ca3a965972';");
        $this->addSql("UPDATE puma_03.tbl_organisateurs SET id=15 WHERE old_id='0cdca684-5a99-11e8-b22b-b8ca3a965972';");
        $this->addSql('ALTER TABLE tbl_organisateurs ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_organisateurs MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_organisateurs ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_organisateurs MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_organisateurs MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE formations_recu ADD COLUMN old_organisateur_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE formations_recu SET old_organisateur_id=organisateur_id;');
        $this->addSql('UPDATE formations_recu SET organisateur_id=(SELECT tbl_organisateurs.id FROM tbl_organisateurs WHERE
                tbl_organisateurs.old_id=formations_recu.old_organisateur_id);');

        $this->addSql('ALTER TABLE formations_recu MODIFY organisateur_id int(10);');
        $this->addSql('ALTER TABLE formations_recu ADD FOREIGN KEY (organisateur_id) REFERENCES tbl_organisateurs(id);');

        // tbl_prestations_services");
        $this->addSql('ALTER TABLE prestation_de_services DROP FOREIGN KEY FK_5503F6F49E45C554;');
        $this->addSql('ALTER TABLE tbl_prestations_services CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_prestations_services DROP PRIMARY KEY;');

        $this->addSql('ALTER TABLE tbl_prestations_services CHANGE id_prestation_service id int;');
        $this->addSql('ALTER TABLE tbl_prestations_services ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_prestations_services MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_prestations_services ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_prestations_services MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_prestations_services MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE prestation_de_services ADD COLUMN old_prestation_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE prestation_de_services SET old_prestation_id=prestation_id;');
        $this->addSql('UPDATE prestation_de_services SET prestation_id=(SELECT tbl_prestations_services.id FROM tbl_prestations_services WHERE
                tbl_prestations_services.old_id=prestation_de_services.old_prestation_id);');

        $this->addSql('ALTER TABLE prestation_de_services MODIFY prestation_id int(10);');
        $this->addSql('ALTER TABLE prestation_de_services ADD FOREIGN KEY (prestation_id) REFERENCES tbl_prestations_services(id);');

        //tbl_produits");
        $this->addSql('ALTER TABLE animaux_produits DROP FOREIGN KEY FK_10735564F347EFB;');
        $this->addSql('ALTER TABLE commercialisation_produits DROP FOREIGN KEY FK_C8BFE321F347EFB;');
        $this->addSql('ALTER TABLE parcelle_recoltes DROP FOREIGN KEY FK_CB0CB02BABEF01C2;');
        $this->addSql('ALTER TABLE parcelle_recoltes DROP FOREIGN KEY FK_CB0CB02BB6FFC1D4;');
        $this->addSql('ALTER TABLE tbl_produits CHANGE id old_id char(36);');

        // ajout id pour element recemments ajoutés (sans id)
        $this->addSql("UPDATE tbl_produits SET id_produit=135 WHERE old_id='b217ae61-5a80-11e8-b22b-b8ca3a965972';");
        $this->addSql("UPDATE tbl_produits SET id_produit=136 WHERE old_id='b21fae0b-5a80-11e8-b22b-b8ca3a965972';");
        $this->addSql("UPDATE tbl_produits SET id_produit=137 WHERE old_id='b2267b5e-5a80-11e8-b22b-b8ca3a965972';");
        $this->addSql("UPDATE tbl_produits SET id_produit=138 WHERE old_id='b2302500-5a80-11e8-b22b-b8ca3a965972';");

        $this->addSql('ALTER TABLE tbl_produits DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_produits CHANGE id_produit id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_produits ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_produits MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_produits DROP INDEX UNIQ_E91E39A1F7384557;');
        $this->addSql('ALTER TABLE tbl_produits ADD UNIQUE INDEX UNIQ_E91E39A1F7384557 (id);');
        $this->addSql('ALTER TABLE tbl_produits MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_produits MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE animaux_produits ADD COLUMN old_produit_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE commercialisation_produits ADD COLUMN old_produit_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_recoltes ADD COLUMN old_produit_primaire_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_recoltes ADD COLUMN old_produit_secondaire_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE animaux_produits SET old_produit_id=produit_id;');
        $this->addSql('UPDATE commercialisation_produits SET old_produit_id=produit_id;');
        $this->addSql('UPDATE parcelle_recoltes SET old_produit_primaire_id=produit_primaire_id;');
        $this->addSql('UPDATE parcelle_recoltes SET old_produit_secondaire_id=produit_secondaire_id;');

        $this->addSql('UPDATE animaux_produits SET produit_id=(SELECT tbl_produits.id FROM tbl_produits WHERE
                tbl_produits.old_id=animaux_produits.old_produit_id);');
        $this->addSql('UPDATE commercialisation_produits SET produit_id=(SELECT tbl_produits.id FROM tbl_produits WHERE
                tbl_produits.old_id=commercialisation_produits.old_produit_id);');
        $this->addSql('UPDATE parcelle_recoltes SET produit_primaire_id=(SELECT tbl_produits.id FROM tbl_produits WHERE
                tbl_produits.old_id=parcelle_recoltes.old_produit_primaire_id);');
        $this->addSql('UPDATE parcelle_recoltes SET produit_secondaire_id=(SELECT tbl_produits.id FROM tbl_produits WHERE
                tbl_produits.old_id=parcelle_recoltes.old_produit_secondaire_id);');

        $this->addSql('ALTER TABLE animaux_produits MODIFY produit_id int(10);');
        $this->addSql('ALTER TABLE commercialisation_produits MODIFY produit_id int(10);');
        $this->addSql('ALTER TABLE parcelle_recoltes MODIFY produit_primaire_id int(10);');
        $this->addSql('ALTER TABLE parcelle_recoltes MODIFY produit_secondaire_id int(10);');

        $this->addSql('ALTER TABLE animaux_produits ADD FOREIGN KEY (produit_id) REFERENCES tbl_produits(id);');
        $this->addSql('ALTER TABLE commercialisation_produits ADD FOREIGN KEY (produit_id) REFERENCES tbl_produits(id);');
        $this->addSql('ALTER TABLE parcelle_recoltes ADD FOREIGN KEY (produit_primaire_id) REFERENCES tbl_produits(id);');
        $this->addSql('ALTER TABLE parcelle_recoltes ADD FOREIGN KEY (produit_secondaire_id) REFERENCES tbl_produits(id);');

        //tbl_professions");
        $this->addSql('ALTER TABLE personne_details DROP FOREIGN KEY FK_ACA9EF77B96414BB;');
        $this->addSql('ALTER TABLE personne_details DROP FOREIGN KEY FK_ACA9EF77E92381CE;');

        $this->addSql('ALTER TABLE tbl_professions CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_professions DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_professions CHANGE id_profession id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_professions ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_professions MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_professions ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_professions MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_professions MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE personne_details ADD COLUMN old_profession_principale_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE personne_details ADD COLUMN old_profession_secondaire_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE personne_details SET old_profession_principale_id=profession_principale_id;');
        $this->addSql('UPDATE personne_details SET old_profession_secondaire_id=profession_secondaire_id;');

        $this->addSql('UPDATE personne_details SET profession_principale_id=(SELECT tbl_professions.id FROM tbl_professions WHERE
                tbl_professions.old_id=personne_details.old_profession_principale_id);');
        $this->addSql('UPDATE personne_details SET profession_secondaire_id=(SELECT tbl_professions.id FROM tbl_professions WHERE
                tbl_professions.old_id=personne_details.old_profession_secondaire_id);');
        $this->addSql('ALTER TABLE personne_details MODIFY profession_principale_id int(10);');
        $this->addSql('ALTER TABLE personne_details MODIFY profession_secondaire_id int(10);');

        $this->addSql('ALTER TABLE personne_details ADD FOREIGN KEY (profession_principale_id) REFERENCES tbl_professions(id);');
        $this->addSql('ALTER TABLE personne_details ADD FOREIGN KEY (profession_secondaire_id) REFERENCES tbl_professions(id);');

        //tbl_saisons");
        $this->addSql('ALTER TABLE commercialisation_produits DROP FOREIGN KEY FK_C8BFE321F965414C;');
        $this->addSql('ALTER TABLE dettes DROP FOREIGN KEY FK_15565CF1F965414C;');
        $this->addSql('ALTER TABLE parcelle_historiques DROP FOREIGN KEY FK_F6AA4853F965414C;');

        $this->addSql('ALTER TABLE tbl_saisons CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_saisons DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_saisons CHANGE id_saison id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_saisons ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_saisons MODIFY id INT(10) FIRST;');
        $this->addSql('ALTER TABLE tbl_saisons ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_saisons MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_saisons MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE commercialisation_produits ADD COLUMN old_saison_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE dettes ADD COLUMN old_saison_id char(36) DEFAULT NULL;');
        $this->addSql('ALTER TABLE parcelle_historiques ADD COLUMN old_saison_id char(36) DEFAULT NULL;');

        $this->addSql('UPDATE commercialisation_produits SET old_saison_id=saison_id;');
        $this->addSql('UPDATE dettes SET old_saison_id=saison_id;');
        $this->addSql('UPDATE parcelle_historiques SET old_saison_id=saison_id;');

        $this->addSql('UPDATE commercialisation_produits SET saison_id=(SELECT tbl_saisons.id FROM tbl_saisons WHERE
                tbl_saisons.old_id=commercialisation_produits.old_saison_id);');
        $this->addSql('UPDATE dettes SET saison_id=(SELECT tbl_saisons.id FROM tbl_saisons WHERE
                tbl_saisons.old_id=dettes.old_saison_id);');
        $this->addSql('UPDATE parcelle_historiques SET saison_id=(SELECT tbl_saisons.id FROM tbl_saisons WHERE
                tbl_saisons.old_id=parcelle_historiques.old_saison_id);');

        $this->addSql('ALTER TABLE commercialisation_produits MODIFY saison_id int(10);');
        $this->addSql('ALTER TABLE dettes MODIFY saison_id int(10);');
        $this->addSql('ALTER TABLE parcelle_historiques MODIFY saison_id int(10);');

        $this->addSql('ALTER TABLE commercialisation_produits ADD FOREIGN KEY (saison_id) REFERENCES tbl_saisons(id);');
        $this->addSql('ALTER TABLE dettes ADD FOREIGN KEY (saison_id) REFERENCES tbl_saisons(id);');
        $this->addSql('ALTER TABLE parcelle_historiques ADD FOREIGN KEY (saison_id) REFERENCES tbl_saisons(id);');

        //tbl_services");
        $this->addSql('ALTER TABLE services_besoin DROP FOREIGN KEY FK_19762A17ED5CA9E6;');

        $this->addSql('ALTER TABLE tbl_services CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_services DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_services CHANGE id_service id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_services ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_services MODIFY id INT(10) FIRST;');

        $this->addSql('ALTER TABLE tbl_services ADD UNIQUE INDEX UNIQ_79EF5137B94D4258 (id);');
        $this->addSql('ALTER TABLE tbl_services MODIFY id INT(10) NOT NULL AUTO_INCREMENT;');
        $this->addSql('ALTER TABLE tbl_services MODIFY old_id CHAR(36) DEFAULT NULL;');

        $this->addSql('ALTER TABLE services_besoin ADD COLUMN old_service_id char(36) DEFAULT NULL;');
        $this->addSql('UPDATE services_besoin SET old_service_id=service_id;');
        $this->addSql('UPDATE services_besoin SET service_id=(SELECT tbl_services.id FROM tbl_services WHERE
                tbl_services.old_id=services_besoin.old_service_id);');

        $this->addSql('ALTER TABLE services_besoin MODIFY service_id int(10);');
        $this->addSql('ALTER TABLE services_besoin ADD FOREIGN KEY (service_id) REFERENCES tbl_services(id);');

        //tbl_services_aux_membres");
        $this->addSql('ALTER TABLE besoin_service_aux_membres DROP FOREIGN KEY FK_204538E4ED5CA9E6;');
        $this->addSql('ALTER TABLE services_aux_membres DROP FOREIGN KEY FK_41333F98ED5CA9E6;');

        $this->addSql('ALTER TABLE tbl_services_aux_membres CHANGE id old_id char(36);');
        $this->addSql('ALTER TABLE tbl_services_aux_membres DROP PRIMARY KEY;');
        $this->addSql('ALTER TABLE tbl_services_aux_membres CHANGE id_service id int NOT NULL ;');
        $this->addSql('ALTER TABLE tbl_services_aux_membres ADD PRIMARY KEY (id);');
        $this->addSql('ALTER TABLE tbl_services_aux_membres MODIFY id INT(10) FIRST;');
    }
}
