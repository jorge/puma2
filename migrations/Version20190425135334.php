<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190425135334 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE parcelle_plan_saison
                ADD mode_culture_id INT DEFAULT NULL, CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql('ALTER TABLE parcelle_plan_saison ADD CONSTRAINT FK_307D5B2A74D5C10E
                FOREIGN KEY (mode_culture_id) REFERENCES tbl_mode_culture (id);');
        $this->addSql('CREATE INDEX IDX_307D5B2A74D5C10E ON parcelle_plan_saison (mode_culture_id);');
    }
}
