<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190320140805 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DROP TABLE  IF EXISTS tbl_unite_filieres;');
        $this->addSql('DROP TABLE  IF EXISTS tbl_provinces_communes;');
        $this->addSql('DROP TABLE IF EXISTS tbl_terre_protection;');
        $this->addSql('DROP TABLE IF EXISTS tbl_unite_mo;');
    }
}
