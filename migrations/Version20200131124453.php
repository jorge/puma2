<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200131124453 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return 'Link many2many intrants et operations culturales';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE tbl_operation_culturale_intrant_tbl_operation_culturale_type (
            tbl_operation_culturale_intrant_id INT NOT NULL,
            tbl_operation_culturale_type_id INT NOT NULL,
            INDEX IDX_CD959DE5663DD5E (tbl_operation_culturale_intrant_id),
            INDEX IDX_CD959DE56DBEF79E (tbl_operation_culturale_type_id),
            PRIMARY KEY(tbl_operation_culturale_intrant_id, tbl_operation_culturale_type_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant_tbl_operation_culturale_type
            ADD CONSTRAINT FK_CD959DE5663DD5E FOREIGN KEY (tbl_operation_culturale_intrant_id) REFERENCES tbl_operation_culturale_intrant (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant_tbl_operation_culturale_type
            ADD CONSTRAINT FK_CD959DE56DBEF79E FOREIGN KEY (tbl_operation_culturale_type_id) REFERENCES tbl_operation_culturale_type (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant DROP FOREIGN KEY FK_3129BF6C640543F3;');
        $this->addSql('DROP INDEX IDX_3129BF6C640543F3 ON tbl_operation_culturale_intrant;');
        $this->addSql('ALTER TABLE tbl_operation_culturale_intrant DROP operation_culturale_type_id;');
    }
}
