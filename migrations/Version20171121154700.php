<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171121154700 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE mo_familiale_externe DROP FOREIGN KEY FK_4DC62DE2F3FDA88C;');
        $this->addSql('ALTER TABLE mo_familiale_externe CHANGE membres membres VARCHAR(255) DEFAULT NULL, CHANGE revenu revenu DECIMAL(18,2);');
        $this->addSql('ALTER TABLE mo_familiale_externe ADD dossier VARCHAR(3) DEFAULT NULL;');
        $this->addSql('UPDATE mo_familiale_externe SET type_travail_id=NULL;');
        $this->addSql('ALTER TABLE mo_familiale_externe ADD CONSTRAINT FK_4DC62DE2F3FDA88C FOREIGN KEY (type_travail_id) REFERENCES tbl_unite_mo (id);');

        //$this->addSql("ALTER TABLE mo_familiale_externe CHANGE membres membres INT(11) DEFAULT NULL;");
        //$this->addSql("ALTER TABLE mo_familiale_interne CHANGE membres membres INT(11) DEFAULT NULL;");
        //$this->addSql("ALTER TABLE tbl_unite_mo SET unite='Jours' WHERE id='794aeabc-2698-11e7-98c8-b8ca3a965972';")
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE mo_familiale_externe DROP FOREIGN KEY FK_4DC62DE2F3FDA88C;');
        $this->addSql('ALTER TABLE mo_familiale_externe DROP dossier, CHANGE membres membres VARCHAR(255) DEFAULT NULL, CHANGE revenu revenu INT DEFAULT NULL;');
        $this->addSql('UPDATE mo_familiale_externe SET type_travail_id=NULL;');
        $this->addSql('ALTER TABLE mo_familiale_externe ADD CONSTRAINT FK_4DC62DE2F3FDA88C FOREIGN KEY (type_travail_id) REFERENCES tbl_travail_type (id);');
        // $this->addSql("ALTER TABLE mo_familiale_interne CHANGE membres membres VARCHAR(255) DEFAULT NULL;");
        // $this->addSql("ALTER TABLE tbl_unite_mo SET unite='Jours/Homme' WHERE id='794aeabc-2698-11e7-98c8-b8ca3a965972';")
    }
}
