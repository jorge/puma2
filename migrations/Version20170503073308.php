<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Generate the member number.
 */
class Version20170503073308 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function down(Schema $schema): void
    {
        throw new Exception('Mais non! Faut pas faire ça, ça été tellement dur de créer ces numéros de membres!!!!', 1);
    }

    public function postUp(Schema $schema): void
    {
        echo 'Generation des numeros de membres (debut)';
        $nMGenerateur = $this->container->get('generateur_numero_membre');
        $nMGenerateur->generationNumeroMembre();
        echo 'Generation des numeros de membres (fin)';
    }

    public function setContainer(?ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE exploitations SET numero_membre=NULL;');
        $this->addSql('ALTER TABLE exploitations CHANGE numero_membre numero_membre INT DEFAULT NULL;');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_A763A7DEAE73E813 ON exploitations (numero_membre);');
    }
}
