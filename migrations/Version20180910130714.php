<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180910130714 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DROP TABLE pivot_user_cooperatives');
    }

    public function up(Schema $schema): void
    {
        $this->addSql("CREATE TABLE pivot_user_cooperatives (
            user_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
            udate DATETIME DEFAULT NOW() ON UPDATE NOW(),
            INDEX IDX_73507243A76ED395 (user_id), INDEX IDX_7350724370E323ED (tbl_organisations_id),
            PRIMARY KEY(user_id, tbl_organisations_id)
        ) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE pivot_user_cooperatives ADD CONSTRAINT FK_73507243A76ED395 FOREIGN KEY (user_id) REFERENCES fos_user (id) ON DELETE CASCADE;');
        $this->addSql('ALTER TABLE pivot_user_cooperatives ADD CONSTRAINT FK_7350724370E323ED FOREIGN KEY (tbl_organisations_id) REFERENCES tbl_organisations (id) ON DELETE CASCADE;');
    }
}
