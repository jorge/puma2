<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200416162636 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->throwIrreversibleMigrationException();
    }

    public function getDescription(): string
    {
        return 'Update the suivi_organisation object for coop (part 1 / IrreversibleMigration)';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE suivi_organisations
            ADD agrement_par_authorite TINYINT(1) NOT NULL,
            ADD alternance_cs_nbr SMALLINT DEFAULT NULL,
            ADD alternance_ce_nbr SMALLINT DEFAULT NULL,
            ADD plan_strategique_pluriannuel TINYINT(1) NOT NULL,
            ADD rapport_suivi_plan_strategique TINYINT(1) NOT NULL,
            DROP annee_renouvellement_ce,
            DROP annee_renouvellement_cs,
            CHANGE annee annee SMALLINT NOT NULL,
            CHANGE existence_roi existence_roi TINYINT(1) NOT NULL,
            CHANGE existence_status existence_status TINYINT(1) NOT NULL,
            CHANGE existence_ce existence_ce TINYINT(1) NOT NULL,
            CHANGE existence_cs existence_cs TINYINT(1) NOT NULL;');
        $this->addSql('CREATE UNIQUE INDEX user_unique ON suivi_organisations (organisation_id, annee);');
        $this->addSql("CREATE TABLE suivi_organisations_tbl_niveaux_reconnaissance (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_niveaux_reconnaissance_id INT NOT NULL, INDEX IDX_9F6F5B45E8E858E2 (suivi_organisations_id),
            INDEX IDX_9F6F5B451EB8FB42 (tbl_niveaux_reconnaissance_id),
            PRIMARY KEY(suivi_organisations_id, tbl_niveaux_reconnaissance_id))
            DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE suivi_organisations_tbl_niveaux_reconnaissance
            ADD CONSTRAINT FK_9F6F5B45E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_niveaux_reconnaissance
            ADD CONSTRAINT FK_9F6F5B451EB8FB42 FOREIGN KEY (tbl_niveaux_reconnaissance_id) REFERENCES tbl_niveaux_reconnaissance (id) ON DELETE CASCADE;');

        $this->addSql('CREATE TABLE tbl_valeur_choix_simple (
                id INT AUTO_INCREMENT NOT NULL,
                valeur VARCHAR(255) NOT NULL,
                entity_class VARCHAR(255) NOT NULL,
                propiete VARCHAR(255) NOT NULL,
                cdate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                udate DATETIME DEFAULT CURRENT_TIMESTAMP NOT NULL,
                utilisateur VARCHAR(255) NOT NULL, PRIMARY KEY(id))
            DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;');

        $this->addSql('ALTER TABLE tbl_valeur_choix_simple MODIFY COLUMN cdate datetime DEFAULT NOW();');
        $this->addSql('ALTER TABLE tbl_valeur_choix_simple MODIFY COLUMN udate datetime DEFAULT NOW() ON UPDATE NOW();');

        $this->addSql("CREATE TABLE suivi_organisations_classement (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_valeur_choix_simple_id INT NOT NULL, INDEX IDX_CC47455DE8E858E2 (suivi_organisations_id),
            INDEX IDX_CC47455DABF24DDC (tbl_valeur_choix_simple_id),
            PRIMARY KEY(suivi_organisations_id, tbl_valeur_choix_simple_id))
            DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE suivi_organisations_classement
            ADD CONSTRAINT FK_CC47455DE8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_classement
            ADD CONSTRAINT FK_CC47455DABF24DDC FOREIGN KEY (tbl_valeur_choix_simple_id) REFERENCES tbl_valeur_choix_simple (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations DROP classement, DROP class_classeur, DROP class_registre, DROP class_rien;');

        $this->addSql("CREATE TABLE suivi_organisations_maj_list_membre (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_valeur_choix_simple_id INT NOT NULL, INDEX IDX_327AD5F5E8E858E2 (suivi_organisations_id),
            INDEX IDX_327AD5F5ABF24DDC (tbl_valeur_choix_simple_id),
            PRIMARY KEY(suivi_organisations_id, tbl_valeur_choix_simple_id))
            DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE suivi_organisations_maj_list_membre
            ADD CONSTRAINT FK_327AD5F5E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_maj_list_membre
            ADD CONSTRAINT FK_327AD5F5ABF24DDC FOREIGN KEY (tbl_valeur_choix_simple_id) REFERENCES tbl_valeur_choix_simple (id) ON DELETE CASCADE;');

        $this->addSql("ALTER TABLE suivi_organisations
            ADD evolution_nbr_membres INT NOT NULL,
            ADD reg_annu_pvreunions_ag INT NOT NULL,
            ADD reg_annu_pvreunions_ce INT NOT NULL,
            ADD reg_annu_pvreunions_cs INT NOT NULL,
            DROP liste_de_membres,
            DROP pv_reunions_ag,
            DROP pv_reunions_ce,
            DROP pv_reunions_cs,
            CHANGE plan_action_annuel plan_action_annuel TINYINT(1) DEFAULT NULL,
            CHANGE rapport_annuel_activites rapport_annuel_activites TINYINT(1) DEFAULT NULL,
            CHANGE compte_bancaire compte_bancaire TINYINT(1) DEFAULT NULL,
            CHANGE signatures_pour_sortie_fonds signatures_pour_sortie_fonds TINYINT(1) DEFAULT NULL,
            CHANGE enregistrement_cotisations enregistrement_cotisations TINYINT(1) DEFAULT NULL,
            CHANGE livre_caisse livre_caisse TINYINT(1) DEFAULT NULL,
            CHANGE carnet_recus carnet_recus TINYINT(1) DEFAULT NULL,
            CHANGE livret_societaire livret_societaire TINYINT(1) DEFAULT NULL,
            CHANGE classement_justificatifs classement_justificatifs TINYINT(1) DEFAULT '0' NOT NULL,
            CHANGE rapports_financiere_annuel rapports_financiere_annuel TINYINT(1) DEFAULT NULL;");

        $this->addSql("CREATE TABLE suivi_organisations_comptabilite (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_valeur_choix_simple_id INT NOT NULL,
            INDEX IDX_F07951A7E8E858E2 (suivi_organisations_id), INDEX IDX_F07951A7ABF24DDC (tbl_valeur_choix_simple_id),
            PRIMARY KEY(suivi_organisations_id, tbl_valeur_choix_simple_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE suivi_organisations_tbl_services_aux_membres (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_services_aux_membres_id INT NOT NULL,
            INDEX IDX_82EA5D09E8E858E2 (suivi_organisations_id), INDEX IDX_82EA5D093B8329E (tbl_services_aux_membres_id),
            PRIMARY KEY(suivi_organisations_id, tbl_services_aux_membres_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE suivi_organisations_tbl_services_payants (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_services_payants_id INT NOT NULL,
            INDEX IDX_EEB95CF8E8E858E2 (suivi_organisations_id), INDEX IDX_EEB95CF893627715 (tbl_services_payants_id),
            PRIMARY KEY(suivi_organisations_id, tbl_services_payants_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE suivi_organisations_tbl_prestations_services (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_prestations_services_id INT NOT NULL,
            INDEX IDX_4F7D74A5E8E858E2 (suivi_organisations_id), INDEX IDX_4F7D74A5F9F70F5B (tbl_prestations_services_id),
            PRIMARY KEY(suivi_organisations_id, tbl_prestations_services_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE suivi_organisations_tbl_infrastructures (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_infrastructures_id INT NOT NULL,
            INDEX IDX_266F5C3DE8E858E2 (suivi_organisations_id), INDEX IDX_266F5C3D133F37C0 (tbl_infrastructures_id),
            PRIMARY KEY(suivi_organisations_id, tbl_infrastructures_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE suivi_organisations_tbl_unites_transformations (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_unites_transformations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            INDEX IDX_16C98A70E8E858E2 (suivi_organisations_id), INDEX IDX_16C98A709B8E2E33 (tbl_unites_transformations_id),
            PRIMARY KEY(suivi_organisations_id, tbl_unites_transformations_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE suivi_organisations_tbl_moyen_transport (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_moyen_transport_id INT NOT NULL,
            INDEX IDX_399A1E1BE8E858E2 (suivi_organisations_id), INDEX IDX_399A1E1B20672699 (tbl_moyen_transport_id),
            PRIMARY KEY(suivi_organisations_id, tbl_moyen_transport_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE suivi_organisations_tbl_materiels_bureaux (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_materiels_bureaux_id INT NOT NULL,
            INDEX IDX_542A6940E8E858E2 (suivi_organisations_id), INDEX IDX_542A6940666F957B (tbl_materiels_bureaux_id),
            PRIMARY KEY(suivi_organisations_id, tbl_materiels_bureaux_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql("CREATE TABLE suivi_organisations_tbl_equipements_agricoles (
            suivi_organisations_id CHAR(36) NOT NULL COMMENT '(DC2Type:guid)',
            tbl_equipements_agricoles_id INT NOT NULL,
            INDEX IDX_5E2EC55EE8E858E2 (suivi_organisations_id), INDEX IDX_5E2EC55EE578940B (tbl_equipements_agricoles_id),
            PRIMARY KEY(suivi_organisations_id, tbl_equipements_agricoles_id)) DEFAULT CHARACTER SET utf8 COLLATE `utf8_unicode_ci` ENGINE = InnoDB;");

        $this->addSql('ALTER TABLE suivi_organisations_comptabilite ADD CONSTRAINT FK_F07951A7E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_comptabilite ADD CONSTRAINT FK_F07951A7ABF24DDC FOREIGN KEY (tbl_valeur_choix_simple_id) REFERENCES tbl_valeur_choix_simple (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_services_aux_membres ADD CONSTRAINT FK_82EA5D09E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_services_aux_membres ADD CONSTRAINT FK_82EA5D093B8329E FOREIGN KEY (tbl_services_aux_membres_id) REFERENCES tbl_services_aux_membres (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_services_payants ADD CONSTRAINT FK_EEB95CF8E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_services_payants ADD CONSTRAINT FK_EEB95CF893627715 FOREIGN KEY (tbl_services_payants_id) REFERENCES tbl_services_payants (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_prestations_services ADD CONSTRAINT FK_4F7D74A5E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_prestations_services ADD CONSTRAINT FK_4F7D74A5F9F70F5B FOREIGN KEY (tbl_prestations_services_id) REFERENCES tbl_prestations_services (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_infrastructures ADD CONSTRAINT FK_266F5C3DE8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_infrastructures ADD CONSTRAINT FK_266F5C3D133F37C0 FOREIGN KEY (tbl_infrastructures_id) REFERENCES tbl_infrastructures (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_unites_transformations ADD CONSTRAINT FK_16C98A70E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_moyen_transport ADD CONSTRAINT FK_399A1E1BE8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_moyen_transport ADD CONSTRAINT FK_399A1E1B20672699 FOREIGN KEY (tbl_moyen_transport_id) REFERENCES tbl_moyen_transport (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_materiels_bureaux ADD CONSTRAINT FK_542A6940E8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_materiels_bureaux ADD CONSTRAINT FK_542A6940666F957B FOREIGN KEY (tbl_materiels_bureaux_id) REFERENCES tbl_materiels_bureaux (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_equipements_agricoles ADD CONSTRAINT FK_5E2EC55EE8E858E2 FOREIGN KEY (suivi_organisations_id) REFERENCES suivi_organisations (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations_tbl_equipements_agricoles ADD CONSTRAINT FK_5E2EC55EE578940B FOREIGN KEY (tbl_equipements_agricoles_id) REFERENCES tbl_equipements_agricoles (id) ON DELETE CASCADE;');

        $this->addSql('ALTER TABLE suivi_organisations
        ADD manuel_procedures TINYINT(1) DEFAULT NULL,
        ADD doubles_signatures TINYINT(1) DEFAULT NULL,
        ADD rapport_cotisation_agrtotal_produit DOUBLE PRECISION DEFAULT NULL,
        ADD budget DOUBLE PRECISION DEFAULT NULL,
        ADD chiffre_affaire_par_membre DOUBLE PRECISION DEFAULT NULL,
        ADD degre_prise_en_charge_ag INT DEFAULT NULL,
        ADD degre_prise_en_charge_ca INT DEFAULT NULL,
        ADD degre_prise_en_charge_cs INT DEFAULT NULL,
        ADD affiliation_federation TINYINT(1) DEFAULT NULL,
        ADD nbr_contrats_avec_acteurs_eco INT DEFAULT NULL,
        ADD nbr_credit_avec_imfou_banque INT DEFAULT NULL,
        CHANGE enregistrement_cotisations enregistrement_cotisations TINYINT(1) NOT NULL,
        CHANGE classement_justificatifs classement_justificatifs TINYINT(1) DEFAULT NULL;');
    }
}
