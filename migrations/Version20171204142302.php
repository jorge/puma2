<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171204142302 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE parcelle_techniques DROP FOREIGN KEY FK_EBB928F9906F1B44;');
        $this->addSql('DROP INDEX IDX_EBB928F9906F1B44 ON parcelle_techniques;');
        $this->addSql('ALTER TABLE parcelle_techniques DROP mo_type_id;');
        $this->addSql('ALTER TABLE parcelle_techniques ADD quantite_mosal NUMERIC(18, 2) DEFAULT NULL, CHANGE quantite quantite_mofam NUMERIC(18, 2) DEFAULT NULL;');
    }
}
