<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200709163927 extends AbstractMigration implements ContainerAwareInterface
{
    private $container;

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function getDescription(): string
    {
        return '';
    }

    public function preUp(Schema $schema): void
    {
        parent::preUp($schema);

        $em = $this->container->get('doctrine.orm.entity_manager');
        $orgs = $em->getRepository('AppBundle:TblOrganisations')->findAll();

        $cpt = 1;

        foreach ($orgs as $org) {
            $org->setCode($cpt);
            $cpt = $cpt + 1;
            echo $cpt;
        }

        $em->flush();
    }

    public function setContainer(?ContainerInterface $container = null)
    {
        $this->container = $container;
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_organisations CHANGE code code INT DEFAULT NULL;');
    }
}
