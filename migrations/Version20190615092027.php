<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190615092027 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DELETE FROM tbl_type_creancier WHERE id=5;');
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO tbl_type_creancier (id, creancier, udate, cdate, utilisateur) VALUES
		     (5, 'Non renseigné', '2019-06-15 09:28:20', '2019-06-15 09:28:20', 'jorge');");
    }
}
