<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190307083954 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE tbl_engrais ADD unite_id INT DEFAULT NULL;');
        $this->addSql('ALTER TABLE tbl_engrais
            ADD CONSTRAINT FK_F34D4857EC4A74AB FOREIGN KEY (unite_id)
            REFERENCES tbl_unite_produits (id);');
        $this->addSql('CREATE INDEX IDX_F34D4857EC4A74AB ON tbl_engrais (unite_id);');
    }
}
