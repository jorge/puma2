<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180522125505 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        /* effacer les données de la table*/
        $this->addSql('DELETE FROM tbl_labour_animaux');
    }

    public function up(Schema $schema): void
    {
        /* populate table  tbl_labour_animaux*/
        $this->addSql("INSERT INTO tbl_labour_animaux (id, labour, cdate, udate, utilisateur) VALUES
                    ('d3d75750-5dbe-11e8-9b41-b8ca3a965972', 'Nourrisage', '2018-05-22 12:51:24', '2018-05-22 12:51:24', 'jorge'),
                    ('d3dd0a71-5dbe-11e8-9b41-b8ca3a965972', 'Abreuvage', '2018-05-22 12:51:24', '2018-05-22 12:51:24', 'jorge'),
                    ('d3dd0b9f-5dbe-11e8-9b41-b8ca3a965972', 'Collecte fourrage', '2018-05-22 12:51:24', '2018-05-22 12:51:24', 'jorge'),
                    ('d3dd0bf2-5dbe-11e8-9b41-b8ca3a965972', 'Parcours', '2018-05-22 12:51:24', '2018-05-22 12:51:24', 'jorge'),
                    ('d3dd0c3c-5dbe-11e8-9b41-b8ca3a965972', 'Nettoyage étable', '2018-05-22 12:51:24', '2018-05-22 12:51:24', 'jorge'),
                    ('d3dd0c7f-5dbe-11e8-9b41-b8ca3a965972', 'Traite/ramassage', '2018-05-22 12:51:24', '2018-05-22 12:51:24', 'jorge'),
                    ('d3dd0cc3-5dbe-11e8-9b41-b8ca3a965972', 'Soins', '2018-05-22 12:51:24', '2018-05-22 12:51:24', 'jorge');");
    }
}
