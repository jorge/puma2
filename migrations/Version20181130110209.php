<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20181130110209 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM tbl_professions WHERE id=11;');
    }

    public function up(Schema $schema): void
    {
        $this->addSql("INSERT INTO tbl_professions (id, old_id, profession, cdate, udate, utilisateur) VALUES (11, NULL, 'Non renseigné', '2018-11-30 10:54:22', '2018-11-30 10:54:22', 'jorge');");
    }
}
