<?php

declare(strict_types=1);

namespace Application\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Exception;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190424151747 extends AbstractMigration
{
    public function down(Schema $schema): void
    {
        throw new Exception('Pas de retour en arrière', 1);
    }

    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql("ALTER TABLE exploitation_plan_saison CHANGE radie radie TINYINT(1) DEFAULT '0' NOT NULL;");
        $this->addSql('CREATE UNIQUE INDEX exploitation_plan_saison__collect_saison_unique ON exploitation_plan_saison (collecte_id, saison_plan_id);');
    }
}
