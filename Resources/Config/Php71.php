<?php

declare(strict_types=1);

use drupol\PhpCsFixerConfigsPhp\Config\Php73;

$config = new Php73();

$config
    ->getFinder()
    ->exclude(['build', 'libraries', 'node_modules', 'vendor', 'var', 'tests', 'templates', 'migrations']);

return $config;
