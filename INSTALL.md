# Install procedure for developper

## Using docker-compose

Beware the installation described in this section is not secure at all.
It is an installation for developpement. Do not install your production version like this.

### Requirements

- docker-compose (https://docs.docker.com/compose/)


### Clone the repository

This step will create a directory `puma2` containing the source code of puma.

```bash
$ git clone git@framagit.org:jorge/puma2.git
```

### Install the php dependencies

Create a version of `parameters.yml` to be sure that it will not be loaded as a directory.

```bash
$ cd puma2
puma2 $ cp app/config/parameters.yml.dist app/config/parameters.yml
```

Then create a version of `parameters.yml` for the instances `master` and local `local`.

**Note:** it's not required to install both *\_local* and *\_master* containers. Here we describe complete installation, but if yout want you can just configure *\_local* containers and it will works.

```bash
$ cp _docker/local/parameters.yml.dist _docker/local/parameters.yml
$ cp _docker/master/parameters.yml.dist _docker/master/parameters.yml
```

Install symfony dependencies with composer

```bash
$ docker-compose run web_local composer install
$ docker-compose run web_master composer install
```

(`ctrl D` to exit from the container web_local)

### Start the app with docker-compose

```bash
$ docker-compose up
```


### Configure the database

Allow connection to root from another host.

```bash
$ docker-compose exec db_local  mysql -pblopblop -e "CREATE USER 'root'@'%' IDENTIFIED BY 'blopblop';"
$ docker-compose exec db_master  mysql -pblopblop -e "CREATE USER 'root'@'%' IDENTIFIED BY 'blopblop';"
```

If it goes well, your should connect to the db from your computer as follows :

```bash
$ mysql -P 3306 -h 0.0.0.0 -u root -p
$ mysql -P 3308 -h 0.0.0.0 -u root -p
```

### Create the databases

For `db_local` :

```bash
$ docker-compose exec db_local mysql -pblopblop mysql

mysql > CREATE DATABASE IF NOT EXISTS puma_03 DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
mysql > GRANT ALL ON puma_03.* to root@'%';
```

For `db_master` :

```bash
$ docker-compose exec db_master mysql -pblopblop mysql

mysql > CREATE DATABASE IF NOT EXISTS puma_03 DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
mysql > GRANT ALL ON puma_03.* to root@'%';
```

### Load the data

First extract downloaded compressed file :

```bash
$ gunzip backup.sql.gz
```

Then copy backup sql inside the container :

```bash
$ docker cp /$(pwd)/backup.sql $(docker ps | grep db_local| cut -f1 -d\ ):/tmp/
$ docker cp /$(pwd)/backup.sql $(docker ps | grep db_master| cut -f1 -d\ ):/tmp/
```

Inject datas in db_local :

```bash
$ docker-compose exec db_local bash
$ mysql -h db_local  -u root -pblopblop puma_03 < /tmp/backup.sql
```
Inject datas in db_master :

```bash
$ docker-compose exec db_master bash

$ mysql -P 3308 -h 0.0.0.0 -u root -pblopblop puma_03 < /tmp/backup.sql
```

### Fix errors

* If you have the error `Access denied; you need (at least one of) the SUPER or SET_USER_ID privilege(s) for this operation`  when you try to load the data,

Fix it as follows :

```bash
$ docker-compose exec db_local_or_master mysql -pblopblop mysql

mysql > UPDATE mysql.user SET Super_Priv='Y' WHERE user='root' AND host='%';
mysql > FLUSH PRIVILEGES;
```

* If you have the sql error `An exception occurred in driver: SQLSTATE[HY000] [2054] The server requested authentication method unknown to the client` when you try
to access to the website from `localhost:8000`

Fix it as follows :

```bash
$ docker-compose exec db_local_or_master mysql -pblopblop mysql

mysql > ALTER USER 'root'@'%' IDENTIFIED WITH mysql_native_password BY 'blopblop';
```

That's it.
