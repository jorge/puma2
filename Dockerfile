FROM php:7.4-fpm

MAINTAINER <info@champs-libres.coop>

RUN apt-get update  && apt-get -y install \
    zlib1g-dev \
    mariadb-client \
    curl \
    ftp \
    libpng-dev \
    libjpeg-dev \
    libfreetype6-dev \
    libjpeg62-turbo-dev \
    libzip-dev \
    wget \
    git

RUN docker-php-ext-install mysqli pdo pdo_mysql zip
RUN docker-php-ext-configure gd --with-jpeg
RUN docker-php-ext-install gd
RUN docker-php-ext-install opcache

#php configuration
RUN printf "\n[Date]\ndate.timezone = Europe/Brussels\n" >> /usr/local/etc/php/php.ini
RUN printf "\nping.path = /ping\npm.status_path = /status\n" >> /usr/local/etc/php-fpm.conf
RUN printf "\nmemory_limit=-1\n" >> /usr/local/etc/php/php.ini
RUN printf "\nupload_max_filesize=40M\n" >> /usr/local/etc/php/php.ini
RUN printf "\npost_max_size=40M\n" >> /usr/local/etc/php/php.ini

WORKDIR /var/www/puma

RUN echo "Install composer"
COPY --from=composer:latest /usr/bin/composer /usr/bin/composer
# https://getcomposer.org/doc/03-cli.md#composer-allow-superuser
ENV COMPOSER_ALLOW_SUPERUSER=1

COPY --from=symfonycorp/cli:latest symfony /usr/bin/symfony

# INSTALL phpunit
ADD https://phar.phpunit.de/phpunit-7.phar /usr/local/bin/phpunit
RUN chmod +x /usr/local/bin/phpunit


#VOLUME ["/var/www/cyclab/web/js", "/var/www/cyclab/web/css", "/var/www/cyclab/web/bundles", "/var/www/cyclab/app/logs" , "/var/www/cyclab/app/config/", "/var/www/cyclab/web/help", "/var/www/cyclab/web/db_dump"]

#CMD ["/entrypoint.sh"]
