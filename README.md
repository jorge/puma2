Programme Unifié de Monitoring Agricole V2 (PUMA2)
==================================================

**[FR]** PUMA2 est un logiciel pour le suivi d'exploitations agricoles.

Son développement a été initié et soutenu par l'ASBL Collectif Stratégies Alimentaires (http://csa-be.org).

PUMA2 est un logiciel libre. PUMA2 est sous license AGPL3 (GNU General Public License version 3).


**[EN]** PUMA2 est is a software for agricultural monitoring.

PUMA2 was created and developped by ASBL Collectif Stratégies Alimentaires (http://csa-be.org).

PUMA2 is free software under the license AGPL3 (GNU General Public License version 3).




## How to install

See file `ÌNSTALL.md`